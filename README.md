# power-generation-api
This repository contains the backend API that drives the visualization of annual net generation data for power plants in the United States. It provides the necessary data and functionalities to support the display of top power plants.

### Notes about the Code
Please take a moment to review this [commit](https://gitlab.com/nermeen-mattar/power-generation-api/-/commit/ecbfffa852edee9cb22f5dcffdb7248beb4b0535) as it represents an important improvement. Prior to this commit, the API supported separate calls for filtering by state and by top plants. However, I have merged these functionalities into a single call, resulting in a more streamlined codebase. This change has a positive impact on the frontend as well, reducing the number of API calls required. Users now have the flexibility to filter power plants based on their preferences, including viewing all plants in the US or narrowing down the results to the top plants in a specific state. Additionally, users can choose to filter by the top plants across all states in the US.

### Hints for Running the Project
The data used in this solution is derived from one of the sheets in the Excel file provided in the challenge document. I downloaded this data in CSV format and then converted it directly to JSON. If you wish to test the API, you can find a [ready-to-use JSON file](https://drive.google.com/file/d/1thb-cwKcpvWRD5-i_v_rckfblNJiqDMt/view?usp=sharing). It's worth noting that uploading the full data can take up to approximately 5 minutes. To save time, consider uploading only a chunk of the data.

Please be aware that retrieving all power plants (without applying any filters) may also take some time, as the API currently supports the upload of all 25,031 power plant generation data objects.

Regarding the plant coordinates data, we have a total of 11,366 objects I didn't create a separate endpoint for uploading the plant coordinates since it follows a similar process.

Swagger Documentation
For further exploration and interaction with the API, please refer to the following [Swagger link](https://power-generation-service.onrender.com/api-docs/).
