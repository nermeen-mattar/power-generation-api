const PlantsData = {
    "Agrium Kenai Nitrogen Operations": {
      "lng": -151.3784,
      "lat": 60.6732
    },
    "Alakanuk": {
      "lng": -164.6544,
      "lat": 62.6833
    },
    "Allison Creek Hydro": {
      "lng": -146.353333,
      "lat": 61.084444
    },
    "Ambler": {
      "lng": -157.856719,
      "lat": 67.08798
    },
    "Angoon": {
      "lng": -134.58614,
      "lat": 57.499166
    },
    "Aniak": {
      "lng": -159.535643,
      "lat": 61.580678
    },
    "Annex Creek": {
      "lng": -134.101,
      "lat": 58.3176
    },
    "Auke Bay": {
      "lng": -134.6446,
      "lat": 58.3875
    },
    "Aurora Energy LLC Chena": {
      "lng": -147.735063,
      "lat": 64.847743
    },
    "Barrow": {
      "lng": -156.7786,
      "lat": 71.292
    },
    "Battery Energy Storage System": {
      "lng": -147.725,
      "lat": 64.8167
    },
    "Beaver Falls": {
      "lng": -131.470269,
      "lat": 55.37975
    },
    "Beluga": {
      "lng": -151.0356,
      "lat": 61.1861
    },
    "Bernice Lake": {
      "lng": -151.3874,
      "lat": 60.6935
    },
    "Bethel": {
      "lng": -161.787778,
      "lat": 60.7897
    },
    "Black Bear Lake": {
      "lng": -133.14772,
      "lat": 55.476472
    },
    "Blue Lake Hydro": {
      "lng": -135.2297,
      "lat": 57.0516
    },
    "Bradley Lake": {
      "lng": -150.94015,
      "lat": 59.77862
    },
    "Brevig Mission": {
      "lng": -166.479506,
      "lat": 65.331716
    },
    "Centennial": {
      "lng": -131.560267,
      "lat": 55.121433
    },
    "Chester Lake": {
      "lng": -131.545879,
      "lat": 55.116879
    },
    "Chevak": {
      "lng": -165.590152,
      "lat": 61.525297
    },
    "Cooper Lake": {
      "lng": -149.665603,
      "lat": 60.392331
    },
    "Craig (AK)": {
      "lng": -133.14869,
      "lat": 55.47691
    },
    "Delta Power": {
      "lng": -145.719444,
      "lat": 64.028056
    },
    "Delta Wind Farm": {
      "lng": -145.596667,
      "lat": 64.013889
    },
    "Dillingham": {
      "lng": -158.468597,
      "lat": 59.042914
    },
    "Dutch Harbor": {
      "lng": -166.538185,
      "lat": 53.892459
    },
    "Eielson AFB Central Heat & Power Plant": {
      "lng": -147.075988,
      "lat": 64.67141
    },
    "Eklutna Generation Station": {
      "lng": -149.351389,
      "lat": 61.457778
    },
    "Eklutna Hydro Project": {
      "lng": -149.15009,
      "lat": 61.475211
    },
    "Elim": {
      "lng": -162.263717,
      "lat": 64.616558
    },
    "Emmonak": {
      "lng": -164.531517,
      "lat": 62.777694
    },
    "ESS Battery Microgrid": {
      "lng": -152.404167,
      "lat": 57.799167
    },
    "Eva Creek Wind": {
      "lng": -148.9,
      "lat": 64.058333
    },
    "Eyak Service Center BESS": {
      "lng": -145.74079,
      "lat": 60.54047
    },
    "Fairbanks": {
      "lng": -147.719351,
      "lat": 64.854171
    },
    "False Island": {
      "lng": -133.1345,
      "lat": 55.48918
    },
    "Fire Island Wind": {
      "lng": -150.243611,
      "lat": 61.13
    },
    "Flywheel Energy Storage System Microgrid": {
      "lng": -152.443783,
      "lat": 57.780114
    },
    "Fort Greely Power Plant": {
      "lng": -145.716581,
      "lat": 63.973572
    },
    "Galena Electric Utility": {
      "lng": -156.8736,
      "lat": 64.74417
    },
    "Gambell": {
      "lng": -171.712439,
      "lat": 63.777058
    },
    "George M Sullivan Generation Plant 2": {
      "lng": -149.716744,
      "lat": 61.229713
    },
    "Glennallen": {
      "lng": -145.532529,
      "lat": 62.110415
    },
    "Goat Lake Hydro": {
      "lng": -135.2123,
      "lat": 59.5357
    },
    "Gold Creek": {
      "lng": -134.4174,
      "lat": 58.3107
    },
    "Green Lake": {
      "lng": -135.12275,
      "lat": 56.986284
    },
    "Gustavus": {
      "lng": -135.70737,
      "lat": 58.419497
    },
    "Gwitchyaa Zhee": {
      "lng": -145.253052,
      "lat": 66.566287
    },
    "Haines": {
      "lng": -135.446228,
      "lat": 59.235931
    },
    "Hank Nikkels Plant 1": {
      "lng": -149.8661,
      "lat": 61.2221
    },
    "Healy Power Plant": {
      "lng": -148.95,
      "lat": 63.8542
    },
    "Hiilangaay Hydro": {
      "lng": -132.603611,
      "lat": 55.216389
    },
    "Hoonah": {
      "lng": -135.43074,
      "lat": 58.106432
    },
    "Hooper Bay": {
      "lng": -166.101944,
      "lat": 61.530858
    },
    "Humpback Creek": {
      "lng": -145.679411,
      "lat": 60.612822
    },
    "Hydaburg": {
      "lng": -132.821435,
      "lat": 55.204937
    },
    "Industrial Plant": {
      "lng": -134.60802,
      "lat": 58.367635
    },
    "International": {
      "lng": -149.911038,
      "lat": 61.168972
    },
    "Jarvis Street": {
      "lng": -135.3128,
      "lat": 57.0497
    },
    "JBER Landfill Gas Power Plant": {
      "lng": -149.61,
      "lat": 61.286
    },
    "Kake": {
      "lng": -133.922557,
      "lat": 56.962983
    },
    "Kasidaya Creek Hydro": {
      "lng": -135.3408,
      "lat": 59.4072
    },
    "Kasigluk": {
      "lng": -162.5197,
      "lat": 60.8731
    },
    "Ketchikan": {
      "lng": -131.633425,
      "lat": 55.344641
    },
    "Kiana": {
      "lng": -160.428592,
      "lat": 66.973889
    },
    "King Cove": {
      "lng": -162.3103,
      "lat": 55.061683
    },
    "Kivalina": {
      "lng": -164.538447,
      "lat": 67.726644
    },
    "Klawock Power Generation Station": {
      "lng": -133.08535,
      "lat": 55.553197
    },
    "Kodiak Microgrid": {
      "lng": -152.396982,
      "lat": 57.789956
    },
    "Kotlik": {
      "lng": -163.553106,
      "lat": 63.03215
    },
    "Kotzebue Hybrid": {
      "lng": -162.556944,
      "lat": 66.837778
    },
    "Koyuk": {
      "lng": -161.167103,
      "lat": 64.932089
    },
    "Lake Dorothy Hydroelectric Project": {
      "lng": -134.0533,
      "lat": 58.2325
    },
    "Lemon Creek": {
      "lng": -134.4953,
      "lat": 58.3536
    },
    "Marshall": {
      "lng": -80.9658,
      "lat": 35.5975
    },
    "McGrath": {
      "lng": -155.594997,
      "lat": 62.95699
    },
    "Mountain Village": {
      "lng": -163.729072,
      "lat": 62.085569
    },
    "Naknek": {
      "lng": -157.007222,
      "lat": 58.730417
    },
    "New Stuyahok": {
      "lng": -157.325528,
      "lat": 59.448358
    },
    "Newhalen": {
      "lng": -154.698735,
      "lat": 59.899054
    },
    "Nikiski Combined Cycle": {
      "lng": -151.377713,
      "lat": 60.676539
    },
    "Noatak": {
      "lng": -162.965728,
      "lat": 67.570931
    },
    "Noorvik": {
      "lng": -161.038717,
      "lat": 66.834519
    },
    "North Pole": {
      "lng": -147.3481,
      "lat": 64.7356
    },
    "Northway": {
      "lng": -141.9372,
      "lat": 62.9617
    },
    "NSB Anaktuvuk Pass": {
      "lng": -151.741017,
      "lat": 68.13795
    },
    "NSB Atqasuk Utility": {
      "lng": -157.4252,
      "lat": 70.4826
    },
    "NSB Kaktovik Utility": {
      "lng": -143.619033,
      "lat": 70.125617
    },
    "NSB Nuiqsut Utility": {
      "lng": -150.993492,
      "lat": 70.220565
    },
    "NSB Point Hope Utility": {
      "lng": -166.737211,
      "lat": 68.348424
    },
    "NSB Point Lay Utility": {
      "lng": -163.005833,
      "lat": 69.740833
    },
    "NSB Wainwright Utility": {
      "lng": -160.020461,
      "lat": 70.642877
    },
    "Nunapitchuk": {
      "lng": -162.459756,
      "lat": 60.89588
    },
    "Nymans Plant Microgrid": {
      "lng": -152.507044,
      "lat": 57.731608
    },
    "Orca": {
      "lng": -145.752983,
      "lat": 60.555889
    },
    "Pelican": {
      "lng": -136.220095,
      "lat": 57.957197
    },
    "Petersburg": {
      "lng": -132.957091,
      "lat": 56.81104
    },
    "Pillar Mountain Wind Project Microgrid": {
      "lng": -152.4406,
      "lat": 57.7869
    },
    "Pilot Station": {
      "lng": -162.880706,
      "lat": 61.936456
    },
    "Port Lions Microgrid": {
      "lng": -152.855441,
      "lat": 57.864775
    },
    "Power Creek": {
      "lng": -145.604539,
      "lat": 60.588686
    },
    "Purple Lake": {
      "lng": -131.544978,
      "lat": 55.091263
    },
    "Quinhagak": {
      "lng": -161.910647,
      "lat": 59.747436
    },
    "S W Bailey": {
      "lng": -131.69696,
      "lat": 55.357397
    },
    "Salmon Creek 1": {
      "lng": -134.4631,
      "lat": 58.3269
    },
    "Sand Point": {
      "lng": -160.497222,
      "lat": 55.339722
    },
    "Savoonga": {
      "lng": -170.475661,
      "lat": 63.695267
    },
    "Scammon Bay": {
      "lng": -165.581497,
      "lat": 61.843036
    },
    "Selawik": {
      "lng": -160.014808,
      "lat": 66.606778
    },
    "Seldovia": {
      "lng": -151.713439,
      "lat": 59.439542
    },
    "Seward (AK)": {
      "lng": -149.43501,
      "lat": 60.130922
    },
    "Shishmaref": {
      "lng": -166.073589,
      "lat": 66.255072
    },
    "Shungnak": {
      "lng": -157.140206,
      "lat": 66.888114
    },
    "Silvis": {
      "lng": -131.517758,
      "lat": 55.381402
    },
    "Skagway": {
      "lng": -135.3131,
      "lat": 59.4545
    },
    "Slana Generating Station": {
      "lng": -143.588869,
      "lat": 62.592756
    },
    "Snake River": {
      "lng": -165.429814,
      "lat": 64.505331
    },
    "Snettisham": {
      "lng": -133.737,
      "lat": 58.1415
    },
    "Soldotna": {
      "lng": -150.997222,
      "lat": 60.499444
    },
    "Solomon Gulch": {
      "lng": -146.3033,
      "lat": 61.0828
    },
    "South Fork": {
      "lng": -132.891111,
      "lat": 55.563333
    },
    "Southcentral Power Project": {
      "lng": -149.905304,
      "lat": 61.167417
    },
    "St Marys IC": {
      "lng": -163.172567,
      "lat": 62.051525
    },
    "St. Michael": {
      "lng": -162.0383,
      "lat": 63.4775
    },
    "Stebbins": {
      "lng": -162.286322,
      "lat": 63.521047
    },
    "Swampy Acres Microgrid": {
      "lng": -152.48028,
      "lat": 57.77556
    },
    "Swan Lake": {
      "lng": -131.356111,
      "lat": 55.615208
    },
    "Terror Lake Microgrid": {
      "lng": -152.895,
      "lat": 57.6861
    },
    "Tesoro Kenai Cogeneration Plant": {
      "lng": -151.3815,
      "lat": 60.677
    },
    "Thorne Bay Plant": {
      "lng": -132.52892,
      "lat": 55.68586
    },
    "TNSG North Plant": {
      "lng": -148.383611,
      "lat": 70.235278
    },
    "TNSG South Plant": {
      "lng": -148.466667,
      "lat": 70.2
    },
    "Togiak": {
      "lng": -160.380278,
      "lat": 59.059744
    },
    "Tok": {
      "lng": -142.99997,
      "lat": 63.33552
    },
    "Toksook Bay": {
      "lng": -165.108575,
      "lat": 60.530142
    },
    "Tyee Lake Hydroelectric Facility": {
      "lng": -131.504344,
      "lat": 56.216403
    },
    "Unalakleet": {
      "lng": -160.790414,
      "lat": 63.876789
    },
    "Unalaska Power Module": {
      "lng": -166.512591,
      "lat": 53.863993
    },
    "Unisea G 2": {
      "lng": -166.5532,
      "lat": 53.8796
    },
    "University of Alaska Fairbanks": {
      "lng": -147.822075,
      "lat": 64.854171
    },
    "Upper Kalskag": {
      "lng": -160.348128,
      "lat": 61.526858
    },
    "Utility Plants Section": {
      "lng": -147.648627,
      "lat": 64.825601
    },
    "Valdez": {
      "lng": -146.3647,
      "lat": 61.1303
    },
    "Valdez Cogen": {
      "lng": -146.2529,
      "lat": 61.0839
    },
    "Viking": {
      "lng": -133.102344,
      "lat": 55.540708
    },
    "Westward Seafoods": {
      "lng": -166.55288,
      "lat": 53.858508
    },
    "Whitman": {
      "lng": -131.530833,
      "lat": 55.328056
    },
    "Wrangell": {
      "lng": -132.379439,
      "lat": 56.460976
    },
    "Yakutat": {
      "lng": -139.724306,
      "lat": 59.544553
    },
    "ABC Coke": {
      "lng": -86.779866,
      "lat": 33.582793
    },
    "Alabama Pine Pulp": {
      "lng": -87.4889,
      "lat": 31.5825
    },
    "Alabama River Pulp": {
      "lng": -87.4889,
      "lat": 31.5825
    },
    "Albertville": {
      "lng": -86.223889,
      "lat": 34.363389
    },
    "AMEA Sylacauga Plant": {
      "lng": -86.2825,
      "lat": 33.1661
    },
    "ANAD Solar Array": {
      "lng": -85.969481,
      "lat": 33.626728
    },
    "Bankhead Dam": {
      "lng": -87.356823,
      "lat": 33.458665
    },
    "Barry": {
      "lng": -88.0103,
      "lat": 31.0069
    },
    "Bartletts Ferry": {
      "lng": -85.0908,
      "lat": 32.663
    },
    "Bellefonte": {
      "lng": -85.9278,
      "lat": 34.7092
    },
    "Browns Ferry": {
      "lng": -87.1189,
      "lat": 34.7042
    },
    "Calhoun Generating Facility": {
      "lng": -85.9731,
      "lat": 33.5883
    },
    "Central Alabama Gen Station": {
      "lng": -86.74,
      "lat": 32.6497
    },
    "Colbert": {
      "lng": -87.8486,
      "lat": 34.7439
    },
    "Crestwood Dothan": {
      "lng": -85.402737,
      "lat": 31.262356
    },
    "Cumblerland Land Holdings, LLC": {
      "lng": -86.868053,
      "lat": 34.94974
    },
    "Decatur Energy Center": {
      "lng": -87.0214,
      "lat": 34.6292
    },
    "Decatur-Morgan Co LFG Recovery Project": {
      "lng": -87.101111,
      "lat": 34.623611
    },
    "E B Harris Generating Plant": {
      "lng": -86.574366,
      "lat": 32.381352
    },
    "E C Gaston": {
      "lng": -86.458056,
      "lat": 33.244211
    },
    "Fort Rucker Solar Array": {
      "lng": -85.73019,
      "lat": 31.331148
    },
    "Gadsden": {
      "lng": -85.9708,
      "lat": 34.0128
    },
    "Gantt": {
      "lng": -86.479469,
      "lat": 31.4033
    },
    "Georgia-Pacific Brewton Mill": {
      "lng": -87.1116,
      "lat": 31.077
    },
    "Georgia-Pacific Consr Prods LP-Naheola": {
      "lng": -88.0252,
      "lat": 32.2268
    },
    "Goat Rock": {
      "lng": -85.0795,
      "lat": 32.6096
    },
    "Greene County": {
      "lng": -87.7811,
      "lat": 32.6017
    },
    "Guntersville": {
      "lng": -86.393931,
      "lat": 34.421272
    },
    "H Neely Henry Dam": {
      "lng": -86.0524,
      "lat": 33.7845
    },
    "Harris Dam": {
      "lng": -85.616047,
      "lat": 33.258281
    },
    "Hillabee Energy Center": {
      "lng": -85.903278,
      "lat": 33.000669
    },
    "Hog Bayou Energy Center": {
      "lng": -88.0575,
      "lat": 30.7478
    },
    "Holt Dam": {
      "lng": -87.4495,
      "lat": 33.2553
    },
    "International Paper Pine Hill Mill": {
      "lng": -87.4806,
      "lat": 31.9698
    },
    "International Paper-Prattville Mill": {
      "lng": -86.4718,
      "lat": 32.4187
    },
    "International Paper-Riverdale Mill": {
      "lng": -86.870912,
      "lat": 32.426016
    },
    "James H Miller Jr": {
      "lng": -87.0597,
      "lat": 33.6319
    },
    "Jones Bluff": {
      "lng": -86.784319,
      "lat": 32.324242
    },
    "Jordan Dam": {
      "lng": -86.2548,
      "lat": 32.6189
    },
    "Joseph M Farley": {
      "lng": -85.1116,
      "lat": 31.2231
    },
    "Kimberly Clark Mobile - CHP Plant": {
      "lng": -88.04977,
      "lat": 30.737741
    },
    "LaFayette Solar Farm": {
      "lng": -85.38821,
      "lat": 32.876313
    },
    "Lay Dam": {
      "lng": -86.5187,
      "lat": 32.9633
    },
    "Lewis Smith Dam": {
      "lng": -87.1077,
      "lat": 33.9406
    },
    "Logan Martin Dam": {
      "lng": -86.337547,
      "lat": 33.425878
    },
    "Martin Dam": {
      "lng": -85.911442,
      "lat": 32.680394
    },
    "McIntosh (7063)": {
      "lng": -88.0299,
      "lat": 31.2546
    },
    "McWilliams": {
      "lng": -86.476449,
      "lat": 31.400255
    },
    "Millers Ferry": {
      "lng": -87.399767,
      "lat": 32.100881
    },
    "Mitchell Dam": {
      "lng": -86.444892,
      "lat": 32.806025
    },
    "Morgan Energy Center": {
      "lng": -87.0639,
      "lat": 34.6397
    },
    "Muscle Shoals": {
      "lng": -87.90454,
      "lat": 34.76973
    },
    "Packaging Corporation of America Jackson Mill": {
      "lng": -87.8989,
      "lat": 31.4894
    },
    "Plant H. Allen Franklin": {
      "lng": -85.0975,
      "lat": 32.6078
    },
    "Point A": {
      "lng": -86.518307,
      "lat": 31.361146
    },
    "Redstone Arsenal Hybrid": {
      "lng": -86.611551,
      "lat": 34.610987
    },
    "River Bend Solar, LLC": {
      "lng": -109.565219,
      "lat": 45.703779
    },
    "SABIC Innovative Plastics - Burkville": {
      "lng": -86.52542,
      "lat": 32.310226
    },
    "Sand Valley Power Station": {
      "lng": -85.866103,
      "lat": 34.343875
    },
    "Sloss Industries Corp": {
      "lng": -86.79873,
      "lat": 33.568164
    },
    "Tenaska Lindsay Hill Generating Station": {
      "lng": -86.7386,
      "lat": 32.6514
    },
    "Theodore Cogeneration": {
      "lng": -88.128477,
      "lat": 30.525445
    },
    "Thurlow Dam": {
      "lng": -85.887614,
      "lat": 32.535436
    },
    "U S Alliance Coosa Pines": {
      "lng": -86.356825,
      "lat": 33.328107
    },
    "W&T Onshore Treating Facility (OTF)": {
      "lng": -88.18241,
      "lat": 30.43203
    },
    "Walter Bouldin Dam": {
      "lng": -86.283056,
      "lat": 32.583889
    },
    "Walton Discover Power Facility": {
      "lng": -85.038879,
      "lat": 32.54098
    },
    "Washington County Cogen (Olin)": {
      "lng": -88.0156,
      "lat": 31.2504
    },
    "Weiss Dam": {
      "lng": -85.753806,
      "lat": 34.172142
    },
    "Westervelt Moundville Cogen": {
      "lng": -87.655419,
      "lat": 32.960714
    },
    "WestRock Coated Board": {
      "lng": -85.0272,
      "lat": 32.1761
    },
    "WestRock Demopolis Mill": {
      "lng": -87.9759,
      "lat": 32.4541
    },
    "Wheeler Dam": {
      "lng": -87.3819,
      "lat": 34.8069
    },
    "Wilson Dam": {
      "lng": -87.62537,
      "lat": 34.79782
    },
    "Yates Dam": {
      "lng": -85.8901,
      "lat": 32.5743
    },
    "Yellowhammer Gas Plant": {
      "lng": -88.12833,
      "lat": 30.403538
    },
    "Arkansas Nuclear One": {
      "lng": -93.2351,
      "lat": 35.311
    },
    "Ashdown": {
      "lng": -94.1083,
      "lat": 33.6417
    },
    "Beaver Dam": {
      "lng": -76.843056,
      "lat": 41.611389
    },
    "Blakely Mountain": {
      "lng": -93.194425,
      "lat": 34.570984
    },
    "Branch Solar Farm": {
      "lng": -93.925758,
      "lat": 35.306183
    },
    "Bull Shoals": {
      "lng": -92.5763,
      "lat": 36.3635
    },
    "Carl Bailey": {
      "lng": -91.363426,
      "lat": 35.260102
    },
    "Carpenter": {
      "lng": -93.0262,
      "lat": 34.4425
    },
    "Chicot Solar": {
      "lng": -91.349,
      "lat": 33.479
    },
    "City of Paris Solar": {
      "lng": -93.734,
      "lat": 35.301
    },
    "City Water & Light - City of Jonesboro": {
      "lng": -90.725377,
      "lat": 35.848561
    },
    "Clearwater Paper APP CB": {
      "lng": -91.2371,
      "lat": 33.7061
    },
    "Dam 2": {
      "lng": -91.315788,
      "lat": 33.986501
    },
    "Dardanelle": {
      "lng": -93.1686,
      "lat": 35.2503
    },
    "Degray": {
      "lng": -93.110963,
      "lat": 34.211913
    },
    "Dell Power Plant": {
      "lng": -90.0253,
      "lat": 35.8619
    },
    "Elkins Generating Center": {
      "lng": -94.0014,
      "lat": 35.9714
    },
    "Ellis Hydro": {
      "lng": -94.294349,
      "lat": 35.351361
    },
    "FECC Solar Benton": {
      "lng": -92.578347,
      "lat": 34.638429
    },
    "Flint Creek Power Plant": {
      "lng": -94.5241,
      "lat": 36.2561
    },
    "Forrest City- Eldridge Road": {
      "lng": -90.80707,
      "lat": 35.03789
    },
    "Forrest City- Prison Site": {
      "lng": -90.80602,
      "lat": 34.97953
    },
    "Fourche Creek Wastewater": {
      "lng": -92.165,
      "lat": 34.6968
    },
    "Fulton": {
      "lng": -93.7924,
      "lat": 33.6094
    },
    "General Dynamics": {
      "lng": -92.541,
      "lat": 33.602
    },
    "Georgia-Pacific Crossett LLC": {
      "lng": -91.975,
      "lat": 33.1421
    },
    "Greers Ferry Lake": {
      "lng": -91.9953,
      "lat": 35.5207
    },
    "Harry D. Mattison Power Plant": {
      "lng": -94.2841,
      "lat": 36.1855
    },
    "Hot Spring Energy Facility": {
      "lng": -92.868344,
      "lat": 34.29759
    },
    "Hot Springs 2020": {
      "lng": -93.19485,
      "lat": 34.421
    },
    "Independence": {
      "lng": -76.4511,
      "lat": 43.495
    },
    "John W. Turk Jr. Power Plant": {
      "lng": -93.811944,
      "lat": 33.649722
    },
    "Lake Catherine": {
      "lng": -92.9049,
      "lat": 34.4341
    },
    "Lee Creek Water Treatment Facility": {
      "lng": -94.392858,
      "lat": 35.484488
    },
    "Magnet Cove Generating Station": {
      "lng": -92.8317,
      "lat": 34.4297
    },
    "McClellan": {
      "lng": -92.7917,
      "lat": 33.5648
    },
    "Municipal Light": {
      "lng": -90.1867,
      "lat": 36.3858
    },
    "Murray": {
      "lng": -92.3569,
      "lat": 34.7956
    },
    "Narrows (AR)": {
      "lng": -93.715168,
      "lat": 34.148427
    },
    "Noland Wastewater Treatment Plant Hybrid": {
      "lng": -94.067149,
      "lat": 36.082301
    },
    "Norfork": {
      "lng": -92.2401,
      "lat": 36.2491
    },
    "Osceola (AR)": {
      "lng": -89.9689,
      "lat": 35.705381
    },
    "Oswald Generating Station": {
      "lng": -92.2166,
      "lat": 34.5923
    },
    "Ozark": {
      "lng": -93.8175,
      "lat": 35.4693
    },
    "Ozarks Natural Energy Community Solar": {
      "lng": -94.082269,
      "lat": 36.164296
    },
    "Paragould Reciprocating": {
      "lng": -90.5094,
      "lat": 36.0242
    },
    "Paragould Turbine": {
      "lng": -90.509229,
      "lat": 36.024272
    },
    "Pine Bluff Energy Center": {
      "lng": -91.9025,
      "lat": 34.2181
    },
    "Pine Bluff Mill": {
      "lng": -91.907365,
      "lat": 34.221213
    },
    "Plum Point Energy Station": {
      "lng": -89.9489,
      "lat": 35.6644
    },
    "Remmel": {
      "lng": -92.8941,
      "lat": 34.4272
    },
    "Riceland Foods Cogeneration Plant": {
      "lng": -91.5331,
      "lat": 34.5136
    },
    "Scenic Hill Solar III": {
      "lng": -93.51517,
      "lat": 35.46715
    },
    "Scenic Hill Solar IV, LLC": {
      "lng": -93.4464,
      "lat": 35.47056
    },
    "SR Camden": {
      "lng": -92.703611,
      "lat": 33.631538
    },
    "Stuttgart Solar": {
      "lng": -91.44,
      "lat": 34.439722
    },
    "Thomas Fitzhugh": {
      "lng": -93.804926,
      "lat": 35.462382
    },
    "Two Pine Landfill Gas Recovery": {
      "lng": -92.157103,
      "lat": 34.8332
    },
    "UA Central Utility Plant": {
      "lng": -94.172254,
      "lat": 36.066336
    },
    "Union Power Station": {
      "lng": -92.589364,
      "lat": 33.296146
    },
    "Waste Management Eco Vista LFGTE": {
      "lng": -94.259713,
      "lat": 36.140849
    },
    "Westside Wastewater Treatment Plant Hybrid": {
      "lng": -94.140079,
      "lat": 36.04135
    },
    "Whillock": {
      "lng": -92.790273,
      "lat": 35.127591
    },
    "White Bluff": {
      "lng": -92.1406,
      "lat": 34.4228
    },
    "White River Lock and Dam 1": {
      "lng": -91.636887,
      "lat": 35.757401
    },
    "White River Lock and Dam 2": {
      "lng": -91.765418,
      "lat": 35.744036
    },
    "White River Lock and Dam 3": {
      "lng": -91.850521,
      "lat": 35.84361
    },
    "Actus Lend Lease DMAFB": {
      "lng": -110.863333,
      "lat": 32.1825
    },
    "AES ES GILBERT": {
      "lng": -111.956261,
      "lat": 33.446804
    },
    "Agua Caliente Solar Project": {
      "lng": -113.4945,
      "lat": 32.9774
    },
    "Agua Fria Generating Station": {
      "lng": -112.2153,
      "lat": 33.5561
    },
    "Amphitheater High School Solar": {
      "lng": -110.975,
      "lat": 32.269
    },
    "Apache Solar 1": {
      "lng": -109.890379,
      "lat": 32.068141
    },
    "Apache Station": {
      "lng": -109.8931,
      "lat": 32.0603
    },
    "APS Saguaro Power Plant": {
      "lng": -111.3,
      "lat": 32.5517
    },
    "APS West Phoenix Power Plant": {
      "lng": -112.1583,
      "lat": 33.4417
    },
    "Arizona State University CHP": {
      "lng": -111.928333,
      "lat": 33.417222
    },
    "Arizona Western College PV": {
      "lng": -114.49611,
      "lat": 32.687778
    },
    "Arlington Valley Energy Facility": {
      "lng": -112.8897,
      "lat": 33.3417
    },
    "Arlington Valley Solar Energy II": {
      "lng": -112.833888,
      "lat": 33.305
    },
    "Avalon Solar": {
      "lng": -110.960556,
      "lat": 32.001389
    },
    "Avalon Solar II": {
      "lng": -110.958427,
      "lat": 32.000854
    },
    "Avra Valley Solar": {
      "lng": -111.282777,
      "lat": 32.371667
    },
    "AZ State University - Tempe Campus Solar": {
      "lng": -111.92822,
      "lat": 33.424279
    },
    "Badger 1": {
      "lng": -112.813458,
      "lat": 33.498003
    },
    "Black Mountain Generating Station": {
      "lng": -114.1594,
      "lat": 35.0361
    },
    "Black Mountain Solar LLC": {
      "lng": -114.1627,
      "lat": 35.0227
    },
    "Bonnybrooke PV": {
      "lng": -111.34,
      "lat": 33.053
    },
    "Broadway 2 - Tucson Phase II": {
      "lng": -111.2611,
      "lat": 32.24817
    },
    "Broadway 3 - Tucson Phase I": {
      "lng": -111.26111,
      "lat": 32.24817
    },
    "Buckeye Union HS District 201": {
      "lng": -112.576111,
      "lat": 33.419167
    },
    "Canyon Del Oro High School Solar": {
      "lng": -110.974,
      "lat": 32.374
    },
    "Chino Solar Valley": {
      "lng": -112.42899,
      "lat": 34.71843
    },
    "Cholla": {
      "lng": -110.3033,
      "lat": 34.9394
    },
    "Cogeneration 1": {
      "lng": -110.952797,
      "lat": 32.229922
    },
    "Cogeneration 2": {
      "lng": -110.948333,
      "lat": 32.240833
    },
    "Cogenra - TEP": {
      "lng": -110.813333,
      "lat": 32.101944
    },
    "Coolidge Generating Station": {
      "lng": -111.503327,
      "lat": 32.917399
    },
    "Coronado Generating Station": {
      "lng": -109.2708,
      "lat": 34.5789
    },
    "Cotton Center Solar Hybrid": {
      "lng": -112.661667,
      "lat": 33.034722
    },
    "Crosscut": {
      "lng": -111.946052,
      "lat": 33.439375
    },
    "Davis Dam": {
      "lng": -114.570669,
      "lat": 35.197044
    },
    "Davis Monthan AFB (AZ) West Airfield": {
      "lng": -110.899734,
      "lat": 32.17356
    },
    "De Moss Petrie Generating Station": {
      "lng": -110.992148,
      "lat": 32.252273
    },
    "Desert Basin Generating Station": {
      "lng": -111.7889,
      "lat": 32.9042
    },
    "Desert Star Hybrid": {
      "lng": -112.66,
      "lat": 33.145
    },
    "Douglas": {
      "lng": -109.5538,
      "lat": 31.364122
    },
    "Dry Lake Wind LLC": {
      "lng": -110.2842,
      "lat": 34.6596
    },
    "East Line Solar": {
      "lng": -111.565072,
      "lat": 32.84703
    },
    "Estrella Mountain PV": {
      "lng": -112.340297,
      "lat": 33.409803
    },
    "Foothills Solar Plant Hybrid": {
      "lng": -114.428741,
      "lat": 32.64856
    },
    "Fort Huachuca Solar PV Project": {
      "lng": -110.35,
      "lat": 31.555278
    },
    "FRB Solar Farm": {
      "lng": -110.824662,
      "lat": 32.108147
    },
    "Gato Montes Solar, LLC": {
      "lng": -110.819722,
      "lat": 32.102778
    },
    "Gila Bend Hybrid": {
      "lng": -112.881389,
      "lat": 32.940833
    },
    "Gila River Power Block 2": {
      "lng": -112.694444,
      "lat": 32.975
    },
    "Gila River Power Station": {
      "lng": -112.6944,
      "lat": 32.975
    },
    "Gilbert Solar Facility I, LLC": {
      "lng": -111.802355,
      "lat": 33.355862
    },
    "Glen Canyon Dam": {
      "lng": -111.483872,
      "lat": 36.936614
    },
    "Gray Hawk Solar": {
      "lng": -113.8978,
      "lat": 35.3635
    },
    "Griffith Energy Project": {
      "lng": -114.133272,
      "lat": 35.054029
    },
    "Headgate Rock": {
      "lng": -114.277356,
      "lat": 34.168264
    },
    "Hoover Dam (AZ)": {
      "lng": -114.738006,
      "lat": 36.01551
    },
    "Horse Mesa": {
      "lng": -111.344,
      "lat": 33.5907
    },
    "Hyder II Hybrid": {
      "lng": -113.3489,
      "lat": 33.027
    },
    "Hyder Solar Hybrid": {
      "lng": -113.34397,
      "lat": 33.02566
    },
    "Intel - Ocotillo Campus Solar": {
      "lng": -111.892354,
      "lat": 33.243261
    },
    "IRC Generator Facility": {
      "lng": -112.5264,
      "lat": 36.9708
    },
    "Iron Horse Battery Storage Hybrid": {
      "lng": -110.819119,
      "lat": 32.098492
    },
    "Irvington Generating Station": {
      "lng": -110.9047,
      "lat": 32.16
    },
    "Jacobson 5 MW Solar": {
      "lng": -114.01183,
      "lat": 35.304528
    },
    "Jewish Community Center PV": {
      "lng": -111.924722,
      "lat": 33.603056
    },
    "Kayenta Solar Project": {
      "lng": -110.263889,
      "lat": 36.771944
    },
    "Kingman 1": {
      "lng": -114.0675,
      "lat": 35.145833
    },
    "Kyrene Generating Station": {
      "lng": -111.9353,
      "lat": 33.3556
    },
    "La Senita": {
      "lng": -114.004591,
      "lat": 35.241675
    },
    "Lake Pleasant WTP": {
      "lng": -112.24,
      "lat": 33.820556
    },
    "Luke Solar": {
      "lng": -112.37944,
      "lat": 33.526111
    },
    "Macys Goodyear": {
      "lng": -112.414596,
      "lat": 33.409063
    },
    "Mesa Carport PV": {
      "lng": -111.602,
      "lat": 33.347
    },
    "Mesquite Generating Station": {
      "lng": -112.864167,
      "lat": 33.345
    },
    "Mesquite Solar 1": {
      "lng": -112.904028,
      "lat": 33.341833
    },
    "Mesquite Solar 2, LLC": {
      "lng": -112.911388,
      "lat": 33.332777
    },
    "Mesquite Solar 3, LLC": {
      "lng": -112.936805,
      "lat": 33.347583
    },
    "Mohave County Wind Farm": {
      "lng": -114.504504,
      "lat": 35.791142
    },
    "Mohave Electric at Fort Mohave": {
      "lng": -114.5565,
      "lat": 34.9843
    },
    "Mohave Electric Cooperative at Joy Lane": {
      "lng": -114.5565,
      "lat": 34.9843
    },
    "Mormon Flat": {
      "lng": -111.4431,
      "lat": 33.5536
    },
    "New Harquahala Generating Company": {
      "lng": -113.113434,
      "lat": 33.47596
    },
    "North Loop": {
      "lng": -111.1262,
      "lat": 32.401
    },
    "Northwest Regional": {
      "lng": -112.476666,
      "lat": 33.681944
    },
    "Novo BioPower Plant": {
      "lng": -110.334953,
      "lat": 34.503158
    },
    "Ocotillo Power Plant": {
      "lng": -111.9122,
      "lat": 33.4225
    },
    "OE_AZ1": {
      "lng": -113.656111,
      "lat": 33.776667
    },
    "Palo Verde": {
      "lng": -112.8617,
      "lat": 33.3881
    },
    "Paloma Solar Hybrid": {
      "lng": -112.661389,
      "lat": 33.021111
    },
    "Paradise Valley H.S. PV": {
      "lng": -111.9997,
      "lat": 33.6416
    },
    "Perrin Ranch Wind LLC": {
      "lng": -112.271111,
      "lat": 35.415556
    },
    "Phoenix Airport East Economy Lot": {
      "lng": -111.9842,
      "lat": 33.4345
    },
    "Phoenix Airport Rental Car Center": {
      "lng": -112.04484,
      "lat": 33.43042
    },
    "Picture Rocks Solar, LLC": {
      "lng": -111.242778,
      "lat": 32.369722
    },
    "Pima Community College": {
      "lng": -110.816669,
      "lat": 32.166494
    },
    "Pima Community College - East Campus": {
      "lng": -110.990639,
      "lat": 32.146142
    },
    "Pima Community College NW": {
      "lng": -111.02786,
      "lat": 32.343491
    },
    "Pima Energy Storage System": {
      "lng": -110.991333,
      "lat": 32.251647
    },
    "Pinal Central Energy Center Hybrid": {
      "lng": -111.551111,
      "lat": 32.875697
    },
    "Poseidon Solar, LLC": {
      "lng": -111.484011,
      "lat": 33.160601
    },
    "Poseidon Wind, LLC": {
      "lng": -110.1734,
      "lat": 34.6062
    },
    "Prairie Fire": {
      "lng": -110.82,
      "lat": 32.116389
    },
    "Prescott Airport": {
      "lng": -112.4008,
      "lat": 34.6584
    },
    "Prescott Solar Plant": {
      "lng": -112.4275,
      "lat": 34.649444
    },
    "Punkin Center Battery Storage": {
      "lng": -111.31356,
      "lat": 33.87581
    },
    "Queen Creek Solar Farm": {
      "lng": -111.612222,
      "lat": 33.2675
    },
    "RE Ajo 1 LLC": {
      "lng": -112.831401,
      "lat": 32.367013
    },
    "RE Bagdad Solar I LLC": {
      "lng": -113.177222,
      "lat": 34.585833
    },
    "RE Gillespie 1 LLC": {
      "lng": -112.665833,
      "lat": 33.026389
    },
    "Red Horse 2": {
      "lng": -110.088056,
      "lat": 32.286111
    },
    "Red Horse III": {
      "lng": -110.05129,
      "lat": 32.13597
    },
    "Red Rock": {
      "lng": -111.289232,
      "lat": 32.551026
    },
    "Redhawk Generating Facility": {
      "lng": -112.840641,
      "lat": 33.334562
    },
    "Rio Rico Solar": {
      "lng": -111.008056,
      "lat": 31.5
    },
    "Roger Road WWTP": {
      "lng": -111.0331,
      "lat": 32.2894
    },
    "Roosevelt": {
      "lng": -111.161786,
      "lat": 33.671104
    },
    "Saddle Mountain Solar I": {
      "lng": -113.180833,
      "lat": 33.378333
    },
    "Saint Solar": {
      "lng": -111.502402,
      "lat": 32.866276
    },
    "Sandstone Solar": {
      "lng": -111.430278,
      "lat": 33.035556
    },
    "Santan": {
      "lng": -111.7503,
      "lat": 33.3325
    },
    "Solana Generating Station": {
      "lng": -112.95594,
      "lat": 32.922294
    },
    "South Consolidated": {
      "lng": -111.781064,
      "lat": 33.466477
    },
    "South Point Energy Center, LLC": {
      "lng": -114.5317,
      "lat": 34.8678
    },
    "Springerville Generating Station": {
      "lng": -109.1639,
      "lat": 34.3186
    },
    "Stewart Mountain": {
      "lng": -111.536,
      "lat": 33.5661
    },
    "Sulphur Springs": {
      "lng": -109.919283,
      "lat": 32.0628
    },
    "Sun Streams 2": {
      "lng": -112.816771,
      "lat": 33.320406
    },
    "Sun Streams, LLC": {
      "lng": -112.8348,
      "lat": 33.34884
    },
    "Sundance Power Plant": {
      "lng": -111.5899,
      "lat": 32.9285
    },
    "T0588 Phoenix - AZ": {
      "lng": -112.217664,
      "lat": 33.447225
    },
    "Tech Park Solar": {
      "lng": -110.815696,
      "lat": 32.097884
    },
    "UASTP I": {
      "lng": -110.8106,
      "lat": 32.096973
    },
    "Union HS at Casa Grande": {
      "lng": -111.738611,
      "lat": 32.936944
    },
    "University of Arizona - Biosphere 2": {
      "lng": -110.852608,
      "lat": 32.578011
    },
    "Valencia": {
      "lng": -110.931343,
      "lat": 31.363472
    },
    "Valencia Solar": {
      "lng": -110.866944,
      "lat": 32.145833
    },
    "Vista Grande HS at Casa Grande": {
      "lng": -111.713056,
      "lat": 32.891389
    },
    "Waddell": {
      "lng": -112.2714,
      "lat": 33.8447
    },
    "Walmart Casa Grande": {
      "lng": -111.771751,
      "lat": 32.866837
    },
    "Western Renewable Energy": {
      "lng": -109.289979,
      "lat": 34.093006
    },
    "Wilmot Energy Center LLC": {
      "lng": -110.892676,
      "lat": 32.043502
    },
    "Yucca Power Plant": {
      "lng": -114.7106,
      "lat": 32.7214
    },
    "Yuma Cogeneration Associates": {
      "lng": -114.654053,
      "lat": 32.728822
    },
    "1420 Coil Av #C": {
      "lng": -118.241405,
      "lat": 33.794283
    },
    "20333 Normandie PV, LLC": {
      "lng": -118.30019,
      "lat": 33.84614
    },
    "2081 Terzian Solar Project": {
      "lng": -119.415833,
      "lat": 36.67
    },
    "2097 Helton Solar Project": {
      "lng": -120.506944,
      "lat": 37.352222
    },
    "2127 Harris Solar Project": {
      "lng": -121.669722,
      "lat": 39.291667
    },
    "2555 E Olympic Bl": {
      "lng": -118.224167,
      "lat": 34.026944
    },
    "85 A": {
      "lng": -118.3372,
      "lat": 35.1011
    },
    "85 B": {
      "lng": -118.3372,
      "lat": 35.1011
    },
    "A G Wishon": {
      "lng": -119.504724,
      "lat": 37.15105
    },
    "ABEC #2 dba West-Star Dairy": {
      "lng": -119.432866,
      "lat": 35.472113
    },
    "ABEC #3 dba Lakeview Dairy": {
      "lng": -119.202974,
      "lat": 35.21426
    },
    "ABEC #4 dba CE&S Dairy": {
      "lng": -119.116384,
      "lat": 35.17306
    },
    "ABEC Bidart-Old River LLC": {
      "lng": -119.105556,
      "lat": 35.169722
    },
    "Acorn I Energy Storage LLC": {
      "lng": -118.931898,
      "lat": 34.196505
    },
    "Adapture CA2, LLC": {
      "lng": -122.175583,
      "lat": 39.69898
    },
    "Adelanto Solar II, LLC": {
      "lng": -117.3775,
      "lat": 34.557222
    },
    "Adelanto Solar Project": {
      "lng": -117.435277,
      "lat": 34.550833
    },
    "Adelanto Solar, LLC": {
      "lng": -117.3775,
      "lat": 34.557222
    },
    "Adera Solar": {
      "lng": -120.32488,
      "lat": 37.120586
    },
    "Adobe San Jose": {
      "lng": -121.893056,
      "lat": 37.330556
    },
    "Adobe Solar": {
      "lng": -118.954444,
      "lat": 35.102778
    },
    "AEC- Santa Rosa Solar": {
      "lng": -116.547706,
      "lat": 33.5293
    },
    "Aera San Ardo Cogen Facility": {
      "lng": -120.859952,
      "lat": 35.953293
    },
    "Aerojet I": {
      "lng": -121.162879,
      "lat": 38.613508
    },
    "Aerojet II": {
      "lng": -121.1703,
      "lat": 38.6086
    },
    "Aerolease": {
      "lng": -118.451355,
      "lat": 34.189857
    },
    "AES Alamitos": {
      "lng": -118.1009,
      "lat": 33.7688
    },
    "AES Alamitos Energy Center": {
      "lng": -118.100194,
      "lat": 33.767229
    },
    "AES ES ALAMITOS, LLC": {
      "lng": -118.099665,
      "lat": 33.770202
    },
    "AES Huntington Beach": {
      "lng": -117.9792,
      "lat": 33.6439
    },
    "AES Huntington Beach Energy Project": {
      "lng": -117.979372,
      "lat": 33.645618
    },
    "AES Redondo Beach": {
      "lng": -118.395,
      "lat": 33.8504
    },
    "Agnews Power Plant": {
      "lng": -121.9272,
      "lat": 37.4058
    },
    "AGT000 Stevens Creek Fuel Cell": {
      "lng": -121.99894,
      "lat": 37.32489
    },
    "Agua Mansa Power": {
      "lng": -117.3603,
      "lat": 34.0414
    },
    "Aidlin Geothermal Power Plant": {
      "lng": -122.881,
      "lat": 38.8339
    },
    "Alameda": {
      "lng": -122.2889,
      "lat": 37.7886
    },
    "Alamo": {
      "lng": -118.6867,
      "lat": 34.8158
    },
    "Alamo Solar": {
      "lng": -117.3475,
      "lat": 34.682222
    },
    "Algonquin Power Sanger": {
      "lng": -119.5522,
      "lat": 36.6842
    },
    "Algonquin SKIC 10 Solar, LLC": {
      "lng": -119.243361,
      "lat": 35.126111
    },
    "Algonquin SKIC20 Solar LLC": {
      "lng": -119.243333,
      "lat": 35.126111
    },
    "Allergan": {
      "lng": -117.85381,
      "lat": 33.67239
    },
    "Almond Power Plant": {
      "lng": -120.985,
      "lat": 37.5744
    },
    "Alpaugh 50": {
      "lng": -119.435278,
      "lat": 35.898333
    },
    "Alpaugh North": {
      "lng": -119.442778,
      "lat": 35.9025
    },
    "Alpine Solar": {
      "lng": -118.5047,
      "lat": 34.7892
    },
    "Alta Mesa Repower LLC": {
      "lng": -116.661,
      "lat": 33.942
    },
    "Alta Powerhouse": {
      "lng": -120.80355,
      "lat": 39.215979
    },
    "Alta Wind Energy Center I": {
      "lng": -118.353422,
      "lat": 35.033725
    },
    "Alta Wind Energy Center II": {
      "lng": -118.295278,
      "lat": 35.023889
    },
    "Alta Wind Energy Center III": {
      "lng": -118.295278,
      "lat": 35.023889
    },
    "Alta Wind Energy Center IV": {
      "lng": -118.242222,
      "lat": 35.008611
    },
    "Alta Wind Energy Center V": {
      "lng": -118.242222,
      "lat": 35.008611
    },
    "Alta Wind VIII": {
      "lng": -118.234811,
      "lat": 35.026683
    },
    "Alta Wind X": {
      "lng": -118.2,
      "lat": 35.06
    },
    "Alta Wind XI": {
      "lng": -118.39,
      "lat": 35.065
    },
    "AltaGas Ripon Energy Inc.": {
      "lng": -121.116006,
      "lat": 37.732533
    },
    "Altamont Gas Recovery": {
      "lng": -121.649866,
      "lat": 37.749308
    },
    "Alticor Inc - Solar Project": {
      "lng": -117.99608,
      "lat": 33.878585
    },
    "Amazon - Patterson PV": {
      "lng": -121.166326,
      "lat": 37.468888
    },
    "Amazon PSP1": {
      "lng": -116.99442,
      "lat": 33.92624
    },
    "Amazon San Bernardino": {
      "lng": -117.245839,
      "lat": 34.088408
    },
    "Amedee Geothermal Venture I": {
      "lng": -120.105,
      "lat": 40.300833
    },
    "Ameresco Butte County": {
      "lng": -121.73,
      "lat": 39.674
    },
    "Ameresco Chiquita Canyon": {
      "lng": -118.642855,
      "lat": 34.431664
    },
    "Ameresco Forward": {
      "lng": -121.185104,
      "lat": 37.883406
    },
    "Ameresco Johnson Canyon": {
      "lng": -121.408611,
      "lat": 36.534167
    },
    "Ameresco Keller Canyon": {
      "lng": -121.934491,
      "lat": 38.003928
    },
    "Ameresco Ox Mountain": {
      "lng": -122.4,
      "lat": 37.5
    },
    "Ameresco San Joaquin": {
      "lng": -120.929722,
      "lat": 38.029722
    },
    "Ameresco Santa Cruz Energy": {
      "lng": -121.8156,
      "lat": 36.9178
    },
    "Ameresco Vasco Road": {
      "lng": -121.728889,
      "lat": 37.754444
    },
    "American Canyon Solar": {
      "lng": -122.214014,
      "lat": 38.160858
    },
    "American Kings Solar, LLC": {
      "lng": -119.898,
      "lat": 36.24
    },
    "Ampersand Chowchilla Biomass LLC": {
      "lng": -120.248504,
      "lat": 37.106668
    },
    "Anaheim Solar Energy Plant": {
      "lng": -117.919722,
      "lat": 33.8025
    },
    "Angels": {
      "lng": -120.539444,
      "lat": 38.071944
    },
    "Anheuser-Busch #2": {
      "lng": -122.0925,
      "lat": 38.233611
    },
    "Antelope Big Sky Ranch": {
      "lng": -118.28769,
      "lat": 34.694839
    },
    "Antelope DSR 1": {
      "lng": -118.280722,
      "lat": 34.655556
    },
    "Antelope DSR 2": {
      "lng": -118.311722,
      "lat": 34.686944
    },
    "Antelope DSR 3": {
      "lng": -118.304071,
      "lat": 34.736859
    },
    "Antelope Expansion 2": {
      "lng": -118.309153,
      "lat": 34.744977
    },
    "Antelope Expansion 3A": {
      "lng": -118.322051,
      "lat": 34.728902
    },
    "Antelope Expansion 3B": {
      "lng": -118.322532,
      "lat": 34.720162
    },
    "Antioch Central Utility Plant": {
      "lng": -121.775429,
      "lat": 37.949488
    },
    "AP North Lake I, LP": {
      "lng": -117.010556,
      "lat": 33.74
    },
    "APP Prune A-D": {
      "lng": -122.006686,
      "lat": 37.331666
    },
    "Apple Campus 2 Fuel Cell": {
      "lng": -122.01,
      "lat": 37.33
    },
    "Apple Campus 2 PV": {
      "lng": -122.01,
      "lat": 37.33
    },
    "Aquamarine": {
      "lng": -119.904166,
      "lat": 36.189722
    },
    "Argus Cogen Plant": {
      "lng": -117.3833,
      "lat": 35.765
    },
    "Arrache 4006": {
      "lng": -118.051111,
      "lat": 34.655
    },
    "Arrache 4013": {
      "lng": -118.051944,
      "lat": 34.650556
    },
    "Arrache 8083": {
      "lng": -118.061111,
      "lat": 34.647778
    },
    "Aspiration G": {
      "lng": -120.352481,
      "lat": 36.552186
    },
    "ASTI": {
      "lng": -122.975278,
      "lat": 38.761389
    },
    "AT&T - Gardena": {
      "lng": -118.289798,
      "lat": 33.874969
    },
    "AT&T - Hawthorne": {
      "lng": -118.320597,
      "lat": 33.92429
    },
    "AT&T - Hayward": {
      "lng": -122.09685,
      "lat": 37.659879
    },
    "AT&T - Redwood City": {
      "lng": -122.200408,
      "lat": 37.480769
    },
    "AT&T - San Diego Trade Street": {
      "lng": -117.16682,
      "lat": 32.88758
    },
    "AT&T Anaheim": {
      "lng": -117.916893,
      "lat": 33.836439
    },
    "AT&T Holger": {
      "lng": -121.94453,
      "lat": 37.417638
    },
    "AT&T San Diego": {
      "lng": -117.1986,
      "lat": 32.904775
    },
    "Atascadero State Hospital": {
      "lng": -120.634011,
      "lat": 35.463333
    },
    "ATT  Kelvin": {
      "lng": -117.836821,
      "lat": 33.684675
    },
    "ATT North Watney": {
      "lng": -122.077759,
      "lat": 38.236257
    },
    "ATT Van Nyus": {
      "lng": -118.448225,
      "lat": 34.196324
    },
    "ATT Ventura Blvd": {
      "lng": -118.456078,
      "lat": 34.152191
    },
    "Atwell Island": {
      "lng": -119.510833,
      "lat": 35.895
    },
    "Atwell Island West Solar": {
      "lng": -119.459722,
      "lat": 35.815
    },
    "AV Solar Ranch One": {
      "lng": -118.42444,
      "lat": 34.780278
    },
    "Avenal Park": {
      "lng": -120.11,
      "lat": 35.989722
    },
    "AVS Lancaster 1": {
      "lng": -117.973571,
      "lat": 34.748894
    },
    "Axium Modesto Solar": {
      "lng": -120.995556,
      "lat": 37.734167
    },
    "Azusa": {
      "lng": -117.9093,
      "lat": 34.1548
    },
    "B Braun Medical": {
      "lng": -117.834793,
      "lat": 33.688721
    },
    "Badger Creek Limited": {
      "lng": -119.029856,
      "lat": 35.483736
    },
    "Baker": {
      "lng": -117.68412,
      "lat": 33.656528
    },
    "Baker Creek Hydroelectric": {
      "lng": -123.725556,
      "lat": 40.526389
    },
    "Bakersfield 111": {
      "lng": -118.989894,
      "lat": 35.317551
    },
    "Bakersfield College Solar 01": {
      "lng": -118.96864,
      "lat": 35.411817
    },
    "Bakersfield Industrial PV 1": {
      "lng": -118.993119,
      "lat": 35.330032
    },
    "Bakersfield PV 1": {
      "lng": -118.981522,
      "lat": 35.324982
    },
    "Balch 1": {
      "lng": -119.087712,
      "lat": 36.909137
    },
    "Balch 2": {
      "lng": -119.087556,
      "lat": 36.908907
    },
    "Barre Generating Station": {
      "lng": -117.9832,
      "lat": 33.8073
    },
    "Bayshore Solar A, LLC": {
      "lng": -118.298089,
      "lat": 34.667817
    },
    "Bayshore Solar B, LLC": {
      "lng": -118.302649,
      "lat": 34.678305
    },
    "Bayshore Solar C, LLC": {
      "lng": -118.314087,
      "lat": 34.681338
    },
    "BBARWA": {
      "lng": -116.815428,
      "lat": 34.267795
    },
    "Beacon BESS 1": {
      "lng": -118.027519,
      "lat": 35.256526
    },
    "Beacon Solar Plant Site 2": {
      "lng": -117.999167,
      "lat": 35.265833
    },
    "Beacon Solar Plant Site 5": {
      "lng": -118.034444,
      "lat": 35.248889
    },
    "Bear Creek": {
      "lng": -121.959109,
      "lat": 40.534454
    },
    "Bear Creek Solar": {
      "lng": -79.449053,
      "lat": 35.540344
    },
    "Bear Mountain Limited": {
      "lng": -118.926643,
      "lat": 35.419181
    },
    "Bear Valley": {
      "lng": -117.016566,
      "lat": 33.166246
    },
    "Bear Valley Power Plant": {
      "lng": -116.9025,
      "lat": 34.2506
    },
    "Beardsley": {
      "lng": -120.076983,
      "lat": 38.202569
    },
    "Beckton Dickenson - San Jose": {
      "lng": -121.88594,
      "lat": 37.39789
    },
    "Belden": {
      "lng": -121.249438,
      "lat": 40.007445
    },
    "Beringer": {
      "lng": -122.479722,
      "lat": 38.510833
    },
    "Berry Cogen": {
      "lng": -119.443617,
      "lat": 35.093046
    },
    "Berry Cogen Tanne Hills 18": {
      "lng": -119.440881,
      "lat": 35.089829
    },
    "Berry NMW Cogens": {
      "lng": -119.611551,
      "lat": 35.307555
    },
    "Berry Placerita Cogen": {
      "lng": -118.494882,
      "lat": 34.38407
    },
    "Bidwell Ditch Project": {
      "lng": -121.4481,
      "lat": 40.8014
    },
    "Big Bear": {
      "lng": -118.192851,
      "lat": 34.015287
    },
    "Big Creek 1": {
      "lng": -119.2396,
      "lat": 37.2042
    },
    "Big Creek 2": {
      "lng": -119.3069,
      "lat": 37.2
    },
    "Big Creek 2A": {
      "lng": -119.30681,
      "lat": 37.200062
    },
    "Big Creek 3": {
      "lng": -119.386623,
      "lat": 37.148517
    },
    "Big Creek 4": {
      "lng": -119.4898,
      "lat": 37.139
    },
    "Big Creek 8": {
      "lng": -119.3289,
      "lat": 37.2096
    },
    "Big Creek Water Works": {
      "lng": -123.498008,
      "lat": 40.646733
    },
    "Big Pine Creek": {
      "lng": -118.324075,
      "lat": 37.142795
    },
    "Big Valley Power LLC": {
      "lng": -121.137222,
      "lat": 41.1325
    },
    "Biola University Hybrid": {
      "lng": -118.013105,
      "lat": 33.906953
    },
    "Bio-Rad": {
      "lng": -122.273663,
      "lat": 38.022486
    },
    "Bishop Creek 2": {
      "lng": -118.573339,
      "lat": 37.273581
    },
    "Bishop Creek 3": {
      "lng": -118.531957,
      "lat": 37.301962
    },
    "Bishop Creek 4": {
      "lng": -118.501714,
      "lat": 37.323035
    },
    "Bishop Creek 5": {
      "lng": -118.4797,
      "lat": 37.3344
    },
    "Bishop Creek 6": {
      "lng": -118.4628,
      "lat": 37.3499
    },
    "Black Butte": {
      "lng": -122.332335,
      "lat": 39.814905
    },
    "Blacksand Generating Facility": {
      "lng": -117.855,
      "lat": 33.9328
    },
    "Blackwell Solar Park": {
      "lng": -119.845,
      "lat": 35.612
    },
    "Blue Lake Power LLC": {
      "lng": -123.9942,
      "lat": 40.8786
    },
    "Blue Shld Of Cal- El Dorado Hlls Mtr B": {
      "lng": -121.074115,
      "lat": 38.650627
    },
    "Blythe Energy": {
      "lng": -114.6865,
      "lat": 33.6157
    },
    "Blythe Solar 110, LLC": {
      "lng": -114.742,
      "lat": 33.68
    },
    "Blythe Solar II, LLC": {
      "lng": -114.73,
      "lat": 33.68
    },
    "Blythe Solar III, LLC Hybrid": {
      "lng": -114.742,
      "lat": 33.656
    },
    "Blythe Solar IV, LLC": {
      "lng": -114.768,
      "lat": 33.682
    },
    "Bodega Avenue Solar LLC": {
      "lng": -122.7373,
      "lat": 38.26147
    },
    "Bolthouse S&P and Rowen Farms Solar": {
      "lng": -117.6933,
      "lat": 34.5644
    },
    "Borden Solar Farm": {
      "lng": -119.96,
      "lat": 36.941111
    },
    "Borrego Springs Energy Storage": {
      "lng": -116.3496,
      "lat": 33.27011
    },
    "Bottle Rock Power": {
      "lng": -122.7677,
      "lat": 38.8348
    },
    "Bowerman Power LFG, LLC": {
      "lng": -117.702222,
      "lat": 33.718889
    },
    "Bowman": {
      "lng": -120.653417,
      "lat": 39.448352
    },
    "Box Canyon Dam": {
      "lng": -122.3292,
      "lat": 41.2792
    },
    "BP Carson Refinery": {
      "lng": -118.2358,
      "lat": 33.8153
    },
    "Brea Expansion Plant": {
      "lng": -117.840233,
      "lat": 33.932022
    },
    "Broadridge Cogen": {
      "lng": -121.0564,
      "lat": 38.6183
    },
    "Broadway 2 - UC Riverside": {
      "lng": -117.33101,
      "lat": 33.97034
    },
    "Broadway 3 - UC Merced 1": {
      "lng": -120.43108,
      "lat": 37.36448
    },
    "Broadway 3 - UCSB Lot 38": {
      "lng": -119.85637,
      "lat": 34.41974
    },
    "Broadway 4 - Target Shafter": {
      "lng": -119.18912,
      "lat": 35.46331
    },
    "Brookfield Tehachapi 1": {
      "lng": -118.241389,
      "lat": 35.008333
    },
    "Bucks Creek": {
      "lng": -121.327692,
      "lat": 39.91062
    },
    "Buena Vista Energy LLC": {
      "lng": -121.6672,
      "lat": 37.805
    },
    "Buidling L": {
      "lng": -118.041944,
      "lat": 33.912917
    },
    "Building F": {
      "lng": -118.050189,
      "lat": 33.914356
    },
    "Building G": {
      "lng": -118.050561,
      "lat": 33.912917
    },
    "Burney Creek": {
      "lng": -121.7225,
      "lat": 40.8579
    },
    "Burney Forest Products": {
      "lng": -121.720342,
      "lat": 40.879719
    },
    "Butt Valley": {
      "lng": -121.190663,
      "lat": 40.175656
    },
    "Butte College Main Campus Solar": {
      "lng": -121.644167,
      "lat": 39.648056
    },
    "C P Kelco San Diego Plant": {
      "lng": -117.1436,
      "lat": 32.6947
    },
    "C&H Sugar Plant": {
      "lng": -122.218889,
      "lat": 38.057222
    },
    "CA Dept of Public Health at Richmond": {
      "lng": -122.344,
      "lat": 37.92
    },
    "CA Flats Solar 130, LLC": {
      "lng": -120.404352,
      "lat": 35.615573
    },
    "CA Flats Solar 150, LLC": {
      "lng": -120.404352,
      "lat": 35.615573
    },
    "CA Institute for Women": {
      "lng": -117.636427,
      "lat": 33.949722
    },
    "Cabazon Wind Partners": {
      "lng": -116.735,
      "lat": 33.9172
    },
    "Cabrillo Power I Encina Power Station": {
      "lng": -117.333916,
      "lat": 33.140507
    },
    "Cal Flats BESS": {
      "lng": -120.329439,
      "lat": 35.866665
    },
    "Cal State Univ San Bernardino FC01": {
      "lng": -117.324167,
      "lat": 34.185833
    },
    "Calabasas Gas to Energy Facility": {
      "lng": -118.7236,
      "lat": 34.1511
    },
    "CalCity Solar 1": {
      "lng": -117.965748,
      "lat": 35.141374
    },
    "California City": {
      "lng": -117.981,
      "lat": 35.157
    },
    "California Institute of Technology": {
      "lng": -118.1256,
      "lat": 34.138467
    },
    "California PV Energy at ISD WWTP": {
      "lng": -121.703889,
      "lat": 38.001111
    },
    "California State Univ at Channel Islands": {
      "lng": -119.043375,
      "lat": 34.162058
    },
    "California Valley Solar Ranch": {
      "lng": -119.91663,
      "lat": 35.323739
    },
    "Calipatria Solar Farm": {
      "lng": -115.532222,
      "lat": 33.165833
    },
    "Calipatria State Prison": {
      "lng": -115.484805,
      "lat": 33.163055
    },
    "Calistoga Power Plant": {
      "lng": -122.7434,
      "lat": 38.7879
    },
    "CalPeak Power - Border": {
      "lng": -116.943869,
      "lat": 32.562448
    },
    "CalPeak Power - Enterprise": {
      "lng": -117.117441,
      "lat": 33.121943
    },
    "CalPeak Power - Panoche": {
      "lng": -120.5783,
      "lat": 36.6535
    },
    "CalPeak Power - Vaca Dixon": {
      "lng": -121.923973,
      "lat": 38.398867
    },
    "Calpine Gilroy Cogen, LP": {
      "lng": -121.5367,
      "lat": 37.0001
    },
    "Calpine Sutter Energy Center": {
      "lng": -121.695873,
      "lat": 39.053139
    },
    "CalRenew-1": {
      "lng": -120.3658,
      "lat": 36.7575
    },
    "CalTech - Pasadena": {
      "lng": -118.12833,
      "lat": 34.138719
    },
    "CalTech - Pasadena (PPA)": {
      "lng": -118.121488,
      "lat": 34.139368
    },
    "CalTech - Pasadena Wilson Ave": {
      "lng": -118.12841,
      "lat": 34.13818
    },
    "Caltech Central": {
      "lng": -118.1267,
      "lat": 34.1358
    },
    "Camanche": {
      "lng": -121.0245,
      "lat": 38.2268
    },
    "Cameron Ridge LLC": {
      "lng": -118.3158,
      "lat": 35.075
    },
    "Camino": {
      "lng": -120.537089,
      "lat": 38.82881
    },
    "Camp Far West": {
      "lng": -121.316966,
      "lat": 39.048802
    },
    "Campbell Power Plant": {
      "lng": -121.4735,
      "lat": 38.511
    },
    "Campo Verde Solar": {
      "lng": -115.726389,
      "lat": 32.753333
    },
    "Canal Energy S23, LLC": {
      "lng": -119.5604,
      "lat": 36.1761
    },
    "Cantua Solar Station": {
      "lng": -120.343543,
      "lat": 36.425085
    },
    "Canyon Crest Academy": {
      "lng": -117.191622,
      "lat": 32.95907
    },
    "Canyon Power Plant": {
      "lng": -117.861667,
      "lat": 33.858056
    },
    "Caribou 1": {
      "lng": -121.148241,
      "lat": 40.085476
    },
    "Caribou 2": {
      "lng": -121.149373,
      "lat": 40.085997
    },
    "Carson Cogeneration Company": {
      "lng": -118.2491,
      "lat": 33.8759
    },
    "Carson Power Plant": {
      "lng": -121.462387,
      "lat": 38.445681
    },
    "Cascade Solar": {
      "lng": -116.234444,
      "lat": 34.157222
    },
    "Castaic": {
      "lng": -118.656638,
      "lat": 34.587237
    },
    "Castaic Lake Phase II": {
      "lng": -118.510837,
      "lat": 34.433332
    },
    "Castle Rock Vineyards": {
      "lng": -119.110556,
      "lat": 35.801111
    },
    "Castor Solar": {
      "lng": -119.387258,
      "lat": 35.211044
    },
    "Catalina Express": {
      "lng": -118.278469,
      "lat": 33.749947
    },
    "Catalina Solar 2, LLC": {
      "lng": -118.352778,
      "lat": 34.812222
    },
    "Catalina Solar LLC": {
      "lng": -118.334492,
      "lat": 34.938261
    },
    "CBS Studio Center": {
      "lng": -118.390833,
      "lat": 34.145
    },
    "CBS Television City": {
      "lng": -118.36,
      "lat": 34.075
    },
    "CCCSD Wastewater Treatment Plnt": {
      "lng": -122.0697,
      "lat": 37.9978
    },
    "CDCR (CA) - Corcoran State Prison": {
      "lng": -119.55756,
      "lat": 36.06502
    },
    "CDCR (CA) - Pleasant Valley State Prison": {
      "lng": -120.224445,
      "lat": 36.137219
    },
    "CDCR (CA) - Solano State Prison": {
      "lng": -121.97062,
      "lat": 38.32272
    },
    "CDCR (CA) - Wasco State Prison": {
      "lng": -119.40366,
      "lat": 35.60053
    },
    "CE Leathers": {
      "lng": -115.565,
      "lat": 33.1779
    },
    "CE Turbo LLC": {
      "lng": -115.5114,
      "lat": 33.1669
    },
    "CED Avenal": {
      "lng": -120.15146,
      "lat": 35.996324
    },
    "CED Ducor 1": {
      "lng": -119.068414,
      "lat": 35.856278
    },
    "CED Ducor 2": {
      "lng": -119.068069,
      "lat": 35.844167
    },
    "CED Ducor 3": {
      "lng": -119.056992,
      "lat": 35.845378
    },
    "CED Ducor 4": {
      "lng": -119.026992,
      "lat": 35.850403
    },
    "Center Generating Station": {
      "lng": -118.1046,
      "lat": 33.9296
    },
    "Centerville PH": {
      "lng": -121.657383,
      "lat": 39.788877
    },
    "Centinela Solar Energy": {
      "lng": -115.647778,
      "lat": 32.686944
    },
    "Centinela State Prison": {
      "lng": -115.782916,
      "lat": 32.825833
    },
    "Central 40": {
      "lng": -121.104,
      "lat": 37.246
    },
    "Central Antelope Dry Ranch B LLC": {
      "lng": -118.301417,
      "lat": 34.705549
    },
    "Central Antelope Dry Ranch C": {
      "lng": -118.30046,
      "lat": 34.720499
    },
    "Central CA Fuel Cell 1": {
      "lng": -119.374348,
      "lat": 36.18347
    },
    "Central Utilities Plant LAX 2": {
      "lng": -118.404167,
      "lat": 33.943889
    },
    "Central Valley Ag Power": {
      "lng": -120.898611,
      "lat": 37.719722
    },
    "Century Generating Facility": {
      "lng": -117.3533,
      "lat": 34.0606
    },
    "CES Placerita Power Plant": {
      "lng": -118.4999,
      "lat": 34.3801
    },
    "CFW Solar X LLC - Vaughn": {
      "lng": -118.429836,
      "lat": 34.277355
    },
    "Chaffey College Rancho Cucamonga Campus": {
      "lng": -117.570404,
      "lat": 34.148139
    },
    "Chalk Cliff Limited": {
      "lng": -119.4299,
      "lat": 35.0968
    },
    "Chambers 4 - Eastvale DCA2": {
      "lng": -117.55391,
      "lat": 33.99347
    },
    "Chambers 4 - Eastvale LGB3": {
      "lng": -117.55289,
      "lat": 33.99848
    },
    "Chambers 4 - Redland LGB4": {
      "lng": -117.20138,
      "lat": 34.08041
    },
    "Chambers 4 - Rialto LGB8": {
      "lng": -117.40554,
      "lat": 34.12937
    },
    "Chambers 4 - Riverside LGB6": {
      "lng": -117.30036,
      "lat": 33.88102
    },
    "Chambers 4 - Sacramento SMF1": {
      "lng": -121.57434,
      "lat": 38.68251
    },
    "Chambers 4 - San Bernardino SNA7": {
      "lng": -117.27257,
      "lat": 34.07752
    },
    "Chevron - Lost Hills Hybrid": {
      "lng": -119.69,
      "lat": 35.62
    },
    "Chicago Park": {
      "lng": -120.890856,
      "lat": 39.177903
    },
    "Childrens Hospital": {
      "lng": -117.1517,
      "lat": 32.7992
    },
    "Chili Bar": {
      "lng": -120.813239,
      "lat": 38.771764
    },
    "Chowchilla II": {
      "lng": -120.2485,
      "lat": 37.1073
    },
    "Chuckawalla Solar": {
      "lng": -114.918056,
      "lat": 33.568333
    },
    "Chuckawalla Valley State Prison": {
      "lng": -114.903629,
      "lat": 33.565329
    },
    "Chula Vista Energy Center": {
      "lng": -117.058611,
      "lat": 32.591667
    },
    "CID Solar (CA)": {
      "lng": -119.566896,
      "lat": 36.140764
    },
    "CID Solar, LLC": {
      "lng": -119.564722,
      "lat": 36.138056
    },
    "Citizen B": {
      "lng": -120.432139,
      "lat": 36.723806
    },
    "Citizens Imperial Solar": {
      "lng": -115.433783,
      "lat": 33.191661
    },
    "City of Hayward WWTP": {
      "lng": -122.139717,
      "lat": 37.633526
    },
    "City of Madera WWTP": {
      "lng": -120.1572,
      "lat": 36.9408
    },
    "City of Palo Alto": {
      "lng": -122.1094,
      "lat": 37.4383
    },
    "City of Soledad Water Reclamation": {
      "lng": -121.340278,
      "lat": 36.421111
    },
    "City of Tulare Water Facility": {
      "lng": -119.375,
      "lat": 36.185
    },
    "Civic Center": {
      "lng": -118.2436,
      "lat": 34.0564
    },
    "Clear Lake Hydro Project": {
      "lng": -122.56668,
      "lat": 38.923556
    },
    "Clearwater Power Plant": {
      "lng": -117.6085,
      "lat": 33.8912
    },
    "Cloverdale Solar Center": {
      "lng": -123.020724,
      "lat": 38.769581
    },
    "Cloverdale Solar I": {
      "lng": -123.017778,
      "lat": 38.773056
    },
    "Coachella": {
      "lng": -116.1714,
      "lat": 33.6758
    },
    "Coachella Hills Wind": {
      "lng": -116.5797,
      "lat": 33.9077
    },
    "Coalinga 25D Cogen": {
      "lng": -120.3972,
      "lat": 36.1556
    },
    "Coalinga 6C Cogen": {
      "lng": -120.3828,
      "lat": 36.2097
    },
    "Coalinga Cogeneration Facility": {
      "lng": -120.364084,
      "lat": 36.170317
    },
    "Cold Canyon 1": {
      "lng": -120.599444,
      "lat": 35.186667
    },
    "Coleman PH": {
      "lng": -122.121904,
      "lat": 40.404645
    },
    "Colgate": {
      "lng": -121.191666,
      "lat": 39.330833
    },
    "Colgreen North Shore Solar Farm": {
      "lng": -115.974771,
      "lat": 33.540124
    },
    "Collierville Powerhouse": {
      "lng": -120.380497,
      "lat": 38.144636
    },
    "Collins Pine Project": {
      "lng": -121.244431,
      "lat": 40.303422
    },
    "Colon PV": {
      "lng": -118.252407,
      "lat": 33.792694
    },
    "Colton Solar One, LLC": {
      "lng": -117.319444,
      "lat": 34.093333
    },
    "Colton Solar Two, LLC": {
      "lng": -117.361667,
      "lat": 34.042222
    },
    "Columbia Solar Energy, LLC": {
      "lng": -121.86506,
      "lat": 38.019671
    },
    "Colusa Generating Station": {
      "lng": -122.27,
      "lat": 39.3661
    },
    "Combie South": {
      "lng": -121.0591,
      "lat": 39.0096
    },
    "Comcast - Universal City": {
      "lng": -118.36082,
      "lat": 34.137717
    },
    "Community Solar 1": {
      "lng": -115.481944,
      "lat": 32.982778
    },
    "Control Gorge": {
      "lng": -118.556988,
      "lat": 37.437921
    },
    "Copco 1": {
      "lng": -122.335317,
      "lat": 41.978914
    },
    "Copco 2": {
      "lng": -122.358127,
      "lat": 41.975716
    },
    "Coram Energy LLC": {
      "lng": -118.3417,
      "lat": 35.075
    },
    "Coram Energy LLC (ECT)": {
      "lng": -118.3417,
      "lat": 35.0583
    },
    "Coram Tehachapi": {
      "lng": -118.339444,
      "lat": 35.0663
    },
    "Corcoran": {
      "lng": -119.566516,
      "lat": 36.102307
    },
    "Corcoran Solar": {
      "lng": -119.573333,
      "lat": 36.138333
    },
    "Corcoran Solar 2": {
      "lng": -119.581667,
      "lat": 36.141944
    },
    "Corcoran Solar 3": {
      "lng": -119.58542,
      "lat": 36.15042
    },
    "CoreSite Real Estate 1656 McCarthy, L.P.": {
      "lng": -121.916248,
      "lat": 37.405727
    },
    "CoreSite Real Estate 2901 Coronado, L.P.": {
      "lng": -121.971728,
      "lat": 37.375089
    },
    "CoreSite Real Estate 2972 Stender, L.P.": {
      "lng": -121.970706,
      "lat": 37.376248
    },
    "CoreSite Real Estate 3032 Coronado, L.P.": {
      "lng": -121.973097,
      "lat": 37.376181
    },
    "CoreSite Real Estate 55 S. Market Street": {
      "lng": -121.891622,
      "lat": 37.334151
    },
    "Corona": {
      "lng": -117.6106,
      "lat": 33.85895
    },
    "Coronal Lost Hills": {
      "lng": -119.905833,
      "lat": 35.726667
    },
    "Coronus Adelanto West 1": {
      "lng": -117.464444,
      "lat": 34.548056
    },
    "Coronus Adelanto West 2": {
      "lng": -117.464444,
      "lat": 34.548056
    },
    "Coso Energy Developers": {
      "lng": -117.7886,
      "lat": 36.0019
    },
    "Coso Finance Partners": {
      "lng": -117.7981,
      "lat": 36.0372
    },
    "Coso Power Developers": {
      "lng": -117.7917,
      "lat": 36.0192
    },
    "Cosumnes Power Plant": {
      "lng": -121.123978,
      "lat": 38.338475
    },
    "Cottonwood": {
      "lng": -118.04389,
      "lat": 36.443421
    },
    "Cottonwood Solar, LLC (City of Corcoran)": {
      "lng": -119.554444,
      "lat": 36.134444
    },
    "Cottonwood Solar, LLC (Goose Lake)": {
      "lng": -119.58,
      "lat": 35.593611
    },
    "Cottonwood Solar, LLC Cottonwood Carport": {
      "lng": -122.568158,
      "lat": 38.129759
    },
    "County of San Diego COC Hybrid": {
      "lng": -117.131123,
      "lat": 32.833645
    },
    "County of San Diego SBRC": {
      "lng": -117.07775,
      "lat": 32.63266
    },
    "Covanta Delano Energy": {
      "lng": -119.2337,
      "lat": 35.7144
    },
    "Covanta Mendota": {
      "lng": -120.365422,
      "lat": 36.755896
    },
    "Covanta Stanislaus Energy": {
      "lng": -121.141,
      "lat": 37.3853
    },
    "Cove Hydroelectric": {
      "lng": -121.9487,
      "lat": 40.8713
    },
    "Cow Creek": {
      "lng": -122.02093,
      "lat": 40.570817
    },
    "Coyote Creek": {
      "lng": -117.95517,
      "lat": 33.92493
    },
    "Crafton Hills College Solar Farm": {
      "lng": -117.098889,
      "lat": 34.043056
    },
    "Creed Energy Center": {
      "lng": -121.854722,
      "lat": 38.242158
    },
    "Cresta": {
      "lng": -121.409687,
      "lat": 39.825907
    },
    "Crockett Cogen Project": {
      "lng": -122.2161,
      "lat": 38.0569
    },
    "Crown Cooling Facility": {
      "lng": -115.5325,
      "lat": 32.967222
    },
    "CSD 2 - Heritage High": {
      "lng": -121.755556,
      "lat": 37.921944
    },
    "CSD 2- Freedom High": {
      "lng": -121.719444,
      "lat": 37.969722
    },
    "CST001 McCarthy Fuel Cell": {
      "lng": -121.91676,
      "lat": 37.40553
    },
    "CSU East Bay": {
      "lng": -122.059444,
      "lat": 37.658889
    },
    "CSU Fresno Solar Project": {
      "lng": -119.741665,
      "lat": 36.809841
    },
    "CSU Long Beach": {
      "lng": -118.10796,
      "lat": 33.78383
    },
    "CSU Long Beach Lots 7 & 14": {
      "lng": -118.11357,
      "lat": 33.78104
    },
    "CSU Northridge Plant": {
      "lng": -118.530556,
      "lat": 34.243333
    },
    "CSU San Jose State University": {
      "lng": -121.884,
      "lat": 37.334
    },
    "CSUCI Site Authority": {
      "lng": -119.047916,
      "lat": 34.161951
    },
    "CSUF Nutwood Solar": {
      "lng": -117.8887,
      "lat": 33.87912
    },
    "CSUF State College": {
      "lng": -117.8887,
      "lat": 33.87912
    },
    "CSUF Trigeneration": {
      "lng": -117.88656,
      "lat": 33.880638
    },
    "CTV Power Purchase Contract Trust": {
      "lng": -118.3083,
      "lat": 35.0583
    },
    "Cuyama Solar, LLC": {
      "lng": -119.596,
      "lat": 34.901
    },
    "Cuyamaca Peak Energy": {
      "lng": -116.972071,
      "lat": 32.796491
    },
    "Cymric 31X Cogen": {
      "lng": -119.6592,
      "lat": 35.3639
    },
    "Cymric 36W Cogen": {
      "lng": -119.6756,
      "lat": 35.3633
    },
    "Cymric 6Z Cogen": {
      "lng": -119.643431,
      "lat": 35.346102
    },
    "Danell Bros Inc": {
      "lng": -119.631577,
      "lat": 36.267574
    },
    "D'Arrigo Brothers - Phase 4": {
      "lng": -121.636,
      "lat": 36.616
    },
    "DATS": {
      "lng": -117.89108,
      "lat": 33.70731
    },
    "De Sabla": {
      "lng": -121.631819,
      "lat": 39.869265
    },
    "Deadwood Creek": {
      "lng": -121.095833,
      "lat": 39.53
    },
    "Dedeaux": {
      "lng": -118.241894,
      "lat": 34.013113
    },
    "Deer Creek 2": {
      "lng": -120.984,
      "lat": 38.628
    },
    "Deer Creek PH": {
      "lng": -120.8443,
      "lat": 39.29807
    },
    "Del Ranch Company": {
      "lng": -115.616,
      "lat": 33.1646
    },
    "Delano Energy Center, LLC": {
      "lng": -119.294469,
      "lat": 35.790864
    },
    "Delano Land 1": {
      "lng": -119.344408,
      "lat": 35.761217
    },
    "Delta Energy Center, LLC": {
      "lng": -121.8439,
      "lat": 38.0169
    },
    "Department of Justice": {
      "lng": -121.44855,
      "lat": 38.548056
    },
    "Depot Park Solar System": {
      "lng": -121.398333,
      "lat": 38.515
    },
    "Dept of General Services -FTB": {
      "lng": -121.343,
      "lat": 38.565
    },
    "Desert Green Solar Farm LLC": {
      "lng": -116.34805,
      "lat": 33.27055
    },
    "Desert Harvest II LLC": {
      "lng": -115.38,
      "lat": 33.8
    },
    "Desert Harvest, LLC": {
      "lng": -115.38,
      "lat": 33.8
    },
    "Desert Hot Springs Solar": {
      "lng": -116.53,
      "lat": 33.923056
    },
    "Desert Sunlight 250, LLC": {
      "lng": -115.3947,
      "lat": 33.8055
    },
    "Desert Sunlight 300, LLC": {
      "lng": -115.393889,
      "lat": 33.823056
    },
    "Desert View Power": {
      "lng": -116.0873,
      "lat": 33.586
    },
    "Devil Canyon": {
      "lng": -117.3344,
      "lat": 34.2056
    },
    "DGS Central California Womens Facility": {
      "lng": -120.149966,
      "lat": 37.089701
    },
    "DGS Central Utility Plant": {
      "lng": -121.501389,
      "lat": 38.573889
    },
    "DGS Valley State Prison": {
      "lng": -120.153072,
      "lat": 37.10117
    },
    "DGS Wasco State Prison": {
      "lng": -119.407215,
      "lat": 35.593903
    },
    "Diablo Canyon": {
      "lng": -120.855542,
      "lat": 35.211536
    },
    "Diablo Winds LLC": {
      "lng": -121.614392,
      "lat": 37.763653
    },
    "Diamond H Dairy Power": {
      "lng": -120.37279,
      "lat": 37.01618
    },
    "Diamond Valley Lake": {
      "lng": -117.0698,
      "lat": 33.6808
    },
    "Diamond Valley Solar Project": {
      "lng": -117.0725,
      "lat": 33.713056
    },
    "Difwind Farms Ltd VI": {
      "lng": -118.1714,
      "lat": 35.0506
    },
    "Dillon Wind LLC": {
      "lng": -116.5527,
      "lat": 33.9393
    },
    "Dinosaur Point": {
      "lng": -121.1708,
      "lat": 37.0478
    },
    "Dinuba Energy": {
      "lng": -119.419176,
      "lat": 36.571485
    },
    "Dinuba Wastewater Treatment Plant": {
      "lng": -119.426388,
      "lat": 36.538056
    },
    "Dion R Holm": {
      "lng": -119.9675,
      "lat": 37.896649
    },
    "DirecTV - Los Angeles": {
      "lng": -118.42517,
      "lat": 33.983377
    },
    "Disney Prospect": {
      "lng": -118.43378,
      "lat": 34.083387
    },
    "Dissigno Healdsburg FPV, LLC": {
      "lng": -122.866189,
      "lat": 38.584405
    },
    "Division 1": {
      "lng": -118.130556,
      "lat": 34.708333
    },
    "Division 2": {
      "lng": -118.130556,
      "lat": 34.708333
    },
    "Division 3": {
      "lng": -118.130556,
      "lat": 34.708333
    },
    "Dome Project": {
      "lng": -119.583056,
      "lat": 35.220833
    },
    "Dominguez Plant": {
      "lng": -118.2347,
      "lat": 33.8431
    },
    "Don Pedro": {
      "lng": -120.420152,
      "lat": 37.695994
    },
    "Donaghy-Stockton": {
      "lng": -121.194893,
      "lat": 37.909523
    },
    "Donald Von Raesfeld": {
      "lng": -121.9508,
      "lat": 37.3767
    },
    "Donnells": {
      "lng": -120.034072,
      "lat": 38.246656
    },
    "Dos Palos": {
      "lng": -120.625358,
      "lat": 36.981755
    },
    "Double C Generation Limited Partnership": {
      "lng": -119.046,
      "lat": 35.5005
    },
    "Downey": {
      "lng": -118.128919,
      "lat": 33.919368
    },
    "Drews Generating Facility": {
      "lng": -117.3532,
      "lat": 34.0609
    },
    "Drop 1": {
      "lng": -114.942666,
      "lat": 32.711611
    },
    "Drop 2 (CA)": {
      "lng": -115.030975,
      "lat": 32.705435
    },
    "Drop 3 (CA)": {
      "lng": -115.126103,
      "lat": 32.705428
    },
    "Drop 4": {
      "lng": -115.219067,
      "lat": 32.705341
    },
    "Drop 5": {
      "lng": -107.810213,
      "lat": 38.404377
    },
    "Drum 1": {
      "lng": -120.767437,
      "lat": 39.256873
    },
    "Drum 2": {
      "lng": -120.767046,
      "lat": 39.257388
    },
    "DSH (CA) - Coalinga State Hospital": {
      "lng": -120.218453,
      "lat": 36.13726
    },
    "DTE Stockton": {
      "lng": -121.330433,
      "lat": 37.943633
    },
    "Dulles": {
      "lng": -117.517753,
      "lat": 34.030119
    },
    "Dutch Flat": {
      "lng": -120.835587,
      "lat": 39.216985
    },
    "Dutch Flat 2": {
      "lng": -120.8352,
      "lat": 39.2184
    },
    "Dynegy Oakland Power Plant": {
      "lng": -122.28185,
      "lat": 37.79675
    },
    "E&B Resources": {
      "lng": -119.060029,
      "lat": 35.534584
    },
    "E. F. Oxnard Energy Facility": {
      "lng": -119.1664,
      "lat": 34.1958
    },
    "EAFB - North Base": {
      "lng": -117.907222,
      "lat": 34.9325
    },
    "EAFB - South Base": {
      "lng": -117.900278,
      "lat": 34.917222
    },
    "Eagle Creek": {
      "lng": -119.39522,
      "lat": 35.214023
    },
    "East Highline": {
      "lng": -115.28245,
      "lat": 32.699451
    },
    "East Portal Generator": {
      "lng": -118.6242,
      "lat": 34.2619
    },
    "East Winds Project": {
      "lng": -116.541515,
      "lat": 33.890913
    },
    "EDF Renewable Windfarm V Inc": {
      "lng": -121.8333,
      "lat": 38.1167
    },
    "Edom Hills Project 1 LLC": {
      "lng": -116.460278,
      "lat": 33.881667
    },
    "Edward C Hyatt": {
      "lng": -121.492188,
      "lat": 39.542985
    },
    "Edwards Air Force Base": {
      "lng": -117.9206,
      "lat": 34.9077
    },
    "Edwards Sanborn E2": {
      "lng": -118.129025,
      "lat": 34.941507
    },
    "Edwards Sanborn E3": {
      "lng": -118.126489,
      "lat": 34.946324
    },
    "El Cajon Energy Center": {
      "lng": -116.972073,
      "lat": 32.796106
    },
    "El Cajon Energy Storage": {
      "lng": -117.050546,
      "lat": 32.8115
    },
    "El Centro": {
      "lng": -115.54,
      "lat": 32.802222
    },
    "El Dorado": {
      "lng": -120.619152,
      "lat": 38.793574
    },
    "El Dorado County": {
      "lng": -120.828256,
      "lat": 38.698145
    },
    "El Dorado Solar, LLC": {
      "lng": -121.059,
      "lat": 38.637
    },
    "El Nido Facility": {
      "lng": -120.4903,
      "lat": 37.1867
    },
    "El Segundo": {
      "lng": -118.425,
      "lat": 33.910556
    },
    "El Segundo Cogen": {
      "lng": -118.4031,
      "lat": 33.9058
    },
    "ELACC Photovoltaic Power Facility": {
      "lng": -118.148056,
      "lat": 34.040278
    },
    "Electra": {
      "lng": -120.669901,
      "lat": 38.330985
    },
    "Elevation Solar C": {
      "lng": -118.30425,
      "lat": 34.730263
    },
    "Elk Hills Cogen": {
      "lng": -119.737464,
      "lat": 35.479146
    },
    "Elk Hills Power": {
      "lng": -119.470707,
      "lat": 35.280318
    },
    "Ellwood": {
      "lng": -119.900055,
      "lat": 34.431419
    },
    "Elmore Company": {
      "lng": -115.6033,
      "lat": 33.1777
    },
    "EMWD - Moreno Valley RWRF": {
      "lng": -117.214055,
      "lat": 33.868252
    },
    "EMWD - Perris Valley RWRF": {
      "lng": -117.18766,
      "lat": 33.75998
    },
    "EMWD - San Jacinto RWRF NEM": {
      "lng": -117.016588,
      "lat": 33.798547
    },
    "EMWD - Sun City RWRF": {
      "lng": -117.218621,
      "lat": 33.696146
    },
    "Encina Water Pollution Control": {
      "lng": -117.3215,
      "lat": 33.1165
    },
    "Energy Center": {
      "lng": -117.163889,
      "lat": 34.063056
    },
    "Enerparc CA1 LLC": {
      "lng": -121.404167,
      "lat": 36.831667
    },
    "Enloe Medical - Chico": {
      "lng": -121.849688,
      "lat": 39.742597
    },
    "Equilon Los Angeles Refining": {
      "lng": -118.234722,
      "lat": 33.791667
    },
    "Equinix Caspian Dr. Fuel Cell": {
      "lng": -122.05211,
      "lat": 37.415092
    },
    "Equinix Douglas St. Fuel Cell": {
      "lng": -118.38453,
      "lat": 33.9212
    },
    "Equinix Great Oaks Blvd. Fuel Cell": {
      "lng": -121.78259,
      "lat": 37.24097
    },
    "Equinix Lundy Ave. Fuel Cell": {
      "lng": -121.88896,
      "lat": 37.38846
    },
    "Equinix Maple Ave. Fuel Cell": {
      "lng": -118.39463,
      "lat": 33.92542
    },
    "Equinix San Jose": {
      "lng": -121.781363,
      "lat": 37.241713
    },
    "EQX005.0 Toyama Fuel Cell": {
      "lng": -122.01457,
      "lat": 37.39961
    },
    "EQX010 Great Oaks Fuel Cell": {
      "lng": -121.78308,
      "lat": 37.24212
    },
    "EQX011 Duane Fuel Cell": {
      "lng": -121.95495,
      "lat": 37.37816
    },
    "EQX015 Great Oaks Fuel Cell": {
      "lng": -121.7816,
      "lat": 37.24187
    },
    "ESCA-LL-COLTON, LLC": {
      "lng": -117.36056,
      "lat": 34.03925
    },
    "Escondido Energy Center, LLC": {
      "lng": -117.1172,
      "lat": 33.1261
    },
    "Escondido Energy Storage": {
      "lng": -117.114274,
      "lat": 33.124446
    },
    "Etiwanda": {
      "lng": -117.5257,
      "lat": 34.10345
    },
    "Exchequer": {
      "lng": -120.26859,
      "lat": 37.584345
    },
    "Exeter Solar": {
      "lng": -119.128581,
      "lat": 36.2675
    },
    "Expressway Solar A": {
      "lng": -117.341667,
      "lat": 34.582222
    },
    "Expressway Solar B": {
      "lng": -117.340278,
      "lat": 34.583333
    },
    "Expressway Solar C2": {
      "lng": -117.341389,
      "lat": 34.584444
    },
    "Extreme San Ignacio": {
      "lng": -121.78532,
      "lat": 37.237
    },
    "Extreme Via Del Oro": {
      "lng": -121.78532,
      "lat": 37.23811
    },
    "ExxonMobil Santa Ynez Facility": {
      "lng": -120.044365,
      "lat": 34.464819
    },
    "FAA NorCal TRACON": {
      "lng": -121.257556,
      "lat": 38.559533
    },
    "Fairhaven Power": {
      "lng": -124.2028,
      "lat": 40.7995
    },
    "Fall Creek": {
      "lng": -122.360455,
      "lat": 41.985605
    },
    "Farmersville": {
      "lng": -119.12,
      "lat": 36.19
    },
    "Feather River Energy Center": {
      "lng": -121.60931,
      "lat": 39.109344
    },
    "Firebaugh-CCS-PV-1": {
      "lng": -120.612773,
      "lat": 36.734667
    },
    "Firestone Walker Brewery - Phase 1": {
      "lng": -120.694306,
      "lat": 35.595877
    },
    "Five H Farms Solar Array": {
      "lng": -120.292797,
      "lat": 37.120446
    },
    "Five Points Solar Park": {
      "lng": -120.236667,
      "lat": 36.386944
    },
    "Five Points Solar Station": {
      "lng": -120.103333,
      "lat": 36.400278
    },
    "FLEX Gibraltar": {
      "lng": -121.894109,
      "lat": 37.417288
    },
    "Folsom": {
      "lng": -121.158443,
      "lat": 38.70675
    },
    "Folsom SP and CSP Sacramento": {
      "lng": -121.16,
      "lat": 38.687
    },
    "Fontana": {
      "lng": -117.397119,
      "lat": 34.155162
    },
    "Foothill": {
      "lng": -118.494449,
      "lat": 34.318248
    },
    "Foothill Feeder": {
      "lng": -118.6092,
      "lat": 34.5148
    },
    "Foothill Sanitary Landfill - Solar Array": {
      "lng": -120.933761,
      "lat": 38.042352
    },
    "Forbestown": {
      "lng": -121.278784,
      "lat": 39.551012
    },
    "Forever 21 Retail, Inc.": {
      "lng": -118.196111,
      "lat": 34.071389
    },
    "Forks of Butte Hydro Project": {
      "lng": -121.631865,
      "lat": 39.869284
    },
    "Foundation AB": {
      "lng": -122.092778,
      "lat": 38.232222
    },
    "Foundation California Training Facility": {
      "lng": -121.382,
      "lat": 36.464
    },
    "Foundation CDCR LAC": {
      "lng": -118.221,
      "lat": 34.691
    },
    "Foundation Cemex BMQ": {
      "lng": -117.1125,
      "lat": 34.623056
    },
    "Foundation Cemex Madison": {
      "lng": -121.9325,
      "lat": 38.692778
    },
    "Foundation Cemex River Plant": {
      "lng": -117.307222,
      "lat": 34.561944
    },
    "Foundation IE": {
      "lng": -117.529444,
      "lat": 34.084722
    },
    "Foundation Mann Packing": {
      "lng": -121.454,
      "lat": 36.504
    },
    "Foundation NWNA": {
      "lng": -116.749167,
      "lat": 33.914167
    },
    "Foundation RRM": {
      "lng": -116.815,
      "lat": 33.917222
    },
    "Foundation SaIinas Valley State Prison": {
      "lng": -121.371,
      "lat": 36.474
    },
    "Foundation Scheid Vineyards": {
      "lng": -121.191589,
      "lat": 36.274508
    },
    "Foundation ST": {
      "lng": -121.521666,
      "lat": 37.714167
    },
    "Foundation Superior Farms": {
      "lng": -121.818056,
      "lat": 38.42
    },
    "Foundation Wal-Mart Red Bluff": {
      "lng": -122.193889,
      "lat": 40.111111
    },
    "FPL Energy Montezuma Winds LLC": {
      "lng": -121.79661,
      "lat": 38.163581
    },
    "Francisco St. Solar": {
      "lng": -118.3055,
      "lat": 33.8482
    },
    "Frankenheimer Power Plant": {
      "lng": -120.806972,
      "lat": 37.830378
    },
    "Franklin (CA)": {
      "lng": -118.413377,
      "lat": 34.103931
    },
    "Franklin Templeton San Mateo": {
      "lng": -122.292778,
      "lat": 37.5425
    },
    "Freeway Springs": {
      "lng": -118.037564,
      "lat": 33.892614
    },
    "Fremont - Kato Rd": {
      "lng": -121.932263,
      "lat": 37.479188
    },
    "French Meadows": {
      "lng": -120.406069,
      "lat": 39.077971
    },
    "Fresno Bullard High School Hybrid": {
      "lng": -119.810408,
      "lat": 36.818646
    },
    "Fresno Cogeneration Partners, LP": {
      "lng": -120.099846,
      "lat": 36.617022
    },
    "Fresno Hoover High School Hybrid": {
      "lng": -119.770709,
      "lat": 36.817816
    },
    "Fresno Solar": {
      "lng": -120.105144,
      "lat": 36.616846
    },
    "Fresno Sunnyside High School Hybrid": {
      "lng": -119.717809,
      "lat": 36.734014
    },
    "Friant Hydro Facility": {
      "lng": -119.703451,
      "lat": 36.997694
    },
    "Frito-Lay Cogen Plant": {
      "lng": -119.321633,
      "lat": 35.395063
    },
    "Frontier HS Solar Project": {
      "lng": -119.14888,
      "lat": 35.41824
    },
    "Frontier Solar LLC": {
      "lng": -121.130661,
      "lat": 37.379919
    },
    "FRV SI Transport Solar LP": {
      "lng": -121.3911,
      "lat": 38.4869
    },
    "FTF-PackingShed": {
      "lng": -119.430179,
      "lat": 36.546901
    },
    "G2 Energy Hay Rd": {
      "lng": -121.833611,
      "lat": 38.314722
    },
    "G2 Energy Ostrom Road LLC": {
      "lng": -121.3992,
      "lat": 39.0728
    },
    "Gap Pacific Distribution Center": {
      "lng": -121.7745,
      "lat": 36.806476
    },
    "Garnet Solar Generation Station I LLC": {
      "lng": -116.5431,
      "lat": 33.8906
    },
    "Garnet Wind Energy Center": {
      "lng": -116.5825,
      "lat": 33.9053
    },
    "Gas Utilization Facility": {
      "lng": -117.246698,
      "lat": 32.679172
    },
    "Gaskell West 1 Solar Facility": {
      "lng": -118.563578,
      "lat": 34.832786
    },
    "Gates Solar Station": {
      "lng": -120.114167,
      "lat": 36.176389
    },
    "Gateway Energy Storage System": {
      "lng": -116.910556,
      "lat": 32.570556
    },
    "Gateway Generating Station": {
      "lng": -121.7587,
      "lat": 38.0175
    },
    "Gavilan Community College Solar Project": {
      "lng": -121.593567,
      "lat": 37.003832
    },
    "GEN005 Antibody Fuel Cell": {
      "lng": -117.299,
      "lat": 33.21086
    },
    "Genentech Vacaville Meter #1": {
      "lng": -121.94777,
      "lat": 38.390756
    },
    "Genentech-Oceanside Hybrid": {
      "lng": -117.299593,
      "lat": 33.211095
    },
    "Genesis Solar Energy Project": {
      "lng": -114.998055,
      "lat": 33.665
    },
    "Geo East Mesa II": {
      "lng": -115.2637,
      "lat": 32.7757
    },
    "Geothermal 1": {
      "lng": -122.7195,
      "lat": 38.752
    },
    "Geothermal 2": {
      "lng": -122.7117,
      "lat": 38.7492
    },
    "Geysers Unit 5-20": {
      "lng": -122.745,
      "lat": 38.777
    },
    "Gianera": {
      "lng": -121.96867,
      "lat": 37.401
    },
    "Giffen": {
      "lng": -120.316111,
      "lat": 36.530278
    },
    "Giffen Solar Park": {
      "lng": -120.316111,
      "lat": 36.544722
    },
    "Gilead Foster City Solar Project": {
      "lng": -122.27797,
      "lat": 37.568597
    },
    "Gilead Oceanside Solar Project": {
      "lng": -117.304166,
      "lat": 33.213138
    },
    "Gilead Sciences La Verne Rooftop Solar": {
      "lng": -117.781867,
      "lat": 34.09623
    },
    "Gilroy Energy Center, LLC": {
      "lng": -121.5363,
      "lat": 36.999
    },
    "Glass House Camarillo Cultivation": {
      "lng": -119.083056,
      "lat": 34.179722
    },
    "Glenarm": {
      "lng": -118.1494,
      "lat": 34.126
    },
    "Glendale Battery Energy Storage System": {
      "lng": -118.283684,
      "lat": 34.162327
    },
    "Goal Line": {
      "lng": -117.0995,
      "lat": 33.1185
    },
    "Golden Acorn Casino": {
      "lng": -116.354587,
      "lat": 32.702107
    },
    "Golden Field Solar III, LLC": {
      "lng": -118.395,
      "lat": 34.847
    },
    "Golden Fields Solar I, LLC": {
      "lng": -118.386843,
      "lat": 34.839012
    },
    "Golden Hills North Wind Energy Center": {
      "lng": -121.630163,
      "lat": 37.769189
    },
    "Golden Hills Wind": {
      "lng": -121.640353,
      "lat": 37.713311
    },
    "Golden Springs Building C-1": {
      "lng": -118.053611,
      "lat": 33.914167
    },
    "Golden Springs Building D": {
      "lng": -118.052222,
      "lat": 33.911944
    },
    "Google Moffett Park Solar Project": {
      "lng": -122.017498,
      "lat": 37.405313
    },
    "Goose Haven Energy Center": {
      "lng": -121.843271,
      "lat": 38.226752
    },
    "Gosselin Hydro Plant": {
      "lng": -123.4347,
      "lat": 40.37
    },
    "GPS Cabazon Wind LLC": {
      "lng": -116.71026,
      "lat": 33.914986
    },
    "Grapeland Generating Station": {
      "lng": -117.5343,
      "lat": 34.0903
    },
    "Grassland 1&2 Solar Project": {
      "lng": -121.694167,
      "lat": 38.500278
    },
    "Grassland 3&4 Solar Project": {
      "lng": -121.694167,
      "lat": 38.500278
    },
    "Grayson Power Plant": {
      "lng": -118.2782,
      "lat": 34.1556
    },
    "Great Valley Solar Portfolio Holdings, LLC": {
      "lng": -120.37957,
      "lat": 36.581182
    },
    "Green Acres Solar Facility 1": {
      "lng": -121.464676,
      "lat": 38.282021
    },
    "Green Acres Solar Facility 2": {
      "lng": -121.461264,
      "lat": 38.28357
    },
    "Green Beanworks B PV": {
      "lng": -117.989541,
      "lat": 34.688042
    },
    "Green Beanworks C PV": {
      "lng": -117.931,
      "lat": 34.597
    },
    "Green Beanworks D PV": {
      "lng": -118.08215,
      "lat": 34.654976
    },
    "Green Power I": {
      "lng": -116.7167,
      "lat": 33.9014
    },
    "Gridley Main": {
      "lng": -121.684167,
      "lat": 39.373333
    },
    "Gridley Main Two": {
      "lng": -121.681667,
      "lat": 39.373333
    },
    "Grizzly": {
      "lng": -121.279173,
      "lat": 39.889287
    },
    "Grossmont Hospital": {
      "lng": -117.006397,
      "lat": 32.77973
    },
    "Grossmont HS Solar Project": {
      "lng": -116.999589,
      "lat": 32.816542
    },
    "Guadalupe Solar Farm": {
      "lng": -121.861079,
      "lat": 37.269039
    },
    "Guernsey Solar Station": {
      "lng": -119.650556,
      "lat": 36.163889
    },
    "H. Gonzales": {
      "lng": -118.2219,
      "lat": 33.9986
    },
    "Haas": {
      "lng": -119.02025,
      "lat": 36.927424
    },
    "Haiwee": {
      "lng": -117.956667,
      "lat": 36.109722
    },
    "Halsey": {
      "lng": -121.041932,
      "lat": 38.956985
    },
    "Hamilton Branch": {
      "lng": -121.089479,
      "lat": 40.268712
    },
    "Hanford 1 and 2": {
      "lng": -119.629167,
      "lat": 36.306667
    },
    "Hanford Energy Park Peaker": {
      "lng": -119.648055,
      "lat": 36.270072
    },
    "Harbor Cogen": {
      "lng": -118.2303,
      "lat": 33.7771
    },
    "Harbor Generating Station": {
      "lng": -118.2656,
      "lat": 33.7706
    },
    "Hat Creek 1": {
      "lng": -121.544462,
      "lat": 40.929297
    },
    "Hat Creek 2": {
      "lng": -121.548657,
      "lat": 40.960675
    },
    "Hatchet Creek Project": {
      "lng": -121.920428,
      "lat": 40.874144
    },
    "Hatchet Ridge Wind Project": {
      "lng": -121.8036,
      "lat": 40.8981
    },
    "Haynes Generating Station": {
      "lng": -118.09913,
      "lat": 33.764741
    },
    "Haypress": {
      "lng": -120.581684,
      "lat": 39.565677
    },
    "Hayworth Solar": {
      "lng": -118.878611,
      "lat": 35.325278
    },
    "Heber Geothermal": {
      "lng": -115.518,
      "lat": 32.7146
    },
    "Heber Solar": {
      "lng": -115.540556,
      "lat": 32.7125
    },
    "HEBT Irvine 1": {
      "lng": -117.852086,
      "lat": 33.713489
    },
    "HEBT Irvine 2": {
      "lng": -117.768222,
      "lat": 33.665246
    },
    "HEBT WLA 1": {
      "lng": -117.646846,
      "lat": 33.669266
    },
    "Hecate Energy Beacon Solar 1": {
      "lng": -118.014444,
      "lat": 35.269444
    },
    "Hecate Energy Beacon Solar 3": {
      "lng": -118.014722,
      "lat": 35.253056
    },
    "Hecate Energy Beacon Solar 4": {
      "lng": -118.026944,
      "lat": 35.253611
    },
    "Hecate Energy Johanna Facility": {
      "lng": -117.853061,
      "lat": 33.714672
    },
    "Heliocentric": {
      "lng": -117.971004,
      "lat": 34.65434
    },
    "Hellyer Solar Farm": {
      "lng": -121.81508,
      "lat": 37.286958
    },
    "Helms Pumped Storage": {
      "lng": -118.968252,
      "lat": 37.029304
    },
    "Helzel & Schwarzhoff 86": {
      "lng": -118.3952,
      "lat": 35.0925
    },
    "Henrietta D Energy Storage LLC": {
      "lng": -119.904686,
      "lat": 36.228872
    },
    "Henrietta Peaker Plant": {
      "lng": -119.9044,
      "lat": 36.2397
    },
    "Henrietta Solar Project": {
      "lng": -119.805833,
      "lat": 36.218806
    },
    "Hesperia": {
      "lng": -117.378056,
      "lat": 34.435
    },
    "Hickman": {
      "lng": -120.746247,
      "lat": 37.621536
    },
    "High Desert Power Project": {
      "lng": -117.3647,
      "lat": 34.5953
    },
    "High Sierra Cogeneration Plant": {
      "lng": -120.4774,
      "lat": 40.4128
    },
    "High Sierra Limited": {
      "lng": -119.030546,
      "lat": 35.509041
    },
    "High Winds LLC": {
      "lng": -121.8053,
      "lat": 38.1381
    },
    "HL Power": {
      "lng": -120.264848,
      "lat": 40.368338
    },
    "HL Solar": {
      "lng": -120.262,
      "lat": 40.373
    },
    "Hoag Hospital Cogen Plant": {
      "lng": -117.936389,
      "lat": 33.622778
    },
    "Hollister Solar LLC": {
      "lng": -121.428889,
      "lat": 36.8625
    },
    "Honda Torrance": {
      "lng": -118.315,
      "lat": 33.838056
    },
    "Horn": {
      "lng": -118.274167,
      "lat": 34.708889
    },
    "Hudson - High Desert Hybrid": {
      "lng": -117.3724,
      "lat": 34.6365
    },
    "Humboldt Bay": {
      "lng": -124.2103,
      "lat": 40.7415
    },
    "Humboldt Sawmill Company": {
      "lng": -124.1014,
      "lat": 40.4728
    },
    "Huron Solar Station": {
      "lng": -120.030555,
      "lat": 36.181389
    },
    "Hyperion Treatment Plant CHP Plant": {
      "lng": -118.4298,
      "lat": 33.9254
    },
    "IKEA Tejon 345": {
      "lng": -118.946944,
      "lat": 34.975278
    },
    "Illumina Way SD": {
      "lng": -117.19968,
      "lat": 32.87302
    },
    "Imperial Solar Energy Center South": {
      "lng": -115.658284,
      "lat": 32.663289
    },
    "Imperial Solar Energy Center West": {
      "lng": -115.779311,
      "lat": 32.772225
    },
    "Imperial Valley Solar Co (IVSC) 2, LLC": {
      "lng": -115.4975,
      "lat": 33.252222
    },
    "Imperial Valley Solar Company 1 LLC": {
      "lng": -115.496667,
      "lat": 33.245833
    },
    "Imperial Valley Solar, LLC": {
      "lng": -115.587265,
      "lat": 32.671099
    },
    "Improvement Dst No. 4": {
      "lng": -119.038611,
      "lat": 35.402778
    },
    "Indian Valley Dam Hydro Project": {
      "lng": -122.535556,
      "lat": 39.0788
    },
    "Indigo Generation Facility": {
      "lng": -116.552994,
      "lat": 33.910873
    },
    "Industry MetroLink PV 1": {
      "lng": -117.845603,
      "lat": 34.008661
    },
    "Industry Solar Power Generation Station 1 LLC": {
      "lng": -117.456944,
      "lat": 34.553056
    },
    "Inskip": {
      "lng": -121.965502,
      "lat": 40.400988
    },
    "Intel Folsom": {
      "lng": -121.172999,
      "lat": 38.645119
    },
    "Intel Folsom Phase 2": {
      "lng": -121.163889,
      "lat": 38.643611
    },
    "Intel Folsom Phase 3": {
      "lng": -121.162623,
      "lat": 38.645961
    },
    "Intel Santa Clara": {
      "lng": -121.963808,
      "lat": 37.384745
    },
    "IOS II-LAX9": {
      "lng": -117.45,
      "lat": 34.09
    },
    "Iron Gate": {
      "lng": -122.43678,
      "lat": 41.932981
    },
    "Ironwood Solar LLC": {
      "lng": -114.918056,
      "lat": 33.565
    },
    "Irwindale Brew Yard, LLC": {
      "lng": -117.938333,
      "lat": 34.121667
    },
    "Isabella Hydro Project": {
      "lng": -118.4822,
      "lat": 35.6439
    },
    "ISH Solar Hospital Downey": {
      "lng": -118.124444,
      "lat": 33.921667
    },
    "ISH Solar Hospital SDMC": {
      "lng": -117.095277,
      "lat": 32.791667
    },
    "Ivanhoe Solar": {
      "lng": -119.255,
      "lat": 36.3875
    },
    "Ivanpah 1": {
      "lng": -115.4525,
      "lat": 35.533056
    },
    "Ivanpah 2": {
      "lng": -115.468611,
      "lat": 35.556111
    },
    "Ivanpah 3": {
      "lng": -115.4825,
      "lat": 35.580833
    },
    "IVC Solar": {
      "lng": -115.506629,
      "lat": 32.83186
    },
    "J S Eastwood": {
      "lng": -119.2567,
      "lat": 37.147926
    },
    "J&A-Santa Maria II LLC": {
      "lng": -120.3803,
      "lat": 34.9494
    },
    "J.R. Simplot Company": {
      "lng": -121.2775,
      "lat": 37.806944
    },
    "Jacumba Solar Farm": {
      "lng": -116.12896,
      "lat": 32.6212
    },
    "James B Black": {
      "lng": -121.975182,
      "lat": 40.992295
    },
    "Jaybird": {
      "lng": -120.53176,
      "lat": 38.83405
    },
    "Johanna Energy Center, LLC": {
      "lng": -117.853984,
      "lat": 33.715016
    },
    "John L. Featherstone Plant": {
      "lng": -115.573303,
      "lat": 33.204635
    },
    "Jones Fork": {
      "lng": -120.381883,
      "lat": 38.850283
    },
    "JSR000 N Mathilda Fuel Cell": {
      "lng": -122.024,
      "lat": 37.4124
    },
    "Judge F Carr": {
      "lng": -122.626973,
      "lat": 40.646934
    },
    "Juniper Networks Sunnyvale": {
      "lng": -122.026944,
      "lat": 37.406944
    },
    "Kaiser Deer Valley Rd. Fuel Cell": {
      "lng": -121.77182,
      "lat": 37.95169
    },
    "Kaiser Downey": {
      "lng": -118.128611,
      "lat": 33.919167
    },
    "Kaiser East La Palma Ave. Fuel Cell": {
      "lng": -117.84531,
      "lat": 33.85098
    },
    "Kaiser Napa Valley Corporate Dr Fuel Cell": {
      "lng": -122.27305,
      "lat": 38.25477
    },
    "Kaiser Ontario": {
      "lng": -117.608889,
      "lat": 34.0325
    },
    "Kaiser Owens Dr. Fuel Cell": {
      "lng": -121.89419,
      "lat": 37.69923
    },
    "Kanaka": {
      "lng": -121.296117,
      "lat": 39.558897
    },
    "Karen Avenue Wind Farm": {
      "lng": -116.56222,
      "lat": 33.92194
    },
    "Kaweah 1": {
      "lng": -118.862023,
      "lat": 36.465066
    },
    "Kaweah 2": {
      "lng": -118.87988,
      "lat": 36.461326
    },
    "Kaweah 3": {
      "lng": -118.835645,
      "lat": 36.486059
    },
    "Kaweah Delta District Hospital": {
      "lng": -119.2947,
      "lat": 36.3275
    },
    "Kearny Mesa Storage LLC": {
      "lng": -117.126079,
      "lat": 32.815256
    },
    "Kekawaka Power House": {
      "lng": -123.508983,
      "lat": 40.0941
    },
    "Kellogg's - San Jose": {
      "lng": -121.871944,
      "lat": 37.356389
    },
    "Kelly Ridge": {
      "lng": -121.491247,
      "lat": 39.531784
    },
    "Kerckhoff": {
      "lng": -119.552826,
      "lat": 37.092486
    },
    "Kerckhoff 2": {
      "lng": -119.558008,
      "lat": 37.071846
    },
    "Kern Canyon": {
      "lng": -118.796498,
      "lat": 35.44072
    },
    "Kern Front Limited": {
      "lng": -119.040128,
      "lat": 35.51647
    },
    "Kern Oil & Refining Co": {
      "lng": -118.918333,
      "lat": 35.295556
    },
    "Kern River 1": {
      "lng": -118.77959,
      "lat": 35.460303
    },
    "Kern River 3": {
      "lng": -118.436233,
      "lat": 35.776185
    },
    "Kern River Cogeneration": {
      "lng": -118.9849,
      "lat": 35.4515
    },
    "Kern River Eastridge Cogen": {
      "lng": -118.9617,
      "lat": 35.4406
    },
    "Keswick": {
      "lng": -122.446441,
      "lat": 40.611858
    },
    "Kettering Solar 1": {
      "lng": -118.123611,
      "lat": 34.700278
    },
    "Kettering Solar 2": {
      "lng": -118.123611,
      "lat": 34.700278
    },
    "Kettleman Solar -Centaurus": {
      "lng": -119.9475,
      "lat": 36.050833
    },
    "Kettleman Solar Project": {
      "lng": -121.14122,
      "lat": 38.12139
    },
    "Keysight - Santa Rosa": {
      "lng": -122.711104,
      "lat": 38.481931
    },
    "Kiefer Landfill": {
      "lng": -121.1944,
      "lat": 38.5136
    },
    "Kilarc": {
      "lng": -121.873253,
      "lat": 40.677827
    },
    "Kilroy Solar": {
      "lng": -117.234157,
      "lat": 32.936137
    },
    "King City Peaking": {
      "lng": -121.124633,
      "lat": 36.224758
    },
    "King City Power Plant": {
      "lng": -121.1278,
      "lat": 36.225
    },
    "Kingbird A Solar LLC": {
      "lng": -118.432,
      "lat": 34.819
    },
    "Kingbird B Solar, LLC": {
      "lng": -118.438547,
      "lat": 34.824206
    },
    "Kings Beach": {
      "lng": -120.027222,
      "lat": 39.245833
    },
    "Kings River PH": {
      "lng": -119.158777,
      "lat": 36.888166
    },
    "Kings River Syphon": {
      "lng": -119.441856,
      "lat": 36.764491
    },
    "Kingsburg Cogen Facility": {
      "lng": -119.5794,
      "lat": 36.5397
    },
    "Kingsburg Solar": {
      "lng": -119.540278,
      "lat": 36.505278
    },
    "Kohls San Bernardino Solar Facility": {
      "lng": -117.266831,
      "lat": 34.094695
    },
    "Kroger La Habra": {
      "lng": -117.939825,
      "lat": 33.917918
    },
    "KSR028 Merced Fuel Cell": {
      "lng": -122.16471,
      "lat": 37.70649
    },
    "KSR053 Cantara Fuel Cell": {
      "lng": -118.29431,
      "lat": 33.7902
    },
    "Kumeyaay Wind": {
      "lng": -116.3425,
      "lat": 32.705556
    },
    "Kyocera America Project": {
      "lng": -117.140548,
      "lat": 32.819675
    },
    "L-8 Solar Project": {
      "lng": -117.973103,
      "lat": 34.654347
    },
    "La Grange": {
      "lng": -120.4436,
      "lat": 37.6697
    },
    "La Joya Del Sol": {
      "lng": -119.7975,
      "lat": 36.678611
    },
    "La Paloma Generating Plant": {
      "lng": -119.5919,
      "lat": 35.2956
    },
    "Lafayette 2 - Internal Services Dept": {
      "lng": -118.18,
      "lat": 34.05
    },
    "Lafayette 2 - MLK Jr. Hospital (MLK)": {
      "lng": -118.24,
      "lat": 33.92
    },
    "Laguna Water Reclamation Facility": {
      "lng": -122.765833,
      "lat": 38.368889
    },
    "Lake": {
      "lng": -118.3147,
      "lat": 34.1772
    },
    "Lake Herman": {
      "lng": -122.1417,
      "lat": 38.0971
    },
    "Lake Hodges Hydroelectric Facility": {
      "lng": -117.119444,
      "lat": 33.058611
    },
    "Lake Mathews": {
      "lng": -117.4537,
      "lat": 33.8553
    },
    "Lake Mendocino": {
      "lng": -123.185556,
      "lat": 39.197222
    },
    "Lambie Energy Center": {
      "lng": -121.868333,
      "lat": 38.221577
    },
    "Lancaster": {
      "lng": -117.972222,
      "lat": 34.743383
    },
    "Lancaster Baptist Church": {
      "lng": -118.058889,
      "lat": 34.695833
    },
    "Lancaster Dry Farm Ranch B": {
      "lng": -118.289444,
      "lat": 34.716944
    },
    "Lancaster Little Rock": {
      "lng": -117.932778,
      "lat": 34.599444
    },
    "Lancaster Solar 1": {
      "lng": -118.095,
      "lat": 34.715
    },
    "Lancaster Solar 2": {
      "lng": -118.166111,
      "lat": 34.710556
    },
    "Lancaster WAD B": {
      "lng": -118.19333,
      "lat": 34.780192
    },
    "Laredo Detention Facility - Solar 2, 3": {
      "lng": -119.109941,
      "lat": 35.510942
    },
    "Larkspur Energy Faciity": {
      "lng": -116.944197,
      "lat": 32.567041
    },
    "Las Virgenes": {
      "lng": -118.70404,
      "lat": 34.1265
    },
    "Las Virgenes Municipal Water District": {
      "lng": -118.701944,
      "lat": 34.136111
    },
    "Lavio Solar": {
      "lng": -122.663388,
      "lat": 38.194599
    },
    "LAWRP": {
      "lng": -117.71377,
      "lat": 33.63778
    },
    "Lemoore 1": {
      "lng": -119.798139,
      "lat": 36.257938
    },
    "Liberty HS Solar Project": {
      "lng": -119.127496,
      "lat": 35.364408
    },
    "Life Technologies - Pleasanton": {
      "lng": -121.884167,
      "lat": 37.643889
    },
    "Life Technologies Carlsbad": {
      "lng": -117.286944,
      "lat": 33.137222
    },
    "Lime Saddle": {
      "lng": -121.57096,
      "lat": 39.698755
    },
    "Lincoln Landfill": {
      "lng": -121.341944,
      "lat": 38.838056
    },
    "Lincoln WWTF Hybrid": {
      "lng": -121.346,
      "lat": 38.859
    },
    "Lindberg FIeld Solar": {
      "lng": -117.207718,
      "lat": 32.729238
    },
    "Lindberg Field Solar 2": {
      "lng": -117.205083,
      "lat": 32.729444
    },
    "Linde Wilmington": {
      "lng": -118.229392,
      "lat": 33.78856
    },
    "Lindsay Solar": {
      "lng": -119.133889,
      "lat": 36.211944
    },
    "Little Bear 3": {
      "lng": -120.415,
      "lat": 36.716
    },
    "Little Bear 4": {
      "lng": -120.415,
      "lat": 36.716
    },
    "Little Bear 5": {
      "lng": -120.415,
      "lat": 36.716
    },
    "Little Bear Solar 1, LLC": {
      "lng": -120.415,
      "lat": 36.716
    },
    "Little Rock Pham Solar": {
      "lng": -117.94751,
      "lat": 34.600231
    },
    "Live Oak Limited": {
      "lng": -119.0298,
      "lat": 35.4837
    },
    "Lockhart Solar PV, LLC": {
      "lng": -117.3563,
      "lat": 35.0308
    },
    "Lockheed Martin Sunnyvale": {
      "lng": -122.028056,
      "lat": 37.413056
    },
    "Lodi": {
      "lng": -121.300541,
      "lat": 38.146571
    },
    "Lodi Energy Center": {
      "lng": -121.3875,
      "lat": 38.088056
    },
    "Loma Linda University Cogen": {
      "lng": -117.248417,
      "lat": 34.050391
    },
    "Lone Valley Solar Park I LLC": {
      "lng": -116.865517,
      "lat": 34.398059
    },
    "Lone Valley Solar Park II LLC": {
      "lng": -116.8625,
      "lat": 34.408333
    },
    "Long Beach Convention Center": {
      "lng": -118.189377,
      "lat": 33.765236
    },
    "Long Beach Generating Station": {
      "lng": -118.2248,
      "lat": 33.7641
    },
    "Longboat Solar, LLC": {
      "lng": -117.061,
      "lat": 34.541
    },
    "Loon Lake": {
      "lng": -120.324503,
      "lat": 38.982697
    },
    "Los Angeles Harbor College": {
      "lng": -118.28493,
      "lat": 33.783102
    },
    "Los Angeles Refinery Wilmington": {
      "lng": -118.2867,
      "lat": 33.7686
    },
    "Los Esteros Critical Energy Facility": {
      "lng": -121.9319,
      "lat": 37.425
    },
    "Los Medanos Energy Center, LLC": {
      "lng": -121.873004,
      "lat": 38.030071
    },
    "Lost Creek I": {
      "lng": -121.4145,
      "lat": 40.7576
    },
    "Lost Hills Cogeneration Plant": {
      "lng": -119.766944,
      "lat": 35.666111
    },
    "Lost Hills/Blackwell": {
      "lng": -119.85,
      "lat": 35.62
    },
    "Lower Tule River": {
      "lng": -118.788868,
      "lat": 36.13568
    },
    "Lundy": {
      "lng": -119.172015,
      "lat": 38.042729
    },
    "Ma": {
      "lng": -118.0625,
      "lat": 34.695278
    },
    "Maclay Solar Project": {
      "lng": -118.430833,
      "lat": 34.319722
    },
    "Madelyn Solar, LLC": {
      "lng": -117.323611,
      "lat": 34.768333
    },
    "Madera 1 PV": {
      "lng": -120.167021,
      "lat": 37.056468
    },
    "Madera Community Hospital": {
      "lng": -120.046667,
      "lat": 36.941111
    },
    "Magnolia": {
      "lng": -118.3153,
      "lat": 34.1786
    },
    "Malaga Power": {
      "lng": -119.7404,
      "lat": 36.6697
    },
    "Malburg Generating Station": {
      "lng": -118.2219,
      "lat": 33.9986
    },
    "Malech Solar Farm": {
      "lng": -121.738616,
      "lat": 37.228736
    },
    "Mammoth G1": {
      "lng": -118.9143,
      "lat": 37.6451
    },
    "Mammoth G2": {
      "lng": -118.9092,
      "lat": 37.6464
    },
    "Mammoth G3": {
      "lng": -118.9096,
      "lat": 37.6455
    },
    "Mammoth Pool": {
      "lng": -119.337983,
      "lat": 37.220302
    },
    "Manteca Land PV": {
      "lng": -121.216191,
      "lat": 37.84013
    },
    "Manzana Wind LLC": {
      "lng": -118.4612,
      "lat": 34.9349
    },
    "Marathon Bay Area Cogen": {
      "lng": -122.0589,
      "lat": 38.0244
    },
    "Mariani Packing Vacaville Solar": {
      "lng": -121.96366,
      "lat": 38.40008
    },
    "Maricopa West Solar PV, LLC": {
      "lng": -119.317778,
      "lat": 35.115278
    },
    "Marin Clean Energy Solar One": {
      "lng": -122.377202,
      "lat": 37.945296
    },
    "Marina Landfill Gas": {
      "lng": -121.7686,
      "lat": 36.7131
    },
    "Marina South": {
      "lng": -118.275617,
      "lat": 33.717993
    },
    "Mariposa Energy Project": {
      "lng": -121.601944,
      "lat": 37.789167
    },
    "Marsh Landing Generating Station": {
      "lng": -121.765,
      "lat": 38.016944
    },
    "Martinez Refining": {
      "lng": -122.1117,
      "lat": 38.0169
    },
    "Martinez Sulfuric Acid Regeneration Plt": {
      "lng": -122.1139,
      "lat": 38.0314
    },
    "Maverick Solar 4, LLC": {
      "lng": -115.38,
      "lat": 33.7
    },
    "Maverick Solar 6, LLC": {
      "lng": -115.38,
      "lat": 33.7
    },
    "Maverick Solar 7, LLC": {
      "lng": -115.38,
      "lat": 33.7
    },
    "Maverick Solar, LLC": {
      "lng": -115.38,
      "lat": 33.7
    },
    "Maxim": {
      "lng": -121.949416,
      "lat": 37.405689
    },
    "MCAGCC Cogen Plant": {
      "lng": -116.054167,
      "lat": 34.237222
    },
    "McClellan (CA)": {
      "lng": -121.393336,
      "lat": 38.652731
    },
    "McClure": {
      "lng": -120.93139,
      "lat": 37.62936
    },
    "McCoy Solar Energy Project Hybrid": {
      "lng": -114.739722,
      "lat": 33.694722
    },
    "McGrath Generating Station": {
      "lng": -119.246944,
      "lat": 34.205556
    },
    "McKittrick Cogen": {
      "lng": -119.6597,
      "lat": 35.3161
    },
    "McKittrick Limited": {
      "lng": -119.661992,
      "lat": 35.319503
    },
    "McSwain": {
      "lng": -120.31009,
      "lat": 37.522065
    },
    "Medline Ind. Phase 2 Tracy": {
      "lng": -121.508945,
      "lat": 37.734761
    },
    "Merced 1 PV": {
      "lng": -120.878081,
      "lat": 36.989475
    },
    "Merced Falls": {
      "lng": -120.329905,
      "lat": 37.52301
    },
    "Merced Solar LLC": {
      "lng": -120.484444,
      "lat": 37.267778
    },
    "Meridian Vineyards": {
      "lng": -120.566944,
      "lat": 35.670278
    },
    "Mesa Wind Power LLC": {
      "lng": -116.6769,
      "lat": 33.9461
    },
    "Mesquite Lake Energy Park Plant 2": {
      "lng": -115.511949,
      "lat": 32.903826
    },
    "Mesquite Lake Water & Power Plant 1": {
      "lng": -115.513747,
      "lat": 32.906569
    },
    "Metcalf Energy Center": {
      "lng": -121.7457,
      "lat": 37.2207
    },
    "Metro Support Services Center Solar": {
      "lng": -118.229722,
      "lat": 34.0578
    },
    "Micro Santa Clara": {
      "lng": -121.899969,
      "lat": 37.378422
    },
    "Middle Fork": {
      "lng": -120.596383,
      "lat": 39.024801
    },
    "Middle Gorge": {
      "lng": -118.569167,
      "lat": 37.51
    },
    "Mid-Set Cogeneration": {
      "lng": -119.57097,
      "lat": 35.19456
    },
    "Midway Peaking": {
      "lng": -120.579727,
      "lat": 36.654114
    },
    "Midway Solar Farm 1": {
      "lng": -115.572788,
      "lat": 33.176627
    },
    "Midway Solar Farm II": {
      "lng": -115.531694,
      "lat": 33.162056
    },
    "Midway Solar Farm III": {
      "lng": -115.544918,
      "lat": 33.154761
    },
    "Midway Sunset Cogen": {
      "lng": -119.6294,
      "lat": 35.2269
    },
    "Mill Creek 3": {
      "lng": -117.039539,
      "lat": 34.088019
    },
    "Millikan BESS": {
      "lng": -117.833,
      "lat": 33.691
    },
    "Mira Loma": {
      "lng": -117.5375,
      "lat": 34.013056
    },
    "Mira Loma Energy Storage Facility": {
      "lng": -117.560934,
      "lat": 34.006125
    },
    "Mira Loma Generating Station": {
      "lng": -117.5604,
      "lat": 34.0053
    },
    "Miramar Energy Facility": {
      "lng": -117.166389,
      "lat": 32.8769
    },
    "Mission College Blvd. Fuel Cell": {
      "lng": -121.96238,
      "lat": 37.38633
    },
    "Mission Solar LLC": {
      "lng": -120.4825,
      "lat": 37.267778
    },
    "Mitchell Solar, LLC": {
      "lng": -117.323611,
      "lat": 34.765
    },
    "MM Lopez Energy": {
      "lng": -118.389444,
      "lat": 34.2925
    },
    "MM San Diego-Miramar": {
      "lng": -117.162747,
      "lat": 32.844845
    },
    "MM Tulare Energy": {
      "lng": -119.394,
      "lat": 36.388
    },
    "MM West Covina": {
      "lng": -117.906129,
      "lat": 34.033206
    },
    "MM Yolo Power": {
      "lng": -121.687518,
      "lat": 38.596283
    },
    "Moccasin": {
      "lng": -120.299254,
      "lat": 37.809388
    },
    "Moccasin Low Head Hydro Project": {
      "lng": -120.312033,
      "lat": 37.816447
    },
    "Mojave 16/17/18": {
      "lng": -118.2642,
      "lat": 35.045
    },
    "Mojave 3/4/5": {
      "lng": -118.2572,
      "lat": 35.0506
    },
    "Mojave Siphon": {
      "lng": -117.323548,
      "lat": 34.307444
    },
    "Mojave Solar Project": {
      "lng": -117.339144,
      "lat": 35.017717
    },
    "Monterey One Water": {
      "lng": -121.7714,
      "lat": 36.7056
    },
    "Monterey Regional Water Pollution Control Agency": {
      "lng": -121.787778,
      "lat": 36.721111
    },
    "Montezuma Wind II": {
      "lng": -121.821944,
      "lat": 38.116667
    },
    "Montgomery Creek Hydro": {
      "lng": -121.956446,
      "lat": 40.846806
    },
    "Monticello Dam": {
      "lng": -122.104444,
      "lat": 38.513056
    },
    "Moonlight Packing - Phase 2": {
      "lng": -119.469986,
      "lat": 36.585794
    },
    "Morelos del Sol": {
      "lng": -119.818712,
      "lat": 35.731362
    },
    "Morgan Lancaster 1": {
      "lng": -118.0836,
      "lat": 34.6608
    },
    "Moss Landing Power Plant": {
      "lng": -121.782241,
      "lat": 36.804837
    },
    "Mount Signal Solar Farm II": {
      "lng": -115.6292,
      "lat": 32.6685
    },
    "Mount Signal Solar Farm V": {
      "lng": -115.56721,
      "lat": 32.678805
    },
    "Mountain View I&2": {
      "lng": -116.5642,
      "lat": 33.9211
    },
    "Mountain View III": {
      "lng": -116.5914,
      "lat": 33.9141
    },
    "Mountain View IV": {
      "lng": -116.5527,
      "lat": 33.8816
    },
    "Mountainview Generating Station": {
      "lng": -117.2418,
      "lat": 34.0818
    },
    "Mt Poso Cogeneration": {
      "lng": -119.006245,
      "lat": 35.576323
    },
    "Muck Valley Hydroelectric": {
      "lng": -121.255833,
      "lat": 40.976389
    },
    "Municipal Cogen Plant": {
      "lng": -116.5106,
      "lat": 33.8247
    },
    "Munro Valley Solar": {
      "lng": -117.9955,
      "lat": 36.2647
    },
    "Murphys": {
      "lng": -120.446396,
      "lat": 38.147093
    },
    "Mustang Hills LLC": {
      "lng": -118.293188,
      "lat": 35.030855
    },
    "Mustang Two": {
      "lng": -119.896389,
      "lat": 36.216806
    },
    "Nacimiento Hydro Project": {
      "lng": -120.883331,
      "lat": 35.798576
    },
    "Narrows 2": {
      "lng": -121.270409,
      "lat": 39.238996
    },
    "Narrows PH": {
      "lng": -121.271741,
      "lat": 39.2364
    },
    "National Raisin": {
      "lng": -119.673088,
      "lat": 36.624512
    },
    "Navajo Solar Power Generation Station 1 LLC": {
      "lng": -117.191111,
      "lat": 34.551111
    },
    "Naval Air Weapons Station China Lake": {
      "lng": -117.68137,
      "lat": 35.68771
    },
    "Naval Hospital Medical Center": {
      "lng": -117.1464,
      "lat": 32.7261
    },
    "NCPA Combustion Turbine Project #2": {
      "lng": -121.386944,
      "lat": 38.088138
    },
    "Nelson Creek": {
      "lng": -121.892351,
      "lat": 41.028893
    },
    "New Hogan Power Plant": {
      "lng": -120.814942,
      "lat": 38.149582
    },
    "New Melones": {
      "lng": -120.528211,
      "lat": 37.946927
    },
    "Newberry Solar 1 LLC": {
      "lng": -116.683108,
      "lat": 34.853581
    },
    "Newcastle": {
      "lng": -121.092982,
      "lat": 38.835182
    },
    "New-Indy Ontario Mill": {
      "lng": -117.540193,
      "lat": 34.045361
    },
    "NextEra Westside PV": {
      "lng": -120.134486,
      "lat": 36.381775
    },
    "Niagara Bottling - Rialto": {
      "lng": -117.418451,
      "lat": 34.126094
    },
    "Niagara Bottling Stockton": {
      "lng": -121.24891,
      "lat": 37.894817
    },
    "Nickel 1 Solar Facility": {
      "lng": -119.716512,
      "lat": 35.647295
    },
    "Nicolis Solar PV Plant": {
      "lng": -119.056111,
      "lat": 35.835833
    },
    "Niland Gas Turbine Plant": {
      "lng": -115.504456,
      "lat": 33.238842
    },
    "Nimbus": {
      "lng": -121.220198,
      "lat": 38.63735
    },
    "NLP Granger": {
      "lng": -117.0588,
      "lat": 33.2704
    },
    "Nordhoff Place": {
      "lng": -118.565833,
      "lat": 34.236667
    },
    "North Bay Solar 1": {
      "lng": -121.966389,
      "lat": 38.300556
    },
    "North Brawley Geothermal Plant": {
      "lng": -115.5439,
      "lat": 33.0139
    },
    "North City - Landfill Gas Engine North": {
      "lng": -117.197788,
      "lat": 32.877822
    },
    "North City - Landfill Gas Engines South": {
      "lng": -117.1958,
      "lat": 32.8817
    },
    "North Hollywood": {
      "lng": -118.39028,
      "lat": 34.194444
    },
    "North Kern State Prison Phase II": {
      "lng": -119.30875,
      "lat": 35.780407
    },
    "North Lancaster Ranch": {
      "lng": -118.31301,
      "lat": 34.719538
    },
    "North Midway Cogen": {
      "lng": -119.5986,
      "lat": 35.2761
    },
    "North Palm Springs 1A": {
      "lng": -116.593333,
      "lat": 33.924167
    },
    "North Palm Springs 4A": {
      "lng": -116.5625,
      "lat": 33.909722
    },
    "North Rosamond Solar LLC": {
      "lng": -118.38,
      "lat": 34.862
    },
    "North Sky River Energy LLC": {
      "lng": -118.186389,
      "lat": 35.347778
    },
    "North Star Solar": {
      "lng": -120.41,
      "lat": 36.73
    },
    "Nove Power Plant": {
      "lng": -122.382,
      "lat": 37.9676
    },
    "Nunn": {
      "lng": -117.154444,
      "lat": 34.509167
    },
    "Oak Creek Energy Systems I": {
      "lng": -118.276388,
      "lat": 35.043889
    },
    "Oak Flat": {
      "lng": -121.16143,
      "lat": 40.07531
    },
    "Oakley Solar Project": {
      "lng": -121.747778,
      "lat": 38.001667
    },
    "Oasis Alta": {
      "lng": -118.291944,
      "lat": 35.059166
    },
    "Oasis Wind": {
      "lng": -118.292406,
      "lat": 35.06094
    },
    "Occidental College Solar Project": {
      "lng": -118.206903,
      "lat": 34.1283
    },
    "Ocotillo Express LLC": {
      "lng": -116.044722,
      "lat": 32.752778
    },
    "OCSD": {
      "lng": -117.9409,
      "lat": 33.693129
    },
    "OK Produce Fresno Carport": {
      "lng": -119.774411,
      "lat": 36.723488
    },
    "Olive": {
      "lng": -118.3147,
      "lat": 34.1764
    },
    "Olive View Medical Center": {
      "lng": -118.44477,
      "lat": 34.326242
    },
    "OLS Energy Chino": {
      "lng": -117.680911,
      "lat": 33.989626
    },
    "Olsen": {
      "lng": -121.92805,
      "lat": 40.63722
    },
    "Oltmans SCE at Champagne": {
      "lng": -117.531111,
      "lat": 34.043889
    },
    "Oltmans SCE at Jurupa": {
      "lng": -117.596111,
      "lat": 34.05
    },
    "One Market Plaza": {
      "lng": -122.394167,
      "lat": 37.793333
    },
    "One Ten Partners PV": {
      "lng": -117.936594,
      "lat": 34.615622
    },
    "ONeill": {
      "lng": -121.047718,
      "lat": 37.098671
    },
    "Orange County Energy Storage 2": {
      "lng": -117.854314,
      "lat": 33.715536
    },
    "Orange County Energy Storage 3": {
      "lng": -117.85431,
      "lat": 33.714896
    },
    "Orange Grove Project": {
      "lng": -117.1114,
      "lat": 33.3594
    },
    "Orion Solar I": {
      "lng": -118.875,
      "lat": 35.150694
    },
    "Orion Solar II": {
      "lng": -118.875278,
      "lat": 35.150694
    },
    "Ormesa I": {
      "lng": -115.2568,
      "lat": 32.8156
    },
    "Ormesa II": {
      "lng": -115.2481,
      "lat": 32.7881
    },
    "Ormesa III": {
      "lng": -115.2637,
      "lat": 32.7757
    },
    "Ormond Beach Power, LLC.": {
      "lng": -119.1689,
      "lat": 34.1292
    },
    "Oro Loma": {
      "lng": -120.667608,
      "lat": 36.876847
    },
    "Oroville Cogeneration LP": {
      "lng": -121.5628,
      "lat": 39.492
    },
    "Ortega Highway Energy Storage": {
      "lng": -117.543777,
      "lat": 33.562061
    },
    "Otay Mesa Energy Center, LLC": {
      "lng": -116.914969,
      "lat": 32.581481
    },
    "Otoe Solar Power Generation Station 1 LLC": {
      "lng": -117.191944,
      "lat": 34.545
    },
    "Owens Valley Solar Project 11": {
      "lng": -118.012151,
      "lat": 36.275144
    },
    "Oxbow (CA)": {
      "lng": -120.746651,
      "lat": 39.003372
    },
    "Oxnard": {
      "lng": -119.1667,
      "lat": 34.196
    },
    "Oxnard Paper Mill": {
      "lng": -119.1831,
      "lat": 34.1414
    },
    "Oxnard Wastewater Treatment Plant": {
      "lng": -119.184529,
      "lat": 34.140501
    },
    "P Plant": {
      "lng": -117.198056,
      "lat": 32.894722
    },
    "Pacific Cruise Ship Terminals Berth 93": {
      "lng": -118.27722,
      "lat": 33.7331
    },
    "Pacific Ethanol Madera Solar Array": {
      "lng": -119.97138,
      "lat": 36.916111
    },
    "Pacific Union College BESS": {
      "lng": -122.444028,
      "lat": 38.570028
    },
    "Pacific Wind LLC": {
      "lng": -118.4325,
      "lat": 34.891389
    },
    "Pacific-Ultrapower Chinese Station": {
      "lng": -120.477556,
      "lat": 37.874056
    },
    "Painted Hills Wind Park": {
      "lng": -116.6183,
      "lat": 33.9369
    },
    "Pala Energy Storage Yard": {
      "lng": -117.113599,
      "lat": 33.35651
    },
    "Palo Verde College": {
      "lng": -114.651667,
      "lat": 33.661667
    },
    "Palomar Energy Center": {
      "lng": -117.117778,
      "lat": 33.1197
    },
    "Panoche Energy Center": {
      "lng": -120.5833,
      "lat": 36.6514
    },
    "Panoche Valley Solar Farm": {
      "lng": -120.879,
      "lat": 36.627
    },
    "Paramount Refinery": {
      "lng": -118.151389,
      "lat": 33.9
    },
    "Pardee": {
      "lng": -120.8507,
      "lat": 38.2569
    },
    "Park Meridian #1": {
      "lng": -117.283611,
      "lat": 33.896111
    },
    "Parker": {
      "lng": -120.444492,
      "lat": 37.477838
    },
    "Parker Dam": {
      "lng": -114.140222,
      "lat": 34.295334
    },
    "Parnassus Central Utility Plant": {
      "lng": -122.456747,
      "lat": 37.762524
    },
    "Pastoria Energy Facility": {
      "lng": -118.844,
      "lat": 34.9556
    },
    "PE Berkeley": {
      "lng": -122.26337,
      "lat": 37.870331
    },
    "Pebbly Beach Generating Station Hybrid": {
      "lng": -118.310278,
      "lat": 33.333056
    },
    "Perris": {
      "lng": -117.1831,
      "lat": 33.8361
    },
    "PFMG Solar Grossmont Helix LLC": {
      "lng": -117.035241,
      "lat": 32.753281
    },
    "Phelan Pinon Hills CSD Solar": {
      "lng": -117.58281,
      "lat": 34.57654
    },
    "Phillips 66 Carbon Plant": {
      "lng": -122.2344,
      "lat": 38.018056
    },
    "Phillips 66 Rodeo Refinery": {
      "lng": -122.2583,
      "lat": 38.0417
    },
    "Phoenix": {
      "lng": -120.326537,
      "lat": 38.032103
    },
    "Pierce College": {
      "lng": -118.574722,
      "lat": 34.183611
    },
    "Pilot Knob": {
      "lng": -114.713858,
      "lat": 32.736642
    },
    "Pine Flat": {
      "lng": -119.327,
      "lat": 36.8326
    },
    "Pine Tree Solar Project": {
      "lng": -118.1957,
      "lat": 35.229635
    },
    "Pine Tree Wind Power Project": {
      "lng": -118.175356,
      "lat": 35.248006
    },
    "Pinyon Pine I": {
      "lng": -118.211835,
      "lat": 35.043411
    },
    "Pinyon Pine II": {
      "lng": -118.223333,
      "lat": 35.026759
    },
    "Pio Pico Energy Center LLC": {
      "lng": -116.917777,
      "lat": 32.573889
    },
    "Pit 1": {
      "lng": -121.498243,
      "lat": 40.99084
    },
    "Pit 3": {
      "lng": -121.748139,
      "lat": 40.997448
    },
    "Pit 4": {
      "lng": -121.84944,
      "lat": 40.986361
    },
    "Pit 5": {
      "lng": -121.977594,
      "lat": 40.986197
    },
    "Pit 6": {
      "lng": -121.992906,
      "lat": 40.922568
    },
    "Pit 7": {
      "lng": -121.990798,
      "lat": 40.847462
    },
    "Pixar - Emeryville": {
      "lng": -122.284003,
      "lat": 37.83286
    },
    "Plant No 1 Orange County": {
      "lng": -117.9381,
      "lat": 33.6942
    },
    "Plant No 2 Orange County": {
      "lng": -117.9556,
      "lat": 33.6433
    },
    "Pleasant Valley Hydro": {
      "lng": -118.523889,
      "lat": 37.41416
    },
    "Pleasanton - Amador Valley High School": {
      "lng": -121.874678,
      "lat": 37.662431
    },
    "Poe": {
      "lng": -121.469814,
      "lat": 39.722892
    },
    "Point Wind": {
      "lng": -118.361389,
      "lat": 35.080833
    },
    "POM Beverage Solar": {
      "lng": -119.35792,
      "lat": 36.39026
    },
    "POM Juice Solar": {
      "lng": -119.351133,
      "lat": 36.392479
    },
    "Ponderosa Bailey Creek": {
      "lng": -121.8427,
      "lat": 40.4579
    },
    "Poole": {
      "lng": -119.2161,
      "lat": 37.944237
    },
    "Port of LA Solar FiT Project": {
      "lng": -118.280032,
      "lat": 33.727977
    },
    "Portal": {
      "lng": -119.158936,
      "lat": 37.257045
    },
    "Portal Ridge Solar B, LLC": {
      "lng": -118.2821,
      "lat": 34.71647
    },
    "Portal Ridge Solar C, LLC": {
      "lng": -118.2739,
      "lat": 34.71412
    },
    "Porterville 6 and 7": {
      "lng": -119.02,
      "lat": 36.05
    },
    "Porterville Solar": {
      "lng": -119.039722,
      "lat": 36.102778
    },
    "Potrero Hills Energy Producers": {
      "lng": -121.979452,
      "lat": 38.216973
    },
    "Potter Valley": {
      "lng": -123.127934,
      "lat": 39.361786
    },
    "Power Generation Station (PGS) 2": {
      "lng": -122.296111,
      "lat": 37.825833
    },
    "Powhatan Solar Power Generation Station 1 LLC": {
      "lng": -117.145,
      "lat": 34.496944
    },
    "Prairie": {
      "lng": -118.583575,
      "lat": 34.239244
    },
    "PRC-Desoto Intl/PPG Aerospace": {
      "lng": -118.150556,
      "lat": 34.989722
    },
    "Preferred Freezer San Leandro": {
      "lng": -122.150665,
      "lat": 37.711995
    },
    "Prima Plant": {
      "lng": -117.614587,
      "lat": 33.494925
    },
    "Procter and Gamble Power Plant": {
      "lng": -121.400128,
      "lat": 38.530943
    },
    "Prologis L.P": {
      "lng": -121.529783,
      "lat": 37.725044
    },
    "PSREC/SIAD Solar": {
      "lng": -120.123352,
      "lat": 40.15006
    },
    "Puente Hills Energy Recovery": {
      "lng": -118.0241,
      "lat": 34.0233
    },
    "Pumpjack Solar I": {
      "lng": -119.6225,
      "lat": 35.305556
    },
    "Putah Creek Solar Farm": {
      "lng": -121.993889,
      "lat": 38.521111
    },
    "PVN Milliken, LLC": {
      "lng": -117.565802,
      "lat": 34.038433
    },
    "Q Plant": {
      "lng": -117.201111,
      "lat": 32.904167
    },
    "Quinto Solar PV Project": {
      "lng": -121.052222,
      "lat": 37.130278
    },
    "R C Kirkwood": {
      "lng": -119.953233,
      "lat": 37.877667
    },
    "R E Badger Filtration Plant": {
      "lng": -117.175,
      "lat": 33.051
    },
    "Radiance Solar 4": {
      "lng": -118.288056,
      "lat": 34.665556
    },
    "Radiance Solar 5": {
      "lng": -118.286944,
      "lat": 34.665556
    },
    "Ralston": {
      "lng": -120.725053,
      "lat": 39.000999
    },
    "Ramona 1": {
      "lng": -116.875,
      "lat": 33.012222
    },
    "Ramona 2": {
      "lng": -116.875,
      "lat": 33.012222
    },
    "Ramona Solar Energy": {
      "lng": -116.860713,
      "lat": 33.018486
    },
    "Rancho Cordova Medical Offices": {
      "lng": -121.291035,
      "lat": 38.576571
    },
    "Rancho Cucamonga Dist #1": {
      "lng": -117.552778,
      "lat": 34.100278
    },
    "Rancho Penasquitos": {
      "lng": -117.113669,
      "lat": 32.93695
    },
    "Rancho Seco Solar II, LLC": {
      "lng": -121.119839,
      "lat": 38.346814
    },
    "Rancho Seco Solar, LLC": {
      "lng": -121.114494,
      "lat": 38.339857
    },
    "Ratkovich Alhambra": {
      "lng": -118.15191,
      "lat": 34.08145
    },
    "RCWD PV Project": {
      "lng": -117.1815,
      "lat": 33.5226
    },
    "RE Adams East, LLC": {
      "lng": -120.290833,
      "lat": 36.6175
    },
    "RE Astoria": {
      "lng": -118.458466,
      "lat": 34.844226
    },
    "RE Astoria 2": {
      "lng": -118.480204,
      "lat": 34.836649
    },
    "RE Barren Ridge 1": {
      "lng": -118.06875,
      "lat": 35.20473
    },
    "RE Bruceville 1 LLC": {
      "lng": -121.412777,
      "lat": 38.3475
    },
    "RE Bruceville 2 LLC": {
      "lng": -121.412777,
      "lat": 38.3475
    },
    "RE Bruceville 3 LLC": {
      "lng": -121.412777,
      "lat": 38.3475
    },
    "RE Camelot LLC": {
      "lng": -118.185556,
      "lat": 35.026667
    },
    "RE Columbia 3 LLC": {
      "lng": -118.164167,
      "lat": 35.024722
    },
    "RE Columbia Two, LLC": {
      "lng": -118.185556,
      "lat": 35.026667
    },
    "RE Dillard 1 LLC": {
      "lng": -121.181944,
      "lat": 38.468611
    },
    "RE Dillard 2 LLC": {
      "lng": -121.177778,
      "lat": 38.466111
    },
    "RE Dillard 3 LLC": {
      "lng": -121.180278,
      "lat": 38.461944
    },
    "RE Dillard 4 LLC": {
      "lng": -121.180278,
      "lat": 38.461944
    },
    "RE Garland": {
      "lng": -118.524989,
      "lat": 34.825267
    },
    "RE Garland A": {
      "lng": -118.497406,
      "lat": 34.821453
    },
    "RE Kammerer 1 LLC": {
      "lng": -121.381101,
      "lat": 38.365053
    },
    "RE Kammerer 2 LLC": {
      "lng": -121.381101,
      "lat": 38.365053
    },
    "RE Kammerer 3 LLC": {
      "lng": -121.381101,
      "lat": 38.365053
    },
    "RE Kansas Solar, LLC": {
      "lng": -119.833889,
      "lat": 36.247778
    },
    "RE Kansas South LLC": {
      "lng": -119.834167,
      "lat": 36.226111
    },
    "RE Kent South, LLC": {
      "lng": -119.905278,
      "lat": 36.226111
    },
    "RE McKenzie 1 LLC": {
      "lng": -121.295819,
      "lat": 38.302594
    },
    "RE McKenzie 2 LLC": {
      "lng": -121.295819,
      "lat": 38.302594
    },
    "RE McKenzie 3 LLC": {
      "lng": -121.295819,
      "lat": 38.302594
    },
    "RE McKenzie 4 LLC": {
      "lng": -121.295819,
      "lat": 38.302594
    },
    "RE McKenzie 5 LLC": {
      "lng": -121.295819,
      "lat": 38.302594
    },
    "RE McKenzie 6 LLC": {
      "lng": -121.295819,
      "lat": 38.302594
    },
    "RE Mustang LLC": {
      "lng": -119.903056,
      "lat": 36.221667
    },
    "RE Old River One, LLC": {
      "lng": -119.088889,
      "lat": 35.223333
    },
    "RE Rio Grande Solar LLC": {
      "lng": -118.16451,
      "lat": 35.00745
    },
    "RE Rosamond One LLC": {
      "lng": -118.246944,
      "lat": 34.9
    },
    "RE Rosamond Two LLC": {
      "lng": -118.238333,
      "lat": 34.900278
    },
    "RE Tranquillity": {
      "lng": -120.40447,
      "lat": 36.581365
    },
    "RE Victor Phelan Solar One LLC": {
      "lng": -117.47753,
      "lat": 34.51231
    },
    "Red Bluff": {
      "lng": -122.2124,
      "lat": 40.1501
    },
    "Red Mountain": {
      "lng": -117.1889,
      "lat": 33.3944
    },
    "Redcrest Solar Farm": {
      "lng": -118.812778,
      "lat": 35.321944
    },
    "Redding Power Plant": {
      "lng": -122.423281,
      "lat": 40.507292
    },
    "Redwood 4 Solar Farm": {
      "lng": -118.81192,
      "lat": 35.314198
    },
    "Redwood Coast Airport Microgrid": {
      "lng": -124.104444,
      "lat": 40.967222
    },
    "Reedley Community College Solar": {
      "lng": -119.4577,
      "lat": 36.60563
    },
    "Regional Wastewater Control Facility": {
      "lng": -121.3294,
      "lat": 37.9369
    },
    "Regulus Solar Project": {
      "lng": -118.851944,
      "lat": 35.3
    },
    "Renew Solar ABC Sacramento LLC": {
      "lng": -121.29319,
      "lat": 38.39945
    },
    "RE-VFO LLC": {
      "lng": -119.400823,
      "lat": 36.353675
    },
    "Rialto - High School": {
      "lng": -117.355604,
      "lat": 34.090318
    },
    "Richard J Donovan Correctional Facility": {
      "lng": -116.930907,
      "lat": 32.583571
    },
    "Richmond Cogen": {
      "lng": -122.3909,
      "lat": 37.9418
    },
    "Ridgetop": {
      "lng": -118.3153,
      "lat": 35.074
    },
    "Ridgetop Energy LLC": {
      "lng": -118.3167,
      "lat": 35.0697
    },
    "Rio Bravo Fresno": {
      "lng": -119.723218,
      "lat": 36.688915
    },
    "Rio Bravo Hydro Project": {
      "lng": -118.8222,
      "lat": 35.43
    },
    "Rio Bravo Rocklin": {
      "lng": -121.313652,
      "lat": 38.8319
    },
    "Rio Bravo Solar 1 LLC": {
      "lng": -119.6825,
      "lat": 35.416111
    },
    "Rio Bravo Solar II LLC": {
      "lng": -119.629167,
      "lat": 35.4225
    },
    "Rio Hondo": {
      "lng": -118.09917,
      "lat": 34.004722
    },
    "Ripon Generation Station": {
      "lng": -121.116,
      "lat": 37.7315
    },
    "Rising Tree Wind Farm": {
      "lng": -118.218333,
      "lat": 35.0775
    },
    "Rising Tree Wind Farm II": {
      "lng": -118.206389,
      "lat": 35.088889
    },
    "Rising Tree Wind Farm III": {
      "lng": -118.243333,
      "lat": 35.060556
    },
    "Riverside Energy Resource Center": {
      "lng": -117.4528,
      "lat": 33.9636
    },
    "Riverside RWQCP Fuel Cell": {
      "lng": -117.455897,
      "lat": 33.9617
    },
    "Riverview Energy Center": {
      "lng": -121.790679,
      "lat": 38.014423
    },
    "Roaring Creek Water Power": {
      "lng": -121.948353,
      "lat": 40.882061
    },
    "Robbs Peak": {
      "lng": -120.378292,
      "lat": 38.896511
    },
    "Robert O Schulz Solar Farm": {
      "lng": -120.8917,
      "lat": 37.8583
    },
    "Rock Creek": {
      "lng": -121.345226,
      "lat": 39.90521
    },
    "Rock Creek LP": {
      "lng": -120.778506,
      "lat": 38.78342
    },
    "Rockwood": {
      "lng": -115.5364,
      "lat": 32.9553
    },
    "Rodeo Solar C2": {
      "lng": -118.289444,
      "lat": 34.713333
    },
    "Rodeo Solar D2": {
      "lng": -118.289444,
      "lat": 34.713333
    },
    "Roll Delano": {
      "lng": -119.236667,
      "lat": 35.734167
    },
    "Roll Delano 2": {
      "lng": -119.238056,
      "lat": 35.739722
    },
    "Roll Lost Hills": {
      "lng": -119.889722,
      "lat": 35.653056
    },
    "Rollins": {
      "lng": -120.953341,
      "lat": 39.134259
    },
    "Roseburg Forest Products Biomass": {
      "lng": -122.376944,
      "lat": 41.435556
    },
    "Roseville Energy Park": {
      "lng": -121.3811,
      "lat": 38.7928
    },
    "Roseville Power Plant #2": {
      "lng": -121.323807,
      "lat": 38.810677
    },
    "RPUWD Scheuer Well Solar Project": {
      "lng": -117.27326,
      "lat": 34.11193
    },
    "Rudy Solar, LLC": {
      "lng": -117.323611,
      "lat": 34.766389
    },
    "Rush Creek": {
      "lng": -119.123284,
      "lat": 37.766954
    },
    "Russell City Energy Company LLC": {
      "lng": -122.13381,
      "lat": 37.63466
    },
    "Rutan": {
      "lng": -118.163056,
      "lat": 34.643889
    },
    "Sacramento (SMUD)": {
      "lng": -121.457778,
      "lat": 38.587222
    },
    "Sacramento Fairbain Water Treatment Plant": {
      "lng": -121.416791,
      "lat": 38.554802
    },
    "Sacramento Point West Medical Offices": {
      "lng": -121.428022,
      "lat": 38.596314
    },
    "Sacramento Regional County Sanitation PV": {
      "lng": -121.46369,
      "lat": 38.448604
    },
    "Sacramento Soleil LLC": {
      "lng": -121.16469,
      "lat": 38.44979
    },
    "Saint Agnes Medical Center": {
      "lng": -119.7667,
      "lat": 36.8347
    },
    "Saint Johns Health Center": {
      "lng": -118.47844,
      "lat": 34.030318
    },
    "Salinas River Cogeneration Facility": {
      "lng": -120.8679,
      "lat": 35.9515
    },
    "Salt Springs": {
      "lng": -120.21811,
      "lat": 38.497914
    },
    "Salton Sea Power Gen Co - Unit 2": {
      "lng": -115.6455,
      "lat": 33.1589
    },
    "Salton Sea Power Gen Co - Unit 3": {
      "lng": -115.638656,
      "lat": 33.158153
    },
    "Salton Sea Power Gen Co - Unit 4": {
      "lng": -115.6386,
      "lat": 33.157
    },
    "Salton Sea Power Gen Co Unit 1": {
      "lng": -115.6475,
      "lat": 33.158
    },
    "Salton Sea Power LLC - Unit 5": {
      "lng": -115.6384,
      "lat": 33.1533
    },
    "San Antonio Regional Hospital": {
      "lng": -117.6381,
      "lat": 34.1019
    },
    "San Antonio West Solar Rooftop": {
      "lng": -117.659059,
      "lat": 33.965617
    },
    "San Diego - EMDF at San Diego": {
      "lng": -116.91949,
      "lat": 32.590258
    },
    "San Diego - NCRC at Vista": {
      "lng": -117.255584,
      "lat": 33.190283
    },
    "San Diego International Airport BESS": {
      "lng": -117.1933,
      "lat": 32.7338
    },
    "San Diego State University": {
      "lng": -117.070114,
      "lat": 32.778225
    },
    "San Diego Zoo": {
      "lng": -117.151635,
      "lat": 32.739676
    },
    "San Dimas": {
      "lng": -117.7958,
      "lat": 34.1275
    },
    "San Dimas Wash Generating Station": {
      "lng": -117.805091,
      "lat": 34.124171
    },
    "San Fernando": {
      "lng": -118.492778,
      "lat": 34.312778
    },
    "San Francisquito 1": {
      "lng": -118.454676,
      "lat": 34.590183
    },
    "San Francisquito 2": {
      "lng": -118.525113,
      "lat": 34.534337
    },
    "San Gabriel Hydro Project": {
      "lng": -117.855714,
      "lat": 34.204311
    },
    "San Gorgonio Farms Wind Farm": {
      "lng": -116.615,
      "lat": 33.926111
    },
    "San Gorgonio Westwinds II LLC": {
      "lng": -116.5781,
      "lat": 33.934
    },
    "San Gorgonio Windplant WPP1993": {
      "lng": -116.573906,
      "lat": 33.918395
    },
    "San Joaquin 2": {
      "lng": -119.497508,
      "lat": 37.203233
    },
    "San Joaquin 3": {
      "lng": -119.519352,
      "lat": 37.253718
    },
    "San Joaquin Solar": {
      "lng": -120.149,
      "lat": 36.527
    },
    "San Jose Cogeneration": {
      "lng": -121.878332,
      "lat": 37.336113
    },
    "San Pablo Raceway": {
      "lng": -118.280345,
      "lat": 34.723748
    },
    "Sanborn BESS 1": {
      "lng": -118.139236,
      "lat": 35.027264
    },
    "Sanborn BESS 2": {
      "lng": -118.140078,
      "lat": 35.027264
    },
    "Sand Bar Power Plant": {
      "lng": -120.15,
      "lat": 38.1861
    },
    "Sand Drag LLC": {
      "lng": -120.110277,
      "lat": 35.982778
    },
    "Santa Ana 1": {
      "lng": -117.058481,
      "lat": 34.14522
    },
    "Santa Ana 3": {
      "lng": -117.099091,
      "lat": 34.107245
    },
    "Santa Clara Cogen": {
      "lng": -121.944238,
      "lat": 37.364297
    },
    "Santa Cruz Energy": {
      "lng": -122.105556,
      "lat": 36.973333
    },
    "Santa Fe Springs Rooftop Solar BLDG H": {
      "lng": -118.044444,
      "lat": 33.910833
    },
    "Santa Fe Springs Rooftop Solar BLDG M": {
      "lng": -118.044444,
      "lat": 33.910833
    },
    "Santa Felicia Dam": {
      "lng": -118.7511,
      "lat": 34.461667
    },
    "Santa Maria EPG": {
      "lng": -120.636667,
      "lat": 35.040833
    },
    "Santa Maria LFG Power Plant": {
      "lng": -120.4142,
      "lat": 34.9537
    },
    "Santa Rita Jail Fuel Cell": {
      "lng": -121.886944,
      "lat": 37.719166
    },
    "Santa Rita Jail Hybrid": {
      "lng": -121.886944,
      "lat": 37.719167
    },
    "Santa Rosa Junior College Petaluma Solar": {
      "lng": -122.72188,
      "lat": 38.46905
    },
    "Sargent Canyon Cogen Facility": {
      "lng": -120.8408,
      "lat": 35.9359
    },
    "Saticoy": {
      "lng": -119.15726,
      "lat": 34.256691
    },
    "SB Water Reclamation Fuel Cell": {
      "lng": -117.287339,
      "lat": 34.075594
    },
    "SC 1 Data Center, Phase 2": {
      "lng": -121.943611,
      "lat": 37.361667
    },
    "Scattergood Generating Station": {
      "lng": -118.427648,
      "lat": 33.918151
    },
    "SCC - San Jose": {
      "lng": -121.90551,
      "lat": 37.35106
    },
    "SCCCD - Clovis Community College": {
      "lng": -119.731899,
      "lat": 36.885463
    },
    "SCCCD - Fresno Community College": {
      "lng": -119.79858,
      "lat": 36.76655
    },
    "SCDA Solar 1": {
      "lng": -121.586514,
      "lat": 38.690186
    },
    "SCE-Snowline-Duncan Road (North)": {
      "lng": -117.562778,
      "lat": 34.473889
    },
    "SCE-Snowline-Duncan Road (South)": {
      "lng": -117.562222,
      "lat": 34.471389
    },
    "SCE-Snowline-White Rd (Central)": {
      "lng": -117.47,
      "lat": 34.415278
    },
    "SCE-Snowline-White Road (North)": {
      "lng": -117.469722,
      "lat": 34.4175
    },
    "SCE-Snowline-White Road (South)": {
      "lng": -117.470833,
      "lat": 34.413056
    },
    "Scott Flat": {
      "lng": -120.932508,
      "lat": 39.272108
    },
    "SCU000 El Camino Real Fuel Cell": {
      "lng": -121.94064,
      "lat": 37.34994
    },
    "SDCCD - Miramar": {
      "lng": -117.121389,
      "lat": 32.906111
    },
    "SDCWA - Twin Oaks": {
      "lng": -117.170556,
      "lat": 33.21
    },
    "SE Athos II, LLC": {
      "lng": -115.35499,
      "lat": 33.751283
    },
    "SeaWorld Aquatica": {
      "lng": -117.004884,
      "lat": 32.586536
    },
    "Second Imperial Geothermal": {
      "lng": -115.5356,
      "lat": 32.7144
    },
    "SEGS III": {
      "lng": -117.555768,
      "lat": 35.00694
    },
    "SEGS IV": {
      "lng": -117.555672,
      "lat": 35.012166
    },
    "SEGS IX": {
      "lng": -117.338,
      "lat": 35.033
    },
    "SEGS V": {
      "lng": -117.55553,
      "lat": 35.01948
    },
    "SEGS VI": {
      "lng": -117.566079,
      "lat": 35.020981
    },
    "SEGS VII": {
      "lng": -117.565903,
      "lat": 35.014836
    },
    "Sentinel Energy Center, LLC": {
      "lng": -116.571388,
      "lat": 33.934167
    },
    "Sepulveda Canyon": {
      "lng": -118.4795,
      "lat": 34.0969
    },
    "SEPV 1": {
      "lng": -117.939444,
      "lat": 34.618861
    },
    "SEPV 18": {
      "lng": -117.93439,
      "lat": 34.613602
    },
    "SEPV 2": {
      "lng": -116.152778,
      "lat": 34.174167
    },
    "SEPV 8": {
      "lng": -116.1525,
      "lat": 34.171944
    },
    "SEPV Imperial Dixieland East": {
      "lng": -115.773448,
      "lat": 32.792038
    },
    "SEPV Imperial Dixieland West": {
      "lng": -115.779317,
      "lat": 32.79411
    },
    "SEPV Mojave West": {
      "lng": -118.282694,
      "lat": 35.022903
    },
    "SEPV Palmdale East": {
      "lng": -117.938889,
      "lat": 34.613056
    },
    "SEPV Sierra": {
      "lng": -118.138525,
      "lat": 34.733011
    },
    "SEPV9 Power Plant": {
      "lng": -116.090278,
      "lat": 34.192222
    },
    "Seville 1": {
      "lng": -116.0139,
      "lat": 33.1125
    },
    "Seville 2": {
      "lng": -116.0059,
      "lat": 33.1101
    },
    "SF Southeast Cogen Plant": {
      "lng": -122.3925,
      "lat": 37.739722
    },
    "SF State University": {
      "lng": -122.4775,
      "lat": 37.723889
    },
    "SFDK Solar": {
      "lng": -122.231965,
      "lat": 38.132245
    },
    "Shafter Solar LLC": {
      "lng": -119.127778,
      "lat": 35.393333
    },
    "Shasta": {
      "lng": -122.422274,
      "lat": 40.717455
    },
    "Shasta Solar Farm": {
      "lng": -121.4225,
      "lat": 41.031389
    },
    "Shelter Creek Condominiums Solar": {
      "lng": -122.4289,
      "lat": 37.61909
    },
    "Shiloh I Wind Project": {
      "lng": -121.85,
      "lat": 38.12
    },
    "Shiloh III Wind Project LLC": {
      "lng": -121.846944,
      "lat": 38.169444
    },
    "Shiloh IV Wind Project LLC": {
      "lng": -121.855342,
      "lat": 38.137488
    },
    "Shiloh Wind Project 2 LLC": {
      "lng": -121.8461,
      "lat": 38.1711
    },
    "Sierra Nevada Brewing Co Hybrid": {
      "lng": -121.815278,
      "lat": 39.724167
    },
    "Sierra Pacific Burney Facility": {
      "lng": -121.7016,
      "lat": 40.8767
    },
    "Sierra Pacific Industries (2042-RD)": {
      "lng": -122.191944,
      "lat": 40.121389
    },
    "Sierra Pacific Lincoln Facility": {
      "lng": -121.3097,
      "lat": 38.9032
    },
    "Sierra Pacific Quincy Facility": {
      "lng": -120.909211,
      "lat": 39.941165
    },
    "Sierra Pacific Sonora": {
      "lng": -120.317777,
      "lat": 37.966389
    },
    "Sierra Solar Greenworks": {
      "lng": -118.334167,
      "lat": 34.703611
    },
    "Signal Hill West Unit": {
      "lng": -118.175645,
      "lat": 33.811182
    },
    "Silicon Valley Clean Water": {
      "lng": -122.231119,
      "lat": 37.542619
    },
    "Silveira Ranch Road Solar": {
      "lng": -122.56565,
      "lat": 38.153474
    },
    "Silverstrand Grid Energy Storage System": {
      "lng": -119.079813,
      "lat": 34.20774
    },
    "Siphon Drop Power Plant": {
      "lng": -114.6344,
      "lat": 32.7792
    },
    "Site 980 65": {
      "lng": -119.899706,
      "lat": 37.056803
    },
    "SJ/SC WPCP": {
      "lng": -121.9464,
      "lat": 37.4344
    },
    "Sky River Wind Energy Center": {
      "lng": -118.2469,
      "lat": 35.2608
    },
    "Slate Creek": {
      "lng": -122.456251,
      "lat": 40.976859
    },
    "Slate Hybrid": {
      "lng": -119.8948,
      "lat": 36.226955
    },
    "Sly Creek": {
      "lng": -121.119479,
      "lat": 39.580413
    },
    "SMUD at Fleshman": {
      "lng": -121.3375,
      "lat": 38.237778
    },
    "SMUD at Grundman": {
      "lng": -121.41278,
      "lat": 38.364722
    },
    "SMUD at Lawrence": {
      "lng": -121.431342,
      "lat": 38.338868
    },
    "SMUD at Van Conett": {
      "lng": -121.278888,
      "lat": 38.253056
    },
    "Soboba Community Solar Project": {
      "lng": -116.905874,
      "lat": 33.767061
    },
    "SOCCD": {
      "lng": -117.777312,
      "lat": 33.676445
    },
    "Sol Orchard El Centro PV": {
      "lng": -115.546389,
      "lat": 32.803333
    },
    "Solano County Cogen Plant": {
      "lng": -122.0394,
      "lat": 38.2494
    },
    "Solano Wind": {
      "lng": -121.769031,
      "lat": 38.116415
    },
    "Solar Blythe 2": {
      "lng": -114.696182,
      "lat": 33.618678
    },
    "Solar Blythe LLC": {
      "lng": -114.7444,
      "lat": 33.5917
    },
    "Solar Borrego I": {
      "lng": -116.35083,
      "lat": 33.298889
    },
    "Solar Gen 2 Solar Facility": {
      "lng": -115.47,
      "lat": 33.08
    },
    "Solar Oasis LLC": {
      "lng": -118.076667,
      "lat": 34.648889
    },
    "Solar Photovoltaic Project #02": {
      "lng": -117.700826,
      "lat": 33.985244
    },
    "Solar Photovoltaic Project #03": {
      "lng": -117.400113,
      "lat": 34.094279
    },
    "Solar Photovoltaic Project #05": {
      "lng": -117.230571,
      "lat": 34.086276
    },
    "Solar Photovoltaic Project #06": {
      "lng": -117.573055,
      "lat": 34.041389
    },
    "Solar Photovoltaic Project #07": {
      "lng": -117.2275,
      "lat": 34.086111
    },
    "Solar Photovoltaic Project #08": {
      "lng": -117.565,
      "lat": 34.041944
    },
    "Solar Photovoltaic Project #09": {
      "lng": -117.571666,
      "lat": 34.042778
    },
    "Solar Photovoltaic Project #10": {
      "lng": -117.516944,
      "lat": 34.075
    },
    "Solar Photovoltaic Project #11": {
      "lng": -117.241388,
      "lat": 34.075833
    },
    "Solar Photovoltaic Project #12": {
      "lng": -117.561666,
      "lat": 34.042222
    },
    "Solar Photovoltaic Project #13": {
      "lng": -117.23557,
      "lat": 34.083499
    },
    "Solar Photovoltaic Project #15": {
      "lng": -117.516666,
      "lat": 34.079167
    },
    "Solar Photovoltaic Project #16": {
      "lng": -117.233611,
      "lat": 34.085556
    },
    "Solar Photovoltaic Project #17": {
      "lng": -117.503333,
      "lat": 34.072778
    },
    "Solar Photovoltaic Project #18": {
      "lng": -117.516388,
      "lat": 34.083333
    },
    "Solar Photovoltaic Project #22": {
      "lng": -117.2114,
      "lat": 34.08
    },
    "Solar Photovoltaic Project #23": {
      "lng": -117.520277,
      "lat": 34.080833
    },
    "Solar Photovoltaic Project #26": {
      "lng": -117.425324,
      "lat": 34.131214
    },
    "Solar Photovoltaic Project #27": {
      "lng": -117.421666,
      "lat": 34.131111
    },
    "Solar Photovoltaic Project #28": {
      "lng": -117.274166,
      "lat": 34.094167
    },
    "Solar Photovoltaic Project #32": {
      "lng": -117.571944,
      "lat": 34.031667
    },
    "Solar Photovoltaic Project #33": {
      "lng": -117.569166,
      "lat": 34.0325
    },
    "Solar Photovoltaic Project #42": {
      "lng": -119.074722,
      "lat": 36.028056
    },
    "Solar Photovoltaic Project #48": {
      "lng": -117.228056,
      "lat": 34.081111
    },
    "Solar Star 1": {
      "lng": -118.4036,
      "lat": 34.8181
    },
    "Solar Star 2": {
      "lng": -118.352778,
      "lat": 34.848611
    },
    "Solar Star California II LLC": {
      "lng": -121.47528,
      "lat": 38.516389
    },
    "Solar Star California, XLI, LLC": {
      "lng": -118.386843,
      "lat": 34.839012
    },
    "Solar Star California, XLIV, LLC": {
      "lng": -117.922819,
      "lat": 34.511889
    },
    "Solar Star Palo Alto I, LLC": {
      "lng": -122.148864,
      "lat": 37.411011
    },
    "Solverde 1": {
      "lng": -118.298166,
      "lat": 34.655694
    },
    "Sonoma California Geothermal": {
      "lng": -122.7559,
      "lat": 38.7903
    },
    "Sonoma Central Landfill Phase I": {
      "lng": -122.7489,
      "lat": 38.3017
    },
    "Sonoma Central Landfill Phase II": {
      "lng": -122.7489,
      "lat": 38.3017
    },
    "Sonoma Central Landfill Phase III": {
      "lng": -122.7489,
      "lat": 38.3017
    },
    "Sonora 1": {
      "lng": -120.528723,
      "lat": 37.889753
    },
    "Soscol Ferry Solar": {
      "lng": -122.275184,
      "lat": 38.237422
    },
    "South": {
      "lng": -121.879186,
      "lat": 40.395396
    },
    "South Bay Fuel Cell Plant": {
      "lng": -117.067778,
      "lat": 32.543333
    },
    "South Belridge Cogeneration Facility": {
      "lng": -119.7075,
      "lat": 35.438611
    },
    "Southeast Kern River Cogen": {
      "lng": -118.9644,
      "lat": 35.4206
    },
    "Southeast Resource Recovery": {
      "lng": -118.239922,
      "lat": 33.759236
    },
    "Spaulding 1": {
      "lng": -120.644807,
      "lat": 39.324613
    },
    "Spaulding 2": {
      "lng": -120.645375,
      "lat": 39.324592
    },
    "Spaulding 3": {
      "lng": -120.635467,
      "lat": 39.337679
    },
    "SPI Anderson 2": {
      "lng": -122.324205,
      "lat": 40.472283
    },
    "Spicer Meadow Project": {
      "lng": -120.0008,
      "lat": 38.3914
    },
    "Spreckels Sugar Company": {
      "lng": -115.568056,
      "lat": 32.911111
    },
    "Spring Creek": {
      "lng": -122.467509,
      "lat": 40.628115
    },
    "Spring Gap": {
      "lng": -120.11168,
      "lat": 38.18653
    },
    "Springbok 3 Solar Farm Hybrid": {
      "lng": -118.001652,
      "lat": 35.171153
    },
    "Springbok Solar Farm 1": {
      "lng": -117.955278,
      "lat": 35.249167
    },
    "Springbok Solar Farm 2": {
      "lng": -117.976847,
      "lat": 35.250031
    },
    "Springs Generating Station": {
      "lng": -117.2933,
      "lat": 33.9308
    },
    "Springville Hydroelectric": {
      "lng": -119.0858,
      "lat": 34.2253
    },
    "SRI International Cogen Project": {
      "lng": -122.172797,
      "lat": 37.455314
    },
    "SSI West Riverside Landfill LLC": {
      "lng": -117.375494,
      "lat": 33.980601
    },
    "Stage Gulch Solar": {
      "lng": -122.542866,
      "lat": 38.216841
    },
    "Stampede": {
      "lng": -120.10455,
      "lat": 39.470447
    },
    "Stanford Campus Solar": {
      "lng": -122.16077,
      "lat": 37.43434
    },
    "Stanislaus": {
      "lng": -120.370325,
      "lat": 38.139048
    },
    "Stanton Energy Reliability Center": {
      "lng": -117.985758,
      "lat": 33.806801
    },
    "Staples La Mirada, CA": {
      "lng": -118.0175,
      "lat": 33.881667
    },
    "Starbucks - Evolution Fresh": {
      "lng": -117.551044,
      "lat": 34.093684
    },
    "Stateline Solar": {
      "lng": -115.4,
      "lat": 35.6
    },
    "Stenner Creek Solar": {
      "lng": -120.691,
      "lat": 35.32
    },
    "Still Water Power": {
      "lng": -119.524252,
      "lat": 36.398796
    },
    "Stony Gorge": {
      "lng": -122.533248,
      "lat": 39.586864
    },
    "Strata Roof 1": {
      "lng": -121.505506,
      "lat": 38.643527
    },
    "Stroud Solar Station": {
      "lng": -120.106944,
      "lat": 36.531389
    },
    "Summer North Solar": {
      "lng": -118.289722,
      "lat": 34.723056
    },
    "Summer Solar A2": {
      "lng": -118.289722,
      "lat": 34.723056
    },
    "Summer Solar B2": {
      "lng": -118.289722,
      "lat": 34.723056
    },
    "Summer Solar C2": {
      "lng": -118.289722,
      "lat": 34.723056
    },
    "Summer Solar D2": {
      "lng": -118.289722,
      "lat": 34.723056
    },
    "Summer Solar LLC": {
      "lng": -118.311006,
      "lat": 34.714703
    },
    "Summit Winds": {
      "lng": -121.69067,
      "lat": 37.761151
    },
    "Sun City Project LLC": {
      "lng": -120.110277,
      "lat": 35.983056
    },
    "Sun Harvest Solar NDP1": {
      "lng": -120.653625,
      "lat": 37.05758
    },
    "SunAnza": {
      "lng": -116.639647,
      "lat": 33.555267
    },
    "SunE- E Philadelphia Ontario": {
      "lng": -117.5341,
      "lat": 34.03527
    },
    "SunE Rochester": {
      "lng": -117.544529,
      "lat": 34.097728
    },
    "SunEdison Anheuser Busch Fairfield": {
      "lng": -122.092706,
      "lat": 38.232108
    },
    "SunEdison Ironwood State Prison": {
      "lng": -114.917979,
      "lat": 33.567268
    },
    "SunEdison Procter & Gamble Oxnard": {
      "lng": -119.13,
      "lat": 34.2089
    },
    "SunEdison Walgreens Moreno Valley": {
      "lng": -117.23,
      "lat": 33.8672
    },
    "SunEdison Walmart Apple Valley DC": {
      "lng": -117.2,
      "lat": 34.5975
    },
    "Sunnyvale City of WPCP": {
      "lng": -122.0153,
      "lat": 37.4194
    },
    "Sunray 2": {
      "lng": -116.827627,
      "lat": 34.863356
    },
    "Sunray 3": {
      "lng": -116.827627,
      "lat": 34.863356
    },
    "Sunrise Power Company": {
      "lng": -119.585,
      "lat": 35.2097
    },
    "SunSelect1": {
      "lng": -118.602184,
      "lat": 35.096709
    },
    "Sunset Reservoir North Basin": {
      "lng": -122.481758,
      "lat": 37.750894
    },
    "Sunshine Gas Producers": {
      "lng": -118.519188,
      "lat": 34.335678
    },
    "Sycamore Cogeneration": {
      "lng": -118.985313,
      "lat": 35.452577
    },
    "Sycamore Energy LLC": {
      "lng": -117.028847,
      "lat": 32.857709
    },
    "TA-Acacia, LLC": {
      "lng": -118.324722,
      "lat": 34.689167
    },
    "Taft 26C Cogen": {
      "lng": -119.4706,
      "lat": 35.1133
    },
    "TA-High Desert LLC": {
      "lng": -118.304722,
      "lat": 34.708889
    },
    "Tahquitz High School": {
      "lng": -117.0175,
      "lat": 33.767222
    },
    "Target Woodland Solar Project": {
      "lng": -121.723451,
      "lat": 38.682746
    },
    "Taylor Farms": {
      "lng": -121.451667,
      "lat": 36.505
    },
    "Taylor Farms Salinas": {
      "lng": -121.623333,
      "lat": 36.6575
    },
    "Tehachapi Energy Storage Project": {
      "lng": -118.38,
      "lat": 35.123333
    },
    "Tehachapi Wind Resource I": {
      "lng": -118.389722,
      "lat": 35.060556
    },
    "Tehachapi Wind Resource II": {
      "lng": -118.3114,
      "lat": 35.1061
    },
    "Teichert Materials-Teichert Vernalis": {
      "lng": -121.3564,
      "lat": 37.6167
    },
    "Temescal": {
      "lng": -117.4971,
      "lat": 33.8343
    },
    "Temescal Canyon RV, LLC": {
      "lng": -117.511667,
      "lat": 33.826389
    },
    "Tequesquite Landfill Solar PV Project": {
      "lng": -117.38514,
      "lat": 33.97819
    },
    "Terminus Hydroelectric Project": {
      "lng": -119.0069,
      "lat": 36.4156
    },
    "Terra Francesco": {
      "lng": -117.606667,
      "lat": 34.039722
    },
    "Terra-Gen VG Wind LLC": {
      "lng": -118.3789,
      "lat": 35.1161
    },
    "Tesoro Wilmington Calciner": {
      "lng": -118.22821,
      "lat": 33.77697
    },
    "Thermalito": {
      "lng": -121.630077,
      "lat": 39.514893
    },
    "Thermalito Diverson Dam": {
      "lng": -121.546359,
      "lat": 39.528749
    },
    "Thomas M Knott Cogen Facility": {
      "lng": -120.618333,
      "lat": 35.184444
    },
    "Three Forks Water Power Project": {
      "lng": -123.523145,
      "lat": 40.199822
    },
    "THUMS": {
      "lng": -118.2141,
      "lat": 33.7684
    },
    "Tiger Creek": {
      "lng": -120.492645,
      "lat": 38.449291
    },
    "Titan Solar 1 (CA)": {
      "lng": -116.010024,
      "lat": 33.101555
    },
    "TLR000 Hansen Fuel Cell": {
      "lng": -121.62428,
      "lat": 36.65676
    },
    "Toadtown": {
      "lng": -121.59338,
      "lat": 39.893491
    },
    "Top Gun Energy Storage": {
      "lng": -117.167791,
      "lat": 32.876198
    },
    "Topaz Solar Farm": {
      "lng": -120.068611,
      "lat": 35.405556
    },
    "Torrance Refining Company, LLC": {
      "lng": -118.33,
      "lat": 33.85336
    },
    "Total Energy Facilities": {
      "lng": -118.2836,
      "lat": 33.7683
    },
    "TPC Windfarms LLC": {
      "lng": -118.2572,
      "lat": 35.05
    },
    "Tracy 210": {
      "lng": -121.53097,
      "lat": 37.72201
    },
    "Tracy Combined Cycle Power Plant": {
      "lng": -121.4906,
      "lat": 37.7107
    },
    "Tranquillity ID": {
      "lng": -120.24594,
      "lat": 36.648026
    },
    "Transamerica Pyramid": {
      "lng": -122.402778,
      "lat": 37.795556
    },
    "Trinity": {
      "lng": -122.762553,
      "lat": 40.797296
    },
    "Tropico Solar PV Plant": {
      "lng": -119.056111,
      "lat": 35.831944
    },
    "Tulare 1 and 2": {
      "lng": -119.42,
      "lat": 36.2
    },
    "Tulare BioMAT Fuel Cell": {
      "lng": -119.375097,
      "lat": 36.187138
    },
    "Tulare Success Power Project": {
      "lng": -118.924097,
      "lat": 36.058635
    },
    "Tule River": {
      "lng": -118.707104,
      "lat": 36.163076
    },
    "Tule Wind LLC": {
      "lng": -116.289722,
      "lat": 32.663889
    },
    "Tulloch": {
      "lng": -120.605,
      "lat": 37.876111
    },
    "Turlock Lake": {
      "lng": -120.594678,
      "lat": 37.611461
    },
    "UC Davis South Campus": {
      "lng": -121.75,
      "lat": 38.55
    },
    "UC Merced Solar Hybrid": {
      "lng": -120.432228,
      "lat": 37.365688
    },
    "UC Riverside Lots 30 & 32": {
      "lng": -117.33099,
      "lat": 33.97036
    },
    "UC Santa Cruz Solar": {
      "lng": -122.0532,
      "lat": 36.9911
    },
    "UCI Facilities Management Central Plant": {
      "lng": -117.8467,
      "lat": 33.6481
    },
    "UCI Fuel Cell": {
      "lng": -117.8901,
      "lat": 33.788
    },
    "UCLA So Campus Cogen Project": {
      "lng": -118.4439,
      "lat": 34.0692
    },
    "UCSD Fuel Cell Plant": {
      "lng": -117.234722,
      "lat": 32.877222
    },
    "Union Valley": {
      "lng": -120.444335,
      "lat": 38.86429
    },
    "Univ of Calif Santa Cruz Cogeneration": {
      "lng": -122.0622,
      "lat": 36.9997
    },
    "Univ of California San Diego Solar": {
      "lng": -117.238927,
      "lat": 32.874379
    },
    "Univ of San Francisco Cogen": {
      "lng": -122.451878,
      "lat": 37.77656
    },
    "University of California San Diego Hybrid": {
      "lng": -117.2392,
      "lat": 32.8747
    },
    "University of San Diego": {
      "lng": -117.191589,
      "lat": 32.771701
    },
    "Upper Dawson": {
      "lng": -120.4706,
      "lat": 37.6508
    },
    "Upper Gorge": {
      "lng": -118.590001,
      "lat": 37.546036
    },
    "US Borax": {
      "lng": -117.701,
      "lat": 35.0329
    },
    "US GSA - Sacramento": {
      "lng": -121.397929,
      "lat": 38.601354
    },
    "USG 1": {
      "lng": -115.857336,
      "lat": 32.79278
    },
    "USG 2": {
      "lng": -115.863214,
      "lat": 32.790403
    },
    "USPS PV": {
      "lng": -118.2587,
      "lat": 33.977922
    },
    "US-TOPCO (Soccer Center)": {
      "lng": -118.081152,
      "lat": 34.66072
    },
    "VA Sepulveda Ambulatory Care Center": {
      "lng": -118.483333,
      "lat": 34.248889
    },
    "Vaca Dixon Battery Storage System": {
      "lng": -121.927778,
      "lat": 38.393333
    },
    "Vaca Dixon Solar Station": {
      "lng": -121.9264,
      "lat": 38.4106
    },
    "Vacaville": {
      "lng": -121.960278,
      "lat": 38.399444
    },
    "Vacaville Hospital": {
      "lng": -121.93864,
      "lat": 38.38867
    },
    "Valencia 1 Solar CA": {
      "lng": -115.62779,
      "lat": 33.04358
    },
    "Valencia 2": {
      "lng": -115.52641,
      "lat": 33.0218
    },
    "Valencia 3": {
      "lng": -115.53765,
      "lat": 32.88567
    },
    "Valentine Solar, LLC": {
      "lng": -118.384341,
      "lat": 34.90422
    },
    "Valero Refinery Cogeneration Unit 1": {
      "lng": -122.1411,
      "lat": 38.0736
    },
    "Valero Wilmington Cogeneration Plant": {
      "lng": -118.23853,
      "lat": 33.778655
    },
    "Vallecito": {
      "lng": -119.511547,
      "lat": 34.407086
    },
    "Vallecitos Water District": {
      "lng": -117.177029,
      "lat": 33.212535
    },
    "Valley Center": {
      "lng": -117.0226,
      "lat": 33.2503
    },
    "Valley Center 1": {
      "lng": -117.001667,
      "lat": 33.234167
    },
    "Valley Center 2": {
      "lng": -117.001667,
      "lat": 33.234167
    },
    "Valley Generating Station": {
      "lng": -118.391327,
      "lat": 34.244991
    },
    "Valley Sanitary District WTP Solar": {
      "lng": -116.198885,
      "lat": 33.708661
    },
    "Valley View": {
      "lng": -117.82905,
      "lat": 33.9001
    },
    "Van der Hoek Solar Array/ Dairy": {
      "lng": -120.098148,
      "lat": 36.504015
    },
    "Van der Kooi Dairy Solary Array": {
      "lng": -120.022439,
      "lat": 36.29169
    },
    "Vanadium Redox Flow Battery Plant": {
      "lng": -116.988781,
      "lat": 32.682506
    },
    "Vandenberg Solar Project": {
      "lng": -120.509982,
      "lat": 34.749708
    },
    "Vasco Winds": {
      "lng": -121.718055,
      "lat": 37.790833
    },
    "Vega Solar": {
      "lng": -120.82,
      "lat": 36.925833
    },
    "Venable Solar 1": {
      "lng": -114.571667,
      "lat": 33.598889
    },
    "Venable Solar 2": {
      "lng": -114.571667,
      "lat": 33.597222
    },
    "Venice Hydro": {
      "lng": -118.4168,
      "lat": 34.01135
    },
    "Ventura Solar": {
      "lng": -118.901987,
      "lat": 34.39919
    },
    "Verizon-Torrance": {
      "lng": -118.304167,
      "lat": 33.850556
    },
    "Vernon": {
      "lng": -118.2219,
      "lat": 33.9986
    },
    "Verwey-Hanford Dairy Digester #1": {
      "lng": -119.681495,
      "lat": 36.18519
    },
    "Verwey-Hanford Dairy Digester #2": {
      "lng": -119.681495,
      "lat": 36.18519
    },
    "Verwey-Hanford Dairy Digester #3": {
      "lng": -119.681495,
      "lat": 36.18519
    },
    "VESI Pomona": {
      "lng": -117.774167,
      "lat": 34.059444
    },
    "Victor Dry Farm Ranch A": {
      "lng": -117.466667,
      "lat": 34.501111
    },
    "Victor Dry Farm Ranch B": {
      "lng": -117.471111,
      "lat": 34.497778
    },
    "Victor Mesa Linda B2": {
      "lng": -117.387222,
      "lat": 34.512778
    },
    "Victor Mesa Linda C2": {
      "lng": -117.387222,
      "lat": 34.511389
    },
    "Victor Mesa Linda D2": {
      "lng": -117.387222,
      "lat": 34.509444
    },
    "Victor Mesa Linda E2": {
      "lng": -117.387222,
      "lat": 34.508056
    },
    "Victor Valley CC CPV Solar": {
      "lng": -117.257938,
      "lat": 34.48017
    },
    "Victory Garden (Tehachapi)": {
      "lng": -118.3833,
      "lat": 35.0686
    },
    "Vinam": {
      "lng": -118.061389,
      "lat": 34.668889
    },
    "Vintner Solar": {
      "lng": -120.68287,
      "lat": 35.55795
    },
    "Vista Energy Storage System": {
      "lng": -117.253852,
      "lat": 33.203977
    },
    "Vitro Flat Glass LLC": {
      "lng": -119.72,
      "lat": 36.69
    },
    "Volta 1": {
      "lng": -121.86708,
      "lat": 40.459111
    },
    "Volta 2": {
      "lng": -121.86215,
      "lat": 40.45193
    },
    "Voyager Wind I": {
      "lng": -118.28273,
      "lat": 35.046061
    },
    "Voyager Wind II": {
      "lng": -118.268616,
      "lat": 35.073377
    },
    "Voyager Wind III": {
      "lng": -118.264734,
      "lat": 35.060768
    },
    "Voyager Wind IV": {
      "lng": -118.2497,
      "lat": 35.077872
    },
    "VS LADWPGLP Francisco, LLC": {
      "lng": -118.304,
      "lat": 33.849
    },
    "Vulcan-BN Geothermal Power Company": {
      "lng": -115.6167,
      "lat": 33.1633
    },
    "W E Warne": {
      "lng": -118.7881,
      "lat": 34.6853
    },
    "W Plant": {
      "lng": -117.195556,
      "lat": 32.903333
    },
    "W R Gianelli": {
      "lng": -121.0772,
      "lat": 37.0692
    },
    "Wadham Energy LP": {
      "lng": -122.1096,
      "lat": 39.1062
    },
    "Wagner Wind LLC": {
      "lng": -116.555556,
      "lat": 33.908889
    },
    "Walgreens Woodland Distribution Center": {
      "lng": -121.713362,
      "lat": 38.674894
    },
    "Walnut": {
      "lng": -120.9044,
      "lat": 37.4903
    },
    "Walnut Creek Energy Park": {
      "lng": -117.944864,
      "lat": 34.008709
    },
    "Walnut Energy Center": {
      "lng": -120.8956,
      "lat": 37.4878
    },
    "Walnut Unified School District Walnut HS Hybrid": {
      "lng": -117.85051,
      "lat": 34.02188
    },
    "Warm Springs Hydro Project": {
      "lng": -123.011188,
      "lat": 38.722141
    },
    "Waste Mangement Redwood LFGTE": {
      "lng": -122.565,
      "lat": 38.160833
    },
    "Watkins Manufacturing Co.": {
      "lng": -117.236908,
      "lat": 33.143847
    },
    "Watson Cogeneration": {
      "lng": -118.244841,
      "lat": 33.816647
    },
    "Watts 3115": {
      "lng": -117.642222,
      "lat": 34.600833
    },
    "Welport Lease Project": {
      "lng": -119.6617,
      "lat": 35.354444
    },
    "West Gates Solar Station": {
      "lng": -120.1325,
      "lat": 36.142222
    },
    "West Point PH": {
      "lng": -120.549572,
      "lat": 38.420898
    },
    "West Valley High School Solar": {
      "lng": -117.009722,
      "lat": 33.719722
    },
    "Westend Facility": {
      "lng": -117.3956,
      "lat": 35.7075
    },
    "Western Antelope Blue Sky B": {
      "lng": -118.313573,
      "lat": 34.73206
    },
    "Western Antelope Blue Sky Ranch A": {
      "lng": -118.324722,
      "lat": 34.681667
    },
    "Western Antelope Dry Ranch": {
      "lng": -118.320278,
      "lat": 34.688889
    },
    "Western Power & Steam Inc": {
      "lng": -119.012861,
      "lat": 35.440539
    },
    "Westlands Solar PV Farm": {
      "lng": -120.075833,
      "lat": 36.138056
    },
    "Westmont 300A": {
      "lng": -118.288925,
      "lat": 33.766067
    },
    "Westmont 300B": {
      "lng": -118.284733,
      "lat": 33.766067
    },
    "Westmont 301": {
      "lng": -118.286558,
      "lat": 33.763012
    },
    "Westmont 400A": {
      "lng": -118.288925,
      "lat": 33.764227
    },
    "Westmont 400B": {
      "lng": -118.284733,
      "lat": 33.764227
    },
    "Westmont 401": {
      "lng": -118.288214,
      "lat": 33.762421
    },
    "Westside Solar Power PV1": {
      "lng": -119.907,
      "lat": 36.219
    },
    "Westside Solar Station": {
      "lng": -120.124898,
      "lat": 36.378726
    },
    "Weymouth Solar Plant": {
      "lng": -117.782246,
      "lat": 34.108618
    },
    "Wheelabrator Shasta": {
      "lng": -122.279248,
      "lat": 40.428329
    },
    "Whiskeytown": {
      "lng": -122.538683,
      "lat": 40.597617
    },
    "White River Solar": {
      "lng": -119.4625,
      "lat": 35.871389
    },
    "White River Solar 2": {
      "lng": -119.046111,
      "lat": 35.874722
    },
    "White Rock/Slab Creek": {
      "lng": -120.787366,
      "lat": 38.765336
    },
    "Whitethorn Solar LLC": {
      "lng": -121.717,
      "lat": 37.691
    },
    "Whitewater Hill Wind Partners": {
      "lng": -116.617778,
      "lat": 33.925
    },
    "Whitewater Hydro Plant": {
      "lng": -116.640548,
      "lat": 33.934725
    },
    "Whitney Point Solar": {
      "lng": -120.138472,
      "lat": 36.376889
    },
    "Whittier LFG Power Plant #1": {
      "lng": -118.0481,
      "lat": 33.9716
    },
    "Wildcat I Energy Storage LLC": {
      "lng": -116.489835,
      "lat": 33.807988
    },
    "Wildflower Solar 1": {
      "lng": -121.477,
      "lat": 38.705
    },
    "Wildwood Solar I, LLC": {
      "lng": -119.578333,
      "lat": 35.615833
    },
    "Wildwood Solar II": {
      "lng": -119.573889,
      "lat": 35.63
    },
    "Willow Spring Solar, LLC": {
      "lng": -118.335211,
      "lat": 34.826697
    },
    "Wilmington Hydrogen Plant": {
      "lng": -118.24,
      "lat": 33.7803
    },
    "Windhub Solar A LLC": {
      "lng": -118.2879,
      "lat": 35.0199
    },
    "Windsor Floating Solar": {
      "lng": -122.81508,
      "lat": 38.53917
    },
    "Windstar 1": {
      "lng": -118.354722,
      "lat": 35.054167
    },
    "Wintec Energy Ltd": {
      "lng": -116.5631,
      "lat": 33.9131
    },
    "Wise": {
      "lng": -121.098525,
      "lat": 38.889862
    },
    "Wistaria Ranch Solar": {
      "lng": -115.619,
      "lat": 32.676
    },
    "Wolfskill Energy Center": {
      "lng": -122.07525,
      "lat": 38.228522
    },
    "Wonderful Huller & Shellling": {
      "lng": -119.78885,
      "lat": 35.57609
    },
    "Wonderful Lost Hills": {
      "lng": -119.87658,
      "lat": 35.66934
    },
    "Wonderful Nursery": {
      "lng": -119.392378,
      "lat": 35.617738
    },
    "Wonderful Orchards - Belridge": {
      "lng": -119.74935,
      "lat": 35.50012
    },
    "Wonderful Orchards - New Columbia": {
      "lng": -120.317876,
      "lat": 36.8249
    },
    "Wonderful Orchards - Westside": {
      "lng": -119.74523,
      "lat": 35.73277
    },
    "Woodland Biomass Power Ltd": {
      "lng": -121.7371,
      "lat": 38.6905
    },
    "Woodland Generation Station": {
      "lng": -121.020397,
      "lat": 37.652602
    },
    "Woodleaf": {
      "lng": -121.204077,
      "lat": 39.553811
    },
    "Woodmere Solar Farm": {
      "lng": -118.821389,
      "lat": 35.323056
    },
    "Woodward Power Plant": {
      "lng": -120.879153,
      "lat": 37.860653
    },
    "Wright Solar Park": {
      "lng": -120.957222,
      "lat": 37.009444
    },
    "WWTP Power Generation Station": {
      "lng": -122.2961,
      "lat": 37.8258
    },
    "Xebec 1": {
      "lng": -118.392921,
      "lat": 34.224041
    },
    "Xilinx San Jose": {
      "lng": -121.934167,
      "lat": 37.252778
    },
    "Yahoo! - HQ": {
      "lng": -122.025747,
      "lat": 37.415419
    },
    "Yerba Buena": {
      "lng": -121.755,
      "lat": 37.305833
    },
    "Yolo County Solar Project": {
      "lng": -121.7294,
      "lat": 38.659722
    },
    "Yorba Linda": {
      "lng": -117.8192,
      "lat": 33.9117
    },
    "Yuba City Cogen Partners": {
      "lng": -121.639846,
      "lat": 39.13667
    },
    "Yuba City Energy Center": {
      "lng": -121.6387,
      "lat": 39.1392
    },
    "ZCO": {
      "lng": -118.3811,
      "lat": 35.0772
    },
    "Zero Waste Energy Development Co LLC": {
      "lng": -121.95285,
      "lat": 37.4325
    },
    "3 MW LLC": {
      "lng": -107.5,
      "lat": 38.9
    },
    "Adams Community Solar Garden III LLC": {
      "lng": -104.5877,
      "lat": 39.7867
    },
    "Adams Community Solar Garden LLC": {
      "lng": -104.5859,
      "lat": 39.7876
    },
    "AFA Solar Farm": {
      "lng": -104.806627,
      "lat": 38.957901
    },
    "Airport 1 Solar (DIA)": {
      "lng": -104.675338,
      "lat": 39.899444
    },
    "Airport Industrial": {
      "lng": -104.5308,
      "lat": 38.2844
    },
    "Alamosa": {
      "lng": -105.8947,
      "lat": 37.4594
    },
    "Alamosa Solar South CSG": {
      "lng": -105.85916,
      "lat": 37.4512
    },
    "Alden Solar CSG LLC": {
      "lng": -104.58107,
      "lat": 40.45633
    },
    "Amazon DEN2 Solar Project": {
      "lng": -104.75,
      "lat": 39.75
    },
    "Amazon Denver DEN3": {
      "lng": -104.982339,
      "lat": 39.885061
    },
    "American Gypsum Cogeneration": {
      "lng": -106.949394,
      "lat": 39.648565
    },
    "Ames Hydro": {
      "lng": -107.8831,
      "lat": 37.866
    },
    "Arapahoe 3 Community Solar Array": {
      "lng": -104.674249,
      "lat": 39.638358
    },
    "Arapahoe Combustion Turbine Facility": {
      "lng": -105.0018,
      "lat": 39.6692
    },
    "Bar D": {
      "lng": -108.462709,
      "lat": 39.923679
    },
    "Basalt": {
      "lng": -106.821944,
      "lat": 39.363056
    },
    "Big Thompson": {
      "lng": -105.223804,
      "lat": 40.421031
    },
    "Bighorn Solar 1": {
      "lng": -104.596708,
      "lat": 38.207518
    },
    "Bison Solar LLC": {
      "lng": -105.0111,
      "lat": 40.863809
    },
    "Blue Mesa": {
      "lng": -107.337483,
      "lat": 38.453191
    },
    "Blue Spruce Energy Center": {
      "lng": -104.681404,
      "lat": 39.747875
    },
    "Boulder Canyon Hydro": {
      "lng": -105.332495,
      "lat": 40.004725
    },
    "Boulder City Betasso Hydro Plant": {
      "lng": -105.3347,
      "lat": 40.0111
    },
    "Boulder City Lakewood Hydro": {
      "lng": -105.3347,
      "lat": 40.0111
    },
    "Boulder City Silver Lake Hydro": {
      "lng": -105.50003,
      "lat": 39.991311
    },
    "Brighton PV Solar Plant": {
      "lng": -104.940833,
      "lat": 39.9825
    },
    "Bronco Plains Wind, LLC": {
      "lng": -103.025074,
      "lat": 39.088845
    },
    "Brush Power Projects": {
      "lng": -103.631,
      "lat": 40.2415
    },
    "Burlington (CO)": {
      "lng": -102.2431,
      "lat": 39.3561
    },
    "Busch Ranch II Wind Farm": {
      "lng": -104.42945,
      "lat": 37.766416
    },
    "Busch Ranch Wind Energy Farm": {
      "lng": -104.491944,
      "lat": 37.801111
    },
    "Cabin Creek": {
      "lng": -105.7088,
      "lat": 39.6551
    },
    "Carousel Wind Farm LLC": {
      "lng": -102.173,
      "lat": 39.334
    },
    "Carson Solar I": {
      "lng": -104.7817,
      "lat": 38.7228
    },
    "Carter Hydro": {
      "lng": -105.208889,
      "lat": 40.324167
    },
    "CEC Solar #1117, LLC": {
      "lng": -104.523951,
      "lat": 38.284425
    },
    "Cedar Creek II": {
      "lng": -103.783889,
      "lat": 40.9475
    },
    "Cedar Creek Wind": {
      "lng": -104.0206,
      "lat": 40.8944
    },
    "Cedar Point Wind": {
      "lng": -103.67814,
      "lat": 39.421787
    },
    "Center": {
      "lng": -106.10467,
      "lat": 37.753606
    },
    "Cherokee": {
      "lng": -104.964548,
      "lat": 39.807275
    },
    "Cheyenne Ridge Wind Farm": {
      "lng": -102.2113,
      "lat": 39.016944
    },
    "City & County of Denver at Denver Int'l": {
      "lng": -104.654183,
      "lat": 39.904133
    },
    "City of Boulder WWTP": {
      "lng": -105.180327,
      "lat": 40.048025
    },
    "Clear Spring Ranch PV Project": {
      "lng": -104.694688,
      "lat": 38.602102
    },
    "CO LI CSG 1 - Kamerra": {
      "lng": -104.583391,
      "lat": 39.784879
    },
    "Cogentrix of Alamosa": {
      "lng": -105.955832,
      "lat": 37.598579
    },
    "Colorado Energy Nations Company": {
      "lng": -105.215,
      "lat": 39.7606
    },
    "Colorado Green Holdings LLC": {
      "lng": -102.6231,
      "lat": 37.7033
    },
    "Colorado Highlands Wind": {
      "lng": -102.743055,
      "lat": 40.756944
    },
    "Comanche (470)": {
      "lng": -104.5747,
      "lat": 38.2081
    },
    "Comanche Solar": {
      "lng": -104.566667,
      "lat": 38.205278
    },
    "Conejos 1 Community Solar Array": {
      "lng": -105.982424,
      "lat": 37.195886
    },
    "Coyote Ridge Community Solar": {
      "lng": -105.13143,
      "lat": 40.500969
    },
    "Craig": {
      "lng": -107.5912,
      "lat": 40.4627
    },
    "Crossing Trails Wind Power Project LLC.": {
      "lng": -102.822377,
      "lat": 39.046623
    },
    "Crystal": {
      "lng": -107.625335,
      "lat": 38.510573
    },
    "CSU Pueblo": {
      "lng": -104.58,
      "lat": 38.306111
    },
    "DADS Gas Recovery": {
      "lng": -104.7161,
      "lat": 39.6522
    },
    "Denver Int Airport": {
      "lng": -104.697149,
      "lat": 39.836789
    },
    "Denver Intl Airport IV Solar": {
      "lng": -104.669444,
      "lat": 39.900556
    },
    "Denver Metro Solar": {
      "lng": -104.621139,
      "lat": 39.759298
    },
    "Dillon Hydro Plant": {
      "lng": -106.0662,
      "lat": 39.6208
    },
    "Eagle Springs Solar LLC": {
      "lng": -107.696111,
      "lat": 39.526111
    },
    "Eagle Valley Clean Energy LLC Biomass": {
      "lng": -106.943056,
      "lat": 39.648056
    },
    "Estes": {
      "lng": -105.509665,
      "lat": 40.376771
    },
    "Flatiron": {
      "lng": -105.236032,
      "lat": 40.364996
    },
    "Foothills Hydro Plant": {
      "lng": -105.0657,
      "lat": 39.4588
    },
    "Fort Carson Battery Energy Storage System": {
      "lng": -104.777675,
      "lat": 38.745474
    },
    "Fort Lupton": {
      "lng": -104.796,
      "lat": 40.0922
    },
    "Fort St. Vrain": {
      "lng": -104.8742,
      "lat": 40.2461
    },
    "Fountain Valley Power Plant": {
      "lng": -104.6875,
      "lat": 38.5569
    },
    "Frank Knutson Station": {
      "lng": -104.682015,
      "lat": 39.941129
    },
    "Fremont CO 1, LLC": {
      "lng": -105.21246,
      "lat": 38.46446
    },
    "Front Range Power Plant": {
      "lng": -104.7069,
      "lat": 38.6281
    },
    "Front Range Project": {
      "lng": -105.004722,
      "lat": 40.029444
    },
    "Fruita": {
      "lng": -108.7347,
      "lat": 39.1364
    },
    "George Birdsall": {
      "lng": -104.816944,
      "lat": 38.881389
    },
    "Georgetown Hydro": {
      "lng": -105.6978,
      "lat": 39.6919
    },
    "Gilcrest Solar": {
      "lng": -104.783613,
      "lat": 40.289366
    },
    "Gilcrest Sun CSG": {
      "lng": -104.806202,
      "lat": 40.293752
    },
    "Gilcrest V CSG": {
      "lng": -104.770747,
      "lat": 40.317966
    },
    "Golden West Power Partners LLC": {
      "lng": -104.212,
      "lat": 38.935
    },
    "Granby Hydro": {
      "lng": -105.8683,
      "lat": 40.1464
    },
    "Grand Valley Project Power Plant": {
      "lng": -108.345556,
      "lat": 39.101667
    },
    "Grazing Yak Solar": {
      "lng": -104.252715,
      "lat": 38.977759
    },
    "Greater Sandhill I": {
      "lng": -105.8909,
      "lat": 37.685467
    },
    "Green Mountain": {
      "lng": -106.332725,
      "lat": 39.878805
    },
    "Gross Hydro Plant": {
      "lng": -105.3565,
      "lat": 39.9456
    },
    "Hayden": {
      "lng": -107.185,
      "lat": 40.4856
    },
    "Hillcrest Pump Station": {
      "lng": -104.9138,
      "lat": 39.6419
    },
    "Holly": {
      "lng": -102.073659,
      "lat": 38.032717
    },
    "Hooper Solar": {
      "lng": -105.990812,
      "lat": 37.691863
    },
    "Huerfano River Wind": {
      "lng": -104.832222,
      "lat": 37.772222
    },
    "I25 Battery Storage": {
      "lng": -104.978012,
      "lat": 40.140285
    },
    "IBM Solar": {
      "lng": -105.244892,
      "lat": 40.038266
    },
    "Imboden Solar Garden": {
      "lng": -104.583574,
      "lat": 39.786769
    },
    "J M Shafer": {
      "lng": -104.7736,
      "lat": 40.0986
    },
    "Jack's Solar Garden": {
      "lng": -105.130321,
      "lat": 40.122552
    },
    "James W. Broderick Hydropower Plant": {
      "lng": -104.72,
      "lat": 38.27
    },
    "Jeffco Community Solar Gardens LLC": {
      "lng": -105.149694,
      "lat": 39.859564
    },
    "Julesburg": {
      "lng": -102.2795,
      "lat": 40.9505
    },
    "Kit Carson Windpower": {
      "lng": -102.3533,
      "lat": 39.3383
    },
    "Lafayette Horizon Solar CSG LLC": {
      "lng": -105.063747,
      "lat": 39.981606
    },
    "Lamar Plant": {
      "lng": -102.537915,
      "lat": 38.033327
    },
    "Las Animas": {
      "lng": -103.2159,
      "lat": 38.0647
    },
    "Limon Generating Station": {
      "lng": -103.7005,
      "lat": 39.2038
    },
    "Limon III Wind LLC": {
      "lng": -103.474167,
      "lat": 39.353333
    },
    "Limon Wind I": {
      "lng": -103.573236,
      "lat": 39.380975
    },
    "Limon Wind II": {
      "lng": -103.581111,
      "lat": 39.341389
    },
    "Logan 1 Community Solar Array": {
      "lng": -103.145084,
      "lat": 40.609968
    },
    "Logan Wind Energy": {
      "lng": -103.259,
      "lat": 40.941
    },
    "Lower Molina": {
      "lng": -108.044088,
      "lat": 39.19366
    },
    "Manchief Generating Station": {
      "lng": -103.68504,
      "lat": 40.217143
    },
    "Manitou Springs": {
      "lng": -104.933056,
      "lat": 38.855556
    },
    "Martin Drake": {
      "lng": -104.833333,
      "lat": 38.824444
    },
    "Marys Lake": {
      "lng": -105.534308,
      "lat": 40.34408
    },
    "McPhee": {
      "lng": -108.574002,
      "lat": 37.57522
    },
    "Mesa CSG 1 Murdock": {
      "lng": -108.64387,
      "lat": 39.108785
    },
    "Mesa CSG 2 Massicotte": {
      "lng": -108.419672,
      "lat": 39.044814
    },
    "Mesa PV1": {
      "lng": -108.509139,
      "lat": 39.0672
    },
    "Metro Wastewater Reclamation District": {
      "lng": -104.9542,
      "lat": 39.8078
    },
    "Monte Vista Solar 2 CSG, LLC": {
      "lng": -106.09988,
      "lat": 37.57176
    },
    "Morrow Point": {
      "lng": -107.539299,
      "lat": 38.451753
    },
    "Mount Elbert": {
      "lng": -106.352331,
      "lat": 39.094197
    },
    "Mountain Breeze Wind, LLC": {
      "lng": -104.003174,
      "lat": 40.960618
    },
    "Mtn. Solar 1": {
      "lng": -104.58591,
      "lat": 39.777621
    },
    "Mtn. Solar 2": {
      "lng": -104.59054,
      "lat": 39.766772
    },
    "Mtn. Solar 3 CSG": {
      "lng": -103.61911,
      "lat": 40.280904
    },
    "Mtn. Solar 4 (CSG)": {
      "lng": -105.113057,
      "lat": 39.727586
    },
    "Niyol Wind, LLC": {
      "lng": -102.9526,
      "lat": 40.613364
    },
    "North Fork Hydro Plant": {
      "lng": -105.677,
      "lat": 39.4616
    },
    "Northern Colorado Wind LLC": {
      "lng": -102.8969,
      "lat": 40.9881
    },
    "NSE Camber BH CSG 2, LLC": {
      "lng": -103.747306,
      "lat": 38.209158
    },
    "Oak Leaf Solar XVIII LLC": {
      "lng": -104.66125,
      "lat": 39.89939
    },
    "Oak Leaf Solar XXI (CSG)": {
      "lng": -104.604,
      "lat": 39.638
    },
    "Oak Leaf Solar XXII LLC (CSG)": {
      "lng": -104.6848,
      "lat": 39.904373
    },
    "Oak Leaf Solar XXIII LLC (CSG)": {
      "lng": -104.62146,
      "lat": 39.80165
    },
    "Oak Leaf Solar XXIV LLC (CSG)": {
      "lng": -104.561,
      "lat": 39.637
    },
    "Oak Leaf Solar XXV LLC (CSG)": {
      "lng": -104.529,
      "lat": 39.637
    },
    "Oak Leaf Solar XXVI LLC": {
      "lng": -104.659,
      "lat": 39.899
    },
    "Oak Leaf Solar XXVII LLC": {
      "lng": -104.64,
      "lat": 40.44
    },
    "Oak Leaf Solar XXVIII LLC (CSG)": {
      "lng": -104.779,
      "lat": 40.237
    },
    "Oak Leaf Solar XXX LLC (CSG)": {
      "lng": -105.878021,
      "lat": 37.453381
    },
    "Oak Leaf Solar XXXI LLC (CSG)": {
      "lng": -108.32,
      "lat": 39.145
    },
    "Oak Leaf Solar XXXII (CSG)": {
      "lng": -107.815,
      "lat": 39.523
    },
    "Oak Leaf Solar XXXIII LLC (Lantz)": {
      "lng": -106.0753,
      "lat": 38.5357
    },
    "OREG 4 Peetz": {
      "lng": -102.853056,
      "lat": 40.9925
    },
    "Palmer Solar": {
      "lng": -104.6526,
      "lat": 38.6299
    },
    "Panasonic Carport Solar Hybrid": {
      "lng": -104.781494,
      "lat": 39.808719
    },
    "Pawnee": {
      "lng": -103.6803,
      "lat": 40.2217
    },
    "Peak View Wind Farm": {
      "lng": -104.505464,
      "lat": 37.710444
    },
    "Peetz Table Wind Energy": {
      "lng": -103.436,
      "lat": 40.986
    },
    "Pikes Peak Solar Garden 1 LLC CSG": {
      "lng": -104.7458,
      "lat": 38.7745
    },
    "Pioneer Solar (CO), LLC": {
      "lng": -104.45903,
      "lat": 39.761569
    },
    "Pitkin County Solar": {
      "lng": -106.876811,
      "lat": 39.247705
    },
    "Plains End": {
      "lng": -105.225967,
      "lat": 39.857499
    },
    "Plains End II LLC": {
      "lng": -105.2256,
      "lat": 39.8586
    },
    "Platteville Solar CSG, LLC": {
      "lng": -104.77471,
      "lat": 40.22833
    },
    "Pole Hill": {
      "lng": -105.325074,
      "lat": 40.365056
    },
    "Pueblo": {
      "lng": -104.6144,
      "lat": 38.2667
    },
    "Pueblo Airport Generating Station": {
      "lng": -104.52639,
      "lat": 38.320833
    },
    "Quincy II Solar Garden": {
      "lng": -104.662508,
      "lat": 39.638409
    },
    "Quincy Solar": {
      "lng": -78.8078,
      "lat": 35.0717
    },
    "Rawhide Energy Station": {
      "lng": -105.021207,
      "lat": 40.860905
    },
    "Rawhide Prairie Solar Hybrid": {
      "lng": -105.015561,
      "lat": 40.857978
    },
    "Ray D Nixon": {
      "lng": -104.70577,
      "lat": 38.633451
    },
    "Redlands Water & Power": {
      "lng": -108.592791,
      "lat": 39.067347
    },
    "Ridge Crest Wind Partners": {
      "lng": -103.162028,
      "lat": 40.945911
    },
    "Rifle Generating Station": {
      "lng": -107.7299,
      "lat": 39.5173
    },
    "Riverview Solar": {
      "lng": -103.132442,
      "lat": 40.66583
    },
    "Rock Creek 2 CSG": {
      "lng": -105.90341,
      "lat": 37.39955
    },
    "Rocky Ford": {
      "lng": -103.7136,
      "lat": 38.0492
    },
    "Rocky Mountain Energy Center": {
      "lng": -104.5953,
      "lat": 40.0908
    },
    "Rush Creek Wind": {
      "lng": -103.845358,
      "lat": 39.172111
    },
    "Ruxton Park": {
      "lng": -104.973802,
      "lat": 38.841354
    },
    "RV CSU Power II LLC": {
      "lng": -105.147097,
      "lat": 40.592841
    },
    "RV CSU Power LLC": {
      "lng": -105.145904,
      "lat": 40.591099
    },
    "Salida": {
      "lng": -106.182003,
      "lat": 38.53419
    },
    "San Isabel Solar, LLC": {
      "lng": -104.843,
      "lat": 37.372
    },
    "San Luis Solar Garden": {
      "lng": -106.018925,
      "lat": 37.068132
    },
    "San Luis Valley Solar Array": {
      "lng": -106.22,
      "lat": 37.777
    },
    "San Luis Valley Solar Ranch": {
      "lng": -105.927622,
      "lat": 37.694535
    },
    "Shavano Falls Hydro": {
      "lng": -108.002283,
      "lat": 38.491966
    },
    "Shavano Falls Hydro Drop 4": {
      "lng": -107.776727,
      "lat": 38.450483
    },
    "Shoshone (CO)": {
      "lng": -107.226876,
      "lat": 39.57013
    },
    "Skylark": {
      "lng": -104.894792,
      "lat": 40.577889
    },
    "SMPA Solar 1 Community Solar": {
      "lng": -108.785556,
      "lat": 38.2725
    },
    "South Canal Hydro-1": {
      "lng": -107.755,
      "lat": 38.483333
    },
    "South Canal Hydro-3": {
      "lng": -107.771389,
      "lat": 38.470278
    },
    "Spindle Hill Energy Center": {
      "lng": -104.887844,
      "lat": 40.092192
    },
    "Spring Canyon": {
      "lng": -102.9647,
      "lat": 40.983
    },
    "Spring Canyon Expansion Wind Energy Ctr": {
      "lng": -103.077222,
      "lat": 40.964167
    },
    "Springfield (CO)": {
      "lng": -102.6217,
      "lat": 37.4028
    },
    "SR Jenkins Ft Lupton": {
      "lng": -104.746661,
      "lat": 40.081444
    },
    "SR Kersey": {
      "lng": -104.55095,
      "lat": 40.386197
    },
    "SR Kersey II": {
      "lng": -104.554038,
      "lat": 40.386223
    },
    "SR Mavericks": {
      "lng": -105.03,
      "lat": 40.23
    },
    "SR Platte Solar Farm": {
      "lng": -104.692067,
      "lat": 40.181742
    },
    "SR Rattlesnake": {
      "lng": -104.413144,
      "lat": 40.105427
    },
    "SR Skylark B": {
      "lng": -104.895675,
      "lat": 40.579472
    },
    "SR Skylark C": {
      "lng": -104.89577,
      "lat": 40.575118
    },
    "Sterling PV 3": {
      "lng": -103.194694,
      "lat": 40.6316
    },
    "Strontia Springs Hydro Plant": {
      "lng": -105.1263,
      "lat": 39.4326
    },
    "Sugarloaf Hydro Plant": {
      "lng": -106.374808,
      "lat": 39.252672
    },
    "SunE Alamosa": {
      "lng": -105.880501,
      "lat": 37.690304
    },
    "Sunnyside Ranch Community Solar Array": {
      "lng": -107.157188,
      "lat": 39.415027
    },
    "Tacoma": {
      "lng": -107.782786,
      "lat": 37.52372
    },
    "Taylor Draw Hydroelectric Facility": {
      "lng": -108.711161,
      "lat": 40.106269
    },
    "Tesla": {
      "lng": -104.901141,
      "lat": 38.973655
    },
    "Titan Solar": {
      "lng": -104.01398,
      "lat": 39.68646
    },
    "Tom Sifers Solar": {
      "lng": -105.846,
      "lat": 39.959166
    },
    "Towaoc": {
      "lng": -108.574871,
      "lat": 37.429538
    },
    "Tri-County Water Hydropower Project": {
      "lng": -107.758056,
      "lat": 38.239167
    },
    "Trinidad (CO)": {
      "lng": -104.487492,
      "lat": 37.178979
    },
    "Trout Creek Solar": {
      "lng": -106.1104,
      "lat": 38.8151
    },
    "TruStile Doors - Denver": {
      "lng": -104.97312,
      "lat": 39.825708
    },
    "Twin Buttes II Wind": {
      "lng": -102.546,
      "lat": 37.773
    },
    "Twin Buttes Wind Project": {
      "lng": -102.8622,
      "lat": 37.6735
    },
    "University of Colorado": {
      "lng": -105.269204,
      "lat": 40.00759
    },
    "Upper Molina": {
      "lng": -108.004777,
      "lat": 39.143808
    },
    "Vallecito Hydroelectric": {
      "lng": -107.578745,
      "lat": 37.377715
    },
    "Valley View Solar": {
      "lng": -104.891944,
      "lat": 40.418786
    },
    "Valmont": {
      "lng": -105.202,
      "lat": 40.0195
    },
    "Valmont Combustion Turbine Facility": {
      "lng": -105.2009,
      "lat": 40.0205
    },
    "Vasquez V CSG": {
      "lng": -104.79894,
      "lat": 40.231806
    },
    "Venture 1 - 1A CSG": {
      "lng": -104.667388,
      "lat": 39.752297
    },
    "Venture 2 - 1B": {
      "lng": -104.66509,
      "lat": 39.752244
    },
    "Venture 3 - 1C": {
      "lng": -104.66733,
      "lat": 39.750499
    },
    "Venture 4 - 1D": {
      "lng": -104.66733,
      "lat": 39.752334
    },
    "Vestas Towers America, Inc.": {
      "lng": -104.62,
      "lat": 38.1728
    },
    "Victory Solar LLC": {
      "lng": -104.433,
      "lat": 39.795
    },
    "Vilas Solar Array": {
      "lng": -102.466,
      "lat": 37.384
    },
    "Weld 1 Community Solar Array": {
      "lng": -104.791865,
      "lat": 40.232146
    },
    "Western Sugar Coop- Ft Morgan": {
      "lng": -103.8069,
      "lat": 40.263
    },
    "Whiskey Hill Solar": {
      "lng": -106.289722,
      "lat": 40.8125
    },
    "Williams Fork Hydro Plant": {
      "lng": -106.2053,
      "lat": 40.035
    },
    "Williams Ignacio Natural Gas Plant": {
      "lng": -107.784722,
      "lat": 37.145556
    },
    "WWRF Solar Plant": {
      "lng": -107.81341,
      "lat": 39.522904
    },
    "Xcel Adams 1 Community Solar Array": {
      "lng": -104.588631,
      "lat": 39.765479
    },
    "Xcel Adams 2 Community Solar Array": {
      "lng": -104.583391,
      "lat": 39.784937
    },
    "Adams Solar": {
      "lng": -72.161,
      "lat": 41.655
    },
    "Advance Stores Company, Inc": {
      "lng": -72.511892,
      "lat": 42.015023
    },
    "ALDI DC 2": {
      "lng": -72.573956,
      "lat": 41.865827
    },
    "Alfred L Pierce Generating Station": {
      "lng": -72.834884,
      "lat": 41.44823
    },
    "Algonquin Power Windsor Locks, LLC": {
      "lng": -72.6255,
      "lat": 41.9227
    },
    "Amazon BDL3 Solar Project": {
      "lng": -72.85,
      "lat": 41.45
    },
    "Antares-GRE 314 East Lyme LLC": {
      "lng": -72.234444,
      "lat": 41.416944
    },
    "Backus Microgrid Project": {
      "lng": -72.093611,
      "lat": 41.542778
    },
    "Barrett Farm Solar - Phase I": {
      "lng": -71.883799,
      "lat": 41.946333
    },
    "Barrow Solar": {
      "lng": -72.50491,
      "lat": 42.03263
    },
    "Becton Canaan": {
      "lng": -73.335,
      "lat": 42.01
    },
    "Blair Solar": {
      "lng": -72.50324,
      "lat": 42.02956
    },
    "Bradley Energy Center": {
      "lng": -72.683143,
      "lat": 41.927102
    },
    "Branford": {
      "lng": -72.7942,
      "lat": 41.2925
    },
    "Brass Mill Center": {
      "lng": -73.0245,
      "lat": 41.5489
    },
    "Bridge Street 1 & 2": {
      "lng": -72.0825,
      "lat": 41.362778
    },
    "Bridgeport Energy": {
      "lng": -73.1844,
      "lat": 41.1692
    },
    "Bridgeport Fuel Cell, LLC": {
      "lng": -73.211111,
      "lat": 41.168056
    },
    "Bridgeport Harbor Station": {
      "lng": -73.1844,
      "lat": 41.1706
    },
    "Bristol Solar One, LLC": {
      "lng": -72.97855,
      "lat": 41.69097
    },
    "Bulls Bridge": {
      "lng": -73.491984,
      "lat": 41.659046
    },
    "Canis Major Solar Farm": {
      "lng": -72.65,
      "lat": 42.009
    },
    "Canis Minor Solar Farm": {
      "lng": -72.98595,
      "lat": 41.892997
    },
    "Capitol District Energy Center": {
      "lng": -72.6925,
      "lat": 41.7639
    },
    "CCSU Co-Gen-STBY Gen": {
      "lng": -72.768611,
      "lat": 41.691389
    },
    "CCSU Fuel Cell Project": {
      "lng": -72.761111,
      "lat": 41.691944
    },
    "Cellu Tissue": {
      "lng": -72.606667,
      "lat": 41.776111
    },
    "CJTS Energy Center": {
      "lng": -72.625,
      "lat": 41.552778
    },
    "Clinton Solar": {
      "lng": -71.9018,
      "lat": 41.5825
    },
    "CMEEC - Bozrah": {
      "lng": -72.158607,
      "lat": 41.570584
    },
    "CMEEC - Navy NE Trident": {
      "lng": -72.079191,
      "lat": 41.39546
    },
    "CMEEC - Norwich Stott St Solar Hybrid": {
      "lng": -72.104609,
      "lat": 41.576946
    },
    "CMEEC - Polaris Park Solar Hybrid": {
      "lng": -72.076886,
      "lat": 41.381256
    },
    "CMEEC - Rogers Rd Solar": {
      "lng": -72.122544,
      "lat": 41.538324
    },
    "Conn Mun Electric Energy Coop": {
      "lng": -72.122544,
      "lat": 41.538324
    },
    "Corbin Russwin Phase 3 Berlin": {
      "lng": -72.740907,
      "lat": 41.642322
    },
    "Cos Cob": {
      "lng": -73.5989,
      "lat": 41.0289
    },
    "Covanta Bristol Energy": {
      "lng": -72.9153,
      "lat": 41.6492
    },
    "Covanta Southeastern Connecticut Company": {
      "lng": -72.0688,
      "lat": 41.475
    },
    "CP Middletown Solar I LLC": {
      "lng": -72.706877,
      "lat": 41.548826
    },
    "CP Middletown Solar II LLC": {
      "lng": -72.706877,
      "lat": 41.548826
    },
    "CPV Towantic Energy Center": {
      "lng": -73.123108,
      "lat": 41.481243
    },
    "CT Resource Rec Authority Facility": {
      "lng": -72.652982,
      "lat": 41.750521
    },
    "Danbury Hospital Cogen Plant": {
      "lng": -73.4475,
      "lat": 41.405278
    },
    "Derby Hydro": {
      "lng": -73.102111,
      "lat": 41.324308
    },
    "Devon": {
      "lng": -73.108976,
      "lat": 41.209797
    },
    "Dickinson Solar (CT)": {
      "lng": -72.0794,
      "lat": 41.7698
    },
    "Digital Fairfield": {
      "lng": -73.14603,
      "lat": 41.24019
    },
    "DWW Solar ll": {
      "lng": -72.475018,
      "lat": 41.541596
    },
    "East Windsor Solar One": {
      "lng": -72.518,
      "lat": 41.89
    },
    "Fairfield University CHP Plant": {
      "lng": -73.2572,
      "lat": 41.1589
    },
    "Falls Village": {
      "lng": -73.369297,
      "lat": 41.957272
    },
    "FDX010.0 FedEx Fuel Cell": {
      "lng": -72.71316,
      "lat": 41.5948
    },
    "Few Solar": {
      "lng": -72.50438,
      "lat": 42.03107
    },
    "Fort Hill 1, 2, 3 & 4": {
      "lng": -72.1022,
      "lat": 41.4711
    },
    "Foxwoods CoGen": {
      "lng": -71.9606,
      "lat": 41.4731
    },
    "Franklin Drive": {
      "lng": -73.116267,
      "lat": 41.798943
    },
    "Franklin Solar": {
      "lng": -75.06,
      "lat": 40.72
    },
    "Frito Lay Incorporated": {
      "lng": -71.891667,
      "lat": 41.862778
    },
    "Fusion Solar Center LLC": {
      "lng": -72.043056,
      "lat": 41.623889
    },
    "Gary Court 1 & 2": {
      "lng": -72.02,
      "lat": 41.3367
    },
    "GenConn Devon LLC": {
      "lng": -73.1075,
      "lat": 41.2108
    },
    "Goodwin Hydroelectric": {
      "lng": -73.0194,
      "lat": 41.988611
    },
    "Hamilton Solar": {
      "lng": -72.161,
      "lat": 41.657
    },
    "Hartford Hospital Cogeneration": {
      "lng": -72.6733,
      "lat": 41.7631
    },
    "Hartford Landfill Solar EGF": {
      "lng": -72.6509,
      "lat": 41.7915
    },
    "HSCo CHP": {
      "lng": -72.6686,
      "lat": 41.7614
    },
    "IBM Southbury": {
      "lng": -73.207043,
      "lat": 41.472492
    },
    "IKEA New Haven Rooftop PV &  Fuel Cell": {
      "lng": -72.91995,
      "lat": 41.295715
    },
    "Jackson Solar (CT)": {
      "lng": -71.904,
      "lat": 41.583
    },
    "Jefferson Solar": {
      "lng": -72.162,
      "lat": 41.656
    },
    "Jewett City 1": {
      "lng": -71.9825,
      "lat": 41.6041
    },
    "Kimberly Clark-Unit 1,2,3": {
      "lng": -73.413611,
      "lat": 41.558333
    },
    "Kinneytown New Old": {
      "lng": -73.0856,
      "lat": 41.3686
    },
    "Kleen Energy Systems Project": {
      "lng": -72.596867,
      "lat": 41.553227
    },
    "Lake Road Generating Company": {
      "lng": -71.895799,
      "lat": 41.872043
    },
    "Lebanon Pines 1 & 2": {
      "lng": -72.2322,
      "lat": 41.5683
    },
    "LNG 1 & 2": {
      "lng": -72.1175,
      "lat": 41.5694
    },
    "Madison Solar": {
      "lng": -72.503952,
      "lat": 42.0305
    },
    "Manchester Community College East": {
      "lng": -72.557144,
      "lat": 41.763257
    },
    "Manchester Community College North": {
      "lng": -72.56329,
      "lat": 41.763592
    },
    "McHenry Solar": {
      "lng": -72.0802,
      "lat": 41.77
    },
    "Medtronic - New Haven": {
      "lng": -72.870281,
      "lat": 41.33788
    },
    "Medtronic - New Haven 2": {
      "lng": -72.865033,
      "lat": 41.339715
    },
    "Middletown": {
      "lng": -72.576666,
      "lat": 41.554444
    },
    "Milford Power Company LLC": {
      "lng": -73.099728,
      "lat": 41.224355
    },
    "Millstone": {
      "lng": -72.1677,
      "lat": 41.3107
    },
    "Montville": {
      "lng": -72.1019,
      "lat": 41.4281
    },
    "New Haven Harbor": {
      "lng": -72.904323,
      "lat": 41.283997
    },
    "New Milford Gas Recovery": {
      "lng": -73.425237,
      "lat": 41.547671
    },
    "Norden 1-3": {
      "lng": -73.3928,
      "lat": 41.1117
    },
    "Norwalk Hospital Plant": {
      "lng": -73.421968,
      "lat": 41.110861
    },
    "Norwich": {
      "lng": -72.064935,
      "lat": 41.52679
    },
    "Norwich WWTP": {
      "lng": -72.0833,
      "lat": 41.5256
    },
    "Nutmeg Solar": {
      "lng": -72.510833,
      "lat": 41.974794
    },
    "Pepperidge Farm Bloomfield": {
      "lng": -72.718315,
      "lat": 41.869302
    },
    "Pfizer Groton Fuel Cell": {
      "lng": -72.071111,
      "lat": 41.333889
    },
    "Pfizer Groton Plant": {
      "lng": -72.0786,
      "lat": 41.3315
    },
    "Plainfield Renewable Energy LLC": {
      "lng": -71.92418,
      "lat": 41.664376
    },
    "Plainfield Solar 1": {
      "lng": -71.901,
      "lat": 41.68
    },
    "Plainfield Solar 2": {
      "lng": -71.905,
      "lat": 41.679
    },
    "Pratt & Whitney, East Hartford": {
      "lng": -72.636693,
      "lat": 41.748327
    },
    "Quinebaug Lower Project": {
      "lng": -71.886759,
      "lat": 41.798656
    },
    "Quinebaug Solar": {
      "lng": -71.932481,
      "lat": 41.749867
    },
    "Rainbow (CT)": {
      "lng": -72.693616,
      "lat": 41.915312
    },
    "Rand Whitney CHP Plant": {
      "lng": -72.136111,
      "lat": 41.452778
    },
    "Rocky River (CT)": {
      "lng": -73.4349,
      "lat": 41.5826
    },
    "Scotland Dam": {
      "lng": -72.1221,
      "lat": 41.6651
    },
    "Shepaug": {
      "lng": -73.295321,
      "lat": 41.44842
    },
    "Sherman Solar": {
      "lng": -71.902,
      "lat": 41.583
    },
    "Sikorsky Aircraft CHP": {
      "lng": -73.09865,
      "lat": 41.250009
    },
    "Somers Solar Center, LLC": {
      "lng": -72.445833,
      "lat": 41.955
    },
    "South Meadow Station": {
      "lng": -72.6524,
      "lat": 41.7495
    },
    "South Windsor Fuel Cell": {
      "lng": -72.614377,
      "lat": 41.815417
    },
    "Stafford MS Ground Mount Community Solar": {
      "lng": -72.309,
      "lat": 41.964
    },
    "Stamford Health": {
      "lng": -73.55198,
      "lat": 41.055636
    },
    "Stevenson": {
      "lng": -73.1714,
      "lat": 41.3833
    },
    "Sydney Solar": {
      "lng": -72.0783,
      "lat": 41.7683
    },
    "Taftville": {
      "lng": -72.0458,
      "lat": 41.5725
    },
    "Tenth Street": {
      "lng": -72.050936,
      "lat": 41.538955
    },
    "Ticket Network South Windsor": {
      "lng": -72.507383,
      "lat": 41.821518
    },
    "Torrington Solar One, LLC": {
      "lng": -73.07802,
      "lat": 41.83019
    },
    "Torrington Terminal": {
      "lng": -73.120886,
      "lat": 41.776109
    },
    "Town of Branford": {
      "lng": -72.81028,
      "lat": 41.28114
    },
    "Town of Rocky Hill PV CSG": {
      "lng": -72.633011,
      "lat": 41.643223
    },
    "Trinity College Fuel Cell": {
      "lng": -72.689356,
      "lat": 41.746288
    },
    "TRS Fuel Cell": {
      "lng": -73.4354,
      "lat": 41.3907
    },
    "Tunnel": {
      "lng": -72.041829,
      "lat": 41.555425
    },
    "UB Fuel Cell": {
      "lng": -73.19606,
      "lat": 41.165036
    },
    "UCONN Cogen Facility": {
      "lng": -72.254167,
      "lat": 41.809167
    },
    "UDR Glastonbury Fuel Cell": {
      "lng": -72.536077,
      "lat": 41.716998
    },
    "UI RCP Bridgeport Seaside": {
      "lng": -73.208084,
      "lat": 41.157591
    },
    "UI RCP New Haven Fuel Cell": {
      "lng": -72.898622,
      "lat": 41.288803
    },
    "UI RCP Woodbridge FC": {
      "lng": -73.007805,
      "lat": 41.358035
    },
    "Wallingford Energy, LLC": {
      "lng": -72.835364,
      "lat": 41.448694
    },
    "Wallingford Solar": {
      "lng": -72.83737,
      "lat": 41.44175
    },
    "Water Treatment 1 & 2": {
      "lng": -72.0411,
      "lat": 41.3469
    },
    "Waterbury Generation": {
      "lng": -73.0417,
      "lat": 41.5444
    },
    "Waterside Power, LLC": {
      "lng": -73.5564,
      "lat": 41.0372
    },
    "Watertown Solar One, LLC": {
      "lng": -73.14844,
      "lat": 41.61968
    },
    "Wesleyan University Cogen 1": {
      "lng": -72.653889,
      "lat": 41.556389
    },
    "Wheelabrator Bridgeport": {
      "lng": -73.2083,
      "lat": 41.1625
    },
    "Wheelabrator Lisbon": {
      "lng": -72.041629,
      "lat": 41.584429
    },
    "Wilson Solar": {
      "lng": -71.98,
      "lat": 42.264167
    },
    "Wind Colebrook South": {
      "lng": -73.140779,
      "lat": 41.961067
    },
    "Woods Hill Solar": {
      "lng": -71.919867,
      "lat": 41.83184
    },
    "7185 13th Pl. NW": {
      "lng": -77.031405,
      "lat": 38.977175
    },
    "AOC, Capitol Power Plant": {
      "lng": -77.007443,
      "lat": 38.882704
    },
    "Benning Road": {
      "lng": -76.95561,
      "lat": 38.89829
    },
    "DC Water CHP": {
      "lng": -77.018333,
      "lat": 38.820556
    },
    "DC Water Solar": {
      "lng": -77.018944,
      "lat": 38.819244
    },
    "IGS CC, LLC": {
      "lng": -76.968186,
      "lat": 38.936172
    },
    "JBAB - Washington DC": {
      "lng": -77.009291,
      "lat": 38.852655
    },
    "Nationals": {
      "lng": -77.006676,
      "lat": 38.874155
    },
    "Ridgecrest": {
      "lng": -76.97592,
      "lat": 38.84683
    },
    "Ross Hall Central Utility Plant": {
      "lng": -77.050788,
      "lat": 38.900216
    },
    "US GSA Heating and Transmission": {
      "lng": -77.028373,
      "lat": 38.885709
    },
    "AGT001 Centerville Fuel Cell": {
      "lng": -75.62208,
      "lat": 39.75787
    },
    "Ameresco Delaware Central": {
      "lng": -75.7233,
      "lat": 39.0339
    },
    "Ameresco Delaware South": {
      "lng": -75.4342,
      "lat": 38.5992
    },
    "Brookside Newark": {
      "lng": -75.716111,
      "lat": 39.668889
    },
    "Bruce A Henry Solar Farm": {
      "lng": -75.400542,
      "lat": 38.663992
    },
    "Christiana Substation": {
      "lng": -75.5371,
      "lat": 39.7302
    },
    "Croda Atlas Point CHP": {
      "lng": -75.543056,
      "lat": 39.693333
    },
    "DD Hay Road Solar 23 LLC": {
      "lng": -75.511747,
      "lat": 39.736708
    },
    "DEC Phase II at Georgetown": {
      "lng": -75.399444,
      "lat": 38.664989
    },
    "Delaware City": {
      "lng": -75.626811,
      "lat": 39.600408
    },
    "Delaware City Refinery": {
      "lng": -75.6343,
      "lat": 39.5871
    },
    "DG AMP Solar Smyrna": {
      "lng": -75.622424,
      "lat": 39.279702
    },
    "Dover Sun Park": {
      "lng": -75.504444,
      "lat": 39.183611
    },
    "Edge Moor": {
      "lng": -75.50384,
      "lat": 39.73886
    },
    "Energy Center Dover LLC": {
      "lng": -75.547335,
      "lat": 39.149549
    },
    "Garrison Energy Center": {
      "lng": -75.499396,
      "lat": 39.187195
    },
    "Hay Road": {
      "lng": -75.5072,
      "lat": 39.7436
    },
    "Indian River": {
      "lng": -80.780246,
      "lat": 28.493176
    },
    "Kent County Wastewater Treatment Solar": {
      "lng": -75.4375,
      "lat": 38.993056
    },
    "McKee Run": {
      "lng": -75.546628,
      "lat": 39.176222
    },
    "Milford Solar Farm": {
      "lng": -75.453333,
      "lat": 38.923056
    },
    "Onyx - Allen Harim": {
      "lng": -75.288789,
      "lat": 38.717509
    },
    "Perdue Bridgeville Photovoltaic": {
      "lng": -75.602031,
      "lat": 38.757689
    },
    "Red Lion Energy Center": {
      "lng": -75.62974,
      "lat": 39.610083
    },
    "South Campus Solar": {
      "lng": -75.548387,
      "lat": 39.781498
    },
    "University of Delaware Wind Turbine": {
      "lng": -75.164903,
      "lat": 38.782931
    },
    "Van Sant": {
      "lng": -75.548172,
      "lat": 39.145036
    },
    "Warren F. Sam Beasley Pwr Station": {
      "lng": -75.623928,
      "lat": 39.279179
    },
    "West Substation": {
      "lng": -75.6289,
      "lat": 39.7283
    },
    "WHA Southbridge Solar Park CSG": {
      "lng": -75.545978,
      "lat": 39.723644
    },
    "ACE-Stanton A PV": {
      "lng": -81.17411,
      "lat": 28.48455
    },
    "ACE-Stanton PV": {
      "lng": -81.18036,
      "lat": 28.47735
    },
    "AEP Jacksonville Solar Project": {
      "lng": -81.686443,
      "lat": 30.523647
    },
    "Altamonte Mall": {
      "lng": -81.3781,
      "lat": 28.66633
    },
    "Anclote": {
      "lng": -82.788611,
      "lat": 28.184444
    },
    "Anheuser-Busch Jacksonville": {
      "lng": -81.645351,
      "lat": 30.43424
    },
    "Arvah B Hopkins": {
      "lng": -84.4,
      "lat": 30.4522
    },
    "Babcock Preserve": {
      "lng": -81.759645,
      "lat": 26.857879
    },
    "Babcock Solar Energy Center Hybrid": {
      "lng": -81.751011,
      "lat": 26.861592
    },
    "Balm Solar": {
      "lng": -82.233972,
      "lat": 27.769667
    },
    "Baptist Medical Center": {
      "lng": -81.662705,
      "lat": 30.314467
    },
    "Barefoot Bay Solar Energy Center": {
      "lng": -80.521111,
      "lat": 27.865833
    },
    "Bartow Solar Energy LLC": {
      "lng": -81.819102,
      "lat": 27.925984
    },
    "Bay County Waste to Energy": {
      "lng": -85.52067,
      "lat": 30.266057
    },
    "Bayboro": {
      "lng": -82.635278,
      "lat": 27.758056
    },
    "Bayside Power Station": {
      "lng": -82.4231,
      "lat": 27.9072
    },
    "Big Bend": {
      "lng": -82.4036,
      "lat": 27.7944
    },
    "Blue Cypress Solar Energy Center": {
      "lng": -80.537189,
      "lat": 27.609456
    },
    "Blue Heron Solar": {
      "lng": -81.129155,
      "lat": 26.701639
    },
    "Blue Springs": {
      "lng": -85.078686,
      "lat": 30.782897
    },
    "Bonnie Mine Solar": {
      "lng": -81.98717,
      "lat": 27.89008
    },
    "Brandy Branch": {
      "lng": -81.947222,
      "lat": 30.320556
    },
    "Brevard": {
      "lng": -80.828666,
      "lat": 28.39265
    },
    "Buckeye Florida LP": {
      "lng": -83.5253,
      "lat": 30.0688
    },
    "Butler Plaza": {
      "lng": -82.38,
      "lat": 29.623056
    },
    "C D McIntosh Jr Power Plant": {
      "lng": -81.9229,
      "lat": 28.0801
    },
    "Cane Island": {
      "lng": -81.533,
      "lat": 28.2764
    },
    "Cape Canaveral": {
      "lng": -80.7644,
      "lat": 28.4698
    },
    "Cattle Ranch": {
      "lng": -81.809077,
      "lat": 27.321884
    },
    "Central District Wastewater Treat Plant": {
      "lng": -80.152791,
      "lat": 25.748565
    },
    "Charles Larsen Memorial Power Plant": {
      "lng": -81.9228,
      "lat": 28.0797
    },
    "Citrus Ridge Solar": {
      "lng": -81.61632,
      "lat": 28.40078
    },
    "Citrus Solar Energy Center Hybrid": {
      "lng": -81.806863,
      "lat": 27.320919
    },
    "Clewiston Sugar House": {
      "lng": -80.9377,
      "lat": 26.7351
    },
    "Columbia Solar Power Plant": {
      "lng": -82.696145,
      "lat": 29.882644
    },
    "Coral Farms Solar Energy Center": {
      "lng": -81.897222,
      "lat": 29.756944
    },
    "CoTAL Solar Farm": {
      "lng": -84.353938,
      "lat": 30.37926
    },
    "Cotton Creek Solar Energy Center": {
      "lng": -87.343975,
      "lat": 30.841873
    },
    "Covanta Lake County Energy": {
      "lng": -81.889188,
      "lat": 28.740206
    },
    "Crystal River": {
      "lng": -82.6977,
      "lat": 28.9656
    },
    "CSL Gas Recovery": {
      "lng": -80.166526,
      "lat": 26.287966
    },
    "Curtis H. Stanton Energy Center": {
      "lng": -81.1678,
      "lat": 28.4822
    },
    "Cutrale Citrus Juices USA I": {
      "lng": -81.865273,
      "lat": 28.811828
    },
    "Cutrale Citrus Juices USA II": {
      "lng": -81.798166,
      "lat": 28.05485
    },
    "Daytona International Speedway Solar": {
      "lng": -81.064121,
      "lat": 29.17016
    },
    "Debary": {
      "lng": -81.332329,
      "lat": 28.903863
    },
    "Debary Solar Power Plant": {
      "lng": -81.330856,
      "lat": 28.899548
    },
    "Deerhaven": {
      "lng": -82.3878,
      "lat": 29.7592
    },
    "Deerhaven Renewable Energy Center": {
      "lng": -82.396388,
      "lat": 29.7675
    },
    "DeSoto Next Generation Solar Energy": {
      "lng": -81.8019,
      "lat": 27.3232
    },
    "Discovery Solar Center": {
      "lng": -80.6789,
      "lat": 28.528933
    },
    "Duette Solar Power Plant": {
      "lng": -82.061964,
      "lat": 27.585836
    },
    "Durrance": {
      "lng": -81.98,
      "lat": 27.78
    },
    "Echo River Solar": {
      "lng": -82.864333,
      "lat": 30.296207
    },
    "Egret Solar Center": {
      "lng": -82.18378,
      "lat": 30.28251
    },
    "Eight Flags Energy": {
      "lng": -81.4706,
      "lat": 30.6608
    },
    "Fernandina Beach Mill": {
      "lng": -81.455413,
      "lat": 30.681823
    },
    "Fernandina Plant": {
      "lng": -81.473,
      "lat": 30.6612
    },
    "Field Street": {
      "lng": -80.9949,
      "lat": 29.0213
    },
    "FIU Solar": {
      "lng": -80.367743,
      "lat": 25.770008
    },
    "FL Solar 4, LLC": {
      "lng": -84.2233,
      "lat": 30.2354
    },
    "Florida's Natural Growers": {
      "lng": -81.6006,
      "lat": 27.9114
    },
    "Fort Drum Energy Center": {
      "lng": -80.75812,
      "lat": 27.607892
    },
    "Fort Myers": {
      "lng": -81.7831,
      "lat": 26.6967
    },
    "G W Ivey": {
      "lng": -80.47,
      "lat": 25.4756
    },
    "G2 Energy Marion LLC": {
      "lng": -82.055452,
      "lat": 29.122882
    },
    "Georgia-Pacific Palatka Operations": {
      "lng": -81.6809,
      "lat": 29.6807
    },
    "Goldfinch": {
      "lng": -82.042005,
      "lat": 27.982089
    },
    "Grange Hall Solar": {
      "lng": -82.164656,
      "lat": 27.681278
    },
    "Greenland Energy Center": {
      "lng": -81.518415,
      "lat": 30.159131
    },
    "Gulf Clean Energy Center": {
      "lng": -87.2244,
      "lat": 30.5661
    },
    "Gulf Coast Solar Center I": {
      "lng": -86.518407,
      "lat": 30.515448
    },
    "Gulf Coast Solar Center II": {
      "lng": -86.895204,
      "lat": 30.430766
    },
    "Gulf Coast Solar Center III": {
      "lng": -87.335193,
      "lat": 30.470351
    },
    "Gulf Power Blue Indigo Energy": {
      "lng": -85.40065,
      "lat": 30.877262
    },
    "Hamilton Solar Power Plant": {
      "lng": -83.186935,
      "lat": 30.444324
    },
    "Hammock Solar": {
      "lng": -81.324953,
      "lat": 26.692646
    },
    "Hardee Power Station": {
      "lng": -81.9636,
      "lat": 27.6364
    },
    "Harmony Solar": {
      "lng": -81.108333,
      "lat": 28.155322
    },
    "Hecate Energy Blair Road LLC": {
      "lng": -81.834,
      "lat": 30.286
    },
    "Hernando": {
      "lng": -82.485733,
      "lat": 28.66898
    },
    "Hibiscus Solar Energy Center": {
      "lng": -80.3065,
      "lat": 26.7601
    },
    "Hillsborough County Resource Recovery": {
      "lng": -82.340469,
      "lat": 27.954901
    },
    "Hines Energy Complex": {
      "lng": -81.869983,
      "lat": 27.788215
    },
    "Horizon Solar Energy Center": {
      "lng": -82.058333,
      "lat": 29.665556
    },
    "Howard F Curren Advncd Wastewater Plant": {
      "lng": -82.4389,
      "lat": 27.9389
    },
    "IKEA Jacksonville Rooftop PV System": {
      "lng": -81.51,
      "lat": 30.23
    },
    "IKEA Miami 327": {
      "lng": -80.3838,
      "lat": 25.7915
    },
    "IKEA Tampa 042": {
      "lng": -82.4325,
      "lat": 27.953889
    },
    "Imeson Solar": {
      "lng": -81.714856,
      "lat": 30.456753
    },
    "Indian River (683)": {
      "lng": -80.780124,
      "lat": 28.493232
    },
    "Indian River Solar Center": {
      "lng": -80.578178,
      "lat": 27.573256
    },
    "Intercession City": {
      "lng": -81.548611,
      "lat": 28.262778
    },
    "International Paper Pensacola": {
      "lng": -87.3264,
      "lat": 30.5966
    },
    "Interstate Solar Energy Center": {
      "lng": -80.44,
      "lat": 27.5
    },
    "J D Kennedy": {
      "lng": -81.625967,
      "lat": 30.364818
    },
    "J R Kelly": {
      "lng": -82.3208,
      "lat": 29.6461
    },
    "J Woodruff": {
      "lng": -84.863869,
      "lat": 30.708571
    },
    "Jacksonville Solar": {
      "lng": -81.956377,
      "lat": 30.320748
    },
    "JED Solid Waste Mgmt Renewable Energy": {
      "lng": -81.097236,
      "lat": 28.063777
    },
    "Lake Hancock Solar": {
      "lng": -82.033889,
      "lat": 27.719139
    },
    "Lake Placid Solar Power Plant": {
      "lng": -81.364018,
      "lat": 27.331136
    },
    "Lakeland Electric Co. (FL) - Airport II": {
      "lng": -82.047303,
      "lat": 27.991144
    },
    "Lakeland Electric Co. (FL)-Airport 1": {
      "lng": -82.047295,
      "lat": 27.988523
    },
    "Lakeside Solar Center": {
      "lng": -80.74458,
      "lat": 27.21421
    },
    "Lansing Smith Generating Plant": {
      "lng": -85.7003,
      "lat": 30.2686
    },
    "Lauderdale": {
      "lng": -80.1984,
      "lat": 26.0686
    },
    "Lee County Solid Waste Energy": {
      "lng": -81.760656,
      "lat": 26.631549
    },
    "Legoland Solar": {
      "lng": -81.690172,
      "lat": 27.986163
    },
    "Lithia Solar": {
      "lng": -82.164656,
      "lat": 27.753272
    },
    "Little Manatee River Solar": {
      "lng": -82.410644,
      "lat": 27.679868
    },
    "LKL BLBD, LLC": {
      "lng": -82.0175,
      "lat": 28.066389
    },
    "Lockheed Martin Solar": {
      "lng": -81.216368,
      "lat": 28.539415
    },
    "Lockheed Martin Solar System": {
      "lng": -82.6853,
      "lat": 28.0479
    },
    "Loggerhead Solar Energy Center": {
      "lng": -80.541389,
      "lat": 27.237778
    },
    "Magnolia Solar": {
      "lng": -82.3049,
      "lat": 27.55325
    },
    "Magnolia Springs Solar Center": {
      "lng": -81.69333,
      "lat": 29.89857
    },
    "Manatee": {
      "lng": -82.3456,
      "lat": 27.6058
    },
    "Manatee County LFGTE": {
      "lng": -82.450833,
      "lat": 27.468889
    },
    "Manatee Solar Energy Center": {
      "lng": -82.355275,
      "lat": 27.607469
    },
    "Marathon Generating Plant": {
      "lng": -81.0927,
      "lat": 24.710833
    },
    "Martin": {
      "lng": -80.5628,
      "lat": 27.0536
    },
    "McKay Bay Facility": {
      "lng": -82.421182,
      "lat": 27.949528
    },
    "Miami Dade County Resource Recovery Fac": {
      "lng": -80.3566,
      "lat": 25.8356
    },
    "Miami Dade Solar Energy Center": {
      "lng": -80.49,
      "lat": 25.64
    },
    "Midulla Generating Station": {
      "lng": -81.9625,
      "lat": 27.641667
    },
    "Mosaic Co Bartow Facility": {
      "lng": -81.9185,
      "lat": 27.908
    },
    "Mosaic Co Tampa Facility": {
      "lng": -82.3903,
      "lat": 27.8605
    },
    "Mosaic New Wales Operations": {
      "lng": -82.051937,
      "lat": 27.833642
    },
    "Mosaic South Pierce Operations": {
      "lng": -81.938787,
      "lat": 27.76522
    },
    "Mulberry Cogeneration Facility": {
      "lng": -81.8775,
      "lat": 27.8489
    },
    "Nassau Solar Center": {
      "lng": -81.88411,
      "lat": 30.50391
    },
    "Northern Preserve Solar": {
      "lng": -82.226907,
      "lat": 30.2473
    },
    "Northside": {
      "lng": -81.5525,
      "lat": 30.4172
    },
    "Okeechobee Clean Energy Center": {
      "lng": -80.791111,
      "lat": 27.634167
    },
    "Okeechobee Solar": {
      "lng": -80.794784,
      "lat": 27.630392
    },
    "Okeelanta Cogeneration": {
      "lng": -80.7469,
      "lat": 26.5769
    },
    "Oleander Power Project": {
      "lng": -80.7947,
      "lat": 28.3661
    },
    "Orange Blossom Solar Center": {
      "lng": -80.57071,
      "lat": 27.59686
    },
    "Orange Cogeneration Facility": {
      "lng": -81.825061,
      "lat": 27.87082
    },
    "Orlando CoGen": {
      "lng": -81.412343,
      "lat": 28.442623
    },
    "Osceola": {
      "lng": -81.0978,
      "lat": 28.1289
    },
    "Osceola Solar Facility": {
      "lng": -81.241681,
      "lat": 28.058078
    },
    "Osprey Energy Center": {
      "lng": -81.8083,
      "lat": 28.0525
    },
    "P L Bartow": {
      "lng": -82.601759,
      "lat": 27.859535
    },
    "Palm Bay Solar": {
      "lng": -80.631554,
      "lat": 27.8421
    },
    "Palm Beach Renewable Energy Facility 1": {
      "lng": -80.141944,
      "lat": 26.771389
    },
    "Palm Beach Renewable Energy Facility 2": {
      "lng": -80.141815,
      "lat": 26.772439
    },
    "Pasco Cnty Solid Waste Resource Recovery": {
      "lng": -82.5583,
      "lat": 28.3681
    },
    "Payne Creek Solar": {
      "lng": -81.964222,
      "lat": 27.664583
    },
    "Pea Ridge": {
      "lng": -87.135391,
      "lat": 30.59208
    },
    "Peace Creek Solar": {
      "lng": -81.810111,
      "lat": 27.913306
    },
    "Pelican Solar Center": {
      "lng": -80.54726,
      "lat": 27.522
    },
    "Pembroke Lakes Mall": {
      "lng": -80.3046,
      "lat": 26.0106
    },
    "Pensacola Florida Plant": {
      "lng": -87.2525,
      "lat": 30.5958
    },
    "Perdido": {
      "lng": -87.39,
      "lat": 30.57
    },
    "Perry Solar Facility": {
      "lng": -83.560054,
      "lat": 30.119304
    },
    "Pinellas County Resource Recovery": {
      "lng": -82.6741,
      "lat": 27.8733
    },
    "Pioneer Trail Solar Energy Center": {
      "lng": -81.1,
      "lat": 29.08
    },
    "Polk": {
      "lng": -81.9897,
      "lat": 27.7286
    },
    "Port Charlotte Energy LLC": {
      "lng": -81.965278,
      "lat": 26.790833
    },
    "Port Everglades": {
      "lng": -80.1253,
      "lat": 26.0856
    },
    "Reedy Creek Central Energy Plant": {
      "lng": -81.5809,
      "lat": 28.4263
    },
    "Rinehart": {
      "lng": -81.353336,
      "lat": 28.770601
    },
    "Riviera Beach Energy Center": {
      "lng": -80.0525,
      "lat": 26.7653
    },
    "Rodeo Solar Center": {
      "lng": -81.81024,
      "lat": 27.32063
    },
    "RP-Orlando, LLC": {
      "lng": -81.182222,
      "lat": 28.488889
    },
    "S O Purdom": {
      "lng": -84.200673,
      "lat": 30.162304
    },
    "Sabal Palm Solar Center": {
      "lng": -80.352619,
      "lat": 26.782655
    },
    "Sanford": {
      "lng": -81.3256,
      "lat": 28.8419
    },
    "Santa Fe Solar Power Plant": {
      "lng": -82.432767,
      "lat": 29.52147
    },
    "Santa Rosa Energy Center": {
      "lng": -87.115,
      "lat": 30.5664
    },
    "Sarasota County LFGTE Facility": {
      "lng": -82.387,
      "lat": 27.203
    },
    "Seminole (136)": {
      "lng": -81.632778,
      "lat": 29.733056
    },
    "Seminole Energy": {
      "lng": -81.089015,
      "lat": 28.790186
    },
    "Seminole Mill": {
      "lng": -81.5978,
      "lat": 30.4178
    },
    "Shady Hills": {
      "lng": -82.558611,
      "lat": 28.3665
    },
    "Solar Park Gainesville, LLC": {
      "lng": -82.337828,
      "lat": 29.700407
    },
    "South District Wastewater Treatment Plt": {
      "lng": -80.336253,
      "lat": 25.549834
    },
    "South Energy Center": {
      "lng": -82.34,
      "lat": 29.6383
    },
    "Southfork Solar": {
      "lng": -82.191421,
      "lat": 27.573193
    },
    "Space Coast Next Gen Solar Energy": {
      "lng": -80.6811,
      "lat": 28.4586
    },
    "Springhill Gas Recovery Plant": {
      "lng": -85.426111,
      "lat": 30.928333
    },
    "St Lucie": {
      "lng": -80.246389,
      "lat": 27.348611
    },
    "Standby Generation Plant": {
      "lng": -87.2361,
      "lat": 30.4736
    },
    "Stanton A": {
      "lng": -81.166944,
      "lat": 28.488246
    },
    "Starratt Solar": {
      "lng": -81.600556,
      "lat": 30.494361
    },
    "Stock Island": {
      "lng": -81.7342,
      "lat": 24.563333
    },
    "Sub 12": {
      "lng": -84.260028,
      "lat": 30.459813
    },
    "Sunshine Gateway Solar Energy Center": {
      "lng": -82.75,
      "lat": 30.29
    },
    "Suwannee River": {
      "lng": -83.180556,
      "lat": 30.376389
    },
    "Suwannee Solar Facility": {
      "lng": -83.174114,
      "lat": 30.376254
    },
    "Sweetbay Solar Center": {
      "lng": -80.474488,
      "lat": 27.060112
    },
    "Swift Creek Chemical Complex": {
      "lng": -82.8644,
      "lat": 30.4522
    },
    "Taylor Creek Solar": {
      "lng": -81.048333,
      "lat": 28.503469
    },
    "TIA Solar": {
      "lng": -82.532816,
      "lat": 27.965797
    },
    "Tiger Bay": {
      "lng": -81.849446,
      "lat": 27.746369
    },
    "Tom G Smith": {
      "lng": -80.0677,
      "lat": 26.612767
    },
    "Trail Ridge Landfill Gas Recovery": {
      "lng": -82.040881,
      "lat": 30.227953
    },
    "Trailside Solar Center": {
      "lng": -81.427853,
      "lat": 29.774987
    },
    "Treasure Coast Energy Center": {
      "lng": -80.3775,
      "lat": 27.383889
    },
    "Trenton": {
      "lng": -82.806892,
      "lat": 29.613561
    },
    "Trenton Solar Power Plant": {
      "lng": -82.843085,
      "lat": 29.635051
    },
    "Tropicana Products Bradent": {
      "lng": -82.548341,
      "lat": 27.483358
    },
    "Turkey Point": {
      "lng": -80.3308,
      "lat": 25.4356
    },
    "Twin Lakes": {
      "lng": -81.844528,
      "lat": 29.612188
    },
    "Twin Rivers Solar Power Plant": {
      "lng": -82.968718,
      "lat": 30.466428
    },
    "Union Springs Solar Center": {
      "lng": -82.30099,
      "lat": 29.97397
    },
    "University of Florida": {
      "lng": -82.348611,
      "lat": 29.640278
    },
    "Vandolah Power Project": {
      "lng": -81.923951,
      "lat": 27.523607
    },
    "Walt Disney World Solar Facility": {
      "lng": -81.562589,
      "lat": 28.3737
    },
    "Waste Management Naples LFGTE Project": {
      "lng": -81.658055,
      "lat": 26.155833
    },
    "West County Energy Center": {
      "lng": -80.3747,
      "lat": 26.6986
    },
    "WestRock Panama City Mill": {
      "lng": -85.621103,
      "lat": 30.141984
    },
    "Wheelabrator South Broward": {
      "lng": -80.1986,
      "lat": 26.0687
    },
    "Wildflower Solar Energy Center": {
      "lng": -81.783899,
      "lat": 27.316187
    },
    "Willow Solar Energy Center": {
      "lng": -82.080693,
      "lat": 27.448142
    },
    "Wimauma Solar": {
      "lng": -82.271722,
      "lat": 27.736389
    },
    "Winston": {
      "lng": -82.0164,
      "lat": 28.0274
    },
    "Wynwood Energy Storage": {
      "lng": -80.197098,
      "lat": 25.798978
    },
    "191 Peachtree Tower": {
      "lng": -84.3869,
      "lat": 33.7583
    },
    "ADS Renewable Energy-Wolf Creek LLC": {
      "lng": -83.4348,
      "lat": 32.7755
    },
    "AL Sandersville Energy Facility": {
      "lng": -82.8608,
      "lat": 33.1189
    },
    "Albany Green Energy LLC": {
      "lng": -84.1103,
      "lat": 31.5559
    },
    "Allatoona": {
      "lng": -84.7282,
      "lat": 34.1637
    },
    "Allen B Wilson Combustion Turbine Plant": {
      "lng": -81.748294,
      "lat": 33.137711
    },
    "Apalachicola": {
      "lng": -82.06055,
      "lat": 33.33632
    },
    "Arnold Cochran": {
      "lng": -83.343779,
      "lat": 32.399366
    },
    "Athens Regional Medical Center": {
      "lng": -83.3981,
      "lat": 33.9625
    },
    "Atlanta Falcons Solar": {
      "lng": -84.400933,
      "lat": 33.755421
    },
    "Atlanta Gift Mart LP": {
      "lng": -84.3892,
      "lat": 33.7611
    },
    "Azalea Solar, LLC": {
      "lng": -82.591667,
      "lat": 32.980833
    },
    "Baconton Power": {
      "lng": -84.08,
      "lat": 31.3869
    },
    "Bainbridge Solar": {
      "lng": -84.625537,
      "lat": 30.967159
    },
    "Bank of America Plaza": {
      "lng": -84.3861,
      "lat": 33.7708
    },
    "Baxley": {
      "lng": -82.333892,
      "lat": 31.877717
    },
    "Bibb Jones": {
      "lng": -83.756164,
      "lat": 32.777747
    },
    "Blue Ridge": {
      "lng": -84.282222,
      "lat": 34.883889
    },
    "Boulevard": {
      "lng": -81.145,
      "lat": 32.0411
    },
    "Bowen": {
      "lng": -84.9222,
      "lat": 34.1256
    },
    "Brunswick Cellulose": {
      "lng": -81.521637,
      "lat": 31.175406
    },
    "Buford": {
      "lng": -84.077367,
      "lat": 34.162279
    },
    "Bulloch Neville Farms": {
      "lng": -81.67292,
      "lat": 32.384398
    },
    "Burton Dam": {
      "lng": -83.5402,
      "lat": 34.7939
    },
    "Butler Solar Farm 20": {
      "lng": -84.316547,
      "lat": 32.575542
    },
    "Butler Solar Project 103": {
      "lng": -84.275833,
      "lat": 32.561944
    },
    "Camilla Solar Energy Project": {
      "lng": -84.228721,
      "lat": 31.26955
    },
    "Camilla Solar Plant": {
      "lng": -84.131944,
      "lat": 31.183889
    },
    "Camp Solar Plant": {
      "lng": -84.543889,
      "lat": 32.964722
    },
    "Carters": {
      "lng": -84.6733,
      "lat": 34.6121
    },
    "Cedartown Battery Energy Storage Project": {
      "lng": -85.223252,
      "lat": 34.041026
    },
    "Chattahoochee Energy Facility": {
      "lng": -85.0386,
      "lat": 33.4072
    },
    "CII Methane Management III LFG Plant": {
      "lng": -83.263889,
      "lat": 33.925556
    },
    "CNN Center": {
      "lng": -84.395,
      "lat": 33.759167
    },
    "Columbia Bryson": {
      "lng": -82.196002,
      "lat": 33.429859
    },
    "Columbia Substation": {
      "lng": -82.299071,
      "lat": 33.567718
    },
    "Comer Solar": {
      "lng": -83.1439,
      "lat": 34.0942
    },
    "Coody Cochran": {
      "lng": -83.374275,
      "lat": 32.367009
    },
    "Cool Springs Solar (Hybrid)": {
      "lng": -84.680492,
      "lat": 30.747916
    },
    "Crisp Plant": {
      "lng": -83.940734,
      "lat": 31.844708
    },
    "Curry Solar": {
      "lng": -82.140318,
      "lat": 32.038819
    },
    "Dahlberg (Jackson County)": {
      "lng": -83.397721,
      "lat": 34.042267
    },
    "Dalton 2": {
      "lng": -84.931944,
      "lat": 34.718056
    },
    "Decatur County Solar Project": {
      "lng": -84.626111,
      "lat": 30.965
    },
    "Decatur Parkway Solar Project, LLC": {
      "lng": -84.633889,
      "lat": 30.991944
    },
    "Denver Braswell PV": {
      "lng": -83.8,
      "lat": 31.3
    },
    "Dougherty County Solar, LLC": {
      "lng": -84.044499,
      "lat": 31.512003
    },
    "Doyle Energy Facility": {
      "lng": -83.699577,
      "lat": 33.837699
    },
    "Dublin Solar I": {
      "lng": -82.926,
      "lat": 32.581
    },
    "Edward L Addison Generating Plant": {
      "lng": -84.3064,
      "lat": 32.9111
    },
    "Edwin I Hatch": {
      "lng": -82.3447,
      "lat": 31.9342
    },
    "Effingham Energy Facility": {
      "lng": -81.284411,
      "lat": 32.277252
    },
    "Emory Decatur Hospital": {
      "lng": -84.2831,
      "lat": 33.7919
    },
    "Emory Hillandale Hospital": {
      "lng": -84.1494,
      "lat": 33.7036
    },
    "Flint River": {
      "lng": -84.137,
      "lat": 31.6026
    },
    "Flint River Operations": {
      "lng": -84.0667,
      "lat": 32.2544
    },
    "Fort Benning Solar Facility": {
      "lng": -84.968819,
      "lat": 32.352369
    },
    "Fort Gordon Solar Facility": {
      "lng": -82.140117,
      "lat": 33.386625
    },
    "Fort Stewart Solar Facility": {
      "lng": -81.595373,
      "lat": 31.90474
    },
    "Fountain Folkston": {
      "lng": -81.960231,
      "lat": 30.916375
    },
    "Franklin Milliken": {
      "lng": -83.099118,
      "lat": 34.442071
    },
    "Freeman Avenue": {
      "lng": -84.14762,
      "lat": 32.182026
    },
    "Georgia LFG Oak Grove Plant": {
      "lng": -83.76564,
      "lat": 33.964988
    },
    "Georgia LFG Pine Ridge Plant": {
      "lng": -84.122025,
      "lat": 33.244192
    },
    "Georgia LFG Richland Creek Plant": {
      "lng": -84.033376,
      "lat": 34.127146
    },
    "Georgia Pacific Center": {
      "lng": -84.386871,
      "lat": 33.757513
    },
    "Georgia Power at Jakin GA PV": {
      "lng": -85.031,
      "lat": 31.138
    },
    "Georgia Power at Swainsboro": {
      "lng": -82.321,
      "lat": 32.605
    },
    "Georgia Power at Wadley GA": {
      "lng": -82.41,
      "lat": 32.883
    },
    "Georgia-Pacific Cedar Springs": {
      "lng": -85.0951,
      "lat": 31.166
    },
    "Graniteville Enterprise Division": {
      "lng": -81.982732,
      "lat": 33.477027
    },
    "Graphic Packaging International Augusta Mill": {
      "lng": -81.953078,
      "lat": 33.328259
    },
    "Gratis Road Solar Facility": {
      "lng": -83.697356,
      "lat": 33.844621
    },
    "Greene Durham": {
      "lng": -83.093491,
      "lat": 33.631459
    },
    "Greenville": {
      "lng": -84.714434,
      "lat": 33.045801
    },
    "GRP Franklin Renewable Energy Facility": {
      "lng": -83.330556,
      "lat": 34.376111
    },
    "GRP Madison Renewable Energy Facility": {
      "lng": -83.192902,
      "lat": 34.040559
    },
    "Guyton Community Solar": {
      "lng": -81.442846,
      "lat": 32.412773
    },
    "Harris Shiloh": {
      "lng": -84.69659,
      "lat": 32.797582
    },
    "Hartwell Energy Facility": {
      "lng": -82.819871,
      "lat": 34.339588
    },
    "Hartwell Lake": {
      "lng": -82.8219,
      "lat": 34.3564
    },
    "Hawk Road Energy Facility": {
      "lng": -84.991391,
      "lat": 33.358472
    },
    "Hazlehurst II": {
      "lng": -82.602222,
      "lat": 31.819444
    },
    "Hazlehurst III": {
      "lng": -82.602139,
      "lat": 31.832474
    },
    "Hewlett Packard Enterprise": {
      "lng": -84.2677,
      "lat": 34.0864
    },
    "Hickory Ridge Landfill Solar Project": {
      "lng": -84.336111,
      "lat": 33.665
    },
    "High Shoals Hydro (GA)": {
      "lng": -83.504932,
      "lat": 33.816547
    },
    "IKEA Savannah 490": {
      "lng": -81.173775,
      "lat": 32.177535
    },
    "Imperial Savannah LP": {
      "lng": -81.14655,
      "lat": 32.143989
    },
    "Inforum": {
      "lng": -84.3889,
      "lat": 33.7533
    },
    "Inland Paperboard Packaging Rome": {
      "lng": -85.3275,
      "lat": 34.2528
    },
    "International Paper Savanna Mill": {
      "lng": -81.124248,
      "lat": 32.100354
    },
    "Interstate Paper LLC Riceboro": {
      "lng": -81.407856,
      "lat": 31.741629
    },
    "Jack McDonough": {
      "lng": -84.4758,
      "lat": 33.8239
    },
    "Jesup Plant": {
      "lng": -81.8439,
      "lat": 31.6593
    },
    "Kimberly-Clark Solar": {
      "lng": -85.034,
      "lat": 33
    },
    "King Mill": {
      "lng": -81.991203,
      "lat": 33.485245
    },
    "Kings Bay Solar Facility": {
      "lng": -81.556238,
      "lat": 30.827627
    },
    "Lake Blackshear Project": {
      "lng": -83.941996,
      "lat": 31.847697
    },
    "Lakeland Solar Energy LLC": {
      "lng": -83.055,
      "lat": 31.026944
    },
    "Lancaster Solar GA": {
      "lng": -84.797206,
      "lat": 31.766506
    },
    "Laredo Bus Facility Solar Canopies": {
      "lng": -84.267778,
      "lat": 33.783333
    },
    "Live Oak Solar, LLC": {
      "lng": -82.102824,
      "lat": 32.423798
    },
    "Lloyd Shoals": {
      "lng": -83.8414,
      "lat": 33.3207
    },
    "Lowndes IDA": {
      "lng": -83.227515,
      "lat": 30.835452
    },
    "Lowndes Tycor Farms": {
      "lng": -83.167146,
      "lat": 30.688193
    },
    "Marine Corps Logistics Base Solar": {
      "lng": -84.093261,
      "lat": 31.560729
    },
    "MAS ASB Cogen Plant": {
      "lng": -84.394444,
      "lat": 33.655
    },
    "McCleskey Cotton": {
      "lng": -84.385575,
      "lat": 31.825063
    },
    "McIntosh (6124)": {
      "lng": -81.168347,
      "lat": 32.356287
    },
    "McIntosh Combined Cycle Facility": {
      "lng": -81.1817,
      "lat": 32.3478
    },
    "MCLB Landfill Gas to Energy": {
      "lng": -84.0675,
      "lat": 31.546111
    },
    "McManus": {
      "lng": -81.546165,
      "lat": 31.213645
    },
    "Meriwether Jackson": {
      "lng": -84.644367,
      "lat": 32.877496
    },
    "Mid-Georgia Cogeneration": {
      "lng": -83.6039,
      "lat": 32.4856
    },
    "Moody Air Force Base Solar": {
      "lng": -83.2229,
      "lat": 30.9574
    },
    "Morgan Falls": {
      "lng": -84.3841,
      "lat": 33.9681
    },
    "Mount Vernon Solar": {
      "lng": -83.638,
      "lat": 33.8388
    },
    "MPC Generating": {
      "lng": -83.6953,
      "lat": 33.8119
    },
    "Multitrade Rabun Gap, LLC": {
      "lng": -83.3786,
      "lat": 34.9536
    },
    "Murray Treadwell Farms": {
      "lng": -84.813284,
      "lat": 34.769747
    },
    "Muscogee Public Works": {
      "lng": -84.861326,
      "lat": 32.49375
    },
    "Nacoochee": {
      "lng": -83.5012,
      "lat": 34.7549
    },
    "Naval Submarine Base Kings Bay": {
      "lng": -81.5333,
      "lat": 30.7917
    },
    "North Highlands": {
      "lng": -84.9965,
      "lat": 32.4994
    },
    "Nottely": {
      "lng": -84.08765,
      "lat": 34.959153
    },
    "OE_GA3": {
      "lng": -84.147539,
      "lat": 31.196185
    },
    "Oil Dri 2 Solar": {
      "lng": -84.066,
      "lat": 30.991
    },
    "Old Midville Solar": {
      "lng": -81.98176,
      "lat": 32.820676
    },
    "Oliver Dam": {
      "lng": -85.0004,
      "lat": 32.5155
    },
    "Pawpaw Solar Plant": {
      "lng": -84.256778,
      "lat": 32.572875
    },
    "PCA-Valdosta Mill": {
      "lng": -83.303056,
      "lat": 30.694444
    },
    "Pecan Row Landfill To Electric Facility": {
      "lng": -83.3625,
      "lat": 30.815
    },
    "Piedmont Green Power": {
      "lng": -84.125556,
      "lat": 33.045278
    },
    "PInova Inc": {
      "lng": -81.478724,
      "lat": 31.164772
    },
    "Pleasant Valley Solar": {
      "lng": -83.6971,
      "lat": 33.7368
    },
    "Polk Cedartown": {
      "lng": -85.280881,
      "lat": 34.019643
    },
    "Port Wentworth Mill": {
      "lng": -81.1586,
      "lat": 32.1561
    },
    "Putnam Erickson": {
      "lng": -83.39233,
      "lat": 33.317653
    },
    "Putnam Erikson 2": {
      "lng": -83.29845,
      "lat": 33.276821
    },
    "Quitman II Solar": {
      "lng": -83.65165,
      "lat": 30.86205
    },
    "Quitman Solar": {
      "lng": -83.643279,
      "lat": 30.849005
    },
    "Richard B Russell": {
      "lng": -82.5953,
      "lat": 34.0256
    },
    "Richland": {
      "lng": -95.2263,
      "lat": 42.363152
    },
    "Richland Solar Center": {
      "lng": -83.378924,
      "lat": 32.638034
    },
    "Richmond Hayes Solar": {
      "lng": -82.022,
      "lat": 33.364
    },
    "Rincon Solar I": {
      "lng": -81.284401,
      "lat": 32.292879
    },
    "Riverwood 100 Building": {
      "lng": -84.4603,
      "lat": 33.8775
    },
    "Riverwood International Macon Mill": {
      "lng": -83.6281,
      "lat": 32.7714
    },
    "Robins": {
      "lng": -83.5822,
      "lat": 32.5792
    },
    "Robins Air Force Base Solar": {
      "lng": -83.616387,
      "lat": 32.681222
    },
    "Rocky Mountain Hydroelectric Plant": {
      "lng": -85.303942,
      "lat": 34.355538
    },
    "Savannah River Mill": {
      "lng": -81.201496,
      "lat": 32.331274
    },
    "Scherer": {
      "lng": -83.8075,
      "lat": 33.0606
    },
    "Seminole": {
      "lng": -84.931702,
      "lat": 31.065112
    },
    "Sewell Creek Energy": {
      "lng": -85.2769,
      "lat": 33.9486
    },
    "Shawnee - GA": {
      "lng": -81.980751,
      "lat": 33.107648
    },
    "Shepherd Center": {
      "lng": -84.3928,
      "lat": 33.8111
    },
    "Sibley Mill": {
      "lng": -81.99252,
      "lat": 33.487881
    },
    "Simon Solar Farm LLC": {
      "lng": -83.675833,
      "lat": 33.678056
    },
    "Sinclair Dam": {
      "lng": -83.2018,
      "lat": 33.1405
    },
    "Smarr Energy Facility": {
      "lng": -83.8464,
      "lat": 32.9842
    },
    "Snipesville": {
      "lng": -82.74946,
      "lat": 31.715814
    },
    "Solar BESS Hybrid": {
      "lng": -84.556128,
      "lat": 33.979348
    },
    "Solar Glynn": {
      "lng": -81.433,
      "lat": 31.25
    },
    "South Columbus Water Resource Facility": {
      "lng": -84.9761,
      "lat": 32.4119
    },
    "South Georgia Medical Center": {
      "lng": -83.2894,
      "lat": 30.8625
    },
    "Sowega Power Project": {
      "lng": -84.08,
      "lat": 31.3869
    },
    "SR Arlington I": {
      "lng": -84.836814,
      "lat": 31.402919
    },
    "SR Arlington II": {
      "lng": -84.830714,
      "lat": 31.415531
    },
    "SR Hazlehurst": {
      "lng": -82.575,
      "lat": 31.810278
    },
    "SR Lumpkin": {
      "lng": -84.7325,
      "lat": 32.025
    },
    "SR Odom": {
      "lng": -83.596183,
      "lat": 31.06987
    },
    "SR Perry": {
      "lng": -83.762901,
      "lat": 32.39597
    },
    "SR Snipesville II": {
      "lng": -82.715724,
      "lat": 31.708821
    },
    "SR Terrell": {
      "lng": -84.361952,
      "lat": 31.871387
    },
    "State Farm Support Center East": {
      "lng": -84.245,
      "lat": 34.086
    },
    "Stevens Creek": {
      "lng": -82.051242,
      "lat": 33.562644
    },
    "Sun Trust Plaza": {
      "lng": -84.3872,
      "lat": 33.7625
    },
    "Superior Landfill Gas Recovery": {
      "lng": -81.2722,
      "lat": 32.0303
    },
    "Talbot Energy Facility": {
      "lng": -84.6917,
      "lat": 32.5892
    },
    "Tallassee Hydro Project": {
      "lng": -83.498448,
      "lat": 33.989153
    },
    "Tallulah Falls": {
      "lng": -83.3956,
      "lat": 34.7388
    },
    "Taylor": {
      "lng": -84.3872,
      "lat": 32.4522
    },
    "Taylor County Solar": {
      "lng": -84.297222,
      "lat": 32.583056
    },
    "Tech Square Microgrid": {
      "lng": -84.3899,
      "lat": 33.7756
    },
    "Telfair Thompson": {
      "lng": -82.871786,
      "lat": 32.058559
    },
    "Tenaska Georgia Generating Station": {
      "lng": -84.9996,
      "lat": 33.3516
    },
    "Terrell Riles Stovall": {
      "lng": -84.448411,
      "lat": 31.785503
    },
    "Terrora": {
      "lng": -83.4162,
      "lat": 34.7653
    },
    "Thomas A. Smith Energy Facility": {
      "lng": -84.9175,
      "lat": 34.7094
    },
    "Tri-County Solar Facility": {
      "lng": -83.389601,
      "lat": 33.308113
    },
    "Troup RC50": {
      "lng": -84.849483,
      "lat": 32.895935
    },
    "Troup RC50 II": {
      "lng": -84.896165,
      "lat": 32.898784
    },
    "Tugalo": {
      "lng": -83.3522,
      "lat": 34.714
    },
    "Twiggs Solar": {
      "lng": -83.491239,
      "lat": 32.604997
    },
    "Upson Rocky Creek Solar Plant": {
      "lng": -84.343056,
      "lat": 32.920833
    },
    "Valdosta": {
      "lng": -83.225958,
      "lat": 30.81012
    },
    "Valdosta Prison": {
      "lng": -83.3,
      "lat": 30.9
    },
    "Valdosta Water Treatment Plant": {
      "lng": -83.2529,
      "lat": 30.9136
    },
    "Vidalia Water Treatment Plant": {
      "lng": -83.394659,
      "lat": 31.050322
    },
    "Vogtle": {
      "lng": -81.7625,
      "lat": 33.1427
    },
    "Wallace Dam": {
      "lng": -83.1574,
      "lat": 33.3502
    },
    "Walter F George": {
      "lng": -85.065237,
      "lat": 31.624501
    },
    "Walton Bainbridge Power Facility": {
      "lng": -84.547304,
      "lat": 30.910985
    },
    "Walton County Power": {
      "lng": -83.6954,
      "lat": 33.8148
    },
    "Wansley (6052)": {
      "lng": -85.032329,
      "lat": 33.41342
    },
    "Wansley (7946)": {
      "lng": -85.0403,
      "lat": 33.4083
    },
    "Wansley CC (55965)": {
      "lng": -85.036988,
      "lat": 33.406385
    },
    "Ware Avra I": {
      "lng": -82.322357,
      "lat": 31.196314
    },
    "Ware Avra II": {
      "lng": -82.322357,
      "lat": 31.196314
    },
    "Washington County Power": {
      "lng": -82.979992,
      "lat": 33.092239
    },
    "Waynesboro Community Solar": {
      "lng": -81.823938,
      "lat": 33.170593
    },
    "West Point (GA)": {
      "lng": -85.1887,
      "lat": 32.918674
    },
    "Westberry Jesup": {
      "lng": -81.932581,
      "lat": 31.602803
    },
    "WestRock Southeast, LLC.": {
      "lng": -82.8443,
      "lat": 32.5036
    },
    "White Oak Solar, LLC": {
      "lng": -82.111292,
      "lat": 33.036789
    },
    "White Pine Solar, LLC": {
      "lng": -84.256976,
      "lat": 32.506017
    },
    "Wilkinson DeFore": {
      "lng": -83.3029,
      "lat": 32.8813
    },
    "Yates": {
      "lng": -84.8986,
      "lat": 33.4622
    },
    "YKK USA Chestney": {
      "lng": -83.5456,
      "lat": 32.8061
    },
    "Yonah": {
      "lng": -83.3418,
      "lat": 34.6821
    },
    "AES Hawaii": {
      "lng": -158.106528,
      "lat": 21.303419
    },
    "AES Kekaha Solar, LLC Hybrid": {
      "lng": -159.7621,
      "lat": 22.0006
    },
    "AES Lawai Solar Hybrid": {
      "lng": -159.491974,
      "lat": 21.90823
    },
    "Aloha Solar Energy Fund 1 PK1": {
      "lng": -158.151389,
      "lat": 21.406944
    },
    "Auwahi Wind Energy Hybrid": {
      "lng": -156.318,
      "lat": 20.596
    },
    "Biomass to Energy Facility, Kauai": {
      "lng": -159.460556,
      "lat": 21.965278
    },
    "BYU - Hawaii": {
      "lng": -157.925056,
      "lat": 21.640774
    },
    "Campbell Industrial Park": {
      "lng": -158.101667,
      "lat": 21.3025
    },
    "EE Waianae Solar Project": {
      "lng": -158.187888,
      "lat": 21.452002
    },
    "Gay Robinson": {
      "lng": -159.6294,
      "lat": 21.9178
    },
    "H Power": {
      "lng": -158.098615,
      "lat": 21.300036
    },
    "Hamakua Energy Plant": {
      "lng": -155.4711,
      "lat": 20.0939
    },
    "Hana Substation": {
      "lng": -155.996111,
      "lat": 20.765833
    },
    "Hawaii Cogen": {
      "lng": -158.113889,
      "lat": 21.311667
    },
    "Hawi Wind Farm": {
      "lng": -155.850382,
      "lat": 20.257252
    },
    "HNL Emergency Power Facility": {
      "lng": -157.919444,
      "lat": 21.336389
    },
    "Kahe Generating Station": {
      "lng": -158.128853,
      "lat": 21.356395
    },
    "Kaheawa Pastures Wind Farm Hybrid": {
      "lng": -156.551,
      "lat": 20.81421
    },
    "Kaheawa Wind Power II LLC": {
      "lng": -156.536,
      "lat": 20.79467
    },
    "Kaheka Hydro": {
      "lng": -156.3569,
      "lat": 20.8886
    },
    "Kahuku Wind Power LLC": {
      "lng": -157.975,
      "lat": 21.68097
    },
    "Kahului": {
      "lng": -156.462696,
      "lat": 20.896884
    },
    "Kalaeloa Cogen Plant": {
      "lng": -158.096318,
      "lat": 21.302106
    },
    "Kalaeloa Renewable Energy Park": {
      "lng": -158.04,
      "lat": 21.328056
    },
    "Kalaeloa Solar Two": {
      "lng": -158.086944,
      "lat": 21.32
    },
    "Kalaheo Hydro": {
      "lng": -159.528581,
      "lat": 21.936075
    },
    "Kanoelehua": {
      "lng": -155.0625,
      "lat": 19.7052
    },
    "Kapaa Photovoltaic Project": {
      "lng": -159.331599,
      "lat": 22.080416
    },
    "Kapaia Power Station": {
      "lng": -159.375803,
      "lat": 21.996451
    },
    "Kapolei Solar Energy Park": {
      "lng": -158.1175,
      "lat": 21.321111
    },
    "Kawailoa Solar": {
      "lng": -158.055,
      "lat": 21.624
    },
    "Kawailoa Wind": {
      "lng": -158.040833,
      "lat": 21.610278
    },
    "Keahole": {
      "lng": -156.0283,
      "lat": 19.7317
    },
    "Kihei Solar Farm": {
      "lng": -156.4339,
      "lat": 20.7937
    },
    "KIUC Kapaia PV and BA Storage Project Hybrid": {
      "lng": -159.379194,
      "lat": 21.997272
    },
    "KRS I Anahola Solar Hybrid": {
      "lng": -159.303056,
      "lat": 22.131667
    },
    "KRS II Koloa Solar": {
      "lng": -159.45,
      "lat": 21.900833
    },
    "Ku'ia Solar": {
      "lng": -156.657889,
      "lat": 20.886013
    },
    "Lanai Solar-Electric Plant": {
      "lng": -156.9233,
      "lat": 20.7667
    },
    "Lanikuhana Solar LLC": {
      "lng": -158.02361,
      "lat": 21.4287
    },
    "Maalaea": {
      "lng": -156.492981,
      "lat": 20.80117
    },
    "Mauka FIT One": {
      "lng": -157.984444,
      "lat": 21.679722
    },
    "Miki Basin": {
      "lng": -156.9342,
      "lat": 20.79
    },
    "Na Pua Makani Wind Project": {
      "lng": -157.97,
      "lat": 21.666944
    },
    "Paia Hydroelectric Plant": {
      "lng": -156.3378,
      "lat": 20.8867
    },
    "Pakini Nui Wind Farm": {
      "lng": -155.6914,
      "lat": 18.9742
    },
    "Palaau Power Hybrid": {
      "lng": -157.0647,
      "lat": 21.106
    },
    "Pearl City Peninsula Solar Park": {
      "lng": -157.968056,
      "lat": 21.375556
    },
    "Port Allen (HI)": {
      "lng": -159.585042,
      "lat": 21.899583
    },
    "Port Allen Solar": {
      "lng": -159.581667,
      "lat": 21.901944
    },
    "Puna": {
      "lng": -155.0313,
      "lat": 19.6316
    },
    "Puna Geothermal Venture I": {
      "lng": -154.888362,
      "lat": 19.478976
    },
    "Puueo": {
      "lng": -155.0908,
      "lat": 19.7264
    },
    "Schofield Generating Station": {
      "lng": -158.0581,
      "lat": 21.4785
    },
    "Tesoro Hawaii": {
      "lng": -158.091438,
      "lat": 21.303229
    },
    "W H Hill": {
      "lng": -155.0607,
      "lat": 19.7041
    },
    "Waiau Generating Station": {
      "lng": -157.961518,
      "lat": 21.388997
    },
    "Waiau Hydro": {
      "lng": -155.1189,
      "lat": 19.7203
    },
    "Waihonu North Solar": {
      "lng": -158.013333,
      "lat": 21.470556
    },
    "Waihonu South Solar": {
      "lng": -158.016389,
      "lat": 21.468889
    },
    "Wailuku River Hydroelectric": {
      "lng": -155.14857,
      "lat": 19.712994
    },
    "Waimea": {
      "lng": -155.6955,
      "lat": 20.0252
    },
    "Wainiha Hydro": {
      "lng": -159.556142,
      "lat": 22.196292
    },
    "Waipio Peninsula": {
      "lng": -157.985594,
      "lat": 21.363514
    },
    "Waipio Solar": {
      "lng": -157.982,
      "lat": 21.456
    },
    "West Loch Solar One": {
      "lng": -158.015084,
      "lat": 21.340339
    },
    "Adair Wind Farm": {
      "lng": -94.665,
      "lat": 41.4236
    },
    "Adams Wind": {
      "lng": -94.671667,
      "lat": 40.92
    },
    "AgriReNew": {
      "lng": -90.866389,
      "lat": 41.691944
    },
    "Algona": {
      "lng": -94.2392,
      "lat": 43.0733
    },
    "Allendorf": {
      "lng": -95.6333,
      "lat": 43.4
    },
    "Alliant SBD 8501 Aegon LI": {
      "lng": -91.70405,
      "lat": 42.02425
    },
    "Alliant SBD 8601 Quad Graphics": {
      "lng": -92.0592,
      "lat": 41.7958
    },
    "Alliant SBD 8602 Marion Sub": {
      "lng": -91.5975,
      "lat": 42.0289
    },
    "Alliant SBD 9106 Rockwell CR": {
      "lng": -91.6458,
      "lat": 42.0356
    },
    "Alliant SBD 9107 JBS USA": {
      "lng": -92.8971,
      "lat": 42.056
    },
    "Alliant SBD 9201 Norplex": {
      "lng": -91.5542,
      "lat": 43.0906
    },
    "Alliant SBD 9203 Profol": {
      "lng": -91.6361,
      "lat": 41.935
    },
    "Alliant SBD 9205 A Y McDonald": {
      "lng": -90.745,
      "lat": 42.490556
    },
    "Alliant SBD 9206 Donaldson": {
      "lng": -92.1331,
      "lat": 43.3744
    },
    "Alliant SBD 9301 Prairie Farms": {
      "lng": -90.6847,
      "lat": 42.53
    },
    "Alliant SBD 9302 Aegon NP": {
      "lng": -91.7042,
      "lat": 42.0264
    },
    "Alliant SBD 9403 Aegon DC": {
      "lng": -91.6542,
      "lat": 41.9167
    },
    "Alliant SBD 9502 Eaton": {
      "lng": -93.60325,
      "lat": 42.83815
    },
    "Alliant SBD0201 Ingredion": {
      "lng": -91.66623,
      "lat": 41.97007
    },
    "Alliant SBG 9802 Toyota": {
      "lng": -91.7111,
      "lat": 42.02771
    },
    "Alta Municipal Utilities": {
      "lng": -95.3026,
      "lat": 42.6736
    },
    "Amana Society Service Company": {
      "lng": -91.89179,
      "lat": 41.79323
    },
    "Ames": {
      "lng": -93.6089,
      "lat": 42.0258
    },
    "Anchor Wind, LLC": {
      "lng": -91.234167,
      "lat": 41.850833
    },
    "Anderson Erickson": {
      "lng": -93.5765,
      "lat": 41.6014
    },
    "Anita": {
      "lng": -94.766474,
      "lat": 41.444224
    },
    "Arbor Hill Wind Farm": {
      "lng": -94.261358,
      "lat": 41.404231
    },
    "Archer Daniels Midland Cedar Rapids": {
      "lng": -91.6875,
      "lat": 41.9221
    },
    "Archer Daniels Midland Clinton": {
      "lng": -90.2097,
      "lat": 41.8206
    },
    "Archer Daniels Midland Des Moines": {
      "lng": -93.584515,
      "lat": 41.624438
    },
    "Atlantic": {
      "lng": -95,
      "lat": 41.409
    },
    "August Wind Farm": {
      "lng": -94.475628,
      "lat": 40.764914
    },
    "Bancroft": {
      "lng": -94.217075,
      "lat": 43.292628
    },
    "Barton Windpower LLC": {
      "lng": -93.091,
      "lat": 43.393
    },
    "Beaver Creek Wind": {
      "lng": -94.164636,
      "lat": 42.106964
    },
    "Bellevue": {
      "lng": -90.4167,
      "lat": 42.25
    },
    "Birch": {
      "lng": -94.474375,
      "lat": 40.767636
    },
    "Bloomfield": {
      "lng": -92.4253,
      "lat": 40.7031
    },
    "Bloomfield Municipal Utilities Solar": {
      "lng": -92.45,
      "lat": 40.75
    },
    "Brooklyn": {
      "lng": -92.4479,
      "lat": 41.7278
    },
    "Buffalo Center Wind LLC": {
      "lng": -92.759444,
      "lat": 43.385833
    },
    "Bulldog": {
      "lng": -94.4406,
      "lat": 41.2258
    },
    "Burlington (IA)": {
      "lng": -91.116667,
      "lat": 40.7412
    },
    "Cargill Corn Milling Division": {
      "lng": -92.6467,
      "lat": 41.1386
    },
    "Carroll Area Wind Farm": {
      "lng": -94.8861,
      "lat": 41.94143
    },
    "Carroll Wind Farm": {
      "lng": -94.93,
      "lat": 42.1622
    },
    "Cascade": {
      "lng": -91.011974,
      "lat": 42.29821
    },
    "CED Centerville Wind": {
      "lng": -92.854,
      "lat": 40.702
    },
    "CED Mason City Wind": {
      "lng": -93.098483,
      "lat": 43.140697
    },
    "Cedar Falls Solar Farm": {
      "lng": -92.458184,
      "lat": 42.481409
    },
    "Century": {
      "lng": -93.6369,
      "lat": 42.5644
    },
    "Cerro Gordo Wind Farm": {
      "lng": -93.453642,
      "lat": 43.061204
    },
    "Charles City Wind Farm": {
      "lng": -92.62,
      "lat": 43.0003
    },
    "Coggon": {
      "lng": -91.532705,
      "lat": 42.280827
    },
    "Contrail Wind Farm": {
      "lng": -94.831625,
      "lat": 40.667406
    },
    "Coon Rapids II": {
      "lng": -94.680777,
      "lat": 41.871043
    },
    "Coralville GT": {
      "lng": -91.563999,
      "lat": 41.671877
    },
    "Corning": {
      "lng": -94.7352,
      "lat": 40.9871
    },
    "Crane Creek Wind Energy Center": {
      "lng": -92.473611,
      "lat": 43.397778
    },
    "Crosswind Energy Project": {
      "lng": -94.9228,
      "lat": 43.0475
    },
    "Crystal Lake 3 LLC": {
      "lng": -93.8836,
      "lat": 43.3192
    },
    "Cumberland Rose": {
      "lng": -94.434722,
      "lat": 41.226944
    },
    "Davenport Water Pollution Control Plant": {
      "lng": -90.6275,
      "lat": 41.4928
    },
    "Dayton (IA)": {
      "lng": -94.069243,
      "lat": 42.260162
    },
    "Dayton Avenue Substation": {
      "lng": -93.5828,
      "lat": 42.0272
    },
    "Decorah Battery": {
      "lng": -91.793,
      "lat": 43.324
    },
    "Des Moines Wastewater Reclamation Fac": {
      "lng": -93.557743,
      "lat": 41.573415
    },
    "Diamond Trail Wind Farm": {
      "lng": -92.1642,
      "lat": 41.6744
    },
    "Dike City Power Plant": {
      "lng": -92.6264,
      "lat": 42.4622
    },
    "Downtown Generation Plant": {
      "lng": -94.46049,
      "lat": 41.30399
    },
    "Durant": {
      "lng": -90.91195,
      "lat": 41.59976
    },
    "Earl F Wisdom": {
      "lng": -95.2569,
      "lat": 43.1606
    },
    "Earlville": {
      "lng": -91.2725,
      "lat": 42.4942
    },
    "Eastern Iowa Solar": {
      "lng": -91.036295,
      "lat": 41.596959
    },
    "Eclipse Wind Farm": {
      "lng": -94.685004,
      "lat": 41.554651
    },
    "Electrifarm": {
      "lng": -92.4205,
      "lat": 42.4407
    },
    "Elk Wind Farm": {
      "lng": -91.370833,
      "lat": 42.583889
    },
    "Emery Station": {
      "lng": -93.292222,
      "lat": 43.094
    },
    "English Farms": {
      "lng": -92.40718,
      "lat": 41.57
    },
    "Estherville": {
      "lng": -94.8433,
      "lat": 43.4028
    },
    "Exira Station": {
      "lng": -94.919729,
      "lat": 41.514365
    },
    "Farmers Electric Cooperative - Kalona": {
      "lng": -91.736663,
      "lat": 41.566688
    },
    "Flying Cloud Power Partners LLC": {
      "lng": -95.2997,
      "lat": 43.3897
    },
    "Fontanelle": {
      "lng": -94.57,
      "lat": 41.338889
    },
    "Forest City Light Plant": {
      "lng": -93.635383,
      "lat": 43.263235
    },
    "Forest City Solar": {
      "lng": -93.629,
      "lat": 43.276
    },
    "FPL Energy Crystal Lake Wind II LLC": {
      "lng": -93.800002,
      "lat": 43.273731
    },
    "FPL Energy Crystal Lake Wind LLC": {
      "lng": -93.8311,
      "lat": 43.2125
    },
    "FPL Energy Story Wind LLC": {
      "lng": -93.2722,
      "lat": 42.1044
    },
    "Franklin County Wind Farm": {
      "lng": -93.336453,
      "lat": 42.606737
    },
    "Garden Wind LLC": {
      "lng": -93.373625,
      "lat": 42.224436
    },
    "Gas Turbine (IA)": {
      "lng": -92.476,
      "lat": 42.512
    },
    "George Neal North": {
      "lng": -96.361707,
      "lat": 42.299794
    },
    "George Neal South": {
      "lng": -96.3617,
      "lat": 42.3006
    },
    "Gilliam South": {
      "lng": -94.327222,
      "lat": 41.496111
    },
    "Glaciers Edge Wind Project": {
      "lng": -95.772,
      "lat": 42.862
    },
    "Golden Plains": {
      "lng": -94.016407,
      "lat": 43.430639
    },
    "Gowrie": {
      "lng": -94.288965,
      "lat": 42.282807
    },
    "Graettinger": {
      "lng": -94.7498,
      "lat": 43.2397
    },
    "Grand Junction": {
      "lng": -94.3699,
      "lat": 42.0296
    },
    "Greater Des Moines Energy Center": {
      "lng": -93.5283,
      "lat": 41.5563
    },
    "Green Energy Machine": {
      "lng": -94.754,
      "lat": 41.373
    },
    "Greenfield Wind": {
      "lng": -94.485,
      "lat": 41.29
    },
    "Grundy Center City Light Plant": {
      "lng": -92.772322,
      "lat": 42.363094
    },
    "Hancock County Wind Energy Center": {
      "lng": -93.629956,
      "lat": 43.052758
    },
    "Hardin Hilltop Wind LLC": {
      "lng": -94.365,
      "lat": 42.0822
    },
    "Harlan": {
      "lng": -95.315283,
      "lat": 41.644542
    },
    "Hawkeye Power Partners LLC": {
      "lng": -93.4775,
      "lat": 43.047222
    },
    "Hawkeye Wind Farm": {
      "lng": -92.062222,
      "lat": 42.948889
    },
    "Heartland Divide Wind Project, LLC": {
      "lng": -94.80199,
      "lat": 41.71138
    },
    "Highland Wind Project (IA)": {
      "lng": -95.656111,
      "lat": 43.080278
    },
    "Hopkinton": {
      "lng": -91.2471,
      "lat": 42.343542
    },
    "IA - City of Ames - Airport Road": {
      "lng": -93.617,
      "lat": 42.003
    },
    "Ida Grove II": {
      "lng": -95.458818,
      "lat": 42.326854
    },
    "Ida Grove Wind": {
      "lng": -95.42038,
      "lat": 42.355721
    },
    "Independence Wind Farm": {
      "lng": -91.568638,
      "lat": 42.431604
    },
    "Indianola": {
      "lng": -93.563268,
      "lat": 41.359491
    },
    "Intrepid": {
      "lng": -95.3283,
      "lat": 42.5539
    },
    "Iowa Distributed Wind Generation Project": {
      "lng": -94.140556,
      "lat": 43.034722
    },
    "Iowa Hydro LLC": {
      "lng": -90.697657,
      "lat": 42.069987
    },
    "Iowa Lakes Community College Wind Farm": {
      "lng": -94.815655,
      "lat": 43.385347
    },
    "Iowa Lakes Superior Wind Farm": {
      "lng": -94.980278,
      "lat": 43.445
    },
    "Iowa State University": {
      "lng": -93.6394,
      "lat": 42.0275
    },
    "Ivester Wind Farm": {
      "lng": -92.946436,
      "lat": 42.33327
    },
    "John Deere Dubuque Works": {
      "lng": -90.6926,
      "lat": 42.5734
    },
    "Junction Hilltop Wind Farm": {
      "lng": -94.269167,
      "lat": 42.098889
    },
    "Keokuk": {
      "lng": -91.3719,
      "lat": 40.3965
    },
    "Kirkwood Wind Turbine": {
      "lng": -91.650897,
      "lat": 41.916558
    },
    "Knoxville Battery Energy Storage": {
      "lng": -93.060973,
      "lat": 41.315962
    },
    "Knoxville Industrial": {
      "lng": -93.060973,
      "lat": 41.315962
    },
    "Kossuth": {
      "lng": -94.128733,
      "lat": 42.994681
    },
    "La Porte": {
      "lng": -92.1928,
      "lat": 42.3159
    },
    "Lake Mills": {
      "lng": -93.5322,
      "lat": 43.4172
    },
    "Lake Mills Gas Recovery": {
      "lng": -93.5589,
      "lat": 43.3864
    },
    "Lake Park": {
      "lng": -95.3153,
      "lat": 43.4301
    },
    "Lakota Wind Wind Farm": {
      "lng": -94.1475,
      "lat": 43.3839
    },
    "Lamoni Municipal Utilities": {
      "lng": -93.9326,
      "lat": 40.622
    },
    "Lansing": {
      "lng": -91.1675,
      "lat": 43.3359
    },
    "Laurel Wind Farm": {
      "lng": -92.933333,
      "lat": 41.883333
    },
    "Laurens": {
      "lng": -94.8506,
      "lat": 42.8486
    },
    "Lenox": {
      "lng": -94.5589,
      "lat": 40.88
    },
    "Leonardo Wind 1 LLC": {
      "lng": -93.367778,
      "lat": 42.498056
    },
    "Lime Creek": {
      "lng": -93.205,
      "lat": 43.2481
    },
    "Lost Lakes Wind Farm LLC": {
      "lng": -95.3233,
      "lat": 43.3456
    },
    "Louisa": {
      "lng": -91.0931,
      "lat": 41.3181
    },
    "Lundgren Wind Project": {
      "lng": -94.170833,
      "lat": 42.334722
    },
    "Luther College Wind Project": {
      "lng": -91.810833,
      "lat": 43.326944
    },
    "Macksburg Wind Project": {
      "lng": -94.103611,
      "lat": 41.209167
    },
    "Manning": {
      "lng": -95.0347,
      "lat": 41.9067
    },
    "Maquoketa 1": {
      "lng": -90.665,
      "lat": 42.035556
    },
    "Maquoketa 2": {
      "lng": -90.66,
      "lat": 42.07
    },
    "Marshalltown CTs": {
      "lng": -92.859444,
      "lat": 42.0474
    },
    "Marshalltown Generating Station": {
      "lng": -92.872778,
      "lat": 42.043333
    },
    "McGregor": {
      "lng": -91.174927,
      "lat": 43.023733
    },
    "Meadow Ridge": {
      "lng": -94.445833,
      "lat": 41.39
    },
    "Merl Parr": {
      "lng": -92.6932,
      "lat": 43.0563
    },
    "Metro Methane Recovery Facility": {
      "lng": -93.355,
      "lat": 41.598889
    },
    "Michelangelo Wind 1 LLC": {
      "lng": -93.623056,
      "lat": 41.881
    },
    "Michelangelo Wind 3 LLC": {
      "lng": -93.514,
      "lat": 42.019
    },
    "Michelangelo Wind 4 LLC": {
      "lng": -93.700556,
      "lat": 41.978
    },
    "Milford": {
      "lng": -95.148339,
      "lat": 43.326083
    },
    "Montezuma": {
      "lng": -92.5228,
      "lat": 41.586026
    },
    "Morning Light Wind Farm": {
      "lng": -94.575056,
      "lat": 41.454241
    },
    "Mt Pleasant": {
      "lng": -91.551221,
      "lat": 40.971826
    },
    "Muscatine": {
      "lng": -91.0569,
      "lat": 41.3917
    },
    "NCAH Central Utility Plant": {
      "lng": -93.579444,
      "lat": 42.046111
    },
    "New Hampton": {
      "lng": -92.3201,
      "lat": 43.0689
    },
    "New Harvest Wind Project LLC": {
      "lng": -95.439722,
      "lat": 42.159722
    },
    "NLMU Wind": {
      "lng": -91.394444,
      "lat": 40.955
    },
    "North English": {
      "lng": -92.499064,
      "lat": 41.642776
    },
    "North Generation Plant": {
      "lng": -94.455,
      "lat": 41.3164
    },
    "Northern Iowa Windfarm": {
      "lng": -93.4167,
      "lat": 43.3833
    },
    "Northern Iowa Windpower II": {
      "lng": -93.2978,
      "lat": 43.3608
    },
    "Oakdale Renewable Energy Plant": {
      "lng": -91.6033,
      "lat": 41.7053
    },
    "O'Brien Wind": {
      "lng": -95.624313,
      "lat": 43.200421
    },
    "Ogden": {
      "lng": -94.0462,
      "lat": 42.0372
    },
    "Optimum Wind 3 LLC": {
      "lng": -93.513611,
      "lat": 42.019167
    },
    "Optimum Wind 4 LLC": {
      "lng": -93.515833,
      "lat": 42.016389
    },
    "Optimum Wind 5 LLC": {
      "lng": -93.518056,
      "lat": 42.014
    },
    "Optimum Wind 6 LLC": {
      "lng": -93.523611,
      "lat": 42.006944
    },
    "Optimum Wind 7 LLC": {
      "lng": -93.430278,
      "lat": 42.028333
    },
    "Orient Wind Farm": {
      "lng": -94.417197,
      "lat": 41.202447
    },
    "Osage (IA)": {
      "lng": -92.810556,
      "lat": 43.279722
    },
    "Osceola Windpower II": {
      "lng": -95.4167,
      "lat": 43.4303
    },
    "Osceola Windpower LLC": {
      "lng": -95.4167,
      "lat": 43.4303
    },
    "Otter Creek Ethanol Poet - Ashton": {
      "lng": -95.809722,
      "lat": 43.275
    },
    "Ottumwa": {
      "lng": -92.555833,
      "lat": 41.0961
    },
    "Ottumwa City of": {
      "lng": -92.414873,
      "lat": 41.016346
    },
    "Palo Alto Wind Farm": {
      "lng": -94.548094,
      "lat": 43.206691
    },
    "Panora": {
      "lng": -94.367571,
      "lat": 41.694673
    },
    "Pella Peaking": {
      "lng": -92.9378,
      "lat": 41.4106
    },
    "Pioneer Prairie Wind Farm": {
      "lng": -92.6131,
      "lat": 43.4839
    },
    "Pleasant Hill Energy Center": {
      "lng": -93.5242,
      "lat": 41.5572
    },
    "Pocahontas Prairie Wind Farm": {
      "lng": -94.658056,
      "lat": 42.568056
    },
    "POET Biorefining - Gowrie": {
      "lng": -94.287353,
      "lat": 42.319817
    },
    "POET Biorefining - Jewell": {
      "lng": -93.6623,
      "lat": 42.3284
    },
    "POET Biorefining LLC - Corning": {
      "lng": -94.795751,
      "lat": 40.966198
    },
    "POET Biorefining-Hanlontown, LLC": {
      "lng": -93.392798,
      "lat": 43.29236
    },
    "Pomeroy Wind Farm": {
      "lng": -94.7467,
      "lat": 42.5842
    },
    "Prairie Creek": {
      "lng": -91.639167,
      "lat": 41.944039
    },
    "Prairie Wind Farm": {
      "lng": -92.703817,
      "lat": 41.4369
    },
    "Preston (IA)": {
      "lng": -90.3942,
      "lat": 42.0507
    },
    "Primghar": {
      "lng": -95.62596,
      "lat": 43.08713
    },
    "Red Rock Hydro Plant": {
      "lng": -92.980833,
      "lat": 41.369722
    },
    "Rippey Wind Farm": {
      "lng": -94.237778,
      "lat": 42.006111
    },
    "River Hills": {
      "lng": -93.619499,
      "lat": 41.590218
    },
    "Riverside": {
      "lng": -90.4481,
      "lat": 41.54
    },
    "Rock Rapids": {
      "lng": -96.169166,
      "lat": 43.429444
    },
    "Rockford": {
      "lng": -92.9468,
      "lat": 43.0489
    },
    "Roeder Family Wind Farm LLC": {
      "lng": -93.283333,
      "lat": 43.297222
    },
    "Rolling Hills Wind Farm": {
      "lng": -94.7744,
      "lat": 41.1973
    },
    "Roquette America": {
      "lng": -91.394167,
      "lat": 40.389167
    },
    "Roseman": {
      "lng": -94.753929,
      "lat": 41.370997
    },
    "Sac County Wind, LLC": {
      "lng": -94.995083,
      "lat": 42.421154
    },
    "Saratoga Wind Farm": {
      "lng": -92.280725,
      "lat": 43.448855
    },
    "Shenandoah": {
      "lng": -95.37683,
      "lat": 40.77069
    },
    "Sibley No Two": {
      "lng": -95.74966,
      "lat": 43.396851
    },
    "Sibley One": {
      "lng": -95.7261,
      "lat": 43.405
    },
    "Sky Volt": {
      "lng": -94.485,
      "lat": 41.29
    },
    "South Plant": {
      "lng": -92.4711,
      "lat": 42.7317
    },
    "South Strawberry": {
      "lng": -91.5469,
      "lat": 42.7114
    },
    "Southern Hills Wind Farm": {
      "lng": -94.4667,
      "lat": 41.1113
    },
    "Spencer Gas Turbine": {
      "lng": -95.144257,
      "lat": 43.144585
    },
    "State Center": {
      "lng": -93.16333,
      "lat": 42.01667
    },
    "Storm Lake 1": {
      "lng": -95.405,
      "lat": 42.6878
    },
    "Storm Lake II": {
      "lng": -95.405,
      "lat": 42.8417
    },
    "Story City": {
      "lng": -93.600479,
      "lat": 42.186076
    },
    "Story City Wind Project": {
      "lng": -93.599444,
      "lat": 42.208056
    },
    "Strawberry Point DPC Solar": {
      "lng": -91.5452,
      "lat": 42.6786
    },
    "Streeter Station": {
      "lng": -92.4394,
      "lat": 42.5267
    },
    "Stuart (IA)": {
      "lng": -94.3172,
      "lat": 41.503889
    },
    "Summit Lake": {
      "lng": -94.3472,
      "lat": 41.1136
    },
    "Sumner": {
      "lng": -92.0978,
      "lat": 42.8464
    },
    "Sycamore Combustion Turbine": {
      "lng": -93.6769,
      "lat": 41.6722
    },
    "Tipton": {
      "lng": -91.1321,
      "lat": 41.7717
    },
    "Top of Iowa Windfarm III": {
      "lng": -93.3208,
      "lat": 43.3597
    },
    "Traer East": {
      "lng": -92.4663,
      "lat": 42.1817
    },
    "Traer Main": {
      "lng": -92.4636,
      "lat": 42.1926
    },
    "Traer South": {
      "lng": -92.4747,
      "lat": 42.1925
    },
    "Traer Wind Project": {
      "lng": -92.4425,
      "lat": 42.161389
    },
    "Turtle Creek Wind Farm LLC": {
      "lng": -92.839506,
      "lat": 43.429604
    },
    "University of Iowa Main Power Plant": {
      "lng": -91.54,
      "lat": 41.6572
    },
    "University of Northern Iowa": {
      "lng": -92.459122,
      "lat": 42.513608
    },
    "Upland Prairie": {
      "lng": -95.349397,
      "lat": 43.207406
    },
    "Venus Wind 3 LLC": {
      "lng": -92.672222,
      "lat": 41.741944
    },
    "Victory Wind Farm": {
      "lng": -95.1292,
      "lat": 42.1169
    },
    "Vienna Wind Farm": {
      "lng": -92.776111,
      "lat": 42.159444
    },
    "Villisca Municipal Power Plant": {
      "lng": -94.9855,
      "lat": 40.9315
    },
    "Vinton": {
      "lng": -92.0222,
      "lat": 42.1697
    },
    "Walnut Wind Farm": {
      "lng": -95.2383,
      "lat": 41.4517
    },
    "Walter Scott Jr. Energy Center": {
      "lng": -95.8408,
      "lat": 41.18
    },
    "Wapello Solar LLC": {
      "lng": -91.177102,
      "lat": 41.153697
    },
    "Wapsie Valley Creamery Back Up Generator": {
      "lng": -91.891111,
      "lat": 42.477778
    },
    "Waterloo Lundquist": {
      "lng": -92.34558,
      "lat": 42.506156
    },
    "Waverly Community Wind Project": {
      "lng": -92.4711,
      "lat": 42.7317
    },
    "Waverly Municipal Electric North Plant": {
      "lng": -92.4711,
      "lat": 42.7317
    },
    "Webster City": {
      "lng": -93.81499,
      "lat": 42.471022
    },
    "Wellsburg Wind Project": {
      "lng": -92.966667,
      "lat": 42.390833
    },
    "West Bend": {
      "lng": -94.445574,
      "lat": 42.961151
    },
    "West Diesel Generation Unit": {
      "lng": -94.664394,
      "lat": 42.730291
    },
    "West Dubuque Solar": {
      "lng": -90.79225,
      "lat": 42.491754
    },
    "West Liberty": {
      "lng": -91.26405,
      "lat": 41.569128
    },
    "West Receiving": {
      "lng": -95.363333,
      "lat": 42.021667
    },
    "Whispering Willow North": {
      "lng": -93.418067,
      "lat": 42.774955
    },
    "Whispering Willow Wind Farm - East": {
      "lng": -93.206389,
      "lat": 42.630513
    },
    "Wilton": {
      "lng": -91.019456,
      "lat": 41.587748
    },
    "Wind GEM": {
      "lng": -94.339357,
      "lat": 41.469985
    },
    "Windwalkers LLC": {
      "lng": -93.221389,
      "lat": 43.384722
    },
    "Winnebago Wind Power Project": {
      "lng": -93.77,
      "lat": 43.315
    },
    "Winterset": {
      "lng": -94.0126,
      "lat": 41.3372
    },
    "Wiota": {
      "lng": -94.876389,
      "lat": 41.391944
    },
    "Wolverine": {
      "lng": -94.4447,
      "lat": 41.3931
    },
    "Albeni Falls": {
      "lng": -116.99859,
      "lat": 48.180245
    },
    "Amalgamated Sugar LLC Nampa": {
      "lng": -116.5753,
      "lat": 43.6058
    },
    "Amalgamated Sugar Twin Falls": {
      "lng": -114.4328,
      "lat": 42.5328
    },
    "American Falls": {
      "lng": -112.8764,
      "lat": 42.7778
    },
    "American Falls Solar": {
      "lng": -112.752,
      "lat": 42.824
    },
    "American Falls Solar II": {
      "lng": -112.752,
      "lat": 42.824
    },
    "Anderson Ranch": {
      "lng": -115.451517,
      "lat": 43.357066
    },
    "Arrowrock Hydroelectric Project": {
      "lng": -115.9233,
      "lat": 43.595
    },
    "Ashton": {
      "lng": -111.497326,
      "lat": 44.07863
    },
    "Bannock County LFG to Energy": {
      "lng": -112.365556,
      "lat": 42.787778
    },
    "Barber Dam": {
      "lng": -116.121269,
      "lat": 43.561001
    },
    "Bennett Creek Windfarm LLC - Mountain Home": {
      "lng": -115.4803,
      "lat": 43.0536
    },
    "Bennett Mountain Power Project": {
      "lng": -115.666637,
      "lat": 43.147153
    },
    "Big Sky Dairy Digester": {
      "lng": -114.7914,
      "lat": 42.8656
    },
    "Birch Creek Power": {
      "lng": -112.719439,
      "lat": 44.027444
    },
    "Black Canyon": {
      "lng": -116.437172,
      "lat": 43.930462
    },
    "Blind Canyon Hydro": {
      "lng": -114.822603,
      "lat": 42.69984
    },
    "Bliss": {
      "lng": -78.3047,
      "lat": 42.5367
    },
    "Boise R Diversion": {
      "lng": -116.093756,
      "lat": 43.537685
    },
    "Brownlee": {
      "lng": -116.8975,
      "lat": 44.8367
    },
    "Burley Butte Windpark": {
      "lng": -113.926605,
      "lat": 42.490341
    },
    "Bypass": {
      "lng": -114.057461,
      "lat": 42.560876
    },
    "BYUI Central Energy Facility": {
      "lng": -111.785833,
      "lat": 43.816944
    },
    "C J Strike": {
      "lng": -115.977858,
      "lat": 42.944869
    },
    "Cabinet Gorge": {
      "lng": -116.053056,
      "lat": 48.084444
    },
    "Camp Reed": {
      "lng": -115.048889,
      "lat": 42.811944
    },
    "Cargill B6 Biofactory": {
      "lng": -114.6167,
      "lat": 42.7167
    },
    "Cascade Dam": {
      "lng": -85.49888,
      "lat": 42.909408
    },
    "Cassia Gulch": {
      "lng": -115.0172,
      "lat": 42.8744
    },
    "Cassia Wind": {
      "lng": -115.0369,
      "lat": 42.8444
    },
    "Chester Diversion Hydroelectric Project": {
      "lng": -111.583611,
      "lat": 44.018333
    },
    "City Power Plant": {
      "lng": -112.04541,
      "lat": 43.491398
    },
    "Clear Lake": {
      "lng": -114.78012,
      "lat": 42.66701
    },
    "Clearwater Paper IPP Lewiston": {
      "lng": -116.976389,
      "lat": 46.423056
    },
    "Clif Bar Bakery of Twin Falls": {
      "lng": -114.412523,
      "lat": 42.540968
    },
    "Cold Springs Windfarm": {
      "lng": -115.400469,
      "lat": 43.028056
    },
    "Crystal Springs": {
      "lng": -114.50093,
      "lat": 42.622513
    },
    "Desert Meadow Windfarm": {
      "lng": -115.452025,
      "lat": 43.056622
    },
    "Dietrich Drop": {
      "lng": -114.267952,
      "lat": 42.836046
    },
    "Dry Creek Project": {
      "lng": -112.719439,
      "lat": 44.027444
    },
    "Dworshak": {
      "lng": -116.2977,
      "lat": 46.5143
    },
    "El Dorado Hydro Elk Creek": {
      "lng": -116.31577,
      "lat": 45.217784
    },
    "Evander Andrews Power Complex": {
      "lng": -115.734315,
      "lat": 43.179176
    },
    "Falls River Hydro": {
      "lng": -111.3535,
      "lat": 44.0597
    },
    "Fargo Drop": {
      "lng": -116.899722,
      "lat": 43.625
    },
    "Felt": {
      "lng": -111.283319,
      "lat": 43.913476
    },
    "Fighting Creek LFGTE Plant": {
      "lng": -116.93,
      "lat": 47.531667
    },
    "Ford Hydro LP": {
      "lng": -115.949338,
      "lat": 46.383828
    },
    "Fossil Gulch": {
      "lng": -114.949444,
      "lat": 42.858056
    },
    "Gem State": {
      "lng": -112.101852,
      "lat": 43.420221
    },
    "Golden Valley Wind Park LLC": {
      "lng": -113.925536,
      "lat": 42.436804
    },
    "Goshen Phase II": {
      "lng": -111.836381,
      "lat": 43.467057
    },
    "Grace": {
      "lng": -111.793967,
      "lat": 42.53675
    },
    "Grand View Solar Two": {
      "lng": -116.093431,
      "lat": 42.989614
    },
    "Hammett Hill Windfarm": {
      "lng": -115.463289,
      "lat": 43.006133
    },
    "Hazelton B Hydro": {
      "lng": -114.093,
      "lat": 42.6051
    },
    "Head of U Canal Hydro Project": {
      "lng": -114.3958,
      "lat": 42.7628
    },
    "Hidden Hollow Energy": {
      "lng": -116.271389,
      "lat": 43.697222
    },
    "High Mesa": {
      "lng": -115.036667,
      "lat": 42.88
    },
    "Horse Butte Wind I, LLC": {
      "lng": -111.726389,
      "lat": 43.386389
    },
    "Horseshoe Bend Hydroelectric Co": {
      "lng": -116.244193,
      "lat": 43.904085
    },
    "ID Solar": {
      "lng": -116.333,
      "lat": 43.444
    },
    "Island Park": {
      "lng": -111.395971,
      "lat": 44.418293
    },
    "Kettle Butte Digester, LLC": {
      "lng": -112.279722,
      "lat": 43.631944
    },
    "Koyle Ranch Hydroelectric Project": {
      "lng": -114.795698,
      "lat": 42.945664
    },
    "Langley Gulch Power Plant": {
      "lng": -116.819722,
      "lat": 43.904444
    },
    "Last Chance": {
      "lng": -111.705304,
      "lat": 42.603349
    },
    "Lateral 10 Ventures": {
      "lng": -114.89031,
      "lat": 42.647376
    },
    "Little Mac Project": {
      "lng": -114.66162,
      "lat": 42.629637
    },
    "Little Wood Hydro Project": {
      "lng": -114.025597,
      "lat": 43.425228
    },
    "Low Line Rapids": {
      "lng": -114.380118,
      "lat": 42.47711
    },
    "Lower Malad": {
      "lng": -114.906172,
      "lat": 42.865783
    },
    "Lower No 1": {
      "lng": -112.062499,
      "lat": 43.468679
    },
    "Lower No 2": {
      "lng": -112.062602,
      "lat": 43.468398
    },
    "Lower Salmon": {
      "lng": -114.903611,
      "lat": 42.841608
    },
    "Lucky Peak Power Plant Project": {
      "lng": -116.0583,
      "lat": 43.528
    },
    "Magic Dam Hydroelectric Project": {
      "lng": -114.3569,
      "lat": 43.2547
    },
    "Mainline Windfarm": {
      "lng": -115.412886,
      "lat": 43.04835
    },
    "Marco Ranch": {
      "lng": -114.557764,
      "lat": 42.636558
    },
    "Marsh Valley Development": {
      "lng": -112.149344,
      "lat": 42.625972
    },
    "MC6 Hydro Facility": {
      "lng": -116.355047,
      "lat": 43.485529
    },
    "Meadow Creek Project Company": {
      "lng": -111.803921,
      "lat": 43.524429
    },
    "Mile 28 Water Power Project": {
      "lng": -114.161173,
      "lat": 42.740139
    },
    "Milner Butte LFGE": {
      "lng": -114.004567,
      "lat": 42.466911
    },
    "Milner Dam Wind Park LLC": {
      "lng": -114.017297,
      "lat": 42.460629
    },
    "Milner Hydro": {
      "lng": -114.036889,
      "lat": 42.526786
    },
    "Minidoka": {
      "lng": -113.483175,
      "lat": 42.669899
    },
    "Mink Creek Hydro": {
      "lng": -111.665589,
      "lat": 42.260844
    },
    "Mora Drop Hydroelectric Project": {
      "lng": -116.47254,
      "lat": 43.460012
    },
    "Mountain Home": {
      "lng": -115.4656,
      "lat": 43.0272
    },
    "Moyie Springs": {
      "lng": -116.1752,
      "lat": 48.7339
    },
    "Mt. Home Solar 1, LLC": {
      "lng": -115.752,
      "lat": 43.129
    },
    "Murphy Flat Solar": {
      "lng": -116.439,
      "lat": 43.213
    },
    "North Gooding Main Hydro": {
      "lng": -114.5588,
      "lat": 43.0451
    },
    "Notch Butte Hydro": {
      "lng": -114.462563,
      "lat": 42.954384
    },
    "Oneida": {
      "lng": -111.748531,
      "lat": 42.267774
    },
    "Orchard Ranch Solar": {
      "lng": -116.285,
      "lat": 43.467
    },
    "Oregon Trail Wind Park": {
      "lng": -114.989722,
      "lat": 42.840556
    },
    "Palisades Dam": {
      "lng": -111.205752,
      "lat": 43.334849
    },
    "Payne's Ferry": {
      "lng": -115.010833,
      "lat": 42.825278
    },
    "Pilgrim Stage Wind Park": {
      "lng": -114.998611,
      "lat": 42.7975
    },
    "Plummer Cogen": {
      "lng": -116.890352,
      "lat": 47.331006
    },
    "Post Falls": {
      "lng": -116.953889,
      "lat": 47.703333
    },
    "Power County Wind Park North": {
      "lng": -112.748611,
      "lat": 42.738889
    },
    "Power County Wind Park South": {
      "lng": -112.757778,
      "lat": 42.703333
    },
    "Raft River Geothermal Power Plant": {
      "lng": -113.38245,
      "lat": 42.099417
    },
    "Rathdrum Combustion Turbine Project": {
      "lng": -116.867312,
      "lat": 47.80427
    },
    "Rathdrum Power, LLC": {
      "lng": -116.9203,
      "lat": 47.7858
    },
    "Rock Creek Dairy": {
      "lng": -114.615,
      "lat": 42.500278
    },
    "Rock Creek I": {
      "lng": -114.538769,
      "lat": 42.632483
    },
    "Rock Creek II": {
      "lng": -114.531489,
      "lat": 42.620724
    },
    "Rockland Wind Farm": {
      "lng": -112.901944,
      "lat": 42.674722
    },
    "Ryegrass Windfarm": {
      "lng": -115.439731,
      "lat": 43.037147
    },
    "S E Hazelton A": {
      "lng": -114.067537,
      "lat": 42.586701
    },
    "Salmon Diesel": {
      "lng": -113.885307,
      "lat": 45.183368
    },
    "Salmon Falls Wind Park": {
      "lng": -114.989167,
      "lat": 42.681944
    },
    "Sawtooth Wind Project": {
      "lng": -115.393333,
      "lat": 42.982222
    },
    "Shoshone Falls": {
      "lng": -114.4033,
      "lat": 42.5975
    },
    "Simcoe Solar": {
      "lng": -115.955371,
      "lat": 43.288439
    },
    "Simplot Leasing Don Plant": {
      "lng": -112.52944,
      "lat": 42.908412
    },
    "Smith Falls Hydro Project": {
      "lng": -116.557375,
      "lat": 48.959547
    },
    "Soda": {
      "lng": -111.696689,
      "lat": 42.64455
    },
    "South Forks Hydro": {
      "lng": -114.3102,
      "lat": 42.4937
    },
    "Swan Falls": {
      "lng": -116.3791,
      "lat": 43.2435
    },
    "Tamarack Energy Partnership": {
      "lng": -116.387134,
      "lat": 44.954853
    },
    "Thousand Springs": {
      "lng": -114.836722,
      "lat": 42.740542
    },
    "Thousand Springs Wind Park": {
      "lng": -114.967778,
      "lat": 42.870278
    },
    "Tuana Gulch Wind Park": {
      "lng": -114.969342,
      "lat": 42.836854
    },
    "Tuana Springs": {
      "lng": -115.0164,
      "lat": 42.8925
    },
    "Twin Falls (ID)": {
      "lng": -114.3553,
      "lat": 42.5892
    },
    "Two Ponds Windfarm": {
      "lng": -115.482592,
      "lat": 43.033781
    },
    "Upper Malad": {
      "lng": -114.885542,
      "lat": 42.864267
    },
    "Upper Power Plant": {
      "lng": -112.050953,
      "lat": 43.552695
    },
    "Upper Salmon A": {
      "lng": -114.924692,
      "lat": 42.767422
    },
    "Upper Salmon B": {
      "lng": -114.908236,
      "lat": 42.766397
    },
    "Wilson Lake Hydroelectric Project": {
      "lng": -114.174722,
      "lat": 42.628889
    },
    "Wolverine Creek": {
      "lng": -111.8294,
      "lat": 43.4195
    },
    "Yahoo Creek": {
      "lng": -114.988611,
      "lat": 42.770833
    },
    "1515 S Caron Road": {
      "lng": -89.0466,
      "lat": 41.9084
    },
    "2662 Freeport Solar 1 CSG": {
      "lng": -89.632719,
      "lat": 42.339696
    },
    "Adkins Energy LLC": {
      "lng": -89.803611,
      "lat": 42.362222
    },
    "AES Belleville Solar LLC": {
      "lng": -90.0135,
      "lat": 38.4872
    },
    "AES Griggs Solar, LLC": {
      "lng": -87.666461,
      "lat": 41.19128
    },
    "Agriwind": {
      "lng": -89.6236,
      "lat": 41.3017
    },
    "Albertsons at Melrose Park": {
      "lng": -87.856212,
      "lat": 41.916874
    },
    "Alden Road Harvard Solar 1": {
      "lng": -88.4988,
      "lat": 42.40028
    },
    "Alsey Station": {
      "lng": -90.43602,
      "lat": 39.569771
    },
    "Altamont": {
      "lng": -88.755547,
      "lat": 39.068747
    },
    "Amazon MDW6 Solar Project": {
      "lng": -88.11549,
      "lat": 41.6721
    },
    "Ameresco Danville Solar, LLC": {
      "lng": -87.64977,
      "lat": 40.10351
    },
    "Apple Canyon Lake Solar": {
      "lng": -90.179581,
      "lat": 42.436665
    },
    "Archer Daniels Midland Co.": {
      "lng": -88.890756,
      "lat": 39.870074
    },
    "Argonne National Laboratory CHP": {
      "lng": -87.970608,
      "lat": 41.716534
    },
    "Aurora": {
      "lng": -88.2265,
      "lat": 41.8151
    },
    "Avenue A Generator Sets": {
      "lng": -89.689416,
      "lat": 41.780735
    },
    "Baldwin Energy Complex": {
      "lng": -89.8544,
      "lat": 38.205
    },
    "Beecher Gas Recovery": {
      "lng": -87.637542,
      "lat": 41.38109
    },
    "Bennington Wind": {
      "lng": -89.085363,
      "lat": 40.96038
    },
    "Big Sky Wind LLC": {
      "lng": -89.456111,
      "lat": 41.599167
    },
    "BioUrja Renewables - Peoria, IL": {
      "lng": -89.604911,
      "lat": 40.677188
    },
    "Bishop Hill Energy LLC": {
      "lng": -90.120833,
      "lat": 41.215278
    },
    "Bishop Hill II Wind Farm": {
      "lng": -90.174167,
      "lat": 41.2325
    },
    "Bishop Hill III": {
      "lng": -90.276024,
      "lat": 41.211057
    },
    "Blackstone Wind Farm II LLC": {
      "lng": -88.53,
      "lat": 41.1406
    },
    "Blackstone Wind Farm LLC": {
      "lng": -88.6233,
      "lat": 41.1508
    },
    "Blooming Grove Wind Energy Center": {
      "lng": -88.395079,
      "lat": 40.661079
    },
    "Blue Goose Solar CSG": {
      "lng": -89.843327,
      "lat": 41.769376
    },
    "Braidwood Generation Station": {
      "lng": -88.2286,
      "lat": 41.2435
    },
    "Breese": {
      "lng": -89.524514,
      "lat": 38.607456
    },
    "Bright Stalk Wind Farm I": {
      "lng": -88.701054,
      "lat": 40.701241
    },
    "Brown County Wind Turbine": {
      "lng": -90.699166,
      "lat": 39.973333
    },
    "Bushnell": {
      "lng": -90.513814,
      "lat": 40.548586
    },
    "Byron Generating Station": {
      "lng": -89.2819,
      "lat": 42.0742
    },
    "California Ridge Wind Energy LLC": {
      "lng": -87.878333,
      "lat": 40.211667
    },
    "Calumet Energy Team, LLC": {
      "lng": -87.5563,
      "lat": 41.6835
    },
    "Camp Grove Wind Farm": {
      "lng": -89.639811,
      "lat": 41.090583
    },
    "Cardinal Point Wind": {
      "lng": -90.751828,
      "lat": 40.588568
    },
    "Carlyle": {
      "lng": -89.3583,
      "lat": 38.6106
    },
    "Carmi": {
      "lng": -88.1631,
      "lat": 38.0967
    },
    "Casey City of": {
      "lng": -87.992592,
      "lat": 39.310555
    },
    "CED Beecher Sun Solar": {
      "lng": -87.61052,
      "lat": 41.327123
    },
    "CED Champaign Solar LLC": {
      "lng": -88.291246,
      "lat": 40.138283
    },
    "CED Hilltop Solar": {
      "lng": -89.198015,
      "lat": 42.339141
    },
    "CED Peoria Solar": {
      "lng": -89.703554,
      "lat": 40.57629
    },
    "CED Spring Creek Solar": {
      "lng": -87.726108,
      "lat": 41.108211
    },
    "Cedarville Road": {
      "lng": -89.80797,
      "lat": 42.37195
    },
    "Charter Dura-Bar": {
      "lng": -88.413,
      "lat": 42.2891
    },
    "Charter Durabar at Woodstock": {
      "lng": -88.40712,
      "lat": 42.2899
    },
    "Chicago West Side Energy Center": {
      "lng": -87.678378,
      "lat": 41.870751
    },
    "CID Gas Recovery": {
      "lng": -87.577716,
      "lat": 41.658794
    },
    "Clendenin A Community Solar, LLC CSG": {
      "lng": -89.011483,
      "lat": 37.829241
    },
    "Clinton LFGTE": {
      "lng": -88.961111,
      "lat": 40.120833
    },
    "Clinton Power Station": {
      "lng": -88.8339,
      "lat": 40.1719
    },
    "Clinton Solar 4 LLC CSG": {
      "lng": -89.2751,
      "lat": 38.59587
    },
    "Com Adam 1WF-1": {
      "lng": -89.249722,
      "lat": 41.660278
    },
    "Conagra Foods at St. Elmo": {
      "lng": -88.8443,
      "lat": 39.0106
    },
    "Cordova Energy Company": {
      "lng": -90.279679,
      "lat": 41.712216
    },
    "Cortland": {
      "lng": -88.68876,
      "lat": 41.92219
    },
    "Crescent Ridge": {
      "lng": -89.5708,
      "lat": 41.25
    },
    "Crete Energy Park": {
      "lng": -87.6187,
      "lat": 41.429444
    },
    "CSG Mt. Morris 2, LLC": {
      "lng": -89.433136,
      "lat": 42.065653
    },
    "CSL Behring LLC": {
      "lng": -87.8542,
      "lat": 41.1603
    },
    "Dallman": {
      "lng": -89.602389,
      "lat": 39.754803
    },
    "Dayton Hydro": {
      "lng": -88.789656,
      "lat": 41.387792
    },
    "Dixon Hydroelectric Dam": {
      "lng": -89.481286,
      "lat": 41.845306
    },
    "Dodds (CSG)": {
      "lng": -89.842957,
      "lat": 42.379459
    },
    "Dresden Generating Station": {
      "lng": -88.27,
      "lat": 41.39
    },
    "Dressor Plains Solar, LLC": {
      "lng": -89.03123,
      "lat": 39.186321
    },
    "E D Edwards": {
      "lng": -89.6631,
      "lat": 40.5958
    },
    "EcoGrove Wind LLC": {
      "lng": -89.878428,
      "lat": 42.451481
    },
    "Edwardsville Solar II": {
      "lng": -89.997299,
      "lat": 38.814542
    },
    "Elgin Energy Center, LLC": {
      "lng": -88.244572,
      "lat": 42.000061
    },
    "Elwood Energy Facility": {
      "lng": -88.1236,
      "lat": 41.4388
    },
    "Elwood Energy Storage Center": {
      "lng": -88.222718,
      "lat": 41.896873
    },
    "ESIL-PFCHICAGO 2, LLC": {
      "lng": -87.735,
      "lat": 41.817
    },
    "ESIL-PFCHICAGO 3, LLC": {
      "lng": -87.669,
      "lat": 41.849
    },
    "Evanston Township High School": {
      "lng": -87.699556,
      "lat": 42.044038
    },
    "Exelon Solar Chicago": {
      "lng": -87.6514,
      "lat": 41.6758
    },
    "Exxonmobil Oil Corporation": {
      "lng": -88.1833,
      "lat": 41.4167
    },
    "Factory Gas Turbine": {
      "lng": -89.637282,
      "lat": 39.825934
    },
    "Farmer City": {
      "lng": -88.638761,
      "lat": 40.24611
    },
    "FedEx Chicago Solar Facility": {
      "lng": -87.789421,
      "lat": 41.753932
    },
    "Fisk": {
      "lng": -87.6533,
      "lat": 41.8508
    },
    "Five Oaks Gas Recovery": {
      "lng": -89.3683,
      "lat": 39.5678
    },
    "Flora Site A": {
      "lng": -88.4635,
      "lat": 38.6693
    },
    "Flora Site B": {
      "lng": -88.4894,
      "lat": 38.6714
    },
    "FPL Energy Illinois Wind LLC Hybrid": {
      "lng": -88.8867,
      "lat": 41.7678
    },
    "Freeburg": {
      "lng": -89.914444,
      "lat": 38.425556
    },
    "Freedom Power Project": {
      "lng": -88.85898,
      "lat": 39.103333
    },
    "Fuyao Glass Illinois Inc.": {
      "lng": -88.901629,
      "lat": 39.77896
    },
    "Gala (CSG)": {
      "lng": -89.836971,
      "lat": 42.356266
    },
    "Galesburg Solar Array": {
      "lng": -90.9484,
      "lat": 40.9199
    },
    "GE-Hitachi Solar - Morris, IL": {
      "lng": -88.272411,
      "lat": 41.381481
    },
    "Geneseo": {
      "lng": -90.148547,
      "lat": 41.451494
    },
    "Geneva Generation Facility": {
      "lng": -88.2711,
      "lat": 41.8911
    },
    "Gibson City Energy Center, LLC": {
      "lng": -88.3987,
      "lat": 40.4705
    },
    "Glacier Sands Wind Power, LLC": {
      "lng": -89.700876,
      "lat": 40.261377
    },
    "Goose Creek Power Plant": {
      "lng": -88.599867,
      "lat": 40.106931
    },
    "Grand Ridge Battery Projects": {
      "lng": -88.6836,
      "lat": 41.2269
    },
    "Grand Ridge Solar Farm": {
      "lng": -88.758341,
      "lat": 41.143421
    },
    "Grand Ridge Wind Energy Center": {
      "lng": -88.6836,
      "lat": 41.2269
    },
    "Grand Tower Energy Center, LLC": {
      "lng": -89.511161,
      "lat": 37.6577
    },
    "Granite City Works": {
      "lng": -90.1283,
      "lat": 38.6972
    },
    "Green River Wind Farm": {
      "lng": -89.557033,
      "lat": 41.620264
    },
    "Greene Valley Gas Recovery": {
      "lng": -88.084908,
      "lat": 41.73379
    },
    "GSG LLC": {
      "lng": -89.173056,
      "lat": 41.643611
    },
    "Harmony Road Solar": {
      "lng": -88.4625,
      "lat": 42.164
    },
    "Harvest Ridge  Wind Farm": {
      "lng": -88.072302,
      "lat": 39.83188
    },
    "Heartland Community College": {
      "lng": -89.019,
      "lat": 40.537
    },
    "High Trail Wind Farm LLC": {
      "lng": -88.7989,
      "lat": 40.4792
    },
    "Highland": {
      "lng": -89.6849,
      "lat": 38.7412
    },
    "Hilltopper Wind Project": {
      "lng": -89.310249,
      "lat": 39.952927
    },
    "Hoffer Plastics": {
      "lng": -88.302222,
      "lat": 41.998889
    },
    "Holland Energy Facility": {
      "lng": -88.758406,
      "lat": 39.223144
    },
    "Hoopeston Wind LLC": {
      "lng": -87.799167,
      "lat": 40.399722
    },
    "Hurricane Creek CSG": {
      "lng": -89.058709,
      "lat": 37.789506
    },
    "IGS East Central, LLC": {
      "lng": -88.043725,
      "lat": 41.471715
    },
    "IGS Frankfort 2 CSG": {
      "lng": -87.7876,
      "lat": 41.49563
    },
    "IGS Stockton DG CSG": {
      "lng": -90.011235,
      "lat": 42.348183
    },
    "IKEA Joliet Rooftop PV System": {
      "lng": -88.07,
      "lat": 41.48
    },
    "Illinois PV Fulton 1 CSG": {
      "lng": -89.967943,
      "lat": 40.698522
    },
    "IMEA Flora": {
      "lng": -88.4625,
      "lat": 38.6658
    },
    "IMEA Highland": {
      "lng": -89.6847,
      "lat": 38.7417
    },
    "Industrial Park": {
      "lng": -89.674385,
      "lat": 41.768199
    },
    "INEOS Naperville Cogeneration Plant": {
      "lng": -88.1486,
      "lat": 41.8108
    },
    "Ingredion Incorporated Argo Plant": {
      "lng": -87.8233,
      "lat": 41.7775
    },
    "Interstate": {
      "lng": -89.5891,
      "lat": 39.8233
    },
    "Iroquois Solar 1B LLC CSG": {
      "lng": -87.754371,
      "lat": 40.774047
    },
    "ITT Cogen Facility": {
      "lng": -87.628465,
      "lat": 41.83442
    },
    "Jake Energy Storage Center": {
      "lng": -88.108757,
      "lat": 41.488679
    },
    "JM Huber at Quincy": {
      "lng": -91.399685,
      "lat": 39.859083
    },
    "John A Logan College Solar": {
      "lng": -89.085297,
      "lat": 37.750571
    },
    "Joliet 29": {
      "lng": -88.123827,
      "lat": 41.494636
    },
    "Joliet 9": {
      "lng": -88.1153,
      "lat": 41.4931
    },
    "Joppa Steam": {
      "lng": -88.858889,
      "lat": 37.2094
    },
    "Kankakee Hydro Facility": {
      "lng": -87.8681,
      "lat": 41.1128
    },
    "Kankakee Solar 4 LLC": {
      "lng": -87.826519,
      "lat": 41.119258
    },
    "Keeversville Solar CSG": {
      "lng": -88.21848,
      "lat": 41.25559
    },
    "Kelly Creek Wind Project": {
      "lng": -88.1975,
      "lat": 40.9675
    },
    "Kendall Energy Facility": {
      "lng": -88.2581,
      "lat": 41.4797
    },
    "Kent School Road Solar 1": {
      "lng": -89.903444,
      "lat": 42.312841
    },
    "Kern A Community Solar, LLC CSG": {
      "lng": -88.952399,
      "lat": 37.897506
    },
    "Kincaid Generating Station": {
      "lng": -89.496389,
      "lat": 39.590556
    },
    "Kinmundy Power Plant": {
      "lng": -89.0128,
      "lat": 38.7619
    },
    "Kish CSG": {
      "lng": -88.920643,
      "lat": 42.215163
    },
    "Kishwaukee CHP Plant": {
      "lng": -89.093333,
      "lat": 42.221944
    },
    "Lafayette 2 - Kankakee Yonke": {
      "lng": -87.869294,
      "lat": 41.097265
    },
    "Lafayette 2 - McHenry Franks 1": {
      "lng": -88.596711,
      "lat": 42.251568
    },
    "Lafayette 2 - McHenry Franks 2": {
      "lng": -88.491906,
      "lat": 42.214828
    },
    "Lake Forest Hospital Central Energy Plant": {
      "lng": -87.864282,
      "lat": 42.25416
    },
    "Lake Gas Recovery": {
      "lng": -87.813738,
      "lat": 42.10625
    },
    "Lakeside": {
      "lng": -89.600512,
      "lat": 39.757327
    },
    "LaSalle Generating Station": {
      "lng": -88.669066,
      "lat": 41.245498
    },
    "Lee County Generating Station, LLC": {
      "lng": -89.4054,
      "lat": 41.8287
    },
    "Lena": {
      "lng": -89.88398,
      "lat": 42.35681
    },
    "Lincoln Generating Facility": {
      "lng": -87.943612,
      "lat": 41.393315
    },
    "Lincoln Land Wind": {
      "lng": -89.955833,
      "lat": 39.821944
    },
    "Lineage Logistics Solar": {
      "lng": -87.752,
      "lat": 41.455
    },
    "Livingston Generating Facility": {
      "lng": -88.6548,
      "lat": 40.9314
    },
    "Lockport Powerhouse": {
      "lng": -88.0789,
      "lat": 41.5697
    },
    "Lone Tree Wind, LLC": {
      "lng": -89.601872,
      "lat": 41.203214
    },
    "Long John CSG": {
      "lng": -88.65309,
      "lat": 41.69398
    },
    "Loyola University Health Plant": {
      "lng": -87.835556,
      "lat": 41.855833
    },
    "LSP University Park, LLC": {
      "lng": -87.7514,
      "lat": 41.4425
    },
    "Lukuc B Community Solar LLC CSG": {
      "lng": -89.042423,
      "lat": 37.836165
    },
    "Marengo Battery Storage LLC": {
      "lng": -88.639983,
      "lat": 42.252733
    },
    "Marengo Solar": {
      "lng": -88.63587,
      "lat": 42.25974
    },
    "Marion": {
      "lng": -88.953114,
      "lat": 37.619747
    },
    "Marlow Solar, LLC CSG": {
      "lng": -88.756379,
      "lat": 38.315279
    },
    "Mars Snackfood US": {
      "lng": -87.79218,
      "lat": 41.91708
    },
    "Marshall (IL)": {
      "lng": -87.68513,
      "lat": 39.392138
    },
    "McDonough Solar 1, LLC CSG": {
      "lng": -90.851133,
      "lat": 40.555424
    },
    "McHenry Battery Storage": {
      "lng": -88.274352,
      "lat": 42.331411
    },
    "McLeansboro": {
      "lng": -88.541331,
      "lat": 38.094683
    },
    "Mendota": {
      "lng": -89.15986,
      "lat": 41.54863
    },
    "Mendota Hills, LLC": {
      "lng": -89.0492,
      "lat": 41.7194
    },
    "MEPI GT Facility": {
      "lng": -88.8661,
      "lat": 37.2175
    },
    "Milam Gas Recovery": {
      "lng": -90.1317,
      "lat": 38.6603
    },
    "Minonk Wind Farm": {
      "lng": -88.997222,
      "lat": 40.888611
    },
    "Moline": {
      "lng": -90.534021,
      "lat": 41.509959
    },
    "Mondelez Global LLC": {
      "lng": -89.0356,
      "lat": 42.3156
    },
    "Mooseheart Power House": {
      "lng": -88.3331,
      "lat": 41.8244
    },
    "Mooseheart School Solar": {
      "lng": -88.3378,
      "lat": 41.8233
    },
    "Moraine CSG": {
      "lng": -88.843405,
      "lat": 40.379212
    },
    "Morgan Solar 1 CSG": {
      "lng": -90.55519,
      "lat": 39.82245
    },
    "Morgan Solar 1b CSG": {
      "lng": -90.55457,
      "lat": 39.82215
    },
    "Morgan Solar 2, LLC CSG": {
      "lng": -90.227099,
      "lat": 39.606529
    },
    "Morgan Solar 4, LLC CSG": {
      "lng": -90.22076,
      "lat": 39.61409
    },
    "Morris Cogeneration, LLC": {
      "lng": -88.332778,
      "lat": 41.411944
    },
    "MPEA Energy Center": {
      "lng": -87.61851,
      "lat": 41.852709
    },
    "Mt. Morris Solar CSG": {
      "lng": -89.496181,
      "lat": 42.063506
    },
    "MTHS Co-Gen Buildings": {
      "lng": -87.852222,
      "lat": 42.001944
    },
    "Museum of Science and Industry": {
      "lng": -87.582778,
      "lat": 41.790556
    },
    "Nalco": {
      "lng": -88.197156,
      "lat": 41.800909
    },
    "Nelson Energy Center": {
      "lng": -89.607222,
      "lat": 41.774444
    },
    "Newton": {
      "lng": -88.2781,
      "lat": 38.9361
    },
    "North Chicago Energy Center": {
      "lng": -87.851944,
      "lat": 42.306389
    },
    "North Ninth Street": {
      "lng": -89.0498,
      "lat": 41.9093
    },
    "Northeastern Illinois University Cogen": {
      "lng": -87.719167,
      "lat": 41.981111
    },
    "Northern Cardinal Solar SCS IL 1, LLC": {
      "lng": -88.243,
      "lat": 40.07
    },
    "NorthropGrummanSolar(Rolling Meadows,IL)": {
      "lng": -88.031041,
      "lat": 42.09972
    },
    "Northwest Community Hospital": {
      "lng": -87.993492,
      "lat": 42.067753
    },
    "Nostrand": {
      "lng": -87.837817,
      "lat": 41.207388
    },
    "Old Trail Wind Farm": {
      "lng": -88.7989,
      "lat": 40.4792
    },
    "Olmstead II CSG": {
      "lng": -89.796287,
      "lat": 42.371669
    },
    "Olney Solar II CSG": {
      "lng": -88.126791,
      "lat": 38.721601
    },
    "Orchard Hills Renewable Energy Station": {
      "lng": -89.079,
      "lat": 42.1424
    },
    "Otter Creek Wind Farm LLC": {
      "lng": -88.993862,
      "lat": 41.055385
    },
    "Pearl Station": {
      "lng": -90.614119,
      "lat": 39.449185
    },
    "Peru (IL)": {
      "lng": -89.1123,
      "lat": 41.3237
    },
    "Peterman II": {
      "lng": -87.78887,
      "lat": 41.070396
    },
    "Phoenix Solar South Farms LLC Solar Farm": {
      "lng": -88.244498,
      "lat": 40.081738
    },
    "Pike County Wind Power": {
      "lng": -90.8461,
      "lat": 39.6189
    },
    "Pilot Hill Wind Farm": {
      "lng": -88.056247,
      "lat": 40.999169
    },
    "Pinckneyville Power Plant": {
      "lng": -89.3467,
      "lat": 38.1114
    },
    "Pine Road Solar, LLC CSG": {
      "lng": -88.664448,
      "lat": 41.674386
    },
    "Pioneer Trail Wind Farm, LLC": {
      "lng": -88.021517,
      "lat": 40.46405
    },
    "Pontiac": {
      "lng": -88.625,
      "lat": 40.91806
    },
    "Powerton": {
      "lng": -89.6786,
      "lat": 40.5408
    },
    "Prairie State Generating Station": {
      "lng": -89.666944,
      "lat": 38.279167
    },
    "Prairie State Solar Project": {
      "lng": -89.579166,
      "lat": 38.208333
    },
    "Prairie View IL": {
      "lng": -88.101389,
      "lat": 41.343056
    },
    "Prairie Wolf Solar LLC": {
      "lng": -87.97198,
      "lat": 39.544605
    },
    "Presence Saint Mary of Nazareth Hospital": {
      "lng": -87.6831,
      "lat": 41.9031
    },
    "Princeton (IL)": {
      "lng": -89.464872,
      "lat": 41.375528
    },
    "Prophet CSG": {
      "lng": -89.804732,
      "lat": 41.721815
    },
    "Providence Heights Wind LLC": {
      "lng": -89.5572,
      "lat": 41.2294
    },
    "Quad Cities Generating Station": {
      "lng": -90.310278,
      "lat": 41.726111
    },
    "Raccoon Creek Power Plant": {
      "lng": -88.5392,
      "lat": 38.6996
    },
    "Radfords Run Wind Farm": {
      "lng": -89.047222,
      "lat": 40.011111
    },
    "Rail Splitter Wind Farm": {
      "lng": -89.4022,
      "lat": 40.3692
    },
    "Rantoul": {
      "lng": -88.159444,
      "lat": 40.312222
    },
    "Rantoul Solar": {
      "lng": -88.132797,
      "lat": 40.286726
    },
    "Red Bud": {
      "lng": -89.9992,
      "lat": 38.2165
    },
    "Reed Road Solar": {
      "lng": -88.240254,
      "lat": 42.110925
    },
    "Reynolds": {
      "lng": -89.641653,
      "lat": 39.806868
    },
    "Ridge Farm": {
      "lng": -87.68593,
      "lat": 39.88821
    },
    "Rochelle Energy Center": {
      "lng": -88.998611,
      "lat": 41.924722
    },
    "Rockford CS 1": {
      "lng": -89.08479,
      "lat": 42.17606
    },
    "Rockford CS 2": {
      "lng": -89.08479,
      "lat": 42.17606
    },
    "Rockford CSG": {
      "lng": -89.03686,
      "lat": 42.16572
    },
    "Rockford Energy Center": {
      "lng": -89.101202,
      "lat": 42.238558
    },
    "Rockford II Energy Center": {
      "lng": -89.1011,
      "lat": 42.2383
    },
    "Rockford Solar Farm": {
      "lng": -89.088333,
      "lat": 42.175278
    },
    "Rockton": {
      "lng": -89.0756,
      "lat": 42.4511
    },
    "Rocky Road Power, LLC": {
      "lng": -88.2397,
      "lat": 42.0931
    },
    "Route 76 Boone Solar 1": {
      "lng": -88.846141,
      "lat": 42.41894
    },
    "Saint Francis Hospital": {
      "lng": -87.6847,
      "lat": 42.0242
    },
    "Sandoval": {
      "lng": -89.149348,
      "lat": 38.589905
    },
    "Sapphire Sky Wind Energy LLC": {
      "lng": -88.584553,
      "lat": 40.2945
    },
    "Schulte 2 Community Solar": {
      "lng": -89.405223,
      "lat": 41.032235
    },
    "SCS American Bottoms 009885 Sauget, LLC": {
      "lng": -90.183286,
      "lat": 38.591745
    },
    "SCS Beltway 011754 Rock Falls, LLC": {
      "lng": -89.670331,
      "lat": 41.767012
    },
    "SCS COUNTY 012631 Champaign, LLC": {
      "lng": -89.177373,
      "lat": 40.318985
    },
    "SCS Galena 012589 Peoria, LLC": {
      "lng": -89.56913,
      "lat": 40.765278
    },
    "SCS Plainfield 011755 Naperville, LLC": {
      "lng": -88.16644,
      "lat": 41.69963
    },
    "SCS West Fairchild 011958 Danville, LLC": {
      "lng": -87.650494,
      "lat": 40.140564
    },
    "Sears Hydroelectric Plant": {
      "lng": -90.5803,
      "lat": 41.4614
    },
    "Segunda II (CSG)": {
      "lng": -88.863709,
      "lat": 41.117733
    },
    "Settlers Hill Gas Recovery": {
      "lng": -88.287721,
      "lat": 41.871914
    },
    "Settlers Trail Wind Farm LLC": {
      "lng": -87.570833,
      "lat": 40.694444
    },
    "Shady Oaks Wind Farm": {
      "lng": -89.059722,
      "lat": 41.666944
    },
    "Shelby County": {
      "lng": -88.4772,
      "lat": 39.2794
    },
    "SIUC": {
      "lng": -89.215,
      "lat": 37.711944
    },
    "Somonauk Road Solar 1": {
      "lng": -88.656288,
      "lat": 41.758266
    },
    "South Main Street": {
      "lng": -89.0504,
      "lat": 41.9092
    },
    "Southeast Chicago Energy Project": {
      "lng": -87.5449,
      "lat": 41.7181
    },
    "Square Barn Solar": {
      "lng": -88.383,
      "lat": 42.164
    },
    "State Farm": {
      "lng": -88.9605,
      "lat": 40.4535
    },
    "Streator Cayuga Ridge South": {
      "lng": -88.49,
      "lat": 40.9969
    },
    "Sugar Creek Wind One LLC": {
      "lng": -89.291944,
      "lat": 40.074167
    },
    "Sullivan (IL)": {
      "lng": -88.611944,
      "lat": 39.596667
    },
    "Sullivan B Community Solar LLC": {
      "lng": -90.641075,
      "lat": 40.469681
    },
    "SV CSG Lily Lake 2 LLC": {
      "lng": -88.478372,
      "lat": 41.954902
    },
    "SV Gardner 1 (Lutz) CSG": {
      "lng": -88.30134,
      "lat": 41.19504
    },
    "TAC-Distributed Energy Resource Hybrid": {
      "lng": -88.243573,
      "lat": 40.089797
    },
    "Tate & Lyle": {
      "lng": -88.931944,
      "lat": 39.84919
    },
    "Tazewell Gas Recovery": {
      "lng": -89.5167,
      "lat": 40.6817
    },
    "The GSI Group LLC at Assumption": {
      "lng": -89.037337,
      "lat": 39.51303
    },
    "Thornridge High School": {
      "lng": -87.5978,
      "lat": 41.6225
    },
    "Thornwood High School": {
      "lng": -87.6047,
      "lat": 41.5839
    },
    "Tilton Power Station": {
      "lng": -87.653818,
      "lat": 40.106125
    },
    "Tower Road Solar, LLC CSG": {
      "lng": -88.925188,
      "lat": 41.805224
    },
    "Triton East and West Cogen": {
      "lng": -87.845556,
      "lat": 41.916944
    },
    "Tuscola Station": {
      "lng": -88.349771,
      "lat": 39.798175
    },
    "University of Illinois Abbott Power Plt": {
      "lng": -88.241528,
      "lat": 40.105169
    },
    "University of Illinois Cogen Facility": {
      "lng": -87.6524,
      "lat": 41.8675
    },
    "University Park Energy": {
      "lng": -87.753189,
      "lat": 41.440893
    },
    "Upper Sterling": {
      "lng": -89.677526,
      "lat": 41.787115
    },
    "USS Solar Brick": {
      "lng": -89.79322,
      "lat": 39.35009
    },
    "Venice": {
      "lng": -90.1764,
      "lat": 38.6642
    },
    "Vermilion Solar 1, LLC CSG": {
      "lng": -87.6292,
      "lat": 40.12437
    },
    "Viking Solar CSG": {
      "lng": -89.995709,
      "lat": 39.405457
    },
    "Vulcan": {
      "lng": -87.97498,
      "lat": 41.10341
    },
    "Walnut Ridge Wind Farm": {
      "lng": -89.580278,
      "lat": 41.496667
    },
    "Waterloo": {
      "lng": -90.158939,
      "lat": 38.334786
    },
    "Waukegan": {
      "lng": -87.8133,
      "lat": 42.3833
    },
    "WestRock (IL)": {
      "lng": -88.28,
      "lat": 41.7697
    },
    "White Oak Energy LLC": {
      "lng": -89.046666,
      "lat": 40.590833
    },
    "Whiteside Solar 1 CSG": {
      "lng": -90.15885,
      "lat": 41.85691
    },
    "Whitney Hill Wind Power LLC": {
      "lng": -89.310249,
      "lat": 39.952927
    },
    "Will County": {
      "lng": -88.0629,
      "lat": 41.6334
    },
    "Winnetka": {
      "lng": -87.731025,
      "lat": 42.115589
    },
    "Wolfcastle": {
      "lng": -88.8308,
      "lat": 41.9506
    },
    "Woodland Landfill Gas Recovery": {
      "lng": -88.27452,
      "lat": 41.981004
    },
    "Woodlawn CSG": {
      "lng": -87.594633,
      "lat": 41.392408
    },
    "Woodlawn II (CSG)": {
      "lng": -87.594818,
      "lat": 41.392408
    },
    "Zion Energy Center": {
      "lng": -87.895,
      "lat": 42.4776
    },
    "Zion Landfill Gas to Energy Facility": {
      "lng": -87.8861,
      "lat": 42.4803
    },
    "A B Brown Generating Station": {
      "lng": -87.715,
      "lat": 37.9053
    },
    "AEP Churubusco NaS": {
      "lng": -85.3149,
      "lat": 41.2326
    },
    "Alcoa Allowance Management Inc": {
      "lng": -87.3328,
      "lat": 37.915
    },
    "Anderson": {
      "lng": -85.7389,
      "lat": 40.0529
    },
    "Anderson 3": {
      "lng": -85.642033,
      "lat": 40.081698
    },
    "Anderson 4": {
      "lng": -85.636079,
      "lat": 40.082667
    },
    "Anderson 5": {
      "lng": -85.632184,
      "lat": 40.085942
    },
    "ArcelorMittal Burns Harbor": {
      "lng": -87.1387,
      "lat": 41.635
    },
    "Belmont": {
      "lng": -86.193056,
      "lat": 39.72
    },
    "Benton County Wind Farm": {
      "lng": -87.4886,
      "lat": 40.695
    },
    "BioTown Ag": {
      "lng": -86.879682,
      "lat": 40.765611
    },
    "Bitter Ridge Wind Farm, LLC": {
      "lng": -85.113149,
      "lat": 40.360533
    },
    "Bluff Point Wind Facility": {
      "lng": -84.9769,
      "lat": 40.319
    },
    "Bos Dairy, LLC": {
      "lng": -87.2328,
      "lat": 41.1153
    },
    "BP Whiting Business Unit": {
      "lng": -87.4803,
      "lat": 41.6703
    },
    "Bunge North America East LLC": {
      "lng": -84.933264,
      "lat": 40.846525
    },
    "C. C. Perry K Steam Plant": {
      "lng": -86.1667,
      "lat": 39.7631
    },
    "Camp Atterbury Microgrid Hybrid": {
      "lng": -86.025,
      "lat": 39.3617
    },
    "Caterpillar": {
      "lng": -86.8447,
      "lat": 40.4175
    },
    "Cayuga": {
      "lng": -87.4244,
      "lat": 39.9242
    },
    "Centerville Solar Park": {
      "lng": -85.003765,
      "lat": 39.828501
    },
    "Clifty Creek": {
      "lng": -85.4206,
      "lat": 38.7378
    },
    "Columbia City Solar Park": {
      "lng": -85.505304,
      "lat": 41.175519
    },
    "Covanta Indianapolis Energy": {
      "lng": -86.189075,
      "lat": 39.734456
    },
    "Crane Battery Energy Storage System": {
      "lng": -86.886417,
      "lat": 38.813511
    },
    "Crane Solar Facility": {
      "lng": -86.8842,
      "lat": 38.8164
    },
    "Crawfordsville 2 Solar Park": {
      "lng": -86.88841,
      "lat": 40.055753
    },
    "Crawfordsville 3 Solar Park": {
      "lng": -86.896811,
      "lat": 40.078126
    },
    "Crawfordsville 5 Solar Park": {
      "lng": -86.85987,
      "lat": 40.017668
    },
    "Crawfordsville Power Plant": {
      "lng": -86.8992,
      "lat": 40.048889
    },
    "Crawfordsville Solar Park 4": {
      "lng": -86.880819,
      "lat": 40.049684
    },
    "Decatur Co. Solar RES (IN)": {
      "lng": -85.506398,
      "lat": 39.337893
    },
    "Deer Creek PV": {
      "lng": -85.64,
      "lat": 40.51
    },
    "Deercroft I & II LFGTE": {
      "lng": -86.9036,
      "lat": 41.6514
    },
    "Duke Building 129": {
      "lng": -86.245278,
      "lat": 39.910833
    },
    "Duke Building 87": {
      "lng": -86.255556,
      "lat": 39.890833
    },
    "Duke Building 98": {
      "lng": -86.26,
      "lat": 39.904722
    },
    "Earthmovers LFGTE": {
      "lng": -85.975161,
      "lat": 41.622942
    },
    "East Chicago": {
      "lng": -87.469167,
      "lat": 41.632222
    },
    "Edwardsport Generating Station": {
      "lng": -87.2472,
      "lat": 38.8067
    },
    "Elkhart": {
      "lng": -85.965,
      "lat": 41.6928
    },
    "Ellettsville Solar RES": {
      "lng": -86.656796,
      "lat": 39.25482
    },
    "Evonik Industries AG": {
      "lng": -86.939136,
      "lat": 40.387242
    },
    "Expander Turbine": {
      "lng": -87.4233,
      "lat": 41.6836
    },
    "F B Culley Generating Station": {
      "lng": -87.3275,
      "lat": 37.9111
    },
    "Fowler Ridge IV Wind Farm LLC": {
      "lng": -87.378056,
      "lat": 40.620556
    },
    "Fowler Ridge Wind Farm LLC": {
      "lng": -87.3336,
      "lat": 40.583889
    },
    "Gas City Solar Park": {
      "lng": -85.570279,
      "lat": 40.481168
    },
    "Georgetown Substation": {
      "lng": -86.24405,
      "lat": 39.902556
    },
    "Gibson": {
      "lng": -87.765833,
      "lat": 38.372222
    },
    "Green Cow Power": {
      "lng": -85.927204,
      "lat": 41.536735
    },
    "Greenfield Solar Park": {
      "lng": -85.816507,
      "lat": 39.793847
    },
    "Griffith": {
      "lng": -87.436111,
      "lat": 41.521389
    },
    "Headwaters Wind Farm II LLC": {
      "lng": -85.206694,
      "lat": 40.051694
    },
    "Headwaters Wind Farm LLC": {
      "lng": -85.013333,
      "lat": 40.066667
    },
    "Heat Recovery Coke Facility": {
      "lng": -87.42,
      "lat": 41.6789
    },
    "Hendricks Regional Health": {
      "lng": -86.501588,
      "lat": 39.761513
    },
    "Henry County Generating Station": {
      "lng": -85.5039,
      "lat": 39.9528
    },
    "Henryville Solar RES": {
      "lng": -85.7773,
      "lat": 38.5955
    },
    "Hidden View Dairy, LLC": {
      "lng": -87.0725,
      "lat": 41.032778
    },
    "Hobart Solar, LLC": {
      "lng": -87.308333,
      "lat": 41.529444
    },
    "Hoosier Energy Lawrence Co Station": {
      "lng": -86.4511,
      "lat": 38.8003
    },
    "Hoosier Wind Project LLC": {
      "lng": -87.2631,
      "lat": 40.7178
    },
    "Hurricane Creek Lift Station": {
      "lng": -86.1858,
      "lat": 39.5879
    },
    "IMPA Anderson Solar Park": {
      "lng": -85.737129,
      "lat": 40.051556
    },
    "IMPA Anderson Solar Park 2": {
      "lng": -85.690907,
      "lat": 40.138307
    },
    "IMPA Crawfordsville Solar Park": {
      "lng": -86.887222,
      "lat": 40.053056
    },
    "IMPA Frankton Solar Park": {
      "lng": -85.774722,
      "lat": 40.216389
    },
    "IMPA Huntingburg Solar Park": {
      "lng": -86.968074,
      "lat": 38.319623
    },
    "IMPA Pendleton Solar Park": {
      "lng": -85.770858,
      "lat": 39.988564
    },
    "IMPA Peru Solar Park": {
      "lng": -86.045556,
      "lat": 40.767778
    },
    "IMPA Rensselaer Solar Park": {
      "lng": -87.136389,
      "lat": 40.946111
    },
    "IMPA Richmond Solar Park": {
      "lng": -84.895,
      "lat": 39.801111
    },
    "IMPA Tell City Solar Park": {
      "lng": -86.7375,
      "lat": 37.946111
    },
    "IMPA Washington Solar Park": {
      "lng": -87.209215,
      "lat": 38.653455
    },
    "IND Community Solar Farm 1st Phase": {
      "lng": -86.315525,
      "lat": 39.695209
    },
    "IND Solar Farm (Phase IIA)": {
      "lng": -86.321667,
      "lat": 39.694722
    },
    "Indiana Crossroads Wind Farm LLC": {
      "lng": -86.8695,
      "lat": 40.6631
    },
    "Indiana Harbor E 5 AC Station": {
      "lng": -87.4214,
      "lat": 41.6844
    },
    "Indiana Harbor West": {
      "lng": -87.452325,
      "lat": 41.663765
    },
    "Indianapolis Motor Speedway Solar PV": {
      "lng": -86.233611,
      "lat": 39.79
    },
    "Indy Grocers": {
      "lng": -86.237732,
      "lat": 39.737936
    },
    "INDY III": {
      "lng": -86.314473,
      "lat": 39.722966
    },
    "Indy Solar I, LLC": {
      "lng": -85.985556,
      "lat": 39.662778
    },
    "Indy Solar II, LLC": {
      "lng": -85.985556,
      "lat": 39.662778
    },
    "Indy Solar III, LLC": {
      "lng": -86.259722,
      "lat": 39.659444
    },
    "IPL - Eagle Valley Generating Station": {
      "lng": -86.4183,
      "lat": 39.48517
    },
    "IPL - Harding Street Station (EW Stout)": {
      "lng": -86.196867,
      "lat": 39.711319
    },
    "IPL - Petersburg Generating Station": {
      "lng": -87.2525,
      "lat": 38.5281
    },
    "Jackson Co. Solar RES": {
      "lng": -86.032969,
      "lat": 38.881719
    },
    "Jay County LFGTE": {
      "lng": -85.088192,
      "lat": 40.379703
    },
    "Johnson Co. Solar RES": {
      "lng": -86.068353,
      "lat": 39.518026
    },
    "Jordan Creek Wind Farm, LLC": {
      "lng": -87.399818,
      "lat": 40.381614
    },
    "Kokomo Solar 1, LLC": {
      "lng": -86.146,
      "lat": 40.473
    },
    "Lanesville Solar RES": {
      "lng": -85.9864,
      "lat": 38.2717
    },
    "Lawrenceburg Energy Facility": {
      "lng": -84.8669,
      "lat": 39.0911
    },
    "Lenape II": {
      "lng": -86.042778,
      "lat": 39.776667
    },
    "Liberty I & II LFGTE": {
      "lng": -86.704444,
      "lat": 40.885556
    },
    "Lilly Technical Center": {
      "lng": -86.183056,
      "lat": 39.7475
    },
    "Lincoln Solar": {
      "lng": -87.260278,
      "lat": 41.462778
    },
    "Logansport Solar": {
      "lng": -86.388288,
      "lat": 40.760083
    },
    "M. G. Emmett J. Bean Federal Center": {
      "lng": -86.014444,
      "lat": 39.853056
    },
    "Marion Solar LNG": {
      "lng": -86.238056,
      "lat": 39.913056
    },
    "Markland": {
      "lng": -84.964,
      "lat": 38.7795
    },
    "Maywood Photovoltaic Project": {
      "lng": -86.218056,
      "lat": 39.743889
    },
    "McDonald Solar Farm, LLC": {
      "lng": -87.474,
      "lat": 39.59458
    },
    "Meadow Lake Wind Farm II LLC": {
      "lng": -87.0242,
      "lat": 40.6431
    },
    "Meadow Lake Wind Farm III LLC": {
      "lng": -87.0194,
      "lat": 40.5986
    },
    "Meadow Lake Wind Farm IV": {
      "lng": -87.0328,
      "lat": 40.6778
    },
    "Meadow Lake Wind Farm LLC": {
      "lng": -86.935,
      "lat": 40.6275
    },
    "Meadow Lake Wind Farm V LLC": {
      "lng": -87.021111,
      "lat": 40.716111
    },
    "Meadow Lake Wind Farm VI LLC": {
      "lng": -87.127011,
      "lat": 40.692552
    },
    "Merom": {
      "lng": -87.5108,
      "lat": 39.0694
    },
    "Michigan City Generating Station": {
      "lng": -86.9086,
      "lat": 41.7208
    },
    "Middlebury Solar": {
      "lng": -85.6875,
      "lat": 41.6972
    },
    "Montpelier Electric Gen Station": {
      "lng": -85.3057,
      "lat": 40.6206
    },
    "Munster Landfill Gas-to-Energy": {
      "lng": -87.509067,
      "lat": 41.524386
    },
    "Nabb Battery Energy Storage System": {
      "lng": -85.694574,
      "lat": 38.501908
    },
    "New Castle Solar RES": {
      "lng": -85.403423,
      "lat": 39.854126
    },
    "New Haven Solar RES": {
      "lng": -86.9889,
      "lat": 38.1941
    },
    "Noblesville": {
      "lng": -85.9714,
      "lat": 40.0969
    },
    "Norway (IN)": {
      "lng": -86.76,
      "lat": 40.7803
    },
    "Oak Hill Solar Array": {
      "lng": -87.53,
      "lat": 37.99
    },
    "Oak Ridge LFGTE": {
      "lng": -86.342144,
      "lat": 40.721322
    },
    "Oakdale": {
      "lng": -86.7528,
      "lat": 40.6564
    },
    "OlivePV": {
      "lng": -86.49,
      "lat": 41.7
    },
    "Osprey Point RES": {
      "lng": -87.506111,
      "lat": 39.059167
    },
    "Pastime Farm, LLC": {
      "lng": -87.18155,
      "lat": 39.52086
    },
    "Peru (IN)": {
      "lng": -86.0575,
      "lat": 40.7551
    },
    "Peru 2": {
      "lng": -86.105014,
      "lat": 40.758183
    },
    "POET Biorefining - North Manchester, LLC": {
      "lng": -85.803585,
      "lat": 40.941967
    },
    "POET Biorefining - Portland": {
      "lng": -85.025545,
      "lat": 40.414455
    },
    "POET Biorefining - Shelbyville": {
      "lng": -85.822,
      "lat": 39.56
    },
    "POET Biorefining- Alexandria, LLC": {
      "lng": -85.654444,
      "lat": 40.294347
    },
    "Portage Solar": {
      "lng": -87.1555,
      "lat": 41.5611
    },
    "Portside Energy": {
      "lng": -87.1728,
      "lat": 41.6317
    },
    "Prairie View I & II LFGTE": {
      "lng": -86.1658,
      "lat": 41.4936
    },
    "Prairies Edge Generating Facility": {
      "lng": -87.307136,
      "lat": 41.06058
    },
    "Purdue University-Wade Utility": {
      "lng": -86.9122,
      "lat": 40.4172
    },
    "R Gallagher": {
      "lng": -85.8381,
      "lat": 38.2636
    },
    "R M Schahfer Generating Station": {
      "lng": -87.0261,
      "lat": 41.2164
    },
    "Randolph Eastern School Wind Turbine": {
      "lng": -84.8219,
      "lat": 40.2097
    },
    "Rensselaer City Light Plant": {
      "lng": -87.154472,
      "lat": 40.939528
    },
    "Rensselaer Solar Site 2": {
      "lng": -87.14817,
      "lat": 40.949786
    },
    "Richmond (IN)": {
      "lng": -84.965682,
      "lat": 39.839572
    },
    "Richmond 5": {
      "lng": -84.874547,
      "lat": 39.805613
    },
    "Richmond Solar Park 3": {
      "lng": -84.825585,
      "lat": 39.83901
    },
    "Richmond Solar Park 4": {
      "lng": -84.92966,
      "lat": 39.862889
    },
    "Richmond Solar Site 2": {
      "lng": -84.816212,
      "lat": 39.833452
    },
    "Riverstart Solar Park  LLC": {
      "lng": -85.045087,
      "lat": 40.046244
    },
    "Rockport": {
      "lng": -87.0372,
      "lat": 37.9256
    },
    "Rockville Solar I LLC": {
      "lng": -86.301111,
      "lat": 39.763056
    },
    "Rockville Solar II, LLC": {
      "lng": -86.3,
      "lat": 39.766944
    },
    "Rosewater Wind Farm": {
      "lng": -86.875007,
      "lat": 40.770899
    },
    "SABIC Innovative Plastics Mt. Vernon": {
      "lng": -87.926111,
      "lat": 37.910278
    },
    "Sagamore Plant Cogeneration": {
      "lng": -86.86,
      "lat": 40.4439
    },
    "Scotland Solar RES": {
      "lng": -86.9473,
      "lat": 38.9099
    },
    "Scottsburg Solar Park": {
      "lng": -85.785544,
      "lat": 38.661999
    },
    "Sheridan School Corporation Solar": {
      "lng": -86.215278,
      "lat": 40.140278
    },
    "Spring Mill Solar RES": {
      "lng": -86.469676,
      "lat": 38.709545
    },
    "St. Joseph Energy Center LLC": {
      "lng": -86.479699,
      "lat": 41.698992
    },
    "St. Joseph Solar": {
      "lng": -86.1083,
      "lat": 41.7311
    },
    "Staunton": {
      "lng": -87.20824,
      "lat": 39.45746
    },
    "Sugar Creek Generating Station": {
      "lng": -87.5108,
      "lat": 39.3931
    },
    "Sullivan Solar, LLC": {
      "lng": -87.41383,
      "lat": 39.13199
    },
    "Tate & Lyle Lafayette South": {
      "lng": -86.843695,
      "lat": 40.376319
    },
    "Tell City Solar Park": {
      "lng": -86.737329,
      "lat": 37.965852
    },
    "Tippecanoe Solar Power Plant": {
      "lng": -86.940157,
      "lat": 40.421142
    },
    "Tipton Solar Park": {
      "lng": -86.076899,
      "lat": 40.276073
    },
    "Troy Solar": {
      "lng": -86.791594,
      "lat": 38.015372
    },
    "Twin Branch": {
      "lng": -86.1322,
      "lat": 41.665
    },
    "Twin Branch PV": {
      "lng": -86.12,
      "lat": 41.69
    },
    "Twin Bridges LFGTE": {
      "lng": -86.494658,
      "lat": 39.744983
    },
    "Union City Wind Turbine": {
      "lng": -84.8172,
      "lat": 40.1908
    },
    "University of Notre Dame": {
      "lng": -86.2367,
      "lat": 41.7094
    },
    "US Steel Corp - Gary Works": {
      "lng": -87.328936,
      "lat": 41.622426
    },
    "Valparaiso Solar, LLC": {
      "lng": -87.161111,
      "lat": 41.502222
    },
    "Veolia ES Blackfoot Landfill Facility": {
      "lng": -87.200556,
      "lat": 38.327222
    },
    "Vermillion Generating Station": {
      "lng": -87.446358,
      "lat": 39.922328
    },
    "Volkman Road Solar Array Hybrid": {
      "lng": -87.548212,
      "lat": 38.1492
    },
    "Wabash River Highland Plant": {
      "lng": -87.4247,
      "lat": 39.53
    },
    "Waterloo Solar, LLC": {
      "lng": -85.043889,
      "lat": 41.433889
    },
    "Wheatland Generating Facility LLC": {
      "lng": -87.2931,
      "lat": 38.6716
    },
    "Whitewater Valley": {
      "lng": -84.8953,
      "lat": 39.8028
    },
    "Whiting Clean Energy, Inc.": {
      "lng": -87.4775,
      "lat": 41.6739
    },
    "Wildcat Wind Farm I, LLC": {
      "lng": -85.843056,
      "lat": 40.349444
    },
    "Worthington Generation": {
      "lng": -87.0128,
      "lat": 39.0708
    },
    "Zimmerman Energy": {
      "lng": -86.246111,
      "lat": 41.237778
    },
    "Alexander Wind Farm LLC": {
      "lng": -99.5075,
      "lat": 38.424722
    },
    "Anthony": {
      "lng": -98.0311,
      "lat": 37.1517
    },
    "Arkalon Ethanol LLC": {
      "lng": -100.803611,
      "lat": 37.110556
    },
    "Ashland": {
      "lng": -99.763073,
      "lat": 37.193787
    },
    "Augusta Electric Plant No 1": {
      "lng": -96.97193,
      "lat": 37.678454
    },
    "Augusta Electric Plant No 2": {
      "lng": -96.964982,
      "lat": 37.686085
    },
    "Baldwin City Plant No 1": {
      "lng": -95.1867,
      "lat": 38.7745
    },
    "Baldwin City Plant No 2": {
      "lng": -95.192006,
      "lat": 38.764905
    },
    "Belleville": {
      "lng": -97.632038,
      "lat": 39.832769
    },
    "Beloit": {
      "lng": -98.1122,
      "lat": 39.4578
    },
    "Bird City": {
      "lng": -101.5319,
      "lat": 39.7581
    },
    "Bloom Wind": {
      "lng": -99.9247,
      "lat": 37.4863
    },
    "Bonanza BioEnergy LLC": {
      "lng": -100.8361,
      "lat": 37.9586
    },
    "BPU Solar Farm": {
      "lng": -94.696892,
      "lat": 39.166436
    },
    "Buckeye Wind Energy Center": {
      "lng": -99.414161,
      "lat": 39.003873
    },
    "Buffalo Dunes Wind Project": {
      "lng": -101.076111,
      "lat": 37.620833
    },
    "Burlingame": {
      "lng": -95.8368,
      "lat": 38.7551
    },
    "Burlington (KS)": {
      "lng": -95.749953,
      "lat": 38.193441
    },
    "Caney River Wind Project": {
      "lng": -96.42,
      "lat": 37.46
    },
    "Cedar Bluff Wind, LLC": {
      "lng": -99.729289,
      "lat": 38.697606
    },
    "Central Plains Wind Farm": {
      "lng": -101.161472,
      "lat": 38.503167
    },
    "Chanute 2": {
      "lng": -95.4586,
      "lat": 37.6953
    },
    "Chanute 3": {
      "lng": -95.4704,
      "lat": 37.6376
    },
    "Cimarron Bend III": {
      "lng": -99.997,
      "lat": 37.414
    },
    "Cimarron Bend Wind Project I, LLC": {
      "lng": -99.991,
      "lat": 37.355
    },
    "Cimarron Bend Wind Project II, LLC": {
      "lng": -99.991,
      "lat": 37.355
    },
    "Cimarron River": {
      "lng": -100.7619,
      "lat": 37.1611
    },
    "Cimarron Wind Energy LLC": {
      "lng": -100.280277,
      "lat": 37.873889
    },
    "Cimarron Windpower II": {
      "lng": -100.372,
      "lat": 37.90133
    },
    "City of Pratt Solar": {
      "lng": -98.720928,
      "lat": 37.710556
    },
    "Clay Center": {
      "lng": -97.127211,
      "lat": 39.373966
    },
    "Clifton Station": {
      "lng": -97.2778,
      "lat": 39.6125
    },
    "Cloud County Wind Farm": {
      "lng": -97.6644,
      "lat": 39.4375
    },
    "CML&P Generating Facility No. 2": {
      "lng": -95.585154,
      "lat": 37.092977
    },
    "Coffeyville": {
      "lng": -95.612689,
      "lat": 37.037344
    },
    "Colby": {
      "lng": -101.038202,
      "lat": 39.403001
    },
    "Colby City of": {
      "lng": -101.058372,
      "lat": 39.394414
    },
    "Diamond Vista Wind Project, LLC": {
      "lng": -97.040628,
      "lat": 38.56564
    },
    "East 12th Street": {
      "lng": -96.9625,
      "lat": 37.2367
    },
    "East Fork Wind Project, LLC": {
      "lng": -101.0744,
      "lat": 39.306
    },
    "East Kansas Agri-Energy, LLC": {
      "lng": -95.240278,
      "lat": 38.2725
    },
    "El Dorado Refinery": {
      "lng": -96.877222,
      "lat": 37.792778
    },
    "Elk River Wind": {
      "lng": -96.5503,
      "lat": 37.5797
    },
    "Ellinwood": {
      "lng": -98.583436,
      "lat": 38.354514
    },
    "Emporia Energy Center": {
      "lng": -96.065247,
      "lat": 38.445883
    },
    "Ensign Wind LLC": {
      "lng": -100.278666,
      "lat": 37.614473
    },
    "Erie": {
      "lng": -95.237499,
      "lat": 37.56681
    },
    "Erie Energy Center": {
      "lng": -95.237222,
      "lat": 37.574167
    },
    "Flat Ridge 2 Wind Energy LLC": {
      "lng": -98.261077,
      "lat": 37.366509
    },
    "Flat Ridge 3": {
      "lng": -98.400225,
      "lat": 37.446969
    },
    "Flat Ridge Wind Energy LLC": {
      "lng": -98.4583,
      "lat": 37.3764
    },
    "Flat Ridge Wind Farm": {
      "lng": -98.460278,
      "lat": 37.369167
    },
    "Fort Dodge aka Judson Large": {
      "lng": -99.9497,
      "lat": 37.7328
    },
    "Fort Hays State University Wind Farm": {
      "lng": -99.405214,
      "lat": 38.874316
    },
    "Garden City": {
      "lng": -100.8956,
      "lat": 37.9703
    },
    "Gardner Energy Center": {
      "lng": -94.890278,
      "lat": 38.830833
    },
    "Garnett Municipal": {
      "lng": -95.2447,
      "lat": 38.2697
    },
    "Girard": {
      "lng": -94.839318,
      "lat": 37.507709
    },
    "Goodland": {
      "lng": -101.705188,
      "lat": 39.341622
    },
    "Goodman Energy Center": {
      "lng": -99.3617,
      "lat": 38.9303
    },
    "Gordon Evans Energy Center": {
      "lng": -97.521667,
      "lat": 37.7903
    },
    "Gray County Wind Energy": {
      "lng": -100.378197,
      "lat": 37.613256
    },
    "Great Bend Station aka Arthur Mullergren": {
      "lng": -98.8689,
      "lat": 38.41
    },
    "Greensburg": {
      "lng": -99.3444,
      "lat": 37.5499
    },
    "Herington": {
      "lng": -96.9492,
      "lat": 38.6658
    },
    "Hill City": {
      "lng": -99.8417,
      "lat": 39.3582
    },
    "Hoisington": {
      "lng": -98.7747,
      "lat": 38.513
    },
    "Holcomb": {
      "lng": -100.9725,
      "lat": 37.9308
    },
    "Holton": {
      "lng": -95.73155,
      "lat": 39.4735
    },
    "Hugoton 2": {
      "lng": -101.340133,
      "lat": 37.16424
    },
    "Hutchinson Energy Center": {
      "lng": -97.874722,
      "lat": 38.0906
    },
    "Iola": {
      "lng": -95.425572,
      "lat": 37.923076
    },
    "Irish Creek Wind": {
      "lng": -96.425143,
      "lat": 39.699456
    },
    "Iron Star Wind Project": {
      "lng": -99.98094,
      "lat": 37.64875
    },
    "Ironwood Wind": {
      "lng": -99.7754,
      "lat": 37.81828
    },
    "Jameson Energy Center": {
      "lng": -100.830556,
      "lat": 37.954722
    },
    "Jayhawk Wind Energy Center": {
      "lng": -95.029817,
      "lat": 37.704461
    },
    "Jeffrey Energy Center": {
      "lng": -96.117231,
      "lat": 39.286453
    },
    "Jetmore": {
      "lng": -99.893587,
      "lat": 38.080332
    },
    "Johnson": {
      "lng": -101.751065,
      "lat": 37.566881
    },
    "Johnson Corner Solar 1": {
      "lng": -101.799234,
      "lat": 37.547002
    },
    "Kansas Ethanol, LLC": {
      "lng": -98.192128,
      "lat": 38.286173
    },
    "Kansas River Project": {
      "lng": -95.235078,
      "lat": 38.974022
    },
    "Kaw": {
      "lng": -94.651389,
      "lat": 39.0867
    },
    "Kingman Municipal Power and Light Plant": {
      "lng": -98.070364,
      "lat": 37.38311
    },
    "Kingman Wind": {
      "lng": -98.579264,
      "lat": 37.586714
    },
    "La Crosse": {
      "lng": -99.308333,
      "lat": 38.531667
    },
    "La Cygne": {
      "lng": -94.6456,
      "lat": 38.3481
    },
    "Larned": {
      "lng": -99.099984,
      "lat": 38.176167
    },
    "Lawrence Energy Center": {
      "lng": -95.269167,
      "lat": 39.0072
    },
    "Lincoln": {
      "lng": -98.152993,
      "lat": 39.03719
    },
    "Marshall Wind Farm": {
      "lng": -96.360833,
      "lat": 39.701111
    },
    "McPherson 2": {
      "lng": -97.68335,
      "lat": 38.363411
    },
    "McPherson Municipal Power Plant #3": {
      "lng": -97.61108,
      "lat": 38.38932
    },
    "Meade": {
      "lng": -100.332438,
      "lat": 37.285176
    },
    "Midwest Energy Community Solar Array": {
      "lng": -101.039444,
      "lat": 39.406944
    },
    "Minneapolis City of": {
      "lng": -97.7086,
      "lat": 39.1194
    },
    "Mulvane 2": {
      "lng": -97.2216,
      "lat": 37.4908
    },
    "Nearman Creek": {
      "lng": -94.6975,
      "lat": 39.1681
    },
    "Neosho Ridge Wind Energy Center": {
      "lng": -95.37673,
      "lat": 37.485357
    },
    "Ninnescah Wind Energy, LLC": {
      "lng": -98.579264,
      "lat": 37.586714
    },
    "Oak Grove Power Producers": {
      "lng": -94.6414,
      "lat": 37.6297
    },
    "Oberlin (KS)": {
      "lng": -100.5289,
      "lat": 39.8217
    },
    "Osage City": {
      "lng": -95.827658,
      "lat": 38.633183
    },
    "Osawatomie City of": {
      "lng": -94.960069,
      "lat": 38.501719
    },
    "Osawatomie Generating Station": {
      "lng": -94.902633,
      "lat": 38.532161
    },
    "Osawatomie Power Plant North Sub": {
      "lng": -94.956918,
      "lat": 38.501309
    },
    "Osawatomie Power Plant South Sub": {
      "lng": -94.95255,
      "lat": 38.495623
    },
    "Osborne": {
      "lng": -98.690296,
      "lat": 39.441979
    },
    "Ottawa": {
      "lng": -95.279742,
      "lat": 38.615579
    },
    "Oxford (KS)": {
      "lng": -97.1636,
      "lat": 37.2758
    },
    "Post Rock Wind Power Project, LLC": {
      "lng": -98.299444,
      "lat": 38.841667
    },
    "Prairie Horizon Agri Energy": {
      "lng": -99.305833,
      "lat": 39.761111
    },
    "Prairie Queen Wind Farm": {
      "lng": -95.2244,
      "lat": 37.9788
    },
    "Prairie Sky Solar Farm": {
      "lng": -97.074497,
      "lat": 37.751716
    },
    "Pratt": {
      "lng": -98.7433,
      "lat": 37.6359
    },
    "Pratt 2": {
      "lng": -98.745,
      "lat": 37.6356
    },
    "Pratt Wind, LLC": {
      "lng": -98.8,
      "lat": 37.55
    },
    "Quindaro": {
      "lng": -94.6381,
      "lat": 39.1492
    },
    "Reading Wind Project": {
      "lng": -95.9655,
      "lat": 38.475
    },
    "Riverton": {
      "lng": -94.698704,
      "lat": 37.072616
    },
    "Rubart": {
      "lng": -101.099444,
      "lat": 37.558611
    },
    "Russell Downtown": {
      "lng": -98.856944,
      "lat": 38.893333
    },
    "Russell Energy Center": {
      "lng": -98.839444,
      "lat": 38.899722
    },
    "Sabetha Power Plant": {
      "lng": -95.803367,
      "lat": 39.904922
    },
    "Sharon Spring": {
      "lng": -101.751525,
      "lat": 38.890552
    },
    "Sharpe": {
      "lng": -95.6839,
      "lat": 38.275
    },
    "Shooting Star": {
      "lng": -99.446389,
      "lat": 37.518889
    },
    "Slate Creek Wind Project LLC": {
      "lng": -97.2376,
      "lat": 37.1146
    },
    "Smoky Hills Wind Project Phase I": {
      "lng": -98.160159,
      "lat": 38.879936
    },
    "Smoky Hills Wind Project Phase II": {
      "lng": -98.168876,
      "lat": 38.892653
    },
    "Soldier Creek Wind": {
      "lng": -96.0294,
      "lat": 39.6572
    },
    "Solomon Forks Wind Project, LLC": {
      "lng": -101.0744,
      "lat": 39.306
    },
    "Spearville": {
      "lng": -99.7528,
      "lat": 37.8641
    },
    "Spearville 3 LLC": {
      "lng": -99.838056,
      "lat": 37.851111
    },
    "St Francis": {
      "lng": -101.810082,
      "lat": 39.770868
    },
    "St John": {
      "lng": -98.7602,
      "lat": 37.9953
    },
    "Stafford": {
      "lng": -98.599286,
      "lat": 37.956117
    },
    "Sterling": {
      "lng": -98.2071,
      "lat": 38.2143
    },
    "Stockton": {
      "lng": -99.27793,
      "lat": 39.43619
    },
    "Thunderbird Generator (Tbird)": {
      "lng": -96.25358,
      "lat": 38.468931
    },
    "Wamego": {
      "lng": -96.3086,
      "lat": 39.1992
    },
    "Washington": {
      "lng": -96.9795,
      "lat": 39.7306
    },
    "Waste Management Rolling Meadows LFGTE": {
      "lng": -95.731126,
      "lat": 39.174577
    },
    "Waste Water Plant Generator": {
      "lng": -96.989722,
      "lat": 37.227778
    },
    "Waverly Wind Farm LLC": {
      "lng": -95.814166,
      "lat": 38.256944
    },
    "Wellington 1": {
      "lng": -97.4057,
      "lat": 37.261372
    },
    "Wellington 2": {
      "lng": -97.3497,
      "lat": 37.2678
    },
    "West 14th Street": {
      "lng": -97.011,
      "lat": 37.2354
    },
    "West Gardner Generating Station": {
      "lng": -94.9861,
      "lat": 38.7875
    },
    "Westar Cities Solar": {
      "lng": -97.9753,
      "lat": 38.0194
    },
    "Western Plains Wind": {
      "lng": -100.733,
      "lat": 39.122
    },
    "Western Plains Wind Farm": {
      "lng": -99.67981,
      "lat": 37.854801
    },
    "Wichita Plant": {
      "lng": -97.424133,
      "lat": 37.581339
    },
    "Wolf Creek Generating Station": {
      "lng": -95.68978,
      "lat": 38.23926
    },
    "Wolf Creek Generator": {
      "lng": -95.68403,
      "lat": 38.27433
    },
    "Barkley": {
      "lng": -88.2211,
      "lat": 37.0217
    },
    "Bavarian LFGTE": {
      "lng": -84.6556,
      "lat": 38.8608
    },
    "Big Sandy": {
      "lng": -82.6176,
      "lat": 38.1707
    },
    "Blue Ridge Generating": {
      "lng": -83.988889,
      "lat": 37.741111
    },
    "Bluegrass Generating Station": {
      "lng": -85.4133,
      "lat": 38.3902
    },
    "Cane Run": {
      "lng": -85.8892,
      "lat": 38.1831
    },
    "Cannelton Hydroelectric Plant": {
      "lng": -86.705833,
      "lat": 37.899722
    },
    "Cooperative Solar One": {
      "lng": -84.254726,
      "lat": 38.027241
    },
    "Cox Waste to Energy": {
      "lng": -85.3611,
      "lat": 37.3208
    },
    "Crittenden Solar Facility": {
      "lng": -84.613255,
      "lat": 38.751822
    },
    "D B Wilson": {
      "lng": -87.0806,
      "lat": 37.4497
    },
    "Dix Dam": {
      "lng": -84.7028,
      "lat": 37.7864
    },
    "DTE Calvert City, LLC": {
      "lng": -88.353889,
      "lat": 37.048333
    },
    "E W Brown": {
      "lng": -84.71257,
      "lat": 37.78831
    },
    "East Bend": {
      "lng": -84.8514,
      "lat": 38.9036
    },
    "Ghent": {
      "lng": -85.035,
      "lat": 38.7497
    },
    "Glasgow LFGTE": {
      "lng": -85.9575,
      "lat": 36.9874
    },
    "Green City Recovery, LLC": {
      "lng": -84.548889,
      "lat": 38.345278
    },
    "Green Valley LFGTE": {
      "lng": -82.808819,
      "lat": 38.396737
    },
    "H L Spurlock": {
      "lng": -83.8181,
      "lat": 38.7
    },
    "Haefling": {
      "lng": -84.5194,
      "lat": 38.0794
    },
    "Hardin County LFGTE": {
      "lng": -85.7258,
      "lat": 37.7258
    },
    "John S. Cooper": {
      "lng": -84.5919,
      "lat": 36.9981
    },
    "Kentucky Dam": {
      "lng": -88.2692,
      "lat": 37.0131
    },
    "Kentucky Mills": {
      "lng": -86.6854,
      "lat": 37.8948
    },
    "Laurel Dam": {
      "lng": -84.27,
      "lat": 36.9614
    },
    "Laurel Ridge LFGTE": {
      "lng": -84.0933,
      "lat": 37.035
    },
    "LGE-KU Solar Share Facility Simpsonville": {
      "lng": -85.391432,
      "lat": 38.216484
    },
    "L'Oreal Solar - Florence": {
      "lng": -84.608172,
      "lat": 38.987696
    },
    "Marshall County": {
      "lng": -88.3958,
      "lat": 37.0286
    },
    "Matilda Hamilton Fee Hydroelectric Station": {
      "lng": -83.948553,
      "lat": 37.677484
    },
    "Meldahl Hydroelectric Project": {
      "lng": -84.172982,
      "lat": 38.791431
    },
    "Mill Creek": {
      "lng": -85.9103,
      "lat": 38.0525
    },
    "Morehead Generating Facility": {
      "lng": -83.544,
      "lat": 38.191
    },
    "Mother Ann Lee": {
      "lng": -84.7247,
      "lat": 37.8297
    },
    "Ohio Falls": {
      "lng": -85.7792,
      "lat": 38.2832
    },
    "Paddy's Run": {
      "lng": -85.841704,
      "lat": 38.223592
    },
    "Paducah Power Systems Plant 1": {
      "lng": -88.615862,
      "lat": 37.03385
    },
    "Paradise": {
      "lng": -86.9783,
      "lat": 37.2608
    },
    "Paris (KY)": {
      "lng": -84.2383,
      "lat": 38.2049
    },
    "Pendleton County LFGTE": {
      "lng": -84.405901,
      "lat": 38.734218
    },
    "R D Green": {
      "lng": -87.5,
      "lat": 37.6461
    },
    "Riverside Generating Company": {
      "lng": -82.604586,
      "lat": 38.191661
    },
    "Robert Reid": {
      "lng": -87.5019,
      "lat": 37.6461
    },
    "Shawnee": {
      "lng": -88.775,
      "lat": 37.1517
    },
    "Smith Generating Facility": {
      "lng": -84.1017,
      "lat": 37.8833
    },
    "Smithland Hydroelectric Plant": {
      "lng": -88.419518,
      "lat": 37.169636
    },
    "Trimble County": {
      "lng": -85.4117,
      "lat": 38.5847
    },
    "Walton 1 Solar Facility": {
      "lng": -84.592263,
      "lat": 38.85018
    },
    "Walton 2 Solar Facility": {
      "lng": -84.592263,
      "lat": 38.85018
    },
    "Wolf Creek": {
      "lng": -85.147,
      "lat": 36.8691
    },
    "Zorn": {
      "lng": -85.702331,
      "lat": 38.280278
    },
    "Acadia Power Station": {
      "lng": -92.413056,
      "lat": 30.428611
    },
    "ADA Carbon Solutions Red River": {
      "lng": -93.391111,
      "lat": 32.003889
    },
    "Agrilectric Power Partners Ltd": {
      "lng": -93.1269,
      "lat": 30.2012
    },
    "Alliance Refinery": {
      "lng": -89.975,
      "lat": 29.6833
    },
    "Arsenal Hill Power Plant": {
      "lng": -93.760125,
      "lat": 32.51947
    },
    "Axiall Plaquemine": {
      "lng": -91.185,
      "lat": 30.2586
    },
    "Bayou Cove Peaking Power Plant": {
      "lng": -92.5997,
      "lat": 30.2825
    },
    "Big Cajun 1": {
      "lng": -91.3556,
      "lat": 30.6739
    },
    "Big Cajun 2": {
      "lng": -91.3692,
      "lat": 30.7261
    },
    "Brame Energy Center": {
      "lng": -92.716667,
      "lat": 31.395
    },
    "Calcasieu Plant": {
      "lng": -93.345,
      "lat": 30.1608
    },
    "Carville Energy Center": {
      "lng": -91.065,
      "lat": 30.2294
    },
    "Chalmette Refining LLC": {
      "lng": -89.97,
      "lat": 29.9383
    },
    "CII Carbon LLC": {
      "lng": -89.9782,
      "lat": 29.9327
    },
    "CITGO Refinery Powerhouse": {
      "lng": -93.3234,
      "lat": 30.1826
    },
    "Coughlin Power Station": {
      "lng": -92.261111,
      "lat": 30.8439
    },
    "D G Hunter": {
      "lng": -92.4613,
      "lat": 31.3207
    },
    "DeRidder Mill": {
      "lng": -93.3756,
      "lat": 30.8606
    },
    "Dolet Hills Power Station": {
      "lng": -93.569167,
      "lat": 32.030556
    },
    "Domino Sugar Arabi Plant": {
      "lng": -90.0018,
      "lat": 29.9468
    },
    "Dow St Charles Operations": {
      "lng": -90.4414,
      "lat": 29.9861
    },
    "Enterprise Sugar Factory": {
      "lng": -91.725556,
      "lat": 29.903611
    },
    "ExxonMobil Baton Rouge Turbine Generator": {
      "lng": -91.187222,
      "lat": 30.489722
    },
    "Formosa Plastics": {
      "lng": -91.1872,
      "lat": 30.4992
    },
    "Gaylord Container Bogalusa": {
      "lng": -89.8575,
      "lat": 30.7811
    },
    "Geismar": {
      "lng": -91,
      "lat": 30.2
    },
    "Geismar Cogen": {
      "lng": -90.9921,
      "lat": 30.209
    },
    "Georgia-Pacific Port Hudson": {
      "lng": -91.28,
      "lat": 30.6547
    },
    "Gramercy Holdings LLC": {
      "lng": -90.6693,
      "lat": 30.054
    },
    "Grand Isle Gas Plant / Treating Station": {
      "lng": -89.9721,
      "lat": 29.252062
    },
    "Hargis-Hebert Electric Generating Statio": {
      "lng": -91.9923,
      "lat": 30.1694
    },
    "Houma": {
      "lng": -90.7225,
      "lat": 29.5806
    },
    "International Paper Red River Mill": {
      "lng": -93.1725,
      "lat": 31.905833
    },
    "LA3 West Baton Rouge Solar Facility": {
      "lng": -91.331855,
      "lat": 30.465105
    },
    "Lake Charles Plant": {
      "lng": -93.335556,
      "lat": 30.148889
    },
    "Lake Charles Polymers": {
      "lng": -93.32197,
      "lat": 30.196571
    },
    "Lake Charles Power Station": {
      "lng": -93.299056,
      "lat": 30.272631
    },
    "LaO Energy Systems": {
      "lng": -91.2325,
      "lat": 30.3186
    },
    "LEPA Unit No. 1": {
      "lng": -91.192778,
      "lat": 29.691111
    },
    "Lieberman Power Plant": {
      "lng": -93.9597,
      "lat": 32.7047
    },
    "Little Gypsy": {
      "lng": -90.461694,
      "lat": 30.005067
    },
    "Louisiana 1": {
      "lng": -91.1864,
      "lat": 30.4922
    },
    "Louisiana 2": {
      "lng": -91.1864,
      "lat": 30.4922
    },
    "Louisiana Sugar Refining": {
      "lng": -90.6839,
      "lat": 30.0497
    },
    "Louisiana Tech University Power Plant": {
      "lng": -92.6497,
      "lat": 32.5256
    },
    "LSU Cogen": {
      "lng": -91.181389,
      "lat": 30.410556
    },
    "Mansfield Mill": {
      "lng": -93.5562,
      "lat": 32.1575
    },
    "Mosaic Phosphates Uncle Sam": {
      "lng": -90.828312,
      "lat": 30.038649
    },
    "Nelson Industrial Steam Company": {
      "lng": -93.2911,
      "lat": 30.2844
    },
    "Neptune Gas Processing Plant": {
      "lng": -91.4431,
      "lat": 29.7336
    },
    "New Orleans Power": {
      "lng": -89.9372,
      "lat": 30.0081
    },
    "New Orleans Solar Power Plant": {
      "lng": -90.023973,
      "lat": 30.011409
    },
    "New Orleans Solar Station": {
      "lng": -89.92,
      "lat": 30.015
    },
    "New Roads": {
      "lng": -91.368333,
      "lat": 30.726111
    },
    "Ninemile Point": {
      "lng": -90.1458,
      "lat": 29.9472
    },
    "Oak Point Cogen": {
      "lng": -90.00843,
      "lat": 29.811956
    },
    "Ouachita Plant": {
      "lng": -92.0683,
      "lat": 32.7061
    },
    "PCS Nitrogen Fertilizer LP": {
      "lng": -91.055114,
      "lat": 30.226977
    },
    "Perryville Power Station": {
      "lng": -92.0183,
      "lat": 32.6908
    },
    "Plant 31 Paper Mill": {
      "lng": -92.149597,
      "lat": 32.486787
    },
    "Plaquemine Cogen Facility": {
      "lng": -91.2347,
      "lat": 30.3217
    },
    "Port Allen (LA)": {
      "lng": -91.211,
      "lat": 30.477
    },
    "PPG Plant C Caustic": {
      "lng": -93.293765,
      "lat": 30.226444
    },
    "PPG Powerhouse A": {
      "lng": -93.28128,
      "lat": 30.225054
    },
    "PPG Powerhouse C": {
      "lng": -93.295173,
      "lat": 30.22612
    },
    "PPG Riverside": {
      "lng": -93.280693,
      "lat": 30.221746
    },
    "R S Cogen LLC": {
      "lng": -93.2992,
      "lat": 30.221
    },
    "Rayne": {
      "lng": -92.272174,
      "lat": 30.234627
    },
    "River Bend": {
      "lng": -91.3327,
      "lat": 30.757
    },
    "Shell Chemical": {
      "lng": -90.983963,
      "lat": 30.187508
    },
    "Sidney A Murray Jr Hydroelectric": {
      "lng": -91.618853,
      "lat": 31.088011
    },
    "St Francisville Mill": {
      "lng": -91.3236,
      "lat": 30.7108
    },
    "St. Charles Power Station": {
      "lng": -90.460833,
      "lat": 30.01
    },
    "St. Mary Clean Energy Center": {
      "lng": -91.470619,
      "lat": 29.68455
    },
    "Sterlington": {
      "lng": -92.0792,
      "lat": 32.7047
    },
    "T J Labbe Electric Generating Station": {
      "lng": -92.0433,
      "lat": 30.2554
    },
    "Taft Cogeneration Facility": {
      "lng": -90.4599,
      "lat": 29.9888
    },
    "TCI Alvar": {
      "lng": -90.028611,
      "lat": 29.992222
    },
    "TCI France": {
      "lng": -90.028888,
      "lat": 29.984166
    },
    "Teche Power Station": {
      "lng": -91.5425,
      "lat": 29.822222
    },
    "Washington Parish Energy Center": {
      "lng": -89.909166,
      "lat": 30.791389
    },
    "Waterford 1 & 2": {
      "lng": -90.47684,
      "lat": 29.998884
    },
    "Waterford 3": {
      "lng": -90.471581,
      "lat": 29.995267
    },
    "Westrock Hodge (LA)": {
      "lng": -92.7277,
      "lat": 32.2752
    },
    "0 Hammond St CSG": {
      "lng": -70.726675,
      "lat": 41.808547
    },
    "115 G Fisher": {
      "lng": -70.83984,
      "lat": 41.877476
    },
    "126 Grove Solar LLC": {
      "lng": -71.422707,
      "lat": 42.076119
    },
    "154 D Fisher": {
      "lng": -70.83984,
      "lat": 41.87476
    },
    "160 Tihonet Rd Hybrid(CSG)": {
      "lng": -70.712667,
      "lat": 41.793414
    },
    "196 Tremont St(CSG)": {
      "lng": -70.730697,
      "lat": 41.842933
    },
    "201 Sturbridge B": {
      "lng": -72.171188,
      "lat": 42.109143
    },
    "205 Sturbridge A": {
      "lng": -72.170507,
      "lat": 42.109272
    },
    "265 Pleasant Solar NG, LLC": {
      "lng": -71.762222,
      "lat": 42.581389
    },
    "276 Federal Rd(CSG)": {
      "lng": -70.725244,
      "lat": 41.805144
    },
    "28 Livermore Hill Road Solar": {
      "lng": -71.951107,
      "lat": 42.55151
    },
    "299 Farm to Market Rd Hybrid(CSG)": {
      "lng": -70.727036,
      "lat": 41.795092
    },
    "301 Chestnut Solar NG": {
      "lng": -72.52178,
      "lat": 42.055179
    },
    "433 Purchase Solar NG, LLC": {
      "lng": -71.192244,
      "lat": 41.809353
    },
    "580 River Solar": {
      "lng": -72.076103,
      "lat": 42.680766
    },
    "59 Federal Rd(CSG)": {
      "lng": -70.721206,
      "lat": 41.828475
    },
    "631-56 Airport Owner, LLC": {
      "lng": -71.117222,
      "lat": 41.745556
    },
    "651 Chase Solar NG": {
      "lng": -71.742778,
      "lat": 42.626667
    },
    "71 Charlotte Furnace Rd Hybrid(CSG)": {
      "lng": -70.741597,
      "lat": 41.797106
    },
    "77 Farm to Market Rd Hybrid(CSG)": {
      "lng": -70.723394,
      "lat": 41.79455
    },
    "Acton Solar Landfill": {
      "lng": -71.430556,
      "lat": 42.473333
    },
    "Acushnet AD Makepeace": {
      "lng": -70.743611,
      "lat": 41.798889
    },
    "Acushnet Ball Plant 2": {
      "lng": -70.97138,
      "lat": 41.7197
    },
    "Acushnet Hawes Reed Road": {
      "lng": -71.0425,
      "lat": 41.662222
    },
    "Acushnet- High Hill": {
      "lng": -70.997222,
      "lat": 41.714444
    },
    "Acushnet-Braley Road 1": {
      "lng": -70.970556,
      "lat": 41.751667
    },
    "Adams Farm Solar": {
      "lng": -72.2,
      "lat": 42.6
    },
    "Adirondack A+B Community Solar Garden": {
      "lng": -71.111058,
      "lat": 41.679012
    },
    "Agawam Solar": {
      "lng": -72.614444,
      "lat": 42.081944
    },
    "AIS Solar Project": {
      "lng": -71.737786,
      "lat": 42.501949
    },
    "Ajax Solar, LLC (MA)": {
      "lng": -71.058569,
      "lat": 42.018945
    },
    "Alicea Solar Project Hybrid CSG": {
      "lng": -72.0062,
      "lat": 42.0686
    },
    "Amaterasu LLC": {
      "lng": -71.6734,
      "lat": 42.519282
    },
    "Ameresco Chicopee Energy": {
      "lng": -72.536701,
      "lat": 42.162647
    },
    "Amherst College Co Gen": {
      "lng": -72.512222,
      "lat": 42.371667
    },
    "Anderson Power Products Division": {
      "lng": -71.743333,
      "lat": 42.471389
    },
    "Annese Solar Project Hybrid CSG": {
      "lng": -72.0052,
      "lat": 42.0755
    },
    "Antanavica Solar": {
      "lng": -71.948876,
      "lat": 42.250771
    },
    "Ashburnham Energy Storage Project": {
      "lng": -71.926696,
      "lat": 42.671444
    },
    "Ashby Duffy CSG Solar Farm": {
      "lng": -71.83,
      "lat": 42.68
    },
    "Ashby Solar, LLC CSG": {
      "lng": -71.798723,
      "lat": 42.630163
    },
    "Axio Green LLC": {
      "lng": -72.61,
      "lat": 42.576111
    },
    "Ayers Village Solar": {
      "lng": -71.1775,
      "lat": 42.791944
    },
    "Barnstable Landfill": {
      "lng": -70.395556,
      "lat": 41.659722
    },
    "Barre II Solar Project": {
      "lng": -72.109,
      "lat": 42.362
    },
    "Barre Solar III LLC": {
      "lng": -72.113901,
      "lat": 42.419331
    },
    "Barrett PV": {
      "lng": -71.864455,
      "lat": 42.162207
    },
    "Barton Acres Solar, LLC CSG": {
      "lng": -72.164,
      "lat": 42.572
    },
    "Bashaw Solar CSG 1, LLC": {
      "lng": -70.880032,
      "lat": 42.789327
    },
    "Bear Swamp": {
      "lng": -72.9599,
      "lat": 42.6839
    },
    "Beebe Substation Battery Storage": {
      "lng": -71.047813,
      "lat": 42.48791
    },
    "Belchertown": {
      "lng": -72.400429,
      "lat": 42.259548
    },
    "Belchertown Renewables Community Solar": {
      "lng": -72.3,
      "lat": 42.3
    },
    "Bellingham": {
      "lng": -71.4833,
      "lat": 42.0925
    },
    "Bellingham Power Generation LLC": {
      "lng": -71.453468,
      "lat": 42.110577
    },
    "Bellingham PV": {
      "lng": -71.703056,
      "lat": 42.075
    },
    "Berkley": {
      "lng": -71.018328,
      "lat": 41.841454
    },
    "Berkley East Solar LLC": {
      "lng": -71.041389,
      "lat": 41.823889
    },
    "Berkshire 1": {
      "lng": -73.405278,
      "lat": 42.114444
    },
    "Berkshire Power": {
      "lng": -72.6478,
      "lat": 42.0476
    },
    "Berkshire Wind Power Project": {
      "lng": -73.275,
      "lat": 42.586667
    },
    "Beverly": {
      "lng": -70.911944,
      "lat": 42.59
    },
    "Big George PV CSG": {
      "lng": -70.716128,
      "lat": 41.902312
    },
    "Bird Machine Solar Farm": {
      "lng": -71.266743,
      "lat": 42.109564
    },
    "BJ's Wholesale Club, Inc- Uxbridge": {
      "lng": -71.604149,
      "lat": 42.019659
    },
    "Blackstone Power Generation LLC": {
      "lng": -71.515811,
      "lat": 42.059876
    },
    "Blair Wire Village Solar, LLC CSG": {
      "lng": -71.975,
      "lat": 42.259
    },
    "Blodgett Solar CSG": {
      "lng": -71.445111,
      "lat": 42.678889
    },
    "BlueWave Capital - Grafton (SREC II)": {
      "lng": -71.662651,
      "lat": 42.225492
    },
    "Boatlock": {
      "lng": -72.6014,
      "lat": 42.2086
    },
    "Bolton PV": {
      "lng": -71.643056,
      "lat": 42.449722
    },
    "Boott Hydropower": {
      "lng": -71.322383,
      "lat": 42.652454
    },
    "Boston Medical Center CHP Plant": {
      "lng": -71.074171,
      "lat": 42.334651
    },
    "Boston Scientific Solar": {
      "lng": -71.033889,
      "lat": 42.293333
    },
    "Bourne (MA) - Holliston I": {
      "lng": -71.454633,
      "lat": 42.177876
    },
    "Boutillier Solar, LLC CSG": {
      "lng": -71.891,
      "lat": 42.219
    },
    "Bowden Solar LLC": {
      "lng": -72.338133,
      "lat": 42.109156
    },
    "Bradford Solar": {
      "lng": -71.094447,
      "lat": 42.751153
    },
    "Braintree Landfill Solar": {
      "lng": -70.997778,
      "lat": 42.207264
    },
    "Braley Road 2": {
      "lng": -70.966944,
      "lat": 41.751667
    },
    "Breckenridge Solar": {
      "lng": -72.315802,
      "lat": 42.17406
    },
    "Brewster Landfill": {
      "lng": -70.103056,
      "lat": 41.745556
    },
    "Bridgewater Complex Co-Generation Plant": {
      "lng": -70.958393,
      "lat": 41.931838
    },
    "Bridgewater Solar CSG": {
      "lng": -70.971931,
      "lat": 41.964116
    },
    "Brockelman": {
      "lng": -71.716944,
      "lat": 42.481389
    },
    "Brockelman Road Solar 2": {
      "lng": -71.71081,
      "lat": 42.48392
    },
    "Brook Street Solar 1 CSG": {
      "lng": -70.786125,
      "lat": 41.955897
    },
    "Brookfield Solar 2013 LLC": {
      "lng": -72.067877,
      "lat": 42.25609
    },
    "Brookfield Wire Company": {
      "lng": -72.12208,
      "lat": 42.21694
    },
    "Browne Solar LLC": {
      "lng": -72.322222,
      "lat": 42.200833
    },
    "Bullock Road Solar 1": {
      "lng": -70.990453,
      "lat": 41.728936
    },
    "BWC Gibbs Brook": {
      "lng": -70.695934,
      "lat": 41.774864
    },
    "BWC Harlow Brook": {
      "lng": -70.690717,
      "lat": 41.782854
    },
    "BWC Lake Lashaway, LLC Hybrid": {
      "lng": -72.0555,
      "lat": 42.24
    },
    "BWC Muddy Brook, LLC Hybrid": {
      "lng": -71.5054,
      "lat": 42.0966
    },
    "BWC Origination 18": {
      "lng": -70.836391,
      "lat": 41.679081
    },
    "BWC Pocasset River": {
      "lng": -70.687728,
      "lat": 41.782181
    },
    "BWC Stony Brook, LLC Hybrid": {
      "lng": -71.4036,
      "lat": 42.6375
    },
    "BWC Swan Pond River CSG": {
      "lng": -71.226628,
      "lat": 41.795599
    },
    "BWC Wading River One, Two, Three CSG": {
      "lng": -71.526667,
      "lat": 42.085278
    },
    "BWC Wareham River": {
      "lng": -70.693242,
      "lat": 41.783733
    },
    "Cabot": {
      "lng": -72.5793,
      "lat": 42.5875
    },
    "Cabot Holyoke": {
      "lng": -72.61,
      "lat": 42.2
    },
    "Camelot Wind LLC": {
      "lng": -70.64,
      "lat": 41.925278
    },
    "Canal Station": {
      "lng": -70.5097,
      "lat": 41.7694
    },
    "Canton": {
      "lng": -71.130918,
      "lat": 42.145126
    },
    "Cape Cod Air Force Station - 6 SWS": {
      "lng": -70.538889,
      "lat": 41.7525
    },
    "Carver": {
      "lng": -70.801648,
      "lat": 41.915338
    },
    "Carver CSG": {
      "lng": -70.810468,
      "lat": 41.905958
    },
    "Carver MA 1 Community Solar": {
      "lng": -70.76114,
      "lat": 41.942796
    },
    "CED Chicopee Solar": {
      "lng": -72.551031,
      "lat": 42.203921
    },
    "CED Northampton Solar LLC Hybrid": {
      "lng": -72.71064,
      "lat": 42.3057
    },
    "CED Westfield Solar, LLC": {
      "lng": -72.755181,
      "lat": 42.175625
    },
    "Cedarville CSG": {
      "lng": -70.549809,
      "lat": 41.802697
    },
    "Centaurus Solar - MA": {
      "lng": -71.426,
      "lat": 42.253
    },
    "Centech Gas Generator": {
      "lng": -71.690509,
      "lat": 42.255386
    },
    "CES Agawam Tuckahoe Solar Hybrid": {
      "lng": -72.691,
      "lat": 42.059
    },
    "CES Sterling LLC": {
      "lng": -71.722222,
      "lat": 42.441944
    },
    "Charlemont A": {
      "lng": -72.896,
      "lat": 42.642
    },
    "Charlestown Wind Turbine": {
      "lng": -71.070833,
      "lat": 42.391389
    },
    "Charlton Solar I CSG": {
      "lng": -71.996389,
      "lat": 42.1275
    },
    "Chatham Landfill": {
      "lng": -70.006111,
      "lat": 41.686111
    },
    "Chemical": {
      "lng": -72.6097,
      "lat": 42.1919
    },
    "Cherry Street": {
      "lng": -71.56,
      "lat": 42.388611
    },
    "Cherry Valley Solar, LLC CSG": {
      "lng": -71.891545,
      "lat": 42.229961
    },
    "Chester Solar Farm": {
      "lng": -72.949,
      "lat": 42.312
    },
    "Chicopee": {
      "lng": -72.510712,
      "lat": 42.181057
    },
    "Chicopee Granby Road Solar": {
      "lng": -72.595833,
      "lat": 42.158056
    },
    "Chicopee Hydroelectric Station": {
      "lng": -72.58279,
      "lat": 42.159595
    },
    "Chicopee River Solar": {
      "lng": -72.552222,
      "lat": 42.150278
    },
    "Chicopee Solar": {
      "lng": -72.5825,
      "lat": 42.185556
    },
    "Chocksett Rd Energy Storage Project": {
      "lng": -71.737747,
      "lat": 42.451836
    },
    "Citizens Agawam Landfill Solar": {
      "lng": -72.597211,
      "lat": 42.092988
    },
    "Clark Road Solar 1, LLC": {
      "lng": -72.020244,
      "lat": 42.59613
    },
    "Clark University": {
      "lng": -71.8226,
      "lat": 42.2508
    },
    "Cleary Flood": {
      "lng": -71.1061,
      "lat": 41.8653
    },
    "Cobble Mountain": {
      "lng": -72.864663,
      "lat": 42.117112
    },
    "Cohasse Solar, LLC CSG": {
      "lng": -72.045,
      "lat": 42.056
    },
    "Collins Facility": {
      "lng": -72.4227,
      "lat": 42.1569
    },
    "Concord Solar Farm": {
      "lng": -71.335,
      "lat": 42.444
    },
    "Cosgrove Intake and Power Station": {
      "lng": -71.687218,
      "lat": 42.40439
    },
    "Cottage Street Solar Facility": {
      "lng": -72.538333,
      "lat": 42.145278
    },
    "Covanta Haverhill": {
      "lng": -71.1242,
      "lat": 42.765
    },
    "Cranberry Highway Solar LLC": {
      "lng": -70.745223,
      "lat": 41.787462
    },
    "Cranberry Solar": {
      "lng": -70.907792,
      "lat": 41.871955
    },
    "Cronin Road Solar 1, LLC CSG Hybrid": {
      "lng": -72.154,
      "lat": 42.1763
    },
    "Curtis Hill Solar CSG": {
      "lng": -71.986,
      "lat": 42.175
    },
    "Cycz Solar Project CSG Hybrid": {
      "lng": -71.9761,
      "lat": 42.218
    },
    "Dartmouth": {
      "lng": -70.996944,
      "lat": 41.676111
    },
    "Dartmouth Farms Solar": {
      "lng": -71.00347,
      "lat": 41.6628
    },
    "Dartmouth II Solar": {
      "lng": -71.030833,
      "lat": 41.680556
    },
    "Dartmouth Landfill": {
      "lng": -70.998056,
      "lat": 41.581111
    },
    "Dartmouth Power": {
      "lng": -70.9994,
      "lat": 41.6731
    },
    "Dartmouth Solar": {
      "lng": -70.974448,
      "lat": 41.715804
    },
    "DDR Shoppers World": {
      "lng": -71.396228,
      "lat": 42.300745
    },
    "Deer Island Treatment": {
      "lng": -70.962778,
      "lat": 42.353333
    },
    "Deerfield 2": {
      "lng": -72.707274,
      "lat": 42.573084
    },
    "Deerfield 3": {
      "lng": -72.7335,
      "lat": 42.5995
    },
    "Deerfield 4": {
      "lng": -72.7376,
      "lat": 42.6198
    },
    "Deerfield 5": {
      "lng": -72.9561,
      "lat": 42.69118
    },
    "Deerfield CSG Solar": {
      "lng": -72.589186,
      "lat": 42.56288
    },
    "Dennis Landfill": {
      "lng": -70.145,
      "lat": 41.693056
    },
    "Depot Hill Solar CSG": {
      "lng": -71.988,
      "lat": 42.153
    },
    "Dept of Corrections NCCI Wind": {
      "lng": -71.944722,
      "lat": 42.572222
    },
    "Devens": {
      "lng": -71.625875,
      "lat": 42.559173
    },
    "DG Crystal Spring CSG": {
      "lng": -70.495276,
      "lat": 41.679836
    },
    "DG Dighton LLC CSG": {
      "lng": -71.144835,
      "lat": 41.801491
    },
    "DG Foxborough Elm CSG": {
      "lng": -71.141312,
      "lat": 42.056053
    },
    "DG Haverhill CSG": {
      "lng": -71.116786,
      "lat": 42.817023
    },
    "DG Tufts Knoll LLC CSG": {
      "lng": -71.672328,
      "lat": 42.24325
    },
    "DG Tufts Science LLC CSG": {
      "lng": -71.686069,
      "lat": 42.237583
    },
    "DG Webster LLC CSG": {
      "lng": -71.513556,
      "lat": 42.41389
    },
    "Dighton": {
      "lng": -71.1239,
      "lat": 41.8312
    },
    "Dorchester Solar Site": {
      "lng": -71.047171,
      "lat": 42.300802
    },
    "Doreen": {
      "lng": -73.2058,
      "lat": 42.4433
    },
    "Douglas Solar": {
      "lng": -71.73,
      "lat": 42.06
    },
    "Dover": {
      "lng": -81.46815,
      "lat": 40.520066
    },
    "Dresser Hill CSG": {
      "lng": -72.006519,
      "lat": 42.081793
    },
    "Dudley": {
      "lng": -71.978849,
      "lat": 42.054345
    },
    "Dudley River 2 Solar, LLC CSG": {
      "lng": -71.997064,
      "lat": 42.07239
    },
    "Dudley River 3 Solar, LLC CSG": {
      "lng": -71.998219,
      "lat": 42.071255
    },
    "Dudley River Solar, LLC CSG": {
      "lng": -71.999663,
      "lat": 42.070934
    },
    "Dudley Solar CSG": {
      "lng": -71.9229,
      "lat": 42.0539
    },
    "Dunstable Solar 1, LLC": {
      "lng": -71.5168,
      "lat": 42.6656
    },
    "Dwight": {
      "lng": -72.610949,
      "lat": 42.149497
    },
    "Dynamic - Walpole": {
      "lng": -71.271945,
      "lat": 42.133208
    },
    "East Acres Solar NG, LLC": {
      "lng": -73.242331,
      "lat": 42.487476
    },
    "East Bridgewater Solar": {
      "lng": -70.981746,
      "lat": 42.059895
    },
    "East Brookfield Adams Road Solar LLC CSG": {
      "lng": -72.03084,
      "lat": 42.196032
    },
    "East Brookfield Main Street Solar LLC CSG": {
      "lng": -72.0294,
      "lat": 42.2323
    },
    "East Longmeadow Solar PV": {
      "lng": -72.459123,
      "lat": 42.046767
    },
    "East Springfield Solar PV": {
      "lng": -72.55,
      "lat": 42.14
    },
    "Easthampton CSG": {
      "lng": -72.679837,
      "lat": 42.24781
    },
    "Easthampton Landfill-City of Easthampton": {
      "lng": -72.701389,
      "lat": 42.286944
    },
    "Easton Landfill": {
      "lng": -71.092222,
      "lat": 42.010278
    },
    "EBZ Solar": {
      "lng": -71.625,
      "lat": 42.570556
    },
    "EDF Lancaster": {
      "lng": -71.665278,
      "lat": 42.529167
    },
    "Elm Street": {
      "lng": -71.141817,
      "lat": 41.791718
    },
    "ENGIE 2019 ProjectCo-MA1, LLC": {
      "lng": -72.653224,
      "lat": 42.041614
    },
    "Equinix - Billerica": {
      "lng": -71.21496,
      "lat": 42.551001
    },
    "Equity Industrial Turbines": {
      "lng": -70.656489,
      "lat": 42.625649
    },
    "Erving CSG": {
      "lng": -72.47116,
      "lat": 42.603717
    },
    "Erving Paper Mills": {
      "lng": -72.3875,
      "lat": 42.5983
    },
    "Exelon West Medway II": {
      "lng": -71.4447,
      "lat": 42.137218
    },
    "Fairhaven C CSG": {
      "lng": -70.86323,
      "lat": 41.66303
    },
    "Fairhaven E": {
      "lng": -72.896,
      "lat": 42.642
    },
    "Fairhaven MA 2": {
      "lng": -70.87831,
      "lat": 41.666
    },
    "Fairhaven Wind": {
      "lng": -70.873372,
      "lat": 41.639061
    },
    "Fairview Farms Solar": {
      "lng": -72.610278,
      "lat": 42.459722
    },
    "Fall River Solar CSG": {
      "lng": -71.114536,
      "lat": 41.751347
    },
    "Falmouth Landfill Solar": {
      "lng": -70.598611,
      "lat": 41.610106
    },
    "Farley Road Community Solar": {
      "lng": -72,
      "lat": 42.1
    },
    "Federal Road Solar 1, LLC CSG": {
      "lng": -70.725717,
      "lat": 41.806894
    },
    "Fife Brook": {
      "lng": -72.977634,
      "lat": 42.685681
    },
    "Fisher Road Solar": {
      "lng": -71.018053,
      "lat": 41.593314
    },
    "Fitchburg Gas Recovery": {
      "lng": -71.848058,
      "lat": 42.535393
    },
    "Fitchburg Renewables": {
      "lng": -71.795705,
      "lat": 42.604396
    },
    "Fore River Energy Center": {
      "lng": -70.9661,
      "lat": 42.2422
    },
    "Framingham State University Plant": {
      "lng": -71.436065,
      "lat": 42.297949
    },
    "Framingham Station": {
      "lng": -71.3983,
      "lat": 42.2672
    },
    "Franklin 1": {
      "lng": -71.393611,
      "lat": 42.040556
    },
    "Franklin 2": {
      "lng": -71.391667,
      "lat": 42.054167
    },
    "Freetown Solar": {
      "lng": -70.980833,
      "lat": 41.766389
    },
    "Front Street": {
      "lng": -72.5911,
      "lat": 42.1483
    },
    "Future Generation Wind": {
      "lng": -70.615833,
      "lat": 41.774444
    },
    "Gardner - Otter River Road": {
      "lng": -72.039178,
      "lat": 42.603029
    },
    "Gardner Solar 1": {
      "lng": -72.030833,
      "lat": 42.586389
    },
    "Gardners Falls": {
      "lng": -72.7308,
      "lat": 42.5914
    },
    "GELD Solar Farm": {
      "lng": -71.5,
      "lat": 42.262269
    },
    "General Electric Aircraft Engines": {
      "lng": -70.9739,
      "lat": 42.45
    },
    "Gillette SBMC": {
      "lng": -71.0559,
      "lat": 42.3436
    },
    "Golden Hills Solar": {
      "lng": -72,
      "lat": 42.4
    },
    "Goose Pond Solar": {
      "lng": -72.7807,
      "lat": 42.0077
    },
    "Grafton PV": {
      "lng": -71.662614,
      "lat": 42.225504
    },
    "Grafton Solar": {
      "lng": -71.679167,
      "lat": 42.199167
    },
    "Granby LFG": {
      "lng": -72.547189,
      "lat": 42.227281
    },
    "Granby Solar, LLC CSG": {
      "lng": -72.5222,
      "lat": 42.22712
    },
    "Greater New Bedford LFG Utiliz. Facility": {
      "lng": -70.977965,
      "lat": 41.72496
    },
    "Green Meadows": {
      "lng": -70.531944,
      "lat": 41.669444
    },
    "Greenfield Solar PV": {
      "lng": -72.58,
      "lat": 42.64
    },
    "Griffin Road Solar 2": {
      "lng": -71.99873,
      "lat": 42.14611
    },
    "Griffin Road Solar, LLC CSG": {
      "lng": -72.004384,
      "lat": 42.146633
    },
    "Groveland Solar": {
      "lng": -71.037222,
      "lat": 42.751111
    },
    "GRS Fall River": {
      "lng": -71.106469,
      "lat": 41.748669
    },
    "GSPP  Raynham TMLP, LLC CSG": {
      "lng": -71.06043,
      "lat": 41.930445
    },
    "GSPP Boxborough Littleton (MA)": {
      "lng": -71.540855,
      "lat": 42.485559
    },
    "GSPP Devens, LLC": {
      "lng": -71.585965,
      "lat": 42.544219
    },
    "GSPP Terrawatt Westfield LLC CSG": {
      "lng": -72.727023,
      "lat": 42.177501
    },
    "Hadley 2 Solar, LLC CSG": {
      "lng": -72.556505,
      "lat": 42.351237
    },
    "Hadley 3 Solar (North)": {
      "lng": -72.542595,
      "lat": 42.35885
    },
    "Hadley 3 Solar (South)": {
      "lng": -72.542595,
      "lat": 42.355782
    },
    "Hadley Falls": {
      "lng": -72.6036,
      "lat": 42.2119
    },
    "Hadley Solar NG, LLC": {
      "lng": -72.538853,
      "lat": 42.352085
    },
    "Halifax Solar": {
      "lng": -70.84382,
      "lat": 41.99527
    },
    "Hampden": {
      "lng": -72.438752,
      "lat": 42.044663
    },
    "Hampden Solar PV": {
      "lng": -72.459268,
      "lat": 42.04565
    },
    "Hampshire College Hybrid": {
      "lng": -72.46914,
      "lat": 42.37686
    },
    "Happy Hollow CSG Hybrid": {
      "lng": -72.045291,
      "lat": 42.606995
    },
    "Hardwick-Athol & Eagle Hill": {
      "lng": -72.18958,
      "lat": 42.308391
    },
    "Harris Energy Realty": {
      "lng": -72.592862,
      "lat": 42.205236
    },
    "Harwich Landfill": {
      "lng": -70.091944,
      "lat": 41.696111
    },
    "Hatfield Renewables Community Solar": {
      "lng": -72.7,
      "lat": 42.3
    },
    "Hatfield Solar PV": {
      "lng": -72.57,
      "lat": 42.41
    },
    "Haverhill Solar Power Project": {
      "lng": -71.092562,
      "lat": 42.781056
    },
    "Hewlett-Packard (HP) - Andover, MA": {
      "lng": -71.182914,
      "lat": 42.624687
    },
    "High Street Station": {
      "lng": -70.8697,
      "lat": 42.6986
    },
    "Hinsdale Solar PV": {
      "lng": -73.11,
      "lat": 42.44
    },
    "Holiday Hill Community Wind": {
      "lng": -72.871262,
      "lat": 42.212535
    },
    "Holliston LLC": {
      "lng": -71.41335,
      "lat": 42.21179
    },
    "Holliston Solar CSG": {
      "lng": -71.468223,
      "lat": 42.169641
    },
    "Holyoke Solar Cooperative at Mueller": {
      "lng": -72.682421,
      "lat": 42.190909
    },
    "Hopkinton CSG": {
      "lng": -71.53153,
      "lat": 42.203492
    },
    "Hudson": {
      "lng": -71.541362,
      "lat": 42.404047
    },
    "Hull Wind II": {
      "lng": -70.856389,
      "lat": 42.261111
    },
    "Hunt Farm Solar": {
      "lng": -72.299718,
      "lat": 42.553732
    },
    "Hunt Road Solar": {
      "lng": -70.960208,
      "lat": 42.829833
    },
    "Hurteau Solar Project Hybrid": {
      "lng": -71.5925,
      "lat": 42.0248
    },
    "Incom Solar, LLC CSG": {
      "lng": -72.017227,
      "lat": 42.107929
    },
    "Indian Hill Solar LLC": {
      "lng": -72.322222,
      "lat": 42.200833
    },
    "Indian Orchard": {
      "lng": -72.507631,
      "lat": 42.160954
    },
    "Indian Orchard Plant 1": {
      "lng": -72.524153,
      "lat": 42.156898
    },
    "Indian Orchard PV Facility": {
      "lng": -72.498889,
      "lat": 42.151389
    },
    "Indian River Hydro": {
      "lng": -72.85025,
      "lat": 42.18656
    },
    "Integrys MA Solar, LLC - Ashburnham Site": {
      "lng": -71.941944,
      "lat": 42.599444
    },
    "International Paper Woronoco Mill": {
      "lng": -72.827509,
      "lat": 42.163378
    },
    "Ipswich Wind Independence": {
      "lng": -70.846389,
      "lat": 42.717222
    },
    "Ipswich Wind Turbine": {
      "lng": -70.841944,
      "lat": 42.716389
    },
    "Iron Horse Solar 4, LLC": {
      "lng": -71.260325,
      "lat": 42.580047
    },
    "Iron Horse Solar I CSG": {
      "lng": -71.246389,
      "lat": 42.582855
    },
    "ISM Solar Dighton 2, LLC CSG": {
      "lng": -71.168944,
      "lat": 41.816858
    },
    "IXYS - Beverly": {
      "lng": -70.910778,
      "lat": 42.579489
    },
    "JH Solar, LLC CSG": {
      "lng": -72.1,
      "lat": 42.243
    },
    "Jiminy Peak Wind QF": {
      "lng": -73.292632,
      "lat": 42.543748
    },
    "Joe Jenny": {
      "lng": -71.81539,
      "lat": 42.10357
    },
    "Katama Farm": {
      "lng": -70.516944,
      "lat": 41.361111
    },
    "Kearsarge Acushnet": {
      "lng": -70.903498,
      "lat": 41.715626
    },
    "Kearsarge Amesbury Hybrid": {
      "lng": -70.938673,
      "lat": 42.837376
    },
    "Kearsarge Bellingham PV": {
      "lng": -71.458562,
      "lat": 42.071176
    },
    "Kearsarge Concord II": {
      "lng": -71.420429,
      "lat": 42.449306
    },
    "Kearsarge GB": {
      "lng": -73.407863,
      "lat": 42.192495
    },
    "Kearsarge Gill": {
      "lng": -72.477491,
      "lat": 42.671105
    },
    "Kearsarge Granby": {
      "lng": -72.508328,
      "lat": 42.222962
    },
    "Kearsarge Haverhill": {
      "lng": -71.046965,
      "lat": 42.75288
    },
    "Kearsarge Ludlow LLC": {
      "lng": -72.49416,
      "lat": 42.20128
    },
    "Kearsarge Montague": {
      "lng": -72.563491,
      "lat": 42.586363
    },
    "Kearsarge Montague BD(CSG)": {
      "lng": -72.558011,
      "lat": 42.585933
    },
    "Kearsarge Southwick LLC": {
      "lng": -72.775239,
      "lat": 42.01627
    },
    "Kearsarge Uxbridge": {
      "lng": -71.597588,
      "lat": 42.103499
    },
    "Kearsarge William Way": {
      "lng": -71.492448,
      "lat": 42.081919
    },
    "Kearsarge Wilmington": {
      "lng": -71.136855,
      "lat": 42.561264
    },
    "Kendall Green Energy LLC": {
      "lng": -71.0792,
      "lat": 42.3633
    },
    "Kenmare - Mansfield Branch Street": {
      "lng": -71.20313,
      "lat": 42.015873
    },
    "Kingston Wind Independence": {
      "lng": -70.715833,
      "lat": 41.978889
    },
    "KS Solar Six LLC": {
      "lng": -72.045,
      "lat": 42.470833
    },
    "Lakeville Solar": {
      "lng": -70.958257,
      "lat": 41.865224
    },
    "Lane Ave Solar LLC": {
      "lng": -72.0727,
      "lat": 42.2648
    },
    "Lawrence Hydroelectric Associates": {
      "lng": -71.1653,
      "lat": 42.6994
    },
    "Ledeaux Solar, LLC CSG": {
      "lng": -72.104,
      "lat": 42.241
    },
    "Lee Site 31 Solar": {
      "lng": -73.2275,
      "lat": 42.296872
    },
    "Leicester One MA Solar LLC": {
      "lng": -71.878611,
      "lat": 42.222778
    },
    "Leominster": {
      "lng": -71.755446,
      "lat": 42.561568
    },
    "Leominster (MA)-South Street-R&D": {
      "lng": -71.177223,
      "lat": 42.606184
    },
    "Lepomis PV Energy LLC": {
      "lng": -70.558333,
      "lat": 41.857222
    },
    "Long Plain Solar": {
      "lng": -72.59714,
      "lat": 42.4457
    },
    "Lowell Solar Landfill": {
      "lng": -71.357936,
      "lat": 42.626473
    },
    "Ludlow Landfill": {
      "lng": -72.5075,
      "lat": 42.182222
    },
    "Ludlow Site 72 - Conti": {
      "lng": -72.446246,
      "lat": 42.164561
    },
    "MA Military Reservation Wind Project": {
      "lng": -70.5942,
      "lat": 41.6647
    },
    "MA Solar Storage 1 Hybrid": {
      "lng": -71.742627,
      "lat": 42.450729
    },
    "Maple(CSG)": {
      "lng": -71.264331,
      "lat": 42.062793
    },
    "Marshfield PV": {
      "lng": -70.709444,
      "lat": 42.111667
    },
    "Mashpee Landfill Solar": {
      "lng": -70.477852,
      "lat": 41.62996
    },
    "Mass Midstate Solar 1": {
      "lng": -71.878333,
      "lat": 42.219722
    },
    "Mass Midstate Solar 2": {
      "lng": -72.200145,
      "lat": 42.170417
    },
    "Mass Midstate Solar 3": {
      "lng": -71.878611,
      "lat": 42.219722
    },
    "MASSPOWER": {
      "lng": -72.5225,
      "lat": 42.1575
    },
    "Maynard PV": {
      "lng": -71.435556,
      "lat": 42.428333
    },
    "MBTA South Boston Power Facility": {
      "lng": -71.031407,
      "lat": 42.339387
    },
    "McDougle-Mitchell Solar Project Hybrid CSG": {
      "lng": -72.0014,
      "lat": 42.1649
    },
    "MDFA Devens-Saratoga": {
      "lng": -71.598813,
      "lat": 42.544134
    },
    "Meadow Solar": {
      "lng": -71.133341,
      "lat": 42.230095
    },
    "Medical Area Total Energy Plant": {
      "lng": -71.10815,
      "lat": 42.33685
    },
    "Medway Station": {
      "lng": -71.446441,
      "lat": 42.140028
    },
    "Mendon": {
      "lng": -71.50187,
      "lat": 42.09749
    },
    "Merrimac Solar": {
      "lng": -70.991944,
      "lat": 42.833056
    },
    "Methuen Landfill": {
      "lng": -71.175,
      "lat": 42.744722
    },
    "Middleton Solar Park": {
      "lng": -70.998672,
      "lat": 42.581678
    },
    "Milford Power, LLC": {
      "lng": -71.511496,
      "lat": 42.129361
    },
    "Millbury Solar": {
      "lng": -71.795,
      "lat": 42.164722
    },
    "Millennium Power": {
      "lng": -72.015113,
      "lat": 42.112291
    },
    "Minuteman Energy Storage": {
      "lng": -71.098131,
      "lat": 42.567614
    },
    "MIT Central Utility Plant": {
      "lng": -71.093876,
      "lat": 42.360986
    },
    "MM Taunton Energy": {
      "lng": -71.086412,
      "lat": 41.923068
    },
    "Monson Solar": {
      "lng": -72.3325,
      "lat": 42.124444
    },
    "Monson Solar, LLC": {
      "lng": -72.290071,
      "lat": 42.098292
    },
    "Montague Road Solar": {
      "lng": -72.536863,
      "lat": 42.421721
    },
    "Montague Site 36-Grosolar": {
      "lng": -72.5,
      "lat": 42.5
    },
    "Morin Solar 2013 LLC": {
      "lng": -72.192896,
      "lat": 42.166995
    },
    "Mt Wachusett Community College": {
      "lng": -71.984194,
      "lat": 42.588583
    },
    "Mt. Tom Solar Project Hybrid": {
      "lng": -72.603,
      "lat": 42.2686
    },
    "Mystic": {
      "lng": -109.762489,
      "lat": 45.225741
    },
    "Nantucket Hybrid": {
      "lng": -70.0517,
      "lat": 41.2583
    },
    "Natick Mall": {
      "lng": -71.3843,
      "lat": 42.3009
    },
    "NEDC Solar Site": {
      "lng": -71.702979,
      "lat": 42.105237
    },
    "New Bedford (MA) Plymouth": {
      "lng": -70.649029,
      "lat": 41.925352
    },
    "New England Wind LLC": {
      "lng": -73.0236,
      "lat": 42.7297
    },
    "Newark America Mill": {
      "lng": -71.848805,
      "lat": 42.561007
    },
    "Newton Rumford Landfill": {
      "lng": -71.244586,
      "lat": 42.356341
    },
    "Nexamp Peak CSG": {
      "lng": -73.287065,
      "lat": 42.562091
    },
    "Nextsun Energy Littleton": {
      "lng": -71.510175,
      "lat": 42.551461
    },
    "NFM Solar Power LLC": {
      "lng": -72.474167,
      "lat": 42.610833
    },
    "Nolan": {
      "lng": -73.361617,
      "lat": 42.238649
    },
    "North Adams Landfill": {
      "lng": -73.092041,
      "lat": 42.669072
    },
    "North Brookfield": {
      "lng": -72.053056,
      "lat": 42.278056
    },
    "Northampton Landfill Solar PV": {
      "lng": -72.714011,
      "lat": 42.293717
    },
    "Northbridge Solar": {
      "lng": -71.645556,
      "lat": 42.166389
    },
    "Northfield Mountain": {
      "lng": -72.471643,
      "lat": 42.610683
    },
    "Norton Landfill Solar": {
      "lng": -71.150524,
      "lat": 41.959582
    },
    "Norton LLC": {
      "lng": -71.21461,
      "lat": 41.98309
    },
    "Norton Powerhouse": {
      "lng": -71.805218,
      "lat": 42.309555
    },
    "Notus Wind 1": {
      "lng": -70.6089,
      "lat": 41.6083
    },
    "Nunnepog": {
      "lng": -70.505,
      "lat": 41.358889
    },
    "Oak Bluffs Diesel Generating Facility": {
      "lng": -70.608273,
      "lat": 41.425786
    },
    "Oak Street Solar": {
      "lng": -72.496944,
      "lat": 42.148611
    },
    "Oakdale Power Station": {
      "lng": -71.802462,
      "lat": 42.38697
    },
    "Old Wardour Solar": {
      "lng": -72.037453,
      "lat": 42.237578
    },
    "One Patriot": {
      "lng": -71.265305,
      "lat": 42.093394
    },
    "Onset East Community Solar Facility": {
      "lng": -70.653675,
      "lat": 41.757027
    },
    "Onset West Community Solar Facility": {
      "lng": -70.653675,
      "lat": 41.757027
    },
    "Onyx - Brockton Thatcher Landfill Solar CSG": {
      "lng": -70.983446,
      "lat": 42.066369
    },
    "Onyx - Pembroke Landfill Solar": {
      "lng": -70.802417,
      "lat": 42.049906
    },
    "Orange PV": {
      "lng": -72.293889,
      "lat": 42.585
    },
    "Oxford": {
      "lng": -71.850758,
      "lat": 42.141319
    },
    "Padelford Solar": {
      "lng": -71.065,
      "lat": 41.853333
    },
    "Palmer": {
      "lng": -72.32536,
      "lat": 42.20382
    },
    "Palmer Landfill": {
      "lng": -72.315526,
      "lat": 42.216634
    },
    "Palmer Solar LLC": {
      "lng": -72.36558,
      "lat": 42.187176
    },
    "Palombo B Solar, LLC CSG": {
      "lng": -71.596,
      "lat": 42.028
    },
    "Palombo Solar, LLC CSG": {
      "lng": -71.599,
      "lat": 42.025
    },
    "Partridge Hill Solar Hybrid": {
      "lng": -71.9371,
      "lat": 42.1189
    },
    "Partridgeville Hybrid CSG": {
      "lng": -72.257389,
      "lat": 42.571449
    },
    "Pepperell Hydro Power Plant": {
      "lng": -71.575556,
      "lat": 42.668889
    },
    "Peterson Road Solar": {
      "lng": -72.3,
      "lat": 42.1
    },
    "Pioneer Valley Resource Recovery": {
      "lng": -72.5917,
      "lat": 42.0906
    },
    "Pittsfield 44 - M&W PV": {
      "lng": -73.215833,
      "lat": 42.487419
    },
    "Pittsfield Generating": {
      "lng": -73.2181,
      "lat": 42.4564
    },
    "Plainfield Community Solar LLC": {
      "lng": -72.9191,
      "lat": 42.5205
    },
    "Pleasantdale Road Community Solar": {
      "lng": -71.987015,
      "lat": 42.322246
    },
    "Plymouth Site 1": {
      "lng": -70.778611,
      "lat": 41.96
    },
    "Plymouth Solar": {
      "lng": -70.688427,
      "lat": 41.94726
    },
    "Potter": {
      "lng": -70.9672,
      "lat": 42.235
    },
    "Princeton Wind Farm": {
      "lng": -71.9,
      "lat": 42.4789
    },
    "Putts Bridge": {
      "lng": -72.48643,
      "lat": 42.157896
    },
    "Quittacas Pond Solar": {
      "lng": -70.913333,
      "lat": 41.781944
    },
    "Raboth - Marion Drive": {
      "lng": -70.72541,
      "lat": 41.96951
    },
    "Rail Trail": {
      "lng": -73.363849,
      "lat": 42.243134
    },
    "Randall Solar Project Hybrid": {
      "lng": -71.6611,
      "lat": 42.3921
    },
    "Randolph": {
      "lng": -71.077386,
      "lat": 42.177563
    },
    "Ravenbrook": {
      "lng": -70.819421,
      "lat": 41.919675
    },
    "Redbridge": {
      "lng": -72.409521,
      "lat": 42.176047
    },
    "Redbrook Community Solar 1": {
      "lng": -70.614839,
      "lat": 41.829117
    },
    "Rehoboth Solar": {
      "lng": -71.2425,
      "lat": 41.901667
    },
    "Renew Canal 1 CSG LLC": {
      "lng": -70.521172,
      "lat": 41.766987
    },
    "RGS-Rutland VNM SREC II Project (MA)": {
      "lng": -71.978189,
      "lat": 42.43177
    },
    "Rising Paper": {
      "lng": -73.344463,
      "lat": 42.228622
    },
    "Riverside Holyoke": {
      "lng": -72.594432,
      "lat": 42.200592
    },
    "Rockland Solar CSG": {
      "lng": -70.897778,
      "lat": 42.089167
    },
    "Rounseville Solar": {
      "lng": -70.828527,
      "lat": 41.738381
    },
    "Rousselot Inc": {
      "lng": -70.938657,
      "lat": 42.521747
    },
    "Route 57 Solar": {
      "lng": -72.664167,
      "lat": 42.058889
    },
    "Ryan Road Solar LLC Hybrid(CSG)": {
      "lng": -72.0639,
      "lat": 42.2637
    },
    "Salem Harbor Station NGCC": {
      "lng": -70.878239,
      "lat": 42.525486
    },
    "Sampson Road Community Solar": {
      "lng": -72,
      "lat": 42.1
    },
    "Savoy Solar PV": {
      "lng": -73.05,
      "lat": 42.56
    },
    "Scituate PV": {
      "lng": -70.740556,
      "lat": 42.1775
    },
    "Scituate Wind": {
      "lng": -70.728467,
      "lat": 42.175083
    },
    "SELCO Community Solar": {
      "lng": -71.68807,
      "lat": 42.26564
    },
    "SEMASS Resource Recovery": {
      "lng": -70.7875,
      "lat": 41.8022
    },
    "Settlers Solar": {
      "lng": -71.76125,
      "lat": 42.60734
    },
    "Shaffer": {
      "lng": -71.249444,
      "lat": 42.594167
    },
    "Sherman": {
      "lng": -72.9303,
      "lat": 42.7297
    },
    "Shirley Landfill": {
      "lng": -71.67636,
      "lat": 42.54171
    },
    "Shirley Water": {
      "lng": -71.62348,
      "lat": 42.554584
    },
    "Shrewsbury": {
      "lng": -71.7172,
      "lat": 42.2747
    },
    "Shrewsbury Solar": {
      "lng": -71.695833,
      "lat": 42.266111
    },
    "Shuman Solar": {
      "lng": -71.066039,
      "lat": 42.114918
    },
    "Signature Breads Chelsea": {
      "lng": -71.046617,
      "lat": 42.393622
    },
    "Silver Lake Solar Photovoltaic Facility": {
      "lng": -73.2403,
      "lat": 42.4525
    },
    "SJA Solar LLC-Solterra Monastery CSG": {
      "lng": -72.008468,
      "lat": 42.283439
    },
    "Smith & Wesson at Springfield MA PV": {
      "lng": -72.5509,
      "lat": 42.13794
    },
    "Smith College Central Heating Plant": {
      "lng": -72.639444,
      "lat": 42.3125
    },
    "Solten Plainville 6000, LLC": {
      "lng": -71.293279,
      "lat": 42.041166
    },
    "Southampton Solar PV": {
      "lng": -72.71,
      "lat": 42.2
    },
    "Southbridge Landfill Gas-to-Energy": {
      "lng": -72.0325,
      "lat": 42.104444
    },
    "Southbridge PV": {
      "lng": -71.992868,
      "lat": 42.047361
    },
    "Southbridge Solar": {
      "lng": -72.011672,
      "lat": 42.090932
    },
    "Southern Sky Renew Energy Berkley LLC": {
      "lng": -71.091296,
      "lat": 41.83777
    },
    "Southwick Solar PV": {
      "lng": -72.75,
      "lat": 42.05
    },
    "Spring Hill Road": {
      "lng": -72.180652,
      "lat": 42.419036
    },
    "Spring Street Solar 1 CSG": {
      "lng": -70.766403,
      "lat": 41.942522
    },
    "Springfield Solar PV": {
      "lng": -72.549688,
      "lat": 42.134453
    },
    "St. Joseph's Solar, LLC CSG": {
      "lng": -71.972112,
      "lat": 42.143586
    },
    "Stafford St 2 Community Solar": {
      "lng": -71.87065,
      "lat": 42.224044
    },
    "Stafford St Solar 1 CSG": {
      "lng": -71.87065,
      "lat": 42.224044
    },
    "Stafford St Solar 3 CSG": {
      "lng": -71.87065,
      "lat": 42.224044
    },
    "State Street Solar LLC": {
      "lng": -72.322222,
      "lat": 42.200833
    },
    "Station 9 Energy Storage System": {
      "lng": -70.976448,
      "lat": 42.187743
    },
    "Stetson Road Solar - Barre I": {
      "lng": -72.1027,
      "lat": 42.4029
    },
    "Stone Hill Solar CSG": {
      "lng": -71.089157,
      "lat": 42.059104
    },
    "Stony Brook Energy Center": {
      "lng": -72.5106,
      "lat": 42.1978
    },
    "Stored Solar Fitchburg, LLC": {
      "lng": -71.8528,
      "lat": 42.5428
    },
    "Stow PV": {
      "lng": -71.506111,
      "lat": 42.436944
    },
    "Sudbury Landfill": {
      "lng": -71.385,
      "lat": 42.363889
    },
    "Sullivan Solar": {
      "lng": -70.956389,
      "lat": 41.655
    },
    "Sunderland Solar PV": {
      "lng": -72.539889,
      "lat": 42.429126
    },
    "Sunpin Blandford": {
      "lng": -73.0247,
      "lat": 42.17337
    },
    "Sutton Solar 2, LLC CSG": {
      "lng": -71.69795,
      "lat": 42.10252
    },
    "Sutton Solar CSG": {
      "lng": -71.697711,
      "lat": 42.102447
    },
    "Syncarpha Blandford Hybrid CSG": {
      "lng": -72.938863,
      "lat": 42.213074
    },
    "Syncarpha Freetown": {
      "lng": -70.786494,
      "lat": 41.768919
    },
    "Syncarpha Halifax Hybrid CSG": {
      "lng": -70.856834,
      "lat": 41.983721
    },
    "Syncarpha Hancock I CSG": {
      "lng": -73.336092,
      "lat": 42.433031
    },
    "Syncarpha Hancock II CSG": {
      "lng": -73.346996,
      "lat": 42.426866
    },
    "Syncarpha Hancock III CSG": {
      "lng": -73.332624,
      "lat": 42.428054
    },
    "Syncarpha Leicester Hybrid CSG": {
      "lng": -71.887883,
      "lat": 42.254281
    },
    "Syncarpha Northampton Hybrid CSG": {
      "lng": -72.709436,
      "lat": 42.290526
    },
    "Syncarpha Northbridge I Hybrid(CSG)": {
      "lng": -71.648261,
      "lat": 42.11239
    },
    "Syncarpha Northbridge II Hybrid CSG": {
      "lng": -71.644047,
      "lat": 42.115652
    },
    "Syncarpha Palmer, LLC": {
      "lng": -72.309507,
      "lat": 42.222824
    },
    "Syncarpha Puddon I Hybrid CSG": {
      "lng": -71.633132,
      "lat": 42.139492
    },
    "Syncarpha Puddon II Hybrid CSG": {
      "lng": -71.631045,
      "lat": 42.133867
    },
    "Syncarpha Still River, LLC CSG": {
      "lng": -71.641533,
      "lat": 42.42075
    },
    "Tanglewood Circle Solar 1 LLC": {
      "lng": -73.109957,
      "lat": 42.252046
    },
    "Tanner Street Generation, LLC": {
      "lng": -71.313235,
      "lat": 42.632124
    },
    "Templeton": {
      "lng": -72.067328,
      "lat": 42.521217
    },
    "TES Rowtier": {
      "lng": -71.8567,
      "lat": 42.541925
    },
    "Texon Hydroelectric Project": {
      "lng": -72.85964,
      "lat": 42.221412
    },
    "Theodore Drive Community Solar": {
      "lng": -71.8,
      "lat": 42.5
    },
    "Tihonet Solar": {
      "lng": -70.723333,
      "lat": 41.7875
    },
    "Tisbury Landfill Solar": {
      "lng": -70.613056,
      "lat": 41.444722
    },
    "Tolland Solar NG LLC": {
      "lng": -73.061431,
      "lat": 42.089484
    },
    "Town of East Bridgewater CSG": {
      "lng": -70.955833,
      "lat": 42.051111
    },
    "Town of Foxborough - Landfill (SREC II) CSG": {
      "lng": -71.135414,
      "lat": 42.059675
    },
    "Town of Lexington Solar": {
      "lng": -71.25546,
      "lat": 42.469135
    },
    "Town of Needham VNEM CSG": {
      "lng": -71.259394,
      "lat": 42.276717
    },
    "Town of Norfolk MA at Medway Branch": {
      "lng": -71.340278,
      "lat": 42.127778
    },
    "Town of Otis Wind Energy Project": {
      "lng": -73.025764,
      "lat": 42.225765
    },
    "Town of Uxbridge MA at Commerce Dr": {
      "lng": -71.6175,
      "lat": 42.043889
    },
    "Town of Ware - Canadian Tree(CSG)": {
      "lng": -72.295532,
      "lat": 42.263482
    },
    "Truck Stop": {
      "lng": -73.396681,
      "lat": 42.341063
    },
    "True North": {
      "lng": -70.898333,
      "lat": 42.8475
    },
    "Turners Falls": {
      "lng": -72.561458,
      "lat": 42.608042
    },
    "Twiss Street Solar": {
      "lng": -72.748056,
      "lat": 42.148333
    },
    "Tyngsborough Solar": {
      "lng": -71.446484,
      "lat": 42.669744
    },
    "UMASS": {
      "lng": -72.5325,
      "lat": 42.391306
    },
    "UMass Boston": {
      "lng": -71.036368,
      "lat": 42.312977
    },
    "Univ of Massachusetts Medical Center": {
      "lng": -71.759772,
      "lat": 42.279015
    },
    "Upper Blackstone (MA) Treasure Valley": {
      "lng": -71.987633,
      "lat": 42.332831
    },
    "Upton Community Solar": {
      "lng": -71.7,
      "lat": 42.1
    },
    "Vuelta Solar": {
      "lng": -72.037453,
      "lat": 42.237578
    },
    "W. Orange Rd Solar LLC": {
      "lng": -72.3418,
      "lat": 42.6026
    },
    "Wachusett Solar": {
      "lng": -71.71081,
      "lat": 42.48392
    },
    "Walpole Solar 2": {
      "lng": -71.265833,
      "lat": 42.117222
    },
    "Ware Palmer Road Solar LLC CSG": {
      "lng": -72.274926,
      "lat": 42.247172
    },
    "Wareham MA 1 Community Solar": {
      "lng": -70.70463,
      "lat": 41.80167
    },
    "Wareham Solar PV": {
      "lng": -70.75,
      "lat": 41.78
    },
    "Washburn Road Solar": {
      "lng": -70.965254,
      "lat": 41.777159
    },
    "Waste Water Treatment Plant": {
      "lng": -73.24,
      "lat": 42.41
    },
    "Waters River": {
      "lng": -70.928314,
      "lat": 42.542949
    },
    "Webster Solar": {
      "lng": -71.85031,
      "lat": 42.0281
    },
    "Wellesley College Central Utility Plant": {
      "lng": -71.308111,
      "lat": 42.293927
    },
    "West A&B Solar Project Hybrid CSG": {
      "lng": -72.0038,
      "lat": 42.0748
    },
    "West Boylston Community Shared Solar": {
      "lng": -71.771135,
      "lat": 42.331362
    },
    "West Bridgewater AB CSG": {
      "lng": -71.0336,
      "lat": 42.0253
    },
    "West Brookfield Solar - Gilbertsville Rd (CSG)": {
      "lng": -72.210291,
      "lat": 42.272516
    },
    "West Brookfield Solar, LLC": {
      "lng": -72.129525,
      "lat": 42.222798
    },
    "West Groton CHP": {
      "lng": -71.633767,
      "lat": 42.61343
    },
    "West Springfield": {
      "lng": -72.595,
      "lat": 42.095
    },
    "West Street Solar 1 LLC": {
      "lng": -72.0446,
      "lat": 42.595
    },
    "West Tisbury Generating Facility": {
      "lng": -70.620089,
      "lat": 41.392486
    },
    "West Water Street": {
      "lng": -71.0927,
      "lat": 41.8809
    },
    "Westborough Solar LLC": {
      "lng": -71.609167,
      "lat": 42.263333
    },
    "Westford Solar Park": {
      "lng": -71.419444,
      "lat": 42.636389
    },
    "Westminster Renewables, LLC CSG": {
      "lng": -71.896242,
      "lat": 42.583671
    },
    "Weston Landfill Solar": {
      "lng": -71.2825,
      "lat": 42.374553
    },
    "Westport B Community Solar Garden": {
      "lng": -71.08973,
      "lat": 41.70463
    },
    "Westport Community Solar Garden": {
      "lng": -71.093832,
      "lat": 41.68994
    },
    "Westport MA 1 Community Solar": {
      "lng": -71.056963,
      "lat": 41.621068
    },
    "Westport MA 2 Community Solar": {
      "lng": -71.056963,
      "lat": 41.621068
    },
    "Whately Solar": {
      "lng": -72.623611,
      "lat": 42.446111
    },
    "Whatley Renewables": {
      "lng": -71.61095,
      "lat": 42.44577
    },
    "Wheelabrator Millbury Facility": {
      "lng": -71.7669,
      "lat": 42.2214
    },
    "Wheelabrator North Andover": {
      "lng": -71.1219,
      "lat": 42.7262
    },
    "Wheelabrator Saugus": {
      "lng": -70.9804,
      "lat": 42.447
    },
    "Wilbraham": {
      "lng": -72.38129,
      "lat": 42.10391
    },
    "Wilbur Woods Solar LLC": {
      "lng": -72.3452,
      "lat": 42.6113
    },
    "Wilkins Station": {
      "lng": -70.8583,
      "lat": 42.5131
    },
    "Williams College - Campus CHP": {
      "lng": -73.202222,
      "lat": 42.709722
    },
    "Williamsburg": {
      "lng": -72.768999,
      "lat": 42.425855
    },
    "Williamsburg Solar LLC VNEM CSG": {
      "lng": -72.713442,
      "lat": 42.387088
    },
    "Williamsville Hybrid CSG": {
      "lng": -72.069781,
      "lat": 42.469505
    },
    "Wilmarth Solar": {
      "lng": -71.353545,
      "lat": 42.025049
    },
    "Wilmington Solar": {
      "lng": -71.175281,
      "lat": 42.546281
    },
    "Winchendon Landfill Solar": {
      "lng": -72.085671,
      "lat": 42.689687
    },
    "Winchendon Solar": {
      "lng": -71.991667,
      "lat": 42.659444
    },
    "Woburn": {
      "lng": -71.13911,
      "lat": 42.5113
    },
    "Woodchuck Solar, LLC CSG": {
      "lng": -71.975189,
      "lat": 42.258218
    },
    "Woodland Avenue Solar 1": {
      "lng": -71.31839,
      "lat": 41.8884
    },
    "Woodland Road": {
      "lng": -73.235486,
      "lat": 42.336215
    },
    "Worcester Landfill": {
      "lng": -71.791,
      "lat": 42.219
    },
    "WYM 1250 Palmer LLC": {
      "lng": -72.26974,
      "lat": 42.151092
    },
    "ZPD-PT Solar Project 2017-006 LLC Hybrid": {
      "lng": -72.21,
      "lat": 42.23
    },
    "ZPD-PT Solar Project 2017-021 LLC Hybrid CSG": {
      "lng": -72.47,
      "lat": 42.1
    },
    "ZPD-PT Solar Project 2017-023 LLC Hybrid CSG": {
      "lng": -72.01,
      "lat": 42.07
    },
    "ZPD-PT Solar Project 2017-038 Hybrid LLC": {
      "lng": -71.6734,
      "lat": 42.516
    },
    "ZPD-PT Solar Project 2017-044 LLC CSG": {
      "lng": -72.0071,
      "lat": 42.2782
    },
    "6685 Santa Barbara Ct": {
      "lng": -76.743011,
      "lat": 39.189589
    },
    "7448 Candlewood Road": {
      "lng": -76.7,
      "lat": 39.16
    },
    "AES Warrior Run": {
      "lng": -78.745333,
      "lat": 39.595171
    },
    "AES Warrior Run Energy Storage Project": {
      "lng": -78.745333,
      "lat": 39.593075
    },
    "Alpha Ridge LFG": {
      "lng": -76.901944,
      "lat": 39.305
    },
    "Amazon Maryland DCA1": {
      "lng": -76.469011,
      "lat": 39.222576
    },
    "American Sugar Refining, Inc.": {
      "lng": -76.5956,
      "lat": 39.2744
    },
    "Annapolis Solar Park, LLC": {
      "lng": -76.573773,
      "lat": 38.992144
    },
    "Anne Arundel County Public Schools": {
      "lng": -76.498041,
      "lat": 39.13728
    },
    "APG Combined Heat and Power Plant": {
      "lng": -76.2991,
      "lat": 39.3975
    },
    "APG New Chesapeake": {
      "lng": -76.146701,
      "lat": 39.493704
    },
    "APG Old Bayside": {
      "lng": -76.146055,
      "lat": 39.498961
    },
    "Archdiocese of Baltimore J": {
      "lng": -76.217,
      "lat": 39.445
    },
    "Archdiocese of Baltimore L": {
      "lng": -76.217,
      "lat": 39.445
    },
    "Back River Waste Water Treatment": {
      "lng": -76.494167,
      "lat": 39.299722
    },
    "Baker Point": {
      "lng": -77.363415,
      "lat": 39.570338
    },
    "Baltimore City B": {
      "lng": -76.22,
      "lat": 39.436
    },
    "Baltimore City D": {
      "lng": -76.22,
      "lat": 39.436
    },
    "Baltimore City F": {
      "lng": -76.22,
      "lat": 39.436
    },
    "Baltimore City G": {
      "lng": -76.22,
      "lat": 39.436
    },
    "Bd of Educ of Queen Anne's Cnty, Cnty HS": {
      "lng": -76.054316,
      "lat": 39.035986
    },
    "Berlin": {
      "lng": -75.217222,
      "lat": 38.327778
    },
    "Blue Star": {
      "lng": -75.85,
      "lat": 39.35
    },
    "Bluefin Origination 1": {
      "lng": -76.82843,
      "lat": 38.73169
    },
    "Bomber CSG": {
      "lng": -76.8526,
      "lat": 39.59902
    },
    "Bowie State Solar": {
      "lng": -76.759479,
      "lat": 39.019083
    },
    "Brandon Shores": {
      "lng": -76.5389,
      "lat": 39.18
    },
    "Brandywine Power Facility": {
      "lng": -76.8678,
      "lat": 38.6681
    },
    "Brown Station Road Plant I": {
      "lng": -76.773149,
      "lat": 38.824863
    },
    "Brown Station Road Plant II": {
      "lng": -76.785912,
      "lat": 38.847585
    },
    "Bulldog Solar One, LLC": {
      "lng": -76.6972,
      "lat": 38.9152
    },
    "Burns Solar One LLC": {
      "lng": -76.85446,
      "lat": 39.47653
    },
    "Calvert Cliffs Nuclear Power Plant": {
      "lng": -76.4417,
      "lat": 38.4344
    },
    "CCBC-Catonsville": {
      "lng": -76.735,
      "lat": 39.2525
    },
    "Cecil County CCVT HS": {
      "lng": -75.808331,
      "lat": 39.651002
    },
    "Central Utility Plant at White Oak": {
      "lng": -76.9825,
      "lat": 39.039444
    },
    "CES VMT Solar": {
      "lng": -77.727117,
      "lat": 39.680865
    },
    "Chalk Point": {
      "lng": -76.6861,
      "lat": 38.5444
    },
    "Chalk Point Steam": {
      "lng": -76.6861,
      "lat": 38.5444
    },
    "Chesapeake College": {
      "lng": -76.083244,
      "lat": 38.953389
    },
    "Chester Woods Point Solar, LLC CSG": {
      "lng": -76.300435,
      "lat": 38.94843
    },
    "Chimes West Friendship (Nixon Farms)": {
      "lng": -76.957413,
      "lat": 39.296749
    },
    "Church Hill": {
      "lng": -75.959681,
      "lat": 39.100947
    },
    "City of Bowie": {
      "lng": -76.688773,
      "lat": 38.904641
    },
    "City of Havre De Grace C": {
      "lng": -76.22,
      "lat": 39.436
    },
    "CNE at Cambridge MD": {
      "lng": -76.0192,
      "lat": 38.5158
    },
    "Conowingo": {
      "lng": -76.1752,
      "lat": 39.6572
    },
    "CPV St. Charles Energy Center": {
      "lng": -76.8919,
      "lat": 38.5686
    },
    "Crisfield": {
      "lng": -75.837679,
      "lat": 37.994134
    },
    "Criterion": {
      "lng": -79.29,
      "lat": 39.4047
    },
    "Deep Creek": {
      "lng": -79.413,
      "lat": 39.523
    },
    "Dickerson": {
      "lng": -77.4644,
      "lat": 39.2097
    },
    "Eastern Correctional Institute": {
      "lng": -75.704,
      "lat": 38.159
    },
    "Eastern Landfill Gas LLC": {
      "lng": -76.392006,
      "lat": 39.389728
    },
    "Easton": {
      "lng": -76.0769,
      "lat": 38.7786
    },
    "Easton 2": {
      "lng": -76.07,
      "lat": 38.795
    },
    "Elkton Solar": {
      "lng": -75.836235,
      "lat": 39.603531
    },
    "Emmitsburg Solar Arrays": {
      "lng": -77.314427,
      "lat": 39.69373
    },
    "Fair Wind": {
      "lng": -79.291143,
      "lat": 39.404564
    },
    "FedEx Field Solar Facility": {
      "lng": -76.864444,
      "lat": 38.907778
    },
    "First Baptist Church of Glenarden": {
      "lng": -76.87,
      "lat": 38.82
    },
    "Fort Detrick Solar PV": {
      "lng": -77.45,
      "lat": 39.44
    },
    "Fourmile Ridge": {
      "lng": -79.011111,
      "lat": 39.641111
    },
    "Francis Scott Key Mall": {
      "lng": -77.40947,
      "lat": 39.38588
    },
    "Frederick County - Landfill": {
      "lng": -77.34985,
      "lat": 39.373263
    },
    "Garrett County - DPU Treatment Plant": {
      "lng": -79.392991,
      "lat": 39.505187
    },
    "Gateway Solar": {
      "lng": -75.274195,
      "lat": 38.376941
    },
    "General Motors Corp at White Marsh MD": {
      "lng": -76.4405,
      "lat": 39.3716
    },
    "Gibbons CSG": {
      "lng": -75.214861,
      "lat": 38.115402
    },
    "Great Bay Solar 1": {
      "lng": -75.6975,
      "lat": 38.168889
    },
    "GWCC PV Solar Farm": {
      "lng": -76.908997,
      "lat": 39.017238
    },
    "Havre de Grace II - E at Perryman": {
      "lng": -76.22,
      "lat": 39.436
    },
    "Herbert A Wagner": {
      "lng": -76.5268,
      "lat": 39.1781
    },
    "Herbert Farm Solar": {
      "lng": -76.799444,
      "lat": 38.545556
    },
    "Hollins Ferry CSG": {
      "lng": -76.68808,
      "lat": 39.225752
    },
    "Hostetter Solar One, LLC": {
      "lng": -77.70058,
      "lat": 39.66448
    },
    "IGS Solar I - BWI2": {
      "lng": -76.548,
      "lat": 39.267
    },
    "IGS Solar I - BWI5": {
      "lng": -76.548,
      "lat": 39.27
    },
    "IKEA College Park 411": {
      "lng": -76.926,
      "lat": 39.021
    },
    "IKEA Perryville 460": {
      "lng": -76.055278,
      "lat": 39.560833
    },
    "Inner Harbor East Heating": {
      "lng": -76.59941,
      "lat": 39.283677
    },
    "Kent County - Worton Complex": {
      "lng": -76.090833,
      "lat": 39.280833
    },
    "Kent County-Kennedyville": {
      "lng": -75.982222,
      "lat": 39.297222
    },
    "Keys Energy Center": {
      "lng": -76.827785,
      "lat": 38.695519
    },
    "Kingsville CSG": {
      "lng": -76.39772,
      "lat": 39.41805
    },
    "Kirby Road Solar, LLC": {
      "lng": -76.923695,
      "lat": 38.77897
    },
    "Longview Solar": {
      "lng": -75.672715,
      "lat": 38.394728
    },
    "Macy's MD Joppa Solar Project": {
      "lng": -76.32095,
      "lat": 39.44752
    },
    "Maryland Solar": {
      "lng": -77.720278,
      "lat": 39.563056
    },
    "Mason Solar One LLC": {
      "lng": -75.91848,
      "lat": 39.69196
    },
    "McCormick & Co. Inc. at Belcamp": {
      "lng": -76.2292,
      "lat": 39.4836
    },
    "MD - CS - Potomac Edison Co - GA29 TPE": {
      "lng": -79.387898,
      "lat": 39.59535
    },
    "MEBA": {
      "lng": -76.138912,
      "lat": 38.772903
    },
    "Millersville LFG": {
      "lng": -76.676389,
      "lat": 39.084722
    },
    "MNCPPC Germantown Solar": {
      "lng": -77.317948,
      "lat": 39.149037
    },
    "MNCPPC Randall Farm": {
      "lng": -76.803146,
      "lat": 38.831624
    },
    "Montgomery County Correctional Facility": {
      "lng": -77.293217,
      "lat": 39.237945
    },
    "Montgomery County Oaks LFGE Plant": {
      "lng": -77.1189,
      "lat": 39.1937
    },
    "Montgomery County Resource Recovery": {
      "lng": -77.4556,
      "lat": 39.2006
    },
    "Montgomery County Solar": {
      "lng": -77.236,
      "lat": 39.113
    },
    "Morgantown": {
      "lng": -75.913688,
      "lat": 40.174563
    },
    "Mount Saint Mary's": {
      "lng": -77.345,
      "lat": 39.675
    },
    "NIH Cogeneration Facility": {
      "lng": -77.0939,
      "lat": 39
    },
    "NIST Solar": {
      "lng": -77.216,
      "lat": 39.127
    },
    "NRG Chalk Point CT": {
      "lng": -76.6861,
      "lat": 38.5444
    },
    "OER Checkerspot": {
      "lng": -76.574153,
      "lat": 38.783572
    },
    "OER Monarch CSG": {
      "lng": -76.789482,
      "lat": 38.799594
    },
    "Old Court Rd Solar": {
      "lng": -76.8351,
      "lat": 39.34167
    },
    "P52ES 1755 Henryton Rd Phase 1 LLC CSG": {
      "lng": -76.92965,
      "lat": 39.32015
    },
    "P52ES 1755 Henryton Rd Phase 2 LLC": {
      "lng": -76.92965,
      "lat": 39.32015
    },
    "P52ES Raphel Rd Community Solar LLC": {
      "lng": -76.403,
      "lat": 39.4167
    },
    "Perdue Salisbury Photovoltaic": {
      "lng": -75.535278,
      "lat": 38.373611
    },
    "Perryman": {
      "lng": -76.221761,
      "lat": 39.442836
    },
    "Pfeffers": {
      "lng": -76.393562,
      "lat": 39.42757
    },
    "Philadelphia": {
      "lng": -76.5636,
      "lat": 39.2986
    },
    "Pinesburg Solar LLC": {
      "lng": -77.860901,
      "lat": 39.625172
    },
    "Pittman Solar One LLC": {
      "lng": -78.108,
      "lat": 39.68753
    },
    "Presbyterian Senior Living Service": {
      "lng": -76.516324,
      "lat": 39.4476
    },
    "Queen Anne's County": {
      "lng": -76.016389,
      "lat": 39.031667
    },
    "Rock Hall": {
      "lng": -76.231644,
      "lat": 39.144369
    },
    "Rock Springs Generating Facility": {
      "lng": -76.15976,
      "lat": 39.71901
    },
    "Rockfish Solar LLC": {
      "lng": -76.87486,
      "lat": 38.591545
    },
    "Roth Rock North Wind Farm, LLC": {
      "lng": -79.434444,
      "lat": 39.287222
    },
    "Roth Rock Wind Farm LLC": {
      "lng": -79.467222,
      "lat": 39.254444
    },
    "Shepherds Mill CSG": {
      "lng": -77.161771,
      "lat": 39.562305
    },
    "Sheriff Road": {
      "lng": -76.894882,
      "lat": 38.911287
    },
    "Smith Island": {
      "lng": -76.038881,
      "lat": 37.9955
    },
    "Snowden River CSG": {
      "lng": -76.837373,
      "lat": 39.175364
    },
    "Sod Run WTP A": {
      "lng": -76.22,
      "lat": 39.436
    },
    "Sol Phoenix": {
      "lng": -76.95,
      "lat": 38.85
    },
    "Solar Hagerstown": {
      "lng": -77.67837,
      "lat": 39.64913
    },
    "Spruce - WCMD - Creek": {
      "lng": -77.82329,
      "lat": 39.67185
    },
    "Spruce - WCMD - Resh I": {
      "lng": -77.80145,
      "lat": 39.68185
    },
    "Spruce - WCMD - Rubble I": {
      "lng": -77.82332,
      "lat": 39.61849
    },
    "Spruce - WCMD - Rubble II": {
      "lng": -77.82543,
      "lat": 39.62143
    },
    "Synergen Panorama, LLC CSG": {
      "lng": -76.973379,
      "lat": 38.772107
    },
    "The Clorox Company": {
      "lng": -76.201342,
      "lat": 39.472128
    },
    "Timonium Fairgrounds": {
      "lng": -76.63,
      "lat": 39.447
    },
    "Todd Solar": {
      "lng": -75.835207,
      "lat": 38.613328
    },
    "Town of Chestertown- Chestertown WWTP": {
      "lng": -76.076693,
      "lat": 39.199453
    },
    "UMCES Ground Mount": {
      "lng": -76.13475,
      "lat": 38.583064
    },
    "UMCP CHP Plant": {
      "lng": -76.935478,
      "lat": 38.985844
    },
    "UMES (MD) - Princess Anne": {
      "lng": -75.6764,
      "lat": 38.20257
    },
    "UMMS at Pocomoke": {
      "lng": -75.611111,
      "lat": 38.098056
    },
    "Upper Marlboro 1 CSG": {
      "lng": -76.800911,
      "lat": 38.789216
    },
    "Vienna": {
      "lng": -75.8208,
      "lat": 38.4878
    },
    "Wheelabrator Baltimore Refuse": {
      "lng": -76.629653,
      "lat": 39.266041
    },
    "White CSG": {
      "lng": -76.480453,
      "lat": 39.205804
    },
    "White Marsh Mall": {
      "lng": -76.46813,
      "lat": 39.37614
    },
    "Wicomico": {
      "lng": -75.6344,
      "lat": 38.3847
    },
    "Wildcat Point Generation Facility": {
      "lng": -76.161625,
      "lat": 39.719364
    },
    "Wor-Wic Community College - Offsite": {
      "lng": -75.690118,
      "lat": 38.413274
    },
    "Wye Mills VNEM CSG": {
      "lng": -76.045658,
      "lat": 38.94037
    },
    "Acton": {
      "lng": -70.912751,
      "lat": 43.543731
    },
    "Androscoggin Energy": {
      "lng": -70.242312,
      "lat": 44.506315
    },
    "Androscoggin Mill": {
      "lng": -70.239474,
      "lat": 44.506273
    },
    "Anson Abenaki Hydros": {
      "lng": -69.8867,
      "lat": 44.7975
    },
    "Athens Energy": {
      "lng": -69.660705,
      "lat": 44.945514
    },
    "Augusta PV - BD Solar Augusta LLC": {
      "lng": -69.731,
      "lat": 44.328
    },
    "Aziscohos Hydroelectric Project": {
      "lng": -70.9975,
      "lat": 44.9439
    },
    "Bar Mills": {
      "lng": -70.5525,
      "lat": 43.6136
    },
    "Barker Lower": {
      "lng": -70.2297,
      "lat": 44.0867
    },
    "Beaver Ridge Wind": {
      "lng": -69.35,
      "lat": 44.4967
    },
    "Benton Falls Associates": {
      "lng": -69.55452,
      "lat": 44.580175
    },
    "Bingham Wind": {
      "lng": -69.761944,
      "lat": 45.103333
    },
    "Bonny Eagle": {
      "lng": -70.611553,
      "lat": 43.687688
    },
    "Brassua Hydroelectric Project": {
      "lng": -69.812168,
      "lat": 45.6603
    },
    "Brunswick Hydro": {
      "lng": -69.967819,
      "lat": 43.920486
    },
    "Bucksport Generation LLC": {
      "lng": -68.8046,
      "lat": 44.5738
    },
    "Bull Hill Wind Project": {
      "lng": -68.2425,
      "lat": 44.723056
    },
    "BWC Maces Pond LLC": {
      "lng": -69.12333,
      "lat": 44.17362
    },
    "Canton Mountain Wind": {
      "lng": -70.303056,
      "lat": 44.514722
    },
    "Cape Gas Turbine": {
      "lng": -70.2544,
      "lat": 43.6436
    },
    "Cataract Hydro": {
      "lng": -70.4472,
      "lat": 43.4956
    },
    "Charles E Monty": {
      "lng": -70.220851,
      "lat": 44.099461
    },
    "Deer Rips": {
      "lng": -70.203715,
      "lat": 44.134572
    },
    "Eastern Maine Medical Center": {
      "lng": -68.750833,
      "lat": 44.808333
    },
    "Ellsworth Hydro Station": {
      "lng": -68.4306,
      "lat": 44.5442
    },
    "Fairfield PV - BD Solar Fairfield LLC": {
      "lng": -69.597,
      "lat": 44.634
    },
    "Farmington Solar": {
      "lng": -70.100835,
      "lat": 44.654659
    },
    "Fox Island Wind LLC": {
      "lng": -68.865833,
      "lat": 44.094167
    },
    "Gardiner": {
      "lng": -69.785675,
      "lat": 44.221796
    },
    "Georges River Energy": {
      "lng": -69.2019,
      "lat": 44.3331
    },
    "Great Lakes Hydro America - ME": {
      "lng": -68.704368,
      "lat": 45.647179
    },
    "Gulf Island": {
      "lng": -70.209378,
      "lat": 44.152854
    },
    "Hancock Wind Plant": {
      "lng": -68.146944,
      "lat": 44.760833
    },
    "Harris Hydro": {
      "lng": -69.8658,
      "lat": 45.4592
    },
    "Hartland Solar": {
      "lng": -69.45384,
      "lat": 44.85045
    },
    "Hiram": {
      "lng": -70.7969,
      "lat": 43.8525
    },
    "Hydro Kennebec Project": {
      "lng": -69.619162,
      "lat": 44.563031
    },
    "Indeck Jonesboro Energy Center": {
      "lng": -67.5477,
      "lat": 44.6781
    },
    "Indeck West Enfield Energy Center": {
      "lng": -68.62792,
      "lat": 45.2537
    },
    "International Paper Jay Hydro": {
      "lng": -70.220971,
      "lat": 44.503704
    },
    "International Paper Livermore Hydro": {
      "lng": -70.187333,
      "lat": 44.470511
    },
    "International Paper Riley Hydro": {
      "lng": -70.249137,
      "lat": 44.503431
    },
    "IOS - MEW Phase 1": {
      "lng": -69.838,
      "lat": 44.804
    },
    "Kibby Wind Facility": {
      "lng": -70.5258,
      "lat": 45.3853
    },
    "Lockwood Hydroelectric Facility": {
      "lng": -69.6292,
      "lat": 44.5467
    },
    "Madison BTM": {
      "lng": -69.841,
      "lat": 44.804
    },
    "Madison ESS": {
      "lng": -69.876,
      "lat": 44.793
    },
    "Maine Independence Station": {
      "lng": -68.7094,
      "lat": 44.8242
    },
    "Mars Hill Wind Farm Project": {
      "lng": -67.8111,
      "lat": 46.54422
    },
    "Mead Rumford Cogen": {
      "lng": -70.544286,
      "lat": 44.546503
    },
    "Mechanic Falls": {
      "lng": -70.390673,
      "lat": 44.111236
    },
    "Medway Hydro": {
      "lng": -68.5453,
      "lat": 45.6072
    },
    "Messalonskee 2 (Oakland)": {
      "lng": -69.710404,
      "lat": 44.548544
    },
    "Messalonskee 3": {
      "lng": -69.681841,
      "lat": 44.569872
    },
    "Messalonskee 5": {
      "lng": -69.652232,
      "lat": 44.534397
    },
    "Milford Hydro Station": {
      "lng": -68.64504,
      "lat": 44.942064
    },
    "Milo PV - BD Solar 1 LLC": {
      "lng": -68.991,
      "lat": 45.267
    },
    "MMWAC Resource Recovery Facility": {
      "lng": -70.260831,
      "lat": 44.067792
    },
    "Naples": {
      "lng": -70.576964,
      "lat": 43.970601
    },
    "North Gorham": {
      "lng": -70.44992,
      "lat": 43.802517
    },
    "Oakfield Wind Project": {
      "lng": -68.147222,
      "lat": 46.058056
    },
    "Orono B": {
      "lng": -68.663889,
      "lat": 44.884444
    },
    "Orono Hydro Station": {
      "lng": -68.6647,
      "lat": 44.8803
    },
    "Otis Hydro": {
      "lng": -70.200759,
      "lat": 44.477807
    },
    "Oxford PV - BD Solar Oxford LLC": {
      "lng": -70.476,
      "lat": 44.151
    },
    "Passadumkeag Windpark LLC": {
      "lng": -68.21,
      "lat": 45.07
    },
    "Pejepscot Hydroelectric Project": {
      "lng": -70.024158,
      "lat": 43.957448
    },
    "Penobscot Energy Recovery": {
      "lng": -68.8258,
      "lat": 44.7383
    },
    "Pine Tree Landfill Gas to Energy": {
      "lng": -68.861667,
      "lat": 44.768611
    },
    "Pisgah Mountain Wind": {
      "lng": -68.523419,
      "lat": 44.777337
    },
    "Pittsfield Hydro": {
      "lng": -69.411992,
      "lat": 44.721371
    },
    "Pittsfield Solar LLC": {
      "lng": -69.449561,
      "lat": 44.814009
    },
    "Pumpkin Hill": {
      "lng": -68.4649,
      "lat": 45.1868
    },
    "Record Hill Wind": {
      "lng": -70.627777,
      "lat": 44.633611
    },
    "Red Shield Envir Old Town Facility": {
      "lng": -68.63554,
      "lat": 44.918063
    },
    "ReEnergy Livermore Falls": {
      "lng": -70.1619,
      "lat": 44.431667
    },
    "ReEnergy Stratton LLC": {
      "lng": -70.425577,
      "lat": 45.140614
    },
    "Regional Waste Systems": {
      "lng": -70.3347,
      "lat": 43.6556
    },
    "Robbins Lumber": {
      "lng": -69.2019,
      "lat": 44.3331
    },
    "Rollins Wind Project": {
      "lng": -68.3802,
      "lat": 45.34598
    },
    "RoxWind": {
      "lng": -70.6176,
      "lat": 44.6148
    },
    "Rumford Cogeneration": {
      "lng": -70.5414,
      "lat": 44.5513
    },
    "Rumford ESS": {
      "lng": -70.518959,
      "lat": 44.532169
    },
    "Rumford Falls Hydro Facility": {
      "lng": -70.6283,
      "lat": 44.5486
    },
    "Rumford Power": {
      "lng": -70.5219,
      "lat": 44.5303
    },
    "S D Warren Westbrook": {
      "lng": -70.353152,
      "lat": 43.684857
    },
    "Saddleback Ridge Wind Farm": {
      "lng": -70.381111,
      "lat": 44.593333
    },
    "Salmon Falls": {
      "lng": -70.810888,
      "lat": 43.226977
    },
    "Sanford Solar": {
      "lng": -70.7803,
      "lat": 43.4569
    },
    "Shawmut": {
      "lng": -69.583527,
      "lat": 44.629517
    },
    "Skelton": {
      "lng": -70.5583,
      "lat": 43.5708
    },
    "Solar Mule, LLC": {
      "lng": -69.671364,
      "lat": 44.557725
    },
    "Somerset Plant": {
      "lng": -69.647441,
      "lat": 44.703467
    },
    "Spruce Mountain WInd": {
      "lng": -70.559814,
      "lat": 44.415587
    },
    "Squa Pan Hydro Station": {
      "lng": -68.325538,
      "lat": 46.556477
    },
    "Stetson Wind I": {
      "lng": -67.9944,
      "lat": 45.4822
    },
    "Stetson Wind II": {
      "lng": -67.971,
      "lat": 45.61498
    },
    "Stillwater B": {
      "lng": -68.683833,
      "lat": 44.912667
    },
    "Stillwater Hydro Station": {
      "lng": -68.6833,
      "lat": 44.9094
    },
    "Twin Rivers Paper Co LLC": {
      "lng": -68.330855,
      "lat": 47.358139
    },
    "Unity Solar": {
      "lng": -69.357,
      "lat": 44.598
    },
    "Upper Androscoggin": {
      "lng": -70.212368,
      "lat": 44.087963
    },
    "Upper Barker": {
      "lng": -70.229294,
      "lat": 44.08687
    },
    "Waste Management Crossroads LFGTE": {
      "lng": -69.8319,
      "lat": 44.7014
    },
    "Weaver Wind": {
      "lng": -68.163611,
      "lat": 44.722519
    },
    "West Buxton": {
      "lng": -70.602125,
      "lat": 43.66687
    },
    "West Enfield Hydro": {
      "lng": -68.6483,
      "lat": 45.2497
    },
    "Westbrook Energy Center": {
      "lng": -70.375,
      "lat": 43.6575
    },
    "Weston Hydro": {
      "lng": -69.71828,
      "lat": 44.763875
    },
    "William F Wyman": {
      "lng": -70.1567,
      "lat": 43.7508
    },
    "Williams Hydro": {
      "lng": -69.8703,
      "lat": 44.9589
    },
    "Woodland Pulp, LLC": {
      "lng": -67.4012,
      "lat": 45.1554
    },
    "Worumbo Hydro Station": {
      "lng": -70.061921,
      "lat": 43.994729
    },
    "Wyman Hydro": {
      "lng": -69.9064,
      "lat": 45.0703
    },
    "13 Mile Solar, LLC": {
      "lng": -85.048584,
      "lat": 42.326656
    },
    "48th Street Peaking Station": {
      "lng": -86.0853,
      "lat": 42.7553
    },
    "A.J. Mihm Generating Station": {
      "lng": -88.614009,
      "lat": 46.792669
    },
    "Ada Cogeneration LP": {
      "lng": -85.494071,
      "lat": 42.962672
    },
    "Ada Dam": {
      "lng": -85.485944,
      "lat": 42.950642
    },
    "Adrian Energy Associates LLC": {
      "lng": -83.99286,
      "lat": 41.890968
    },
    "Alcona": {
      "lng": -83.804505,
      "lat": 44.561961
    },
    "Allegan Dam": {
      "lng": -85.953976,
      "lat": 42.563803
    },
    "Alpena Cement Plant": {
      "lng": -83.408182,
      "lat": 45.069929
    },
    "Alpine Power Plant": {
      "lng": -84.825348,
      "lat": 45.063748
    },
    "Angola Solar, LLC": {
      "lng": -85.007397,
      "lat": 41.834826
    },
    "Apple Blossom Wind Farm": {
      "lng": -83.315002,
      "lat": 43.815032
    },
    "Assembly Solar II LLC": {
      "lng": -83.95998,
      "lat": 43.043718
    },
    "Assembly Solar Project": {
      "lng": -83.94527,
      "lat": 43.02268
    },
    "Autumn Hills Generating Facility": {
      "lng": -85.92195,
      "lat": 42.783
    },
    "Battle Creek Mill": {
      "lng": -85.2043,
      "lat": 42.3221
    },
    "Bay Windpower I": {
      "lng": -84.7381,
      "lat": 45.7644
    },
    "Beaumont Hospital Dearborn Campus": {
      "lng": -83.2119,
      "lat": 42.2917
    },
    "Beaver Island": {
      "lng": -85.5244,
      "lat": 45.7291
    },
    "Beebe 1A": {
      "lng": -84.499722,
      "lat": 43.256944
    },
    "Beebe 1B": {
      "lng": -84.499722,
      "lat": 43.256944
    },
    "Belle River": {
      "lng": -82.495,
      "lat": 42.7756
    },
    "Berrien Springs": {
      "lng": -86.3289,
      "lat": 41.9439
    },
    "Big Quinnesec 61": {
      "lng": -88.0415,
      "lat": 45.7883
    },
    "Big Quinnesec 92": {
      "lng": -88.0408,
      "lat": 45.7881
    },
    "Big Turtle Wind Farm 2": {
      "lng": -82.794428,
      "lat": 43.919558
    },
    "Big Turtle Wind Farm, LLC": {
      "lng": -82.7825,
      "lat": 43.850833
    },
    "Bingham Solar, LLC": {
      "lng": -84.528,
      "lat": 43.012
    },
    "Bissell Solar and Battery Generator": {
      "lng": -85.724,
      "lat": 43.007
    },
    "Blue Water Renewables Inc": {
      "lng": -82.594507,
      "lat": 42.91577
    },
    "Brent Run Generating Station": {
      "lng": -83.847974,
      "lat": 43.17642
    },
    "Brookfield": {
      "lng": -83.297778,
      "lat": 43.757778
    },
    "Bryon Center": {
      "lng": -85.6728,
      "lat": 42.7814
    },
    "Buchanan (MI)": {
      "lng": -86.35105,
      "lat": 41.839566
    },
    "Bullhead Solar, LLC": {
      "lng": -84.67383,
      "lat": 41.96019
    },
    "C W Tippy": {
      "lng": -85.939565,
      "lat": 44.259135
    },
    "Cadillac Renewable Energy": {
      "lng": -85.435224,
      "lat": 44.26169
    },
    "Captain Solar, LLC": {
      "lng": -83.781622,
      "lat": 43.100538
    },
    "Cargill Salt": {
      "lng": -82.4856,
      "lat": 42.8183
    },
    "Cargill Salt Hersey": {
      "lng": -85.355833,
      "lat": 43.834167
    },
    "Caro": {
      "lng": -83.3278,
      "lat": 43.4659
    },
    "Cataract (MI)": {
      "lng": -87.5128,
      "lat": 46.3156
    },
    "Central Generating": {
      "lng": -85.506011,
      "lat": 43.33349
    },
    "Central Michigan University": {
      "lng": -84.779,
      "lat": 43.590456
    },
    "Chalk Hill": {
      "lng": -87.8011,
      "lat": 45.5136
    },
    "Chatham": {
      "lng": -85.3475,
      "lat": 42.934444
    },
    "Cheboygan": {
      "lng": -84.481387,
      "lat": 45.637537
    },
    "Claude Vandyke": {
      "lng": -85.8551,
      "lat": 42.731
    },
    "Coldwater Peaking Plant": {
      "lng": -85.024167,
      "lat": 41.919444
    },
    "Coldwater Solar, LLC": {
      "lng": -83.777071,
      "lat": 43.093344
    },
    "Colfax": {
      "lng": -84.095181,
      "lat": 42.658653
    },
    "Constantine": {
      "lng": -85.6694,
      "lat": 41.8436
    },
    "Cooke": {
      "lng": -83.571947,
      "lat": 44.472624
    },
    "Coopersville": {
      "lng": -85.956173,
      "lat": 43.047306
    },
    "Crescent Wind Park": {
      "lng": -84.477,
      "lat": 41.935
    },
    "Cross Winds Energy Park": {
      "lng": -83.485556,
      "lat": 43.610278
    },
    "Croswell": {
      "lng": -82.613889,
      "lat": 43.271944
    },
    "Croton": {
      "lng": -85.664462,
      "lat": 43.437322
    },
    "Crystal Falls": {
      "lng": -88.334531,
      "lat": 46.106278
    },
    "Dafter": {
      "lng": -84.41723,
      "lat": 46.37437
    },
    "Dan E Karn": {
      "lng": -83.840074,
      "lat": 43.644996
    },
    "Dean Peakers": {
      "lng": -82.4953,
      "lat": 42.7725
    },
    "Dearborn Energy Center": {
      "lng": -83.23125,
      "lat": 42.2969
    },
    "Dearborn Industrial Generation": {
      "lng": -83.154,
      "lat": 42.3026
    },
    "Decorative Panels Intl": {
      "lng": -83.423333,
      "lat": 45.062836
    },
    "Deerfield Wind Energy, LLC": {
      "lng": -82.898675,
      "lat": 43.941851
    },
    "Delray": {
      "lng": -83.1019,
      "lat": 42.2947
    },
    "Delta Energy Park": {
      "lng": -84.657783,
      "lat": 42.690644
    },
    "Delta Solar Power I": {
      "lng": -84.701,
      "lat": 42.7
    },
    "Delta Solar Power II": {
      "lng": -84.7,
      "lat": 42.707
    },
    "Demille Solar Farm": {
      "lng": -83.333247,
      "lat": 43.040011
    },
    "Detour": {
      "lng": -83.90788,
      "lat": 45.98363
    },
    "DG AMP Solar Coldwater": {
      "lng": -84.997244,
      "lat": 41.934117
    },
    "Domino Farms Solar": {
      "lng": -83.680252,
      "lat": 42.323377
    },
    "Donald C Cook": {
      "lng": -86.565206,
      "lat": 41.975604
    },
    "E.B. Eddy Paper Inc": {
      "lng": -82.4379,
      "lat": 42.9896
    },
    "Eagle Valley (MI)": {
      "lng": -83.263055,
      "lat": 42.7225
    },
    "East. Michigan Univ. Heating Plant": {
      "lng": -83.628889,
      "lat": 42.248889
    },
    "Echo Wind Park": {
      "lng": -83.161389,
      "lat": 43.8825
    },
    "Eckert Station": {
      "lng": -84.55808,
      "lat": 42.71839
    },
    "Edison Sault": {
      "lng": -84.332,
      "lat": 46.4974
    },
    "Electric City Solar": {
      "lng": -85.45,
      "lat": 41.75
    },
    "Erickson": {
      "lng": -84.657222,
      "lat": 42.692222
    },
    "Escanaba Mill": {
      "lng": -87.0891,
      "lat": 45.8044
    },
    "F.D. Kuester Generating Station": {
      "lng": -87.510741,
      "lat": 46.512697
    },
    "Fermi": {
      "lng": -83.2581,
      "lat": 41.9631
    },
    "Five Channels": {
      "lng": -83.676274,
      "lat": 44.455518
    },
    "Foote": {
      "lng": -83.440722,
      "lat": 44.435381
    },
    "Ford World Headquarters": {
      "lng": -83.209489,
      "lat": 42.317437
    },
    "Four Mile Hydropower Project": {
      "lng": -83.5025,
      "lat": 45.0933
    },
    "Frank Jenkins": {
      "lng": -84.895555,
      "lat": 42.868611
    },
    "Fremont Community Digester, LLC": {
      "lng": -85.9761,
      "lat": 43.45965
    },
    "French Landing Dam": {
      "lng": -83.440692,
      "lat": 42.214264
    },
    "French Paper Hydro": {
      "lng": -86.2592,
      "lat": 41.8203
    },
    "Garden 1 Solar Park": {
      "lng": -86.534614,
      "lat": 45.796239
    },
    "Gaylord - Wolverine": {
      "lng": -84.7218,
      "lat": 45.0046
    },
    "Geddes 1 Solar, LLC": {
      "lng": -84.118662,
      "lat": 43.433775
    },
    "Geddes 2 Solar, LLC": {
      "lng": -84.119325,
      "lat": 43.428036
    },
    "Genesee Power Station": {
      "lng": -83.669386,
      "lat": 43.08511
    },
    "George Johnson": {
      "lng": -85.426,
      "lat": 43.8393
    },
    "Gladstone": {
      "lng": -87.0036,
      "lat": 45.8475
    },
    "Grand Blanc Generating Station": {
      "lng": -83.725085,
      "lat": 42.908815
    },
    "Grand Rapids": {
      "lng": -87.6494,
      "lat": 45.3547
    },
    "Grand River": {
      "lng": -84.695682,
      "lat": 42.791028
    },
    "Grand Valley Solar Gardens": {
      "lng": -85.90117,
      "lat": 42.9531
    },
    "Graphic Packaging International, Inc.": {
      "lng": -85.578969,
      "lat": 42.306809
    },
    "Gratiot County Wind LLC": {
      "lng": -84.462222,
      "lat": 43.408056
    },
    "Gratiot Farms Wind Project": {
      "lng": -84.8653,
      "lat": 43.1695
    },
    "Gratiot Wind Park": {
      "lng": -84.4975,
      "lat": 43.433889
    },
    "Grayling Generating Station": {
      "lng": -84.690578,
      "lat": 44.604921
    },
    "Green Lake Solar": {
      "lng": -85.692272,
      "lat": 44.77666
    },
    "Greenwood": {
      "lng": -82.6964,
      "lat": 43.1056
    },
    "Greenwood Solar Farm": {
      "lng": -82.6964,
      "lat": 43.1056
    },
    "GRS Arbor Hills": {
      "lng": -83.557999,
      "lat": 42.395335
    },
    "GRS C&C": {
      "lng": -85.005779,
      "lat": 42.356343
    },
    "Hancock Peakers": {
      "lng": -83.4425,
      "lat": 42.5497
    },
    "Hardy": {
      "lng": -85.629866,
      "lat": 43.486618
    },
    "Hart": {
      "lng": -86.366256,
      "lat": 43.704076
    },
    "Harvest": {
      "lng": -83.2064,
      "lat": 43.83
    },
    "Harvest 2": {
      "lng": -83.1669,
      "lat": 43.8556
    },
    "Hazel Solar, LLC": {
      "lng": -85.4535,
      "lat": 43.402157
    },
    "Hemlock Falls": {
      "lng": -88.225,
      "lat": 46.1311
    },
    "Hendershot Solar, LLC": {
      "lng": -83.888515,
      "lat": 41.982616
    },
    "Henry Station": {
      "lng": -83.89986,
      "lat": 43.595101
    },
    "Heritage Garden Wind Farm I LLC": {
      "lng": -86.533889,
      "lat": 45.795556
    },
    "Hillman Power LLC": {
      "lng": -83.8932,
      "lat": 45.0688
    },
    "Hillsdale": {
      "lng": -84.6319,
      "lat": 41.9061
    },
    "Hodenpyl": {
      "lng": -85.819968,
      "lat": 44.36286
    },
    "Hoist": {
      "lng": -87.5686,
      "lat": 46.5628
    },
    "Holland Energy Park": {
      "lng": -86.092222,
      "lat": 42.7925
    },
    "Hydro Plant": {
      "lng": -85.5381,
      "lat": 41.9711
    },
    "IKEA Canton Rooftop PV System": {
      "lng": -83.42888,
      "lat": 42.32345
    },
    "Interchange Solar, LLC": {
      "lng": -83.771004,
      "lat": 43.099899
    },
    "Isabella Wind Park": {
      "lng": -84.788455,
      "lat": 43.684092
    },
    "J H Campbell": {
      "lng": -86.20074,
      "lat": 42.910296
    },
    "Jack Francis Solar, LLC": {
      "lng": -83.679171,
      "lat": 43.149282
    },
    "Jackson Generating Station": {
      "lng": -84.3767,
      "lat": 42.2488
    },
    "James R Smith": {
      "lng": -87.410455,
      "lat": 46.571639
    },
    "John H Warden": {
      "lng": -88.4558,
      "lat": 46.7553
    },
    "Kalamazoo River Generating Station": {
      "lng": -85.494956,
      "lat": 42.281345
    },
    "Kalkaska CT Project #1": {
      "lng": -85.2019,
      "lat": 44.6889
    },
    "Kent County Waste to Energy Facility": {
      "lng": -85.693209,
      "lat": 42.949575
    },
    "Kingsford": {
      "lng": -88.125121,
      "lat": 45.807956
    },
    "Kleber": {
      "lng": -84.333257,
      "lat": 45.391984
    },
    "Lake Winds Energy Park": {
      "lng": -86.334722,
      "lat": 43.871944
    },
    "Lansing BWL REO Town Plant": {
      "lng": -84.551667,
      "lat": 42.719722
    },
    "Lennon Generating": {
      "lng": -83.979444,
      "lat": 42.981667
    },
    "Livingston Generating Station": {
      "lng": -84.730795,
      "lat": 45.030397
    },
    "Lorin Industries": {
      "lng": -86.214444,
      "lat": 43.216639
    },
    "Loud": {
      "lng": -83.7208,
      "lat": 44.4631
    },
    "Ludington": {
      "lng": -86.4447,
      "lat": 43.8942
    },
    "M72 West Solar": {
      "lng": -85.701356,
      "lat": 44.776787
    },
    "Main Street (MI)": {
      "lng": -83.453306,
      "lat": 43.732508
    },
    "Manistique": {
      "lng": -86.256291,
      "lat": 45.955014
    },
    "Marquette Energy Center": {
      "lng": -87.437245,
      "lat": 46.56597
    },
    "Marshall (MI)": {
      "lng": -84.9411,
      "lat": 42.2717
    },
    "May Shannon Solar, LLC": {
      "lng": -83.690891,
      "lat": 43.151133
    },
    "McClure Dam": {
      "lng": -87.4764,
      "lat": 46.5717
    },
    "McKinley Wind Park": {
      "lng": -83.241388,
      "lat": 43.871944
    },
    "Menominee Mill Marinette": {
      "lng": -87.6378,
      "lat": 45.1069
    },
    "Michigamme Falls": {
      "lng": -88.1958,
      "lat": 45.9553
    },
    "Michigan Power Limited Partnership": {
      "lng": -86.425,
      "lat": 43.9375
    },
    "Michigan Wind 1": {
      "lng": -82.9633,
      "lat": 43.7092
    },
    "Michigan Wind 2": {
      "lng": -82.746944,
      "lat": 43.663056
    },
    "Midland Cogeneration Venture": {
      "lng": -84.2242,
      "lat": 43.5861
    },
    "Minden Wind Park": {
      "lng": -82.758611,
      "lat": 43.67
    },
    "Mio": {
      "lng": -84.131691,
      "lat": 44.661114
    },
    "Monroe": {
      "lng": -83.3464,
      "lat": 41.8906
    },
    "Mottville": {
      "lng": -85.7505,
      "lat": 41.8056
    },
    "MSC Croswell": {
      "lng": -82.619167,
      "lat": 43.266389
    },
    "MSC Sebewaing": {
      "lng": -83.447222,
      "lat": 43.741111
    },
    "Municipal Power Plant": {
      "lng": -86.0335,
      "lat": 42.8129
    },
    "Neenah Paper Munising Mill": {
      "lng": -86.6443,
      "lat": 46.4094
    },
    "New Covert Generating Project": {
      "lng": -86.29368,
      "lat": 42.32238
    },
    "Newberry": {
      "lng": -85.506408,
      "lat": 46.351975
    },
    "Ninth Street Hydropower Project": {
      "lng": -83.4381,
      "lat": 45.0717
    },
    "Northeast (MI)": {
      "lng": -83.0381,
      "lat": 42.45
    },
    "Norway (MI)": {
      "lng": -87.86278,
      "lat": 45.74123
    },
    "Norway Point Hydropower Project": {
      "lng": -83.5189,
      "lat": 45.1025
    },
    "Oliver": {
      "lng": -83.2383,
      "lat": 43.8264
    },
    "O'Shea Solar Farm": {
      "lng": -83.200064,
      "lat": 42.378001
    },
    "Otsego Paper, Inc.": {
      "lng": -85.6961,
      "lat": 42.4628
    },
    "Palisades": {
      "lng": -86.3146,
      "lat": 42.323
    },
    "Parkview Battery": {
      "lng": -85.649722,
      "lat": 42.26
    },
    "PCA, Filer City Mill": {
      "lng": -86.286667,
      "lat": 44.213333
    },
    "Peavy Falls": {
      "lng": -88.210514,
      "lat": 45.990781
    },
    "Pegasus Wind": {
      "lng": -83.50721,
      "lat": 43.452003
    },
    "Peoples Generating Station": {
      "lng": -83.865066,
      "lat": 43.27975
    },
    "Pheasant Run Wind LLC": {
      "lng": -83.297778,
      "lat": 43.757778
    },
    "Pickford Solar": {
      "lng": -84.345452,
      "lat": 46.189254
    },
    "Pinconning White Feather": {
      "lng": -83.9494,
      "lat": 43.8983
    },
    "Pine River Wind Park": {
      "lng": -84.696781,
      "lat": 43.524324
    },
    "Pine Street": {
      "lng": -83.449953,
      "lat": 43.740164
    },
    "Pine Tree Acres": {
      "lng": -82.747087,
      "lat": 42.764456
    },
    "Pine Tree Acres WM LFGTE": {
      "lng": -82.745555,
      "lat": 42.765
    },
    "Placid 12": {
      "lng": -83.4569,
      "lat": 42.7106
    },
    "Plant Four": {
      "lng": -87.403579,
      "lat": 46.575965
    },
    "POET Biorefining Caro, LLC": {
      "lng": -83.411763,
      "lat": 43.472885
    },
    "Polaris Wind Park": {
      "lng": -84.499838,
      "lat": 43.262611
    },
    "Portage": {
      "lng": -88.6472,
      "lat": 47.0808
    },
    "Prickett": {
      "lng": -88.6625,
      "lat": 46.7261
    },
    "Putnam (MI)": {
      "lng": -83.4561,
      "lat": 42.7108
    },
    "Renaissance Power": {
      "lng": -84.8429,
      "lat": 43.1864
    },
    "Riley Generating Station": {
      "lng": -86.0053,
      "lat": 42.8333
    },
    "River Rouge": {
      "lng": -83.1119,
      "lat": 42.2739
    },
    "Riverview Energy Systems": {
      "lng": -83.212903,
      "lat": 42.166106
    },
    "Rogers": {
      "lng": -85.479469,
      "lat": 43.613001
    },
    "Saint Marys Falls": {
      "lng": -84.349267,
      "lat": 46.50625
    },
    "Sigel Wind Park": {
      "lng": -82.783611,
      "lat": 43.850833
    },
    "Sixth Street Gas Turbine": {
      "lng": -86.103583,
      "lat": 42.792645
    },
    "Slocum": {
      "lng": -83.185,
      "lat": 42.1225
    },
    "Southeast Berrien Generating Facility": {
      "lng": -86.323889,
      "lat": 41.808611
    },
    "Spartan PV 1, LLC": {
      "lng": -84.4824,
      "lat": 42.7151
    },
    "St Louis": {
      "lng": -84.6014,
      "lat": 43.4019
    },
    "St. Clair": {
      "lng": -82.4719,
      "lat": 42.7642
    },
    "Stoneheart Solar, LLC": {
      "lng": -83.996589,
      "lat": 43.50677
    },
    "Stoney Corners Wind Farm": {
      "lng": -85.2983,
      "lat": 44.19
    },
    "Sturgis City Diesel Plant": {
      "lng": -85.4256,
      "lat": 41.7992
    },
    "Sumpter Energy Associates": {
      "lng": -83.498845,
      "lat": 42.105669
    },
    "Sumpter Plant": {
      "lng": -83.5318,
      "lat": 42.167
    },
    "Superior": {
      "lng": -83.6422,
      "lat": 42.2639
    },
    "Superior Falls": {
      "lng": -90.416147,
      "lat": 46.564601
    },
    "T B Simon Power Plant": {
      "lng": -84.4836,
      "lat": 42.7178
    },
    "TCLP Solar Phase 1": {
      "lng": -85.692272,
      "lat": 44.77666
    },
    "Temperance Solar, LLC": {
      "lng": -83.524,
      "lat": 41.753
    },
    "TES Filer City Station": {
      "lng": -86.28905,
      "lat": 44.217299
    },
    "The Andersons Albion Ethanol LLC": {
      "lng": -84.788333,
      "lat": 42.256111
    },
    "Tower": {
      "lng": -84.29497,
      "lat": 45.36282
    },
    "Trenton Channel": {
      "lng": -83.1808,
      "lat": 42.1217
    },
    "Turrill Solar Farm": {
      "lng": -83.308886,
      "lat": 43.034739
    },
    "Tuscola Bay Wind": {
      "lng": -83.65,
      "lat": 43.525278
    },
    "Tuscola Wind II LLC": {
      "lng": -83.566944,
      "lat": 43.526944
    },
    "Ubly": {
      "lng": -82.9388,
      "lat": 43.7217
    },
    "University of Michigan": {
      "lng": -83.734617,
      "lat": 42.281167
    },
    "Venice Resources Gas Recovery": {
      "lng": -83.9764,
      "lat": 42.985
    },
    "Verso Paper Quinnesec Mich Mill": {
      "lng": -87.9556,
      "lat": 45.7956
    },
    "Vestaburg": {
      "lng": -84.9141,
      "lat": 43.4016
    },
    "Victoria Dam": {
      "lng": -89.2089,
      "lat": 46.6964
    },
    "Viking Energy of Lincoln": {
      "lng": -83.4167,
      "lat": 44.68
    },
    "Viking Energy of McBain": {
      "lng": -85.2206,
      "lat": 44.204
    },
    "Warner Lambert": {
      "lng": -83.705318,
      "lat": 42.299332
    },
    "Waste Management Northern Oaks LFGTE": {
      "lng": -84.8003,
      "lat": 43.9906
    },
    "Water Street Station": {
      "lng": -83.896696,
      "lat": 43.582846
    },
    "Watervliet": {
      "lng": -86.28,
      "lat": 42.175833
    },
    "Watervliet PV": {
      "lng": -86.23,
      "lat": 42.19
    },
    "Way Dam": {
      "lng": -88.2353,
      "lat": 46.1592
    },
    "Webber": {
      "lng": -84.9025,
      "lat": 42.9528
    },
    "Western Michigan Solar Gardens": {
      "lng": -85.64273,
      "lat": 42.2525
    },
    "Western Michigan University Power Plant": {
      "lng": -85.606389,
      "lat": 42.28
    },
    "White Rapids": {
      "lng": -87.802178,
      "lat": 45.482084
    },
    "William Beaumont Hospital": {
      "lng": -83.1919,
      "lat": 42.5175
    },
    "Wilmot": {
      "lng": -83.1889,
      "lat": 43.4566
    },
    "Wood Road": {
      "lng": -84.52367,
      "lat": 42.772888
    },
    "Workman Road Solar Farm, LLC": {
      "lng": -85.212021,
      "lat": 44.273592
    },
    "Wyandotte": {
      "lng": -83.1453,
      "lat": 42.2081
    },
    "Zeeland": {
      "lng": -86.0558,
      "lat": 42.8067
    },
    "Zeeland Farm Services": {
      "lng": -85.986111,
      "lat": 42.813056
    },
    "Zeeland Generating Station": {
      "lng": -85.9975,
      "lat": 42.8206
    },
    "4710 Hosting": {
      "lng": -92.522265,
      "lat": 44.070814
    },
    "Acturus Community Solar": {
      "lng": -93.954417,
      "lat": 44.283211
    },
    "Adams Wind Farm": {
      "lng": -92.724139,
      "lat": 43.554778
    },
    "Adams Wind Generations LLC": {
      "lng": -94.735555,
      "lat": 44.916944
    },
    "Adrian": {
      "lng": -95.932794,
      "lat": 43.63711
    },
    "Agassiz Beach LLC": {
      "lng": -96.4356,
      "lat": 47.0053
    },
    "Albany Solar": {
      "lng": -94.599251,
      "lat": 45.641268
    },
    "Alexandria": {
      "lng": -95.379722,
      "lat": 45.889167
    },
    "Allen S King": {
      "lng": -92.7786,
      "lat": 45.03
    },
    "Allium Community Solar Garden, LLC": {
      "lng": -94.723142,
      "lat": 45.399701
    },
    "Altair Community Solar Garden": {
      "lng": -93.485831,
      "lat": 43.924981
    },
    "American Crystal Sugar Crookston": {
      "lng": -96.633375,
      "lat": 47.76468
    },
    "American Crystal Sugar East Grand Forks": {
      "lng": -97.007153,
      "lat": 47.927202
    },
    "American Crystal Sugar Moorhead": {
      "lng": -96.76112,
      "lat": 46.9026
    },
    "Andromeda Community Solar": {
      "lng": -94.031626,
      "lat": 44.901926
    },
    "Annandale Solar": {
      "lng": -94.106932,
      "lat": 45.247009
    },
    "Anoka BESS": {
      "lng": -93.48,
      "lat": 45.32
    },
    "Anoka County MN CONX": {
      "lng": -93.482957,
      "lat": 45.234722
    },
    "Antares Community Solar": {
      "lng": -95.623804,
      "lat": 44.226179
    },
    "Antlia Community Solar": {
      "lng": -96.138235,
      "lat": 43.906524
    },
    "Aquarius Community Solar": {
      "lng": -95.620106,
      "lat": 44.229142
    },
    "Aquila Community Solar": {
      "lng": -95.3401,
      "lat": 45.651056
    },
    "Archer Daniels Midland Mankato": {
      "lng": -93.99242,
      "lat": 44.187066
    },
    "Argo Navis Community Solar": {
      "lng": -92.772284,
      "lat": 45.255245
    },
    "Aries Community Solar": {
      "lng": -92.988569,
      "lat": 44.044924
    },
    "Armstrong Solar": {
      "lng": -95.35635,
      "lat": 45.642252
    },
    "Aster Community Solar Garden, LLC": {
      "lng": -95.229451,
      "lat": 45.008221
    },
    "Athens - Coopers Corner BESS": {
      "lng": -93.26763,
      "lat": 45.458119
    },
    "Athens BESS": {
      "lng": -93.24,
      "lat": 45.45
    },
    "Athens MN CONX": {
      "lng": -93.265706,
      "lat": 45.459021
    },
    "Atwater Solar": {
      "lng": -94.769733,
      "lat": 45.140869
    },
    "Auriga Community Solar": {
      "lng": -95.767356,
      "lat": 43.977875
    },
    "Autumn Hills LLC": {
      "lng": -96.4325,
      "lat": 44.4131
    },
    "B&K Energy Systems LLC": {
      "lng": -95.3755,
      "lat": 43.651389
    },
    "B.R. Corcoran CSG": {
      "lng": -93.6432,
      "lat": 45.0695
    },
    "B.R. Sartell CSG": {
      "lng": -94.2254,
      "lat": 45.665
    },
    "B.R. Sauk Rapids CSG": {
      "lng": -94.0782,
      "lat": 45.6236
    },
    "Barone CSG": {
      "lng": -94.073899,
      "lat": 44.944513
    },
    "Bauer Solar CSG": {
      "lng": -94.231949,
      "lat": 45.273478
    },
    "Becker Solar 2 CSG": {
      "lng": -93.684085,
      "lat": 45.174612
    },
    "Becker Solar 3 CSG": {
      "lng": -93.683653,
      "lat": 45.174942
    },
    "Becker Solar 4 CSG": {
      "lng": -93.679726,
      "lat": 45.175662
    },
    "Becker Solar 5 CSG": {
      "lng": -93.679354,
      "lat": 45.175654
    },
    "Becker Solar CSG": {
      "lng": -93.683884,
      "lat": 45.175532
    },
    "Bellflower Solar, LLC": {
      "lng": -95.12733,
      "lat": 45.507295
    },
    "Benson City of": {
      "lng": -95.602777,
      "lat": 45.314444
    },
    "Bent Tree Wind Farm Phase 1": {
      "lng": -93.450429,
      "lat": 43.724596
    },
    "BentonSun LLC": {
      "lng": -93.857944,
      "lat": 44.766167
    },
    "Betcher Community Solar Garden": {
      "lng": -92.595952,
      "lat": 44.392415
    },
    "Betcher CSG 1, LLC": {
      "lng": -92.595,
      "lat": 44.392
    },
    "Big Blue": {
      "lng": -94.191944,
      "lat": 43.612222
    },
    "Big Lake Holdco Solar CSG": {
      "lng": -93.775577,
      "lat": 45.334575
    },
    "Big Lake Project CSG": {
      "lng": -93.792,
      "lat": 45.334
    },
    "Bisson Windfarm LLC": {
      "lng": -96.219444,
      "lat": 43.984444
    },
    "Black Dog": {
      "lng": -93.2501,
      "lat": 44.8108
    },
    "Black Oak Wind Farm": {
      "lng": -95.012778,
      "lat": 45.660833
    },
    "Blanchard": {
      "lng": -94.3592,
      "lat": 45.8614
    },
    "Blazing Star 2 Wind Farm": {
      "lng": -96.35276,
      "lat": 44.445293
    },
    "Blazing Star Wind Farm 1": {
      "lng": -96.36824,
      "lat": 44.544137
    },
    "Blooming Prairie": {
      "lng": -93.0504,
      "lat": 43.866
    },
    "Blue Breezes II LLC": {
      "lng": -94.1564,
      "lat": 43.6558
    },
    "Blue Breezes LLC": {
      "lng": -94.1536,
      "lat": 43.6558
    },
    "Blue Earth": {
      "lng": -94.101667,
      "lat": 43.6375
    },
    "Blue Lake Generating Plant": {
      "lng": -93.4315,
      "lat": 44.7855
    },
    "Blue Lake Solar": {
      "lng": -93.429593,
      "lat": 44.79642
    },
    "Boeve Windfarm LLC": {
      "lng": -96.0933,
      "lat": 44.05
    },
    "Boise Cascade International Falls": {
      "lng": -93.4066,
      "lat": 48.6067
    },
    "Bolduan CSG": {
      "lng": -93.489353,
      "lat": 43.906161
    },
    "Boswell Energy Center": {
      "lng": -93.6528,
      "lat": 47.2611
    },
    "Brainerd Public Utilities": {
      "lng": -94.1817,
      "lat": 46.3758
    },
    "Brenda Luhman - CSG": {
      "lng": -92.72945,
      "lat": 44.291838
    },
    "Brooten CSG1, LLC": {
      "lng": -95.162232,
      "lat": 45.498798
    },
    "Buffalo Garden": {
      "lng": -93.49681,
      "lat": 44.00036
    },
    "Buffalo Lake CSG1, LLC": {
      "lng": -94.623028,
      "lat": 44.732611
    },
    "Byllesby": {
      "lng": -92.9394,
      "lat": 44.5119
    },
    "Caelum Community Solar": {
      "lng": -93.936782,
      "lat": 45.662006
    },
    "Cambridge Station": {
      "lng": -93.2081,
      "lat": 45.601
    },
    "Camellia Solar, LLC": {
      "lng": -95.353265,
      "lat": 45.642745
    },
    "Camp Ripley Solar": {
      "lng": -94.34,
      "lat": 46.091
    },
    "Cannon Falls Energy Center": {
      "lng": -92.915528,
      "lat": 44.536688
    },
    "Cannon Garden Solar": {
      "lng": -93.26916,
      "lat": 44.24416
    },
    "Canopus Community Solar": {
      "lng": -94.823247,
      "lat": 45.428308
    },
    "Capella Community Solar": {
      "lng": -96.161204,
      "lat": 44.0901
    },
    "Capricornus Community Solar Garden": {
      "lng": -94.717433,
      "lat": 45.398742
    },
    "Carina Community Solar": {
      "lng": -92.241187,
      "lat": 44.164677
    },
    "Carleton College": {
      "lng": -93.156387,
      "lat": 44.461908
    },
    "Carlson Community (CSG)": {
      "lng": -92.815184,
      "lat": 45.483155
    },
    "Carstensen Wind": {
      "lng": -96.4369,
      "lat": 44.0247
    },
    "Carver Gladden CSG": {
      "lng": -93.8717,
      "lat": 44.8071
    },
    "Cascade Creek": {
      "lng": -92.4908,
      "lat": 44.0322
    },
    "Cassiopeia Community Solar": {
      "lng": -95.328661,
      "lat": 45.647342
    },
    "Centaurus Community Solar": {
      "lng": -95.511257,
      "lat": 44.949053
    },
    "Centennial Generating Station": {
      "lng": -96.7464,
      "lat": 46.8661
    },
    "CF Novel Solar CSG Gardens Eleven, LLC": {
      "lng": -93.96,
      "lat": 44.24
    },
    "CF Novel Solar CSG Gardens Five, LLC": {
      "lng": -93.401684,
      "lat": 44.093763
    },
    "CF Novel Solar CSG Gardens Seven, LLC": {
      "lng": -94.02188,
      "lat": 45.430807
    },
    "CF Novel Solar CSG Gardens Two, LLC": {
      "lng": -93.83675,
      "lat": 43.845905
    },
    "CG Windfarm LLC": {
      "lng": -96.2186,
      "lat": 43.9931
    },
    "CGSun, LLC Community Solar": {
      "lng": -92.874985,
      "lat": 44.847916
    },
    "Champepaden Wind Power": {
      "lng": -95.950556,
      "lat": 43.899722
    },
    "Chanarambie Power Partners, LLC": {
      "lng": -96.0446,
      "lat": 44.011
    },
    "Chisago Community Solar": {
      "lng": -92.682957,
      "lat": 45.413357
    },
    "Chisago Holdco LLC, CSG": {
      "lng": -92.801239,
      "lat": 45.386802
    },
    "Chisago Solar": {
      "lng": -92.901952,
      "lat": 45.452479
    },
    "Chub Garden Solar": {
      "lng": -93.21279,
      "lat": 44.48595
    },
    "Cisco": {
      "lng": -95.3725,
      "lat": 43.6256
    },
    "Clara City Solar": {
      "lng": -95.350237,
      "lat": 44.94664
    },
    "Clear Garden CSG": {
      "lng": -93.494894,
      "lat": 44.035361
    },
    "Cokato CSG": {
      "lng": -94.215608,
      "lat": 45.087225
    },
    "Community Wind North LLC": {
      "lng": -96.406389,
      "lat": 44.245556
    },
    "Community Wind South": {
      "lng": -95.802778,
      "lat": 43.713611
    },
    "Connexus Solar Baldwin 1BDN": {
      "lng": -93.593675,
      "lat": 45.546415
    },
    "Connexus Solar Stanford 1STF": {
      "lng": -93.38854,
      "lat": 45.41715
    },
    "Coral Bells Solar, LLC": {
      "lng": -92.891,
      "lat": 45.471
    },
    "Corcoran CSG": {
      "lng": -93.645011,
      "lat": 45.095757
    },
    "Cornillie": {
      "lng": -92.89609,
      "lat": 45.46013
    },
    "Cornillie 2 Community (CSG)": {
      "lng": -92.89476,
      "lat": 45.45942
    },
    "Cornillie Solar CSG": {
      "lng": -92.89781,
      "lat": 45.459283
    },
    "Corvus Community Solar": {
      "lng": -93.944002,
      "lat": 44.191352
    },
    "Cottage Grove Cogeneration": {
      "lng": -92.9119,
      "lat": 44.7956
    },
    "Cottage Grove CSG, LLC": {
      "lng": -92.873655,
      "lat": 44.842124
    },
    "Cottage Grove Project CSG": {
      "lng": -92.8727,
      "lat": 44.7907
    },
    "Covanta Hennepin Energy": {
      "lng": -93.280382,
      "lat": 44.9833
    },
    "Crater Community Solar": {
      "lng": -95.508331,
      "lat": 44.798733
    },
    "Crux Community Solar": {
      "lng": -92.69318,
      "lat": 44.04006
    },
    "Dakota Community Solar One LLC CSG": {
      "lng": -93.10852,
      "lat": 44.65779
    },
    "Danielson Wind Farms LLC": {
      "lng": -94.752222,
      "lat": 45.071667
    },
    "Delano": {
      "lng": -93.790522,
      "lat": 45.041378
    },
    "Delphinus Community Solar": {
      "lng": -94.088758,
      "lat": 45.618565
    },
    "Deneb Community Solar": {
      "lng": -94.831075,
      "lat": 45.404139
    },
    "Detroit Lakes": {
      "lng": -95.843333,
      "lat": 46.8175
    },
    "Dickinson Solar Array": {
      "lng": -93.813799,
      "lat": 45.11444
    },
    "DL Windy Acres LLC": {
      "lng": -95.368611,
      "lat": 43.651389
    },
    "Dodge Center Solar": {
      "lng": -92.881554,
      "lat": 44.026007
    },
    "Dodge Holdco Solar CSG": {
      "lng": -92.756972,
      "lat": 44.022374
    },
    "DodgeSun CSG": {
      "lng": -92.69,
      "lat": 44.040418
    },
    "Douglas Todd Community Solar One LLC CSG": {
      "lng": -95.15002,
      "lat": 45.843344
    },
    "Dundas Solar Holdings LLC CSG": {
      "lng": -93.197327,
      "lat": 44.439647
    },
    "E. Goenner Project CSG": {
      "lng": -94.091014,
      "lat": 45.463956
    },
    "Eastridge Wind Project": {
      "lng": -95.9222,
      "lat": 43.9358
    },
    "Eastwood Solar": {
      "lng": -93.916561,
      "lat": 44.154526
    },
    "Eichtens Community Solar": {
      "lng": -92.772538,
      "lat": 45.382662
    },
    "Eichtens II CSG": {
      "lng": -92.782627,
      "lat": 45.396581
    },
    "EKS Landfill": {
      "lng": -93.3094,
      "lat": 44.7806
    },
    "Elk River": {
      "lng": -93.558447,
      "lat": 45.296797
    },
    "Elk River City of": {
      "lng": -93.5781,
      "lat": 45.3158
    },
    "Elm Creek Wind II LLC": {
      "lng": -94.8799,
      "lat": 43.6848
    },
    "Elm Creek Wind LLC": {
      "lng": -94.9497,
      "lat": 43.7542
    },
    "Empire Solar": {
      "lng": -93.108809,
      "lat": 44.66438
    },
    "Enter Solar": {
      "lng": -93.7077,
      "lat": 44.141576
    },
    "Eolos Wind Energy Research Field Station": {
      "lng": -93.048056,
      "lat": 44.728333
    },
    "Equuleus Community Solar Gardens": {
      "lng": -93.022687,
      "lat": 44.726098
    },
    "Erin Garden Solar": {
      "lng": -94.01007,
      "lat": 44.67524
    },
    "Ewington Energy Systems, LLC": {
      "lng": -95.4172,
      "lat": 43.6181
    },
    "Fairfax": {
      "lng": -94.718812,
      "lat": 44.525887
    },
    "Fairmont (MN)": {
      "lng": -94.4647,
      "lat": 43.6579
    },
    "Fairmont Wind": {
      "lng": -94.446667,
      "lat": 43.575
    },
    "Falls Creek Garden": {
      "lng": -93.14274,
      "lat": 44.1757
    },
    "Faribault Energy Park": {
      "lng": -93.290556,
      "lat": 44.3356
    },
    "Farmington Holdco Solar": {
      "lng": -93.134002,
      "lat": 44.623117
    },
    "FastSun 1 CSG": {
      "lng": -94.306657,
      "lat": 45.072314
    },
    "FastSun 10 CSG": {
      "lng": -92.719482,
      "lat": 44.291517
    },
    "FastSun 11 CSG": {
      "lng": -92.736238,
      "lat": 44.300868
    },
    "FastSun 13 CSG": {
      "lng": -95.148043,
      "lat": 45.841655
    },
    "FastSun 14 CSG": {
      "lng": -93.865226,
      "lat": 44.178968
    },
    "FastSun 18 CSG": {
      "lng": -91.327513,
      "lat": 43.865428
    },
    "FastSun 19 CSG": {
      "lng": -93.110769,
      "lat": 44.655202
    },
    "FastSun 2 CSG": {
      "lng": -92.979196,
      "lat": 44.531686
    },
    "FastSun 21 CSG": {
      "lng": -94.114719,
      "lat": 45.498092
    },
    "FastSun 3 CSG": {
      "lng": -94.486485,
      "lat": 45.617723
    },
    "FastSun 4 CSG": {
      "lng": -92.959023,
      "lat": 45.190434
    },
    "FastSun 5 CSG": {
      "lng": -91.319564,
      "lat": 43.85946
    },
    "FastSun 7 CSG": {
      "lng": -94.494115,
      "lat": 44.71922
    },
    "FastSun 8 CSG": {
      "lng": -94.290757,
      "lat": 44.260517
    },
    "FastSun 9 CSG": {
      "lng": -92.695652,
      "lat": 44.295395
    },
    "FastSun12 CSG": {
      "lng": -93.456969,
      "lat": 44.220125
    },
    "Felton CSG PV1-5": {
      "lng": -92.973584,
      "lat": 44.541704
    },
    "Fenton Wind Farm": {
      "lng": -96.018333,
      "lat": 43.852778
    },
    "Fey Windfarm LLC": {
      "lng": -96.0736,
      "lat": 44.0364
    },
    "Flint Hills Resources Pine Bend, LLC": {
      "lng": -93.02119,
      "lat": 44.45483
    },
    "Flodquist Community Solar LLC CSG": {
      "lng": -92.857015,
      "lat": 45.489436
    },
    "Florence Hills LLC": {
      "lng": -96.225,
      "lat": 44.2297
    },
    "Fond Du Lac": {
      "lng": -92.293897,
      "lat": 46.666768
    },
    "Foreman's Hill Community Solar": {
      "lng": -92.636857,
      "lat": 44.553337
    },
    "Forest Lake Solar CSG": {
      "lng": -92.89397,
      "lat": 45.253488
    },
    "Fountain": {
      "lng": -92.12943,
      "lat": 43.74609
    },
    "Fox CSG, LLC": {
      "lng": -92.681317,
      "lat": 45.392643
    },
    "Franklin Heating Station": {
      "lng": -92.465599,
      "lat": 44.020576
    },
    "Freeborn Wind Farm": {
      "lng": -93.188617,
      "lat": 43.513056
    },
    "Frontenac Holdco LLC, CSG": {
      "lng": -92.358711,
      "lat": 44.501673
    },
    "G Flow Wind LLC": {
      "lng": -95.776111,
      "lat": 43.586111
    },
    "G McNeilus Wind Farm Dodge Center": {
      "lng": -92.851078,
      "lat": 43.978486
    },
    "Gemini Community Solar": {
      "lng": -92.804361,
      "lat": 45.300294
    },
    "Geranium Solar, LLC": {
      "lng": -95.229505,
      "lat": 44.964569
    },
    "Gibbon Solar": {
      "lng": -94.524364,
      "lat": 44.520923
    },
    "GL Wind": {
      "lng": -91.877777,
      "lat": 43.991667
    },
    "Glencoe": {
      "lng": -94.1582,
      "lat": 44.7709
    },
    "Gohman Community Solar (CSG)": {
      "lng": -94.044799,
      "lat": 45.435683
    },
    "Goodhue Community Solar One LLC CSG": {
      "lng": -92.64087,
      "lat": 44.29741
    },
    "Goodhue Community Solar Three LLC CSG": {
      "lng": -92.74543,
      "lat": 44.29725
    },
    "Goodhue Community Solar Two LLC": {
      "lng": -92.723598,
      "lat": 44.293907
    },
    "Goose Lake MN DPC-GM": {
      "lng": -93.294017,
      "lat": 43.667563
    },
    "Gopher CSG": {
      "lng": -92.679157,
      "lat": 45.398562
    },
    "Grand Marais": {
      "lng": -90.357222,
      "lat": 47.750278
    },
    "Grand Meadow": {
      "lng": -92.705,
      "lat": 43.7189
    },
    "Granite Falls": {
      "lng": -95.53455,
      "lat": 44.811685
    },
    "Granite Falls 2": {
      "lng": -95.557,
      "lat": 44.8047
    },
    "Grant County Wind LLC": {
      "lng": -95.8844,
      "lat": 45.8033
    },
    "GRE Marshan Solar": {
      "lng": -92.8169,
      "lat": 44.6922
    },
    "Green Acres Breeze LLC": {
      "lng": -95.780278,
      "lat": 43.586111
    },
    "Greenback Energy": {
      "lng": -96.4331,
      "lat": 44.0208
    },
    "Grimm Community Solar": {
      "lng": -94.609342,
      "lat": 44.731404
    },
    "GSPP Held LLC CSG": {
      "lng": -94.284745,
      "lat": 45.52677
    },
    "GSPP Imholte CSG": {
      "lng": -94.16324,
      "lat": 45.557945
    },
    "GSSP Schneider LLC": {
      "lng": -94.297452,
      "lat": 45.529535
    },
    "Guse CSG": {
      "lng": -93.707763,
      "lat": 44.092021
    },
    "Hadley Ridge LLC": {
      "lng": -96.235,
      "lat": 44.2253
    },
    "Halstad": {
      "lng": -96.831911,
      "lat": 47.350064
    },
    "Hammer Community (CSG)": {
      "lng": -94.005708,
      "lat": 45.414071
    },
    "Hammer CSG": {
      "lng": -94.0094,
      "lat": 45.4176
    },
    "Hampton MN GRE": {
      "lng": -93.020502,
      "lat": 44.548799
    },
    "Hartmann Project CSG": {
      "lng": -94.113443,
      "lat": 45.493595
    },
    "Hastings City Hydroelectric": {
      "lng": -92.867743,
      "lat": 44.760249
    },
    "Hastings Solar": {
      "lng": -92.824883,
      "lat": 44.759275
    },
    "Haven Solar Project CSG": {
      "lng": -94.122,
      "lat": 45.518
    },
    "Hayfield Solar I LLC CSG": {
      "lng": -92.867187,
      "lat": 43.894113
    },
    "Hayfield Solar III LLC CSG": {
      "lng": -92.861257,
      "lat": 43.919214
    },
    "Hazel": {
      "lng": -91.94974,
      "lat": 43.78177
    },
    "Held Solar Project": {
      "lng": -94.2736,
      "lat": 45.5239
    },
    "Helen CSG 1 LLC": {
      "lng": -94.05075,
      "lat": 44.7893
    },
    "Helen Solar CSG": {
      "lng": -94.060012,
      "lat": 44.789369
    },
    "Hennepin Island": {
      "lng": -93.254163,
      "lat": 44.982359
    },
    "Hertzberg Community (CSG)": {
      "lng": -94.702055,
      "lat": 45.410876
    },
    "Heyer CSG": {
      "lng": -93.421725,
      "lat": 44.227172
    },
    "Hibbard Energy Center": {
      "lng": -92.151711,
      "lat": 46.735331
    },
    "Hibbing": {
      "lng": -92.935,
      "lat": 47.4294
    },
    "High Bridge": {
      "lng": -93.1117,
      "lat": 44.9314
    },
    "Hilltop Power": {
      "lng": -96.193221,
      "lat": 43.912882
    },
    "Hinterland CSG, LLC": {
      "lng": -94.01,
      "lat": 45.416
    },
    "Hometown Bio Energy LLC": {
      "lng": -93.906389,
      "lat": 44.435556
    },
    "Honeysuckle Solar, LLC": {
      "lng": -94.027151,
      "lat": 44.907623
    },
    "Hoot Lake": {
      "lng": -96.043252,
      "lat": 46.290641
    },
    "Hope Creek LLC": {
      "lng": -96.2192,
      "lat": 44.2275
    },
    "Houston/Winona Community Solar One LLC": {
      "lng": -91.322351,
      "lat": 43.866327
    },
    "Huneke CSG 1, LLC": {
      "lng": -92.736,
      "lat": 44.297
    },
    "Huneke CSG 2, LLC": {
      "lng": -92.669,
      "lat": 44.263
    },
    "Huneke I CSG": {
      "lng": -92.669448,
      "lat": 44.726789
    },
    "Huneke II CSG": {
      "lng": -92.741545,
      "lat": 44.297154
    },
    "Hutchinson - Plant 2": {
      "lng": -94.349348,
      "lat": 44.888881
    },
    "Hutchinson Plant #1": {
      "lng": -94.368433,
      "lat": 44.895741
    },
    "Hwy 14 Holdco Solar CSG": {
      "lng": -92.592192,
      "lat": 44.021848
    },
    "Hyacinth Solar, LLC": {
      "lng": -93.199277,
      "lat": 44.474147
    },
    "Hydra Community Solar Garden, LLC (CSG)": {
      "lng": -93.219857,
      "lat": 44.416723
    },
    "IBM West": {
      "lng": -92.5119,
      "lat": 44.0587
    },
    "International Falls Power": {
      "lng": -93.404313,
      "lat": 48.608054
    },
    "Inver Hills": {
      "lng": -93.042,
      "lat": 44.781
    },
    "Iris Solar, LLC (MN)": {
      "lng": -94.209687,
      "lat": 44.253592
    },
    "Jack River LLC": {
      "lng": -96.4411,
      "lat": 44.4233
    },
    "Janesville": {
      "lng": -93.7073,
      "lat": 44.1172
    },
    "Jeffers Wind 20 LLC": {
      "lng": -95.2525,
      "lat": 44.057778
    },
    "Jessica Mills LLC": {
      "lng": -96.43,
      "lat": 44.4278
    },
    "Johnson 1 Community Solar": {
      "lng": -92.815184,
      "lat": 45.483155
    },
    "Johnson CSG 1": {
      "lng": -92.815639,
      "lat": 45.485222
    },
    "Johnson II Community Solar": {
      "lng": -92.854173,
      "lat": 45.491965
    },
    "Johnson Solar CSG": {
      "lng": -96.187032,
      "lat": 43.912138
    },
    "Julia Hills LLC": {
      "lng": -96.4458,
      "lat": 44.4231
    },
    "Kas Brothers Windfarm": {
      "lng": -96.1125,
      "lat": 44.0119
    },
    "Kaus Community Solar": {
      "lng": -92.961372,
      "lat": 44.050383
    },
    "K-Brink Windfarm LLC": {
      "lng": -96.1183,
      "lat": 44.0197
    },
    "Kenyon Municipal": {
      "lng": -92.9848,
      "lat": 44.2718
    },
    "Kerria Solar, LLC": {
      "lng": -95.203195,
      "lat": 44.795609
    },
    "Knife Falls": {
      "lng": -92.448619,
      "lat": 46.726627
    },
    "Koda Biomass Plant": {
      "lng": -93.5386,
      "lat": 44.7956
    },
    "Koppelman Sun CSG": {
      "lng": -93.876267,
      "lat": 44.182609
    },
    "Kramer Solar CSG": {
      "lng": -94.705735,
      "lat": 44.746994
    },
    "Krause CSG": {
      "lng": -95.209719,
      "lat": 44.795624
    },
    "Lady Slipper Solar Array CSG": {
      "lng": -92.661686,
      "lat": 44.554384
    },
    "Lahr 1, LLC": {
      "lng": -94.426041,
      "lat": 45.431189
    },
    "Lake Benton I": {
      "lng": -96.375,
      "lat": 44.2917
    },
    "Lake Benton II": {
      "lng": -96.116971,
      "lat": 44.129369
    },
    "Lake Crystal": {
      "lng": -94.219697,
      "lat": 44.106888
    },
    "Lake Emily Solar": {
      "lng": -93.903166,
      "lat": 44.320364
    },
    "Lake Pulaski Solar": {
      "lng": -93.812137,
      "lat": 45.195854
    },
    "Lake Region Community Hybrid, LLC": {
      "lng": -96.157281,
      "lat": 46.505052
    },
    "Lake Waconia Community Solar Garden": {
      "lng": -93.815011,
      "lat": 44.874961
    },
    "Lake Waconia IV Community Solar Garden": {
      "lng": -93.815011,
      "lat": 44.874961
    },
    "Lakefield Junction Generating": {
      "lng": -94.841451,
      "lat": 43.798439
    },
    "Lakefield Utilities": {
      "lng": -95.1706,
      "lat": 43.674186
    },
    "Lakefield Wind Project LLC": {
      "lng": -95.156666,
      "lat": 43.675
    },
    "Lakeswind Power Partners": {
      "lng": -96.211944,
      "lat": 46.718056
    },
    "Lakota Ridge LLC": {
      "lng": -96.428367,
      "lat": 44.39956
    },
    "Lanesboro": {
      "lng": -91.977738,
      "lat": 43.720026
    },
    "Lange Solar": {
      "lng": -92.63908,
      "lat": 44.028216
    },
    "Lantana Solar, LLC": {
      "lng": -95.358,
      "lat": 44.646
    },
    "Laskin Energy Center": {
      "lng": -92.1619,
      "lat": 47.5306
    },
    "Laurel Village (CSG)": {
      "lng": -94.179684,
      "lat": 45.083808
    },
    "Lawrence Creek Solar": {
      "lng": -92.693726,
      "lat": 45.401289
    },
    "Lemond Solar": {
      "lng": -93.285927,
      "lat": 44.080995
    },
    "Leo Community Solar": {
      "lng": -95.850781,
      "lat": 43.978197
    },
    "LeSun CSG, LLC": {
      "lng": -93.552423,
      "lat": 44.213446
    },
    "Leven Garden Solar": {
      "lng": -95.26357,
      "lat": 45.71693
    },
    "Libra Community Solar": {
      "lng": -95.388514,
      "lat": 45.671231
    },
    "Lincoln Heights I": {
      "lng": -96.433333,
      "lat": 44.287778
    },
    "Lincoln Heights II": {
      "lng": -96.331677,
      "lat": 44.315537
    },
    "Lind Solar CSG": {
      "lng": -93.908719,
      "lat": 44.802355
    },
    "Linden 01 CSG": {
      "lng": -93.845389,
      "lat": 44.591619
    },
    "Linden 02 CSG": {
      "lng": -93.846816,
      "lat": 44.591933
    },
    "Linden 03 CSG": {
      "lng": -93.844971,
      "lat": 44.592132
    },
    "Lindstrom Solar CSG": {
      "lng": -92.796599,
      "lat": 45.319892
    },
    "Litchfield": {
      "lng": -94.53515,
      "lat": 45.128164
    },
    "Little Falls (MN)": {
      "lng": -94.369094,
      "lat": 45.975013
    },
    "Loon Garden": {
      "lng": -93.55615,
      "lat": 44.08968
    },
    "Lowry CSG2, LLC": {
      "lng": -95.510528,
      "lat": 45.669611
    },
    "Lucky Wind": {
      "lng": -96.4406,
      "lat": 44.0211
    },
    "Luverne": {
      "lng": -96.201944,
      "lat": 43.654444
    },
    "Lyman Garden": {
      "lng": -93.15309,
      "lat": 44.47637
    },
    "Lyra Community Solar": {
      "lng": -93.585787,
      "lat": 44.240346
    },
    "Madelia": {
      "lng": -94.4178,
      "lat": 44.0501
    },
    "Malmedal Solar Garden": {
      "lng": -95.51856,
      "lat": 45.65736
    },
    "Mankato Energy Center": {
      "lng": -94.003,
      "lat": 44.1988
    },
    "Maple Lake Station": {
      "lng": -94.009673,
      "lat": 45.229073
    },
    "Mapleton Community Solar": {
      "lng": -93.956064,
      "lat": 43.928851
    },
    "Marigold Community Solar Garden, LLC": {
      "lng": -93.615267,
      "lat": 44.197311
    },
    "Marion Garden": {
      "lng": -93.27397,
      "lat": 44.25009
    },
    "Marmas Solar CSG": {
      "lng": -94.117112,
      "lat": 45.556056
    },
    "Marshall (MN)": {
      "lng": -95.792118,
      "lat": 44.446585
    },
    "Marshall Solar Energy Project": {
      "lng": -95.669011,
      "lat": 44.475314
    },
    "Marshall Wind 1 LLC": {
      "lng": -95.8142,
      "lat": 44.3253
    },
    "Marshall Wind 2 LLC": {
      "lng": -95.8222,
      "lat": 44.3236
    },
    "Marshall Wind 3 LLC": {
      "lng": -95.7942,
      "lat": 44.3211
    },
    "Marshall Wind 4 LLC": {
      "lng": -95.8242,
      "lat": 44.3361
    },
    "Marshall Wind 5 LLC": {
      "lng": -95.8344,
      "lat": 44.3303
    },
    "Marshall Wind 6 LLC": {
      "lng": -95.8297,
      "lat": 44.3308
    },
    "Maston Garden Solar": {
      "lng": -92.69045,
      "lat": 44.04334
    },
    "McLeod Community Solar One LLC CSG": {
      "lng": -94.49429,
      "lat": 44.7192
    },
    "Medin 2 Community Solar LLC CSG": {
      "lng": -92.776389,
      "lat": 45.479972
    },
    "Medin CSG": {
      "lng": -92.78473,
      "lat": 45.488101
    },
    "Meeker Community Solar One LLC CSG": {
      "lng": -94.30718,
      "lat": 45.07106
    },
    "Melrose": {
      "lng": -94.815277,
      "lat": 45.675
    },
    "Melrose 2": {
      "lng": -94.801944,
      "lat": 45.676389
    },
    "Michael Solar": {
      "lng": -94.708334,
      "lat": 45.667472
    },
    "MinnDakota Wind LLC": {
      "lng": -96.3444,
      "lat": 44.2667
    },
    "Minnesota Breeze LLC": {
      "lng": -95.7781,
      "lat": 43.5833
    },
    "Minnesota River Station": {
      "lng": -93.581013,
      "lat": 44.795231
    },
    "Minwind": {
      "lng": -96.303611,
      "lat": 43.5564
    },
    "Minwind 3-9": {
      "lng": -96.391944,
      "lat": 43.6383
    },
    "MNDOT Standby Generation": {
      "lng": -93.105692,
      "lat": 44.953014
    },
    "Montevideo Solar LLC, CSG": {
      "lng": -95.694192,
      "lat": 44.899475
    },
    "Montgomery Winsted CSG": {
      "lng": -94.0556,
      "lat": 44.9517
    },
    "Monticello Nuclear Facility": {
      "lng": -93.8493,
      "lat": 45.3338
    },
    "Monticello Project CSG": {
      "lng": -93.8278,
      "lat": 45.2722
    },
    "Montrose Solar": {
      "lng": -93.922807,
      "lat": 45.055628
    },
    "MontSun Community Solar": {
      "lng": -93.904656,
      "lat": 45.060283
    },
    "Moorhead Wind Turbine": {
      "lng": -96.7347,
      "lat": 46.895
    },
    "Moose Lake": {
      "lng": -92.760557,
      "lat": 46.454097
    },
    "Mora": {
      "lng": -93.295137,
      "lat": 45.877088
    },
    "Moraine II Wind LLC": {
      "lng": -96.0784,
      "lat": 44.1238
    },
    "Moraine Wind LLC": {
      "lng": -96.0667,
      "lat": 44.0667
    },
    "Morgan Community Solar": {
      "lng": -94.907619,
      "lat": 44.413186
    },
    "Moulton Wind Power": {
      "lng": -95.955885,
      "lat": 43.9204
    },
    "Mountain Lake": {
      "lng": -94.9434,
      "lat": 43.9405
    },
    "Mower County Wind Energy Center": {
      "lng": -92.59882,
      "lat": 43.61407
    },
    "MSC-Chisago01 LLC CSG": {
      "lng": -92.787942,
      "lat": 45.492389
    },
    "MSC-Chisago02 LLC": {
      "lng": -92.775487,
      "lat": 45.48776
    },
    "MSC-Empire01, LLC": {
      "lng": -93.11627,
      "lat": 44.67413
    },
    "MSC-GreyCloud01 Community Solar": {
      "lng": -92.996879,
      "lat": 44.802017
    },
    "MSC-Rice01 LLC CSG": {
      "lng": -93.392662,
      "lat": 44.239015
    },
    "MSC-Scandia01 CSG": {
      "lng": -92.884717,
      "lat": 45.289314
    },
    "MSC-Scott01 LLC": {
      "lng": -93.504197,
      "lat": 44.669475
    },
    "Mud Garden Solar": {
      "lng": -94.2226,
      "lat": 44.54328
    },
    "Nautilus Lindstrom Solar (CSG)": {
      "lng": -92.813972,
      "lat": 45.336131
    },
    "Nautilus Saint Cloud Solar (CSG)": {
      "lng": -94.118924,
      "lat": 45.495562
    },
    "Nautilus Winsted Solar (CSG)": {
      "lng": -94.085661,
      "lat": 44.948882
    },
    "Nesvold Watertown Solar": {
      "lng": -93.828672,
      "lat": 44.937098
    },
    "New Germany Community Solar Garden": {
      "lng": -94.000847,
      "lat": 44.905428
    },
    "New Munich Solar": {
      "lng": -94.75565,
      "lat": 45.654086
    },
    "New Prague": {
      "lng": -93.5731,
      "lat": 44.5431
    },
    "New Ulm": {
      "lng": -94.4581,
      "lat": 44.3159
    },
    "Nicollet Community Solar One LLC": {
      "lng": -94.29093,
      "lat": 44.26035
    },
    "Nicollet Garden": {
      "lng": -94.27067,
      "lat": 44.25364
    },
    "Noble Ridge LLC": {
      "lng": -95.853447,
      "lat": 43.754739
    },
    "Nobles 2 Wind Project": {
      "lng": -95.852766,
      "lat": 43.789636
    },
    "Nobles Wind Project": {
      "lng": -95.7131,
      "lat": 43.7036
    },
    "North Branch (MN)": {
      "lng": -92.979042,
      "lat": 45.510601
    },
    "North Star Solar Project": {
      "lng": -92.91,
      "lat": 45.47
    },
    "Northern Lights Wind LLC": {
      "lng": -96.4367,
      "lat": 44.015
    },
    "Northfield Community Solar": {
      "lng": -93.198308,
      "lat": 44.475433
    },
    "Northfield Holdco CSG": {
      "lng": -93.124648,
      "lat": 44.457874
    },
    "Northfield Solar CSG": {
      "lng": -93.153061,
      "lat": 44.417131
    },
    "Northshore Mining Silver Bay Power": {
      "lng": -91.260531,
      "lat": 47.286573
    },
    "Novel - OYA of Mapleton CSG": {
      "lng": -93.96,
      "lat": 43.93
    },
    "Novel Bartel Solar CSG": {
      "lng": -92.677538,
      "lat": 44.043671
    },
    "Novel Benedix Solar CSG": {
      "lng": -92.746788,
      "lat": 44.111261
    },
    "Novel Brooten Solar CSG": {
      "lng": -95.113544,
      "lat": 45.512612
    },
    "Novel Byron Solar CSG": {
      "lng": -92.62,
      "lat": 44.01
    },
    "Novel Debra Solar LLC CSG": {
      "lng": -92.74975,
      "lat": 44.09394
    },
    "Novel DeCook Solar CSG": {
      "lng": -92.626988,
      "lat": 44.012841
    },
    "Novel Haley Solar CSG": {
      "lng": -93.54,
      "lat": 44.09
    },
    "Novel Herber Solar CSG": {
      "lng": -91.887343,
      "lat": 44.106728
    },
    "Novel Herickhoff Solar CSG": {
      "lng": -95.148447,
      "lat": 45.847225
    },
    "Novel Historical Society Solar CSG": {
      "lng": -95.360491,
      "lat": 44.787705
    },
    "Novel Huneke Solar LLC CSG": {
      "lng": -92.664872,
      "lat": 44.267319
    },
    "Novel Jewison Solar CSG": {
      "lng": -93.66,
      "lat": 44.21
    },
    "Novel Kanewischer Solar CSG": {
      "lng": -93.5,
      "lat": 44.05
    },
    "Novel Martin Solar One LLC (McLeod) CSG": {
      "lng": -94.02864,
      "lat": 44.90295
    },
    "Novel MNDot Solar CSG": {
      "lng": -92.810388,
      "lat": 44.94625
    },
    "Novel Novak Solar LLC CSG": {
      "lng": -93.93175,
      "lat": 45.659361
    },
    "Novel OYA of Osakis CSG": {
      "lng": -95.15,
      "lat": 45.85
    },
    "Novel Pederson Solar CSG": {
      "lng": -94.06,
      "lat": 45.61
    },
    "Novel Peter Solar CSG": {
      "lng": -93.608721,
      "lat": 44.196124
    },
    "Novel Reber Solar CSG": {
      "lng": -94.58,
      "lat": 45.61
    },
    "Novel Schmoll Farms Solar CSG": {
      "lng": -92.86,
      "lat": 43.97
    },
    "Novel Stavem Solar LLC CSG": {
      "lng": -95.40195,
      "lat": 45.66683
    },
    "Novel Wayne Solar LLC CSG": {
      "lng": -92.746477,
      "lat": 44.102341
    },
    "Novel Winegar Partnership Solar CSG": {
      "lng": -93.554806,
      "lat": 44.08845
    },
    "Oak Glen Wind Farm": {
      "lng": -93.140555,
      "lat": 43.920556
    },
    "Odell Wind Farm": {
      "lng": -94.931944,
      "lat": 43.853333
    },
    "Odin Wind Farm LLC": {
      "lng": -94.9142,
      "lat": 43.8953
    },
    "Olinda Trail Solar": {
      "lng": -92.80411,
      "lat": 45.31593
    },
    "Olmsted Waste Energy": {
      "lng": -92.432313,
      "lat": 44.026072
    },
    "OREG 3 Inc": {
      "lng": -94.7514,
      "lat": 43.76
    },
    "Orion Community Solar": {
      "lng": -94.401439,
      "lat": 45.587899
    },
    "Owatonna": {
      "lng": -93.23,
      "lat": 44.0833
    },
    "Owatonna Energy Station": {
      "lng": -93.262714,
      "lat": 44.085225
    },
    "Palmer Community Solar LLC CSG": {
      "lng": -92.793667,
      "lat": 45.491444
    },
    "Palmer's Creek Wind Farm, LLC": {
      "lng": -95.568888,
      "lat": 44.857694
    },
    "Paulson Community (CSG)": {
      "lng": -93.917023,
      "lat": 44.335686
    },
    "Paynesville Community Solar": {
      "lng": -94.715472,
      "lat": 45.402543
    },
    "Paynesville CSG1, LLC": {
      "lng": -94.722778,
      "lat": 45.401833
    },
    "Paynesville Solar": {
      "lng": -94.721226,
      "lat": 45.392905
    },
    "Pegasus Community Solar": {
      "lng": -95.124682,
      "lat": 45.495453
    },
    "Pillager": {
      "lng": -94.484944,
      "lat": 46.316033
    },
    "Pine Island Solar": {
      "lng": -92.663649,
      "lat": 44.206624
    },
    "Pine Island Solar CSG": {
      "lng": -92.654324,
      "lat": 44.220122
    },
    "Pipestone City Solar LLC CSG": {
      "lng": -96.317514,
      "lat": 44.015972
    },
    "Plato CSG1, LLC": {
      "lng": -94.082833,
      "lat": 44.790278
    },
    "Pleasant Valley Station": {
      "lng": -92.682874,
      "lat": 43.799446
    },
    "Pleasant Valley Wind Farm": {
      "lng": -92.769722,
      "lat": 43.805
    },
    "POET Biorefining Lake Crystal": {
      "lng": -94.276667,
      "lat": 44.093333
    },
    "PopeSun CSG, LLC": {
      "lng": -95.526171,
      "lat": 45.643438
    },
    "Porcher CSG": {
      "lng": -92.389,
      "lat": 44.279
    },
    "Porter Way Community Solar Garden": {
      "lng": -93.778397,
      "lat": 44.851028
    },
    "Prairie Island": {
      "lng": -92.6333,
      "lat": 44.622
    },
    "Prairie River": {
      "lng": -93.498266,
      "lat": 47.287206
    },
    "Prairie Rose Wind Farm": {
      "lng": -96.321567,
      "lat": 43.828217
    },
    "Prairie Star Wind Farm": {
      "lng": -92.6272,
      "lat": 43.6861
    },
    "Prawer Project CSG": {
      "lng": -94.045078,
      "lat": 45.463678
    },
    "Preston (MN)": {
      "lng": -92.0831,
      "lat": 43.6661
    },
    "Primrose Solar, LLC": {
      "lng": -93.9792,
      "lat": 44.205398
    },
    "Princeton (MN)": {
      "lng": -93.587244,
      "lat": 45.569468
    },
    "Randolph CSG PV1-5": {
      "lng": -92.972111,
      "lat": 44.527671
    },
    "Rapidan Hydro Facility": {
      "lng": -94.108817,
      "lat": 44.092689
    },
    "Rapids Energy Center": {
      "lng": -93.537169,
      "lat": 47.234825
    },
    "Red Lake Falls Community Hybrid": {
      "lng": -96.318586,
      "lat": 47.870581
    },
    "Red Maple Solar": {
      "lng": -93.899,
      "lat": 44.3194
    },
    "Red Pine Wind Project": {
      "lng": -96.1538,
      "lat": 44.478
    },
    "Red Wing": {
      "lng": -92.516912,
      "lat": 44.5692
    },
    "Red Wing Community Solar": {
      "lng": -92.516083,
      "lat": 44.53509
    },
    "Redwood Falls": {
      "lng": -95.1178,
      "lat": 44.5431
    },
    "Redwood Falls Wind": {
      "lng": -95.175,
      "lat": 44.502778
    },
    "REG Juhl Glenville Wind, LLC": {
      "lng": -93.305,
      "lat": 43.582
    },
    "Rengstorf Solar CSG": {
      "lng": -94.28907,
      "lat": 44.259504
    },
    "Rice Two Solar CSG": {
      "lng": -93.45757,
      "lat": 44.22012
    },
    "Richmond CSG": {
      "lng": -94.513847,
      "lat": 45.458606
    },
    "Ridgewind": {
      "lng": -96.065277,
      "lat": 44.018889
    },
    "Riverside (1927)": {
      "lng": -93.2753,
      "lat": 45.0203
    },
    "RJC CSG, LLC": {
      "lng": -93.277,
      "lat": 44.125
    },
    "RJC I CSG": {
      "lng": -93.279116,
      "lat": 44.125053
    },
    "RJC II Community Solar Garden": {
      "lng": -93.257152,
      "lat": 44.138851
    },
    "Roberds Garden CSG": {
      "lng": -93.239088,
      "lat": 44.29946
    },
    "Rochester Hydro": {
      "lng": -92.480177,
      "lat": 44.212981
    },
    "Rock County Wind Fuel, LLC": {
      "lng": -96.262481,
      "lat": 43.641928
    },
    "Rock Lake Station": {
      "lng": -92.97176,
      "lat": 45.805443
    },
    "Rollingstone Holdco CSG": {
      "lng": -91.791247,
      "lat": 44.114425
    },
    "Rosemount Community Solar": {
      "lng": -93.022909,
      "lat": 44.72241
    },
    "Rushford Village": {
      "lng": -91.791594,
      "lat": 43.8066
    },
    "Ruthton Ridge LLC": {
      "lng": -96.2131,
      "lat": 44.2128
    },
    "S&P Windfarm LLC": {
      "lng": -95.3825,
      "lat": 43.651389
    },
    "Sacred Heart CSG1, LLC": {
      "lng": -95.361083,
      "lat": 44.783167
    },
    "SAF Hydroelectric LLC": {
      "lng": -93.247777,
      "lat": 44.978889
    },
    "Sagitta Community Solar": {
      "lng": -95.386192,
      "lat": 44.948328
    },
    "Saint Marys Hospital Power Plant": {
      "lng": -92.479815,
      "lat": 44.09785
    },
    "Sappi Cloquet Mill": {
      "lng": -92.4298,
      "lat": 46.7243
    },
    "Sartell Dam": {
      "lng": -94.200786,
      "lat": 45.617996
    },
    "Sartell Holdco CSG": {
      "lng": -94.123291,
      "lat": 45.36443
    },
    "Scandia Community Solar Garden": {
      "lng": -92.8125,
      "lat": 45.28718
    },
    "Scandia CSG": {
      "lng": -92.8766,
      "lat": 45.2528
    },
    "Scanlon": {
      "lng": -92.421733,
      "lat": 46.709126
    },
    "School Sisters CSG": {
      "lng": -93.968902,
      "lat": 44.17897
    },
    "Schueler Community Solar": {
      "lng": -94.088307,
      "lat": 45.306139
    },
    "Schull CSG": {
      "lng": -93.930384,
      "lat": 43.91635
    },
    "Schultz CSG": {
      "lng": -93.376,
      "lat": 44.5576
    },
    "Schwinghamer Project CSG": {
      "lng": -94.596082,
      "lat": 45.626942
    },
    "Severson Garden": {
      "lng": -93.49594,
      "lat": 44.00057
    },
    "Shakopee Energy Park": {
      "lng": -93.481111,
      "lat": 44.783611
    },
    "Shakopee Met Council WTP": {
      "lng": -93.436971,
      "lat": 44.796611
    },
    "Shane's Wind Machine": {
      "lng": -96.1361,
      "lat": 44.1508
    },
    "Shaokatan Hills LLC": {
      "lng": -96.434694,
      "lat": 44.421711
    },
    "Sherburne Community Solar": {
      "lng": -94.044166,
      "lat": 45.438123
    },
    "Sherburne Community Solar One LLC CSG": {
      "lng": -94.13578,
      "lat": 45.53199
    },
    "Sherburne County": {
      "lng": -93.8931,
      "lat": 45.3808
    },
    "Sherburne North Project CSG": {
      "lng": -94.1143,
      "lat": 45.4978
    },
    "Siems Solar Project CSG": {
      "lng": -92.55696,
      "lat": 44.272392
    },
    "Silver Lake Garden": {
      "lng": -93.49541,
      "lat": 44.00789
    },
    "Slayton Solar": {
      "lng": -95.767222,
      "lat": 43.980278
    },
    "Sleepy Eye": {
      "lng": -94.725191,
      "lat": 44.298116
    },
    "SMMPA Methane Energy Facility": {
      "lng": -93.354167,
      "lat": 45.847778
    },
    "Sobania Community Solar LLC": {
      "lng": -94.02,
      "lat": 45.42
    },
    "SolarClub 10 LLC": {
      "lng": -93.83,
      "lat": 45.273
    },
    "SolarClub 15 LLC": {
      "lng": -94.026,
      "lat": 45.43
    },
    "SolarClub 20 LLC": {
      "lng": -92.028,
      "lat": 44.368
    },
    "SolarClub 23 LLC": {
      "lng": -91.323,
      "lat": 43.866
    },
    "SolarClub 28 LLC": {
      "lng": -94.226,
      "lat": 44.543
    },
    "SolarClub 35 LLC": {
      "lng": -94.077,
      "lat": 44.101
    },
    "Soliloquoy Ridge LLC": {
      "lng": -96.2331,
      "lat": 44.2364
    },
    "Solway Plant": {
      "lng": -95.129652,
      "lat": 47.544582
    },
    "South Fork Wind Farm": {
      "lng": -95.375119,
      "lat": 43.587283
    },
    "South Generation": {
      "lng": -95.1122,
      "lat": 44.5331
    },
    "Southeast Steam Plant": {
      "lng": -93.249722,
      "lat": 44.980833
    },
    "Southern Minnesota Beet Sugar": {
      "lng": -95.179787,
      "lat": 44.796405
    },
    "Spartan Hills LLC": {
      "lng": -96.2383,
      "lat": 44.2358
    },
    "Spica Community Solar": {
      "lng": -94.854097,
      "lat": 44.936146
    },
    "Spring Valley": {
      "lng": -92.389392,
      "lat": 43.687322
    },
    "Springfield (MN)": {
      "lng": -94.976262,
      "lat": 44.239539
    },
    "Spruce Ridge Gas Recovery": {
      "lng": -94.2489,
      "lat": 44.8158
    },
    "St James": {
      "lng": -94.61564,
      "lat": 43.988158
    },
    "St Paul Cogeneration": {
      "lng": -93.096559,
      "lat": 44.942889
    },
    "St. Bonifacius Station": {
      "lng": -93.820761,
      "lat": 44.905026
    },
    "St. Charles Power Plant": {
      "lng": -92.0597,
      "lat": 43.9742
    },
    "St. Charles Solar": {
      "lng": -92.032,
      "lat": 43.976
    },
    "St. Cloud Hydro": {
      "lng": -94.148577,
      "lat": 45.547592
    },
    "St. Cloud Solar CSG": {
      "lng": -94.121314,
      "lat": 45.477397
    },
    "St. Olaf StandBy Generators": {
      "lng": -93.180833,
      "lat": 44.461111
    },
    "St. Olaf Wind Turbine": {
      "lng": -93.192778,
      "lat": 44.462222
    },
    "St. Paul Intl Airport Red & Blue Parking": {
      "lng": -93.21,
      "lat": 44.88
    },
    "STAG St. Paul Project CSG": {
      "lng": -93.026432,
      "lat": 44.885633
    },
    "Stahl Wind Energy": {
      "lng": -96.4303,
      "lat": 44.0256
    },
    "Star Garden": {
      "lng": -95.53088,
      "lat": 45.65063
    },
    "Stearns Community Solar One LLC CSG": {
      "lng": -94.48596,
      "lat": 45.61804
    },
    "Stearns Solar I": {
      "lng": -94.557542,
      "lat": 45.650158
    },
    "Stevens Community Medical Center": {
      "lng": -95.9042,
      "lat": 45.585
    },
    "Stevens Community Solar LLC": {
      "lng": -93.94,
      "lat": 45.662
    },
    "Stewart CSG1, LLC": {
      "lng": -94.495083,
      "lat": 44.725639
    },
    "Stockton (MN)": {
      "lng": -91.7669,
      "lat": 44.04105
    },
    "Stoneray Power Partners, LLC": {
      "lng": -96.126733,
      "lat": 44.104377
    },
    "Straight Garden Solar": {
      "lng": -93.27304,
      "lat": 44.25271
    },
    "Strandness Garden": {
      "lng": -95.52985,
      "lat": 45.65727
    },
    "Studenski Community Solar LLC CSG": {
      "lng": -94.05,
      "lat": 45.63
    },
    "Sun River LLC": {
      "lng": -96.4358,
      "lat": 44.4289
    },
    "SunE Feely 1 CSG, LLC": {
      "lng": -93.110402,
      "lat": 44.655217
    },
    "SunE Stolee CSG, LLC": {
      "lng": -93.35047,
      "lat": 44.173879
    },
    "Sunrise Community Solar": {
      "lng": -92.891092,
      "lat": 45.474994
    },
    "Sunset Breeze LLC": {
      "lng": -95.78,
      "lat": 43.5811
    },
    "Svihel Community Solar LLC CSG": {
      "lng": -93.919,
      "lat": 45.678
    },
    "Swan Garden": {
      "lng": -95.2653,
      "lat": 45.7204
    },
    "Sylvan": {
      "lng": -94.3775,
      "lat": 46.305
    },
    "Syncarpha Clara City CSG I (Stamer)": {
      "lng": -95.357069,
      "lat": 44.955515
    },
    "Syncarpha Dodge 1 CSG": {
      "lng": -92.880709,
      "lat": 44.044978
    },
    "Syncarpha Prinsburg CSG (Ledeboer)": {
      "lng": -95.188419,
      "lat": 44.931794
    },
    "T Luhman CSG": {
      "lng": -92.682,
      "lat": 44.282
    },
    "Taconite Harbor Energy Center": {
      "lng": -90.9114,
      "lat": 47.5314
    },
    "Taconite Ridge 1 Wind Energy Center": {
      "lng": -92.5917,
      "lat": 47.5747
    },
    "Tatanka Wi": {
      "lng": -93.884273,
      "lat": 45.210766
    },
    "Taurus Community Solar": {
      "lng": -95.691471,
      "lat": 44.95028
    },
    "Taylors Falls CSG": {
      "lng": -92.68435,
      "lat": 45.391842
    },
    "TG Windfarm LLC": {
      "lng": -96.2236,
      "lat": 43.9897
    },
    "Thief River Falls": {
      "lng": -96.179176,
      "lat": 48.114262
    },
    "Thomson": {
      "lng": -92.3333,
      "lat": 46.655
    },
    "Thomson Reuters Campus Bldg A-D": {
      "lng": -93.1156,
      "lat": 44.8196
    },
    "Thomson Reuters Data Center Bldg ES101": {
      "lng": -93.1139,
      "lat": 44.8244
    },
    "Thomson Reuters Data Center Bldg H": {
      "lng": -93.1167,
      "lat": 44.8289
    },
    "Tiller CSG": {
      "lng": -94.0403,
      "lat": 45.435
    },
    "Tofteland Windfarm LLC": {
      "lng": -96.223333,
      "lat": 43.980278
    },
    "Trimont Area Wind Farm": {
      "lng": -94.8499,
      "lat": 43.7938
    },
    "Truman": {
      "lng": -94.436908,
      "lat": 43.828415
    },
    "Tsar Nicholas LLC": {
      "lng": -96.4347,
      "lat": 44.42
    },
    "Tweite Community Solar LLC": {
      "lng": -92.674494,
      "lat": 44.025148
    },
    "Twin Cities Hydro LLC": {
      "lng": -93.1994,
      "lat": 44.9156
    },
    "Twin Lake Hills LLC": {
      "lng": -96.2789,
      "lat": 44.2258
    },
    "Uilk Wind LLC": {
      "lng": -96.4269,
      "lat": 43.9811
    },
    "UMM Wind Turbine": {
      "lng": -95.877033,
      "lat": 45.5897
    },
    "Union Garden": {
      "lng": -93.13565,
      "lat": 44.46936
    },
    "Univ Minnesota CHP Plant": {
      "lng": -93.240833,
      "lat": 44.978611
    },
    "Ursa Community Solar": {
      "lng": -93.082555,
      "lat": 44.671581
    },
    "USS All In Solar LLC CSG": {
      "lng": -93.38967,
      "lat": 43.83688
    },
    "USS B&B Solar LLC CSG": {
      "lng": -95.009,
      "lat": 44.413
    },
    "USS Big Lake 1 Solar CSG": {
      "lng": -93.764931,
      "lat": 45.317071
    },
    "USS Bluff Country Solar CSG": {
      "lng": -91.843552,
      "lat": 44.111881
    },
    "USS Brockway Solar CSG": {
      "lng": -94.211331,
      "lat": 45.682305
    },
    "USS Buckaroo Solar LLC CSG": {
      "lng": -96.500779,
      "lat": 46.97727
    },
    "USS Bush Solar LLC CSG": {
      "lng": -93.26689,
      "lat": 44.12347
    },
    "USS Centerfield Solar CSG": {
      "lng": -93.348,
      "lat": 44.529
    },
    "USS Chariot Solar LLC": {
      "lng": -95.09562,
      "lat": 44.78883
    },
    "USS Cheyenne Solar LLC CSG": {
      "lng": -92.922343,
      "lat": 45.347008
    },
    "USS Christoffer Solar LLC CSG": {
      "lng": -95.766554,
      "lat": 44.985518
    },
    "USS Cougar Solar LLC CSG": {
      "lng": -94.926419,
      "lat": 44.454726
    },
    "USS Danube Solar LLC": {
      "lng": -95.103019,
      "lat": 44.795592
    },
    "USS Dot Com Solar LLC CSG": {
      "lng": -94.92664,
      "lat": 44.422314
    },
    "USS Dubhe Solar CSG": {
      "lng": -92.904452,
      "lat": 45.492616
    },
    "USS DVL Solar CSG": {
      "lng": -93.785,
      "lat": 44.818
    },
    "USS Eggo Solar CSG": {
      "lng": -93.788,
      "lat": 44.72
    },
    "USS Flower Solar LLC CSG": {
      "lng": -94.20905,
      "lat": 44.269143
    },
    "USS Good Solar CSG": {
      "lng": -92.940656,
      "lat": 45.436226
    },
    "USS Greenhouse Solar LLC CSG": {
      "lng": -92.67075,
      "lat": 44.238655
    },
    "USS Hancock Solar LLC CSG": {
      "lng": -93.7858,
      "lat": 44.7025
    },
    "USS Haven Solar LLC CSG": {
      "lng": -94.109836,
      "lat": 45.515706
    },
    "USS Horne North Solar LLC CSG": {
      "lng": -96.479959,
      "lat": 47.021803
    },
    "USS Horne South Solar LLC CSG": {
      "lng": -96.50004,
      "lat": 47.00414
    },
    "USS Hubers Solar LLC CSG": {
      "lng": -93.12447,
      "lat": 44.45692
    },
    "USS JJ Clay Solar LLC CSG": {
      "lng": -96.49989,
      "lat": 47.02945
    },
    "USS JJ Solar CSG": {
      "lng": -94.050356,
      "lat": 45.636126
    },
    "USS Kasch Solar CSG": {
      "lng": -94.059269,
      "lat": 45.623423
    },
    "USS Kass Solar LLC": {
      "lng": -95.68949,
      "lat": 44.22443
    },
    "USS King 2 CSG": {
      "lng": -93.846,
      "lat": 44.848
    },
    "USS Kost Trail Solar CSG": {
      "lng": -92.872,
      "lat": 45.468
    },
    "USS KVPV Solar LLC CSG": {
      "lng": -96.500649,
      "lat": 46.983191
    },
    "USS Lake Patterson Solar CSG": {
      "lng": -93.888,
      "lat": 44.839
    },
    "USS Mayhew Solar LLC CSG": {
      "lng": -94.049866,
      "lat": 45.62512
    },
    "USS Midtown Solar LLC CSG": {
      "lng": -94.748979,
      "lat": 45.671603
    },
    "USS Milkweed Solar LLC CSG": {
      "lng": -94.64286,
      "lat": 45.43538
    },
    "USS Monarch Solar LLC CSG": {
      "lng": -95.139788,
      "lat": 45.49896
    },
    "USS Nillie Corn Solar CSG": {
      "lng": -92.89758,
      "lat": 45.456657
    },
    "USS Norelius Solar CSG": {
      "lng": -92.88658,
      "lat": 45.473642
    },
    "USS Pheasant Solar LLC CSG": {
      "lng": -95.251327,
      "lat": 44.934706
    },
    "USS Reindeer Solar LLC CSG": {
      "lng": -94.043216,
      "lat": 45.643806
    },
    "USS Rockpoint Solar CSG": {
      "lng": -92.922057,
      "lat": 45.439639
    },
    "USS Solar Dawn CSG": {
      "lng": -92.841712,
      "lat": 45.491799
    },
    "USS Solar Rapids CSG": {
      "lng": -94.138625,
      "lat": 45.590952
    },
    "USS Solar Sources LLC CSG": {
      "lng": -92.885607,
      "lat": 45.231914
    },
    "USS Solar Way LLC CSG": {
      "lng": -93.43378,
      "lat": 43.82563
    },
    "USS Steamboat Solar LLC CSG": {
      "lng": -94.9858,
      "lat": 44.41301
    },
    "USS Sunrise Solar LLC CSG": {
      "lng": -92.813822,
      "lat": 45.491496
    },
    "USS Turkey Solar LLC CSG": {
      "lng": -94.739521,
      "lat": 45.139302
    },
    "USS Verde Solar LLC CSG": {
      "lng": -94.930618,
      "lat": 44.427985
    },
    "USS Viceroy Solar LLC CSG": {
      "lng": -95.16451,
      "lat": 45.49776
    },
    "USS Water City Solar LLC CSG": {
      "lng": -93.58042,
      "lat": 44.21782
    },
    "USS Water Town Solar LLC CSG": {
      "lng": -93.58484,
      "lat": 44.19777
    },
    "USS Webster Solar CSG": {
      "lng": -93.395,
      "lat": 44.529
    },
    "USS Westeros Solar LLC CSG": {
      "lng": -93.768207,
      "lat": 44.681452
    },
    "USS White Cloud LLC CSG": {
      "lng": -94.13041,
      "lat": 45.52079
    },
    "USS Wildcat Solar LLC": {
      "lng": -92.606278,
      "lat": 44.394128
    },
    "Valley View Transmission LLC": {
      "lng": -96.021744,
      "lat": 43.902753
    },
    "Vega Community Solar": {
      "lng": -95.203741,
      "lat": 45.382813
    },
    "Vermillion River_MN_GRE DEA-GM": {
      "lng": -93.136105,
      "lat": 44.599804
    },
    "Veseli Community Solar Garden": {
      "lng": -93.366494,
      "lat": 44.526806
    },
    "Viking Wind Partners": {
      "lng": -96.011809,
      "lat": 43.983462
    },
    "Virginia": {
      "lng": -92.540951,
      "lat": 47.522428
    },
    "Wabasha Holdco Solar CSG": {
      "lng": -92.01928,
      "lat": 44.36714
    },
    "Wabasha Solar": {
      "lng": -92.021668,
      "lat": 44.363109
    },
    "WakeSun LLC": {
      "lng": -94.406417,
      "lat": 45.472889
    },
    "Walcott Solar CSG": {
      "lng": -93.275012,
      "lat": 44.235164
    },
    "Wapsipinicon Wind Project": {
      "lng": -92.6822,
      "lat": 43.8006
    },
    "Warsaw Solar CSG": {
      "lng": -93.39953,
      "lat": 44.225457
    },
    "Waseca Solar": {
      "lng": -93.530984,
      "lat": 44.092414
    },
    "WasecaSun": {
      "lng": -93.386458,
      "lat": 44.051448
    },
    "Water Reclamation Plant": {
      "lng": -92.469167,
      "lat": 44.064722
    },
    "Waterford Holdco CSG": {
      "lng": -93.19859,
      "lat": 44.475467
    },
    "Waterville CSG Solar Holdings LLC": {
      "lng": -93.571169,
      "lat": 44.197103
    },
    "WaveSun Community Solar": {
      "lng": -93.980233,
      "lat": 45.060844
    },
    "WCROC Wind Farm": {
      "lng": -95.876944,
      "lat": 45.589722
    },
    "Webster Holdco Solar CSG": {
      "lng": -93.410622,
      "lat": 44.529703
    },
    "Welcome Wind Turbine": {
      "lng": -94.5892,
      "lat": 43.6825
    },
    "Wells IC": {
      "lng": -93.724129,
      "lat": 43.74453
    },
    "West Faribault Solar": {
      "lng": -93.310634,
      "lat": 44.274932
    },
    "West Group Data Center": {
      "lng": -93.1139,
      "lat": 44.8256
    },
    "West Group Data Center F": {
      "lng": -93.1167,
      "lat": 44.8283
    },
    "West Waconia Solar": {
      "lng": -93.901292,
      "lat": 44.789894
    },
    "Westbrook": {
      "lng": -95.4429,
      "lat": 44.0399
    },
    "Westport Community Solar, LLC": {
      "lng": -94.904383,
      "lat": 44.939037
    },
    "Westridge Windfarm LLC": {
      "lng": -96.215556,
      "lat": 43.988611
    },
    "Westside Energy Station": {
      "lng": -92.551866,
      "lat": 44.039023
    },
    "Willmar": {
      "lng": -95.053236,
      "lat": 45.121705
    },
    "Willmar Municipal East Substation": {
      "lng": -95.025,
      "lat": 45.1272
    },
    "Willmar Municipal SW Substation": {
      "lng": -95.0844,
      "lat": 45.105
    },
    "Wilmarth": {
      "lng": -94.0009,
      "lat": 44.1967
    },
    "Windcurrent Farms LLC": {
      "lng": -96.0789,
      "lat": 44.0236
    },
    "Windom": {
      "lng": -95.114444,
      "lat": 43.86811
    },
    "Windom Wind Project": {
      "lng": -95.1306,
      "lat": 43.9578
    },
    "Winegar CSG": {
      "lng": -93.507258,
      "lat": 44.11283
    },
    "Winona Community Solar One LLC": {
      "lng": -91.321983,
      "lat": 43.858524
    },
    "Winona County Wind LLC": {
      "lng": -91.924722,
      "lat": 44.1125
    },
    "Winona Solar": {
      "lng": -91.900997,
      "lat": 44.124097
    },
    "Winters Spawn LLC": {
      "lng": -96.1989,
      "lat": 44.2081
    },
    "Winton": {
      "lng": -91.7639,
      "lat": 47.9344
    },
    "Wolf Wind Enterprises LLC": {
      "lng": -95.775833,
      "lat": 43.581111
    },
    "Wollan Garden Solar": {
      "lng": -95.530903,
      "lat": 45.637121
    },
    "Woodbury Solar": {
      "lng": -92.9675,
      "lat": 44.881944
    },
    "Woodstock Windfarm": {
      "lng": -96.078611,
      "lat": 43.985833
    },
    "Worthington": {
      "lng": -95.609234,
      "lat": 43.63349
    },
    "Wright Cuddyer CSG": {
      "lng": -93.7341,
      "lat": 45.1831
    },
    "Wright Kirby 1-5 CSG": {
      "lng": -93.791,
      "lat": 45.259
    },
    "WrightSun CSG, LLC": {
      "lng": -93.718376,
      "lat": 45.21643
    },
    "Wyoming 2 CSG, LLC": {
      "lng": -92.95,
      "lat": 45.35
    },
    "Zumbro Community Solar Garden": {
      "lng": -92.670339,
      "lat": 44.319478
    },
    "Zumbro Solar Garden": {
      "lng": -92.75999,
      "lat": 44.04603
    },
    "Ameresco Jefferson City": {
      "lng": -92.04493,
      "lat": 38.552371
    },
    "Anheuser-Busch St Louis": {
      "lng": -90.210671,
      "lat": 38.597755
    },
    "Atchison County Wind": {
      "lng": -95.354881,
      "lat": 40.531283
    },
    "Audrain Power Plant": {
      "lng": -91.5369,
      "lat": 39.3092
    },
    "Black Oak Power Producers": {
      "lng": -92.453889,
      "lat": 37.231944
    },
    "Bluegrass Ridge": {
      "lng": -94.4869,
      "lat": 40.1006
    },
    "Butler Municipal Power Plant": {
      "lng": -94.344385,
      "lat": 38.264408
    },
    "Butler Solar Power Project": {
      "lng": -94.35,
      "lat": 38.27
    },
    "Callaway": {
      "lng": -91.778841,
      "lat": 38.758919
    },
    "Carrollton": {
      "lng": -93.5016,
      "lat": 39.3515
    },
    "Carthage": {
      "lng": -94.299431,
      "lat": 37.176646
    },
    "Chillicothe": {
      "lng": -93.564112,
      "lat": 39.783388
    },
    "Chillicothe Solar Farm": {
      "lng": -93.536833,
      "lat": 39.788769
    },
    "City of West Plains Power Station": {
      "lng": -91.868611,
      "lat": 36.748611
    },
    "Clarence Cannon": {
      "lng": -91.643969,
      "lat": 39.524791
    },
    "Clear Creek Wind": {
      "lng": -94.892014,
      "lat": 40.369799
    },
    "Columbia": {
      "lng": -89.4203,
      "lat": 43.4864
    },
    "Columbia Energy Center (MO)": {
      "lng": -92.2629,
      "lat": 39.0192
    },
    "Conception": {
      "lng": -94.6722,
      "lat": 40.2572
    },
    "Cow Branch": {
      "lng": -95.4683,
      "lat": 40.4535
    },
    "Cox Battery Energy Storage": {
      "lng": -93.324363,
      "lat": 37.135022
    },
    "Dogwood Energy Facility": {
      "lng": -94.3006,
      "lat": 38.7931
    },
    "El Dorado Springs Solar Farm": {
      "lng": -94.002631,
      "lat": 37.858706
    },
    "Elm Street Substation Generators": {
      "lng": -90.4156,
      "lat": 37.7814
    },
    "Empire District Elec Co Energy Ctr": {
      "lng": -94.1041,
      "lat": 37.1385
    },
    "Essex Power Plant": {
      "lng": -89.840173,
      "lat": 36.868206
    },
    "Fairgrounds": {
      "lng": -92.2206,
      "lat": 38.5935
    },
    "Farmers City Wind LLC": {
      "lng": -95.5228,
      "lat": 40.575
    },
    "Farmington Solar Farm": {
      "lng": -90.427736,
      "lat": 37.755333
    },
    "Fayette": {
      "lng": -92.68,
      "lat": 39.1406
    },
    "Fredericktown Energy Center": {
      "lng": -90.317251,
      "lat": 37.571672
    },
    "Fulton (MO)": {
      "lng": -91.94778,
      "lat": 38.868095
    },
    "Gallatin (MO)": {
      "lng": -93.953889,
      "lat": 39.917222
    },
    "Grand Ave Plant": {
      "lng": -94.581111,
      "lat": 39.111944
    },
    "Greenwood Energy Center": {
      "lng": -94.2982,
      "lat": 38.8615
    },
    "Hannibal - Oakwood Substation": {
      "lng": -91.3988,
      "lat": 39.679842
    },
    "Hannibal - Wastewater Treatment Plant": {
      "lng": -91.37138,
      "lat": 39.699516
    },
    "Hannibal - Water Treatment Plant": {
      "lng": -91.374428,
      "lat": 39.723032
    },
    "Harry Truman": {
      "lng": -93.4062,
      "lat": 38.2623
    },
    "Hawthorn": {
      "lng": -94.4778,
      "lat": 39.1306
    },
    "Higginsville Municipal Power Plant": {
      "lng": -93.714748,
      "lat": 39.073315
    },
    "Higginsville Solar Farm": {
      "lng": -93.726825,
      "lat": 39.002139
    },
    "High Prairie Wind Farm": {
      "lng": -92.518612,
      "lat": 40.398244
    },
    "Holden Power Plant": {
      "lng": -93.9983,
      "lat": 38.7538
    },
    "Iatan": {
      "lng": -94.98,
      "lat": 39.4472
    },
    "IKEA St. Louis 410": {
      "lng": -90.245549,
      "lat": 38.634995
    },
    "Independence II Solar Farm": {
      "lng": -94.27,
      "lat": 39.04
    },
    "Independence Solar Farm": {
      "lng": -94.288161,
      "lat": 39.117142
    },
    "Jackson (MO)": {
      "lng": -89.6606,
      "lat": 37.3847
    },
    "Jackson Square": {
      "lng": -94.411522,
      "lat": 39.094574
    },
    "James River": {
      "lng": -93.261944,
      "lat": 37.108611
    },
    "John Twitty Energy Center": {
      "lng": -93.38804,
      "lat": 37.151706
    },
    "Kahoka": {
      "lng": -91.7206,
      "lat": 40.4225
    },
    "KCP&L SmartGrid Innovation Park": {
      "lng": -94.571215,
      "lat": 39.040263
    },
    "Kennett": {
      "lng": -90.0521,
      "lat": 36.2338
    },
    "Kings Point Wind Energy Center": {
      "lng": -94.05271,
      "lat": 37.269858
    },
    "La Plata": {
      "lng": -92.4896,
      "lat": 40.0244
    },
    "Labadie": {
      "lng": -90.837686,
      "lat": 38.562244
    },
    "Lake Road": {
      "lng": -94.8773,
      "lat": 39.7246
    },
    "Lebanon Solar Farm (MO)": {
      "lng": -92.670628,
      "lat": 37.667831
    },
    "Loess Hills": {
      "lng": -95.54,
      "lat": 40.4144
    },
    "Lost Creek Wind Energy Facility": {
      "lng": -94.518056,
      "lat": 39.981389
    },
    "Macon": {
      "lng": -92.4689,
      "lat": 39.7378
    },
    "Macon Energy Center": {
      "lng": -92.384476,
      "lat": 39.748975
    },
    "Macon Solar Power Project": {
      "lng": -92.46,
      "lat": 39.76
    },
    "Malden": {
      "lng": -89.9682,
      "lat": 36.5531
    },
    "Marceline": {
      "lng": -92.945833,
      "lat": 39.716944
    },
    "Marshall (MO)": {
      "lng": -93.2064,
      "lat": 39.1228
    },
    "Marshall Solar Farm (MO)": {
      "lng": -93.2104,
      "lat": 39.0948
    },
    "MBS Textbook Exchange": {
      "lng": -92.3776,
      "lat": 38.95718
    },
    "McCartney Generating Station": {
      "lng": -93.170839,
      "lat": 37.248431
    },
    "Memphis": {
      "lng": -92.181,
      "lat": 40.4578
    },
    "Meramec": {
      "lng": -90.3358,
      "lat": 38.4017
    },
    "Mexico": {
      "lng": -91.8306,
      "lat": 39.1481
    },
    "MJMEUC Generating Station #1": {
      "lng": -91.640071,
      "lat": 39.251076
    },
    "Moberly": {
      "lng": -92.4837,
      "lat": 39.4244
    },
    "Moreau": {
      "lng": -92.102316,
      "lat": 38.555713
    },
    "MU Combined Heat and Power Plant": {
      "lng": -92.3328,
      "lat": 38.9461
    },
    "Nevada": {
      "lng": -94.3444,
      "lat": 37.8503
    },
    "New Madrid Power Plant": {
      "lng": -89.5617,
      "lat": 36.5147
    },
    "Niangua": {
      "lng": -92.847701,
      "lat": 37.9385
    },
    "Nixa Solar, LLC": {
      "lng": -93.338969,
      "lat": 37.043903
    },
    "Noble Hill Landfill": {
      "lng": -93.3514,
      "lat": 37.3811
    },
    "Nodaway Power Plant": {
      "lng": -94.7913,
      "lat": 40.2877
    },
    "North Fork Ridge Wind Energy Center": {
      "lng": -94.51027,
      "lat": 37.38999
    },
    "Northeast Generating Station": {
      "lng": -94.5606,
      "lat": 39.1228
    },
    "O'Fallon Renewable Energy Center": {
      "lng": -90.678124,
      "lat": 38.826648
    },
    "Osage Dam": {
      "lng": -92.6239,
      "lat": 38.2044
    },
    "Osborn Wind Energy": {
      "lng": -94.473482,
      "lat": 39.747699
    },
    "Overall Road Station": {
      "lng": -90.4526,
      "lat": 37.7608
    },
    "Ozark Beach": {
      "lng": -93.1235,
      "lat": 36.658995
    },
    "Peno Creek Energy Center": {
      "lng": -91.2295,
      "lat": 39.3531
    },
    "Poplar Bluff Generating Station": {
      "lng": -90.3903,
      "lat": 36.7597
    },
    "Prosperity Solar Farm CSG": {
      "lng": -94.433694,
      "lat": 37.127139
    },
    "Ralph Green Station": {
      "lng": -94.277689,
      "lat": 38.78758
    },
    "Renewable Energy Center at BJH": {
      "lng": -90.258198,
      "lat": 38.636459
    },
    "Rock Creek Wind Project": {
      "lng": -95.2,
      "lat": 40.4
    },
    "Rolla Solar Farm (MO)": {
      "lng": -91.7169,
      "lat": 37.9853
    },
    "Rush Island": {
      "lng": -90.263157,
      "lat": 38.131248
    },
    "Salisbury City of": {
      "lng": -92.8028,
      "lat": 39.4236
    },
    "Shelbina Power #1": {
      "lng": -92.047014,
      "lat": 39.694755
    },
    "Shelbina Power #2": {
      "lng": -92.04674,
      "lat": 39.694784
    },
    "Shelbina Power #3": {
      "lng": -92.053456,
      "lat": 39.69755
    },
    "Sikeston": {
      "lng": -89.6209,
      "lat": 36.8791
    },
    "Sioux": {
      "lng": -90.290247,
      "lat": 38.915479
    },
    "South Harper Peaking Facility": {
      "lng": -94.4824,
      "lat": 38.6803
    },
    "Southwestern Bell Telephone": {
      "lng": -90.193377,
      "lat": 38.627566
    },
    "Springfield Solar 1 LLC": {
      "lng": -93.166389,
      "lat": 37.246389
    },
    "St Joseph Landfill Generating Station": {
      "lng": -94.776111,
      "lat": 39.674444
    },
    "St. Francis Power Plant": {
      "lng": -90.1779,
      "lat": 36.5852
    },
    "Stanberry": {
      "lng": -94.542359,
      "lat": 40.212746
    },
    "State Line (MO)": {
      "lng": -94.614,
      "lat": 37.0659
    },
    "Station H": {
      "lng": -94.3804,
      "lat": 39.1067
    },
    "Station I": {
      "lng": -94.392957,
      "lat": 39.055954
    },
    "Stockton Hydro": {
      "lng": -93.7691,
      "lat": 37.6936
    },
    "Sub 2 Generating Station": {
      "lng": -92.5,
      "lat": 39.7503
    },
    "Sub 3 Generating Station": {
      "lng": -92.453333,
      "lat": 39.761389
    },
    "Table Rock": {
      "lng": -93.3086,
      "lat": 36.597
    },
    "Taum Sauk": {
      "lng": -90.834722,
      "lat": 37.520833
    },
    "Thomas Hill Energy Center": {
      "lng": -92.6381,
      "lat": 39.5522
    },
    "Trenton North": {
      "lng": -93.635,
      "lat": 40.0786
    },
    "Trenton Solar Farm (MO)": {
      "lng": -93.6122,
      "lat": 40.1155
    },
    "Trenton South": {
      "lng": -93.604363,
      "lat": 40.067218
    },
    "Trigen St. Louis": {
      "lng": -90.1809,
      "lat": 38.6359
    },
    "Truman Solar": {
      "lng": -92.243,
      "lat": 38.956
    },
    "Unionville": {
      "lng": -93.021833,
      "lat": 40.459344
    },
    "Unionville City of": {
      "lng": -93.010018,
      "lat": 40.476742
    },
    "Walton Street Substation": {
      "lng": -90.4336,
      "lat": 37.7881
    },
    "Waynesville Solar Farm (MO)": {
      "lng": -92.2535,
      "lat": 37.8041
    },
    "White Cloud Wind Project, LLC": {
      "lng": -95.38,
      "lat": 40.44
    },
    "Ackerman Combined Cycle Plant": {
      "lng": -89.2089,
      "lat": 33.3792
    },
    "Attala Generating Plant": {
      "lng": -89.6753,
      "lat": 33.0147
    },
    "Batesville Generation Facility": {
      "lng": -89.9275,
      "lat": 34.3356
    },
    "Baxter Wilson": {
      "lng": -90.9306,
      "lat": 32.2831
    },
    "Benndale": {
      "lng": -88.8054,
      "lat": 30.8889
    },
    "Caledonia": {
      "lng": -88.2717,
      "lat": 33.6464
    },
    "CF Industries Yazoo City Complex": {
      "lng": -90.3781,
      "lat": 32.90411
    },
    "Chevron Cogenerating Station": {
      "lng": -88.4919,
      "lat": 30.34
    },
    "Choctaw County Gen": {
      "lng": -89.4201,
      "lat": 33.2881
    },
    "Columbus MS": {
      "lng": -88.46,
      "lat": 33.3602
    },
    "Crossroads Energy Center (CPU)": {
      "lng": -90.5621,
      "lat": 34.183
    },
    "Daniel Electric Generating Plant": {
      "lng": -88.5553,
      "lat": 30.5322
    },
    "David M Ratcliffe": {
      "lng": -88.762222,
      "lat": 32.654722
    },
    "District 70 Transco Gas Pipe Line": {
      "lng": -90.19914,
      "lat": 31.24793
    },
    "Ergon Refining Vicksburg": {
      "lng": -90.908344,
      "lat": 32.388271
    },
    "Georgia-Pacific Monticello Paper": {
      "lng": -90.0809,
      "lat": 31.6285
    },
    "Georgia-Pacific Taylorsville Plywood": {
      "lng": -89.4635,
      "lat": 31.837173
    },
    "Gerald Andrus": {
      "lng": -91.1167,
      "lat": 33.35
    },
    "Grand Gulf": {
      "lng": -91.0478,
      "lat": 32.0081
    },
    "Gulfport Naval Base CSG PV System": {
      "lng": -89.119233,
      "lat": 30.379866
    },
    "Hattiesburg Solar Farm": {
      "lng": -89.301156,
      "lat": 31.275828
    },
    "Hinds Energy Facility": {
      "lng": -90.219906,
      "lat": 32.378969
    },
    "International Paper Vicksburg Mill": {
      "lng": -90.7742,
      "lat": 32.5292
    },
    "Kemper County": {
      "lng": -88.606,
      "lat": 32.7976
    },
    "L L Wilkins": {
      "lng": -90.562662,
      "lat": 34.1845
    },
    "Leaf River Cellulose LLC": {
      "lng": -89.0456,
      "lat": 31.2434
    },
    "Lineage MS": {
      "lng": -90.15538,
      "lat": 32.254379
    },
    "Magnolia Combined Cycle Plant": {
      "lng": -89.2026,
      "lat": 34.835
    },
    "Meridian": {
      "lng": -88.60669,
      "lat": 32.55078
    },
    "Meridian II": {
      "lng": -88.580396,
      "lat": 32.55332
    },
    "Meridian III": {
      "lng": -88.549484,
      "lat": 32.453955
    },
    "Mississippi Baptist Medical Center": {
      "lng": -90.1797,
      "lat": 32.3133
    },
    "Moselle Generation Complex": {
      "lng": -89.3004,
      "lat": 31.528
    },
    "Prairie Bluff": {
      "lng": -88.9925,
      "lat": 33.954444
    },
    "Red Hills Generation Facility": {
      "lng": -89.2183,
      "lat": 33.3761
    },
    "Silver Creek Generating Plant": {
      "lng": -89.9468,
      "lat": 31.6004
    },
    "Southaven Combined Cycle Plant": {
      "lng": -90.0378,
      "lat": 34.9939
    },
    "SR Houston": {
      "lng": -88.997563,
      "lat": 33.926666
    },
    "Sumrall I Solar Farm": {
      "lng": -89.49764,
      "lat": 31.403531
    },
    "Sumrall II Solar Farm": {
      "lng": -89.485884,
      "lat": 31.397089
    },
    "Sweatt Electric Generating Plant": {
      "lng": -88.7461,
      "lat": 32.2925
    },
    "Sylvarena Generating Plant": {
      "lng": -89.412714,
      "lat": 31.982076
    },
    "Watson Electric Generating Plant": {
      "lng": -89.0286,
      "lat": 30.4392
    },
    "Yazoo": {
      "lng": -90.4211,
      "lat": 32.8458
    },
    "Basin Creek Plant": {
      "lng": -112.5194,
      "lat": 45.9293
    },
    "Big Fork": {
      "lng": -114.0708,
      "lat": 48.0592
    },
    "Big Timber Wind Farm": {
      "lng": -109.649167,
      "lat": 45.855428
    },
    "Black Eagle": {
      "lng": -111.261442,
      "lat": 47.521331
    },
    "Black Eagle Solar, LLC": {
      "lng": -111.253974,
      "lat": 47.547466
    },
    "Broadwater Power Project": {
      "lng": -111.407656,
      "lat": 46.12061
    },
    "Canyon Ferry": {
      "lng": -111.727874,
      "lat": 46.649026
    },
    "Cochrane": {
      "lng": -111.194969,
      "lat": 47.5375
    },
    "Colstrip": {
      "lng": -106.614,
      "lat": 45.8831
    },
    "Colstrip Energy Limited Partnership": {
      "lng": -106.6547,
      "lat": 45.9752
    },
    "Culbertson Station": {
      "lng": -104.391667,
      "lat": 48.21
    },
    "Dave Gates Generating Station": {
      "lng": -112.876605,
      "lat": 46.104713
    },
    "Diamond Willow Wind Facility": {
      "lng": -104.1836,
      "lat": 46.2753
    },
    "Fairfield Wind": {
      "lng": -111.965278,
      "lat": 47.723333
    },
    "Flathead Landfill to Gas Energy Facility": {
      "lng": -114.342137,
      "lat": 48.313067
    },
    "Flint Creek Hydroelectric LLC": {
      "lng": -113.294722,
      "lat": 46.2275
    },
    "Fort Peck": {
      "lng": -106.4123,
      "lat": 48.0122
    },
    "Glendive Generating Station": {
      "lng": -104.74,
      "lat": 47.0539
    },
    "Gordon Butte Wind LLC": {
      "lng": -110.337777,
      "lat": 46.411944
    },
    "Great Divide Solar, LLC": {
      "lng": -112.249048,
      "lat": 46.805739
    },
    "Green Meadow Solar, LLC": {
      "lng": -112.063623,
      "lat": 46.694488
    },
    "Greenfield Wind - MT": {
      "lng": -111.963,
      "lat": 47.723
    },
    "Hardin Generating Station": {
      "lng": -107.6,
      "lat": 45.7578
    },
    "Hauser": {
      "lng": -111.887341,
      "lat": 46.765941
    },
    "Holter": {
      "lng": -112.0047,
      "lat": 46.9915
    },
    "Horseshoe Bend Wind Park": {
      "lng": -111.4392,
      "lat": 47.4983
    },
    "Hungry Horse": {
      "lng": -114.01443,
      "lat": 48.341521
    },
    "Judith Gap Wind Energy Center": {
      "lng": -109.7531,
      "lat": 46.5725
    },
    "Lewis & Clark": {
      "lng": -104.15665,
      "lat": 47.6785
    },
    "Libby": {
      "lng": -115.3143,
      "lat": 48.4098
    },
    "Madison Dam": {
      "lng": -111.633807,
      "lat": 45.487869
    },
    "Magpie Solar, LLC": {
      "lng": -108.937935,
      "lat": 46.291439
    },
    "Miles City GT": {
      "lng": -105.7953,
      "lat": 46.4112
    },
    "Morony": {
      "lng": -111.060898,
      "lat": 47.581737
    },
    "Musselshell Wind Project": {
      "lng": -109.482778,
      "lat": 46.275278
    },
    "Musselshell Wind Project Two LLC": {
      "lng": -109.482778,
      "lat": 46.275278
    },
    "NaturEner Glacier Wind Energy 1": {
      "lng": -112.1097,
      "lat": 48.51
    },
    "NaturEner Glacier Wind Energy 2": {
      "lng": -112.1847,
      "lat": 48.5333
    },
    "NaturEner Rim Rock Energy": {
      "lng": -112.103333,
      "lat": 48.820278
    },
    "Noxon Rapids": {
      "lng": -115.73361,
      "lat": 47.96048
    },
    "OREG 1 Inc": {
      "lng": -104.3975,
      "lat": 48.2139
    },
    "OREG 2 Inc": {
      "lng": -104.3975,
      "lat": 48.2142
    },
    "Phillips 66 Billings Refinery": {
      "lng": -108.4911,
      "lat": 45.7769
    },
    "Pryor Mountain Wind": {
      "lng": -108.693138,
      "lat": 45.165525
    },
    "Rainbow (MT)": {
      "lng": -111.2045,
      "lat": 47.5344
    },
    "Ryan": {
      "lng": -111.122546,
      "lat": 47.569939
    },
    "Selis Ksanka Qlispe": {
      "lng": -114.2339,
      "lat": 47.6777
    },
    "Sidney MT Plant": {
      "lng": -104.135556,
      "lat": 47.717222
    },
    "South Dry Creek Hydro": {
      "lng": -109.1652,
      "lat": 45.206341
    },
    "South Mills Solar, LLC": {
      "lng": -107.632624,
      "lat": 45.730113
    },
    "South Peak Wind": {
      "lng": -110.614592,
      "lat": 47.329911
    },
    "Spion Kop Wind Farm": {
      "lng": -110.639167,
      "lat": 47.333333
    },
    "Stillwater Wind, LLC": {
      "lng": -109.476647,
      "lat": 45.86584
    },
    "Stoltze CoGen1": {
      "lng": -114.2409,
      "lat": 48.3877
    },
    "Thompson Falls": {
      "lng": -115.358237,
      "lat": 47.593155
    },
    "Tiber Dam Hydroelectric Plant": {
      "lng": -111.1018,
      "lat": 48.318
    },
    "Turnbull Hydro": {
      "lng": -112.096944,
      "lat": 47.605278
    },
    "Two Dot Wind Broadview East LLC": {
      "lng": -108.806609,
      "lat": 46.10265
    },
    "Two Dot Wind Farm": {
      "lng": -110.087222,
      "lat": 46.449444
    },
    "Western Sugar Cooperative - Billings": {
      "lng": -108.499232,
      "lat": 45.765346
    },
    "Yellowstone Energy Limited Partnership": {
      "lng": -108.4278,
      "lat": 45.8117
    },
    "Yellowtail": {
      "lng": -107.957515,
      "lat": 45.307406
    },
    "1001 Ebenezer Church Solar": {
      "lng": -80.843419,
      "lat": 36.283819
    },
    "1008 Matthews Solar": {
      "lng": -80.552473,
      "lat": 36.21669
    },
    "1009 Yadkin Solar": {
      "lng": -80.656114,
      "lat": 36.14749
    },
    "1025 Traveller Solar, LLC": {
      "lng": -79.126319,
      "lat": 35.427308
    },
    "1034 Catherine Lake Solar, LLC": {
      "lng": -77.498759,
      "lat": 34.78471
    },
    "1045 Tomlin Mill Solar": {
      "lng": -80.835488,
      "lat": 35.916178
    },
    "1047 Little Mountain Solar, LLC": {
      "lng": -80.806725,
      "lat": 36.197067
    },
    "1051 Lucky Solar, LLC": {
      "lng": -80.819654,
      "lat": 36.148986
    },
    "1073 Onslow Solar": {
      "lng": -77.185771,
      "lat": 34.773578
    },
    "231 Dixon 74 Solar I, LLC": {
      "lng": -81.430385,
      "lat": 35.244125
    },
    "232 Long Branch 29 Solar I, LLC": {
      "lng": -81.559722,
      "lat": 35.23222
    },
    "315 Vinson Road": {
      "lng": -78.385888,
      "lat": 35.629398
    },
    "4Oaks": {
      "lng": -78.411667,
      "lat": 35.4325
    },
    "510 REPP One": {
      "lng": -77.6058,
      "lat": 36.4972
    },
    "ABD Farms": {
      "lng": -79.748273,
      "lat": 35.728738
    },
    "Achilles Solar": {
      "lng": -79.911423,
      "lat": 35.359674
    },
    "Acme Solar, LLC": {
      "lng": -78.996148,
      "lat": 34.324366
    },
    "AGA TAG Solar IV LLC": {
      "lng": -78.06444,
      "lat": 34.27355
    },
    "Ahoskie": {
      "lng": -76.993138,
      "lat": 36.306369
    },
    "Ajax Solar": {
      "lng": -79.1194,
      "lat": 34.83091
    },
    "Albemarle Beach Solar": {
      "lng": -76.620452,
      "lat": 35.928188
    },
    "Albemarle Hospital Unit": {
      "lng": -80.1939,
      "lat": 35.3636
    },
    "Albemarle Prime Power Park": {
      "lng": -80.151849,
      "lat": 35.407208
    },
    "Albemarle Solar Center LLC": {
      "lng": -77.556944,
      "lat": 35.293056
    },
    "Albertson Solar LLC": {
      "lng": -77.812667,
      "lat": 35.093021
    },
    "Alpha Value Solar": {
      "lng": -76.489563,
      "lat": 36.157401
    },
    "AM Best Farm": {
      "lng": -77.985556,
      "lat": 35.418889
    },
    "American Legion PV 1": {
      "lng": -77.655833,
      "lat": 36.418333
    },
    "Amethyst Solar": {
      "lng": -81.926389,
      "lat": 35.316944
    },
    "Anderson Farm LLC": {
      "lng": -78.516667,
      "lat": 34.975556
    },
    "Andrew Solar": {
      "lng": -77.1099,
      "lat": 35.1608
    },
    "Angel Solar": {
      "lng": -81.208611,
      "lat": 35.651111
    },
    "Angier Farm": {
      "lng": -78.75,
      "lat": 35.516389
    },
    "Apple Data Center - Fuel Cell 1&2": {
      "lng": -81.266667,
      "lat": 35.588333
    },
    "Apple Data Center - PV2": {
      "lng": -81.207778,
      "lat": 35.738889
    },
    "Apple Data Center PV": {
      "lng": -81.25198,
      "lat": 35.588135
    },
    "Apple Data Center PV3": {
      "lng": -81.15,
      "lat": 35.715
    },
    "Apple One": {
      "lng": -81.156944,
      "lat": 35.629444
    },
    "Ararat Rock Solar, LLC": {
      "lng": -80.600556,
      "lat": 36.461111
    },
    "Arba Solar, LLC": {
      "lng": -77.703611,
      "lat": 35.396389
    },
    "Arborgate Solar": {
      "lng": -79.11946,
      "lat": 35.717391
    },
    "Archer Daniels Midland Southport": {
      "lng": -77.9905,
      "lat": 33.9397
    },
    "Arndt Farm": {
      "lng": -81.175556,
      "lat": 35.779167
    },
    "Arthur Solar, LLC": {
      "lng": -78.84663,
      "lat": 34.16087
    },
    "Asheville": {
      "lng": -82.5417,
      "lat": 35.4731
    },
    "Asheville-Rock Hill Battery": {
      "lng": -82.517383,
      "lat": 35.54012
    },
    "Ashley Solar Farm": {
      "lng": -80.798889,
      "lat": 36.126111
    },
    "Aspen Solar, LLC": {
      "lng": -78.012122,
      "lat": 34.791163
    },
    "Atkinson Farm Solar": {
      "lng": -77.795169,
      "lat": 34.896953
    },
    "Atkinson Solar II": {
      "lng": -77.792,
      "lat": 34.897
    },
    "ATOOD Solar IV, LLC": {
      "lng": -78.061015,
      "lat": 34.27532
    },
    "Audrey Solar": {
      "lng": -81.621944,
      "lat": 35.328056
    },
    "Augustus Farm, LLC": {
      "lng": -77.639109,
      "lat": 35.599685
    },
    "Aulander Holloman Solar, LLC": {
      "lng": -77.066561,
      "lat": 36.29116
    },
    "Aulander Hwy 42 Solar, LLC": {
      "lng": -77.085241,
      "lat": 36.248563
    },
    "Auten Road Farm, LLC": {
      "lng": -81.13735,
      "lat": 35.30498
    },
    "Avalon Hydropower": {
      "lng": -79.952494,
      "lat": 36.423715
    },
    "Ayrshire": {
      "lng": -81.615278,
      "lat": 35.283056
    },
    "Badger": {
      "lng": -77.228776,
      "lat": 34.83896
    },
    "Bailey Farm LLC": {
      "lng": -78.139722,
      "lat": 35.7875
    },
    "Bakatsias Solar": {
      "lng": -79.347471,
      "lat": 36.072359
    },
    "Baker PV 1": {
      "lng": -77.060278,
      "lat": 36.270278
    },
    "Balsam": {
      "lng": -79.173889,
      "lat": 35.458889
    },
    "Baltimore Church Solar, LLC": {
      "lng": -79.152443,
      "lat": 34.466688
    },
    "Barker Solar, LLC": {
      "lng": -78.928657,
      "lat": 34.635599
    },
    "Barnhill Road Solar, LLC": {
      "lng": -76.299469,
      "lat": 36.269175
    },
    "Battleboro Farm": {
      "lng": -77.76077,
      "lat": 36.05864
    },
    "Battleground": {
      "lng": -81.33,
      "lat": 35.22
    },
    "Bay Branch Solar": {
      "lng": -77.1191,
      "lat": 35.4745
    },
    "Bay Tree": {
      "lng": -78.393358,
      "lat": 34.70414
    },
    "Bayboro Solar Farm": {
      "lng": -76.810226,
      "lat": 35.148339
    },
    "Beaker": {
      "lng": -77.609438,
      "lat": 35.597904
    },
    "Bear Creek Dam": {
      "lng": -83.072007,
      "lat": 35.242677
    },
    "Bearford Farm Solar Project": {
      "lng": -77.924167,
      "lat": 34.61
    },
    "Bearford Solar II": {
      "lng": -77.928889,
      "lat": 34.611944
    },
    "BearPond Solar Center LLC": {
      "lng": -78.377222,
      "lat": 36.276111
    },
    "Beaufort Solar, LLC": {
      "lng": -77.134,
      "lat": 35.456
    },
    "Beetle Solar": {
      "lng": -81.665,
      "lat": 35.1785
    },
    "Belafonte": {
      "lng": -78.090148,
      "lat": 35.161492
    },
    "Belews Creek": {
      "lng": -80.0603,
      "lat": 36.2811
    },
    "Belwood Farm": {
      "lng": -81.508333,
      "lat": 35.47
    },
    "Benson Solar, LLC": {
      "lng": -78.56223,
      "lat": 35.3962
    },
    "Benthall Bridge PV 1": {
      "lng": -77.100278,
      "lat": 36.42
    },
    "Bernhardt Furniture Solar Farm": {
      "lng": -81.540861,
      "lat": 35.84995
    },
    "Beth": {
      "lng": -79.618368,
      "lat": 35.975586
    },
    "Bethel Price Solar, LLC": {
      "lng": -77.381111,
      "lat": 35.794722
    },
    "Bethel Solar": {
      "lng": -77.384542,
      "lat": 35.794328
    },
    "Beulaville": {
      "lng": -77.78,
      "lat": 34.9
    },
    "BG Stewart Solar Farm, LLC": {
      "lng": -80.389622,
      "lat": 34.970486
    },
    "Big Boy": {
      "lng": -79.633767,
      "lat": 36.07767
    },
    "Biltmore Solar Fields": {
      "lng": -82.58844,
      "lat": 35.561156
    },
    "Biscoe Solar LLC": {
      "lng": -79.770278,
      "lat": 35.383889
    },
    "Bizzell Church Solar 1, LLC": {
      "lng": -78.179859,
      "lat": 35.549401
    },
    "Bizzell Church Solar 2": {
      "lng": -78.177204,
      "lat": 35.544405
    },
    "Blackburn Landfill Co-Generation": {
      "lng": -81.3006,
      "lat": 35.6067
    },
    "Bladen Solar": {
      "lng": -78.62345,
      "lat": 34.823522
    },
    "Bladenboro Farm": {
      "lng": -78.813056,
      "lat": 34.535
    },
    "Bladenboro Solar 2": {
      "lng": -78.799,
      "lat": 34.536
    },
    "Bladenboro Solar, LLC": {
      "lng": -78.817652,
      "lat": 34.547761
    },
    "Blewett": {
      "lng": -79.8775,
      "lat": 34.9833
    },
    "Blue Ridge Paper Products Inc.": {
      "lng": -82.8411,
      "lat": 35.535
    },
    "Blueberry One": {
      "lng": -78.154167,
      "lat": 35.313889
    },
    "Bo Biggs Solar": {
      "lng": -78.94951,
      "lat": 34.58125
    },
    "Boaz Farm Solar": {
      "lng": -79.13444,
      "lat": 35.46777
    },
    "Bolton Farm": {
      "lng": -78.132311,
      "lat": 36.421617
    },
    "Bondi Solar": {
      "lng": -77.53849,
      "lat": 35.134054
    },
    "Boseman Solar Center LLC": {
      "lng": -77.81,
      "lat": 36.008056
    },
    "Bostic Delivery No 2": {
      "lng": -81.8344,
      "lat": 35.3619
    },
    "Boykin PV1": {
      "lng": -78.750423,
      "lat": 35.113386
    },
    "Bradley PV1": {
      "lng": -77.076389,
      "lat": 36.260556
    },
    "Brantley Solar": {
      "lng": -78.155153,
      "lat": 35.890485
    },
    "BRE": {
      "lng": -78.598507,
      "lat": 36.372546
    },
    "BRE NC Solar 3": {
      "lng": -78.612,
      "lat": 36.383
    },
    "Bridgewater": {
      "lng": -81.8372,
      "lat": 35.7428
    },
    "Broad River Solar, LLC": {
      "lng": -81.751,
      "lat": 35.188
    },
    "Broadridge Solar, LLC": {
      "lng": -78.957206,
      "lat": 34.522942
    },
    "Broadway Road Solar, LLC": {
      "lng": -79.095952,
      "lat": 34.458665
    },
    "Broadway Solar Center, LLC": {
      "lng": -78.831433,
      "lat": 34.309261
    },
    "Brunswick Nuclear": {
      "lng": -78.0114,
      "lat": 33.9597
    },
    "Buck": {
      "lng": -80.3767,
      "lat": 35.7133
    },
    "Buckleberry Solar": {
      "lng": -77.405992,
      "lat": 35.380683
    },
    "Buddy Solar": {
      "lng": -80.363333,
      "lat": 35.541944
    },
    "Bullock Solar, LLC": {
      "lng": -78.315078,
      "lat": 36.475369
    },
    "Bunn Level Farm, LLC": {
      "lng": -78.774331,
      "lat": 35.308639
    },
    "Burgaw Solar, LLC": {
      "lng": -77.911678,
      "lat": 34.536
    },
    "Butler-Warner Generation Plant": {
      "lng": -78.8294,
      "lat": 35.0986
    },
    "Buttercup Solar, LLC": {
      "lng": -77.97275,
      "lat": 34.82354
    },
    "Buxton": {
      "lng": -75.534722,
      "lat": 35.267222
    },
    "Camden Dam Solar, LLC": {
      "lng": -76.125847,
      "lat": 36.311822
    },
    "Camden Solar LLC": {
      "lng": -76.144486,
      "lat": 36.30786
    },
    "Camp Lejeune Solar": {
      "lng": -77.350273,
      "lat": 34.725699
    },
    "Candace Solar": {
      "lng": -78.1922,
      "lat": 35.487
    },
    "Carl Friedrich Gauss Solar": {
      "lng": -77.633477,
      "lat": 36.345842
    },
    "Carol Jean Solar": {
      "lng": -80.144444,
      "lat": 36.316111
    },
    "Carolina Lily Solar": {
      "lng": -80.444,
      "lat": 35.956
    },
    "Carolina Poultry Power Farmville": {
      "lng": -77.614507,
      "lat": 35.591719
    },
    "Carter PV1": {
      "lng": -78.7875,
      "lat": 35.021111
    },
    "Cash Solar": {
      "lng": -77.923306,
      "lat": 36.02818
    },
    "Castalia Solar": {
      "lng": -77.783333,
      "lat": 36.0939
    },
    "Catawba River Pollution Control": {
      "lng": -81.6643,
      "lat": 35.7766
    },
    "Catawba Solar LLC": {
      "lng": -80.53,
      "lat": 35.61
    },
    "CB Bladen Solar, LLC": {
      "lng": -78.624278,
      "lat": 34.822869
    },
    "CBC Solar Energy Gen Fac Phase 2": {
      "lng": -78.526111,
      "lat": 35.674444
    },
    "Cedar Cliff": {
      "lng": -83.0983,
      "lat": 35.2531
    },
    "Cedar Solar, LLC": {
      "lng": -77.775743,
      "lat": 34.915767
    },
    "Century Drive Solar Farm": {
      "lng": -78.6689,
      "lat": 35.837841
    },
    "CenturyLink Regional HQ": {
      "lng": -78.516,
      "lat": 36.0205
    },
    "Chadbourn Farm": {
      "lng": -78.764901,
      "lat": 34.324656
    },
    "Changeup": {
      "lng": -79.78553,
      "lat": 35.74901
    },
    "Charlie": {
      "lng": -79.60948,
      "lat": 35.97485
    },
    "Charlotte Solar": {
      "lng": -81.821667,
      "lat": 35.261944
    },
    "Chatuge": {
      "lng": -83.791438,
      "lat": 35.019131
    },
    "Chauncey Farm LLC": {
      "lng": -78.521944,
      "lat": 34.3375
    },
    "Chei Solar": {
      "lng": -78.1823,
      "lat": 35.8918
    },
    "Cheoah": {
      "lng": -83.9383,
      "lat": 35.4478
    },
    "Cherryville City Hall": {
      "lng": -81.38,
      "lat": 35.3778
    },
    "Chestnut Solar": {
      "lng": -77.712,
      "lat": 36.18
    },
    "Choco Solar, LLC": {
      "lng": -77.113067,
      "lat": 35.482032
    },
    "Chocowinity Solar LLC": {
      "lng": -77.09,
      "lat": 35.515
    },
    "Chowan Jehu Solar": {
      "lng": -76.602304,
      "lat": 36.283778
    },
    "Church Road Solar LLC": {
      "lng": -78.625352,
      "lat": 35.523663
    },
    "CII Methane Management IV, LLC": {
      "lng": -78.433794,
      "lat": 35.511903
    },
    "Cirrus Solar LLC": {
      "lng": -78.265,
      "lat": 35.969444
    },
    "Clear Solar I, LLC": {
      "lng": -81.841826,
      "lat": 35.308314
    },
    "Cleveland County Generating Facility": {
      "lng": -81.416648,
      "lat": 35.170547
    },
    "Cliffside": {
      "lng": -81.7594,
      "lat": 35.22
    },
    "Cline Solar Farm, LLC": {
      "lng": -81.417,
      "lat": 35.72
    },
    "Clipperton Holdings LLC": {
      "lng": -78.354444,
      "lat": 34.966944
    },
    "Coats Solar Farm, LLC": {
      "lng": -78.690576,
      "lat": 35.414507
    },
    "COC Surry LFG, LLC": {
      "lng": -80.568333,
      "lat": 36.438611
    },
    "Cohen Farm Solar, LLC": {
      "lng": -79.195789,
      "lat": 35.557224
    },
    "Concord Energy": {
      "lng": -80.670385,
      "lat": 35.34598
    },
    "Concord Farm": {
      "lng": -80.663641,
      "lat": 35.444235
    },
    "Conetoe II Solar, LLC": {
      "lng": -77.481,
      "lat": 35.822
    },
    "Conetoe Solar": {
      "lng": -77.457772,
      "lat": 35.825182
    },
    "Cookstown": {
      "lng": -77.973754,
      "lat": 35.436844
    },
    "Copperfield": {
      "lng": -79.991158,
      "lat": 35.340067
    },
    "Cork Oak Solar": {
      "lng": -77.571,
      "lat": 36.371
    },
    "Cornelius Delivery No 1": {
      "lng": -80.856916,
      "lat": 35.483274
    },
    "Cornwall Solar Center, LLC": {
      "lng": -78.626111,
      "lat": 36.366944
    },
    "Cotten Farm, LLC": {
      "lng": -79.198481,
      "lat": 35.528489
    },
    "Cottonwood Solar, LLC": {
      "lng": -77.61444,
      "lat": 36.50806
    },
    "Cougar Solar, LLC": {
      "lng": -79.180654,
      "lat": 35.462405
    },
    "County Home Solar Center, LLC": {
      "lng": -81.508684,
      "lat": 35.263255
    },
    "County Home Solar LLC": {
      "lng": -79.722852,
      "lat": 34.925452
    },
    "Cowans Ford": {
      "lng": -80.9588,
      "lat": 35.4346
    },
    "CPI USA North Carolina Roxboro": {
      "lng": -78.9619,
      "lat": 36.435
    },
    "CPI USA North Carolina Southport": {
      "lng": -78.011824,
      "lat": 33.944384
    },
    "Craven County Wood Energy": {
      "lng": -77.167883,
      "lat": 35.130193
    },
    "Creech Solar 2, LLC": {
      "lng": -78.60434,
      "lat": 35.72196
    },
    "Crestwood Solar Center LLC": {
      "lng": -78.584212,
      "lat": 36.353533
    },
    "Creswell Alligood Solar, LLC": {
      "lng": -76.379444,
      "lat": 35.898889
    },
    "Crockett Farm": {
      "lng": -77.5525,
      "lat": 35.230833
    },
    "Crooked Run": {
      "lng": -77.977497,
      "lat": 34.67704
    },
    "Cruise Solar, LLC": {
      "lng": -77.543559,
      "lat": 34.69193
    },
    "CS Murphy Point, LLC": {
      "lng": -84.166197,
      "lat": 34.989539
    },
    "Cubera Solar, LLC": {
      "lng": -78.610091,
      "lat": 35.330022
    },
    "Currin Solar, LLC": {
      "lng": -78.633283,
      "lat": 36.289625
    },
    "Dan River": {
      "lng": -79.7208,
      "lat": 36.4862
    },
    "Daniel Farm LLC": {
      "lng": -80.553611,
      "lat": 35.848889
    },
    "Davidson Gas Producers LLC": {
      "lng": -80.183458,
      "lat": 35.841299
    },
    "Davis Lane Solar, LLC": {
      "lng": -76.52924,
      "lat": 36.107345
    },
    "Daystar Solar": {
      "lng": -77.202453,
      "lat": 35.035589
    },
    "DD Fayetteville Solar NC LLC": {
      "lng": -78.843611,
      "lat": 34.834444
    },
    "DE Solar 10240 Old Dowd Rd": {
      "lng": -80.9967,
      "lat": 35.2458
    },
    "DE Solar 1725 Drywall Dr": {
      "lng": -80.9967,
      "lat": 35.3261
    },
    "DE Solar 657 Brigham Rd": {
      "lng": -79.9692,
      "lat": 36.1053
    },
    "Deep Branch Farm": {
      "lng": -79.199375,
      "lat": 34.670842
    },
    "Delco Farm": {
      "lng": -78.20905,
      "lat": 34.318997
    },
    "Dement Farm LLC": {
      "lng": -78.348333,
      "lat": 36.27
    },
    "Desert Wind Farm, LLC": {
      "lng": -76.42,
      "lat": 36.31
    },
    "Dessie Solar Center LLC": {
      "lng": -78.835833,
      "lat": 34.302778
    },
    "Dibrell Farm": {
      "lng": -79.528056,
      "lat": 36.471667
    },
    "Dixon Dairy Road Solar": {
      "lng": -81.410833,
      "lat": 35.199167
    },
    "Dogwood Solar, LLC": {
      "lng": -77.409167,
      "lat": 36.125
    },
    "Domtar Paper Co LLC Plymouth NC": {
      "lng": -76.7831,
      "lat": 35.8628
    },
    "Downs Farm Solar": {
      "lng": -76.916944,
      "lat": 36.361944
    },
    "Dragstrip Farm": {
      "lng": -81.416111,
      "lat": 35.758056
    },
    "Drexel Operations Center": {
      "lng": -81.6072,
      "lat": 35.7481
    },
    "DSM Pharmaceuticals": {
      "lng": -77.35744,
      "lat": 35.665961
    },
    "Dunn": {
      "lng": -78.548611,
      "lat": 35.475
    },
    "Duplin Solar I LLC (160 Houston Lane)": {
      "lng": -77.980388,
      "lat": 34.960083
    },
    "Duplin Solar LLC": {
      "lng": -78.078333,
      "lat": 35.011944
    },
    "Durham Solar": {
      "lng": -78.848475,
      "lat": 36.004941
    },
    "Eagle Solar": {
      "lng": -78.155087,
      "lat": 36.458327
    },
    "East Wayne Solar LLC": {
      "lng": -77.85,
      "lat": 35.441667
    },
    "Eastover Farm": {
      "lng": -79.463056,
      "lat": 34.742222
    },
    "Eastside WWTP": {
      "lng": -79.9089,
      "lat": 35.9428
    },
    "Eden Solar LLC": {
      "lng": -79.636372,
      "lat": 35.148503
    },
    "Edenton Generators": {
      "lng": -76.571944,
      "lat": 36.028611
    },
    "Edenton Solar": {
      "lng": -76.544,
      "lat": 36.054
    },
    "Ellerbe": {
      "lng": -79.73,
      "lat": 35.1
    },
    "Elliana Solar": {
      "lng": -79.3325,
      "lat": 36.131111
    },
    "Elm City Solar Facility": {
      "lng": -77.846944,
      "lat": 35.781111
    },
    "Elm Solar, LLC": {
      "lng": -79.189525,
      "lat": 35.453828
    },
    "Ennis Solar, LLC": {
      "lng": -78.721683,
      "lat": 35.421964
    },
    "Eros Solar, LLC": {
      "lng": -78.08025,
      "lat": 35.16513
    },
    "Erwin Farm": {
      "lng": -78.661389,
      "lat": 35.333333
    },
    "ESA Boston Solar, LLC": {
      "lng": -78.921771,
      "lat": 36.486008
    },
    "ESA Buies Creek, LLC": {
      "lng": -78.71433,
      "lat": 35.41424
    },
    "ESA Four Oaks 2 NC LLC": {
      "lng": -78.409231,
      "lat": 35.416104
    },
    "ESA Hamlet NC , LLC": {
      "lng": -79.673277,
      "lat": 34.879581
    },
    "ESA Selma": {
      "lng": -78.2898,
      "lat": 35.5262
    },
    "ESA Smithfield": {
      "lng": -78.319751,
      "lat": 35.48835
    },
    "Everett PV1": {
      "lng": -77.30957,
      "lat": 35.821137
    },
    "Everetts Wildcat Solar, LLC": {
      "lng": -77.111389,
      "lat": 35.847222
    },
    "Exum Farm Solar, LLC": {
      "lng": -77.4549,
      "lat": 35.370441
    },
    "Facile Solar": {
      "lng": -79.493889,
      "lat": 36.098889
    },
    "Fairmont-FLS 100": {
      "lng": -79.13,
      "lat": 34.52
    },
    "Faison Solar": {
      "lng": -78.116111,
      "lat": 35.069722
    },
    "Falls Hydro": {
      "lng": -80.0753,
      "lat": 35.3944
    },
    "Farrington Farm": {
      "lng": -79.025,
      "lat": 35.832778
    },
    "Fern Solar LLC": {
      "lng": -77.696443,
      "lat": 36.003272
    },
    "Fisher Solar Farm - NC": {
      "lng": -80.03465,
      "lat": 35.24125
    },
    "Five Forks Solar": {
      "lng": -78.033704,
      "lat": 36.515544
    },
    "Flash Solar": {
      "lng": -81.229444,
      "lat": 35.758056
    },
    "Flat Meeks PV 1": {
      "lng": -77.270556,
      "lat": 35.806667
    },
    "Flatwood Farm": {
      "lng": -79.0323,
      "lat": 35.588
    },
    "Flemming Solar Center LLC": {
      "lng": -77.383889,
      "lat": 35.668889
    },
    "Flint Hill Solar, LLC": {
      "lng": -79.55308,
      "lat": 35.40274
    },
    "Flowers Solar LLC": {
      "lng": -78.603088,
      "lat": 35.41218
    },
    "Floyd Road Solar Farm": {
      "lng": -77.677474,
      "lat": 36.50592
    },
    "Floyd Solar, LLC": {
      "lng": -79.135875,
      "lat": 34.509128
    },
    "FLS Solar 170, LLC": {
      "lng": -78.97,
      "lat": 34.6
    },
    "FLS Solar 200, LLC": {
      "lng": -78.94,
      "lat": 34.581944
    },
    "FLS Solar 230 (Warren)": {
      "lng": -77.821944,
      "lat": 35.016111
    },
    "Fontana Dam": {
      "lng": -83.805,
      "lat": 35.4507
    },
    "Fox Creek Solar": {
      "lng": -78.273106,
      "lat": 36.126912
    },
    "Foxfire Solar Farm": {
      "lng": -79.551944,
      "lat": 35.220556
    },
    "Franklin (NC)": {
      "lng": -83.3708,
      "lat": 35.2194
    },
    "Franklin Solar 2": {
      "lng": -78.1525,
      "lat": 36.126944
    },
    "Franklin Solar, LLC": {
      "lng": -78.167778,
      "lat": 36.0975
    },
    "Franklinton Solar": {
      "lng": -78.35,
      "lat": 36.08
    },
    "Freemont Solar Center LLC": {
      "lng": -80.997219,
      "lat": 35.427587
    },
    "Freight Line Solar, LLC": {
      "lng": -77.76236,
      "lat": 34.96885
    },
    "Fremont Farm LLC": {
      "lng": -77.969167,
      "lat": 35.531944
    },
    "Fuquay Farm": {
      "lng": -78.710261,
      "lat": 35.564354
    },
    "G G Allen": {
      "lng": -81.0122,
      "lat": 35.1897
    },
    "Gainey Solar, LLC": {
      "lng": -78.653,
      "lat": 34.499
    },
    "Gamble Solar": {
      "lng": -81.817362,
      "lat": 35.348375
    },
    "Garrell Solar Farm": {
      "lng": -78.793056,
      "lat": 34.325278
    },
    "Garysburg Solar": {
      "lng": -77.592,
      "lat": 36.468
    },
    "Gaston": {
      "lng": -77.811543,
      "lat": 36.499147
    },
    "Gaston County Renewable Energy Center": {
      "lng": -81.172,
      "lat": 35.3857
    },
    "Gaston Memorial Hospital": {
      "lng": -81.1383,
      "lat": 35.2747
    },
    "Gaston Solar": {
      "lng": -77.656111,
      "lat": 36.514722
    },
    "Gaston Solar Power Plant": {
      "lng": -81.1343,
      "lat": 35.3224
    },
    "Gastonia Prime Power Park": {
      "lng": -81.2092,
      "lat": 35.3017
    },
    "Gastonia Solar Center": {
      "lng": -81.189078,
      "lat": 35.229033
    },
    "Gastonia, Tulip Drive": {
      "lng": -81.205219,
      "lat": 35.28914
    },
    "Gates Solar LLC": {
      "lng": -76.803611,
      "lat": 36.450833
    },
    "Germantown Solar, LLC": {
      "lng": -80.25463,
      "lat": 36.24634
    },
    "Gilead": {
      "lng": -80.013247,
      "lat": 35.198537
    },
    "GKS Solar": {
      "lng": -76.501944,
      "lat": 36.248333
    },
    "Gladstone Farm": {
      "lng": -79.557136,
      "lat": 35.213491
    },
    "Gliden (Op Zone)": {
      "lng": -76.60357,
      "lat": 36.30161
    },
    "Graham Solar Center LLC": {
      "lng": -79.014444,
      "lat": 34.309722
    },
    "Grandy PV 1": {
      "lng": -75.880833,
      "lat": 36.233611
    },
    "Granite Falls Walmart": {
      "lng": -81.3928,
      "lat": 35.7811
    },
    "Granville Solar PV Power Project": {
      "lng": -78.593889,
      "lat": 36.411667
    },
    "Green Farm": {
      "lng": -77.269722,
      "lat": 35.831667
    },
    "Grissom Solar, LLC": {
      "lng": -77.897552,
      "lat": 36.23891
    },
    "Grove Solar": {
      "lng": -77.1182,
      "lat": 35.16922
    },
    "GRS CMS": {
      "lng": -80.668024,
      "lat": 35.353269
    },
    "Gutenberg Solar": {
      "lng": -77.534825,
      "lat": 36.486428
    },
    "H F Lee Steam Electric Plant": {
      "lng": -78.089444,
      "lat": 35.373611
    },
    "Halifax": {
      "lng": -78.9236,
      "lat": 36.7817
    },
    "Hanover Solar, LLC": {
      "lng": -77.213772,
      "lat": 34.800947
    },
    "Happy Solar": {
      "lng": -78.5108,
      "lat": 35.3965
    },
    "Hardison Farm Solar, LLC": {
      "lng": -77.02194,
      "lat": 35.83079
    },
    "Harrell's Hill Solar Center LLC": {
      "lng": -78.100833,
      "lat": 35.211944
    },
    "Harris": {
      "lng": -78.9556,
      "lat": 35.6334
    },
    "Harrison Solar, LLC": {
      "lng": -78.524535,
      "lat": 34.984262
    },
    "Harts Mill Solar, LLC": {
      "lng": -77.64,
      "lat": 35.9
    },
    "Harvest Beulaville, LLC": {
      "lng": -77.784167,
      "lat": 34.90389
    },
    "Haw River Hydro": {
      "lng": -79.3244,
      "lat": 35.9483
    },
    "Hawtree Solar": {
      "lng": -78.105,
      "lat": 36.533
    },
    "Hayes Solar, LLC": {
      "lng": -80.737407,
      "lat": 36.388216
    },
    "Haynes Farm": {
      "lng": -81.235278,
      "lat": 35.525833
    },
    "Haywood Farm Solar, LLC": {
      "lng": -79.915,
      "lat": 35.343
    },
    "HCE Johnston I, LLC": {
      "lng": -78.59453,
      "lat": 35.560002
    },
    "HCE Moore I": {
      "lng": -79.419519,
      "lat": 35.13432
    },
    "Hector Farm": {
      "lng": -79.909719,
      "lat": 35.376236
    },
    "Heedeh Solar": {
      "lng": -78.221475,
      "lat": 34.308744
    },
    "Hemlock Solar": {
      "lng": -77.597517,
      "lat": 36.469089
    },
    "Henry Farm": {
      "lng": -79.577907,
      "lat": 35.859618
    },
    "Hertford Solar Farm": {
      "lng": -76.490915,
      "lat": 36.13026
    },
    "Hew Fulton Farm, LLC": {
      "lng": -79.474542,
      "lat": 34.784039
    },
    "Hickory": {
      "lng": -78.009167,
      "lat": 34.802222
    },
    "High Point, Fairfield": {
      "lng": -79.990124,
      "lat": 35.924393
    },
    "High Point, POLO": {
      "lng": -79.96,
      "lat": 35.97
    },
    "High Point, Pump Station Rd": {
      "lng": -79.9497,
      "lat": 35.9861
    },
    "High Rock Hydro": {
      "lng": -80.2339,
      "lat": 35.6008
    },
    "High Shoals Hydro (NC)": {
      "lng": -81.201002,
      "lat": 35.394254
    },
    "Highland Solar Center LLC": {
      "lng": -77.451944,
      "lat": 35.363889
    },
    "Highwater Solar I": {
      "lng": -77.99,
      "lat": 35.433889
    },
    "Hiwassee Dam": {
      "lng": -84.177533,
      "lat": 35.150864
    },
    "Holly Swamp Solar, LLC": {
      "lng": -79.132926,
      "lat": 34.662627
    },
    "Holstein Plant": {
      "lng": -79.340833,
      "lat": 34.774167
    },
    "Hood Farm Solar, LLC": {
      "lng": -77.497,
      "lat": 35.205
    },
    "Hopewell Friends": {
      "lng": -79.8727,
      "lat": 35.62845
    },
    "Howell Midland Farm, LLC": {
      "lng": -80.543086,
      "lat": 35.225669
    },
    "Huntersville Delivery No 2": {
      "lng": -80.832879,
      "lat": 35.383222
    },
    "Husky Solar": {
      "lng": -79.5667,
      "lat": 36.2847
    },
    "Hutchinson Farm": {
      "lng": -81.515833,
      "lat": 35.260833
    },
    "HWY 158 PV": {
      "lng": -77.073839,
      "lat": 36.423222
    },
    "HXNAir Solar One": {
      "lng": -77.631511,
      "lat": 36.348734
    },
    "Ingredion Winston Salem": {
      "lng": -80.227404,
      "lat": 36.033761
    },
    "Innovative Solar 10": {
      "lng": -82.890319,
      "lat": 35.473629
    },
    "Innovative Solar 14, LLC": {
      "lng": -82.3575,
      "lat": 35.368889
    },
    "Innovative Solar 15, LLC": {
      "lng": -81.764167,
      "lat": 35.319444
    },
    "Innovative Solar 16": {
      "lng": -82.350001,
      "lat": 35.363863
    },
    "Innovative Solar 18, LLC": {
      "lng": -81.795051,
      "lat": 35.378187
    },
    "Innovative Solar 23": {
      "lng": -81.419019,
      "lat": 35.259825
    },
    "Innovative Solar 26, LLC": {
      "lng": -81.413308,
      "lat": 35.70974
    },
    "Innovative Solar 31": {
      "lng": -78.755394,
      "lat": 34.539425
    },
    "Innovative Solar 35, LLC": {
      "lng": -77.825,
      "lat": 35.046
    },
    "Innovative Solar 37 LLC": {
      "lng": -79.943889,
      "lat": 34.838297
    },
    "Innovative Solar 42": {
      "lng": -78.877359,
      "lat": 34.847627
    },
    "Innovative Solar 43, LLC": {
      "lng": -78.304811,
      "lat": 34.529956
    },
    "Innovative Solar 44": {
      "lng": -79.108,
      "lat": 34.859
    },
    "Innovative Solar 46": {
      "lng": -78.94355,
      "lat": 34.918194
    },
    "Innovative Solar 47": {
      "lng": -79.332447,
      "lat": 34.801933
    },
    "Innovative Solar 48": {
      "lng": -79.30235,
      "lat": 34.711628
    },
    "Innovative Solar 54": {
      "lng": -77.712408,
      "lat": 35.213697
    },
    "Innovative Solar 55": {
      "lng": -79.216,
      "lat": 34.695
    },
    "Innovative Solar 59, LLC": {
      "lng": -77.97928,
      "lat": 36.094719
    },
    "Innovative Solar 6": {
      "lng": -82.710278,
      "lat": 35.6575
    },
    "Innovative Solar 60, LLC": {
      "lng": -77.992,
      "lat": 36.091
    },
    "Innovative Solar 63, LLC": {
      "lng": -77.620395,
      "lat": 35.39169
    },
    "Innovative Solar 64": {
      "lng": -78.329675,
      "lat": 35.041048
    },
    "Innovative Solar 65": {
      "lng": -79.5,
      "lat": 34.7587
    },
    "Innovative Solar 67": {
      "lng": -78.005377,
      "lat": 34.623309
    },
    "International Paper Riegelwood Mill": {
      "lng": -78.2137,
      "lat": 34.3533
    },
    "Iredell County LFG Facility": {
      "lng": -80.826001,
      "lat": 35.769281
    },
    "Ivy River Hydro": {
      "lng": -82.61827,
      "lat": 35.771627
    },
    "Jackson Solar Farm": {
      "lng": -77.83,
      "lat": 35.75
    },
    "Jacob Solar": {
      "lng": -79.6214,
      "lat": 36.2922
    },
    "Jakana Solar": {
      "lng": -77.081111,
      "lat": 35.875278
    },
    "Jamesville Road Solar, LLC": {
      "lng": -76.801491,
      "lat": 35.845354
    },
    "Jersey Holdings": {
      "lng": -79.037483,
      "lat": 34.564307
    },
    "Jester Solar": {
      "lng": -77.8861,
      "lat": 35.36118
    },
    "Jordan Hydroelectric Project": {
      "lng": -79.068333,
      "lat": 35.654722
    },
    "Kalish Farm Solar": {
      "lng": -79.720544,
      "lat": 35.308176
    },
    "Kathleen Solar": {
      "lng": -78.802384,
      "lat": 35.513769
    },
    "Keen Farm": {
      "lng": -78.399983,
      "lat": 35.426667
    },
    "Kelford": {
      "lng": -77.218502,
      "lat": 36.154227
    },
    "Kelly Solar, LLC": {
      "lng": -78.449024,
      "lat": 36.391194
    },
    "Kenansville": {
      "lng": -78.04,
      "lat": 34.96
    },
    "Kenansville Solar 2, LLC": {
      "lng": -77.981667,
      "lat": 34.957778
    },
    "Kenansville Solar Farm, LLC": {
      "lng": -77.976111,
      "lat": 34.97
    },
    "Kendall Farm": {
      "lng": -79.555024,
      "lat": 35.850341
    },
    "Kennedy Solar, LLC": {
      "lng": -78.092185,
      "lat": 35.012282
    },
    "Kenneth Solar": {
      "lng": -78.1526,
      "lat": 36.4338
    },
    "Kings Mountain Energy Center": {
      "lng": -81.363056,
      "lat": 35.2025
    },
    "Kinston": {
      "lng": -77.608333,
      "lat": 35.370556
    },
    "Kinston Davis Farm": {
      "lng": -77.64259,
      "lat": 35.2184
    },
    "Kinston Solar": {
      "lng": -77.472283,
      "lat": 35.351436
    },
    "Kirkwall Holdings": {
      "lng": -78.1375,
      "lat": 34.988889
    },
    "Kojak Farm": {
      "lng": -78.102178,
      "lat": 35.79915
    },
    "L V Sutton": {
      "lng": -77.985278,
      "lat": 34.283056
    },
    "Lafayette Solar I, LLC": {
      "lng": -81.564248,
      "lat": 35.232396
    },
    "Lake Lure": {
      "lng": -82.184005,
      "lat": 35.42591
    },
    "Landis Delivery No 2": {
      "lng": -80.612301,
      "lat": 35.541982
    },
    "Lane II Solar, LLC": {
      "lng": -78.05327,
      "lat": 35.39977
    },
    "Lane Solar": {
      "lng": -78.051,
      "lat": 35.395
    },
    "Lang Solar Farm": {
      "lng": -77.502759,
      "lat": 35.19587
    },
    "Langdon Solar Farm, LLC": {
      "lng": -78.413481,
      "lat": 35.447278
    },
    "Langley PV1": {
      "lng": -77.848889,
      "lat": 35.801944
    },
    "Lanier Solar": {
      "lng": -77.808889,
      "lat": 34.898889
    },
    "Laurinburg Farm": {
      "lng": -79.442969,
      "lat": 34.764403
    },
    "Laurinburg Solar": {
      "lng": -79.430243,
      "lat": 34.756481
    },
    "Laurinburg Solar, LLC (Heelstone)": {
      "lng": -79.29538,
      "lat": 34.707644
    },
    "Leggett Solar, LLC": {
      "lng": -77.623,
      "lat": 36.058
    },
    "Lenoir Farm": {
      "lng": -77.666389,
      "lat": 35.331667
    },
    "Lenoir Farm 2": {
      "lng": -77.475106,
      "lat": 35.350081
    },
    "Lewiston Solar": {
      "lng": -77.211111,
      "lat": 36.141111
    },
    "Lexington": {
      "lng": -80.2594,
      "lat": 35.7736
    },
    "Lexington Health Center": {
      "lng": -80.242154,
      "lat": 35.830877
    },
    "Lillington Solar": {
      "lng": -78.801,
      "lat": 35.368
    },
    "Lincoln Combustion Turbine": {
      "lng": -81.0347,
      "lat": 35.4317
    },
    "Lincoln Solar, LLC (NC)": {
      "lng": -81.817582,
      "lat": 35.379505
    },
    "Lincolnton High School": {
      "lng": -81.262259,
      "lat": 35.477415
    },
    "Little River PV 1": {
      "lng": -79.804167,
      "lat": 35.538889
    },
    "Littlefield Solar Center LLC": {
      "lng": -77.438056,
      "lat": 35.433889
    },
    "Lockville Hydropower": {
      "lng": -79.0912,
      "lat": 35.6184
    },
    "Long Creek Waste Water Plant": {
      "lng": -81.1414,
      "lat": 35.3119
    },
    "Long Farm 46 Solar, LLC": {
      "lng": -77.617957,
      "lat": 36.467021
    },
    "Long Henry Solar": {
      "lng": -77.895125,
      "lat": 35.195302
    },
    "Lookout Shoals": {
      "lng": -81.0894,
      "lat": 35.7575
    },
    "Lotus": {
      "lng": -79.53957,
      "lat": 36.13621
    },
    "Lux Solar, LLC": {
      "lng": -81.429433,
      "lat": 35.241145
    },
    "Maiden Community Center": {
      "lng": -81.2081,
      "lat": 35.5772
    },
    "Maiden Creek Solar Power Plant": {
      "lng": -81.167,
      "lat": 35.593
    },
    "Manning PV 1": {
      "lng": -79.521389,
      "lat": 36.119167
    },
    "Manway Solar Farm": {
      "lng": -78.429444,
      "lat": 34.745833
    },
    "Mariposa Solar Center LLC": {
      "lng": -81.086944,
      "lat": 35.373889
    },
    "Market Farm": {
      "lng": -79.647222,
      "lat": 36.390278
    },
    "Marshall Dam": {
      "lng": -82.710571,
      "lat": 35.702527
    },
    "Marshville Farm": {
      "lng": -80.383611,
      "lat": 34.988889
    },
    "Martin Creek Farm LLC": {
      "lng": -78.386111,
      "lat": 36.286389
    },
    "Martins Creek Solar NC, LLC": {
      "lng": -84.015676,
      "lat": 35.020415
    },
    "Mas Durham Power, LLC": {
      "lng": -78.86,
      "lat": 36.0311
    },
    "Mas Wayne Co Power, LLC": {
      "lng": -78.0703,
      "lat": 35.2889
    },
    "Maxton Solar, LLC": {
      "lng": -79.31463,
      "lat": 34.71472
    },
    "Mayberry Solar LLC": {
      "lng": -80.601111,
      "lat": 36.476667
    },
    "Mayo": {
      "lng": -78.8917,
      "lat": 36.5278
    },
    "MC1 Solar": {
      "lng": -77.1397,
      "lat": 35.8748
    },
    "McCallum Farm": {
      "lng": -79.30219,
      "lat": 34.543889
    },
    "McGoogan Farm, LLC": {
      "lng": -79.062708,
      "lat": 34.888736
    },
    "McGrigor Farm Solar": {
      "lng": -78.350088,
      "lat": 35.019771
    },
    "McGuire": {
      "lng": -80.9486,
      "lat": 35.4331
    },
    "McKenzie Farm": {
      "lng": -78.691944,
      "lat": 34.323889
    },
    "Meadowbrook Solar Farm": {
      "lng": -80.38589,
      "lat": 35.55543
    },
    "Meadowlark Solar": {
      "lng": -79.023055,
      "lat": 35.551944
    },
    "Meadows PV 1": {
      "lng": -77.126944,
      "lat": 35.836944
    },
    "Meeks Solar, LLC": {
      "lng": -77.291111,
      "lat": 35.815556
    },
    "Melinda Solar": {
      "lng": -78.3475,
      "lat": 36.2487
    },
    "Meriwether Farm": {
      "lng": -78.604589,
      "lat": 36.288911
    },
    "Metropolitan Sewerage District": {
      "lng": -82.5992,
      "lat": 35.6497
    },
    "Mile Farm": {
      "lng": -78.949586,
      "lat": 36.230561
    },
    "Mill Pond Solar Farm, LLC": {
      "lng": -78.701463,
      "lat": 34.307121
    },
    "Mill Pond Solar, LLC": {
      "lng": -76.790764,
      "lat": 35.829426
    },
    "MILL SOLAR 1": {
      "lng": -80.821,
      "lat": 35.575
    },
    "Millikan Farm": {
      "lng": -79.771085,
      "lat": 35.81771
    },
    "Mills Anson Farm": {
      "lng": -79.935844,
      "lat": 34.968056
    },
    "Milo Solar": {
      "lng": -79.348889,
      "lat": 36.148889
    },
    "Minnie Solar": {
      "lng": -79.361944,
      "lat": 35.948056
    },
    "Misenheimer Farm": {
      "lng": -80.293611,
      "lat": 35.468611
    },
    "Mission": {
      "lng": -83.9258,
      "lat": 35.0647
    },
    "Mocksville Farm": {
      "lng": -80.541944,
      "lat": 35.878611
    },
    "Mocksville Solar": {
      "lng": -80.574444,
      "lat": 35.833889
    },
    "Modlin Solar Farm": {
      "lng": -77.11023,
      "lat": 35.85028
    },
    "Moncure Farm LLC": {
      "lng": -79.0325,
      "lat": 35.568333
    },
    "Monroe Generating Station": {
      "lng": -80.506037,
      "lat": 34.985814
    },
    "Monroe Middle School": {
      "lng": -80.540038,
      "lat": 34.973274
    },
    "Monroe Moore Farm": {
      "lng": -80.601944,
      "lat": 34.971944
    },
    "Monroe Solar Facility": {
      "lng": -80.625615,
      "lat": 34.929945
    },
    "Montgomery Solar LLC": {
      "lng": -79.768333,
      "lat": 35.344444
    },
    "Moore Solar": {
      "lng": -79.48112,
      "lat": 36.38427
    },
    "Moore Solar Farm": {
      "lng": -79.759203,
      "lat": 36.273192
    },
    "Moorings Farm": {
      "lng": -77.829167,
      "lat": 35.305833
    },
    "Moorings Farm 2, LLC": {
      "lng": -77.828889,
      "lat": 35.305747
    },
    "Morgan Farm, LLC": {
      "lng": -78.151987,
      "lat": 36.095572
    },
    "Morgan's Corner": {
      "lng": -76.364244,
      "lat": 36.421064
    },
    "Morganton Station 5": {
      "lng": -81.748075,
      "lat": 35.729
    },
    "Morganton, Parker Road": {
      "lng": -81.66243,
      "lat": 35.7208
    },
    "Morning View": {
      "lng": -79.742842,
      "lat": 35.546315
    },
    "Mount Olive Farm": {
      "lng": -78.081667,
      "lat": 35.201944
    },
    "Mount Olive Solar": {
      "lng": -78.098,
      "lat": 35.18
    },
    "Mountain Island": {
      "lng": -80.9867,
      "lat": 35.3339
    },
    "Mt Olive Farm 2": {
      "lng": -78.069722,
      "lat": 35.218056
    },
    "Mt Olive Solar 1": {
      "lng": -78.096944,
      "lat": 35.181111
    },
    "Murdock Solar": {
      "lng": -78.5206,
      "lat": 35.6633
    },
    "Murphy Farm Power, LLC": {
      "lng": -84.157254,
      "lat": 34.999167
    },
    "Murphy-Brown LLC": {
      "lng": -78.1583,
      "lat": 34.9944
    },
    "Mustang Solar": {
      "lng": -79.57597,
      "lat": 35.416167
    },
    "Nantahala": {
      "lng": -83.6762,
      "lat": 35.2715
    },
    "Narrows (NC)": {
      "lng": -80.0917,
      "lat": 35.4189
    },
    "Nash 58 Farm": {
      "lng": -78.030833,
      "lat": 36.047222
    },
    "Nash 64 Farm": {
      "lng": -78.0925,
      "lat": 35.949444
    },
    "Nash 97 Solar 2": {
      "lng": -78.2162,
      "lat": 35.8503
    },
    "Nash 97 Solar, LLC": {
      "lng": -78.21152,
      "lat": 35.84927
    },
    "Nashville Farms, LLC": {
      "lng": -78.010611,
      "lat": 36.034354
    },
    "NC 102 Project LLC": {
      "lng": -80.4975,
      "lat": 35.2925
    },
    "NC Renewable Power - Lumberton LLC": {
      "lng": -78.9968,
      "lat": 34.59
    },
    "NCEMC Anson Plant": {
      "lng": -79.921738,
      "lat": 34.968651
    },
    "NCEMC Hamlet Plant": {
      "lng": -79.736087,
      "lat": 34.842311
    },
    "NCSU Cates Cogeneration Plant": {
      "lng": -78.674704,
      "lat": 35.784027
    },
    "NCSU CCUP Cogeneration Plant": {
      "lng": -78.673723,
      "lat": 35.775583
    },
    "Neisler Street Solar": {
      "lng": -81.568878,
      "lat": 35.289757
    },
    "Neuse River Solar Farm": {
      "lng": -78.498889,
      "lat": 35.714444
    },
    "New Bern": {
      "lng": -77.2267,
      "lat": 35.1681
    },
    "New Bern Farm": {
      "lng": -77.056725,
      "lat": 35.209564
    },
    "Newton Grove": {
      "lng": -78.367439,
      "lat": 35.251628
    },
    "Nick Solar": {
      "lng": -79.548056,
      "lat": 35.991111
    },
    "Nickelson Solar 2": {
      "lng": -77.988,
      "lat": 34.7457
    },
    "Nickelson Solar, LLC": {
      "lng": -77.987375,
      "lat": 34.747103
    },
    "Nitro Solar": {
      "lng": -78.313889,
      "lat": 35.486944
    },
    "North 301 Solar": {
      "lng": -77.624,
      "lat": 36.213
    },
    "North Carolina Solar Bethea I": {
      "lng": -79.525833,
      "lat": 34.704722
    },
    "North Carolina Solar III LLC": {
      "lng": -79.559722,
      "lat": 34.766667
    },
    "North Nash Farm, LLC": {
      "lng": -78.038319,
      "lat": 36.070336
    },
    "Northern Cardinal Solar": {
      "lng": -77.719745,
      "lat": 36.433289
    },
    "Oakboro Farm": {
      "lng": -79.538333,
      "lat": 34.795833
    },
    "Ocracoke Hybrid": {
      "lng": -75.979722,
      "lat": 35.109444
    },
    "Old Caroleen Solar Farm": {
      "lng": -81.812045,
      "lat": 35.297715
    },
    "Old Catawba PV 1": {
      "lng": -81.134167,
      "lat": 35.711667
    },
    "Old Cedar Solar Energy Storage": {
      "lng": -79.788,
      "lat": 35.713
    },
    "Old Pageland Monroe Road Solar Farm": {
      "lng": -80.48191,
      "lat": 34.95233
    },
    "Old Wire Farm": {
      "lng": -79.513928,
      "lat": 34.817836
    },
    "Onslow Power Producers": {
      "lng": -77.543056,
      "lat": 34.7975
    },
    "Orbit Energy Charlotte": {
      "lng": -80.811667,
      "lat": 35.2625
    },
    "Organ Church Solar": {
      "lng": -80.451125,
      "lat": 35.525584
    },
    "Ouchchy PV1": {
      "lng": -79.24481,
      "lat": 36.084097
    },
    "Owen Solar": {
      "lng": -81.315278,
      "lat": 35.586111
    },
    "Oxford Dam": {
      "lng": -81.1922,
      "lat": 35.8214
    },
    "Page Solar": {
      "lng": -78.639456,
      "lat": 35.533191
    },
    "Pamlico Partners Solar": {
      "lng": -76.87,
      "lat": 35.501111
    },
    "Panda Solar NC 1, LLC": {
      "lng": -79.090206,
      "lat": 34.436698
    },
    "Panda Solar NC 10, LLC": {
      "lng": -78.912997,
      "lat": 34.909631
    },
    "Panda Solar NC 11, LLC": {
      "lng": -78.912481,
      "lat": 34.908589
    },
    "Panda Solar NC 2, LLC": {
      "lng": -79.0847,
      "lat": 34.4384
    },
    "Panda Solar NC 3, LLC": {
      "lng": -79.090672,
      "lat": 34.631814
    },
    "Panda Solar NC 4, LLC": {
      "lng": -78.52434,
      "lat": 34.519408
    },
    "Panda Solar NC 5, LLC": {
      "lng": -78.810478,
      "lat": 35.047153
    },
    "Panda Solar NC 6, LLC": {
      "lng": -78.7874,
      "lat": 34.353586
    },
    "Panda Solar NC 7, LLC": {
      "lng": -78.205464,
      "lat": 35.121181
    },
    "Panda Solar NC 8, LLC": {
      "lng": -79.705958,
      "lat": 35.420931
    },
    "Panda Solar NC 9, LLC": {
      "lng": -78.9136,
      "lat": 34.910856
    },
    "Pasquotank": {
      "lng": -76.306111,
      "lat": 36.264722
    },
    "Pate Farm": {
      "lng": -79.199444,
      "lat": 34.816667
    },
    "PCIP Solar": {
      "lng": -78.9675,
      "lat": 36.3125
    },
    "PCS Phosphate": {
      "lng": -76.779864,
      "lat": 35.376266
    },
    "PCSP3 Airport": {
      "lng": -78.986111,
      "lat": 36.288056
    },
    "Peake Road": {
      "lng": -78.653997,
      "lat": 36.271819
    },
    "Pecan Grove Solar, LLC": {
      "lng": -77.185206,
      "lat": 35.067661
    },
    "Pecan PV1": {
      "lng": -79.829444,
      "lat": 34.909167
    },
    "Pecan Solar": {
      "lng": -77.481164,
      "lat": 36.481008
    },
    "Penny Hill Solar": {
      "lng": -77.513,
      "lat": 35.759
    },
    "Perkins Solar, LLC": {
      "lng": -78.97198,
      "lat": 36.43741
    },
    "PG Solar, LLC": {
      "lng": -78.017811,
      "lat": 35.167164
    },
    "Phelps 158 Solar Farm": {
      "lng": -77.161514,
      "lat": 36.442636
    },
    "Pikeville Farm": {
      "lng": -77.979166,
      "lat": 35.478155
    },
    "Pilot Mountain Solar, LLC": {
      "lng": -80.37635,
      "lat": 36.27066
    },
    "Pinesage": {
      "lng": -79.51909,
      "lat": 35.220823
    },
    "Pineville Delivery 2": {
      "lng": -80.8816,
      "lat": 35.0767
    },
    "Plant Rowan County": {
      "lng": -80.6019,
      "lat": 35.7314
    },
    "Plott Hound Solar": {
      "lng": -77.092,
      "lat": 35.138
    },
    "Plymouth Solar LLC": {
      "lng": -76.710556,
      "lat": 35.875
    },
    "Pollocksville Solar": {
      "lng": -77.219,
      "lat": 34.998
    },
    "Porter Solar": {
      "lng": -77.0743,
      "lat": 35.2167
    },
    "Powatan Road Solar": {
      "lng": -78.372088,
      "lat": 35.602063
    },
    "Princeton": {
      "lng": -78.192222,
      "lat": 35.504444
    },
    "Progress Manis I": {
      "lng": -79.447222,
      "lat": 34.756389
    },
    "Progress Solar 1, LLC": {
      "lng": -78.250833,
      "lat": 35.948056
    },
    "Progress Solar II, LLC": {
      "lng": -79.0975,
      "lat": 34.476111
    },
    "Progress Solar III, LLC": {
      "lng": -79.353889,
      "lat": 34.714167
    },
    "Quarter Horse Solar": {
      "lng": -77.642,
      "lat": 34.437
    },
    "Queens Creek": {
      "lng": -83.676016,
      "lat": 35.27127
    },
    "QVC Inc": {
      "lng": -77.6731,
      "lat": 35.9222
    },
    "Raeford Farm": {
      "lng": -79.211944,
      "lat": 34.968889
    },
    "Railroad Farm": {
      "lng": -78.970833,
      "lat": 34.82
    },
    "Railroad Farm 2": {
      "lng": -78.950494,
      "lat": 34.807903
    },
    "Railroad Solar Farm, LLC": {
      "lng": -79.185,
      "lat": 34.68056
    },
    "Rams Horn Solar Center LLC": {
      "lng": -77.316111,
      "lat": 35.64
    },
    "Ranchland Solar, LLC": {
      "lng": -76.154089,
      "lat": 36.472591
    },
    "Ray Wilson Solar": {
      "lng": -80.376,
      "lat": 35.933
    },
    "Red Hill Solar Center, LLC": {
      "lng": -78.141105,
      "lat": 36.404382
    },
    "Red Oak Solar Farm, LLC": {
      "lng": -77.933518,
      "lat": 35.993015
    },
    "Red Toad 1425 A Powatan Road, LLC": {
      "lng": -78.370875,
      "lat": 35.604208
    },
    "Red Toad 4451 Buffalo Road, LLC": {
      "lng": -78.297336,
      "lat": 35.557397
    },
    "Red Toad 5840 Buffalo Road, LLC": {
      "lng": -78.302297,
      "lat": 35.572873
    },
    "Redmon Solar Farm LLC": {
      "lng": -80.665833,
      "lat": 35.710833
    },
    "Reventure Park": {
      "lng": -80.998889,
      "lat": 35.294444
    },
    "Rhodhiss": {
      "lng": -81.437773,
      "lat": 35.774234
    },
    "Richlands Solar, LLC": {
      "lng": -77.637943,
      "lat": 34.784644
    },
    "Richmond County Plant": {
      "lng": -79.7406,
      "lat": 34.8392
    },
    "Ridgeback": {
      "lng": -79.57982,
      "lat": 35.86546
    },
    "River Road Solar, LLC": {
      "lng": -76.842,
      "lat": 36.351
    },
    "Roanoke Rapids": {
      "lng": -77.6722,
      "lat": 36.4789
    },
    "Robeson County LFG to Energy": {
      "lng": -78.9088,
      "lat": 34.791
    },
    "Robin Solar": {
      "lng": -78.081389,
      "lat": 35.264418
    },
    "Rock Farm": {
      "lng": -79.609722,
      "lat": 34.965556
    },
    "Rockingham County Combustion Turbine": {
      "lng": -79.8297,
      "lat": 36.3297
    },
    "Rockingham Solar, LLC": {
      "lng": -79.603732,
      "lat": 34.980661
    },
    "Rockwell Solar LLC": {
      "lng": -80.474722,
      "lat": 35.541667
    },
    "Rocky Mount Mill": {
      "lng": -77.802435,
      "lat": 35.960058
    },
    "Roper Farm, LLC": {
      "lng": -81.440717,
      "lat": 35.187475
    },
    "Rose Hill": {
      "lng": -77.969722,
      "lat": 34.823889
    },
    "Rosemary Power Station": {
      "lng": -77.6594,
      "lat": 36.4517
    },
    "Rosewood Solar": {
      "lng": -78.10709,
      "lat": 35.4375
    },
    "Roxboro": {
      "lng": -79.0731,
      "lat": 36.4833
    },
    "Roxboro Farm": {
      "lng": -78.917364,
      "lat": 36.4797
    },
    "Roxboro Solar Farm, LLC": {
      "lng": -78.622242,
      "lat": 36.326821
    },
    "Royal Solar": {
      "lng": -78.689,
      "lat": 35.216
    },
    "Ruff Solar LLC": {
      "lng": -81.7634,
      "lat": 35.32062
    },
    "Ruskin Solar": {
      "lng": -78.022,
      "lat": 35.274
    },
    "Rutherford Farm": {
      "lng": -81.830556,
      "lat": 35.257778
    },
    "Ryland Road Solar": {
      "lng": -76.64867,
      "lat": 36.27585
    },
    "Sabattus Solar LLC": {
      "lng": -78.135,
      "lat": 35.819
    },
    "Sadiebrook NC Solar": {
      "lng": -78.658,
      "lat": 34.44
    },
    "Salem Energy Systems LLC": {
      "lng": -80.183458,
      "lat": 36.191241
    },
    "Salisbury Solar": {
      "lng": -80.422626,
      "lat": 35.632585
    },
    "Samarcand Solar Farm, LLC": {
      "lng": -79.695,
      "lat": 35.311667
    },
    "Sampson County Disposal": {
      "lng": -78.4625,
      "lat": 34.9856
    },
    "Sampson Solar": {
      "lng": -78.470536,
      "lat": 34.961039
    },
    "Sandy Cross Solar, LLC": {
      "lng": -77.954167,
      "lat": 35.868333
    },
    "Sandy Solar": {
      "lng": -76.058,
      "lat": 36.33
    },
    "Santeetlah": {
      "lng": -83.863889,
      "lat": 35.4475
    },
    "Sarah Solar": {
      "lng": -78.146944,
      "lat": 36.139444
    },
    "SAS Solar Farm": {
      "lng": -78.7503,
      "lat": 35.8136
    },
    "Scarlet Solar": {
      "lng": -77.795061,
      "lat": 35.287803
    },
    "Schell Solar Farm": {
      "lng": -77.566389,
      "lat": 36.435556
    },
    "Scotch Bonnet Solar, LLC": {
      "lng": -77.908113,
      "lat": 34.53827
    },
    "Seaboard Solar LLC": {
      "lng": -77.467778,
      "lat": 36.476944
    },
    "Seagrove": {
      "lng": -79.787634,
      "lat": 35.532427
    },
    "Sedberry Farm": {
      "lng": -79.321667,
      "lat": 35.243889
    },
    "Selma Solar LLC": {
      "lng": -78.183333,
      "lat": 35.576389
    },
    "Shankle Solar Center LLC": {
      "lng": -79.195833,
      "lat": 34.943889
    },
    "Shannon Farm": {
      "lng": -79.128889,
      "lat": 34.8475
    },
    "Shawboro PV1": {
      "lng": -76.087222,
      "lat": 36.433333
    },
    "Shelby Randolph Road Solar 1, LLC": {
      "lng": -81.588436,
      "lat": 35.309229
    },
    "Shelby Solar Energy Generation Facility": {
      "lng": -81.5956,
      "lat": 35.2536
    },
    "Shelby, NC": {
      "lng": -81.6292,
      "lat": 35.3264
    },
    "Shelby, Toms Street": {
      "lng": -81.5225,
      "lat": 35.3036
    },
    "Shelter Solar": {
      "lng": -79.268026,
      "lat": 34.983772
    },
    "Shiloh Hwy Solar": {
      "lng": -76.033,
      "lat": 36.27
    },
    "Shoe Creek Solar, LLC": {
      "lng": -79.38472,
      "lat": 34.84492
    },
    "SID Solar I, LLC": {
      "lng": -81.505606,
      "lat": 35.258647
    },
    "Sigmon Catawba Farm": {
      "lng": -81.243333,
      "lat": 35.612222
    },
    "Siler 421 Farm, LLC": {
      "lng": -79.449675,
      "lat": 35.747372
    },
    "Siler City Solar 2": {
      "lng": -79.465408,
      "lat": 35.688107
    },
    "Simons Farm": {
      "lng": -76.997222,
      "lat": 36.306389
    },
    "Smith Solar Farm": {
      "lng": -79.558408,
      "lat": 34.775083
    },
    "Smithfield Farmland Corp Bladen": {
      "lng": -78.807292,
      "lat": 34.74625
    },
    "Smithfield Packaged Meats Corp.": {
      "lng": -77.659167,
      "lat": 35.268333
    },
    "Sneads Grove": {
      "lng": -79.465814,
      "lat": 34.79217
    },
    "Snow Camp Solar, LLC": {
      "lng": -79.42346,
      "lat": 35.86267
    },
    "Snow Hill Solar 2 LLC": {
      "lng": -77.649444,
      "lat": 35.444167
    },
    "SoINCPower5, LLC": {
      "lng": -77.762458,
      "lat": 36.052801
    },
    "SoINCPower6, LLC": {
      "lng": -76.581,
      "lat": 36.443
    },
    "Solar Lee, LLC": {
      "lng": -78.08459,
      "lat": 35.184704
    },
    "Solar Star North Carolina II LLC": {
      "lng": -77.064,
      "lat": 36.425
    },
    "Soluga Farms 1": {
      "lng": -78.386944,
      "lat": 35.243056
    },
    "Soluga Farms 2 LLC": {
      "lng": -78.689167,
      "lat": 34.500008
    },
    "Soluga Farms III": {
      "lng": -78.673331,
      "lat": 35.219272
    },
    "Soluga Farms IV": {
      "lng": -78.669479,
      "lat": 35.21871
    },
    "Sonne One": {
      "lng": -77.833611,
      "lat": 35.437222
    },
    "Sonne Two": {
      "lng": -81.183611,
      "lat": 35.656667
    },
    "Sophie Solar": {
      "lng": -81.696944,
      "lat": 35.305
    },
    "Soul City Solar": {
      "lng": -78.25,
      "lat": 36.25
    },
    "South Atlantic Services Solar Farm I": {
      "lng": -77.975668,
      "lat": 34.290189
    },
    "South Louisburg Solar": {
      "lng": -78.308,
      "lat": 36.057
    },
    "South Robeson Farm": {
      "lng": -79.28097,
      "lat": 34.561146
    },
    "South Winston Farm, LLC": {
      "lng": -80.302,
      "lat": 36.0288
    },
    "Southerland Farm Solar": {
      "lng": -78.367806,
      "lat": 36.265611
    },
    "Speedway Solar NC, LLC": {
      "lng": -80.536,
      "lat": 35.218
    },
    "Spencer Farm, LLC": {
      "lng": -80.407853,
      "lat": 35.712886
    },
    "Spicewood Solar Farm LLC": {
      "lng": -79.701667,
      "lat": 35.3025
    },
    "Spring Hope Solar 3, LLC": {
      "lng": -78.192464,
      "lat": 35.905578
    },
    "Spring Valley Farm 2, LLC": {
      "lng": -78.415278,
      "lat": 36.371111
    },
    "St. Pauls Solar 1, LLC": {
      "lng": -78.9836,
      "lat": 34.7716
    },
    "St. Pauls Solar 2": {
      "lng": -78.981,
      "lat": 34.7691
    },
    "Stagecoach Solar": {
      "lng": -78.459722,
      "lat": 36.414722
    },
    "Stainback Solar Farm": {
      "lng": -78.328294,
      "lat": 36.395286
    },
    "Star Solar": {
      "lng": -78.823889,
      "lat": 36.08
    },
    "Starr": {
      "lng": -78.350802,
      "lat": 35.015398
    },
    "Statesville Delivery No 3": {
      "lng": -80.8542,
      "lat": 35.7972
    },
    "Statesville Solar": {
      "lng": -80.712405,
      "lat": 35.739704
    },
    "Statesville, Highway 64": {
      "lng": -80.942709,
      "lat": 35.798462
    },
    "Stikeleather Farm": {
      "lng": -81.106111,
      "lat": 35.902222
    },
    "Stone Solar": {
      "lng": -79.031836,
      "lat": 34.583119
    },
    "Stout Farm": {
      "lng": -79.199722,
      "lat": 36.079167
    },
    "Strider Solar, LLC": {
      "lng": -79.79,
      "lat": 35.6
    },
    "Sugar Run Solar": {
      "lng": -76.597825,
      "lat": 36.437752
    },
    "Sugar Solar, LLC": {
      "lng": -80.6574,
      "lat": 36.1607
    },
    "Summit Farms Solar": {
      "lng": -76.136,
      "lat": 36.4765
    },
    "Sun Devil Solar": {
      "lng": -77.9703,
      "lat": 36.432
    },
    "Sun Farm V, LLC": {
      "lng": -76.489357,
      "lat": 36.160629
    },
    "Sun Farm VI, LLC": {
      "lng": -76.507778,
      "lat": 36.114444
    },
    "Sun Farm VIII, LLC": {
      "lng": -77.75,
      "lat": 36.06
    },
    "Suncaster, LLC": {
      "lng": -77.969554,
      "lat": 35.504069
    },
    "SunEdison LV Sutton Plant Site": {
      "lng": -77.9814,
      "lat": 34.2867
    },
    "SunEnergy1-Scotland Neck, LLC": {
      "lng": -77.409085,
      "lat": 36.124961
    },
    "Sunfish Farm": {
      "lng": -78.735833,
      "lat": 35.558056
    },
    "Sunflower Solar": {
      "lng": -77.593,
      "lat": 36.395
    },
    "Swansboro Solar, LLC": {
      "lng": -77.21041,
      "lat": 34.810913
    },
    "Sweet Tea Solar, LLC": {
      "lng": -77.133295,
      "lat": 35.160889
    },
    "Sweetgum Solar": {
      "lng": -79.181111,
      "lat": 35.458333
    },
    "Tamworth Holdings": {
      "lng": -78.421185,
      "lat": 34.745617
    },
    "Tanager Holdings": {
      "lng": -79.784627,
      "lat": 35.7527
    },
    "Tarboro Solar": {
      "lng": -77.499167,
      "lat": 35.858056
    },
    "Tart Farm": {
      "lng": -78.681111,
      "lat": 35.204444
    },
    "Tate Solar": {
      "lng": -78.029612,
      "lat": 36.066109
    },
    "Taylorsville Solar LLC": {
      "lng": -81.152498,
      "lat": 35.909074
    },
    "Tennessee Creek": {
      "lng": -83.0028,
      "lat": 35.2139
    },
    "Thanksgiving Fire Solar Farm, LLC": {
      "lng": -78.327,
      "lat": 35.701
    },
    "Thornton PV1": {
      "lng": -77.661667,
      "lat": 35.395833
    },
    "Thorpe": {
      "lng": -83.125398,
      "lat": 35.233988
    },
    "Three Bridge": {
      "lng": -79.2001,
      "lat": 34.6705
    },
    "Thunderhead": {
      "lng": -79.78992,
      "lat": 35.75101
    },
    "Tiburon Holdings": {
      "lng": -80.416944,
      "lat": 35.903333
    },
    "Tides Lane Farm": {
      "lng": -78.298796,
      "lat": 36.06936
    },
    "Tillery": {
      "lng": -80.06483,
      "lat": 35.206741
    },
    "Tinker (NC)": {
      "lng": -79.136921,
      "lat": 35.688268
    },
    "Tolson Solar": {
      "lng": -77.615869,
      "lat": 35.829572
    },
    "Town of Cary": {
      "lng": -77.969722,
      "lat": 34.823889
    },
    "Tracy Solar": {
      "lng": -78.1398,
      "lat": 35.7998
    },
    "Trent River Farm": {
      "lng": -77.186594,
      "lat": 35.059582
    },
    "Trent River Solar, LLC": {
      "lng": -77.2514,
      "lat": 34.9915
    },
    "Trinity Solar": {
      "lng": -79.9282,
      "lat": 35.87025
    },
    "Tripple State Farm": {
      "lng": -80.774867,
      "lat": 35.734972
    },
    "Trojan Solar, LLC": {
      "lng": -78.20425,
      "lat": 34.312472
    },
    "Truman (NC)": {
      "lng": -78.326891,
      "lat": 35.713283
    },
    "Tuckasegee": {
      "lng": -83.128111,
      "lat": 35.247011
    },
    "Tuckertown Hydro": {
      "lng": -80.1758,
      "lat": 35.4856
    },
    "Turkey Branch Solar LLC": {
      "lng": -79.104444,
      "lat": 34.524444
    },
    "Turkey Creek PV1": {
      "lng": -77.135575,
      "lat": 36.441359
    },
    "Turner Shoals": {
      "lng": -82.186369,
      "lat": 35.334767
    },
    "Tuxedo": {
      "lng": -82.389371,
      "lat": 35.245006
    },
    "TWE Chocowinity Solar, LLC": {
      "lng": -77.105614,
      "lat": 35.501688
    },
    "TWE New Bern Solar Project, LLC": {
      "lng": -77.13432,
      "lat": 35.154273
    },
    "Two Lines Farm": {
      "lng": -81.326667,
      "lat": 35.629167
    },
    "Two Mile Desert Project": {
      "lng": -76.46,
      "lat": 36.22
    },
    "Two Mile Solar": {
      "lng": -76.461944,
      "lat": 36.223056
    },
    "Underwood PV2": {
      "lng": -77.049537,
      "lat": 36.419353
    },
    "University of NC Chapel Hill": {
      "lng": -79.0617,
      "lat": 35.9069
    },
    "Upchurch Solar Center LLC": {
      "lng": -77.883889,
      "lat": 35.781944
    },
    "Uwharrie Mountain Renewable": {
      "lng": -79.964939,
      "lat": 35.332974
    },
    "Van Buren": {
      "lng": -78.527499,
      "lat": 34.984045
    },
    "Van Slyke Solar Center, LLC": {
      "lng": -77.880517,
      "lat": 35.776992
    },
    "Vance Solar Farm, LLC": {
      "lng": -78.411966,
      "lat": 36.258478
    },
    "Vaughn Creek PV1": {
      "lng": -77.120595,
      "lat": 36.451012
    },
    "Ventura Solar, LLC": {
      "lng": -81.177116,
      "lat": 35.878809
    },
    "Vickers": {
      "lng": -79.080278,
      "lat": 35.823333
    },
    "Vicksburg Solar": {
      "lng": -78.343056,
      "lat": 36.325
    },
    "Violet Solar": {
      "lng": -80.4185,
      "lat": 35.401091
    },
    "Viper Solar": {
      "lng": -78.962159,
      "lat": 34.934886
    },
    "W H Weatherspoon": {
      "lng": -78.97552,
      "lat": 34.587538
    },
    "Waco Farm": {
      "lng": -81.426111,
      "lat": 35.352222
    },
    "Wadesboro 4": {
      "lng": -80.122015,
      "lat": 35.102174
    },
    "Wadesboro Farm 1": {
      "lng": -80.09732,
      "lat": 34.99115
    },
    "Wadesboro Farm 2": {
      "lng": -80.093192,
      "lat": 35.023406
    },
    "Wadesboro Farm 3": {
      "lng": -80.051028,
      "lat": 34.969356
    },
    "Wadesboro Solar": {
      "lng": -80.028987,
      "lat": 34.973863
    },
    "Wagstaff Farm 1": {
      "lng": -78.956739,
      "lat": 36.458658
    },
    "Wake County LFG Facility": {
      "lng": -78.854,
      "lat": 35.674
    },
    "Wakefield Solar": {
      "lng": -78.312403,
      "lat": 35.814236
    },
    "Wallace": {
      "lng": -77.97173,
      "lat": 34.75813
    },
    "Walters": {
      "lng": -83.0503,
      "lat": 35.6946
    },
    "Walters Solar (FLS 260)": {
      "lng": -79.048889,
      "lat": 34.496944
    },
    "Warbler Holdings": {
      "lng": -81.623934,
      "lat": 35.318824
    },
    "Ward WTP": {
      "lng": -79.9708,
      "lat": 35.9678
    },
    "Warren Solar Farm LLC": {
      "lng": -78.369,
      "lat": 35.252
    },
    "Warrenton Farm": {
      "lng": -78.171111,
      "lat": 36.415
    },
    "Warrenton I Solar": {
      "lng": -78.178,
      "lat": 36.412
    },
    "Warsaw Farm": {
      "lng": -78.125833,
      "lat": 35.005556
    },
    "Warsaw I": {
      "lng": -78.11,
      "lat": 35.01
    },
    "Warsaw II": {
      "lng": -78.05,
      "lat": 35.01
    },
    "Washington Airport Solar LLC": {
      "lng": -77.053611,
      "lat": 35.565278
    },
    "Washington Millfield Solar, LLC": {
      "lng": -76.833611,
      "lat": 35.499444
    },
    "Washington White Post Solar LLC": {
      "lng": -76.8484,
      "lat": 35.5052
    },
    "Waste Management Piedmont LFGTE Project": {
      "lng": -80.040555,
      "lat": 36.193611
    },
    "Water Filter Plant #2": {
      "lng": -81.7278,
      "lat": 35.737748
    },
    "Watson Seed Farm PV1": {
      "lng": -77.721389,
      "lat": 36.129722
    },
    "Watts Farm": {
      "lng": -79.335278,
      "lat": 34.726944
    },
    "Wayne Solar I LLC": {
      "lng": -78.019026,
      "lat": 35.221083
    },
    "Wayne Solar II LLC": {
      "lng": -78.07948,
      "lat": 35.31556
    },
    "Wayne Solar III LLC": {
      "lng": -77.871225,
      "lat": 35.324377
    },
    "Wedge": {
      "lng": -77.919201,
      "lat": 35.416191
    },
    "Wellons Farm": {
      "lng": -78.291129,
      "lat": 35.511698
    },
    "Wendell Solar Farm LLC": {
      "lng": -78.354999,
      "lat": 35.734581
    },
    "West Salisbury Farm LLC": {
      "lng": -80.523611,
      "lat": 35.668333
    },
    "West Siler": {
      "lng": -79.503747,
      "lat": 35.720841
    },
    "WestRock Kraft Paper, LLC": {
      "lng": -77.6414,
      "lat": 36.4769
    },
    "Westside WWTP": {
      "lng": -80.1106,
      "lat": 35.9403
    },
    "Weyerhaeuser - New Bern": {
      "lng": -77.1144,
      "lat": 35.212
    },
    "Whitakers": {
      "lng": -77.734444,
      "lat": 36.127222
    },
    "Whitakers Farm (Fisher Rd)": {
      "lng": -77.725147,
      "lat": 36.115542
    },
    "White Cross Farm": {
      "lng": -79.188297,
      "lat": 35.907144
    },
    "White Farm Solar, LLC": {
      "lng": -77.22244,
      "lat": 36.34764
    },
    "White Street Renewables": {
      "lng": -79.723622,
      "lat": 36.10447
    },
    "Wilkinson Solar LLC": {
      "lng": -76.763849,
      "lat": 35.596814
    },
    "Willard Solar": {
      "lng": -77.9697,
      "lat": 34.70249
    },
    "Williamston Solar": {
      "lng": -77.063056,
      "lat": 35.801111
    },
    "Williamston Speight Solar, LLC": {
      "lng": -77.164201,
      "lat": 35.887545
    },
    "Williamston West Farm, LLC": {
      "lng": -77.108044,
      "lat": 35.836544
    },
    "Wilson Farm 1": {
      "lng": -77.873611,
      "lat": 35.803611
    },
    "Wilson Solar Farm 1": {
      "lng": -77.9,
      "lat": 35.7
    },
    "Wilson Solar Farm 2": {
      "lng": -78.016,
      "lat": 35.683
    },
    "Wilson Solar Farm 3": {
      "lng": -78,
      "lat": 35.78
    },
    "Wilson Solar Farm 4": {
      "lng": -78,
      "lat": 35.7
    },
    "Wilson Solar Farm 5": {
      "lng": -77.87,
      "lat": 35.68
    },
    "Wilson Solar Farm 6": {
      "lng": -77.87,
      "lat": 35.68
    },
    "Wilson Solar Farm 7": {
      "lng": -78,
      "lat": 35.75
    },
    "Windsor Cooper HIill Solar, LLC": {
      "lng": -76.917778,
      "lat": 35.981944
    },
    "Windsor Hwy 17 Solar": {
      "lng": -76.872438,
      "lat": 36.021253
    },
    "Windsor Solar": {
      "lng": -76.883056,
      "lat": 36.016944
    },
    "Winton Solar": {
      "lng": -76.938889,
      "lat": 36.375
    },
    "Woodland Church Farm": {
      "lng": -77.853611,
      "lat": 35.058333
    },
    "Woodland Solar": {
      "lng": -77.221111,
      "lat": 36.341944
    },
    "Woodleaf Solar Facility": {
      "lng": -80.608449,
      "lat": 35.724427
    },
    "Woodsdale Farm, LLC": {
      "lng": -78.95,
      "lat": 36.464
    },
    "Works 53": {
      "lng": -80.277778,
      "lat": 35.755
    },
    "Wortham Solar Farm": {
      "lng": -78.376344,
      "lat": 36.352489
    },
    "Wyse Fork Solar Farm, LLC": {
      "lng": -77.503222,
      "lat": 35.21377
    },
    "XPF Solar Field": {
      "lng": -78.873333,
      "lat": 35.911667
    },
    "Yadkin 601 Farm": {
      "lng": -80.697222,
      "lat": 36.199167
    },
    "Yadkinville Solar": {
      "lng": -80.596009,
      "lat": 36.13727
    },
    "Yanceyville Farm": {
      "lng": -79.278889,
      "lat": 36.439444
    },
    "Yanceyville Farm 2 LLC": {
      "lng": -79.316944,
      "lat": 36.413889
    },
    "Yanceyville Farm 3, LLC": {
      "lng": -79.348886,
      "lat": 36.428608
    },
    "York Road Solar I, LLC": {
      "lng": -81.333702,
      "lat": 35.222208
    },
    "Zuma Solar, LLC": {
      "lng": -79.437885,
      "lat": 36.510939
    },
    "ZV Solar 1": {
      "lng": -79.2,
      "lat": 34.8
    },
    "ZV Solar 2, LLC": {
      "lng": -79.2,
      "lat": 34.816
    },
    "ZV Solar 3, LLC": {
      "lng": -79.200218,
      "lat": 34.816182
    },
    "American Crystal Sugar Drayton": {
      "lng": -97.17608,
      "lat": 48.593158
    },
    "American Crystal Sugar Hillsboro": {
      "lng": -97.06311,
      "lat": 47.437562
    },
    "Antelope Valley": {
      "lng": -101.83566,
      "lat": 47.370542
    },
    "Ashtabula Wind Energy Center": {
      "lng": -97.8961,
      "lat": 47.1244
    },
    "Ashtabula Wind II LLC": {
      "lng": -97.931692,
      "lat": 47.292437
    },
    "Ashtabula Wind III LLC": {
      "lng": -97.8956,
      "lat": 47.1242
    },
    "Aurora Wind Project": {
      "lng": -103.1,
      "lat": 48.5
    },
    "Baldwin Wind LLC": {
      "lng": -100.7086,
      "lat": 47.1133
    },
    "Bison 2 Wind Energy Center": {
      "lng": -101.554722,
      "lat": 46.98
    },
    "Bison 3 Wind Energy Center": {
      "lng": -101.554722,
      "lat": 46.98
    },
    "Bison 4 Wind Energy Center": {
      "lng": -101.554722,
      "lat": 46.98
    },
    "Bison I Wind Energy Center": {
      "lng": -101.554722,
      "lat": 46.98
    },
    "Border Winds Wind Farm": {
      "lng": -99.613889,
      "lat": 48.964722
    },
    "Brady II Wind Energy Center": {
      "lng": -102.78821,
      "lat": 46.611515
    },
    "Brady Wind Energy Center": {
      "lng": -102.765089,
      "lat": 46.646471
    },
    "Burke Wind, LLC": {
      "lng": -102.637766,
      "lat": 48.754492
    },
    "Cedar Hills Wind Farm": {
      "lng": -103.767222,
      "lat": 46.245556
    },
    "Coal Creek": {
      "lng": -101.157058,
      "lat": 47.377743
    },
    "Courtenay Wind Farm": {
      "lng": -98.594722,
      "lat": 47.179722
    },
    "Coyote": {
      "lng": -101.815722,
      "lat": 47.221447
    },
    "Dakota Magic": {
      "lng": -96.835833,
      "lat": 45.938611
    },
    "Emmons-Logan Wind, LLC": {
      "lng": -100.051111,
      "lat": 46.373013
    },
    "Enderlin": {
      "lng": -97.5765,
      "lat": 46.6144
    },
    "Foxtail Wind, LLC": {
      "lng": -98.880073,
      "lat": 46.098211
    },
    "FPL Energy Ashtabula Wind LLC": {
      "lng": -97.8853,
      "lat": 47.1294
    },
    "FPL Energy Burleigh County Wind": {
      "lng": -100.7333,
      "lat": 47.1167
    },
    "FPL Energy North Dakota Wind I/II": {
      "lng": -98.898356,
      "lat": 46.327169
    },
    "FPL Energy Oliver Wind I LLC": {
      "lng": -101.197716,
      "lat": 47.152847
    },
    "FPL Energy Oliver Wind II LLC": {
      "lng": -101.197716,
      "lat": 47.152847
    },
    "Garrison": {
      "lng": -101.4115,
      "lat": 47.4945
    },
    "Glen Ullin Energy Center": {
      "lng": -101.816495,
      "lat": 46.964077
    },
    "Glen Ullin Station 6": {
      "lng": -101.7564,
      "lat": 46.8214
    },
    "Grafton": {
      "lng": -97.403328,
      "lat": 48.430028
    },
    "Hillsboro": {
      "lng": -97.060555,
      "lat": 47.396944
    },
    "Jamestown": {
      "lng": -98.662183,
      "lat": 46.905665
    },
    "Kindred School": {
      "lng": -97.00953,
      "lat": 46.64974
    },
    "Langdon Renewables, LLC": {
      "lng": -98.315,
      "lat": 48.63
    },
    "Langdon Wind Energy Center": {
      "lng": -98.3169,
      "lat": 48.6303
    },
    "Langdon Wind II LLC": {
      "lng": -98.3164,
      "lat": 48.6292
    },
    "Leland Olds": {
      "lng": -101.321213,
      "lat": 47.280769
    },
    "Lindahl Wind Project, LLC": {
      "lng": -102.933,
      "lat": 48.533
    },
    "Lonesome Creek Station": {
      "lng": -103.57861,
      "lat": 47.796667
    },
    "Luverne Wind Energy Center": {
      "lng": -97.8975,
      "lat": 47.3272
    },
    "Merricourt Wind Energy Center": {
      "lng": -99.0592,
      "lat": 46.1075
    },
    "Milton R Young": {
      "lng": -101.213093,
      "lat": 47.065854
    },
    "Minot Wind Project": {
      "lng": -101.280517,
      "lat": 48.021271
    },
    "New Frontier Wind": {
      "lng": -100.947664,
      "lat": 47.861531
    },
    "Oliver Wind III, LLC": {
      "lng": -101.1681,
      "lat": 46.9571
    },
    "Pembina Land Port of Entry Wind Turbine": {
      "lng": -97.24161,
      "lat": 48.996866
    },
    "Pioneer Generating Station": {
      "lng": -103.95278,
      "lat": 48.2325
    },
    "Portable Generator 2": {
      "lng": -103.6375,
      "lat": 48.145
    },
    "Portable Generator 3": {
      "lng": -103.6375,
      "lat": 48.145
    },
    "Prairie Winds ND1": {
      "lng": -101.269189,
      "lat": 47.933621
    },
    "R M Heskett": {
      "lng": -100.8836,
      "lat": 46.8669
    },
    "Rugby Wind Power Project": {
      "lng": -99.9513,
      "lat": 48.4968
    },
    "Spiritwood Station": {
      "lng": -98.499713,
      "lat": 46.926423
    },
    "Sunflower Wind Project": {
      "lng": -102.07455,
      "lat": 46.837569
    },
    "Tatanka Wind Power LLC": {
      "lng": -98.946325,
      "lat": 45.959325
    },
    "Tesoro Mandan Cogeneration Plant": {
      "lng": -100.8808,
      "lat": 46.8492
    },
    "Thunder Spirit Wind, LLC": {
      "lng": -102.563333,
      "lat": 46.075
    },
    "Velva Windfarm LLC": {
      "lng": -100.9215,
      "lat": 48.083625
    },
    "Wilton Wind II LLC": {
      "lng": -100.7011,
      "lat": 47.1333
    },
    "Ainsworth Wind": {
      "lng": -99.8917,
      "lat": 42.4489
    },
    "Airport 009239 SCS Lexington, LLC": {
      "lng": -99.768018,
      "lat": 40.785402
    },
    "Ansley": {
      "lng": -99.386336,
      "lat": 41.289185
    },
    "Archer Daniels Midland Columbus": {
      "lng": -97.286328,
      "lat": 41.416442
    },
    "Archer Daniels Midland Lincoln": {
      "lng": -96.614486,
      "lat": 40.866463
    },
    "Arnold": {
      "lng": -100.19561,
      "lat": 41.421928
    },
    "Auburn": {
      "lng": -95.8464,
      "lat": 40.3883
    },
    "Beatrice": {
      "lng": -96.808153,
      "lat": 40.330184
    },
    "Beaver City": {
      "lng": -99.827772,
      "lat": 40.130304
    },
    "Broken Bow": {
      "lng": -99.6392,
      "lat": 41.4031
    },
    "Broken Bow Wind II, LLC": {
      "lng": -99.323056,
      "lat": 41.323889
    },
    "Broken Bow Wind LLC": {
      "lng": -99.568056,
      "lat": 41.454722
    },
    "Burt County Solar Hybrid": {
      "lng": -96.232167,
      "lat": 41.863778
    },
    "Burwell": {
      "lng": -99.136071,
      "lat": 41.780313
    },
    "C W Burdick": {
      "lng": -98.3269,
      "lat": 40.9228
    },
    "Cambridge": {
      "lng": -100.176836,
      "lat": 40.286178
    },
    "Campbell Village": {
      "lng": -98.7331,
      "lat": 40.2992
    },
    "Canaday": {
      "lng": -99.7011,
      "lat": 40.6942
    },
    "Cass County Station": {
      "lng": -95.964026,
      "lat": 40.947913
    },
    "CCC Hastings Wind Turbine": {
      "lng": -98.334,
      "lat": 40.57
    },
    "Chappell": {
      "lng": -102.471547,
      "lat": 41.092589
    },
    "City Light & Water": {
      "lng": -98.450252,
      "lat": 40.327952
    },
    "City of Lexington": {
      "lng": -99.734,
      "lat": 40.757
    },
    "Columbus": {
      "lng": -97.3283,
      "lat": 41.4639
    },
    "Cooper Nuclear Station": {
      "lng": -95.6408,
      "lat": 40.3628
    },
    "Cottonwood Wind Energy Center": {
      "lng": -98.405956,
      "lat": 40.240168
    },
    "Creston Ridge II, LLC": {
      "lng": -97.391,
      "lat": 41.677
    },
    "Creston Ridge Wind Farm": {
      "lng": -97.386,
      "lat": 41.678
    },
    "Crete": {
      "lng": -96.9601,
      "lat": 40.6253
    },
    "Crofton Bluffs Wind LLC": {
      "lng": -97.58,
      "lat": 42.698333
    },
    "Cuming County Renewables, LLC": {
      "lng": -96.861833,
      "lat": 42.019972
    },
    "Curtis": {
      "lng": -100.515161,
      "lat": 40.631453
    },
    "David City": {
      "lng": -97.119202,
      "lat": 41.254652
    },
    "Deshler": {
      "lng": -97.7239,
      "lat": 40.1408
    },
    "Dodge County Solar Hybrid": {
      "lng": -96.752,
      "lat": 41.612
    },
    "Don Henry": {
      "lng": -98.420055,
      "lat": 40.583155
    },
    "Elk City Station": {
      "lng": -96.254444,
      "lat": 41.383889
    },
    "Elkhorn Ridge Wind LLC": {
      "lng": -97.6189,
      "lat": 42.6978
    },
    "Emerson": {
      "lng": -96.7292,
      "lat": 42.276
    },
    "Falls City": {
      "lng": -95.6083,
      "lat": 40.055
    },
    "Flat Water Wind Farm LLC": {
      "lng": -95.9289,
      "lat": 40.0011
    },
    "Fort Calhoun Community Solar": {
      "lng": -96.004766,
      "lat": 41.456612
    },
    "Franklin (NE)": {
      "lng": -98.9508,
      "lat": 40.0892
    },
    "Gavins Point": {
      "lng": -97.481479,
      "lat": 42.848829
    },
    "Gerald Gentleman Station": {
      "lng": -101.1408,
      "lat": 41.0808
    },
    "Gerald Whelan Energy Center": {
      "lng": -98.312437,
      "lat": 40.580872
    },
    "Grande Prairie Wind Farm": {
      "lng": -98.428333,
      "lat": 42.608056
    },
    "Hallam Peaking": {
      "lng": -96.786177,
      "lat": 40.558652
    },
    "Hastings Community Solar Farm": {
      "lng": -98.437,
      "lat": 40.602
    },
    "Hebron Peaking": {
      "lng": -97.577857,
      "lat": 40.187928
    },
    "Holdrege Solar Center": {
      "lng": -96.820315,
      "lat": 40.824736
    },
    "J Street": {
      "lng": -96.710667,
      "lat": 40.80788
    },
    "Jeffrey": {
      "lng": -100.397905,
      "lat": 40.959405
    },
    "Johnson 1": {
      "lng": -99.8178,
      "lat": 40.6936
    },
    "Johnson 2": {
      "lng": -99.7447,
      "lat": 40.6839
    },
    "Jones Street": {
      "lng": -95.922682,
      "lat": 41.251488
    },
    "Kearney": {
      "lng": -99.100753,
      "lat": 40.703511
    },
    "Kearney NPPD Solar Project": {
      "lng": -99.041196,
      "lat": 40.721688
    },
    "Kimball": {
      "lng": -103.6667,
      "lat": 41.2382
    },
    "Kimball Wind": {
      "lng": -103.6983,
      "lat": 41.2736
    },
    "Kingsley": {
      "lng": -101.6681,
      "lat": 41.2114
    },
    "Laredo Ridge Wind LLC": {
      "lng": -98.023889,
      "lat": 41.880278
    },
    "Laurel": {
      "lng": -97.090814,
      "lat": 42.430539
    },
    "Little Blue Wind Project, LLC": {
      "lng": -98.407586,
      "lat": 40.284421
    },
    "Lon D Wright Power Plant": {
      "lng": -96.4623,
      "lat": 41.4281
    },
    "Lyons": {
      "lng": -96.4668,
      "lat": 41.9374
    },
    "Madison Utilities": {
      "lng": -97.456275,
      "lat": 41.829754
    },
    "McCook Peaking": {
      "lng": -100.6508,
      "lat": 40.2214
    },
    "Milligan 1 Wind Farm": {
      "lng": -97.220567,
      "lat": 40.478624
    },
    "Missouri River Wastewater Treatment": {
      "lng": -95.9292,
      "lat": 41.2033
    },
    "Mobile": {
      "lng": -97.601363,
      "lat": 40.883013
    },
    "Monroe (NE)": {
      "lng": -97.6078,
      "lat": 41.4872
    },
    "Monroe 009452 SCS Cozad, LLC": {
      "lng": -99.978579,
      "lat": 40.872664
    },
    "Nebraska City # 1": {
      "lng": -95.8475,
      "lat": 40.6806
    },
    "Nebraska City # 2": {
      "lng": -95.868333,
      "lat": 40.6661
    },
    "Nebraska City Station": {
      "lng": -95.7764,
      "lat": 40.6214
    },
    "North Denver": {
      "lng": -98.38863,
      "lat": 40.598839
    },
    "North Omaha Station": {
      "lng": -95.94466,
      "lat": 41.32909
    },
    "North Platte": {
      "lng": -100.7594,
      "lat": 41.0864
    },
    "Ord": {
      "lng": -98.926144,
      "lat": 41.604164
    },
    "Oxford (NE)": {
      "lng": -99.6108,
      "lat": 40.2581
    },
    "Papillion Creek Wastewater": {
      "lng": -95.87,
      "lat": 41.0772
    },
    "Pender": {
      "lng": -96.705431,
      "lat": 42.114742
    },
    "Perennial Windfarm": {
      "lng": -97.642708,
      "lat": 40.631872
    },
    "Plainview Muncipal Power": {
      "lng": -97.790556,
      "lat": 42.35
    },
    "Platte": {
      "lng": -98.348222,
      "lat": 40.854765
    },
    "Plum Creek Wind Project (NE)": {
      "lng": -97.203789,
      "lat": 42.16159
    },
    "Polk County Renewables, LLC": {
      "lng": -97.570194,
      "lat": 41.165278
    },
    "Prairie Breeze": {
      "lng": -98.076667,
      "lat": 41.951667
    },
    "Prairie Breeze II": {
      "lng": -98.210401,
      "lat": 41.987656
    },
    "Prairie Breeze III": {
      "lng": -98.210401,
      "lat": 41.987656
    },
    "Rattlesnake Creek Wind Project": {
      "lng": -96.829,
      "lat": 42.374
    },
    "Red Cloud": {
      "lng": -98.519032,
      "lat": 40.097954
    },
    "Rokeby": {
      "lng": -96.7364,
      "lat": 40.7316
    },
    "Salt Valley Wind Plant": {
      "lng": -96.6231,
      "lat": 40.9044
    },
    "Sargent": {
      "lng": -99.3698,
      "lat": 41.641588
    },
    "Sarpy County Station": {
      "lng": -95.970603,
      "lat": 41.170594
    },
    "Scribner Diesel Generation Facility": {
      "lng": -96.669132,
      "lat": 41.668419
    },
    "Seward Wind Farm": {
      "lng": -97.1951,
      "lat": 40.895
    },
    "Sheldon": {
      "lng": -96.7847,
      "lat": 40.5589
    },
    "Sholes Wind Energy Center": {
      "lng": -97.3647,
      "lat": 42.3119
    },
    "Sidney (NE)": {
      "lng": -102.977836,
      "lat": 41.145634
    },
    "South Sioux City Energy Storage": {
      "lng": -96.407199,
      "lat": 42.433124
    },
    "South Sioux City Solar": {
      "lng": -96.439688,
      "lat": 42.45847
    },
    "Spalding": {
      "lng": -98.36756,
      "lat": 41.68138
    },
    "Springview": {
      "lng": -99.777467,
      "lat": 42.82579
    },
    "Steele Flats Wind Project LLC": {
      "lng": -96.952222,
      "lat": 40.048889
    },
    "Stuart (NE)": {
      "lng": -99.143707,
      "lat": 42.599377
    },
    "Syracuse # 2": {
      "lng": -96.177778,
      "lat": 40.6756
    },
    "Tecumseh": {
      "lng": -96.19,
      "lat": 40.3664
    },
    "Terry Bundy Generating Station": {
      "lng": -96.613086,
      "lat": 40.909653
    },
    "TPW Petersburg": {
      "lng": -97.961194,
      "lat": 41.864583
    },
    "Upstream Wind Energy LLC": {
      "lng": -97.964978,
      "lat": 42.184336
    },
    "Valentine Wind, LLC": {
      "lng": -100.592439,
      "lat": 42.859103
    },
    "Wahoo": {
      "lng": -96.61155,
      "lat": 41.211798
    },
    "Wakefield": {
      "lng": -96.8621,
      "lat": 42.2691
    },
    "Wayne IC": {
      "lng": -97.0183,
      "lat": 42.2278
    },
    "West Point Municipal": {
      "lng": -96.71345,
      "lat": 41.844936
    },
    "Western Meadowlark Solar SCS NE 1, LLC": {
      "lng": -103.649,
      "lat": 41.889
    },
    "Western Sugar Coop - Scottsbluff": {
      "lng": -103.63438,
      "lat": 41.85887
    },
    "Wilber": {
      "lng": -96.9604,
      "lat": 40.4797
    },
    "Wisner": {
      "lng": -96.915994,
      "lat": 41.988069
    },
    "Amoskeag": {
      "lng": -71.47208,
      "lat": 43.00223
    },
    "Antrim Wind": {
      "lng": -72.00635,
      "lat": 43.06435
    },
    "Ayers Island": {
      "lng": -71.717683,
      "lat": 43.597849
    },
    "Berlin Gorham": {
      "lng": -71.1645,
      "lat": 44.3889
    },
    "Bridgewater Power LP": {
      "lng": -71.6585,
      "lat": 43.7155
    },
    "Burgess BioPower": {
      "lng": -71.175278,
      "lat": 44.471944
    },
    "China Mill Hydro": {
      "lng": -71.459866,
      "lat": 43.128712
    },
    "Clement Dam Hydro LLC": {
      "lng": -71.595975,
      "lat": 43.440538
    },
    "Comerford": {
      "lng": -72.000975,
      "lat": 44.325053
    },
    "Crotched Mountain Rehabilitation Center": {
      "lng": -71.87268,
      "lat": 42.971653
    },
    "Dartmouth College Heating Plant": {
      "lng": -72.286735,
      "lat": 43.701916
    },
    "Dodge Falls Associates": {
      "lng": -72.0581,
      "lat": 44.2081
    },
    "Eastman Falls": {
      "lng": -71.6579,
      "lat": 43.447219
    },
    "EHC West Hopkinton": {
      "lng": -71.7483,
      "lat": 43.1897
    },
    "Errol Hydroelectric Project": {
      "lng": -71.12433,
      "lat": 44.786648
    },
    "Garvins Falls": {
      "lng": -71.509441,
      "lat": 43.165524
    },
    "General Electric Great Falls Upper Hydro": {
      "lng": -70.8597,
      "lat": 43.2597
    },
    "Gorham": {
      "lng": -71.195354,
      "lat": 44.408278
    },
    "Granite Reliable Power": {
      "lng": -71.2925,
      "lat": 44.704444
    },
    "Granite Ridge Energy": {
      "lng": -71.4261,
      "lat": 42.9042
    },
    "Gregg Falls": {
      "lng": -71.5679,
      "lat": 43.0168
    },
    "Groton Wind LLC": {
      "lng": -71.8189,
      "lat": 43.7681
    },
    "Hampton Facility": {
      "lng": -70.8406,
      "lat": 42.9383
    },
    "Hillsborough Hosiery": {
      "lng": -71.894001,
      "lat": 43.11391
    },
    "Hooksett": {
      "lng": -71.465662,
      "lat": 43.101699
    },
    "Indeck Energy-Alexandria": {
      "lng": -71.780556,
      "lat": 43.560833
    },
    "Jackman": {
      "lng": -71.94889,
      "lat": 43.11055
    },
    "Jericho Power": {
      "lng": -71.225,
      "lat": 44.466944
    },
    "Lempster Wind LLC": {
      "lng": -72.1455,
      "lat": 43.233
    },
    "Lochmere Hydroelectric Plant": {
      "lng": -71.533623,
      "lat": 43.473238
    },
    "Lost Nation": {
      "lng": -71.494398,
      "lat": 44.595117
    },
    "Lower Village Water Power Project": {
      "lng": -72.347603,
      "lat": 43.374292
    },
    "Mascoma Hydro": {
      "lng": -72.317162,
      "lat": 43.633373
    },
    "Merrimack": {
      "lng": -71.4692,
      "lat": 43.1411
    },
    "Merrimack Solar Farm, LLC": {
      "lng": -71.501167,
      "lat": 42.811381
    },
    "Milton Hydro": {
      "lng": -70.98647,
      "lat": 43.403087
    },
    "Mine Falls Generating Station": {
      "lng": -71.505278,
      "lat": 42.750278
    },
    "Nashua Plant": {
      "lng": -71.522418,
      "lat": 42.73227
    },
    "Newfound Hydroelectric": {
      "lng": -71.73295,
      "lat": 43.59
    },
    "Newington": {
      "lng": -70.790833,
      "lat": 43.1
    },
    "Newington Energy": {
      "lng": -70.8061,
      "lat": 43.1047
    },
    "Pembroke Hydro": {
      "lng": -71.453632,
      "lat": 43.130362
    },
    "Penacook Lower Falls": {
      "lng": -71.594962,
      "lat": 43.285761
    },
    "Penacook Upper Falls Hydro": {
      "lng": -71.600827,
      "lat": 43.283208
    },
    "Plymouth State College Cogeneration": {
      "lng": -71.6881,
      "lat": 43.7644
    },
    "Pontook Hydro Facility": {
      "lng": -71.24742,
      "lat": 44.632842
    },
    "Rolfe Canal Hydro": {
      "lng": -71.604027,
      "lat": 43.27456
    },
    "Rollinsford": {
      "lng": -70.817222,
      "lat": 43.236111
    },
    "S C Moore": {
      "lng": -71.8742,
      "lat": 44.3356
    },
    "Schiller": {
      "lng": -70.7842,
      "lat": 43.0978
    },
    "Seabrook": {
      "lng": -70.848889,
      "lat": 42.899167
    },
    "Smith (NH)": {
      "lng": -71.178062,
      "lat": 44.469463
    },
    "Somersworth Lower Great Dam": {
      "lng": -70.840833,
      "lat": 43.251111
    },
    "Stevens Mills Dam": {
      "lng": -71.644646,
      "lat": 43.446107
    },
    "Stored Solar Bethlehem LLC": {
      "lng": -71.68,
      "lat": 44.3274
    },
    "Stored Solar Springfield, LLC": {
      "lng": -72.056,
      "lat": 43.442871
    },
    "Stored Solar Tamworth, LLC": {
      "lng": -71.1967,
      "lat": 43.835833
    },
    "Stored Solar Whitefield, LLC": {
      "lng": -71.5449,
      "lat": 44.358047
    },
    "Turnkey Landfill Gas Recovery": {
      "lng": -70.966404,
      "lat": 43.241555
    },
    "UNH 7.9 MW Plant": {
      "lng": -70.936125,
      "lat": 43.13674
    },
    "Wheelabrator Concord Facility": {
      "lng": -71.5769,
      "lat": 43.2866
    },
    "White Lake": {
      "lng": -71.2069,
      "lat": 43.8478
    },
    "10 Finderne Avenue Solar, LLC": {
      "lng": -74.57594,
      "lat": 40.55812
    },
    "101 Carnegie Center Solar, LLC": {
      "lng": -74.64512,
      "lat": 40.32312
    },
    "12 Applegate Solar LLC": {
      "lng": -74.576111,
      "lat": 40.200278
    },
    "145 Talmadge Solar": {
      "lng": -74.391268,
      "lat": 40.535787
    },
    "180 Raritan Solar": {
      "lng": -74.34,
      "lat": 40.516111
    },
    "24 Applegate Solar LLC": {
      "lng": -74.579722,
      "lat": 40.205833
    },
    "350 Clark Solar, NG, LLC": {
      "lng": -74.730162,
      "lat": 40.894197
    },
    "4 Applegate Solar LLC": {
      "lng": -74.583611,
      "lat": 40.196389
    },
    "435A Bergen Avenue": {
      "lng": -74.131258,
      "lat": 40.751938
    },
    "46 Meadowlands Parkway": {
      "lng": -74.071521,
      "lat": 40.78976
    },
    "510 Carnegie Center": {
      "lng": -74.65551,
      "lat": 40.3147
    },
    "5601 Westside CDG": {
      "lng": -74.04182,
      "lat": 40.77957
    },
    "601 Doremus CDG": {
      "lng": -74.120486,
      "lat": 40.731966
    },
    "701 Carnegie Center": {
      "lng": -74.64994,
      "lat": 40.32358
    },
    "77 Metro Way": {
      "lng": -74.072239,
      "lat": 40.770516
    },
    "Absegami": {
      "lng": -74.516,
      "lat": 39.48
    },
    "AC Criminal Courts Complex SPE2, LLC": {
      "lng": -74.690556,
      "lat": 39.4375
    },
    "AC Landfill Energy LLC": {
      "lng": -74.542222,
      "lat": 39.4175
    },
    "AC Power": {
      "lng": -74.935167,
      "lat": 40.049778
    },
    "ACCC Mays Landing": {
      "lng": -74.68093,
      "lat": 39.460566
    },
    "ACCP NJ 1": {
      "lng": -74.675089,
      "lat": 40.92635
    },
    "ACM - Midtown Thermal": {
      "lng": -74.434228,
      "lat": 39.359831
    },
    "ACUA": {
      "lng": -74.445146,
      "lat": 39.381969
    },
    "Adams Ave MS and HS": {
      "lng": -74.530865,
      "lat": 39.411372
    },
    "Alethea I": {
      "lng": -74.735,
      "lat": 40.000556
    },
    "Allied Beverages Elizabeth": {
      "lng": -74.175226,
      "lat": 40.665678
    },
    "Altice - Parsippany": {
      "lng": -74.428245,
      "lat": 40.845086
    },
    "Amazon": {
      "lng": -74.221389,
      "lat": 40.566389
    },
    "American Goldfinch SCS ACY2 NJ, LLC": {
      "lng": -74.84089,
      "lat": 40.08556
    },
    "AT&T Middletown": {
      "lng": -74.134837,
      "lat": 40.397872
    },
    "Atlantic City Convention Center": {
      "lng": -74.4408,
      "lat": 39.3639
    },
    "Atlantic Coast Freezers Solar Facility": {
      "lng": -75.029038,
      "lat": 39.519935
    },
    "Augusta Solar Farm": {
      "lng": -74.731032,
      "lat": 41.099995
    },
    "Barrette Outdoor Living, Inc.": {
      "lng": -74.60035,
      "lat": 39.4877
    },
    "Bayonne Energy Center": {
      "lng": -74.09155,
      "lat": 40.652834
    },
    "Bayonne MUA- Leitner-Poma Wind Turbine": {
      "lng": -74.117778,
      "lat": 40.652778
    },
    "Bayville Central Facility": {
      "lng": -74.178041,
      "lat": 39.909424
    },
    "Bayway Refinery": {
      "lng": -74.221219,
      "lat": 40.637554
    },
    "BBB Corporate Headquarters": {
      "lng": -74.305,
      "lat": 40.697222
    },
    "Beaver Run": {
      "lng": -74.638077,
      "lat": 41.152955
    },
    "Bed Bath & Beyond DC Port Reading NJ": {
      "lng": -74.2425,
      "lat": 40.568333
    },
    "Ben Moreell Solar Farm": {
      "lng": -74.1,
      "lat": 40.28
    },
    "Benjamin Moore & Co. Solar": {
      "lng": -74.714722,
      "lat": 40.825556
    },
    "Bergen Generating Station": {
      "lng": -74.0244,
      "lat": 40.8375
    },
    "Bergenmand Solar Partners, LLC Mahwah": {
      "lng": -74.150011,
      "lat": 41.079073
    },
    "Bernards Solar": {
      "lng": -74.58158,
      "lat": 40.70266
    },
    "Berry Plastics Solar": {
      "lng": -75.151944,
      "lat": 40.698333
    },
    "BJ's Wholesale Club Inc Dist Center": {
      "lng": -74.825925,
      "lat": 40.098458
    },
    "BlackRock-Matrix": {
      "lng": -74.4797,
      "lat": 40.3622
    },
    "BNB Camden Solar": {
      "lng": -75.10802,
      "lat": 39.93903
    },
    "Bordentown Solar": {
      "lng": -74.7766,
      "lat": 40.0389
    },
    "Brickyard Solar": {
      "lng": -74.154483,
      "lat": 40.189425
    },
    "Bridgeport Disposal Solar Farm": {
      "lng": -75.350833,
      "lat": 39.7875
    },
    "Bristol Myers Squibb Lawrenceville": {
      "lng": -74.707778,
      "lat": 40.324722
    },
    "Bristol Myers Squibb New Brunswick": {
      "lng": -74.446199,
      "lat": 40.475724
    },
    "Burlington Coat Factory Solar": {
      "lng": -74.807687,
      "lat": 40.09283
    },
    "Burlington Generating Station": {
      "lng": -74.879172,
      "lat": 40.076611
    },
    "Caldwell Wastewater Treatment Plant Hybrid": {
      "lng": -74.312476,
      "lat": 40.838568
    },
    "Calpine Vineland Solar LLC": {
      "lng": -75.0667,
      "lat": 39.4636
    },
    "Camden Plant Holding, LLC": {
      "lng": -75.1192,
      "lat": 39.9175
    },
    "Camden Resource Recovery Facility": {
      "lng": -75.1169,
      "lat": 39.9092
    },
    "Campus Drive Solar": {
      "lng": -74.880941,
      "lat": 40.056213
    },
    "Carlls Corner Energy Center": {
      "lng": -75.2014,
      "lat": 39.4547
    },
    "Carneys Point": {
      "lng": -75.485811,
      "lat": 39.693494
    },
    "Carrier Clinic": {
      "lng": -74.685,
      "lat": 40.47
    },
    "Cascades": {
      "lng": -74.45646,
      "lat": 40.55879
    },
    "CCG Marketing": {
      "lng": -74.302956,
      "lat": 40.859343
    },
    "CCMUA": {
      "lng": -75.1275,
      "lat": 39.923056
    },
    "CCUA Solar": {
      "lng": -75.235661,
      "lat": 39.415336
    },
    "Cedar Branch": {
      "lng": -74.964482,
      "lat": 39.503337
    },
    "Cedar Creek": {
      "lng": -80.8756,
      "lat": 34.5414
    },
    "CentraState Medical Center PV Facility": {
      "lng": -74.321389,
      "lat": 40.236111
    },
    "CES Cherry Hill Solar": {
      "lng": -75.026208,
      "lat": 39.941853
    },
    "Chanel Piscataway Rooftop": {
      "lng": -74.482969,
      "lat": 40.550342
    },
    "Cherry Hill": {
      "lng": -75.008611,
      "lat": 39.876111
    },
    "Christmas Tree Shops DC Burlington NJ": {
      "lng": -74.812778,
      "lat": 40.104167
    },
    "Cinnaminson Landfill Solar": {
      "lng": -74.98355,
      "lat": 40.01583
    },
    "Cinnamon Bay Edgeboro Landfill": {
      "lng": -74.393889,
      "lat": 40.465278
    },
    "CIP II/AR Bridgewater Holdings - NJCOE": {
      "lng": -74.634,
      "lat": 40.624
    },
    "Clayville": {
      "lng": -75.02,
      "lat": 39.424444
    },
    "College of New Jersey": {
      "lng": -74.773333,
      "lat": 40.269444
    },
    "Colonial Pipeline Allentown Chesterfield": {
      "lng": -74.657754,
      "lat": 40.152302
    },
    "Colonial Pipeline Woodbury West Deptford": {
      "lng": -75.196874,
      "lat": 39.817319
    },
    "Community Foodbank of NJ": {
      "lng": -74.212235,
      "lat": 40.697259
    },
    "Connell East LLC": {
      "lng": -74.407814,
      "lat": 40.666976
    },
    "Constellation New Energy Inc": {
      "lng": -74.818056,
      "lat": 39.928889
    },
    "Cornerstone Power Holmdel LLC": {
      "lng": -74.187778,
      "lat": 40.352222
    },
    "Cornerstone Power Vineland I LLC": {
      "lng": -75.043889,
      "lat": 39.439722
    },
    "Corning Pharmaceutical Glass": {
      "lng": -75.018611,
      "lat": 39.496667
    },
    "County College of Morris": {
      "lng": -74.581389,
      "lat": 40.856667
    },
    "Covanta Essex Company": {
      "lng": -74.1255,
      "lat": 40.7376
    },
    "Cranbury": {
      "lng": -74.499167,
      "lat": 40.314722
    },
    "Cumberland County Solid Waste Complex": {
      "lng": -75.0928,
      "lat": 39.4494
    },
    "Cumberland Energy Center": {
      "lng": -74.9654,
      "lat": 39.3757
    },
    "Day Four Solar LLC": {
      "lng": -74.556,
      "lat": 40.053
    },
    "DCO Burlington": {
      "lng": -74.826404,
      "lat": 40.108316
    },
    "Delanco": {
      "lng": -74.9353,
      "lat": 40.04936
    },
    "Delilah Road Landfill": {
      "lng": -74.543889,
      "lat": 39.416111
    },
    "Deutsche Bank- Piscataway Solar": {
      "lng": -74.4964,
      "lat": 40.545755
    },
    "DG Amaze ACY1": {
      "lng": -75.216293,
      "lat": 39.822945
    },
    "DG Camden LLC Holtec": {
      "lng": -75.1186,
      "lat": 39.910969
    },
    "DG Florham Park Solar LLC": {
      "lng": -74.427242,
      "lat": 40.777783
    },
    "DG Infineum": {
      "lng": -74.234881,
      "lat": 40.640209
    },
    "DG Iron Mountain": {
      "lng": -74.347394,
      "lat": 40.52132
    },
    "DG New Jersey Solar RLS Logistics": {
      "lng": -75.014147,
      "lat": 39.575217
    },
    "Dix Solar, L.L.C": {
      "lng": -74.621862,
      "lat": 39.977433
    },
    "Dow Jones South Brunswick Solar": {
      "lng": -74.58685,
      "lat": 40.369772
    },
    "DRPA Lindenwold Station Solar Project": {
      "lng": -74.999234,
      "lat": 39.831654
    },
    "DSM Nutritional Products Solar": {
      "lng": -75.065465,
      "lat": 40.836806
    },
    "DSM Solar": {
      "lng": -75.065155,
      "lat": 40.843699
    },
    "DTE Atlantic, LLC": {
      "lng": -74.4369,
      "lat": 39.3733
    },
    "E F Kenilworth, Inc.": {
      "lng": -74.2744,
      "lat": 40.6781
    },
    "Eagle Point Power Generation": {
      "lng": -75.1592,
      "lat": 39.8746
    },
    "East Amwell": {
      "lng": -74.876373,
      "lat": 40.443266
    },
    "East Orange Solar": {
      "lng": -74.335762,
      "lat": 40.747582
    },
    "EDF Ph1 Toms River": {
      "lng": -74.238,
      "lat": 39.984
    },
    "EFS Parlin Holdings, LLC": {
      "lng": -74.327161,
      "lat": 40.460717
    },
    "Elmwood Power LLC": {
      "lng": -74.130448,
      "lat": 40.905337
    },
    "Engelhard Solar LLC": {
      "lng": -74.4625,
      "lat": 40.33
    },
    "EQX002 Secaucus Rd Fuel Cell": {
      "lng": -74.070335,
      "lat": 40.77536
    },
    "EQX003 Secaucus Rd Fuel Cell": {
      "lng": -74.07224,
      "lat": 40.77853
    },
    "EQX012 Harz Fuel Cell": {
      "lng": -74.07591,
      "lat": 40.77782
    },
    "Essex": {
      "lng": -74.1206,
      "lat": 40.7372
    },
    "Essex County Correctional Facility Cogen": {
      "lng": -74.1263,
      "lat": 40.7205
    },
    "ETS Ewing Solar Facility": {
      "lng": -74.793638,
      "lat": 40.281801
    },
    "FedEx Woodbridge": {
      "lng": -74.323519,
      "lat": 40.506358
    },
    "Flemington Solar": {
      "lng": -74.856666,
      "lat": 40.483888
    },
    "Florence Solar W3-080, LLC": {
      "lng": -74.787611,
      "lat": 40.095611
    },
    "Forked River Power": {
      "lng": -74.2098,
      "lat": 39.8154
    },
    "Frankford Solar": {
      "lng": -74.701645,
      "lat": 41.166566
    },
    "Freeze Solar": {
      "lng": -74.527849,
      "lat": 40.376292
    },
    "French's Landfill PV": {
      "lng": -74.130198,
      "lat": 40.109677
    },
    "Frenchtown I Solar": {
      "lng": -75.012222,
      "lat": 40.521667
    },
    "Frenchtown II Solar": {
      "lng": -74.976944,
      "lat": 40.514444
    },
    "Frenchtown III Solar": {
      "lng": -75.018056,
      "lat": 40.4825
    },
    "G&S Wantage Solar LLC": {
      "lng": -74.591667,
      "lat": 41.189167
    },
    "Galloway Landfill": {
      "lng": -74.023243,
      "lat": 40.172415
    },
    "Garfield Solar": {
      "lng": -74.122229,
      "lat": 40.888632
    },
    "Gemini Technologies Services, Inc. Solar": {
      "lng": -74.467582,
      "lat": 40.848445
    },
    "Gilbert Generating Station": {
      "lng": -75.163889,
      "lat": 40.565833
    },
    "Glopak Solar PV Power Plant": {
      "lng": -74.39281,
      "lat": 40.575378
    },
    "Gloucester Community College Solar": {
      "lng": -75.122887,
      "lat": 39.780789
    },
    "Goya Foods, Inc- Jersey City Solar": {
      "lng": -74.065394,
      "lat": 40.760069
    },
    "Goya Foods, Inc- Secaucus Solar": {
      "lng": -74.077397,
      "lat": 40.77312
    },
    "Great Falls Hydro Project": {
      "lng": -74.180986,
      "lat": 40.915319
    },
    "GSPP Onyx New Brunswick LLC": {
      "lng": -74.48185,
      "lat": 40.48071
    },
    "Hall's Warehouse Solar Project": {
      "lng": -74.394414,
      "lat": 40.573708
    },
    "Hamilton Solar-Crosswicks": {
      "lng": -74.630095,
      "lat": 40.168958
    },
    "Hanover": {
      "lng": -74.590556,
      "lat": 40.065278
    },
    "Harmon DC Totowa NJ": {
      "lng": -74.247778,
      "lat": 40.903889
    },
    "Harmony": {
      "lng": -75.170955,
      "lat": 40.755289
    },
    "Hartz Way": {
      "lng": -74.072782,
      "lat": 40.782829
    },
    "Haworth Water Treatment Plant": {
      "lng": -74.015107,
      "lat": 40.959257
    },
    "Heller 400M": {
      "lng": -74.3878,
      "lat": 40.4987
    },
    "Heller Industrial Parks": {
      "lng": -74.489236,
      "lat": 40.373898
    },
    "Highland Park Borough Solar": {
      "lng": -74.41746,
      "lat": 40.4966
    },
    "Hoffmann LaRoche": {
      "lng": -74.155,
      "lat": 40.8359
    },
    "Holland Solar": {
      "lng": -75.108622,
      "lat": 40.599743
    },
    "Hopewell Cogeneration NJ": {
      "lng": -74.771501,
      "lat": 40.348275
    },
    "Hopewell Valley High School Hybrid": {
      "lng": -74.804896,
      "lat": 40.328005
    },
    "Howard M Down": {
      "lng": -75.0347,
      "lat": 39.4889
    },
    "Howell Solar": {
      "lng": -74.167516,
      "lat": 40.147009
    },
    "Hunterdon Health System Solar Project": {
      "lng": -74.908,
      "lat": 40.652
    },
    "IFF Hazlet": {
      "lng": -74.160147,
      "lat": 40.43819
    },
    "IFF Union Beach Project": {
      "lng": -74.154627,
      "lat": 40.446574
    },
    "IGS FE Trenton, LLC": {
      "lng": -74.65676,
      "lat": 40.18933
    },
    "IGS Solar I - EWR5": {
      "lng": -74.255,
      "lat": 40.582
    },
    "IGS Solar I - EWR6": {
      "lng": -74.257,
      "lat": 40.583
    },
    "IIV000 Mt Bethel Fuel Cell": {
      "lng": -74.50348,
      "lat": 40.63251
    },
    "IKEA Westhampton 061": {
      "lng": -74.853611,
      "lat": 40.013889
    },
    "ILR Landfill": {
      "lng": -74.383616,
      "lat": 40.470539
    },
    "Imclone Solar Electric Facility": {
      "lng": -74.709444,
      "lat": 40.555
    },
    "IOS - ERW9": {
      "lng": -74.225,
      "lat": 40.595
    },
    "ISH Solar Central, LLC": {
      "lng": -74.278611,
      "lat": 40.2675
    },
    "Jackson Board of Education-Liberty HS": {
      "lng": -74.262001,
      "lat": 40.092672
    },
    "Jackson Legler Solar 1 LLC": {
      "lng": -74.35037,
      "lat": 40.06451
    },
    "Jacobstown": {
      "lng": -74.582429,
      "lat": 40.063743
    },
    "Jefferson Avenue": {
      "lng": -74.068674,
      "lat": 40.772215
    },
    "Jersey City DWP": {
      "lng": -74.087431,
      "lat": 40.690025
    },
    "Jersey Gardens Phase 1": {
      "lng": -74.173333,
      "lat": 40.660833
    },
    "Jersey Gardens Phase 2": {
      "lng": -74.173333,
      "lat": 40.660833
    },
    "Jersey-Atlantic Wind Farm": {
      "lng": -74.4475,
      "lat": 39.382222
    },
    "JMB Mcguire-Dix-Lakehurst Solar Project": {
      "lng": -74.636933,
      "lat": 39.992818
    },
    "Johnson Matthey, Inc. Solar": {
      "lng": -75.219722,
      "lat": 39.825278
    },
    "Jordache Enterprises Solar": {
      "lng": -74.40836,
      "lat": 40.55569
    },
    "Junction Road": {
      "lng": -74.843398,
      "lat": 40.5237
    },
    "KDC Solar ASGM": {
      "lng": -75.218545,
      "lat": 39.409744
    },
    "KDC Solar CSCP LLC": {
      "lng": -74.314421,
      "lat": 40.237803
    },
    "KDC Solar PR1, LLC": {
      "lng": -74.439668,
      "lat": 40.136696
    },
    "Kearny Generating Station": {
      "lng": -74.096464,
      "lat": 40.737283
    },
    "Kinsley II Landfill Solar System": {
      "lng": -75.11138,
      "lat": 39.79271
    },
    "Kinsley Landfill Solar": {
      "lng": -75.106389,
      "lat": 39.793333
    },
    "L&D Landfill Solar": {
      "lng": -74.770278,
      "lat": 39.980278
    },
    "LabCorp Engine": {
      "lng": -74.63951,
      "lat": 40.570448
    },
    "Lakehurst Solar": {
      "lng": -74.312,
      "lat": 40.024
    },
    "Lakewood": {
      "lng": -74.1686,
      "lat": 40.0613
    },
    "Lakewood Cheddar School": {
      "lng": -74.1808,
      "lat": 40.0709
    },
    "Lebanon Solar": {
      "lng": -74.912222,
      "lat": 40.675833
    },
    "Linden Cogeneration Facility": {
      "lng": -74.2156,
      "lat": 40.6322
    },
    "Linden Generating Station": {
      "lng": -74.2072,
      "lat": 40.6217
    },
    "Linden Hawk Rise Solar CSG": {
      "lng": -74.24549,
      "lat": 40.60395
    },
    "Linden Solar Farm": {
      "lng": -74.2144,
      "lat": 40.62
    },
    "Livingston Solar Canopies": {
      "lng": -74.438333,
      "lat": 40.5275
    },
    "Livingston Solar Farm": {
      "lng": -74.43,
      "lat": 40.523611
    },
    "Logan Generating Plant": {
      "lng": -75.406667,
      "lat": 39.7928
    },
    "L'Oreal Franklin": {
      "lng": -74.521366,
      "lat": 40.487906
    },
    "L'Oreal Monmouth": {
      "lng": -74.504689,
      "lat": 40.400273
    },
    "L'Oreal Piscataway": {
      "lng": -74.468014,
      "lat": 40.554341
    },
    "Lower Cape May HS": {
      "lng": -74.906513,
      "lat": 38.976718
    },
    "LTMUA": {
      "lng": -74.93556,
      "lat": 39.00776
    },
    "Manalapan Village Solar": {
      "lng": -74.349722,
      "lat": 40.256667
    },
    "Manheim New Jersey": {
      "lng": -74.70629,
      "lat": 40.11359
    },
    "Maple Solar": {
      "lng": -74.980833,
      "lat": 39.4875
    },
    "Mars Wrigley Confectionery US, LLC": {
      "lng": -74.825,
      "lat": 40.8625
    },
    "Matrix Buildings A&B (Perth Amboy) Solar": {
      "lng": -74.269998,
      "lat": 40.534393
    },
    "Matrix Stults Road Solar Facility": {
      "lng": -74.4797,
      "lat": 40.3622
    },
    "McCullough Road Solar Farm": {
      "lng": -74.958162,
      "lat": 40.757615
    },
    "McGraw Hill Solar": {
      "lng": -74.556389,
      "lat": 40.279722
    },
    "McKee City Solar Phase 2": {
      "lng": -74.639167,
      "lat": 39.421944
    },
    "Medford WWTP": {
      "lng": -74.806667,
      "lat": 39.922222
    },
    "Mercer County Community College": {
      "lng": -74.645278,
      "lat": 40.255833
    },
    "Mercer Mall": {
      "lng": -74.681944,
      "lat": 40.290556
    },
    "Merck Rahway Power Plant": {
      "lng": -74.26529,
      "lat": 40.615616
    },
    "Mickleton Energy Center": {
      "lng": -75.249031,
      "lat": 39.8119
    },
    "Middlesex Apple Orchard Solar": {
      "lng": -74.492222,
      "lat": 40.426389
    },
    "Middlesex County Utilities Authority": {
      "lng": -74.3156,
      "lat": 40.4903
    },
    "Milford Solar Farm (NJ) LLC": {
      "lng": -75.156486,
      "lat": 40.614955
    },
    "Milhurst Solar": {
      "lng": -74.380834,
      "lat": 40.254975
    },
    "Mill Creek Solar": {
      "lng": -74.824722,
      "lat": 40.031388
    },
    "Millville City Sewer Auth WTP": {
      "lng": -75.033056,
      "lat": 39.376389
    },
    "Monroe Solar Farm, LLC": {
      "lng": -74.425,
      "lat": 40.312
    },
    "Montclair State University Cogen": {
      "lng": -74.196651,
      "lat": 40.865207
    },
    "Mount Laurel Solar": {
      "lng": -74.888716,
      "lat": 39.975579
    },
    "Mt Arlington Solar 1 LLC": {
      "lng": -74.62377,
      "lat": 40.93207
    },
    "Munich Re Plaza": {
      "lng": -74.59268,
      "lat": 40.350341
    },
    "Murray Hill Solar": {
      "lng": -74.404444,
      "lat": 40.684444
    },
    "NES Rutgers Solar": {
      "lng": -74.161153,
      "lat": 40.070879
    },
    "New Road Solar, LLC": {
      "lng": -74.567786,
      "lat": 40.403238
    },
    "Newark Bay Cogen": {
      "lng": -74.13,
      "lat": 40.7197
    },
    "Newark Energy Center": {
      "lng": -74.125833,
      "lat": 40.707222
    },
    "NHA at Mansfield NJ": {
      "lng": -74.935,
      "lat": 40.76
    },
    "NJ Oak Solar Plant": {
      "lng": -75.188312,
      "lat": 39.380565
    },
    "NJMC Landfill": {
      "lng": -74.11,
      "lat": 40.750833
    },
    "North Bergen Solar": {
      "lng": -74.017,
      "lat": 40.818
    },
    "North Jersey Media Group Solar Facility": {
      "lng": -74.494722,
      "lat": 40.924167
    },
    "North Run": {
      "lng": -74.584842,
      "lat": 40.069601
    },
    "NorthPark Solar": {
      "lng": -74.441969,
      "lat": 40.257726
    },
    "Ocean County Landfill": {
      "lng": -74.250137,
      "lat": 40.024937
    },
    "Ocean Peaking Power": {
      "lng": -74.1672,
      "lat": 40.0633
    },
    "Old Bridge Solar Farm": {
      "lng": -74.243376,
      "lat": 40.444372
    },
    "Overlook Medical Center": {
      "lng": -74.354231,
      "lat": 40.711684
    },
    "Owens Corning": {
      "lng": -75.052996,
      "lat": 39.838784
    },
    "Paradise Solar Energy Center": {
      "lng": -75.2225,
      "lat": 39.835833
    },
    "Parkland Landfill Solar": {
      "lng": -74.710833,
      "lat": 40.1275
    },
    "Parlin Solar LLC": {
      "lng": -74.328889,
      "lat": 40.456667
    },
    "Passaic Valley Water Commission": {
      "lng": -74.22999,
      "lat": 40.883345
    },
    "Patriots Stadium": {
      "lng": -74.551389,
      "lat": 40.560556
    },
    "Paulsboro Refining Company LLC": {
      "lng": -75.2583,
      "lat": 39.84
    },
    "Pedricktown Cogeneration Plant": {
      "lng": -75.4238,
      "lat": 39.7668
    },
    "Pemberton Road I": {
      "lng": -74.697369,
      "lat": 39.981764
    },
    "Pemberton Road II": {
      "lng": -74.701917,
      "lat": 39.979936
    },
    "Pennsauken Brownfield Solar": {
      "lng": -75.07949,
      "lat": 39.96307
    },
    "Pennsauken Landfill": {
      "lng": -75.04,
      "lat": 39.9894
    },
    "Pennsauken Solar": {
      "lng": -75.04,
      "lat": 39.9894
    },
    "Pfizer Peapack Solar": {
      "lng": -74.663434,
      "lat": 40.711882
    },
    "Pilesgrove": {
      "lng": -75.309477,
      "lat": 39.613028
    },
    "Pittsgrove Solar": {
      "lng": -75.179342,
      "lat": 39.539042
    },
    "Plainfield One Solar LLC": {
      "lng": -74.439444,
      "lat": 40.605
    },
    "Plumsted 537 LLC": {
      "lng": -74.497779,
      "lat": 40.1094
    },
    "Pohatcong Solar Farm": {
      "lng": -75.16,
      "lat": 40.67
    },
    "PPL Renewable Energy Merck Solar": {
      "lng": -74.3783,
      "lat": 40.727386
    },
    "Princeton Energy Center, LLC": {
      "lng": -74.62393,
      "lat": 40.339838
    },
    "Princeton Solar Project": {
      "lng": -74.625694,
      "lat": 40.379736
    },
    "Princeton University Cogeneration": {
      "lng": -74.657222,
      "lat": 40.341389
    },
    "Prudential 55 Livingston Roseland Solar": {
      "lng": -74.297098,
      "lat": 40.814586
    },
    "Prudential 80 Livingston Roseland Solar": {
      "lng": -74.304989,
      "lat": 40.815363
    },
    "PSEG Hackettstown": {
      "lng": -74.822935,
      "lat": 40.860629
    },
    "PSEG Hope Creek Generating Station": {
      "lng": -75.5377,
      "lat": 39.4666
    },
    "Quakertown Solar Farm, LLC": {
      "lng": -74.944168,
      "lat": 40.522818
    },
    "Raritan ITS": {
      "lng": -74.654167,
      "lat": 40.574444
    },
    "Raritan Solar": {
      "lng": -74.857879,
      "lat": 40.464221
    },
    "Raritan Solar - 53 Highway": {
      "lng": -74.863033,
      "lat": 40.453864
    },
    "Rariton OMP": {
      "lng": -74.648056,
      "lat": 40.574444
    },
    "RB Manufacturing LLC Belle Mead NJ": {
      "lng": -74.650123,
      "lat": 40.483601
    },
    "Red Oak Power, LLC": {
      "lng": -74.348879,
      "lat": 40.448674
    },
    "Reeves South": {
      "lng": -74.813611,
      "lat": 39.926667
    },
    "Reeves Station Rd East": {
      "lng": -74.814444,
      "lat": 39.928333
    },
    "River Terminal Development Solar": {
      "lng": -74.1142,
      "lat": 40.7278
    },
    "Riverside Renewable Energy LLC": {
      "lng": -75.125833,
      "lat": 39.903056
    },
    "Rock Solid": {
      "lng": -74.225818,
      "lat": 40.229725
    },
    "Rotor Clip": {
      "lng": -74.532375,
      "lat": 40.539051
    },
    "Royal Wine Corp Solar Power Plant": {
      "lng": -74.100182,
      "lat": 40.66362
    },
    "Rutgers Biomedical and Health Cogen": {
      "lng": -74.187633,
      "lat": 40.73928
    },
    "Sabert Solar": {
      "lng": -74.320933,
      "lat": 40.475477
    },
    "Salem Generating Station": {
      "lng": -75.5358,
      "lat": 39.4625
    },
    "Samuel Mickle School": {
      "lng": -75.24615,
      "lat": 39.78736
    },
    "Sayreville": {
      "lng": -74.352336,
      "lat": 40.476525
    },
    "Sayreville Power LP": {
      "lng": -74.3444,
      "lat": 40.439
    },
    "Sayreville Solar Project": {
      "lng": -74.336,
      "lat": 40.438
    },
    "SCS Randolph 012175 Somerset, LLC": {
      "lng": -74.558799,
      "lat": 40.54009
    },
    "Seabrook Solar Plant": {
      "lng": -75.215,
      "lat": 39.485
    },
    "Seabrook Village": {
      "lng": -74.087225,
      "lat": 40.234177
    },
    "Seaside Heights Power Plant": {
      "lng": -74.078611,
      "lat": 39.943333
    },
    "Selective Insurance": {
      "lng": -74.744444,
      "lat": 41.152222
    },
    "Sewaren Generating Station": {
      "lng": -74.2469,
      "lat": 40.5558
    },
    "Sharon Station": {
      "lng": -74.539003,
      "lat": 40.182623
    },
    "Sherman Avenue": {
      "lng": -75.0578,
      "lat": 39.451
    },
    "Shore Point Solar": {
      "lng": -74.243333,
      "lat": 40.245
    },
    "Short Hills Mall": {
      "lng": -74.3642,
      "lat": 40.7397
    },
    "Silver Lake Solar Farm": {
      "lng": -74.399511,
      "lat": 40.494804
    },
    "Silvi Gibraltar Rock": {
      "lng": -74.717077,
      "lat": 40.450916
    },
    "Skillman": {
      "lng": -74.648056,
      "lat": 40.574444
    },
    "Solar Star New Jersey NJ LLC": {
      "lng": -74.669697,
      "lat": 40.401528
    },
    "Solvay Solar": {
      "lng": -75.212019,
      "lat": 39.846549
    },
    "Spartan": {
      "lng": -74.616461,
      "lat": 40.047769
    },
    "Springfield Solar Project": {
      "lng": -74.647387,
      "lat": 40.032087
    },
    "St.Joseph's Regional Medical Center": {
      "lng": -74.1656,
      "lat": 40.9026
    },
    "Stockton Athletic Center": {
      "lng": -74.535263,
      "lat": 39.488443
    },
    "Stryker 22, L.L.C.": {
      "lng": -75.144467,
      "lat": 40.66538
    },
    "Summit Associates": {
      "lng": -74.3536,
      "lat": 40.5003
    },
    "Summit Water Nexus Mt. Holly, LLC Solar": {
      "lng": -74.801807,
      "lat": 39.987281
    },
    "The City of Vineland at North Vineland": {
      "lng": -74.997703,
      "lat": 39.505034
    },
    "The City of Vineland at West Vineland": {
      "lng": -75.070039,
      "lat": 39.462376
    },
    "The Lawrenceville School Solar Facility": {
      "lng": -74.722117,
      "lat": 40.301409
    },
    "Thermo Fisher": {
      "lng": -74.62947,
      "lat": 40.5737
    },
    "Timber Creek HS": {
      "lng": -75.014791,
      "lat": 39.760172
    },
    "Tinton Falls Solar Farm": {
      "lng": -74.084167,
      "lat": 40.263889
    },
    "Tioga Solar Morris County 1 LLC": {
      "lng": -74.475833,
      "lat": 40.815278
    },
    "Tissington Solar": {
      "lng": -74.491944,
      "lat": 41.200278
    },
    "Titusville Solar": {
      "lng": -74.847222,
      "lat": 40.292778
    },
    "Toys R Us- DE, Inc. at Mt. Olive, NJ": {
      "lng": -74.718611,
      "lat": 40.818888
    },
    "Trenton Solar Farm": {
      "lng": -74.7539,
      "lat": 40.2314
    },
    "Union County Resource Recovery": {
      "lng": -74.266372,
      "lat": 40.601264
    },
    "United Stationers Supply Solar Electric": {
      "lng": -74.501667,
      "lat": 40.320556
    },
    "US Foods Solar": {
      "lng": -74.275153,
      "lat": 40.534414
    },
    "USAA Black Bear Energy": {
      "lng": -74.4847,
      "lat": 40.313573
    },
    "Vicinity Energy Trenton, L.P.": {
      "lng": -74.7667,
      "lat": 40.2167
    },
    "Vineland Headquarters": {
      "lng": -75.037391,
      "lat": 39.490271
    },
    "Vineland Mays Landing Solar": {
      "lng": -74.973889,
      "lat": 39.4175
    },
    "Wakefern Food Corp": {
      "lng": -74.328333,
      "lat": 40.51
    },
    "Warren County Solar": {
      "lng": -75.10697,
      "lat": 40.788957
    },
    "WC Landfill Energy LLC": {
      "lng": -75.015775,
      "lat": 40.831012
    },
    "WEA Texas Bayonne": {
      "lng": -74.0919,
      "lat": 40.6772
    },
    "West Deptford Energy Station": {
      "lng": -75.2214,
      "lat": 39.8414
    },
    "West Pemberton": {
      "lng": -74.730278,
      "lat": 40.006667
    },
    "West Station": {
      "lng": -75.0486,
      "lat": 39.4912
    },
    "Westside Avenue": {
      "lng": -74.021325,
      "lat": 40.814092
    },
    "Wheelabrator Gloucester LP": {
      "lng": -75.1381,
      "lat": 39.8733
    },
    "William G Mennen Sports Solar": {
      "lng": -74.47586,
      "lat": 40.814851
    },
    "William Paterson University": {
      "lng": -74.195502,
      "lat": 40.952084
    },
    "Williams-Sonoma Solar Facility": {
      "lng": -74.455278,
      "lat": 40.387778
    },
    "Wilzig Associates, LLC": {
      "lng": -74.09442,
      "lat": 40.825316
    },
    "Woodbine Landfill Plant": {
      "lng": -74.780833,
      "lat": 39.231111
    },
    "Woodbridge Energy Center": {
      "lng": -74.318889,
      "lat": 40.515
    },
    "Yards Creek Energy": {
      "lng": -75.0314,
      "lat": 41.0006
    },
    "Yardville Solar Farm": {
      "lng": -74.656719,
      "lat": 40.168811
    },
    "Abiquiu Dam": {
      "lng": -106.423043,
      "lat": 36.239886
    },
    "Afton Generating Station": {
      "lng": -106.846483,
      "lat": 32.114185
    },
    "Alamogordo Solar Energy Center": {
      "lng": -105.999775,
      "lat": 32.86571
    },
    "Albuquerque Solar Energy Center": {
      "lng": -106.60005,
      "lat": 35.168957
    },
    "Alcalde Solar Array": {
      "lng": -106.040879,
      "lat": 36.091143
    },
    "Algodones": {
      "lng": -106.464167,
      "lat": 35.384444
    },
    "Alta Luna": {
      "lng": -107.486458,
      "lat": 32.572022
    },
    "Anderson Wind I": {
      "lng": -103.855833,
      "lat": 33.029444
    },
    "Anderson Wind II": {
      "lng": -103.865,
      "lat": 33.013889
    },
    "Aragonne Wind LLC": {
      "lng": -105.0108,
      "lat": 34.8147
    },
    "Blue Sky One": {
      "lng": -105.985278,
      "lat": 36.907778
    },
    "Bluewater CDEC 1": {
      "lng": -107.952296,
      "lat": 35.24102
    },
    "Bluffview Power Plant": {
      "lng": -108.215282,
      "lat": 36.716628
    },
    "Borderlands Wind, LLC": {
      "lng": -108.894458,
      "lat": 34.159842
    },
    "Britton Solar Energy Center": {
      "lng": -106.098,
      "lat": 35.016
    },
    "Broadview Energy JN, LLC": {
      "lng": -103.108047,
      "lat": 34.734179
    },
    "Broadview Energy KW, LLC": {
      "lng": -103.166771,
      "lat": 34.727007
    },
    "Broadview Energy Prime 2 LLC": {
      "lng": -103.305,
      "lat": 34.728056
    },
    "Broadview Energy Prime LLC": {
      "lng": -103.325,
      "lat": 34.746944
    },
    "Caprock Solar 1 LLC": {
      "lng": -103.378333,
      "lat": 34.982778
    },
    "Caprock Wind Farm": {
      "lng": -103.353889,
      "lat": 34.980556
    },
    "Carrizozo Solar": {
      "lng": -105.747825,
      "lat": 33.610306
    },
    "Casa Mesa Wind Energy Center Hybrid": {
      "lng": -103.966089,
      "lat": 34.604989
    },
    "Chaves Solar, LLC": {
      "lng": -104.456,
      "lat": 33.45
    },
    "Chino Mines": {
      "lng": -108.1225,
      "lat": 32.6956
    },
    "Cibola": {
      "lng": -107.245156,
      "lat": 35.20115
    },
    "Cimarron Solar Facility": {
      "lng": -104.635659,
      "lat": 36.468078
    },
    "City of Gallup Solar": {
      "lng": -108.778791,
      "lat": 35.52236
    },
    "City of Rio Rancho WWTP": {
      "lng": -106.806583,
      "lat": 35.179889
    },
    "City of Truth or Consequences PV": {
      "lng": -107.287778,
      "lat": 33.121111
    },
    "Clines Corners Wind Farm LLC": {
      "lng": -105.43676,
      "lat": 34.35655
    },
    "CNMEC Solar Energy Center": {
      "lng": -106.051092,
      "lat": 34.783205
    },
    "Columbus Solar Project": {
      "lng": -107.587513,
      "lat": 31.86673
    },
    "Cunningham": {
      "lng": -103.3533,
      "lat": 32.7131
    },
    "Deming Solar Energy Center": {
      "lng": -107.75056,
      "lat": 32.165833
    },
    "Duran Mesa LLC": {
      "lng": -105.492126,
      "lat": 34.390679
    },
    "El Cabo Wind": {
      "lng": -105.461667,
      "lat": 34.651111
    },
    "El Vado Dam": {
      "lng": -106.73213,
      "lat": 36.592775
    },
    "Elephant Butte": {
      "lng": -107.191973,
      "lat": 33.153358
    },
    "Encino Solar Energy Center": {
      "lng": -106.858,
      "lat": 35.354
    },
    "Eubank Landfill Solar Array": {
      "lng": -106.53,
      "lat": 35.049167
    },
    "Facebook 1 Solar Energy Center": {
      "lng": -106.762,
      "lat": 34.831
    },
    "Facebook 2 Solar Energy Center": {
      "lng": -106.865,
      "lat": 35.08
    },
    "Facebook 3 Solar Energy Center": {
      "lng": -106.865,
      "lat": 35.075
    },
    "Ford Utilities Center": {
      "lng": -106.625191,
      "lat": 35.084078
    },
    "Four Corners Steam Elec Station": {
      "lng": -108.4814,
      "lat": 36.69
    },
    "Four Peaks Camino Real": {
      "lng": -106.59326,
      "lat": 31.811078
    },
    "Freeport McMoRan": {
      "lng": -108.3628,
      "lat": 32.66176
    },
    "Grady Wind Energy Center, LLC": {
      "lng": -103.092227,
      "lat": 34.734142
    },
    "Grants CDEC 2": {
      "lng": -107.821,
      "lat": 35.14
    },
    "GSE NM1": {
      "lng": -104.4,
      "lat": 33.3
    },
    "Hatch Solar Energy Center I, LLC": {
      "lng": -107.259722,
      "lat": 32.63
    },
    "High Lonesome Mesa LLC": {
      "lng": -105.9625,
      "lat": 34.466667
    },
    "Hobbs Generating Station": {
      "lng": -103.309444,
      "lat": 32.728333
    },
    "Holloman Solar Facility": {
      "lng": -106.072411,
      "lat": 32.921261
    },
    "Kit Carson": {
      "lng": -105.438056,
      "lat": 36.966667
    },
    "La Joya NM": {
      "lng": -105.74,
      "lat": 34.81
    },
    "La Luz Energy Center": {
      "lng": -106.815,
      "lat": 34.616111
    },
    "Las Vegas Solar Energy Center": {
      "lng": -105.206667,
      "lat": 35.643333
    },
    "LCEC Generation LLVC": {
      "lng": -103.323889,
      "lat": 32.978056
    },
    "Lightning Dock Geothermal HI-01, LLC": {
      "lng": -108.838333,
      "lat": 32.144722
    },
    "Lordsburg Generating Station": {
      "lng": -108.69799,
      "lat": 32.35051
    },
    "Los Alamos PV Site": {
      "lng": -106.31,
      "lat": 35.875556
    },
    "Los Lunas Solar Energy Center": {
      "lng": -106.772413,
      "lat": 34.833136
    },
    "Luna Energy Facility": {
      "lng": -107.7834,
      "lat": 32.299349
    },
    "Macho Springs": {
      "lng": -107.48,
      "lat": 32.57
    },
    "Macho Springs Power I": {
      "lng": -107.50929,
      "lat": 32.55434
    },
    "Maddox": {
      "lng": -103.301469,
      "lat": 32.714224
    },
    "Manzano Solar": {
      "lng": -106.654444,
      "lat": 34.746667
    },
    "Meadow Lake Solar Energy Center": {
      "lng": -106.52,
      "lat": 34.81
    },
    "Mesalands Comm College Wind Turbine": {
      "lng": -103.74,
      "lat": 35.17
    },
    "Middle Daisy": {
      "lng": -103.371284,
      "lat": 32.978636
    },
    "Milo Wind Project LLC": {
      "lng": -103.34675,
      "lat": 33.901781
    },
    "Navajo Dam": {
      "lng": -107.6131,
      "lat": 36.8061
    },
    "New Mexico State University": {
      "lng": -106.753716,
      "lat": 32.280032
    },
    "New Mexico Wind Energy Center": {
      "lng": -104.04733,
      "lat": 34.63566
    },
    "NMSU Solar and Storage": {
      "lng": -106.744529,
      "lat": 32.286048
    },
    "Oso Grande Wind Farm": {
      "lng": -103.6664,
      "lat": 32.9323
    },
    "Otero Solar": {
      "lng": -105.971111,
      "lat": 32.976389
    },
    "Prosperity Energy Storage Facility Hybrid": {
      "lng": -106.637222,
      "lat": 35.001944
    },
    "Pyramid Generating Station": {
      "lng": -108.5494,
      "lat": 32.2363
    },
    "Quay County": {
      "lng": -103.731111,
      "lat": 35.182778
    },
    "Questa Solar Facility": {
      "lng": -105.60944,
      "lat": 36.716944
    },
    "RC Energy AA LLC Solar Facility": {
      "lng": -106.547222,
      "lat": 35.152721
    },
    "Red Cloud Wind LLC": {
      "lng": -105.42291,
      "lat": 34.274062
    },
    "Red Mesa Wind LLC": {
      "lng": -107.3828,
      "lat": 35.2689
    },
    "Reeves Generating Station": {
      "lng": -106.6019,
      "lat": 35.171
    },
    "Rio Bravo Generating Station": {
      "lng": -106.644,
      "lat": 35.026
    },
    "Rio Communities Solar Energy Center": {
      "lng": -106.682975,
      "lat": 34.582598
    },
    "Rio De Oro Solar Energy Center": {
      "lng": -106.688,
      "lat": 34.692
    },
    "Rio Grande": {
      "lng": -106.5472,
      "lat": 31.8047
    },
    "Rio Rancho High School": {
      "lng": -106.667778,
      "lat": 35.264444
    },
    "Rio Rancho Solar Energy Center": {
      "lng": -106.48237,
      "lat": 35.10476
    },
    "Roadrunner Solar": {
      "lng": -106.673333,
      "lat": 31.792778
    },
    "Roosevelt County": {
      "lng": -103.511111,
      "lat": 33.926111
    },
    "Roswell Solar, LLC": {
      "lng": -104.461074,
      "lat": 33.45293
    },
    "Sagamore Wind": {
      "lng": -103.184611,
      "lat": 33.938421
    },
    "San Juan": {
      "lng": -108.4386,
      "lat": 36.8006
    },
    "San Juan Mesa Wind Project LLC": {
      "lng": -103.8604,
      "lat": 33.9716
    },
    "San Miguel I Solar Energy Center": {
      "lng": -105.1784,
      "lat": 35.63457
    },
    "San Miguel II Solar Energy Center": {
      "lng": -105.1732,
      "lat": 35.63471
    },
    "Sandoval Solar Energy Center": {
      "lng": -106.812778,
      "lat": 35.283611
    },
    "Santa Fe Solar Energy Center": {
      "lng": -106.060859,
      "lat": 35.587795
    },
    "Santolina Solar Energy Center": {
      "lng": -106.855498,
      "lat": 35.029719
    },
    "SEV NM Phase 2": {
      "lng": -104.506944,
      "lat": 33.351111
    },
    "Silver City WWTP PV Project": {
      "lng": -108.248056,
      "lat": 32.713889
    },
    "SoCore Clovis 1": {
      "lng": -103.254078,
      "lat": 34.423035
    },
    "South Valley Solar Energy Center": {
      "lng": -106.736129,
      "lat": 34.986073
    },
    "Southside Water Reclamation Plant": {
      "lng": -106.669722,
      "lat": 35.016111
    },
    "Springer Solar 1": {
      "lng": -104.594167,
      "lat": 36.398056
    },
    "SPS1 Dollarhide": {
      "lng": -103.137825,
      "lat": 32.12743
    },
    "SPS2 Jal": {
      "lng": -103.183056,
      "lat": 32.183611
    },
    "SPS3 Lea": {
      "lng": -103.323333,
      "lat": 32.469722
    },
    "SPS4 Monument": {
      "lng": -103.303611,
      "lat": 32.681944
    },
    "SPS5 Hopi": {
      "lng": -104.234722,
      "lat": 32.345556
    },
    "Sterling I Wind Farm": {
      "lng": -103.25058,
      "lat": 33.3596
    },
    "Storrie Lake Solar Project": {
      "lng": -105.183889,
      "lat": 35.656944
    },
    "Sue Cleveland High School": {
      "lng": -106.657778,
      "lat": 35.313333
    },
    "SunE EPE1 LLC": {
      "lng": -106.340986,
      "lat": 32.034383
    },
    "SunE EPE2 LLC": {
      "lng": -106.913333,
      "lat": 32.251667
    },
    "Syncarpha Eagle Nest": {
      "lng": -105.2772,
      "lat": 36.544166
    },
    "Syncarpha El Rito": {
      "lng": -106.190105,
      "lat": 36.331724
    },
    "Syncarpha Questa": {
      "lng": -105.588421,
      "lat": 36.810668
    },
    "Syncarpha Taos": {
      "lng": -105.660152,
      "lat": 36.371326
    },
    "Tecolote Wind LLC": {
      "lng": -105.44,
      "lat": 34.36
    },
    "Texico Wind Ranch LP": {
      "lng": -103.0667,
      "lat": 34.36
    },
    "Valencia Power Plant": {
      "lng": -106.732234,
      "lat": 34.611549
    },
    "Vista Solar Energy Center": {
      "lng": -106.655,
      "lat": 34.741
    },
    "Wal-Mart Truth or Consequences PV": {
      "lng": -107.249831,
      "lat": 33.1526
    },
    "Wildcat Wind": {
      "lng": -103.285278,
      "lat": 32.952778
    },
    "WSMR I": {
      "lng": -106.47722,
      "lat": 32.395833
    },
    "Apex Generating Station": {
      "lng": -114.960916,
      "lat": 36.415999
    },
    "Apex Solar PV Power Project": {
      "lng": -114.97,
      "lat": 36.39
    },
    "Battle Mountain Solar Project": {
      "lng": -117.06143,
      "lat": 40.718433
    },
    "Beowawe": {
      "lng": -116.6175,
      "lat": 40.5547
    },
    "Boulder Solar II, LLC": {
      "lng": -114.944,
      "lat": 35.85
    },
    "Boulder Solar Power, LLC": {
      "lng": -114.97,
      "lat": 35.348
    },
    "Brady": {
      "lng": -119.010013,
      "lat": 39.796032
    },
    "Brunswick": {
      "lng": -119.7478,
      "lat": 39.1522
    },
    "Chuck Lenzie Generating Station": {
      "lng": -114.921779,
      "lat": 36.383719
    },
    "Chukar Battery Energy Storage System": {
      "lng": -119.43665,
      "lat": 39.55636
    },
    "CityCenter Central Plant Cogen Units": {
      "lng": -115.1794,
      "lat": 36.1039
    },
    "Clark": {
      "lng": -115.0507,
      "lat": 36.0875
    },
    "CM10": {
      "lng": -114.9933,
      "lat": 35.7881
    },
    "CM48": {
      "lng": -114.9933,
      "lat": 35.7814
    },
    "Copper Mountain Solar 2": {
      "lng": -114.962778,
      "lat": 35.786389
    },
    "Copper Mountain Solar 3": {
      "lng": -114.957912,
      "lat": 35.889419
    },
    "Copper Mountain Solar 4, LLC": {
      "lng": -114.975375,
      "lat": 35.78222
    },
    "Copper Mountain Solar 5, LLC": {
      "lng": -114.973676,
      "lat": 35.776286
    },
    "Crescent Dunes Solar Energy": {
      "lng": -117.36361,
      "lat": 38.238889
    },
    "Desert Peak Power Plant": {
      "lng": -118.953497,
      "lat": 39.753999
    },
    "Desert Star Energy Center": {
      "lng": -114.994167,
      "lat": 35.788889
    },
    "Dignity - San Martin": {
      "lng": -115.272131,
      "lat": 36.057181
    },
    "Dignity - Siena Campus": {
      "lng": -115.115305,
      "lat": 36.004293
    },
    "Dixie Valley": {
      "lng": -117.8557,
      "lat": 39.9663
    },
    "Don A Campbell 1 Geothermal": {
      "lng": -118.323889,
      "lat": 38.836667
    },
    "Don A Campbell 2 Geothermal": {
      "lng": -118.325339,
      "lat": 38.835836
    },
    "EGP Stillwater Solar PV II, LLC": {
      "lng": -118.560147,
      "lat": 39.542041
    },
    "ENEL Salt Wells LLC": {
      "lng": -118.5725,
      "lat": 39.2944
    },
    "Fleish": {
      "lng": -119.5933,
      "lat": 39.2851
    },
    "Fort Churchill": {
      "lng": -119.1322,
      "lat": 39.1281
    },
    "Ft. Churchill PV": {
      "lng": -119.14,
      "lat": 39.128056
    },
    "Galena 2 Geothermal Power Plant": {
      "lng": -119.764507,
      "lat": 39.373189
    },
    "Galena 3 Geothermal Power Plant": {
      "lng": -119.748908,
      "lat": 39.388722
    },
    "Goodsprings Waste Heat Recovery": {
      "lng": -115.411389,
      "lat": 35.809444
    },
    "Harry Allen": {
      "lng": -114.902405,
      "lat": 36.430554
    },
    "Harry Allen Solar Energy LLC": {
      "lng": -114.9471,
      "lat": 36.43395
    },
    "Hoover Dam (NV)": {
      "lng": -114.738006,
      "lat": 36.01551
    },
    "IKEA Las Vegas 462": {
      "lng": -115.280825,
      "lat": 36.068926
    },
    "Jersey Valley Geothermal Power Plant": {
      "lng": -117.4739,
      "lat": 40.1808
    },
    "Lahontan": {
      "lng": -119.0667,
      "lat": 39.4625
    },
    "Las Vegas Generating Station": {
      "lng": -115.1222,
      "lat": 36.2319
    },
    "Las Vegas WPCF Solar Plant": {
      "lng": -115.035278,
      "lat": 36.134444
    },
    "Luning Energy": {
      "lng": -118.19156,
      "lat": 38.546947
    },
    "McGinness Hills": {
      "lng": -116.9119,
      "lat": 39.5911
    },
    "McGinness Hills 3": {
      "lng": -116.8942,
      "lat": 39.5963
    },
    "Moapa Southern Paiute": {
      "lng": -114.770308,
      "lat": 36.52995
    },
    "Mountain View Solar": {
      "lng": -114.964444,
      "lat": 36.390556
    },
    "Nellis Air Force Base Solar Array": {
      "lng": -115.0544,
      "lat": 36.260278
    },
    "Nellis Solar PV II": {
      "lng": -115.047703,
      "lat": 36.206672
    },
    "Nevada Cogen Assoc#1 GarnetVly": {
      "lng": -114.920686,
      "lat": 36.343229
    },
    "Nevada Cogen Associates 2 Black Mountain": {
      "lng": -114.8783,
      "lat": 36.2253
    },
    "Nevada Solar One": {
      "lng": -114.98173,
      "lat": 35.799798
    },
    "New Lahontan": {
      "lng": -119.065227,
      "lat": 39.463569
    },
    "NGP Blue Mountain I LLC": {
      "lng": -118.1414,
      "lat": 40.9961
    },
    "North Valmy": {
      "lng": -117.151605,
      "lat": 40.881317
    },
    "NVSS-II": {
      "lng": -115.972329,
      "lat": 36.275051
    },
    "Patua Acquisition Project, LLC": {
      "lng": -119.073056,
      "lat": 39.585833
    },
    "Playa Solar 1, LLC": {
      "lng": -114.908,
      "lat": 36.408
    },
    "Playa Solar 2": {
      "lng": -114.908,
      "lat": 36.408
    },
    "Republic Services Renewable Energy, LLC": {
      "lng": -114.87483,
      "lat": 36.35867
    },
    "Richard Burdette Geothermal": {
      "lng": -119.754593,
      "lat": 39.390843
    },
    "River Mountains Solar": {
      "lng": -114.929399,
      "lat": 36.031616
    },
    "Saguaro Power": {
      "lng": -115.0117,
      "lat": 36.0417
    },
    "San Emidio": {
      "lng": -119.399722,
      "lat": 40.380556
    },
    "Searchlight Solar": {
      "lng": -114.936111,
      "lat": 35.479722
    },
    "Silver State Solar Power North": {
      "lng": -115.35028,
      "lat": 35.793056
    },
    "Silver State Solar Power South": {
      "lng": -115.32,
      "lat": 35.63
    },
    "Silverhawk": {
      "lng": -114.9606,
      "lat": 36.4078
    },
    "Soda Lake 3": {
      "lng": -118.85,
      "lat": 39.5545
    },
    "Soda Lake Geothermal No I II": {
      "lng": -118.8383,
      "lat": 39.557
    },
    "Solar Las Vegas MB 2, LLC": {
      "lng": -115.175381,
      "lat": 36.087426
    },
    "Solar Las Vegas MB-1": {
      "lng": -115.177778,
      "lat": 36.088611
    },
    "Spectrum Solar PV Power Project": {
      "lng": -114.87,
      "lat": 36.23
    },
    "Spring Valley Wind Project": {
      "lng": -114.492222,
      "lat": 39.104167
    },
    "Steamboat Hills LP": {
      "lng": -119.766944,
      "lat": 39.37
    },
    "Steamboat II": {
      "lng": -119.746762,
      "lat": 39.395603
    },
    "Steamboat III": {
      "lng": -119.7475,
      "lat": 39.394444
    },
    "Stillwater Facility": {
      "lng": -118.555613,
      "lat": 39.547537
    },
    "Sun Peak Generating Station": {
      "lng": -115.0339,
      "lat": 36.1375
    },
    "Sunshine Valley Solar": {
      "lng": -116.497,
      "lat": 36.518
    },
    "Techren Solar I LLC": {
      "lng": -114.974386,
      "lat": 35.851336
    },
    "Techren Solar II LLC": {
      "lng": -114.956428,
      "lat": 35.867258
    },
    "Techren Solar III LLC": {
      "lng": -114.9713,
      "lat": 35.844106
    },
    "Techren Solar IV LLC": {
      "lng": -114.936525,
      "lat": 35.892658
    },
    "Techren Solar V": {
      "lng": -114.92625,
      "lat": 35.899694
    },
    "Tesla Reno GigaFactory": {
      "lng": -119.439158,
      "lat": 39.539511
    },
    "Townsite Solar Project Hybrid": {
      "lng": -114.883999,
      "lat": 35.937336
    },
    "Tracy": {
      "lng": -119.525,
      "lat": 39.5625
    },
    "TS Power Plant": {
      "lng": -116.5297,
      "lat": 40.7461
    },
    "Tungsten Mountain": {
      "lng": -117.693658,
      "lat": 39.667002
    },
    "Turquoise Liberty Solar": {
      "lng": -119.52,
      "lat": 39.59
    },
    "Turquoise Nevada, LLC": {
      "lng": -119.5242,
      "lat": 39.57395
    },
    "Tuscarora Geothermal Power Plant": {
      "lng": -116.1511,
      "lat": 41.4828
    },
    "Verdi": {
      "lng": -119.585,
      "lat": 39.3125
    },
    "Walter M. Higgins III Generating Station": {
      "lng": -115.3561,
      "lat": 35.6139
    },
    "Washoe": {
      "lng": -119.5604,
      "lat": 39.3022
    },
    "Waste Management Lockwood LFGTE": {
      "lng": -119.620304,
      "lat": 39.493315
    },
    "Western 102 Power Plant": {
      "lng": -119.5108,
      "lat": 39.5603
    },
    "Whitegrass No. 1": {
      "lng": -119.180472,
      "lat": 39.163279
    },
    "100 Brook Hill Drive Solar": {
      "lng": -73.9828,
      "lat": 41.09305
    },
    "100 Lower": {
      "lng": -74.539,
      "lat": 41.294
    },
    "1639 RT 29 Solar 1": {
      "lng": -74.24935,
      "lat": 43.04296
    },
    "1639 RT 29 Solar 2": {
      "lng": -74.24935,
      "lat": 43.04296
    },
    "23rd and 3rd": {
      "lng": -74,
      "lat": 40.6631
    },
    "25 Ashdown Road Solar, LLC": {
      "lng": -73.879,
      "lat": 42.8826
    },
    "456 Lower": {
      "lng": -74.522,
      "lat": 41.311
    },
    "59th Street": {
      "lng": -73.99115,
      "lat": 40.771285
    },
    "6140 Route 209 - North": {
      "lng": -74.288885,
      "lat": 41.787555
    },
    "74th Street": {
      "lng": -73.9515,
      "lat": 40.768
    },
    "98th Street Battery Storage Station": {
      "lng": -73.840187,
      "lat": 40.679137
    },
    "Adirondack Solar": {
      "lng": -73.841731,
      "lat": 44.893641
    },
    "Aegis CSG": {
      "lng": -77.343879,
      "lat": 42.764688
    },
    "AES Tonawanda Solar LLC": {
      "lng": -78.9017,
      "lat": 42.9971
    },
    "AG - Energy": {
      "lng": -75.4417,
      "lat": 44.7258
    },
    "Al Turi": {
      "lng": -74.37671,
      "lat": 41.40432
    },
    "Albany Medical Ctr Cogen Plant": {
      "lng": -73.7777,
      "lat": 42.655
    },
    "Albert Einstein College of Medicine": {
      "lng": -73.8483,
      "lat": 40.85132
    },
    "Alice Falls Hydro Project": {
      "lng": -73.464752,
      "lat": 44.519153
    },
    "Allegany Station No. 133": {
      "lng": -78.0686,
      "lat": 42.5027
    },
    "Allens Falls": {
      "lng": -74.860265,
      "lat": 44.660338
    },
    "Allis Medina Solar LLC CSG": {
      "lng": -78.3574,
      "lat": 43.2205
    },
    "Altice - Hicksville": {
      "lng": -73.504192,
      "lat": 40.759984
    },
    "Altona": {
      "lng": -73.6509,
      "lat": 44.8216
    },
    "Amazon JFK8 Solar Project": {
      "lng": -74.187521,
      "lat": 40.618892
    },
    "Amsterdam North": {
      "lng": -74.119802,
      "lat": 42.953622
    },
    "Amsterdam South": {
      "lng": -74.126159,
      "lat": 42.948957
    },
    "Anheuser-Busch Baldwinsville": {
      "lng": -76.414844,
      "lat": 43.151537
    },
    "Arkwright Summit Wind Farm LLC": {
      "lng": -79.242681,
      "lat": 42.414911
    },
    "Arthur Kill": {
      "lng": -74.200035,
      "lat": 40.591564
    },
    "Ashokan": {
      "lng": -74.2063,
      "lat": 41.949
    },
    "Astoria Energy": {
      "lng": -73.896618,
      "lat": 40.781321
    },
    "Astoria Gas Turbine Power": {
      "lng": -73.9048,
      "lat": 40.787
    },
    "Astoria Generating Station": {
      "lng": -73.912561,
      "lat": 40.787674
    },
    "Athens Generating Company": {
      "lng": -73.8492,
      "lat": 42.2728
    },
    "ATT Jericho": {
      "lng": -73.352241,
      "lat": 40.830219
    },
    "Auburn LFG Energy Facility": {
      "lng": -76.593944,
      "lat": 42.944093
    },
    "Baer Road CSG": {
      "lng": -75.068,
      "lat": 41.797
    },
    "Barneveld Solar": {
      "lng": -75.177222,
      "lat": 43.251388
    },
    "Bassett Medical Center": {
      "lng": -74.9242,
      "lat": 42.6944
    },
    "Batavia Energy": {
      "lng": -78.1592,
      "lat": 42.9828
    },
    "Bayswater Peaking Facility": {
      "lng": -73.762218,
      "lat": 40.610654
    },
    "Beals Medina Solar LLC CSG": {
      "lng": -78.3559,
      "lat": 43.2256
    },
    "Beardslee": {
      "lng": -74.741349,
      "lat": 43.019113
    },
    "Beaver Falls, LLC": {
      "lng": -75.4342,
      "lat": 43.8861
    },
    "Beebee Island Hydro Plant": {
      "lng": -75.9072,
      "lat": 43.9767
    },
    "Belfort": {
      "lng": -75.3331,
      "lat": 43.9275
    },
    "Bellisario Solar 1": {
      "lng": -76.121116,
      "lat": 42.449783
    },
    "Bellisario Solar 2": {
      "lng": -76.121116,
      "lat": 42.451944
    },
    "Bellisario Solar 3": {
      "lng": -76.13176,
      "lat": 42.452956
    },
    "Bennett": {
      "lng": -77.47107,
      "lat": 42.9352
    },
    "Bennetts Bridge": {
      "lng": -75.9531,
      "lat": 43.5317
    },
    "Bethlehem - East": {
      "lng": -73.824295,
      "lat": 42.539006
    },
    "Bethlehem - West": {
      "lng": -73.824295,
      "lat": 42.539006
    },
    "Bethlehem Energy Center (Albany)": {
      "lng": -73.763752,
      "lat": 42.594196
    },
    "Bethlehem Solar": {
      "lng": -73.887416,
      "lat": 42.601145
    },
    "Bethpage Energy Center": {
      "lng": -73.4994,
      "lat": 40.7469
    },
    "Big Tree Community Solar Farm": {
      "lng": -78.559,
      "lat": 42.766
    },
    "Black River": {
      "lng": -75.813151,
      "lat": 44.000122
    },
    "Black River Generation, LLC": {
      "lng": -75.77204,
      "lat": 44.03647
    },
    "Black River Hydro Associates": {
      "lng": -75.344207,
      "lat": 43.591246
    },
    "Blake": {
      "lng": -74.753442,
      "lat": 44.506474
    },
    "Blanchard Road 1 Community Solar": {
      "lng": -76.01178,
      "lat": 44.25479
    },
    "Blanchard Road 2 Community Solar": {
      "lng": -76.01178,
      "lat": 44.25479
    },
    "Blenheim Gilboa": {
      "lng": -74.4479,
      "lat": 42.4423
    },
    "Bloom-Altice Fuel Cell": {
      "lng": -73.49435,
      "lat": 40.76119
    },
    "Bluestone": {
      "lng": -74.030193,
      "lat": 41.987467
    },
    "Blydenburgh Solar Project": {
      "lng": -73.182407,
      "lat": 40.815331
    },
    "Boas Rd #1 Community Solar Farm": {
      "lng": -73.672382,
      "lat": 44.934274
    },
    "Boas Rd #2 Community Solar Farm": {
      "lng": -73.67122,
      "lat": 44.933177
    },
    "Boas Rd #3 Community Solar Farm": {
      "lng": -73.66933,
      "lat": 44.933525
    },
    "Boas Rd #4 Community Solar Farm": {
      "lng": -73.666505,
      "lat": 44.928987
    },
    "Bowline Generating Station": {
      "lng": -73.9689,
      "lat": 41.2044
    },
    "Breesport Road Community Solar Farm": {
      "lng": -76.713,
      "lat": 42.088
    },
    "Brentwood": {
      "lng": -73.293412,
      "lat": 40.787062
    },
    "Brentwood Solar": {
      "lng": -73.243889,
      "lat": 40.781389
    },
    "Brick Church Solar": {
      "lng": -73.580034,
      "lat": 42.755594
    },
    "Bright Field Solar LLC(CSG)": {
      "lng": -77.73832,
      "lat": 42.46
    },
    "Bright Hill Solar LLC(CSG)": {
      "lng": -77.70378,
      "lat": 42.46723
    },
    "Bright Oak Solar LLC(CSG)": {
      "lng": -78.24481,
      "lat": 43.033
    },
    "Broadalbin": {
      "lng": -74.154885,
      "lat": 43.061306
    },
    "Broadalbin-Perth Solar": {
      "lng": -74.191836,
      "lat": 43.018029
    },
    "Bronx Zoo": {
      "lng": -73.876888,
      "lat": 40.844114
    },
    "Brookfield Power Glen Falls Hydro": {
      "lng": -73.636584,
      "lat": 43.308505
    },
    "Brooklyn Hospital Center": {
      "lng": -73.97793,
      "lat": 40.690544
    },
    "Brooklyn Navy Yard Cogeneration": {
      "lng": -73.976392,
      "lat": 40.699357
    },
    "Broome County": {
      "lng": -75.93073,
      "lat": 42.104731
    },
    "Browns Falls": {
      "lng": -75.059441,
      "lat": 44.211407
    },
    "Burritt Rd Community Solar Farm": {
      "lng": -77.764,
      "lat": 43.275
    },
    "Burrstone Energy Center": {
      "lng": -75.278357,
      "lat": 43.09534
    },
    "Cadyville": {
      "lng": -73.6219,
      "lat": 44.695
    },
    "Caithness Long Island Energy Center": {
      "lng": -72.9403,
      "lat": 40.8142
    },
    "Calabro Airport North Solar Project": {
      "lng": -72.875,
      "lat": 40.8224
    },
    "Calabro Airport South Solar Project": {
      "lng": -72.857053,
      "lat": 40.811524
    },
    "Call Farms 1": {
      "lng": -78.187295,
      "lat": 43.037794
    },
    "Call Farms 3": {
      "lng": -78.178591,
      "lat": 43.023046
    },
    "Calverton": {
      "lng": -72.743701,
      "lat": 40.921086
    },
    "Camden CSD Solar Array": {
      "lng": -75.695263,
      "lat": 43.312306
    },
    "Canandaigua Solar Array": {
      "lng": -77.284561,
      "lat": 42.86887
    },
    "Canandaigua Westbrook Solar Array": {
      "lng": -77.28446,
      "lat": 42.869086
    },
    "Carr Street Generating Station": {
      "lng": -76.08239,
      "lat": 43.06132
    },
    "Carthage Energy": {
      "lng": -75.6225,
      "lat": 43.9842
    },
    "Carver Falls": {
      "lng": -73.3073,
      "lat": 43.6262
    },
    "Cassadaga Wind Farm": {
      "lng": -79.195556,
      "lat": 42.338333
    },
    "Castleton": {
      "lng": -73.703,
      "lat": 42.523
    },
    "Castleton Power, LLC": {
      "lng": -73.7433,
      "lat": 42.5375
    },
    "Catlin Solar 1 LLC": {
      "lng": -76.953981,
      "lat": 42.285942
    },
    "CBP Solar": {
      "lng": -73.946882,
      "lat": 41.266188
    },
    "CEC, LLC Westtown B": {
      "lng": -74.561334,
      "lat": 41.345272
    },
    "Cedar Creek PV": {
      "lng": -73.506672,
      "lat": 40.64889
    },
    "Center Falls": {
      "lng": -73.460172,
      "lat": 43.096677
    },
    "Central Hudson High Falls": {
      "lng": -74.131877,
      "lat": 41.828814
    },
    "Certain Solar - Staten Island": {
      "lng": -74.176847,
      "lat": 40.540416
    },
    "CES Marbletown Solar": {
      "lng": -74.102718,
      "lat": 41.885716
    },
    "Chaffee Gas Recovery": {
      "lng": -78.4864,
      "lat": 42.5831
    },
    "Chambers 3 - Erie County Alden": {
      "lng": -78.55716,
      "lat": 42.93284
    },
    "Chambers 3 - Lewis County": {
      "lng": -75.49986,
      "lat": 43.78013
    },
    "Chambers Road Solar": {
      "lng": -76.894394,
      "lat": 42.187443
    },
    "Charles P Keller": {
      "lng": -73.640556,
      "lat": 40.658333
    },
    "Chasm": {
      "lng": -74.2169,
      "lat": 44.7533
    },
    "Chasm Hydro Partnership": {
      "lng": -74.1125,
      "lat": 44.9318
    },
    "Chateaugay": {
      "lng": -74.0299,
      "lat": 44.8917
    },
    "Chateaugay High Falls Hydro": {
      "lng": -74.085798,
      "lat": 44.909915
    },
    "Chautauqua LFGTE Facility": {
      "lng": -79.3186,
      "lat": 42.1989
    },
    "Chenango Solar": {
      "lng": -75.927282,
      "lat": 42.198167
    },
    "Chester CSG": {
      "lng": -74.288,
      "lat": 41.339
    },
    "City of Rochester Solar": {
      "lng": -77.678512,
      "lat": 43.176918
    },
    "CJ Solar I, LLC": {
      "lng": -75.3,
      "lat": 44.7
    },
    "Clarkson Solar": {
      "lng": -74.949444,
      "lat": 44.671944
    },
    "Clarkstown Landfill Solar Facility": {
      "lng": -73.951593,
      "lat": 41.100409
    },
    "Clay Solar CSG": {
      "lng": -76.21195,
      "lat": 43.194525
    },
    "Clifton Park Solar 1, LLC": {
      "lng": -73.834722,
      "lat": 42.839013
    },
    "Clifton Park Solar 2, LLC": {
      "lng": -73.83546,
      "lat": 42.84037
    },
    "Clinton": {
      "lng": -74.0061,
      "lat": 44.8914
    },
    "Clinton LFGTE Facility": {
      "lng": -73.5944,
      "lat": 44.6917
    },
    "CMR Solar LLC": {
      "lng": -74.345912,
      "lat": 41.561847
    },
    "Cohalan": {
      "lng": -73.194441,
      "lat": 40.762465
    },
    "Cohocton Wind Project": {
      "lng": -77.52,
      "lat": 42.45
    },
    "Colonie LFGTE Facility": {
      "lng": -73.726741,
      "lat": 42.803325
    },
    "Colton": {
      "lng": -74.9583,
      "lat": 44.5839
    },
    "Columbia University - Johnson Farms": {
      "lng": -74.263804,
      "lat": 41.379107
    },
    "Columbia University - Minisink": {
      "lng": -74.554452,
      "lat": 41.293747
    },
    "Copenhagen Plant": {
      "lng": -75.660268,
      "lat": 43.899745
    },
    "Copenhagen Wind Farm": {
      "lng": -75.698611,
      "lat": 43.913056
    },
    "Cornell Geneva Solar Farm": {
      "lng": -77.031,
      "lat": 42.866
    },
    "Cornell Hydro": {
      "lng": -76.485092,
      "lat": 42.451922
    },
    "Cornell Snyder Road Solar Array": {
      "lng": -76.46,
      "lat": 42.4975
    },
    "Cornell University Ithaca Campus": {
      "lng": -76.4764,
      "lat": 42.4428
    },
    "Corning Riverview (CSG)": {
      "lng": -78.91407,
      "lat": 43.001066
    },
    "Cortlandville I Solar CSG": {
      "lng": -76.154486,
      "lat": 42.606794
    },
    "COU Solar I, LLC": {
      "lng": -75.3,
      "lat": 44.7
    },
    "County of Dutchess, NY (Airport)": {
      "lng": -73.876845,
      "lat": 41.631398
    },
    "County Road 16": {
      "lng": -76.9022,
      "lat": 42.3771
    },
    "County Route 11 Community Solar Farm": {
      "lng": -73.677,
      "lat": 44.935
    },
    "Covanta Babylon Inc": {
      "lng": -73.3868,
      "lat": 40.7348
    },
    "Covanta Hempstead": {
      "lng": -73.5906,
      "lat": 40.7389
    },
    "Covanta Niagara": {
      "lng": -79.0056,
      "lat": 43.0839
    },
    "Cream Street Solar": {
      "lng": -73.87539,
      "lat": 41.75596
    },
    "Crescent": {
      "lng": -73.72264,
      "lat": 42.8056
    },
    "Cricket Valley Energy Center": {
      "lng": -73.581111,
      "lat": 41.676111
    },
    "Croton Solar": {
      "lng": -73.867575,
      "lat": 41.280404
    },
    "Cummins, Inc": {
      "lng": -79.36125,
      "lat": 42.08124
    },
    "Curtis Palmer Hydroelectric": {
      "lng": -73.8281,
      "lat": 43.2469
    },
    "Czub CSG": {
      "lng": -73.59598,
      "lat": 42.91408
    },
    "Dahowa Hydro": {
      "lng": -73.5369,
      "lat": 43.1033
    },
    "DANC LFGTE Facility": {
      "lng": -75.9175,
      "lat": 43.82
    },
    "Danskammer Generating Station": {
      "lng": -73.974981,
      "lat": 41.571247
    },
    "Dashville": {
      "lng": -74.0497,
      "lat": 41.8217
    },
    "Daum Solar": {
      "lng": -77.247333,
      "lat": 42.760799
    },
    "Deer Park": {
      "lng": -73.193889,
      "lat": 40.760833
    },
    "Deferiet": {
      "lng": -75.6806,
      "lat": 44.0381
    },
    "Dennison": {
      "lng": -73.323611,
      "lat": 40.763056
    },
    "Dexter Plant": {
      "lng": -76.041277,
      "lat": 44.003984
    },
    "Diamond Island Plant": {
      "lng": -75.886102,
      "lat": 43.978626
    },
    "Diana Hydroelectric": {
      "lng": -75.321725,
      "lat": 44.151944
    },
    "DOCCS Eastern": {
      "lng": -74.368626,
      "lat": 41.738928
    },
    "DOCCS Greenhaven": {
      "lng": -73.72058,
      "lat": 41.578415
    },
    "DOCCS Midstate": {
      "lng": -75.304087,
      "lat": 43.164033
    },
    "DOCCS Wende": {
      "lng": -78.55481,
      "lat": 42.929982
    },
    "Dolgeville Hydro": {
      "lng": -74.766961,
      "lat": 43.090635
    },
    "Dover CSG": {
      "lng": -73.576568,
      "lat": 41.680879
    },
    "Dryden Rd #2 Community Solar Farm CSG": {
      "lng": -76.319983,
      "lat": 42.487821
    },
    "Duanesburg": {
      "lng": -74.201552,
      "lat": 42.762173
    },
    "Dunkirk Generating Plant": {
      "lng": -79.346631,
      "lat": 42.491467
    },
    "Dunn Paper": {
      "lng": -75.50274,
      "lat": 44.336358
    },
    "Dusenberry": {
      "lng": -73.6247,
      "lat": 42.7677
    },
    "Dutch Hill Wind Project": {
      "lng": -77.4967,
      "lat": 42.5456
    },
    "Dutchess Cnty Resource Recovery Facility": {
      "lng": -73.943,
      "lat": 41.647
    },
    "E F Barrett": {
      "lng": -73.647881,
      "lat": 40.616689
    },
    "E J West": {
      "lng": -73.920785,
      "lat": 43.318906
    },
    "Eagle": {
      "lng": -75.200831,
      "lat": 43.909714
    },
    "East Fishkill CSG": {
      "lng": -73.829,
      "lat": 41.571
    },
    "East Hampton Energy Storage Center": {
      "lng": -72.209858,
      "lat": 40.961751
    },
    "East Hampton Facility": {
      "lng": -72.210277,
      "lat": 40.961706
    },
    "East Norfolk": {
      "lng": -74.9856,
      "lat": 44.7989
    },
    "East Pulaski BESS": {
      "lng": -76.10403,
      "lat": 43.546613
    },
    "East River": {
      "lng": -73.9742,
      "lat": 40.7281
    },
    "Edgewood Energy": {
      "lng": -73.2931,
      "lat": 40.7861
    },
    "Eel Weir": {
      "lng": -75.49052,
      "lat": 44.638365
    },
    "Effley": {
      "lng": -75.277923,
      "lat": 43.922831
    },
    "Ellenburg": {
      "lng": -74.0061,
      "lat": 44.8914
    },
    "Ellis Solar": {
      "lng": -76.433237,
      "lat": 42.444628
    },
    "Ellsworth I CSG": {
      "lng": -73.709799,
      "lat": 42.899313
    },
    "Elmer": {
      "lng": -75.288322,
      "lat": 43.927536
    },
    "ELP Greenport CSG": {
      "lng": -73.782,
      "lat": 42.237
    },
    "ELP Kinderhook CSG": {
      "lng": -73.691,
      "lat": 42.464
    },
    "ELP Myer CSG": {
      "lng": -73.978,
      "lat": 42.041
    },
    "ELP Rockdale CSG": {
      "lng": -77.873,
      "lat": 39.625
    },
    "ELP Saugerties CSG": {
      "lng": -74.074,
      "lat": 42.04
    },
    "Empire Generating Co, LLC": {
      "lng": -73.750021,
      "lat": 42.625239
    },
    "Enfield Community Solar": {
      "lng": -76.594813,
      "lat": 42.453307
    },
    "Ephratah": {
      "lng": -74.5372,
      "lat": 43.0092
    },
    "Equus  Power I": {
      "lng": -73.5683,
      "lat": 40.6439
    },
    "Feeder Dam Hydro Plant": {
      "lng": -73.665834,
      "lat": 43.289874
    },
    "Fenner Wind": {
      "lng": -75.762536,
      "lat": 42.988989
    },
    "Finch Paper": {
      "lng": -73.6376,
      "lat": 43.3084
    },
    "Finchville Solar, LLC Hybrid CSG": {
      "lng": -74.5465,
      "lat": 41.4565
    },
    "Finger Lakes Solar I": {
      "lng": -77.045631,
      "lat": 42.814025
    },
    "Fishers Island 1": {
      "lng": -72.01,
      "lat": 41.268611
    },
    "Five Falls": {
      "lng": -74.849177,
      "lat": 44.527491
    },
    "Flat Rock": {
      "lng": -75.0742,
      "lat": 44.2217
    },
    "Fogarty CSG": {
      "lng": -74.349999,
      "lat": 41.508729
    },
    "Fordham University": {
      "lng": -73.887419,
      "lat": 40.861278
    },
    "Forestport": {
      "lng": -75.215946,
      "lat": 43.442468
    },
    "Fort Miller Hydroelectric Facility": {
      "lng": -73.584076,
      "lat": 43.160868
    },
    "Fortistar North Tonawanda Inc": {
      "lng": -78.8539,
      "lat": 43.0483
    },
    "Fourth Branch Hydroelectric Facility": {
      "lng": -73.691676,
      "lat": 42.787575
    },
    "Franklin (NY)": {
      "lng": -73.9725,
      "lat": 44.4372
    },
    "Franklin Solar Site": {
      "lng": -74.306523,
      "lat": 44.86832
    },
    "Fredonia Solar LLC": {
      "lng": -79.3408,
      "lat": 42.453047
    },
    "Freehold(CSG)": {
      "lng": -74.038047,
      "lat": 42.370433
    },
    "Freeport Power Plant No. 2": {
      "lng": -73.5683,
      "lat": 40.6447
    },
    "Frey Rd #1 Community Solar Farm": {
      "lng": -77.34513,
      "lat": 43.099866
    },
    "Frey Rd #2 Community Solar Farm": {
      "lng": -77.343495,
      "lat": 43.099893
    },
    "Frog Hollow CSG": {
      "lng": -74.496708,
      "lat": 41.739327
    },
    "Fuel Cell 18A Sneden Avenue": {
      "lng": -74.17676,
      "lat": 40.540377
    },
    "Fuel Cell 18B Sneden Avenue": {
      "lng": -74.176559,
      "lat": 40.540457
    },
    "Fulton Hydro": {
      "lng": -76.418984,
      "lat": 43.32411
    },
    "Fulton LFGTE Facility": {
      "lng": -74.4739,
      "lat": 43.0142
    },
    "Furnace Rd Community Solar Farm": {
      "lng": -77.27,
      "lat": 43.241
    },
    "Garnet Solar (NY)": {
      "lng": -74.062378,
      "lat": 43.723851
    },
    "Gaskill Rd Community Solar Farm CSG": {
      "lng": -76.183,
      "lat": 42.116
    },
    "GE 19th Hole": {
      "lng": -74.313086,
      "lat": 42.985644
    },
    "Gifford CSG": {
      "lng": -74.07086,
      "lat": 42.82395
    },
    "Glen Park Hydroelectric Project": {
      "lng": -75.961156,
      "lat": 44.000608
    },
    "Glenmere Lake": {
      "lng": -74.366,
      "lat": 41.3698
    },
    "Glenwood": {
      "lng": -73.647473,
      "lat": 40.827149
    },
    "Glenwood Hydro": {
      "lng": -78.3906,
      "lat": 43.2358
    },
    "Glenwood Landing Energy Center": {
      "lng": -73.6478,
      "lat": 40.8275
    },
    "Gloversville Landfill Solar": {
      "lng": -74.308356,
      "lat": 43.052981
    },
    "Goldman Sachs Carports Solar": {
      "lng": -73.75055,
      "lat": 42.76573
    },
    "Good Samaritan Hospital": {
      "lng": -74.135402,
      "lat": 41.111498
    },
    "Goodyear Lake Plant": {
      "lng": -74.98356,
      "lat": 42.502473
    },
    "Gore Mountain Solar II": {
      "lng": -73.32307,
      "lat": 43.543378
    },
    "Gouldtown": {
      "lng": -75.339072,
      "lat": 43.612619
    },
    "Gowanus Generating Station": {
      "lng": -74.0051,
      "lat": 40.6635
    },
    "Grabinski": {
      "lng": -74.17967,
      "lat": 41.578731
    },
    "Grahamsville": {
      "lng": -74.5106,
      "lat": 41.8592
    },
    "Granby": {
      "lng": -76.420841,
      "lat": 43.323441
    },
    "Grand Prix Solar": {
      "lng": -73.72252,
      "lat": 41.21125
    },
    "Great Lakes Seaway Community Solar": {
      "lng": -76.01178,
      "lat": 44.25479
    },
    "Green Island Hydroelectric Station": {
      "lng": -73.69022,
      "lat": 42.748106
    },
    "Greene County Meter #1": {
      "lng": -74.00076,
      "lat": 42.294823
    },
    "Greenidge Generation LLC": {
      "lng": -76.9483,
      "lat": 42.6789
    },
    "Greenport": {
      "lng": -72.3758,
      "lat": 41.1055
    },
    "Greenville CSG": {
      "lng": -74.622,
      "lat": 41.356
    },
    "Greenwich Solar 1, LLC CSG": {
      "lng": -73.5331,
      "lat": 43.0783
    },
    "Guildlerland CSG": {
      "lng": -74.000814,
      "lat": 42.727288
    },
    "Hailesboro 4 Plant": {
      "lng": -75.444992,
      "lat": 44.311681
    },
    "Hales Mills Solar, LLC": {
      "lng": -74.32918,
      "lat": 43.01827
    },
    "Hamlin Solar 1, LLC": {
      "lng": -77.94429,
      "lat": 43.30141
    },
    "Hampshire Paper": {
      "lng": -75.36645,
      "lat": 44.296544
    },
    "Hannawa": {
      "lng": -74.9733,
      "lat": 44.6192
    },
    "Harbec Energy": {
      "lng": -77.364092,
      "lat": 43.22731
    },
    "Hardscrabble Wind Power LLC": {
      "lng": -74.878888,
      "lat": 43.112222
    },
    "Harford Solar Farm": {
      "lng": -76.245,
      "lat": 42.448
    },
    "Harlem Hospital Center": {
      "lng": -73.9399,
      "lat": 40.81453
    },
    "Harlem River Yard": {
      "lng": -73.9156,
      "lat": 40.7991
    },
    "Harris Lake": {
      "lng": -74.1861,
      "lat": 43.9719
    },
    "Hawkeye Energy Greenport, LLC": {
      "lng": -72.376155,
      "lat": 41.10603
    },
    "Hell Gate": {
      "lng": -73.9094,
      "lat": 40.799
    },
    "Henkel U.S. Operations Corporation": {
      "lng": -76.968333,
      "lat": 42.886944
    },
    "Herkimer": {
      "lng": -74.983391,
      "lat": 43.033577
    },
    "Herrings": {
      "lng": -75.6511,
      "lat": 44.0217
    },
    "Heuvelton": {
      "lng": -75.4042,
      "lat": 44.6172
    },
    "Hewittville Hydroelectric": {
      "lng": -75.007933,
      "lat": 44.705167
    },
    "HGS Solar I": {
      "lng": -73.325257,
      "lat": 43.543716
    },
    "Hickory Grove #1": {
      "lng": -76.845064,
      "lat": 42.186827
    },
    "Hickory Grove #2": {
      "lng": -76.845,
      "lat": 42.186
    },
    "High Acres Gas Recovery": {
      "lng": -77.381667,
      "lat": 43.081667
    },
    "High Dam": {
      "lng": -76.4961,
      "lat": 43.4436
    },
    "High Falls - Croghan NY": {
      "lng": -75.3731,
      "lat": 43.9247
    },
    "High Falls - Saranac NY": {
      "lng": -73.7492,
      "lat": 44.6389
    },
    "High Sheldon Wind Farm": {
      "lng": -78.4269,
      "lat": 42.7794
    },
    "Higley": {
      "lng": -74.9331,
      "lat": 44.5306
    },
    "Hillburn": {
      "lng": -74.1653,
      "lat": 41.1269
    },
    "Hilton NY Co Gen Plant": {
      "lng": -73.979167,
      "lat": 40.7625
    },
    "Hobart & William Smith College Gates Rd.": {
      "lng": -77.05058,
      "lat": 42.86228
    },
    "Hollow Dam Power Partnership": {
      "lng": -75.336142,
      "lat": 44.296453
    },
    "Hollygrove CSG": {
      "lng": -76.7217,
      "lat": 43.26034
    },
    "Holtsville Facility": {
      "lng": -73.066265,
      "lat": 40.817037
    },
    "Holtsville Solar Project": {
      "lng": -73.033977,
      "lat": 40.797667
    },
    "Homer Street East": {
      "lng": -78.44095,
      "lat": 42.099342
    },
    "Homer Street West": {
      "lng": -78.442286,
      "lat": 42.098128
    },
    "Hope Solar Farm LLC": {
      "lng": -73.602129,
      "lat": 42.726428
    },
    "Hospital Rd Community Solar Farm CSG": {
      "lng": -75.036074,
      "lat": 41.774772
    },
    "Houghton": {
      "lng": -78.156589,
      "lat": 42.426731
    },
    "Howard Wind Farm": {
      "lng": -77.538333,
      "lat": 42.315278
    },
    "Howell CSG": {
      "lng": -74.312978,
      "lat": 41.265258
    },
    "Howland CSG": {
      "lng": -76.08921,
      "lat": 43.63481
    },
    "Hudson Avenue": {
      "lng": -73.9807,
      "lat": 40.7052
    },
    "Hudson Falls Hydroelectric Project": {
      "lng": -73.5956,
      "lat": 43.2958
    },
    "Huntington Resource Recovery Facility": {
      "lng": -73.2906,
      "lat": 40.8814
    },
    "HVCC Cogen Plant": {
      "lng": -73.68,
      "lat": 42.696944
    },
    "Hydraulic Race": {
      "lng": -78.691551,
      "lat": 43.170948
    },
    "Hyland LFGTE Facility": {
      "lng": -78.0061,
      "lat": 42.2853
    },
    "Indeck-Corinth Energy Center": {
      "lng": -73.8125,
      "lat": 43.25
    },
    "Indeck-Olean Energy Center": {
      "lng": -78.45425,
      "lat": 42.085944
    },
    "Indeck-Oswego Energy Center": {
      "lng": -76.493474,
      "lat": 43.471745
    },
    "Indeck-Silver Springs Energy Center": {
      "lng": -78.0772,
      "lat": 42.6544
    },
    "Indeck-Yerkes Energy Center": {
      "lng": -78.9182,
      "lat": 42.9671
    },
    "Indian Point 3": {
      "lng": -73.9526,
      "lat": 41.2706
    },
    "Inghams": {
      "lng": -74.764347,
      "lat": 43.061017
    },
    "Interfaith Medical Center": {
      "lng": -73.93742,
      "lat": 40.67855
    },
    "Iola Powerhouse & Cogeneration Facility": {
      "lng": -77.613815,
      "lat": 43.112107
    },
    "IRE Solar I, LLC": {
      "lng": -75.5,
      "lat": 44.3
    },
    "Jacobi Medical Center": {
      "lng": -73.84737,
      "lat": 40.85652
    },
    "Jamaica Bay Peaking": {
      "lng": -73.7622,
      "lat": 40.6097
    },
    "James A Fitzpatrick": {
      "lng": -76.408394,
      "lat": 43.52139
    },
    "Jarvis (Hinckley)": {
      "lng": -75.109,
      "lat": 43.3121
    },
    "JB-Tobin 2": {
      "lng": -73.8105,
      "lat": 41.6896
    },
    "Jefferson-Lewis BOCES Solar": {
      "lng": -75.969805,
      "lat": 43.970958
    },
    "Jericho Rise Wind Farm LLC": {
      "lng": -74.103333,
      "lat": 44.888889
    },
    "JLL-One Rockwood Road": {
      "lng": -73.862594,
      "lat": 41.109042
    },
    "Johnsonville Dam": {
      "lng": -73.508886,
      "lat": 42.920374
    },
    "Johnstown": {
      "lng": -74.314787,
      "lat": 42.999179
    },
    "Johnstown Solar 1, LLC": {
      "lng": -74.3228,
      "lat": 43.0724
    },
    "Kamargo": {
      "lng": -75.7967,
      "lat": 44.0117
    },
    "KCE NY 1": {
      "lng": -73.746506,
      "lat": 42.962412
    },
    "Kearsarge Johnstown 1 CSG": {
      "lng": -74.360177,
      "lat": 43.066012
    },
    "Kearsarge Johnstown 2 CSG": {
      "lng": -74.360221,
      "lat": 43.064422
    },
    "Kearsarge Oppenheim CSG": {
      "lng": -74.736424,
      "lat": 43.060223
    },
    "Kelly Bridge Road Community Solar Farm": {
      "lng": -74.776464,
      "lat": 41.775422
    },
    "Kenmare - Sullivan Community College": {
      "lng": -74.666717,
      "lat": 41.772796
    },
    "Kent Falls": {
      "lng": -73.5967,
      "lat": 44.7053
    },
    "KIAC Cogeneration": {
      "lng": -73.7778,
      "lat": 40.6417
    },
    "Kinder Morgan Fordham": {
      "lng": -74.236582,
      "lat": 40.542557
    },
    "Kings Falls Hydroelectric": {
      "lng": -75.633822,
      "lat": 43.917267
    },
    "Kings Park Solar I": {
      "lng": -73.249281,
      "lat": 40.868294
    },
    "Kings Park Solar II": {
      "lng": -73.249281,
      "lat": 40.868294
    },
    "Kings Plaza Total Energy Plant (TEP)": {
      "lng": -73.920008,
      "lat": 40.610181
    },
    "Kingsbrook Jewish Medical Center": {
      "lng": -73.933752,
      "lat": 40.659328
    },
    "Knaggs Brothers Farm": {
      "lng": -73.9691,
      "lat": 42.73035
    },
    "Knapp East (CSG)": {
      "lng": -78.4601,
      "lat": 43.0134
    },
    "Knapp West(CSG)": {
      "lng": -78.4609,
      "lat": 43.0148
    },
    "Kosterville": {
      "lng": -75.332705,
      "lat": 43.614218
    },
    "Lachute Hydro Lower": {
      "lng": -73.420701,
      "lat": 43.849029
    },
    "Lachute Hydro Upper": {
      "lng": -73.431207,
      "lat": 43.843869
    },
    "Lansing Renewables, LLC": {
      "lng": -76.56563,
      "lat": 42.59251
    },
    "Leavenworth Greenworks LLC": {
      "lng": -72.763333,
      "lat": 40.925278
    },
    "Lederle Laboratories": {
      "lng": -74.0181,
      "lat": 41.0772
    },
    "Letchworth Solar Project": {
      "lng": -78.17952,
      "lat": 42.618969
    },
    "Lewiston Niagara": {
      "lng": -79.021958,
      "lat": 43.142973
    },
    "Lichtenthal": {
      "lng": -78.158181,
      "lat": 43.020053
    },
    "Lighthouse Hill": {
      "lng": -75.970318,
      "lat": 43.524103
    },
    "Lincoln Ave Solar Project": {
      "lng": -73.083437,
      "lat": 40.78144
    },
    "Lincoln Medical and Mental Health Center": {
      "lng": -73.92411,
      "lat": 40.816971
    },
    "Little Falls Hydro": {
      "lng": -74.850621,
      "lat": 43.040335
    },
    "Little Falls Solar": {
      "lng": -74.88603,
      "lat": 43.05899
    },
    "Little Falls Solar 1": {
      "lng": -74.85455,
      "lat": 43.05414
    },
    "Lockheed Martin RMS Syracuse": {
      "lng": -76.189224,
      "lat": 43.109574
    },
    "Lockport": {
      "lng": -78.744971,
      "lat": 43.161947
    },
    "Long Island Solar Farm LLC": {
      "lng": -72.847777,
      "lat": 40.861111
    },
    "Longfalls Facility": {
      "lng": -75.624677,
      "lat": 43.980806
    },
    "Lower Beaver Falls Project": {
      "lng": -75.430556,
      "lat": 43.884444
    },
    "Lower Saranac Hydroelectric Facility": {
      "lng": -73.5075,
      "lat": 44.6694
    },
    "LR Wheatfield Solar 1": {
      "lng": -78.91409,
      "lat": 43.12163
    },
    "Lyons Falls Hydroelectric": {
      "lng": -75.358577,
      "lat": 43.61863
    },
    "Lyonsdale Associates": {
      "lng": -75.30546,
      "lat": 43.618874
    },
    "MacArthur Waste to Energy Facility": {
      "lng": -73.1064,
      "lat": 40.7869
    },
    "Macedon": {
      "lng": -77.346042,
      "lat": 43.058633
    },
    "Macomb": {
      "lng": -74.306288,
      "lat": 44.877326
    },
    "Madison County": {
      "lng": -75.700978,
      "lat": 43.028289
    },
    "Madison Windpower LLC": {
      "lng": -75.451666,
      "lat": 42.894722
    },
    "Maimonides Medical Center": {
      "lng": -73.998833,
      "lat": 40.639538
    },
    "Malone Solar Site": {
      "lng": -74.307248,
      "lat": 44.870858
    },
    "Maple Ridge Wind Farm": {
      "lng": -75.5753,
      "lat": 43.7853
    },
    "Marble River Wind Farm": {
      "lng": -73.929722,
      "lat": 44.975833
    },
    "Marsh Hill Wind Farm": {
      "lng": -77.528889,
      "lat": 42.163889
    },
    "Massena Energy Facility": {
      "lng": -74.8928,
      "lat": 44.9503
    },
    "Mattas Farms": {
      "lng": -74.216251,
      "lat": 42.893714
    },
    "Mechanicville Hydroelectric Station": {
      "lng": -73.680169,
      "lat": 42.876535
    },
    "Medusa NY 1": {
      "lng": -74.099533,
      "lat": 42.466286
    },
    "MER Queens": {
      "lng": -73.870942,
      "lat": 40.7345
    },
    "Mercy Medical Center": {
      "lng": -73.630833,
      "lat": 40.688333
    },
    "Mereand CSG": {
      "lng": -75.98679,
      "lat": 43.96414
    },
    "Middle Falls Hydro": {
      "lng": -73.525271,
      "lat": 43.098711
    },
    "Middletown NY 1": {
      "lng": -74.509799,
      "lat": 41.40888
    },
    "Middletown Solar 1, LLC Hybrid CSG": {
      "lng": -74.5199,
      "lat": 41.3902
    },
    "Mill C": {
      "lng": -73.6122,
      "lat": 44.6967
    },
    "Mill Pond Hydro": {
      "lng": -73.897951,
      "lat": 42.252975
    },
    "Mill Seat Renewable Energy Facility": {
      "lng": -77.932902,
      "lat": 43.063706
    },
    "Millbrook School": {
      "lng": -73.619485,
      "lat": 41.84581
    },
    "Minetto": {
      "lng": -76.4739,
      "lat": 43.4003
    },
    "Minisink Community Solar 2 LLC": {
      "lng": -74.515278,
      "lat": 41.319351
    },
    "Minisink CSG": {
      "lng": -74.515411,
      "lat": 41.353476
    },
    "Minisink Solar 1 LLC": {
      "lng": -74.514084,
      "lat": 41.321224
    },
    "MM Albany Energy": {
      "lng": -73.850866,
      "lat": 42.704391
    },
    "Model City Energy Facility": {
      "lng": -78.9836,
      "lat": 43.2094
    },
    "Modern Innovative Energy LLC": {
      "lng": -78.9836,
      "lat": 43.2094
    },
    "Mohonasen Central School District": {
      "lng": -73.956847,
      "lat": 42.767228
    },
    "Mongaup Falls": {
      "lng": -74.771383,
      "lat": 41.537722
    },
    "Monroe Community College Plant": {
      "lng": -77.610278,
      "lat": 43.103889
    },
    "Monroe County Sites A & B": {
      "lng": -77.392234,
      "lat": 43.174071
    },
    "Monroe County Sites C, D, & E": {
      "lng": -77.740208,
      "lat": 43.31879
    },
    "Montauk Energy Storage Center": {
      "lng": -71.964359,
      "lat": 41.03988
    },
    "Montefieor Medical Center Moses Division": {
      "lng": -73.880278,
      "lat": 40.881389
    },
    "Montefiore - Westchester Square": {
      "lng": -73.848367,
      "lat": 40.840648
    },
    "Montefiore Mount Vernon Hospital": {
      "lng": -73.868738,
      "lat": 40.902825
    },
    "Montefiore Nyack Hospital": {
      "lng": -73.91824,
      "lat": 41.09098
    },
    "Montefiore Weiler (Einstein) Hospital": {
      "lng": -73.879075,
      "lat": 40.880375
    },
    "Montefiore-New Rochelle": {
      "lng": -73.78781,
      "lat": 40.91261
    },
    "Moore Road Community Solar": {
      "lng": -78.903,
      "lat": 43.213
    },
    "Moose River": {
      "lng": -75.341325,
      "lat": 43.610664
    },
    "Moshier": {
      "lng": -75.1364,
      "lat": 43.8719
    },
    "Mount Hope East": {
      "lng": -74.509646,
      "lat": 41.467654
    },
    "Mount Hope West": {
      "lng": -74.509417,
      "lat": 41.466974
    },
    "Mount Kisco Landfill Solar & Storage CSG": {
      "lng": -73.73606,
      "lat": 41.195478
    },
    "Mount Sinai - Brooklyn": {
      "lng": -73.94298,
      "lat": 40.61873
    },
    "Mount Sinai Beth Israel": {
      "lng": -73.981597,
      "lat": 40.732666
    },
    "Mount Sinai Hospital": {
      "lng": -73.953689,
      "lat": 40.78999
    },
    "Mount Sinai South Nassau Hospital": {
      "lng": -73.62993,
      "lat": 40.651614
    },
    "MSAP 13": {
      "lng": -73.67174,
      "lat": 42.698335
    },
    "Mt Ida Hydroelectric": {
      "lng": -73.680608,
      "lat": 42.721326
    },
    "Mt.Sinai-St.Lukes Roosevelt Hosp. Center": {
      "lng": -73.898015,
      "lat": 40.928827
    },
    "Munnsville Wind Farm LLC": {
      "lng": -75.5697,
      "lat": 42.9086
    },
    "Musgrave East Solar Farm": {
      "lng": -76.663665,
      "lat": 42.731496
    },
    "Musgrave West Solar Farm": {
      "lng": -76.663665,
      "lat": 42.731496
    },
    "Nanticoke LFG": {
      "lng": -75.982533,
      "lat": 42.243704
    },
    "Narrows Generating Station": {
      "lng": -74.020833,
      "lat": 40.648611
    },
    "Nassau Energy Corporation": {
      "lng": -73.5892,
      "lat": 40.7264
    },
    "Neversink": {
      "lng": -74.6358,
      "lat": 41.8194
    },
    "New York Presbyterian Hospital-168th St": {
      "lng": -73.941391,
      "lat": 40.840854
    },
    "New York State Dam Hydro": {
      "lng": -73.696096,
      "lat": 42.781779
    },
    "New York University Central Plant": {
      "lng": -73.9233,
      "lat": 40.7899
    },
    "Newfield Community Solar LLC": {
      "lng": -76.564236,
      "lat": 42.39131
    },
    "Newport Hydro": {
      "lng": -75.0169,
      "lat": 43.1853
    },
    "NewYork-Presbyterian Brooklyn Methodist Hospital": {
      "lng": -73.9787,
      "lat": 40.6679
    },
    "NHH003 Clarkson Fuel Cell": {
      "lng": -73.94278,
      "lat": 40.65653
    },
    "NHH004 Winthrop Fuel Cell": {
      "lng": -73.946662,
      "lat": 40.656847
    },
    "Nine Mile Point Nuclear Station": {
      "lng": -76.41,
      "lat": 43.5211
    },
    "Nissequogue Energy Center": {
      "lng": -73.129056,
      "lat": 40.916833
    },
    "Norfolk": {
      "lng": -74.9975,
      "lat": 44.8053
    },
    "Normanskill Hydro Project": {
      "lng": -73.957324,
      "lat": 42.712356
    },
    "North 1st": {
      "lng": -73.9664,
      "lat": 40.7171
    },
    "North Central Bronx Hospital": {
      "lng": -73.8812,
      "lat": 40.88043
    },
    "North County": {
      "lng": -73.323611,
      "lat": 40.763056
    },
    "Northern Westchester Hospital": {
      "lng": -73.725695,
      "lat": 41.196839
    },
    "Northport": {
      "lng": -73.342323,
      "lat": 40.923565
    },
    "Norwich Solar and Energy Storage CSG": {
      "lng": -75.529572,
      "lat": 42.552861
    },
    "Norwood": {
      "lng": -75.0042,
      "lat": 44.7439
    },
    "NY - CSG - Ellsworth II": {
      "lng": -73.707473,
      "lat": 42.899691
    },
    "NY - Mines Press": {
      "lng": -73.86612,
      "lat": 41.27803
    },
    "NY - Presbyt. Hospital - 525 E 68TH St": {
      "lng": -73.95393,
      "lat": 40.764396
    },
    "NY 26 Carthage CSG": {
      "lng": -75.618066,
      "lat": 43.955471
    },
    "NY- CSG- Johnstown 2": {
      "lng": -74.37876,
      "lat": 43.04232
    },
    "NY- CSG- Livingston 4": {
      "lng": -77.7642,
      "lat": 42.9199
    },
    "NY Times Daily Production Facility": {
      "lng": -73.830918,
      "lat": 40.775397
    },
    "NY3 Battery": {
      "lng": -74.07467,
      "lat": 41.18512
    },
    "NY8 - Branscomb Solar": {
      "lng": -73.586739,
      "lat": 42.962972
    },
    "NYC-HH - CONEY ISLAND HOSPITAL": {
      "lng": -73.965581,
      "lat": 40.58549
    },
    "NYC-HH-New Bellevue Hospital": {
      "lng": -73.976083,
      "lat": 40.739556
    },
    "NYPH - Lawrence": {
      "lng": -73.83686,
      "lat": 40.94233
    },
    "NYPH-Queens": {
      "lng": -73.82573,
      "lat": 40.75043
    },
    "NYP-Hudson Valley Hospital Center": {
      "lng": -73.888802,
      "lat": 41.286739
    },
    "NYP-Lower Manhattan Hospital": {
      "lng": -74.004965,
      "lat": 40.710315
    },
    "NYS Rte 12 Community Solar": {
      "lng": -76.01178,
      "lat": 44.25479
    },
    "NYU Langone - Orthopedic": {
      "lng": -73.98285,
      "lat": 40.73402
    },
    "NYU LANGONE HEALTH": {
      "lng": -73.972226,
      "lat": 40.746778
    },
    "NYU Luthern Medical Center": {
      "lng": -74.0211,
      "lat": 40.6467
    },
    "OBP Cogen": {
      "lng": -73.983248,
      "lat": 40.754865
    },
    "Off Airport Road - West": {
      "lng": -74.229036,
      "lat": 41.785648
    },
    "Ogdensburg": {
      "lng": -75.487049,
      "lat": 44.704089
    },
    "Oneida - South": {
      "lng": -75.386866,
      "lat": 43.166353
    },
    "Oneida - West": {
      "lng": -75.404866,
      "lat": 43.169617
    },
    "Oneida County- DPW": {
      "lng": -75.371258,
      "lat": 43.150761
    },
    "Oneida Herkimer": {
      "lng": -75.418333,
      "lat": 43.462222
    },
    "Onondaga County - Metro Water Board": {
      "lng": -76.224075,
      "lat": 43.183782
    },
    "Onondaga County - Oak Orchard WWTP": {
      "lng": -76.130123,
      "lat": 43.172719
    },
    "Onondaga County- Clearwater": {
      "lng": -76.515717,
      "lat": 43.429317
    },
    "Onondaga County- Jamesville": {
      "lng": -76.064219,
      "lat": 42.986644
    },
    "Onondaga County Resource Recovery": {
      "lng": -76.1149,
      "lat": 43.0047
    },
    "Ontario LFGTE": {
      "lng": -77.0872,
      "lat": 42.8586
    },
    "Onyx - Lamphear Road CSG": {
      "lng": -75.476724,
      "lat": 43.18413
    },
    "Onyx - Saratoga Springs Landfill Solar CSG": {
      "lng": -73.744855,
      "lat": 43.096874
    },
    "Orange County Solar Farm (NY)": {
      "lng": -74.359495,
      "lat": 41.404879
    },
    "Orbit Bloom Fuel Cell": {
      "lng": -72.965736,
      "lat": 40.819031
    },
    "Oswegatchie": {
      "lng": -75.199167,
      "lat": 44.27
    },
    "Oswego County - Fulton Solar": {
      "lng": -76.34325,
      "lat": 43.342437
    },
    "Oswego County Energy Recovery": {
      "lng": -76.425169,
      "lat": 43.348808
    },
    "Oswego Falls East": {
      "lng": -76.4153,
      "lat": 43.3161
    },
    "Oswego Falls West": {
      "lng": -76.4172,
      "lat": 43.3156
    },
    "Oswego Harbor Power": {
      "lng": -76.5319,
      "lat": 43.4586
    },
    "Owens Corning at Bethlehem": {
      "lng": -73.865,
      "lat": 42.586944
    },
    "Owlville Creek Solar 2, LLC": {
      "lng": -75.77222,
      "lat": 43.078888
    },
    "Owlville Creek Solar, LLC": {
      "lng": -75.77959,
      "lat": 43.08748
    },
    "OYA State Route 122": {
      "lng": -74.265,
      "lat": 44.922
    },
    "Parishville": {
      "lng": -74.8258,
      "lat": 44.63
    },
    "Pasto Solar": {
      "lng": -76.523763,
      "lat": 42.210028
    },
    "Peak Power 1 Cogen": {
      "lng": -73.991667,
      "lat": 40.751111
    },
    "Pearl CSG": {
      "lng": -78.290418,
      "lat": 42.981335
    },
    "Pearl II": {
      "lng": -78.290419,
      "lat": 42.981336
    },
    "Pendleton Solar 1": {
      "lng": -78.75382,
      "lat": 43.09723
    },
    "Philadlephia": {
      "lng": -75.708778,
      "lat": 44.158611
    },
    "Phoenix Hydro Project": {
      "lng": -76.302137,
      "lat": 43.227058
    },
    "Piercefield": {
      "lng": -74.566217,
      "lat": 44.233419
    },
    "Pinelawn Power": {
      "lng": -73.388424,
      "lat": 40.736611
    },
    "Plant No 1 Freeport": {
      "lng": -73.5922,
      "lat": 40.6561
    },
    "Podunk Road CSG": {
      "lng": -76.65289,
      "lat": 42.46806
    },
    "Podunque Road CSG": {
      "lng": -78.23753,
      "lat": 42.415067
    },
    "Poletti 500 MW CC": {
      "lng": -73.9069,
      "lat": 40.7889
    },
    "Pool Brook Rd Community Solar Farm": {
      "lng": -75.083378,
      "lat": 42.542175
    },
    "Port Jefferson Energy Center": {
      "lng": -73.078507,
      "lat": 40.949696
    },
    "Port Leyden Hydroelectric Project": {
      "lng": -75.339167,
      "lat": 43.583611
    },
    "Port Richmond WWT Solar": {
      "lng": -74.126111,
      "lat": 40.638056
    },
    "Pouch Terminal": {
      "lng": -74.06849,
      "lat": 40.6182
    },
    "Prospect": {
      "lng": -75.147734,
      "lat": 43.291861
    },
    "Pyrites Plant": {
      "lng": -75.188108,
      "lat": 44.522043
    },
    "R E Ginna Nuclear Power Plant": {
      "lng": -77.3099,
      "lat": 43.2777
    },
    "Rainbow Falls Auscble": {
      "lng": -73.4617,
      "lat": 44.5236
    },
    "Rainbow Falls Hydro": {
      "lng": -74.8228,
      "lat": 44.5178
    },
    "Ransomville Solar 1, LLC": {
      "lng": -78.949837,
      "lat": 43.1994
    },
    "Ravenswood Generating Station": {
      "lng": -73.946111,
      "lat": 40.759167
    },
    "Raymondville": {
      "lng": -74.9803,
      "lat": 44.835
    },
    "RED-Rochester, LLC-Eastman Business Park": {
      "lng": -77.6319,
      "lat": 43.1989
    },
    "Regeneron Pharmaceuticals Inc.": {
      "lng": -73.82227,
      "lat": 41.07875
    },
    "Regeneron Tarrytown": {
      "lng": -73.820649,
      "lat": 41.077952
    },
    "Rensselaer Cogen": {
      "lng": -73.749955,
      "lat": 42.625325
    },
    "Richard M Flynn (Holtsville)": {
      "lng": -73.064,
      "lat": 40.8158
    },
    "Richmond University Medical Center": {
      "lng": -74.10579,
      "lat": 40.63589
    },
    "Riley Road LLC": {
      "lng": -74.078372,
      "lat": 41.469132
    },
    "Rio": {
      "lng": -74.7578,
      "lat": 41.4625
    },
    "RIT Henrietta Solar 1, LLC": {
      "lng": -77.66724,
      "lat": 43.076198
    },
    "River Valley LLC (NY) (csg)": {
      "lng": -78.47783,
      "lat": 42.06724
    },
    "Riverbay Corp. - Co-Op City": {
      "lng": -73.824437,
      "lat": 40.869972
    },
    "Riverhead": {
      "lng": -73.232222,
      "lat": 40.826111
    },
    "Riverhead Solar Farm": {
      "lng": -72.7645,
      "lat": 40.922992
    },
    "Roaring Brook, LLC": {
      "lng": -75.616219,
      "lat": 43.709597
    },
    "Robert Moses Niagara": {
      "lng": -79.0394,
      "lat": 43.1427
    },
    "Robert Moses Power Dam": {
      "lng": -74.7994,
      "lat": 45.0038
    },
    "Rochester 2": {
      "lng": -77.615,
      "lat": 43.1611
    },
    "Rochester 26": {
      "lng": -77.6098,
      "lat": 43.1522
    },
    "Rochester 5": {
      "lng": -77.6278,
      "lat": 43.1803
    },
    "Rockland Bakery Inc.": {
      "lng": -74.065114,
      "lat": 41.190589
    },
    "Rosemond Solar Community Solar": {
      "lng": -74.54163,
      "lat": 41.71133
    },
    "Roseton Generating LLC": {
      "lng": -73.966269,
      "lat": 41.573783
    },
    "Route 14A CDG Solar North and South LLC": {
      "lng": -77.046128,
      "lat": 42.815086
    },
    "Route 22 Community Solar Farm CSG": {
      "lng": -73.467034,
      "lat": 44.725076
    },
    "Rowe (CSG)": {
      "lng": -78.3752,
      "lat": 42.0687
    },
    "RT 52 Walden Solar 1, LLC Hybrid": {
      "lng": -74.2163,
      "lat": 41.5841
    },
    "RT32 Westerlo Solar 1": {
      "lng": -74.01607,
      "lat": 42.44623
    },
    "RT405 Westerlo Solar 2": {
      "lng": -74.01609,
      "lat": 42.44506
    },
    "S A Carlson": {
      "lng": -79.2478,
      "lat": 42.0933
    },
    "Sacket Lake Rd #1 Community Solar Farm": {
      "lng": -74.43526,
      "lat": 41.38251
    },
    "Sacket Lake Rd #2 Community Solar Farm": {
      "lng": -74.43526,
      "lat": 41.38251
    },
    "Saint Catherine of Siena Medical Center": {
      "lng": -73.27093,
      "lat": 40.89952
    },
    "Sangerfield Solar CSG": {
      "lng": -75.42005,
      "lat": 42.924419
    },
    "Saranac Power Partners, LP": {
      "lng": -73.4557,
      "lat": 44.7132
    },
    "Saratoga Solar LLC": {
      "lng": -73.903954,
      "lat": 43.07377
    },
    "Schaghticoke": {
      "lng": -73.5989,
      "lat": 42.8992
    },
    "School Street": {
      "lng": -73.707983,
      "lat": 42.78498
    },
    "Schuylerville": {
      "lng": -73.582107,
      "lat": 43.096823
    },
    "SDM000 Clarkson Fuel Cell": {
      "lng": -73.94415,
      "lat": 40.65511
    },
    "Selkirk Cogen Partners": {
      "lng": -73.8592,
      "lat": 42.5744
    },
    "Seneca Energy": {
      "lng": -76.8408,
      "lat": 42.9281
    },
    "Seneca Falls Hydroelectric Project": {
      "lng": -76.78672,
      "lat": 42.915299
    },
    "Seneca Nation Cattaraugus Wind Turbine": {
      "lng": -79.1027,
      "lat": 42.5809
    },
    "Sewalls": {
      "lng": -75.8942,
      "lat": 43.9775
    },
    "Sharon Springs": {
      "lng": -74.567145,
      "lat": 42.775105
    },
    "Sherman Island": {
      "lng": -73.7094,
      "lat": 43.2797
    },
    "Shoemaker": {
      "lng": -74.4186,
      "lat": 41.4278
    },
    "Shoreham Energy": {
      "lng": -72.865877,
      "lat": 40.957053
    },
    "Shoreham Solar Commons": {
      "lng": -72.888511,
      "lat": 40.934411
    },
    "Sissonville Hydro": {
      "lng": -75.002868,
      "lat": 44.685589
    },
    "Skidmore College": {
      "lng": -73.81132,
      "lat": 43.093682
    },
    "SL Babylon": {
      "lng": -73.375833,
      "lat": 40.730844
    },
    "Slate Hill(CSG)": {
      "lng": -74.503,
      "lat": 41.363
    },
    "Soft Maple": {
      "lng": -75.2433,
      "lat": 43.9206
    },
    "Solean Solar Project": {
      "lng": -78.448212,
      "lat": 42.086926
    },
    "South Cairo": {
      "lng": -73.986452,
      "lat": 42.291275
    },
    "South Colton": {
      "lng": -74.8831,
      "lat": 44.5133
    },
    "South Edwards": {
      "lng": -75.1961,
      "lat": 44.2678
    },
    "South Glens Falls Hydroelectric": {
      "lng": -73.63993,
      "lat": 43.304413
    },
    "South Goshen (Community Solar)": {
      "lng": -74.3478,
      "lat": 41.3927
    },
    "South Hampton": {
      "lng": -72.381977,
      "lat": 40.899997
    },
    "South Oaks Hospital": {
      "lng": -73.4258,
      "lat": 40.6853
    },
    "Southold": {
      "lng": -72.3761,
      "lat": 41.1059
    },
    "Spier Falls": {
      "lng": -73.7556,
      "lat": 43.2339
    },
    "St Johns Riverside Hospital": {
      "lng": -73.886589,
      "lat": 40.968929
    },
    "St. Johns Episcopal Hospital": {
      "lng": -73.753461,
      "lat": 40.598688
    },
    "St. Joseph Hospital": {
      "lng": -73.898015,
      "lat": 40.928827
    },
    "St. Lawrence University - Sutton": {
      "lng": -75.872257,
      "lat": 43.932397
    },
    "St. Luke's Cornwall Hospital": {
      "lng": -74.01454,
      "lat": 41.50365
    },
    "St. Mary's Hospital For Children": {
      "lng": -73.77045,
      "lat": 40.77698
    },
    "Stark": {
      "lng": -74.7614,
      "lat": 44.4553
    },
    "Steel Sun": {
      "lng": -78.845313,
      "lat": 42.812811
    },
    "Steel Sun 2: 2303-III-2 Hamburg Tpke": {
      "lng": -78.85337,
      "lat": 42.809003
    },
    "Steel Sun 2: 2303-III-4 Hamburg Tpke": {
      "lng": -78.854217,
      "lat": 42.811146
    },
    "Steel Sun 2: 2303-III-9 Hamburg Tpke": {
      "lng": -78.855377,
      "lat": 42.81849
    },
    "Steel Winds II": {
      "lng": -78.8617,
      "lat": 42.8072
    },
    "Steel Winds Wind Farm": {
      "lng": -78.8675,
      "lat": 42.8167
    },
    "Stephentown Spindle": {
      "lng": -73.377477,
      "lat": 42.556501
    },
    "Sterling Power Plant": {
      "lng": -75.600963,
      "lat": 43.080338
    },
    "Sterlington Greenworks LLC": {
      "lng": -72.440278,
      "lat": 41.0675
    },
    "Stewarts Bridge": {
      "lng": -73.8853,
      "lat": 43.2978
    },
    "Stillwater Hydro Electric Project": {
      "lng": -73.654393,
      "lat": 42.937465
    },
    "Stillwater Reservoir Hydro": {
      "lng": -75.04,
      "lat": 43.897386
    },
    "Stony Creek Wind Farm NY": {
      "lng": -78.415843,
      "lat": 42.767614
    },
    "Strauss CSG": {
      "lng": -76.993915,
      "lat": 42.891451
    },
    "Sturgeon": {
      "lng": -74.046678,
      "lat": 41.848191
    },
    "Stuyvesant Falls": {
      "lng": -73.733889,
      "lat": 42.351389
    },
    "Sugar Island": {
      "lng": -74.976473,
      "lat": 44.642181
    },
    "Sugarhill Road - Solitude Solar CSG": {
      "lng": -73.84853,
      "lat": 42.822572
    },
    "Sullivan County - Adult Care Solar": {
      "lng": -74.708077,
      "lat": 41.7966
    },
    "Sunlight Beacon": {
      "lng": -73.981635,
      "lat": 41.494632
    },
    "Sunvestment Energy Group NY 63 LLC": {
      "lng": -76.0419,
      "lat": 43.0447
    },
    "SUNY Old Westbury College": {
      "lng": -73.565661,
      "lat": 40.797858
    },
    "Sutter Greenworks LLC": {
      "lng": -72.771111,
      "lat": 40.928056
    },
    "Swinging Bridge 2": {
      "lng": -74.7828,
      "lat": 41.5717
    },
    "SWM Fuel Cell": {
      "lng": -72.93,
      "lat": 40.79
    },
    "Synergy Biogas": {
      "lng": -78.055,
      "lat": 42.828889
    },
    "Syracuse, LLC": {
      "lng": -76.2144,
      "lat": 43.0664
    },
    "Talcville": {
      "lng": -75.3083,
      "lat": 44.3083
    },
    "Tannery Island Power": {
      "lng": -75.6131,
      "lat": 43.98
    },
    "Tannery Road Landfill": {
      "lng": -75.534588,
      "lat": 43.239004
    },
    "Taylorville": {
      "lng": -75.321877,
      "lat": 43.927002
    },
    "Telegraph Rd #1 Community Solar Farm": {
      "lng": -78.17953,
      "lat": 42.561672
    },
    "Telegraph Rd #2 Community Solar Farm": {
      "lng": -78.179411,
      "lat": 42.559475
    },
    "The Allen Hospital": {
      "lng": -73.912967,
      "lat": 40.873268
    },
    "The Bank of New York": {
      "lng": -73.933752,
      "lat": 40.659328
    },
    "Theresa Plant": {
      "lng": -75.795173,
      "lat": 44.21742
    },
    "Ticonderoga Mill": {
      "lng": -73.396998,
      "lat": 43.891437
    },
    "Time Warner Cable - Knowles": {
      "lng": -77.375323,
      "lat": 42.337435
    },
    "Time Warner Cable Enterprises - Martino": {
      "lng": -75.43048,
      "lat": 43.183689
    },
    "Tioga Solar North(CSG)": {
      "lng": -76.28797,
      "lat": 42.14134
    },
    "Tioga Solar South(CSG)": {
      "lng": -76.28797,
      "lat": 42.14134
    },
    "Tompkins Cortland Community College": {
      "lng": -76.2865,
      "lat": 42.5025
    },
    "Town of Halfmoon": {
      "lng": -73.675675,
      "lat": 42.834978
    },
    "Town of Williamson Landfill PV": {
      "lng": -77.174722,
      "lat": 43.244444
    },
    "Townsend Road(CSG)": {
      "lng": -76.938014,
      "lat": 42.350101
    },
    "Trenton Falls": {
      "lng": -75.1567,
      "lat": 43.2758
    },
    "Troupsburg": {
      "lng": -77.3751,
      "lat": 42.0121
    },
    "Turner Rd Community Solar Project": {
      "lng": -76.67848,
      "lat": 42.09305
    },
    "Ulster County Solar": {
      "lng": -73.981234,
      "lat": 41.961896
    },
    "Underhill": {
      "lng": -73.884263,
      "lat": 41.716281
    },
    "Union Falls": {
      "lng": -73.913079,
      "lat": 44.510739
    },
    "Unionville Hydro Project 2499 NY": {
      "lng": -74.996581,
      "lat": 44.714518
    },
    "University of Rochester": {
      "lng": -77.628889,
      "lat": 43.123889
    },
    "Upper Beaver Falls Project": {
      "lng": -75.428056,
      "lat": 43.883611
    },
    "Upper Mechanicville": {
      "lng": -73.6805,
      "lat": 42.9126
    },
    "Upper Newton Falls": {
      "lng": -74.991389,
      "lat": 44.214444
    },
    "US Gypsum Oakfield": {
      "lng": -78.2972,
      "lat": 43.0647
    },
    "Valley Energy Center": {
      "lng": -74.4378,
      "lat": 41.4122
    },
    "Valley Falls Hydroelectric Facility": {
      "lng": -73.5625,
      "lat": 42.9044
    },
    "Varick": {
      "lng": -76.5056,
      "lat": 43.4486
    },
    "Vassar Brothers Medical Center": {
      "lng": -73.934817,
      "lat": 41.693606
    },
    "Vernon Boulevard": {
      "lng": -73.9508,
      "lat": 40.7537
    },
    "Victory Mills": {
      "lng": -76.5477,
      "lat": 42.9197
    },
    "Villa Roma Rd #1": {
      "lng": -74.960959,
      "lat": 41.757083
    },
    "Villa Roma Rd #2": {
      "lng": -74.961191,
      "lat": 41.756217
    },
    "Villa Roma Rd #3 CSG": {
      "lng": -74.96152,
      "lat": 41.754421
    },
    "Villa Roma Rd #4 CSG": {
      "lng": -74.958625,
      "lat": 41.754435
    },
    "Vischer Ferry": {
      "lng": -73.84269,
      "lat": 42.80746
    },
    "Volney II": {
      "lng": -76.354,
      "lat": 43.341
    },
    "Vulcraft Solar": {
      "lng": -76.61935,
      "lat": 42.005874
    },
    "Wading River Facility": {
      "lng": -72.864976,
      "lat": 40.96159
    },
    "Walden": {
      "lng": -74.195154,
      "lat": 41.55998
    },
    "Walden NY 1": {
      "lng": -74.145471,
      "lat": 41.552749
    },
    "Wappinger 9D Solar": {
      "lng": -73.937972,
      "lat": 41.56544
    },
    "Wappinger Falls Hydroelectric": {
      "lng": -73.922036,
      "lat": 41.598395
    },
    "Warrensburg Hydroelectric": {
      "lng": -73.7986,
      "lat": 43.4833
    },
    "Washington Avenue Solar, LLC": {
      "lng": -73.949911,
      "lat": 42.821552
    },
    "Washington St Community Solar Farm #1": {
      "lng": -77.846,
      "lat": 43.173
    },
    "Washington St Community Solar Farm #3": {
      "lng": -77.844,
      "lat": 43.172
    },
    "Washington St Community Solar Farm #4": {
      "lng": -77.849,
      "lat": 43.175
    },
    "Washingtonville(CSG)": {
      "lng": -74.167,
      "lat": 41.424
    },
    "Waste Management Madison County LFGTE": {
      "lng": -75.7028,
      "lat": 43.0336
    },
    "Watchtower Educational Center": {
      "lng": -73.57456,
      "lat": 41.498392
    },
    "Waterloo Hydroelectric Project": {
      "lng": -76.86233,
      "lat": 42.901914
    },
    "Waterport": {
      "lng": -78.2394,
      "lat": 43.3272
    },
    "Watertown": {
      "lng": -75.874669,
      "lat": 43.975801
    },
    "West Babylon Facility": {
      "lng": -73.348573,
      "lat": 40.695485
    },
    "West Coxsackie": {
      "lng": -73.815798,
      "lat": 42.350889
    },
    "West Delaware Tunnel Plant": {
      "lng": -74.510259,
      "lat": 41.859601
    },
    "West End Dam Hydroelectric Project": {
      "lng": -75.620583,
      "lat": 43.98289
    },
    "Westchester County Medical Center": {
      "lng": -73.80443,
      "lat": 41.08921
    },
    "Westerlo NY 1": {
      "lng": -74.015894,
      "lat": 42.43822
    },
    "Westtown CSG": {
      "lng": -74.561,
      "lat": 41.345
    },
    "Westtown NY 2": {
      "lng": -74.52377,
      "lat": 41.345455
    },
    "Wethersfield": {
      "lng": -78.2278,
      "lat": 42.6697
    },
    "Wethersfield Wind Farm": {
      "lng": -78.243792,
      "lat": 42.678819
    },
    "Wheelabrator Hudson Falls": {
      "lng": -73.591447,
      "lat": 43.3056
    },
    "Wheelabrator Westchester": {
      "lng": -73.942344,
      "lat": 41.277129
    },
    "White Plains Hospital Medical Center": {
      "lng": -73.75296,
      "lat": 41.032
    },
    "Whittier": {
      "lng": -77.861396,
      "lat": 43.150002
    },
    "William Floyd School District": {
      "lng": -72.850833,
      "lat": 40.776944
    },
    "Williams Rd": {
      "lng": -78.943,
      "lat": 43.204
    },
    "Williamson": {
      "lng": -77.0908,
      "lat": 43.1401
    },
    "Wiscoy 170": {
      "lng": -78.0828,
      "lat": 42.5042
    },
    "Woodhull Hospital": {
      "lng": -73.94186,
      "lat": 40.704774
    },
    "Woodoak Drive Community Solar Farm CSG": {
      "lng": -75.027,
      "lat": 41.594
    },
    "Wyckoff Heights Medical Center": {
      "lng": -73.91773,
      "lat": 40.70412
    },
    "Yaleville": {
      "lng": -74.9997,
      "lat": 44.7661
    },
    "AEP Bluffton NaS": {
      "lng": -83.8863,
      "lat": 40.8928
    },
    "Akron Recycle Energy Plant": {
      "lng": -81.530642,
      "lat": 41.073336
    },
    "Akron WRF": {
      "lng": -81.568889,
      "lat": 41.153333
    },
    "AMP Napoleon Solar Facility": {
      "lng": -84.110833,
      "lat": 41.406667
    },
    "AMP-Ohio Gas Turbines Bowling Green": {
      "lng": -83.641236,
      "lat": 41.397369
    },
    "AMP-Ohio Gas Turbines Galion": {
      "lng": -82.789,
      "lat": 40.7167
    },
    "AMP-Ohio Gas Turbines Napoleon": {
      "lng": -84.1095,
      "lat": 41.4079
    },
    "Anadarko": {
      "lng": -98.23,
      "lat": 35.0847
    },
    "Anthony Wayne Solar #1": {
      "lng": -83.574722,
      "lat": 41.627222
    },
    "Arcanum": {
      "lng": -84.5503,
      "lat": 39.9861
    },
    "Arcanum Peaking": {
      "lng": -84.5513,
      "lat": 39.9867
    },
    "Arcelormittal Cleveland Inc": {
      "lng": -81.6728,
      "lat": 41.4739
    },
    "ArcelorMittal Warren": {
      "lng": -80.8176,
      "lat": 41.2119
    },
    "Ashtabula": {
      "lng": -80.7612,
      "lat": 41.890026
    },
    "Auglaize Hydro": {
      "lng": -84.399756,
      "lat": 41.237206
    },
    "Avon Lake Power Plant": {
      "lng": -82.054619,
      "lat": 41.504453
    },
    "Battery Utility of Ohio": {
      "lng": -82.833056,
      "lat": 40.260278
    },
    "Bay Shore": {
      "lng": -83.4378,
      "lat": 41.6917
    },
    "Bay View Backup Power Facility": {
      "lng": -83.484722,
      "lat": 41.688333
    },
    "Bay View Cogeneration Facility": {
      "lng": -83.484722,
      "lat": 41.688333
    },
    "Blue Creek Wind Project": {
      "lng": -84.561824,
      "lat": 40.931515
    },
    "Bowling Green Wind": {
      "lng": -83.7383,
      "lat": 41.3786
    },
    "Bridgewater Dairy Anaerobic Digester": {
      "lng": -84.6694,
      "lat": 41.6292
    },
    "Broshco Fabricated Products": {
      "lng": -82.568611,
      "lat": 40.776111
    },
    "Brown County LFGTE Power Station": {
      "lng": -83.903,
      "lat": 38.895
    },
    "Bryan (OH)": {
      "lng": -84.543148,
      "lat": 41.471822
    },
    "Bryan Peaking": {
      "lng": -84.527,
      "lat": 41.4609
    },
    "Bryan Solar Field": {
      "lng": -84.586111,
      "lat": 41.476389
    },
    "Carbon Limestone": {
      "lng": -80.5208,
      "lat": 40.9975
    },
    "Cardinal": {
      "lng": -80.6486,
      "lat": 40.2522
    },
    "Carroll County Energy": {
      "lng": -81.05918,
      "lat": 40.60441
    },
    "Celina Solar Project #1, LLC": {
      "lng": -84.590556,
      "lat": 40.540556
    },
    "Central Utility Plant Cincinnati": {
      "lng": -84.509419,
      "lat": 39.134533
    },
    "Clean Energy Future - Lordstown, LLC": {
      "lng": -80.85162,
      "lat": 41.14889
    },
    "Cleveland Peaking": {
      "lng": -81.588233,
      "lat": 41.556714
    },
    "Cleveland Thermal": {
      "lng": -81.682272,
      "lat": 41.5094
    },
    "Clinton Battery": {
      "lng": -83.960782,
      "lat": 39.295883
    },
    "Clyde Peaking Engine": {
      "lng": -82.965261,
      "lat": 41.293294
    },
    "Clyde Solar Array": {
      "lng": -82.984331,
      "lat": 41.319351
    },
    "Collinwood": {
      "lng": -81.58796,
      "lat": 41.556867
    },
    "Collinwood BioEnergy Facility": {
      "lng": -81.589167,
      "lat": 41.555556
    },
    "Cooper Farms VW Project": {
      "lng": -84.570277,
      "lat": 40.905833
    },
    "CU Solar Plant": {
      "lng": -83.816944,
      "lat": 39.741944
    },
    "Cuyahoga County Landfill": {
      "lng": -81.751526,
      "lat": 41.443122
    },
    "Cuyahoga Falls 1": {
      "lng": -81.474535,
      "lat": 41.141773
    },
    "Darby Electric Generating Station": {
      "lng": -83.1778,
      "lat": 39.7139
    },
    "Davis Besse": {
      "lng": -83.0861,
      "lat": 41.5967
    },
    "Denison Solar Array": {
      "lng": -82.517957,
      "lat": 40.073337
    },
    "DG AMP 1048 Wadsworth": {
      "lng": -81.764344,
      "lat": 41.012786
    },
    "DG AMP Rittman Rd": {
      "lng": -81.75074,
      "lat": 41.000755
    },
    "DG AMP Solar Bowling Green": {
      "lng": -83.582,
      "lat": 41.396
    },
    "DG AMP Solar Brewster": {
      "lng": -81.58983,
      "lat": 40.702397
    },
    "DG AMP Solar Jackson Center": {
      "lng": -84.047247,
      "lat": 40.444969
    },
    "DG AMP Solar Orrville 3": {
      "lng": -81.759461,
      "lat": 40.856428
    },
    "DG AMP Solar Piqua Manier": {
      "lng": -84.25545,
      "lat": 40.13043
    },
    "DG AMP Solar Piqua Staunton": {
      "lng": -84.23148,
      "lat": 40.140866
    },
    "DG AMP Solar Versailles": {
      "lng": -84.4978,
      "lat": 40.2207
    },
    "Dicks Creek Power Company LLC": {
      "lng": -84.3778,
      "lat": 39.465
    },
    "Dodge Park Engine No 1": {
      "lng": -83.014425,
      "lat": 39.952367
    },
    "Dover Peaking": {
      "lng": -81.4985,
      "lat": 40.5416
    },
    "Dresden Energy Facility": {
      "lng": -82.0276,
      "lat": 40.0928
    },
    "East Campus Utility Plant": {
      "lng": -84.504948,
      "lat": 39.137935
    },
    "Edgerton": {
      "lng": -84.767208,
      "lat": 41.449992
    },
    "Euclid Farm, Stamco N-54": {
      "lng": -81.496944,
      "lat": 41.602222
    },
    "Findlay Wind Farm": {
      "lng": -83.644394,
      "lat": 41.101219
    },
    "Fremont Energy Center": {
      "lng": -83.161388,
      "lat": 41.377116
    },
    "Galion": {
      "lng": -82.7939,
      "lat": 40.7303
    },
    "Gen J M Gavin": {
      "lng": -82.1158,
      "lat": 38.9347
    },
    "Geneva": {
      "lng": -80.91,
      "lat": 41.795
    },
    "Genoa Diesel Generating Station": {
      "lng": -83.3606,
      "lat": 41.5092
    },
    "Greenup Hydro": {
      "lng": -82.8594,
      "lat": 38.6472
    },
    "Greenville Electric Gen Station": {
      "lng": -84.615136,
      "lat": 40.075783
    },
    "Hamilton Hydro": {
      "lng": -84.5558,
      "lat": 39.4128
    },
    "Hamilton Municipal Power Plant": {
      "lng": -84.5543,
      "lat": 39.4098
    },
    "Hanging Rock Power Company LLC": {
      "lng": -82.7833,
      "lat": 38.5731
    },
    "Hardin Solar Energy LLC": {
      "lng": -83.813708,
      "lat": 40.659506
    },
    "Harpster Wind": {
      "lng": -83.22865,
      "lat": 40.718292
    },
    "Haverhill North Cogeneration Facility": {
      "lng": -82.828642,
      "lat": 38.596657
    },
    "Haviland Plastic Products": {
      "lng": -84.5875,
      "lat": 41.016111
    },
    "Hillcrest Solar": {
      "lng": -83.906046,
      "lat": 39.076972
    },
    "HMV Minster Energy Storage System": {
      "lng": -84.387266,
      "lat": 40.398844
    },
    "HMW Minster PV I": {
      "lng": -84.401595,
      "lat": 40.390362
    },
    "Hog Creek Wind Project": {
      "lng": -83.728714,
      "lat": 40.793073
    },
    "Jackson Cntr Peaking": {
      "lng": -84.0401,
      "lat": 40.4597
    },
    "KSU Trumbull": {
      "lng": -80.83665,
      "lat": 41.27874
    },
    "Kyger Creek": {
      "lng": -82.1289,
      "lat": 38.9144
    },
    "LafargeHolcim - Paulding Wind Project": {
      "lng": -84.61256,
      "lat": 41.192377
    },
    "LE Wind Turbine 1": {
      "lng": -81.525555,
      "lat": 41.584444
    },
    "Long Ridge Energy Generation": {
      "lng": -80.841847,
      "lat": 39.704887
    },
    "Loraine County Project": {
      "lng": -82.1803,
      "lat": 41.3006
    },
    "Lordstown Motor Corp": {
      "lng": -80.883753,
      "lat": 41.148019
    },
    "Madison Generating Station": {
      "lng": -84.4647,
      "lat": 39.4522
    },
    "Mahoning": {
      "lng": -80.586111,
      "lat": 40.914444
    },
    "MCCo Solar Generating Facility": {
      "lng": -81.597222,
      "lat": 41.516389
    },
    "Miami Fort Power Station": {
      "lng": -84.8036,
      "lat": 39.1128
    },
    "Middletown Coke Company, LLC": {
      "lng": -84.401389,
      "lat": 39.472222
    },
    "Middletown Energy Center": {
      "lng": -84.347778,
      "lat": 39.465
    },
    "Model Gas Power Station": {
      "lng": -83.0272,
      "lat": 39.8908
    },
    "Monroeville Solar": {
      "lng": -82.711785,
      "lat": 41.230142
    },
    "Montpelier": {
      "lng": -84.5794,
      "lat": 41.5855
    },
    "Monument": {
      "lng": -84.1739,
      "lat": 39.7667
    },
    "Morton Salt Rittman": {
      "lng": -81.7756,
      "lat": 40.9692
    },
    "Napoleon Biogas": {
      "lng": -84.1025,
      "lat": 41.355556
    },
    "Napoleon Peaking": {
      "lng": -84.1095,
      "lat": 41.4079
    },
    "Napoleon Solar I": {
      "lng": -84.095841,
      "lat": 41.388431
    },
    "New Knoxville": {
      "lng": -84.309201,
      "lat": 40.499364
    },
    "Niles": {
      "lng": -80.74769,
      "lat": 41.166928
    },
    "Northwest Ohio Wind": {
      "lng": -84.593611,
      "lat": 41.016944
    },
    "O H Hutchings": {
      "lng": -84.2921,
      "lat": 39.6088
    },
    "Oberlin (OH)": {
      "lng": -82.2194,
      "lat": 41.2836
    },
    "Oberlin Spear Point Solar One": {
      "lng": -82.227047,
      "lat": 41.302761
    },
    "Ohio Northern University Solar Site": {
      "lng": -83.8391,
      "lat": 40.759622
    },
    "Omega JV2 Bowling Green": {
      "lng": -83.6402,
      "lat": 41.3884
    },
    "Omega JV2 Hamilton": {
      "lng": -84.5183,
      "lat": 39.3528
    },
    "Oregon Clean Energy Center": {
      "lng": -83.443664,
      "lat": 41.667928
    },
    "Orrville": {
      "lng": -81.765764,
      "lat": 40.850964
    },
    "Orrville Peaking": {
      "lng": -81.7586,
      "lat": 40.86
    },
    "O'Shaughnessy Hydro": {
      "lng": -83.126719,
      "lat": 40.153328
    },
    "Ottawa County Project": {
      "lng": -83.0353,
      "lat": 41.5283
    },
    "Owens Corning Headquarters": {
      "lng": -83.5364,
      "lat": 41.6447
    },
    "Painesville": {
      "lng": -81.254,
      "lat": 41.7265
    },
    "Parking Lot Array": {
      "lng": -84.51074,
      "lat": 39.141852
    },
    "Paulding Wind Farm II": {
      "lng": -84.779166,
      "lat": 41.025278
    },
    "Paulding Wind Farm III": {
      "lng": -84.751,
      "lat": 41.121
    },
    "Perry": {
      "lng": -81.1439,
      "lat": 41.8006
    },
    "Piqua Power Plant": {
      "lng": -84.237258,
      "lat": 40.133848
    },
    "Pixelle Specialty Solutions LLC": {
      "lng": -82.974039,
      "lat": 39.324611
    },
    "POET Biorefining - Fostoria": {
      "lng": -83.3777,
      "lat": 41.1718
    },
    "POET Biorefining - Leipsic, LLC": {
      "lng": -83.97062,
      "lat": 41.12055
    },
    "POET Biorefining - Marion, LLC": {
      "lng": -83.157,
      "lat": 40.622
    },
    "Procter & Gamble Cincinnati Plant": {
      "lng": -84.5042,
      "lat": 39.175
    },
    "Progress Drive Generation Station": {
      "lng": -82.4,
      "lat": 40.53
    },
    "Prospect Municipal": {
      "lng": -83.1883,
      "lat": 40.4411
    },
    "PS ST-8 Engine No 1": {
      "lng": -83.049072,
      "lat": 39.961419
    },
    "Racine": {
      "lng": -81.9081,
      "lat": 38.9153
    },
    "Renewable Energy Services of Ohio": {
      "lng": -82.602,
      "lat": 41.3362
    },
    "Renick Run Pumping Station 03-04": {
      "lng": -83.012,
      "lat": 39.9294
    },
    "Richland Peaking Station": {
      "lng": -84.334622,
      "lat": 41.305203
    },
    "Robert P Mone": {
      "lng": -84.7392,
      "lat": 40.9297
    },
    "Rolling Hills Generating LLC": {
      "lng": -82.3328,
      "lat": 39.0839
    },
    "RP Wind": {
      "lng": -83.893333,
      "lat": 40.449444
    },
    "Sauder Power Plant": {
      "lng": -84.293549,
      "lat": 41.515211
    },
    "Scioto Ridge Wind Farm": {
      "lng": -83.736389,
      "lat": 40.561389
    },
    "Seville": {
      "lng": -81.8714,
      "lat": 41.0317
    },
    "Shelby North": {
      "lng": -82.6625,
      "lat": 40.8969
    },
    "Shelby Solar Array": {
      "lng": -82.672395,
      "lat": 40.896042
    },
    "Shelby South": {
      "lng": -82.653333,
      "lat": 40.908889
    },
    "Sidney (OH)": {
      "lng": -84.19097,
      "lat": 40.27917
    },
    "South Field Energy, LLC": {
      "lng": -80.678245,
      "lat": 40.63534
    },
    "Springfield Solar LLC": {
      "lng": -83.805556,
      "lat": 39.893889
    },
    "St Marys": {
      "lng": -84.3889,
      "lat": 40.5458
    },
    "ST-1/1A Engine No 1": {
      "lng": -83.015833,
      "lat": 39.95
    },
    "Stryker": {
      "lng": -84.4283,
      "lat": 41.5014
    },
    "Suburban Landfill Gas Recovery": {
      "lng": -82.244992,
      "lat": 39.912438
    },
    "Summit Street Power Plant": {
      "lng": -81.340833,
      "lat": 41.143056
    },
    "Tait Electric Generating Station": {
      "lng": -84.211084,
      "lat": 39.727249
    },
    "Timber Road IV": {
      "lng": -84.751,
      "lat": 41.121
    },
    "Toledo Ref Power Recovery Train": {
      "lng": -83.5042,
      "lat": 41.6325
    },
    "Troy Energy, LLC": {
      "lng": -83.45983,
      "lat": 41.47724
    },
    "Valfilm Wind Project": {
      "lng": -83.644131,
      "lat": 41.084329
    },
    "Versailles Peaking": {
      "lng": -84.4769,
      "lat": 40.2247
    },
    "W H Sammis": {
      "lng": -80.6319,
      "lat": 40.5317
    },
    "W H Zimmer Generating Station": {
      "lng": -84.2289,
      "lat": 38.8675
    },
    "Walter C Beckjord": {
      "lng": -84.2981,
      "lat": 38.9917
    },
    "Wapakoneta-Pratt": {
      "lng": -84.187,
      "lat": 40.544
    },
    "Washington Power Company LLC": {
      "lng": -81.6564,
      "lat": 39.58
    },
    "Waterford Plant": {
      "lng": -81.7178,
      "lat": 39.5333
    },
    "Wellington": {
      "lng": -82.227208,
      "lat": 41.163069
    },
    "West 41st Street": {
      "lng": -81.7145,
      "lat": 41.474
    },
    "West Lorain": {
      "lng": -82.264444,
      "lat": 41.429444
    },
    "Whirlpool Corp-Greenville Wind Farm": {
      "lng": -84.609857,
      "lat": 40.131293
    },
    "Whirlpool Corporation - Marion Wind Farm": {
      "lng": -83.178491,
      "lat": 40.591154
    },
    "Whirlpool Corporation - Ottawa Wind Farm": {
      "lng": -84.033742,
      "lat": 41.002764
    },
    "Willey Battery Utility": {
      "lng": -84.680313,
      "lat": 39.287747
    },
    "Woodsdale": {
      "lng": -84.4611,
      "lat": 39.4492
    },
    "Wooster Renewable Energy": {
      "lng": -81.951389,
      "lat": 40.785
    },
    "Wyandot Solar Farm": {
      "lng": -83.317072,
      "lat": 40.8795
    },
    "Yankee Street": {
      "lng": -84.2047,
      "lat": 39.603
    },
    "Zephyr Wind": {
      "lng": -83.641381,
      "lat": 41.096111
    },
    "Zephyr Wind Project - 2.0": {
      "lng": -83.637778,
      "lat": 41.093078
    },
    "Arbuckle Mountain Wind Farm LLC": {
      "lng": -97.136944,
      "lat": 34.386944
    },
    "Armadillo Flats Wind Project, LLC": {
      "lng": -97.567042,
      "lat": 36.300964
    },
    "Balko Wind LLC": {
      "lng": -100.847222,
      "lat": 36.518056
    },
    "Blue Canyon Windpower": {
      "lng": -98.577311,
      "lat": 34.850781
    },
    "Blue Canyon Windpower II": {
      "lng": -98.6056,
      "lat": 34.8661
    },
    "Blue Canyon Windpower V LLC": {
      "lng": -98.4525,
      "lat": 34.8217
    },
    "Blue Canyon Windpower VI LLC": {
      "lng": -98.5599,
      "lat": 34.910361
    },
    "Bluestem": {
      "lng": -100.587557,
      "lat": 36.552151
    },
    "Boiling Springs Wind Farm": {
      "lng": -99.357603,
      "lat": 36.546545
    },
    "Boomer Lake Station": {
      "lng": -97.0681,
      "lat": 36.1438
    },
    "Breckinridge Wind Project LLC": {
      "lng": -97.685556,
      "lat": 36.476944
    },
    "Broken Bow Dam": {
      "lng": -94.684498,
      "lat": 34.138779
    },
    "Buffalo Bear LLC": {
      "lng": -99.6353,
      "lat": 36.7767
    },
    "Cana Gas Processing Plant": {
      "lng": -98.1011,
      "lat": 35.5331
    },
    "Canadian Hills Wind": {
      "lng": -98.024722,
      "lat": 35.629722
    },
    "Centennial Wind Farm": {
      "lng": -99.601869,
      "lat": 36.637733
    },
    "Charles D Lamb Energy Center": {
      "lng": -97.125278,
      "lat": 36.813889
    },
    "Chickasaw Nation Solar Farm": {
      "lng": -97.105237,
      "lat": 34.47741
    },
    "Chisholm View Wind Project": {
      "lng": -97.673611,
      "lat": 36.574722
    },
    "Choctaw Nation Solar Farm": {
      "lng": -96.434578,
      "lat": 33.968856
    },
    "Chouteau Power Plant": {
      "lng": -95.277064,
      "lat": 36.222058
    },
    "Comanche (8059)": {
      "lng": -98.3244,
      "lat": 34.5431
    },
    "Covanta Tulsa Renewable Energy LLC": {
      "lng": -96.0181,
      "lat": 36.1319
    },
    "Covington Solar Farm": {
      "lng": -97.58589,
      "lat": 36.31792
    },
    "Crossroads Wind Farm": {
      "lng": -98.722222,
      "lat": 36.035
    },
    "Cushing": {
      "lng": -96.775683,
      "lat": 35.982597
    },
    "Cyril": {
      "lng": -98.227872,
      "lat": 34.8981
    },
    "Dempsey Ridge Wind Farm": {
      "lng": -99.814456,
      "lat": 35.529778
    },
    "Diamond Spring, LLC": {
      "lng": -96.780459,
      "lat": 34.487369
    },
    "Drift Sand Wind Project LLC": {
      "lng": -97.957222,
      "lat": 34.827778
    },
    "Elk City LLC": {
      "lng": -99.8983,
      "lat": 35.4642
    },
    "Eufaula Dam": {
      "lng": -95.3572,
      "lat": 35.3069
    },
    "Fort Gibson": {
      "lng": -95.2269,
      "lat": 35.8693
    },
    "Frontier Generating Station": {
      "lng": -97.625,
      "lat": 35.439722
    },
    "Frontier Windpower": {
      "lng": -97.181015,
      "lat": 36.838884
    },
    "Frontier Windpower II": {
      "lng": -97.029777,
      "lat": 36.82478
    },
    "Georgia-Pacific Muskogee LLC": {
      "lng": -95.2939,
      "lat": 35.7322
    },
    "Glass Sands Wind Facility": {
      "lng": -96.915909,
      "lat": 34.514165
    },
    "Goodwell Wind Project LLC": {
      "lng": -101.565556,
      "lat": 36.541389
    },
    "Grand River Dam Authority": {
      "lng": -95.2894,
      "lat": 36.190278
    },
    "Grant Plains Wind, LLC": {
      "lng": -97.7053,
      "lat": 36.9708
    },
    "Grant Wind, LLC": {
      "lng": -97.696331,
      "lat": 36.927071
    },
    "Great Western Wind Energy, LLC": {
      "lng": -99.596006,
      "lat": 36.166503
    },
    "Green Country Energy, LLC": {
      "lng": -95.9346,
      "lat": 35.9833
    },
    "Hinton": {
      "lng": -98.385661,
      "lat": 35.494656
    },
    "Horseshoe Lake": {
      "lng": -97.17969,
      "lat": 35.50866
    },
    "Hugo": {
      "lng": -95.3206,
      "lat": 34.0158
    },
    "International Paper Valliant OK": {
      "lng": -95.1114,
      "lat": 33.9961
    },
    "Kaw Hydro": {
      "lng": -96.9278,
      "lat": 36.6994
    },
    "Kay Wind, LLC": {
      "lng": -97.133611,
      "lat": 36.984167
    },
    "Keenan II Renewable Energy Co LLC": {
      "lng": -99.490833,
      "lat": 36.276111
    },
    "Keystone Dam": {
      "lng": -96.2517,
      "lat": 36.1508
    },
    "King Plains Wind Project": {
      "lng": -97.477464,
      "lat": 36.400148
    },
    "Kingfisher": {
      "lng": -97.9277,
      "lat": 35.8575
    },
    "Kingfisher Wind LLC": {
      "lng": -97.799722,
      "lat": 35.770278
    },
    "KODE Novus I": {
      "lng": -101.393611,
      "lat": 36.543889
    },
    "KODE Novus II": {
      "lng": -101.495936,
      "lat": 36.548958
    },
    "Laverne Diesel Generating Plant": {
      "lng": -99.8906,
      "lat": 36.7106
    },
    "Little Elk Wind Project LLC": {
      "lng": -98.907222,
      "lat": 35.194722
    },
    "Mammoth Plains": {
      "lng": -98.710278,
      "lat": 35.978889
    },
    "Mangum": {
      "lng": -99.5022,
      "lat": 34.8836
    },
    "Marietta": {
      "lng": -97.108608,
      "lat": 33.943183
    },
    "Markham": {
      "lng": -95.1822,
      "lat": 36.2317
    },
    "Maverick Wind Project, LLC": {
      "lng": -98.164671,
      "lat": 36.293919
    },
    "McClain Energy Facility": {
      "lng": -97.58985,
      "lat": 35.29774
    },
    "Minco Wind I, LLC": {
      "lng": -97.975556,
      "lat": 35.280278
    },
    "Minco Wind II, LLC": {
      "lng": -98.071389,
      "lat": 35.325278
    },
    "Minco Wind III, LLC": {
      "lng": -98.182731,
      "lat": 35.372359
    },
    "Minco Wind IV, LLC": {
      "lng": -98.269562,
      "lat": 35.381047
    },
    "Mooreland": {
      "lng": -99.22543,
      "lat": 36.43725
    },
    "Muskogee": {
      "lng": -95.28732,
      "lat": 35.76135
    },
    "Mustang": {
      "lng": -97.67498,
      "lat": 35.46988
    },
    "NextEra-Blackwell Wind, LLC": {
      "lng": -97.4175,
      "lat": 36.846667
    },
    "Norman Solar": {
      "lng": -97.360353,
      "lat": 35.226913
    },
    "Northeastern": {
      "lng": -95.7008,
      "lat": 36.4317
    },
    "Oklahoma Wind Energy Center": {
      "lng": -99.340047,
      "lat": 36.603967
    },
    "Oneta Energy Center": {
      "lng": -95.6967,
      "lat": 36.0119
    },
    "Origin Wind": {
      "lng": -97.269722,
      "lat": 34.446111
    },
    "Osage Wind, LLC": {
      "lng": -96.700278,
      "lat": 36.706944
    },
    "OU Spirit Wind Farm": {
      "lng": -99.480144,
      "lat": 36.333803
    },
    "Pawhuska": {
      "lng": -96.347408,
      "lat": 36.669425
    },
    "Pensacola": {
      "lng": -95.041389,
      "lat": 36.4675
    },
    "Persimmon Creek Wind Farm 1, LLC": {
      "lng": -99.349164,
      "lat": 36.174278
    },
    "Pine Ridge": {
      "lng": -98.444717,
      "lat": 35.013397
    },
    "Ponca": {
      "lng": -97.085758,
      "lat": 36.71979
    },
    "Ponderosa Wind Energy Center": {
      "lng": -100.668949,
      "lat": 36.555532
    },
    "Red Dirt Wind Project": {
      "lng": -97.8,
      "lat": 36.1
    },
    "Red Hills Wind Project LLC": {
      "lng": -99.3828,
      "lat": 35.535
    },
    "Redbed Plains Wind Farm": {
      "lng": -97.854,
      "lat": 35.290759
    },
    "Redbud Power Plant": {
      "lng": -97.2264,
      "lat": 35.6856
    },
    "River Valley": {
      "lng": -94.6458,
      "lat": 35.1931
    },
    "Riverside (4940)": {
      "lng": -95.9567,
      "lat": 35.9978
    },
    "Robert S Kerr": {
      "lng": -94.7755,
      "lat": 35.34447
    },
    "Rock Falls Wind Farm LLC": {
      "lng": -97.4271,
      "lat": 36.927165
    },
    "Rockhaven Wind Project, LLC": {
      "lng": -97.361366,
      "lat": 34.475095
    },
    "Rocky Ridge Wind Project": {
      "lng": -99.003056,
      "lat": 35.135556
    },
    "Rush Springs Energy Storage (BA)": {
      "lng": -97.829705,
      "lat": 34.695647
    },
    "Rush Springs Wind": {
      "lng": -97.829283,
      "lat": 34.682883
    },
    "Salina": {
      "lng": -95.103558,
      "lat": 36.264685
    },
    "Seiling Wind I": {
      "lng": -98.992222,
      "lat": 36.1225
    },
    "Seiling Wind II": {
      "lng": -98.992222,
      "lat": 36.1225
    },
    "Seminole (2956)": {
      "lng": -96.7258,
      "lat": 34.96645
    },
    "Skeleton Creek Energy Center Hybrid": {
      "lng": -98.03436,
      "lat": 36.521428
    },
    "Sleeping Bear LLC": {
      "lng": -99.500556,
      "lat": 36.640278
    },
    "Sooner": {
      "lng": -97.05279,
      "lat": 36.45307
    },
    "Southwestern": {
      "lng": -98.3524,
      "lat": 35.1009
    },
    "Spring Creek Power Plant": {
      "lng": -97.655,
      "lat": 35.7422
    },
    "Stillwater Energy Center": {
      "lng": -97.036944,
      "lat": 36.161944
    },
    "Stillwater Water Treatment Plant": {
      "lng": -97.0739,
      "lat": 36.2039
    },
    "Sundance Wind Project, LLC": {
      "lng": -98.7492,
      "lat": 36.5597
    },
    "Taloga Wind LLC": {
      "lng": -98.968333,
      "lat": 35.855556
    },
    "Tenaska Kiamichi Generating Station": {
      "lng": -95.9349,
      "lat": 34.6831
    },
    "Tenkiller Ferry": {
      "lng": -95.0511,
      "lat": 35.595
    },
    "Thunder Ranch Wind Project": {
      "lng": -97.058944,
      "lat": 36.550834
    },
    "Tinker": {
      "lng": -97.373445,
      "lat": 35.414719
    },
    "Tulsa": {
      "lng": -95.990974,
      "lat": 36.116492
    },
    "Tulsa LFG LLC": {
      "lng": -96.186509,
      "lat": 36.161953
    },
    "Tuttle": {
      "lng": -97.846528,
      "lat": 35.266556
    },
    "University of Oklahoma": {
      "lng": -97.4424,
      "lat": 35.2089
    },
    "Waynoka Gas Processing Plant": {
      "lng": -98.7656,
      "lat": 36.651
    },
    "Weatherford Wind Energy Center": {
      "lng": -98.7346,
      "lat": 35.5094
    },
    "Webbers Falls": {
      "lng": -95.1708,
      "lat": 35.5531
    },
    "Weleetka": {
      "lng": -96.1352,
      "lat": 35.3233
    },
    "WFEC GenCo LLC": {
      "lng": -98.2264,
      "lat": 35.0833
    },
    "Wildhorse Mountain Wind Facility": {
      "lng": -95.191808,
      "lat": 34.591031
    },
    "45 Mile Hydroelectric Project": {
      "lng": -121.152778,
      "lat": 44.528056
    },
    "Adams Solar Center": {
      "lng": -121.122,
      "lat": 44.675
    },
    "Airport Solar": {
      "lng": -120.402317,
      "lat": 42.170176
    },
    "Alden Bailey Power Plant": {
      "lng": -123.40596,
      "lat": 46.158402
    },
    "Alkali Solar, LLC": {
      "lng": -121.0035,
      "lat": 43.31942
    },
    "Arlington Wind Power Project": {
      "lng": -120.2008,
      "lat": 45.7167
    },
    "Baker City Solar": {
      "lng": -117.763482,
      "lat": 44.702418
    },
    "Baldock Solar Highway": {
      "lng": -122.771667,
      "lat": 45.269444
    },
    "Ballston Solar LLC": {
      "lng": -123.395,
      "lat": 45.077
    },
    "BC Solar": {
      "lng": -120.3598,
      "lat": 42.175
    },
    "Bear Creek Solar Center": {
      "lng": -121.237203,
      "lat": 44.060814
    },
    "Beaver": {
      "lng": -123.17392,
      "lat": 46.172401
    },
    "Bellevue Solar Project": {
      "lng": -123.234722,
      "lat": 45.112222
    },
    "Bend": {
      "lng": -121.313331,
      "lat": 44.062326
    },
    "Benson Creek Windfarm (Burnt River)": {
      "lng": -117.343889,
      "lat": 44.366944
    },
    "Big Cliff": {
      "lng": -122.2832,
      "lat": 44.7514
    },
    "Big Top LLC": {
      "lng": -119.4647,
      "lat": 45.655
    },
    "Bighorn Solar": {
      "lng": -122.954234,
      "lat": 44.965702
    },
    "Biglow Canyon Wind Farm": {
      "lng": -120.603414,
      "lat": 45.653745
    },
    "Biomass One LP": {
      "lng": -122.849958,
      "lat": 42.436111
    },
    "Black Cap Solar Plant": {
      "lng": -120.362631,
      "lat": 42.17505
    },
    "Bly Solar Center": {
      "lng": -121.024494,
      "lat": 42.392193
    },
    "Bonneville": {
      "lng": -121.941009,
      "lat": 45.6441
    },
    "Boring Solar LLC": {
      "lng": -122.285,
      "lat": 45.431
    },
    "Brightwood Solar, LLC": {
      "lng": -122.084421,
      "lat": 45.384949
    },
    "BristolSolar": {
      "lng": -122.79,
      "lat": 45.15
    },
    "Brush Creek Solar": {
      "lng": -122.813127,
      "lat": 44.984217
    },
    "Brush Solar Center": {
      "lng": -118.204075,
      "lat": 44.465334
    },
    "Butler Solar, LLC": {
      "lng": -123.410102,
      "lat": 45.101804
    },
    "Butter Creek Power LLC": {
      "lng": -119.4222,
      "lat": 45.7017
    },
    "Carmen Smith": {
      "lng": -122.041352,
      "lat": 44.285977
    },
    "Carty Generating Station": {
      "lng": -119.813056,
      "lat": 45.698611
    },
    "Case Creek Solar": {
      "lng": -122.912,
      "lat": 45.172
    },
    "C-Drop Hydro": {
      "lng": -121.685,
      "lat": 42.165833
    },
    "Chiloquin Solar, LLC": {
      "lng": -121.861248,
      "lat": 42.610732
    },
    "Chopin Wind LLC": {
      "lng": -118.463333,
      "lat": 45.88
    },
    "Clearwater 1": {
      "lng": -122.337924,
      "lat": 43.251244
    },
    "Clearwater 2": {
      "lng": -122.409807,
      "lat": 43.266894
    },
    "Coffin Butte": {
      "lng": -123.2234,
      "lat": 44.6969
    },
    "Co-Gen II LLC": {
      "lng": -123.359138,
      "lat": 42.961126
    },
    "Co-Gen LLC": {
      "lng": -118.720601,
      "lat": 44.458696
    },
    "Colton Solar": {
      "lng": -122.473,
      "lat": 45.17
    },
    "Combine Hills I": {
      "lng": -118.591,
      "lat": 45.943
    },
    "Condon Windpower LLC": {
      "lng": -120.2794,
      "lat": 45.2766
    },
    "Copper Dam Plant": {
      "lng": -121.523761,
      "lat": 45.66523
    },
    "Cougar": {
      "lng": -122.2439,
      "lat": 44.1307
    },
    "Covanta Marion Inc": {
      "lng": -122.962761,
      "lat": 45.048467
    },
    "Coyote Springs": {
      "lng": -119.67392,
      "lat": 45.847998
    },
    "Coyote Springs II": {
      "lng": -119.673965,
      "lat": 45.848039
    },
    "Dalles Dam North Fishway Project": {
      "lng": -121.136885,
      "lat": 45.614375
    },
    "Day Hill Solar, LLC": {
      "lng": -122.32942,
      "lat": 45.26866
    },
    "Dayton Cutoff Solar": {
      "lng": -123.135,
      "lat": 45.17
    },
    "Detroit": {
      "lng": -122.2511,
      "lat": 44.7224
    },
    "Dexter": {
      "lng": -122.8055,
      "lat": 43.9242
    },
    "Dillard Complex": {
      "lng": -123.4156,
      "lat": 43.0895
    },
    "Dorena Hydro-Electric Facility": {
      "lng": -122.958056,
      "lat": 43.786667
    },
    "Douglas County Forest Products": {
      "lng": -123.361638,
      "lat": 43.2898
    },
    "Drift Creek Solar": {
      "lng": -122.803509,
      "lat": 44.970572
    },
    "Dry Creek LFG to Energy Project": {
      "lng": -122.7718,
      "lat": 42.3844
    },
    "Dunn Road Solar": {
      "lng": -122.286,
      "lat": 45.431
    },
    "Durbin Creek Windfarm (Burnt River)": {
      "lng": -117.323056,
      "lat": 44.353056
    },
    "Durham AWTF": {
      "lng": -122.765822,
      "lat": 45.400524
    },
    "Duus Solar, LLC": {
      "lng": -122.35,
      "lat": 45.35
    },
    "Eagle Point": {
      "lng": -122.757062,
      "lat": 42.514575
    },
    "Eagle Point Solar": {
      "lng": -122.82917,
      "lat": 42.400814
    },
    "East Side": {
      "lng": -121.795496,
      "lat": 42.224847
    },
    "Elbe Solar Center": {
      "lng": -121.205,
      "lat": 44.618
    },
    "Elkhorn Valley Wind Farm": {
      "lng": -117.816273,
      "lat": 45.085221
    },
    "Eurus Combine Hills Turbine Ranch 2": {
      "lng": -118.591,
      "lat": 45.943
    },
    "Evergreen BioPower LLC": {
      "lng": -122.611974,
      "lat": 44.770792
    },
    "Falls Creek": {
      "lng": -122.350246,
      "lat": 44.396815
    },
    "Faraday": {
      "lng": -122.32009,
      "lat": 45.268045
    },
    "Finley Buttes Landfill Gas": {
      "lng": -119.61,
      "lat": 45.69
    },
    "Firwood Solar, LLC": {
      "lng": -122.25,
      "lat": 45.35
    },
    "Fish Creek": {
      "lng": -122.448978,
      "lat": 43.274221
    },
    "Fort Rock I": {
      "lng": -121.003496,
      "lat": 43.320081
    },
    "Fort Rock IV": {
      "lng": -120.956087,
      "lat": 43.38988
    },
    "Foster": {
      "lng": -122.6712,
      "lat": 44.4146
    },
    "Four Corners Windfarm LLC": {
      "lng": -119.4181,
      "lat": 45.7286
    },
    "Four Mile Canyon Windfarm LLC": {
      "lng": -119.4533,
      "lat": 45.6447
    },
    "FPL Energy Vansycle LLC (OR)": {
      "lng": -118.816246,
      "lat": 46.01279
    },
    "Galesville Project": {
      "lng": -123.1778,
      "lat": 42.8489
    },
    "Garrett Solar": {
      "lng": -120.4,
      "lat": 42.16
    },
    "Georgia-Pacific Toledo Mill": {
      "lng": -123.931944,
      "lat": 44.611944
    },
    "Georgia-Pacific Wauna Mill": {
      "lng": -123.40658,
      "lat": 46.153955
    },
    "Grand Ronde Solar": {
      "lng": -123.61,
      "lat": 45.054
    },
    "Green Peter": {
      "lng": -122.5494,
      "lat": 44.4494
    },
    "Green Springs": {
      "lng": -122.547896,
      "lat": 42.121168
    },
    "GreenparkSolar": {
      "lng": -122.937451,
      "lat": 44.879414
    },
    "Grove Solar Center, LLC": {
      "lng": -117.381214,
      "lat": 43.936687
    },
    "Hay Canyon Wind Power LLC": {
      "lng": -120.5744,
      "lat": 45.52
    },
    "Hells Canyon": {
      "lng": -116.7008,
      "lat": 45.2439
    },
    "Hermiston": {
      "lng": -119.37,
      "lat": 45.8042
    },
    "Hermiston Power Plant": {
      "lng": -119.313323,
      "lat": 45.794046
    },
    "Hills Creek": {
      "lng": -122.4238,
      "lat": 43.7107
    },
    "Horseshoe Bend Wind LLC": {
      "lng": -120.008249,
      "lat": 45.58555
    },
    "Hyline Solar Center, LLC": {
      "lng": -117.000071,
      "lat": 44.151256
    },
    "Interstate Solar": {
      "lng": -122.899,
      "lat": 45.129
    },
    "IP Springfield Oregon": {
      "lng": -122.9555,
      "lat": 44.0569
    },
    "Jett Creek Windfarm (Burnt River)": {
      "lng": -117.273889,
      "lat": 44.425
    },
    "John C Boyle": {
      "lng": -122.0703,
      "lat": 42.093611
    },
    "John Day": {
      "lng": -120.69408,
      "lat": 45.71644
    },
    "Juniper Ridge Hydroelectric Project": {
      "lng": -121.269747,
      "lat": 44.142698
    },
    "Kale Patch Solar": {
      "lng": -122.904,
      "lat": 45.083
    },
    "Klamath Cogeneration Project": {
      "lng": -121.810556,
      "lat": 42.173889
    },
    "Klamath Falls Solar 2 CSG": {
      "lng": -121.764,
      "lat": 42.183
    },
    "Klamath Generation Peakers": {
      "lng": -121.814393,
      "lat": 42.172718
    },
    "Klondike Wind Power": {
      "lng": -120.5507,
      "lat": 45.5563
    },
    "Klondike Windpower II": {
      "lng": -120.593,
      "lat": 45.571
    },
    "Klondike Windpower III": {
      "lng": -120.5042,
      "lat": 45.5836
    },
    "Labish Solar LLC": {
      "lng": -122.922,
      "lat": 45.023
    },
    "Lacomb Irrigation District": {
      "lng": -122.685441,
      "lat": 44.597518
    },
    "Leaburg": {
      "lng": -122.6892,
      "lat": 44.1011
    },
    "Leaning Juniper": {
      "lng": -120.209783,
      "lat": 45.652457
    },
    "Leaning Juniper Wind Power II": {
      "lng": -120.2598,
      "lat": 45.651
    },
    "Lemolo 1": {
      "lng": -122.2494,
      "lat": 43.3558
    },
    "Lemolo 2": {
      "lng": -122.40248,
      "lat": 43.280611
    },
    "Lime Wind": {
      "lng": -117.271944,
      "lat": 44.396944
    },
    "Lookout Point": {
      "lng": -122.7531,
      "lat": 43.9153
    },
    "Lost Creek": {
      "lng": -122.6772,
      "lat": 42.6717
    },
    "McNary": {
      "lng": -119.2988,
      "lat": 45.9402
    },
    "Medford Operation": {
      "lng": -122.904642,
      "lat": 42.355517
    },
    "Michell Butte Power Project": {
      "lng": -117.155023,
      "lat": 43.771896
    },
    "Middle Fork Irrigation District": {
      "lng": -121.59984,
      "lat": 45.480701
    },
    "MilfordSolar OR": {
      "lng": -123.05,
      "lat": 45.57
    },
    "Mill Creek Solar (OR)": {
      "lng": -123.13633,
      "lat": 45.16964
    },
    "Millican Solar Energy LLC": {
      "lng": -120.53409,
      "lat": 44.154107
    },
    "Minke Solar": {
      "lng": -122.622665,
      "lat": 45.172332
    },
    "Montague Wind Power Facility LLC": {
      "lng": -120.0904,
      "lat": 45.5857
    },
    "Morgan Solar Center": {
      "lng": -117.07478,
      "lat": 43.951376
    },
    "Mt Hope Solar": {
      "lng": -122.61382,
      "lat": 45.12795
    },
    "Neal Hot Springs Geothermal Project": {
      "lng": -117.46806,
      "lat": 44.023056
    },
    "North Fork": {
      "lng": -122.27995,
      "lat": 45.243287
    },
    "North Hurlburt Wind LLC": {
      "lng": -120.083466,
      "lat": 45.731611
    },
    "NorWest Energy 2, LLC": {
      "lng": -121.22838,
      "lat": 44.06803
    },
    "NorWest Energy 4, LLC": {
      "lng": -121.614,
      "lat": 42.695
    },
    "NorWest Energy 9 LLC": {
      "lng": -119.343,
      "lat": 45.917
    },
    "Oak Grove": {
      "lng": -96.4866,
      "lat": 31.1803
    },
    "Old Mill Solar": {
      "lng": -121.046389,
      "lat": 42.203333
    },
    "O'Neill Creek Solar": {
      "lng": -123.129,
      "lat": 45.472
    },
    "Ontario Solar Center": {
      "lng": -117.028093,
      "lat": 44.112841
    },
    "Opal Springs Hydro": {
      "lng": -121.298812,
      "lat": 44.49004
    },
    "Open Range Solar Center, LLC": {
      "lng": -117.065311,
      "lat": 43.796766
    },
    "OR Solar 2, LLC": {
      "lng": -122.781338,
      "lat": 42.542084
    },
    "OR Solar 3, LLC": {
      "lng": -121.418873,
      "lat": 42.022336
    },
    "OR Solar 5, LLC": {
      "lng": -121.611565,
      "lat": 42.030332
    },
    "OR Solar 6, LLC": {
      "lng": -120.367807,
      "lat": 42.208988
    },
    "OR Solar 8, LLC": {
      "lng": -121.585833,
      "lat": 42.238888
    },
    "Orchard Windfarm 1, LLC": {
      "lng": -119.3332,
      "lat": 45.38
    },
    "Orchard Windfarm 2, LLC": {
      "lng": -119.3332,
      "lat": 45.38
    },
    "Orchard Windfarm 3, LLC": {
      "lng": -119.3332,
      "lat": 45.38
    },
    "Orchard Windfarm 4, LLC": {
      "lng": -119.33322,
      "lat": 45.38
    },
    "Oregon Convention Center": {
      "lng": -122.66317,
      "lat": 45.528457
    },
    "Oregon State University Energy Center": {
      "lng": -123.289089,
      "lat": 44.564333
    },
    "Oregon Trail Windfarm LLC": {
      "lng": -119.4044,
      "lat": 45.7122
    },
    "Oregon University System OIT Klamath Falls": {
      "lng": -121.785,
      "lat": 42.254167
    },
    "Oregon University System Rabbit Field": {
      "lng": -123.31695,
      "lat": 44.573484
    },
    "OSLH, LLC": {
      "lng": -121.228,
      "lat": 44.061
    },
    "Outback Solar At Christmas Valley": {
      "lng": -120.49,
      "lat": 43.236944
    },
    "Owyhee Dam Power Project": {
      "lng": -117.24374,
      "lat": 43.641603
    },
    "Oxbow (OR)": {
      "lng": -116.835042,
      "lat": 44.971643
    },
    "Pacific Canyon Windfarm LLC": {
      "lng": -119.4869,
      "lat": 45.6542
    },
    "Paisley Geothermal Generating Plant": {
      "lng": -120.557778,
      "lat": 42.695833
    },
    "Palmer Creek Solar, LLC": {
      "lng": -123.10175,
      "lat": 45.18484
    },
    "PaTu Wind Farm LLC": {
      "lng": -120.616906,
      "lat": 45.613061
    },
    "Pebble Springs Wind LLC": {
      "lng": -120.125,
      "lat": 45.7119
    },
    "Pelton": {
      "lng": -121.231379,
      "lat": 44.694374
    },
    "Peters Drive Plant": {
      "lng": -121.548455,
      "lat": 45.66764
    },
    "PHP 1": {
      "lng": -122.083188,
      "lat": 45.481708
    },
    "PHP 2": {
      "lng": -122.151257,
      "lat": 45.448977
    },
    "Pika Solar": {
      "lng": -122.7705,
      "lat": 45.089244
    },
    "Port Westward": {
      "lng": -123.172039,
      "lat": 46.17894
    },
    "Port Westward Unit 2": {
      "lng": -123.171944,
      "lat": 46.178889
    },
    "POTB Digester": {
      "lng": -123.804167,
      "lat": 45.414444
    },
    "Prineville Solar Energy LLC": {
      "lng": -120.524077,
      "lat": 44.163296
    },
    "Prospect 1": {
      "lng": -122.515122,
      "lat": 42.730717
    },
    "Prospect 2": {
      "lng": -122.513974,
      "lat": 42.731237
    },
    "Prospect 3": {
      "lng": -122.41846,
      "lat": 42.730606
    },
    "Prospect 4": {
      "lng": -122.515079,
      "lat": 42.733073
    },
    "Prospector Windfarm (Burnt River)": {
      "lng": -117.255,
      "lat": 44.418056
    },
    "Rafael Solar": {
      "lng": -123.007913,
      "lat": 44.859838
    },
    "Railroad Solar Center, LLC": {
      "lng": -117.10118,
      "lat": 43.998019
    },
    "Red Prairie Solar": {
      "lng": -123.417361,
      "lat": 45.087389
    },
    "Riley": {
      "lng": -119.6852,
      "lat": 43.5559
    },
    "River Mill": {
      "lng": -122.350023,
      "lat": 45.299991
    },
    "Riverbend Landfill": {
      "lng": -123.2511,
      "lat": 45.1672
    },
    "Rock Garden Solar, LLC": {
      "lng": -120.93038,
      "lat": 43.39691
    },
    "Roseburg LFG": {
      "lng": -123.380278,
      "lat": 43.186111
    },
    "Round Butte": {
      "lng": -121.277334,
      "lat": 44.605837
    },
    "Salem Smart Power Center": {
      "lng": -123.0183,
      "lat": 44.922
    },
    "Sand Ranch Windfarm LLC": {
      "lng": -119.4767,
      "lat": 45.6447
    },
    "Seneca Sustainable Energy LLC": {
      "lng": -123.179007,
      "lat": 44.116306
    },
    "Sheep Solar": {
      "lng": -122.949,
      "lat": 44.826
    },
    "Shell New Energies, Junction City": {
      "lng": -123.200454,
      "lat": 44.183526
    },
    "Short Mountain": {
      "lng": -122.997361,
      "lat": 43.96211
    },
    "Silverton Solar": {
      "lng": -122.807,
      "lat": 45.014
    },
    "Siphon Power Project": {
      "lng": -121.3297,
      "lat": 44.033022
    },
    "Slide Creek": {
      "lng": -122.4728,
      "lat": 43.2939
    },
    "Soda Springs": {
      "lng": -122.5014,
      "lat": 43.3061
    },
    "Solar Star Oregon II": {
      "lng": -120.918,
      "lat": 44.1843
    },
    "South Hurlburt Wind LLC": {
      "lng": -120.079987,
      "lat": 45.706262
    },
    "Sprague Hydro": {
      "lng": -120.98944,
      "lat": 42.50136
    },
    "SSD Clackamas 1, LLC": {
      "lng": -122.333,
      "lat": 45.42
    },
    "SSD Clackamas 4, LLC": {
      "lng": -122.662,
      "lat": 45.102
    },
    "SSD Clackamas 7, LLC": {
      "lng": -122.591,
      "lat": 45.182
    },
    "SSD Marion 1, LLC": {
      "lng": -122.904499,
      "lat": 45.139507
    },
    "SSD Marion 3, LLC": {
      "lng": -122.938,
      "lat": 44.985
    },
    "SSD Marion 5, LLC": {
      "lng": -122.793791,
      "lat": 45.11601
    },
    "SSD Marion 6, LLC": {
      "lng": -122.806,
      "lat": 45.254
    },
    "St Louis Solar": {
      "lng": -122.9361,
      "lat": 45.11994
    },
    "Star Point Wind Project LLC": {
      "lng": -120.48,
      "lat": 45.51
    },
    "Starvation": {
      "lng": -119.14852,
      "lat": 43.50222
    },
    "Steel Bridge Solar, LLC": {
      "lng": -123.47,
      "lat": 45.068
    },
    "Stone Creek": {
      "lng": -121.886611,
      "lat": 45.081111
    },
    "Sullivan": {
      "lng": -122.618954,
      "lat": 45.354004
    },
    "SulusSolar17": {
      "lng": -122.433515,
      "lat": 45.34891
    },
    "SulusSolar22": {
      "lng": -122.78,
      "lat": 44.97
    },
    "SulusSolar25": {
      "lng": -122.82,
      "lat": 45.27
    },
    "SulusSolar28": {
      "lng": -123.52,
      "lat": 45.1
    },
    "SulusSolar29": {
      "lng": -122.29,
      "lat": 45.46
    },
    "SulusSolar33": {
      "lng": -123.4,
      "lat": 45.08
    },
    "SulusSolar35": {
      "lng": -122.649655,
      "lat": 45.140551
    },
    "Suntex Solar, LLC": {
      "lng": -119.68621,
      "lat": 43.56149
    },
    "The Dalles": {
      "lng": -121.1346,
      "lat": 45.614
    },
    "Thomas Creek Solar": {
      "lng": -122.619475,
      "lat": 45.131407
    },
    "Threemile Canyon": {
      "lng": -119.9375,
      "lat": 45.6758
    },
    "Threemile Digester": {
      "lng": -119.901111,
      "lat": 45.71
    },
    "Thunderegg Solar Center, LLC": {
      "lng": -116.987626,
      "lat": 43.93097
    },
    "Tickle Creek Solar": {
      "lng": -122.265,
      "lat": 45.375
    },
    "Timothy Lake Powerhouse": {
      "lng": -121.756527,
      "lat": 45.370546
    },
    "Toketee Falls": {
      "lng": -122.447498,
      "lat": 43.272729
    },
    "Tumbleweed Solar, LLC": {
      "lng": -121.233106,
      "lat": 44.192575
    },
    "Tunnel 1 Power Project": {
      "lng": -117.232674,
      "lat": 43.642023
    },
    "Univ of Oregon Central Power Station": {
      "lng": -123.073889,
      "lat": 44.048333
    },
    "Vale Air Solar Center, LLC": {
      "lng": -117.25806,
      "lat": 43.965031
    },
    "Vale Solar Center": {
      "lng": -117.44434,
      "lat": 44.157874
    },
    "Valley Creek Solar": {
      "lng": -123.094,
      "lat": 45.033
    },
    "Vansycle": {
      "lng": -118.65999,
      "lat": 45.936157
    },
    "Vansycle II Wind Energy Center": {
      "lng": -118.591389,
      "lat": 45.899167
    },
    "Wagon Trail LLC": {
      "lng": -119.4625,
      "lat": 45.6531
    },
    "Wallowa Falls": {
      "lng": -117.212732,
      "lat": 45.266702
    },
    "Walterville": {
      "lng": -122.835046,
      "lat": 44.069636
    },
    "Ward Butte Windfarm LLC": {
      "lng": -119.4097,
      "lat": 45.6967
    },
    "Warm Springs Power Enterprises": {
      "lng": -121.247729,
      "lat": 44.724242
    },
    "Waste Management Columbia Ridge LFGTE": {
      "lng": -120.2969,
      "lat": 45.5739
    },
    "West Hines Solar I, LLC": {
      "lng": -119.14713,
      "lat": 43.50259
    },
    "Wheat Field Wind Power Project": {
      "lng": -120.31832,
      "lat": 45.675804
    },
    "Wheatridge 1": {
      "lng": -119.6338,
      "lat": 45.55975
    },
    "Wheatridge Hybrid": {
      "lng": -119.6338,
      "lat": 45.55975
    },
    "Williams Acres Solar": {
      "lng": -122.799,
      "lat": 45.137
    },
    "Willow Creek Energy Center": {
      "lng": -119.9914,
      "lat": 45.6533
    },
    "Willow Spring Windfarm (Burnt River)": {
      "lng": -117.273056,
      "lat": 44.381944
    },
    "WLWPCF Cogeneration Facility": {
      "lng": -123.05286,
      "lat": 45.00812
    },
    "Woodline Solar": {
      "lng": -121.57766,
      "lat": 42.236443
    },
    "WyEast Solar": {
      "lng": -120.59994,
      "lat": 45.5846
    },
    "Yamhill Solar LLC": {
      "lng": -123.278611,
      "lat": 45.108889
    },
    "500 Virginia Solar": {
      "lng": -75.191667,
      "lat": 40.139167
    },
    "Allegheny Hydro No 8": {
      "lng": -79.478982,
      "lat": 40.896596
    },
    "Allegheny Hydro No 9": {
      "lng": -79.550681,
      "lat": 40.955961
    },
    "Allegheny No 6 Hydro Station": {
      "lng": -79.577222,
      "lat": 40.716389
    },
    "Allegheny No. 5 Hydro Station": {
      "lng": -79.665298,
      "lat": 40.682875
    },
    "Allegheny Ridge Wind Farm": {
      "lng": -78.579444,
      "lat": 40.383889
    },
    "Allenwood": {
      "lng": -76.917777,
      "lat": 41.151111
    },
    "Alpaca": {
      "lng": -76.805556,
      "lat": 41.663056
    },
    "Aqua Ingrams Mill": {
      "lng": -75.6578,
      "lat": 39.9647
    },
    "Armenia Mountain Wind Farm": {
      "lng": -76.8553,
      "lat": 41.7619
    },
    "Armstrong Power, LLC": {
      "lng": -79.351725,
      "lat": 40.638021
    },
    "Beaver Solar LLC": {
      "lng": -80.323889,
      "lat": 40.691389
    },
    "Beaver Valley": {
      "lng": -80.4336,
      "lat": 40.6219
    },
    "Beaver Valley Patterson Dam": {
      "lng": -80.317839,
      "lat": 40.744105
    },
    "Bethlehem Power Plant": {
      "lng": -75.3147,
      "lat": 40.6175
    },
    "Big Level Wind": {
      "lng": -77.665939,
      "lat": 41.869326
    },
    "Birdsboro Power": {
      "lng": -75.799669,
      "lat": 40.268522
    },
    "Blossburg": {
      "lng": -77.08184,
      "lat": 41.706475
    },
    "Blue Ridge Landfill": {
      "lng": -77.561111,
      "lat": 39.961944
    },
    "Broad Mountain": {
      "lng": -76.380716,
      "lat": 40.669608
    },
    "Brunner Island, LLC": {
      "lng": -76.6962,
      "lat": 40.096111
    },
    "Brunot Island Power Station": {
      "lng": -80.043761,
      "lat": 40.464851
    },
    "Bucknell University": {
      "lng": -76.878843,
      "lat": 40.955015
    },
    "Cambria Wind LLC": {
      "lng": -78.6908,
      "lat": 40.3042
    },
    "CASD Solar": {
      "lng": -77.2061,
      "lat": 40.2142
    },
    "Casselman Wind Power Project": {
      "lng": -79.138,
      "lat": 39.85
    },
    "Chambersburg Units 12 & 13": {
      "lng": -77.685833,
      "lat": 39.866944
    },
    "Chester Generating Station": {
      "lng": -75.3837,
      "lat": 39.8301
    },
    "Chester Operations": {
      "lng": -75.358801,
      "lat": 39.843743
    },
    "Chestnut Flats Wind Farm": {
      "lng": -78.476921,
      "lat": 40.522846
    },
    "Cheswick": {
      "lng": -79.7906,
      "lat": 40.5383
    },
    "Colver Green Energy": {
      "lng": -78.8,
      "lat": 40.55
    },
    "Conemaugh": {
      "lng": -79.0611,
      "lat": 40.3842
    },
    "Conemaugh Hydro Plant": {
      "lng": -79.365703,
      "lat": 40.46439
    },
    "Covanta Delaware Valley": {
      "lng": -75.3882,
      "lat": 39.8265
    },
    "Covanta Plymouth Renewable Energy": {
      "lng": -75.3103,
      "lat": 40.0967
    },
    "CPV Fairview, LLC": {
      "lng": -78.855,
      "lat": 40.412
    },
    "Crayola Solar Project": {
      "lng": -75.238452,
      "lat": 40.760545
    },
    "Croydon Generating Station": {
      "lng": -74.8917,
      "lat": 40.08
    },
    "Cumberland County PA LFG Recovery": {
      "lng": -77.5072,
      "lat": 40.1356
    },
    "Dart Container Corp": {
      "lng": -76.17603,
      "lat": 40.086463
    },
    "Delaware Generating Station": {
      "lng": -75.125203,
      "lat": 39.968021
    },
    "Dickinson Solar": {
      "lng": -77.2197,
      "lat": 40.1976
    },
    "Domtar Paper Company, LLC": {
      "lng": -78.675759,
      "lat": 41.491466
    },
    "East Campus Steam Plant": {
      "lng": -77.8475,
      "lat": 40.809167
    },
    "Ebensburg Power Company": {
      "lng": -78.7472,
      "lat": 40.455
    },
    "ECP Uptown Campus": {
      "lng": -79.98741,
      "lat": 40.43727
    },
    "Eddystone Generating Station": {
      "lng": -75.323,
      "lat": 39.858
    },
    "Elizabethtown Solar": {
      "lng": -76.581401,
      "lat": 40.152693
    },
    "Elk Hill Solar 2": {
      "lng": -77.811561,
      "lat": 39.799589
    },
    "Fairless Energy Center": {
      "lng": -74.7411,
      "lat": 40.1475
    },
    "Falling Spring": {
      "lng": -77.6583,
      "lat": 39.9389
    },
    "Falls": {
      "lng": -74.789976,
      "lat": 40.176486
    },
    "Fayette Power Company LLC": {
      "lng": -79.9182,
      "lat": 39.8592
    },
    "Fort Indiantown Gap": {
      "lng": -76.57313,
      "lat": 40.427558
    },
    "Forward Windpower LLC": {
      "lng": -78.8633,
      "lat": 40.0853
    },
    "Frey Farm Landfill": {
      "lng": -76.4442,
      "lat": 39.9589
    },
    "Gans Generating Facility": {
      "lng": -79.838888,
      "lat": 39.7475
    },
    "General Electric Diesel Engine Plant": {
      "lng": -80.1055,
      "lat": 41.1694
    },
    "Gettysburg Energy & Nutrient Rec Facility": {
      "lng": -77.125278,
      "lat": 39.950278
    },
    "Gilberton Power Company": {
      "lng": -76.1983,
      "lat": 40.7903
    },
    "Glades Pike Generation Plant": {
      "lng": -79.041035,
      "lat": 40.00665
    },
    "GLRA Landfill": {
      "lng": -76.4931,
      "lat": 40.3664
    },
    "Grays Ferry Cogen Partnership": {
      "lng": -75.188069,
      "lat": 39.942219
    },
    "Green Knight Energy Center": {
      "lng": -75.2627,
      "lat": 40.8592
    },
    "Green Mountain Storage, LLC": {
      "lng": -79.070556,
      "lat": 39.851389
    },
    "GSK York RDC Solar Facility": {
      "lng": -76.735474,
      "lat": 40.043856
    },
    "Hamilton (PA)": {
      "lng": -76.988454,
      "lat": 39.908732
    },
    "Hamilton Liberty Generation Plant": {
      "lng": -76.39,
      "lat": 41.7675
    },
    "Hamilton Patriot Generation Plant": {
      "lng": -76.839167,
      "lat": 41.180833
    },
    "Handsome Lake Energy": {
      "lng": -79.8061,
      "lat": 41.2908
    },
    "Harrisburg Facility": {
      "lng": -76.853872,
      "lat": 40.244242
    },
    "Hazle Spindle": {
      "lng": -76.04739,
      "lat": 40.949536
    },
    "Hazleton Generation": {
      "lng": -76.040929,
      "lat": 40.928161
    },
    "Helix Ironwood LLC": {
      "lng": -76.3658,
      "lat": 40.3509
    },
    "Hickory Run Energy Station": {
      "lng": -80.43,
      "lat": 40.994444
    },
    "Highland North Wind Farm": {
      "lng": -78.680633,
      "lat": 40.319561
    },
    "Hill Top Energy Center": {
      "lng": -79.938201,
      "lat": 39.894212
    },
    "Holtwood": {
      "lng": -76.331772,
      "lat": 39.827198
    },
    "Homer City": {
      "lng": -79.196107,
      "lat": 40.512825
    },
    "Hummel Station": {
      "lng": -76.825517,
      "lat": 40.839014
    },
    "Hunker Solar River, LLC": {
      "lng": -79.647108,
      "lat": 40.216382
    },
    "Hunlock Creek Energy Center": {
      "lng": -76.07,
      "lat": 41.200556
    },
    "Hunlock Unit 4": {
      "lng": -76.068333,
      "lat": 41.2025
    },
    "Hunterstown": {
      "lng": -77.1648,
      "lat": 39.8662
    },
    "Hunterstown Combined Cycle": {
      "lng": -77.1672,
      "lat": 39.8725
    },
    "IKEA Conshohocken Rooftop PV System": {
      "lng": -75.306372,
      "lat": 40.095176
    },
    "Indiana University of Pennsylvania": {
      "lng": -79.1594,
      "lat": 40.6144
    },
    "Jefferson Torresdale Hospital": {
      "lng": -74.983057,
      "lat": 40.071128
    },
    "Juniata Locomotive Shop": {
      "lng": -78.384125,
      "lat": 40.533299
    },
    "Keystone": {
      "lng": -79.3411,
      "lat": 40.6604
    },
    "Keystone Solar": {
      "lng": -76.221111,
      "lat": 39.861111
    },
    "Knouse Solar Project 1": {
      "lng": -77.2344,
      "lat": 40.0264
    },
    "L&S Sweetners": {
      "lng": -76.145556,
      "lat": 40.090556
    },
    "Lackawanna Energy Center": {
      "lng": -75.544167,
      "lat": 41.471389
    },
    "Lake Lynn Hydro Station": {
      "lng": -79.856111,
      "lat": 39.720278
    },
    "Lakeview Gas Recovery": {
      "lng": -80.020765,
      "lat": 42.060172
    },
    "Lancaster County Resource Recovery": {
      "lng": -76.6436,
      "lat": 40.0711
    },
    "Lanchester Generating Station": {
      "lng": -75.9547,
      "lat": 40.1108
    },
    "Laurel Hill Wind": {
      "lng": -77.0296,
      "lat": 41.5323
    },
    "Liberty Electric Power Plant": {
      "lng": -75.3358,
      "lat": 39.8614
    },
    "Limerick": {
      "lng": -75.58744,
      "lat": 40.224303
    },
    "Lincoln Financial Field": {
      "lng": -75.1675,
      "lat": 39.900833
    },
    "Locust Ridge": {
      "lng": -76.1292,
      "lat": 40.855
    },
    "Locust Ridge II LLC": {
      "lng": -76.1547,
      "lat": 40.8411
    },
    "Longwood Gardens": {
      "lng": -75.667777,
      "lat": 39.866667
    },
    "Lookout Windpower LLC": {
      "lng": -78.9183,
      "lat": 39.8794
    },
    "Lower Mount Bethel Energy, LLC": {
      "lng": -75.107563,
      "lat": 40.801924
    },
    "Lycoming County": {
      "lng": -76.917777,
      "lat": 41.151111
    },
    "Mahoning Creek Hydroelectric Project": {
      "lng": -79.281667,
      "lat": 40.921111
    },
    "Marcus Hook Energy, LP": {
      "lng": -75.421606,
      "lat": 39.807038
    },
    "Marlboro Mushrooms Solar Field": {
      "lng": -75.829444,
      "lat": 39.882778
    },
    "Martin Limestone Solar Array": {
      "lng": -76.061667,
      "lat": 40.107778
    },
    "Martins Creek, LLC": {
      "lng": -75.105416,
      "lat": 40.797762
    },
    "Masser Farms Realty Solar": {
      "lng": -76.605417,
      "lat": 40.640556
    },
    "Mehoopany Wind Energy LLC": {
      "lng": -76.049167,
      "lat": 41.420556
    },
    "Merck - Upper Gwynedd Solar Array": {
      "lng": -75.28309,
      "lat": 40.215399
    },
    "Merck & Company - West Point": {
      "lng": -75.298823,
      "lat": 40.217963
    },
    "Meyersdale Battery": {
      "lng": -79.00205,
      "lat": 39.790739
    },
    "Meyersdale Windpower, LLC": {
      "lng": -79.002047,
      "lat": 39.790739
    },
    "MF Mesa Lane LLC": {
      "lng": -74.7772,
      "lat": 40.1483
    },
    "Mid-River PA, LLC (ABE4)": {
      "lng": -75.27774,
      "lat": 40.73998
    },
    "Milan": {
      "lng": -76.582778,
      "lat": 41.894444
    },
    "Mill Run Windpower LLC": {
      "lng": -79.393644,
      "lat": 39.913408
    },
    "Montour, LLC": {
      "lng": -76.6672,
      "lat": 41.0714
    },
    "Morgantown Solar Park": {
      "lng": -75.865916,
      "lat": 40.152069
    },
    "Moser Generating Station": {
      "lng": -75.6193,
      "lat": 40.2352
    },
    "Mount Joy Wire": {
      "lng": -76.476728,
      "lat": 40.10846
    },
    "Mountain": {
      "lng": -77.1716,
      "lat": 40.12282
    },
    "Mountain View": {
      "lng": -77.7883,
      "lat": 39.8014
    },
    "Moxie Freedom Generation Plant": {
      "lng": -76.158056,
      "lat": 41.113056
    },
    "Mt. Carmel Cogeneration": {
      "lng": -76.452951,
      "lat": 40.811189
    },
    "Muddy Run": {
      "lng": -76.2993,
      "lat": 39.8076
    },
    "Navy Yard Peaker Station": {
      "lng": -75.185539,
      "lat": 39.890223
    },
    "New Castle": {
      "lng": -80.369007,
      "lat": 40.937939
    },
    "Newman (PA)": {
      "lng": -75.05283,
      "lat": 40.013993
    },
    "North Allegheny Windpower Project": {
      "lng": -78.5436,
      "lat": 40.4381
    },
    "North Wales Diesel": {
      "lng": -75.316396,
      "lat": 40.203333
    },
    "Northampton Generating Plant": {
      "lng": -75.4792,
      "lat": 40.6917
    },
    "Norther Tier Landfill": {
      "lng": -76.6258,
      "lat": 41.7792
    },
    "Ontelaunee Energy Center": {
      "lng": -75.9356,
      "lat": 40.4219
    },
    "Orchard Park": {
      "lng": -77.6667,
      "lat": 39.9109
    },
    "Orrtanna": {
      "lng": -77.3508,
      "lat": 39.8442
    },
    "Oxbow Creek": {
      "lng": -75.870556,
      "lat": 41.608611
    },
    "PA Solar Park": {
      "lng": -75.852206,
      "lat": 40.860142
    },
    "PA Solar Park II": {
      "lng": -75.84656,
      "lat": 40.86159
    },
    "Panther Creek Energy Facility": {
      "lng": -75.8781,
      "lat": 40.8556
    },
    "Patton Wind Farm": {
      "lng": -78.696078,
      "lat": 40.63067
    },
    "Paxton Creek Cogeneration": {
      "lng": -76.8771,
      "lat": 40.2653
    },
    "Peach Bottom": {
      "lng": -76.268742,
      "lat": 39.758936
    },
    "PEI Power Corporation": {
      "lng": -75.5406,
      "lat": 41.485
    },
    "Pennsbury": {
      "lng": -74.769146,
      "lat": 40.153128
    },
    "PG Solar 1": {
      "lng": -80.253073,
      "lat": 40.481959
    },
    "Phoenix Contact - CCHP Plant": {
      "lng": -76.75007,
      "lat": 40.229379
    },
    "Pickering Solar": {
      "lng": -75.488056,
      "lat": 40.120556
    },
    "Pine Grove": {
      "lng": -76.3886,
      "lat": 40.5553
    },
    "Piney": {
      "lng": -79.433501,
      "lat": 41.192088
    },
    "Pioneer Crossing Energy, LLC": {
      "lng": -75.8167,
      "lat": 40.275
    },
    "Pittsburgh Airport Gas Plant": {
      "lng": -80.233,
      "lat": 40.495
    },
    "Pixelle Specialty Solutions": {
      "lng": -76.868117,
      "lat": 39.870935
    },
    "Pocono Solar Project": {
      "lng": -75.514924,
      "lat": 41.061022
    },
    "Portland": {
      "lng": -75.079398,
      "lat": 40.910205
    },
    "PPG Monroeville Chemicals Center": {
      "lng": -79.731111,
      "lat": 40.442222
    },
    "PPL Frey Farm Landfill Wind": {
      "lng": -76.455555,
      "lat": 39.959167
    },
    "PPL Glendon LFGTE Plant": {
      "lng": -75.232222,
      "lat": 40.667778
    },
    "Procter & Gamble Paper Products": {
      "lng": -76.043233,
      "lat": 41.574259
    },
    "PWD Northeast WPCP Biogas Cogen Plant": {
      "lng": -75.083333,
      "lat": 39.988611
    },
    "Rausch Creek Generation, LLC": {
      "lng": -76.45,
      "lat": 40.6191
    },
    "Richmond": {
      "lng": -75.075247,
      "lat": 39.984469
    },
    "Ringer Hill Wind Farm, LLC": {
      "lng": -79.188056,
      "lat": 39.744722
    },
    "Romark PA Solar": {
      "lng": -76.038888,
      "lat": 40.936111
    },
    "Roundtop": {
      "lng": -76.049167,
      "lat": 41.657778
    },
    "Safe Harbor": {
      "lng": -76.39,
      "lat": 39.9244
    },
    "Sandy Ridge Wind Farm": {
      "lng": -78.28735,
      "lat": 40.729207
    },
    "Schuylkill Generating Station": {
      "lng": -75.1883,
      "lat": 39.9419
    },
    "Scrubgrass Generating Plant": {
      "lng": -79.8134,
      "lat": 41.2691
    },
    "SECCRA Community Landfill": {
      "lng": -75.8428,
      "lat": 39.8639
    },
    "Seneca Generation LLC": {
      "lng": -79.0056,
      "lat": 41.8389
    },
    "Seward": {
      "lng": -79.03366,
      "lat": 40.40625
    },
    "Shawnee (PA)": {
      "lng": -75.058242,
      "lat": 41.061194
    },
    "Shawville": {
      "lng": -78.366228,
      "lat": 41.06755
    },
    "Shell Chemical Appalachia LLC": {
      "lng": -80.336389,
      "lat": 40.671667
    },
    "Somerset Windpower LLC": {
      "lng": -79.012778,
      "lat": 39.979167
    },
    "South Chestnut LLC": {
      "lng": -79.7271,
      "lat": 39.7513
    },
    "Southwark": {
      "lng": -75.135,
      "lat": 39.9144
    },
    "Spring House": {
      "lng": -75.234167,
      "lat": 40.183889
    },
    "Springdale Generating Station (55196)": {
      "lng": -79.768333,
      "lat": 40.546667
    },
    "Springdale Generating Station (55710)": {
      "lng": -79.768611,
      "lat": 40.545278
    },
    "St. Nicholas Cogeneration Project": {
      "lng": -76.1736,
      "lat": 40.8222
    },
    "Stony Creek Wind Farm LLC": {
      "lng": -78.8153,
      "lat": 40.0311
    },
    "Sunbury Generation LP": {
      "lng": -76.825,
      "lat": 40.8361
    },
    "Susquehanna University Solar": {
      "lng": -76.882525,
      "lat": 40.795166
    },
    "TalenEnergy Martin Creek LLC Allentown": {
      "lng": -75.45599,
      "lat": 40.608388
    },
    "TalenEnergy Martins Creek LLC Fishbach": {
      "lng": -76.219639,
      "lat": 40.689064
    },
    "TalenEnergy Martins Creek LLC Harrisburg": {
      "lng": -76.857897,
      "lat": 40.257865
    },
    "TalenEnergy Martins Creek LLC Harwood": {
      "lng": -76.0284,
      "lat": 40.9353
    },
    "TalenEnergy Martins Creek LLC Jenkins": {
      "lng": -75.816439,
      "lat": 41.282405
    },
    "TalenEnergy Martins Creek LLC Lock Haven": {
      "lng": -77.4709,
      "lat": 41.1189
    },
    "TalenEnergy Martins Creek LLC West Shore": {
      "lng": -76.94579,
      "lat": 40.1908
    },
    "TalenEnergy Martins Creek LLC Williamsport": {
      "lng": -77.00755,
      "lat": 41.2361
    },
    "TalenEnergy Susquehanna": {
      "lng": -76.1462,
      "lat": 41.0919
    },
    "Temple SEGF": {
      "lng": -75.150556,
      "lat": 39.98
    },
    "Temple Solar Arrays": {
      "lng": -75.925225,
      "lat": 40.423484
    },
    "Tenaska Westmoreland Generating Station": {
      "lng": -79.696665,
      "lat": 40.175249
    },
    "Titus": {
      "lng": -75.9081,
      "lat": 40.3056
    },
    "Tolna": {
      "lng": -76.635341,
      "lat": 39.760634
    },
    "Townsend Hydro": {
      "lng": -80.314795,
      "lat": 40.733545
    },
    "TPE Pennsylvania Solar 1, LLC": {
      "lng": -76.161594,
      "lat": 40.198529
    },
    "Trexlertown Solar Array North and South": {
      "lng": -75.593873,
      "lat": 40.556211
    },
    "Tullytown": {
      "lng": -74.780556,
      "lat": 40.151389
    },
    "Twin Ridges Wind Farm": {
      "lng": -78.874722,
      "lat": 39.760556
    },
    "University Park Solar": {
      "lng": -77.844117,
      "lat": 40.817019
    },
    "US Steel (Clairton Coke)": {
      "lng": -79.8819,
      "lat": 40.3097
    },
    "US Steel (Edgar Thomson)": {
      "lng": -79.8564,
      "lat": 40.3925
    },
    "Vitro Architectural Glass (PA)": {
      "lng": -77.164444,
      "lat": 40.126389
    },
    "Wallenpaupack": {
      "lng": -75.130895,
      "lat": 41.467858
    },
    "Warren": {
      "lng": -79.188668,
      "lat": 41.835947
    },
    "Warrior Ridge Hydro": {
      "lng": -78.034628,
      "lat": 40.540042
    },
    "Waste Management Arden LFGTE": {
      "lng": -80.2611,
      "lat": 40.2058
    },
    "Waymart Wind": {
      "lng": -75.4542,
      "lat": 41.5533
    },
    "West Campus Steam Plant": {
      "lng": -77.864722,
      "lat": 40.792778
    },
    "Wheelabrator Falls": {
      "lng": -74.768164,
      "lat": 40.162633
    },
    "Whitemarsh Central Utility Plant": {
      "lng": -75.2428,
      "lat": 40.0975
    },
    "Whitetail Solar 1": {
      "lng": -77.814218,
      "lat": 40.904211
    },
    "Whitetail Solar 2": {
      "lng": -77.814218,
      "lat": 39.904211
    },
    "Whitetail Solar 3": {
      "lng": -77.769144,
      "lat": 39.87817
    },
    "William F Matson Generating Station": {
      "lng": -78.002887,
      "lat": 40.432915
    },
    "Wind Park Bear Creek": {
      "lng": -75.753056,
      "lat": 41.235278
    },
    "Wolf Run Energy": {
      "lng": -76.299361,
      "lat": 41.610981
    },
    "York County Resource Recovery": {
      "lng": -76.7183,
      "lat": 40.0017
    },
    "York Energy Center": {
      "lng": -76.306666,
      "lat": 39.7375
    },
    "York Generation Company LLC": {
      "lng": -76.676158,
      "lat": 39.985641
    },
    "York Haven": {
      "lng": -76.71195,
      "lat": 40.113625
    },
    "Yough Hydro Power": {
      "lng": -79.368214,
      "lat": 39.801637
    },
    "AES ILUMINA": {
      "lng": -66.15887,
      "lat": 17.947524
    },
    "AES Puerto Rico": {
      "lng": -66.1499,
      "lat": 17.947115
    },
    "Aguirre Plant": {
      "lng": -66.234,
      "lat": 17.9492
    },
    "Arecibo Filtration Plant": {
      "lng": -66.6325,
      "lat": 18.426418
    },
    "Cambalache Plant": {
      "lng": -66.6969,
      "lat": 18.471
    },
    "Canovanas Filtration Plant": {
      "lng": -65.87931,
      "lat": 18.377243
    },
    "Caonillas": {
      "lng": -66.656,
      "lat": 18.277
    },
    "Central San Juan Plant": {
      "lng": -66.1045,
      "lat": 18.4271
    },
    "Cervecera de Puerto Rico": {
      "lng": -67.14164,
      "lat": 18.207153
    },
    "Costa Sur Plant": {
      "lng": -66.7597,
      "lat": 18.003
    },
    "Coto Laurel Solar Farm": {
      "lng": -66.55043,
      "lat": 18.059965
    },
    "Culebra": {
      "lng": -65.27956,
      "lat": 18.316713
    },
    "Daguao": {
      "lng": -65.6656,
      "lat": 18.2364
    },
    "Dos Bocas": {
      "lng": -66.667,
      "lat": 18.337
    },
    "Eaton Corp - Arecibo - Puerto Rico Hybrid": {
      "lng": -66.72205,
      "lat": 18.470243
    },
    "EcoElectrica": {
      "lng": -66.75542,
      "lat": 17.981025
    },
    "El Yunque Filtration Plant": {
      "lng": -65.81755,
      "lat": 18.37739
    },
    "Fajardo MSWL": {
      "lng": -65.68132,
      "lat": 18.293924
    },
    "Fort Buchanan Distributed Generation": {
      "lng": -66.11616,
      "lat": 18.408354
    },
    "Garzas": {
      "lng": -66.73,
      "lat": 18.0733
    },
    "GASNA 18P, LLC": {
      "lng": -66.09242,
      "lat": 18.453211
    },
    "HIMA San Pablo - Caguas": {
      "lng": -66.03833,
      "lat": 18.21805
    },
    "HIMA San Pablo Bayamon": {
      "lng": -66.14712,
      "lat": 18.39838
    },
    "HIMA San Pablo Cupey": {
      "lng": -66.04649,
      "lat": 18.372652
    },
    "HIMA San Pablo Fajardo": {
      "lng": -65.6547,
      "lat": 18.33643
    },
    "HIMA San Pablo Humacao": {
      "lng": -65.82669,
      "lat": 18.150795
    },
    "Holsum de Puerto Rico, Inc.": {
      "lng": -66.23506,
      "lat": 18.409197
    },
    "Horizon Energy (PR) Hybrid": {
      "lng": -66.22053,
      "lat": 17.979256
    },
    "Humacao Solar Project, LLC": {
      "lng": -65.82234,
      "lat": 18.134597
    },
    "Janssen Ortho LLC": {
      "lng": -65.9483,
      "lat": 18.244494
    },
    "Jobos": {
      "lng": -66.1398,
      "lat": 17.9602
    },
    "Martino": {
      "lng": -66.57895,
      "lat": 18.05695
    },
    "Mayaguez Plant": {
      "lng": -67.1592,
      "lat": 18.2186
    },
    "Medtronic Solar": {
      "lng": -65.91161,
      "lat": 18.236289
    },
    "Oriana Energy Hybrid": {
      "lng": -67.04641,
      "lat": 18.475032
    },
    "Palo Seco Plant": {
      "lng": -66.1484,
      "lat": 18.4559
    },
    "Patillas": {
      "lng": -66.019,
      "lat": 18.017
    },
    "Pattern Santa Isabel LLC": {
      "lng": -66.39315,
      "lat": 17.971696
    },
    "Pfizer Guayama": {
      "lng": -66.14796,
      "lat": 17.963472
    },
    "Pfizer Vega Baja": {
      "lng": -66.35,
      "lat": 18.450607
    },
    "Punta Lima Wind Farm": {
      "lng": -65.6952,
      "lat": 18.189872
    },
    "Rio Blanco": {
      "lng": -65.794,
      "lat": 18.268
    },
    "San Fermin Solar Farm Hybrid": {
      "lng": -65.9045,
      "lat": 18.4115
    },
    "Toa Baja MSWL": {
      "lng": -66.20186,
      "lat": 18.422147
    },
    "Toro Negro": {
      "lng": -66.488,
      "lat": 18.1317
    },
    "USCG Puerto Rico PV": {
      "lng": -67.13052,
      "lat": 18.498007
    },
    "Vega Baja": {
      "lng": -66.391,
      "lat": 18.446
    },
    "Vieques": {
      "lng": -65.43176,
      "lat": 18.152679
    },
    "Yabucoa": {
      "lng": -65.864,
      "lat": 18.047
    },
    "Yauco": {
      "lng": -66.871,
      "lat": 18.1105
    },
    "10 Briggs Solar NG, LLC (East)": {
      "lng": -71.49625,
      "lat": 41.63269
    },
    "A Street 1": {
      "lng": -71.546775,
      "lat": 41.797953
    },
    "A Street 2": {
      "lng": -71.545929,
      "lat": 41.797544
    },
    "Alton Road Solar": {
      "lng": -71.748328,
      "lat": 41.407547
    },
    "Blackhorse Farm Solar, LLC CSG": {
      "lng": -71.2342,
      "lat": 41.7186
    },
    "Blackstone/Tupperware": {
      "lng": -71.5444,
      "lat": 42.014467
    },
    "Block Island": {
      "lng": -71.5711,
      "lat": 41.175556
    },
    "Block Island Wind Farm": {
      "lng": -71.521111,
      "lat": 41.114722
    },
    "Brookside": {
      "lng": -71.583963,
      "lat": 41.987579
    },
    "CED Foster": {
      "lng": -71.787,
      "lat": 41.795
    },
    "Central Power Plant": {
      "lng": -71.458415,
      "lat": 41.744973
    },
    "Forbes Street Solar": {
      "lng": -71.338889,
      "lat": 41.773056
    },
    "Foster Solar": {
      "lng": -71.71459,
      "lat": 41.84242
    },
    "Founders Homestead Farms Solar": {
      "lng": -71.2782,
      "lat": 41.57677
    },
    "GD Glocester White Oak I, LLC": {
      "lng": -71.596239,
      "lat": 41.861544
    },
    "GD Hopkinton Main I, LLC": {
      "lng": -71.781092,
      "lat": 41.438293
    },
    "GD Johnson Scituate I, LLC": {
      "lng": -71.505379,
      "lat": 41.807129
    },
    "GD Richmond Buttonwoods I, LLC": {
      "lng": -71.684093,
      "lat": 41.522911
    },
    "GD West Greenwich Victory I, LLC": {
      "lng": -71.687478,
      "lat": 41.657866
    },
    "GDIM 1, LLC": {
      "lng": -71.525993,
      "lat": 41.966678
    },
    "GDIM 2, LLC": {
      "lng": -71.519606,
      "lat": 41.967314
    },
    "GDIM 3, LLC": {
      "lng": -71.516929,
      "lat": 41.963066
    },
    "GDIM 4, LLC": {
      "lng": -71.516929,
      "lat": 41.963066
    },
    "Gold Meadow Farms": {
      "lng": -71.528175,
      "lat": 41.734903
    },
    "Green Providence Wind I, LLC": {
      "lng": -71.384771,
      "lat": 41.789486
    },
    "Green Providence Wind II, LLC": {
      "lng": -71.385542,
      "lat": 41.786045
    },
    "Hope Farm Solar, LLC": {
      "lng": -71.5263,
      "lat": 41.7479
    },
    "Hopkins Hill CSG": {
      "lng": -71.566825,
      "lat": 41.61609
    },
    "Hopkinton Phase 2": {
      "lng": -71.791133,
      "lat": 41.443293
    },
    "ISM Solar Cranston CSG": {
      "lng": -71.46304,
      "lat": 41.735147
    },
    "Johnston LFG Turbine Plant": {
      "lng": -71.523333,
      "lat": 41.803333
    },
    "Johnston Solar": {
      "lng": -71.559472,
      "lat": 41.800906
    },
    "Kearsarge East Providence": {
      "lng": -71.369746,
      "lat": 41.833746
    },
    "Kearsarge Fogland": {
      "lng": -71.188221,
      "lat": 41.555955
    },
    "Kearsarge SKSC1 LLC": {
      "lng": -71.494452,
      "lat": 41.471571
    },
    "Kearsarge SKSC2 LLC": {
      "lng": -71.533414,
      "lat": 41.498281
    },
    "Kearsarge Westerly": {
      "lng": -71.732202,
      "lat": 41.385761
    },
    "Kilvert": {
      "lng": -71.449611,
      "lat": 41.727505
    },
    "Little Bay": {
      "lng": -71.436048,
      "lat": 41.799201
    },
    "Manchester Street Station": {
      "lng": -71.4042,
      "lat": 41.8167
    },
    "Nautilus Goat Island Solar LLC (CSG)": {
      "lng": -71.662038,
      "lat": 41.941786
    },
    "NBC Field's Point Wind Farm": {
      "lng": -71.38919,
      "lat": 41.79413
    },
    "North Providence": {
      "lng": -71.476493,
      "lat": 41.869826
    },
    "North Smithfield Solar Power 1": {
      "lng": -71.586,
      "lat": 41.9759
    },
    "Ocean State Power": {
      "lng": -71.668909,
      "lat": 42.009722
    },
    "Ocean State Power II": {
      "lng": -71.6701,
      "lat": 42.0099
    },
    "Orbit Energy RI": {
      "lng": -71.514166,
      "lat": 41.8054
    },
    "Pawtucket Power Associates, LP": {
      "lng": -71.406856,
      "lat": 41.861531
    },
    "Pine Hill": {
      "lng": -71.562498,
      "lat": 41.821004
    },
    "Plainfield Pike": {
      "lng": -71.553484,
      "lat": 41.793111
    },
    "Rhode Island Hospital": {
      "lng": -71.41,
      "lat": 41.8103
    },
    "Rhode Island State Energy Center": {
      "lng": -71.5186,
      "lat": 41.8017
    },
    "Richmond NMCA": {
      "lng": -71.715387,
      "lat": 41.440191
    },
    "Ridgewood Providence Power": {
      "lng": -71.53,
      "lat": 41.8069
    },
    "Roger Williams - Melville at Portsmouth": {
      "lng": -71.273083,
      "lat": 41.579508
    },
    "Smithfield Solar Farm, LLC": {
      "lng": -71.520368,
      "lat": 41.92237
    },
    "Thundermist Hydro": {
      "lng": -71.517434,
      "lat": 41.99897
    },
    "Tiverton Power, LLC": {
      "lng": -71.1706,
      "lat": 41.6422
    },
    "Toray Plastic America's CHP Plant": {
      "lng": -71.427609,
      "lat": 41.593802
    },
    "Town of Burrillville Solar CSG": {
      "lng": -71.661196,
      "lat": 41.954239
    },
    "TPE Hopkins Solar Holdings1, LLC": {
      "lng": -71.566825,
      "lat": 41.61609
    },
    "University Solar, LLC": {
      "lng": -71.6593,
      "lat": 41.6133
    },
    "WED Coventry 1": {
      "lng": -71.7075,
      "lat": 41.675833
    },
    "WED Coventry 2": {
      "lng": -71.705833,
      "lat": 41.6725
    },
    "WED Coventry 3": {
      "lng": -71.720833,
      "lat": 41.695556
    },
    "WED Coventry 4": {
      "lng": -71.728333,
      "lat": 41.686389
    },
    "WED Coventry 5": {
      "lng": -71.699722,
      "lat": 41.659722
    },
    "WED Coventry 6": {
      "lng": -71.708333,
      "lat": 41.669722
    },
    "WED Coventry Seven, LLC": {
      "lng": -71.722267,
      "lat": 41.697563
    },
    "WED Green Hill, LLC": {
      "lng": -71.529736,
      "lat": 41.79785
    },
    "WED GW Solar, LLC": {
      "lng": -71.487574,
      "lat": 41.937033
    },
    "WED Kingstown Solar I - East Array": {
      "lng": -71.619114,
      "lat": 41.492253
    },
    "WED Kingstown Solar I, LLC - West": {
      "lng": -71.624581,
      "lat": 41.49175
    },
    "WED NK Green": {
      "lng": -71.486667,
      "lat": 41.581667
    },
    "WED Plainfield II, LLC": {
      "lng": -71.539967,
      "lat": 41.794583
    },
    "WED Plainfield III, LLC": {
      "lng": -71.530622,
      "lat": 41.793519
    },
    "WED Plainfield, LLC": {
      "lng": -71.535825,
      "lat": 41.796078
    },
    "WED Portsmouth One, LLC": {
      "lng": -71.249262,
      "lat": 41.612027
    },
    "WED Shun I, LLC": {
      "lng": -71.540656,
      "lat": 41.798456
    },
    "WED Shun II, LLC": {
      "lng": -71.536189,
      "lat": 41.800111
    },
    "WED Shun III, LLC": {
      "lng": -71.549331,
      "lat": 41.799078
    },
    "WED Stilson Solar": {
      "lng": -71.686461,
      "lat": 41.518751
    },
    "West Davisville Solar": {
      "lng": -71.477222,
      "lat": 41.603611
    },
    "West Greenwich Solar": {
      "lng": -71.71,
      "lat": 41.649444
    },
    "1634 Solar": {
      "lng": -80.081493,
      "lat": 34.3368
    },
    "99 Islands": {
      "lng": -81.4936,
      "lat": 35.0314
    },
    "Abbot Solar": {
      "lng": -80.240292,
      "lat": 33.678358
    },
    "Ace Solar": {
      "lng": -82.525795,
      "lat": 34.722882
    },
    "Adams": {
      "lng": -82.528284,
      "lat": 34.695185
    },
    "AGA TAG Solar III LLC": {
      "lng": -82.0421,
      "lat": 34.99647
    },
    "Allendale Biomass": {
      "lng": -81.281944,
      "lat": 32.995
    },
    "Anderson (SC)": {
      "lng": -82.676137,
      "lat": 34.493707
    },
    "Anderson Regional Landfill": {
      "lng": -82.4621,
      "lat": 34.5739
    },
    "Atood II": {
      "lng": -81.808469,
      "lat": 35.030303
    },
    "ATOOD Solar III LLC": {
      "lng": -82.0421,
      "lat": 34.99647
    },
    "Augusta Solar": {
      "lng": -82.13,
      "lat": 34.124
    },
    "Bad Creek": {
      "lng": -82.9975,
      "lat": 35.0075
    },
    "Ballenger Road Solar A": {
      "lng": -82.112843,
      "lat": 35.04354
    },
    "Bani Solar": {
      "lng": -79.93713,
      "lat": 33.953775
    },
    "Barnwell Solar": {
      "lng": -81.351,
      "lat": 33.237
    },
    "Bell Bay Solar Farm": {
      "lng": -78.917988,
      "lat": 33.667675
    },
    "Berkeley County Landfill": {
      "lng": -80.029444,
      "lat": 33.121111
    },
    "Berry Road Solar": {
      "lng": -82.328691,
      "lat": 34.639249
    },
    "Blacktip Solar": {
      "lng": -79.755961,
      "lat": 33.900481
    },
    "Blackville Solar Farm, LLC": {
      "lng": -81.258933,
      "lat": 33.342619
    },
    "Blackville Solar II": {
      "lng": -81.261774,
      "lat": 33.344319
    },
    "Bloom Solar": {
      "lng": -80.336613,
      "lat": 33.610454
    },
    "Bluebird Solar SC LLC": {
      "lng": -82.739217,
      "lat": 34.599083
    },
    "BMW Manufacturing Co": {
      "lng": -82.178333,
      "lat": 34.894167
    },
    "Bond Solar": {
      "lng": -80.362997,
      "lat": 33.617189
    },
    "Bonefish Solar": {
      "lng": -80.190821,
      "lat": 33.720936
    },
    "Boyds Mill Hydro": {
      "lng": -82.199339,
      "lat": 34.455514
    },
    "Briarwood": {
      "lng": -82.10133,
      "lat": 34.08475
    },
    "Broad River Energy Center": {
      "lng": -81.575,
      "lat": 35.0786
    },
    "Bull Street Plant": {
      "lng": -80.870272,
      "lat": 33.489767
    },
    "Buzzard Roost": {
      "lng": -81.902452,
      "lat": 34.169733
    },
    "Camden South Carolina": {
      "lng": -80.6558,
      "lat": 34.2338
    },
    "Cameron Solar": {
      "lng": -80.707,
      "lat": 33.574
    },
    "Cameron Solar II": {
      "lng": -80.699,
      "lat": 33.567
    },
    "Cardinal Solar LLC": {
      "lng": -79.38585,
      "lat": 34.199824
    },
    "Catawba": {
      "lng": -81.0694,
      "lat": 35.0514
    },
    "Centerfield Cooper Solar, LLC": {
      "lng": -80.118838,
      "lat": 34.708762
    },
    "Central Energy Facility": {
      "lng": -82.840278,
      "lat": 34.679444
    },
    "Central Plant": {
      "lng": -82.366035,
      "lat": 34.871811
    },
    "Champion Solar": {
      "lng": -81.262417,
      "lat": 33.794732
    },
    "Cherokee County Cogen": {
      "lng": -81.613,
      "lat": 35.0727
    },
    "Cherokee Falls": {
      "lng": -81.5458,
      "lat": 35.0642
    },
    "Cherry Blossom Solar LLC": {
      "lng": -79.458012,
      "lat": 33.762185
    },
    "City West Diesel Plant": {
      "lng": -81.646944,
      "lat": 34.7225
    },
    "Clifton Dam 3 Power Station": {
      "lng": -81.8344,
      "lat": 34.9953
    },
    "Coit GT": {
      "lng": -81.04067,
      "lat": 33.980254
    },
    "Colleton Solar Farm": {
      "lng": -80.6925,
      "lat": 32.913333
    },
    "Columbia Canal Hydro": {
      "lng": -81.0494,
      "lat": 33.9972
    },
    "Columbia Energy Center (SC)": {
      "lng": -81.0178,
      "lat": 33.8697
    },
    "Cope Station": {
      "lng": -81.03,
      "lat": 33.3644
    },
    "Cross": {
      "lng": -80.113235,
      "lat": 33.371506
    },
    "Crown": {
      "lng": -80.719878,
      "lat": 34.648205
    },
    "Darlington County": {
      "lng": -80.1657,
      "lat": 34.4185
    },
    "Darlington Solar, LLC": {
      "lng": -79.917,
      "lat": 34.32
    },
    "Dearborn": {
      "lng": -80.8914,
      "lat": 34.5583
    },
    "Denmark Solar": {
      "lng": -81.142844,
      "lat": 33.347128
    },
    "Diamond Solar": {
      "lng": -81.273625,
      "lat": 33.786193
    },
    "Dorchester Biomass": {
      "lng": -80.450278,
      "lat": 33.238611
    },
    "Duke Energy CHP at Clemson University": {
      "lng": -82.8231,
      "lat": 34.6779
    },
    "Edison Solar": {
      "lng": -81.386156,
      "lat": 33.244191
    },
    "Enoree Phase II Landfill Gas Recovery": {
      "lng": -82.1847,
      "lat": 34.8008
    },
    "Estill Solar": {
      "lng": -81.242151,
      "lat": 32.763843
    },
    "Estill Solar II": {
      "lng": -81.253453,
      "lat": 32.727842
    },
    "Fairfield Pumped Storage": {
      "lng": -81.3308,
      "lat": 34.3061
    },
    "Fishing Creek": {
      "lng": -80.8928,
      "lat": 34.6
    },
    "Freedom Solar": {
      "lng": -79.148,
      "lat": 34.287
    },
    "Gaines Solar": {
      "lng": -80.218533,
      "lat": 34.192353
    },
    "Gary Solar": {
      "lng": -80.221678,
      "lat": 34.199094
    },
    "Gaston II": {
      "lng": -81.110251,
      "lat": 33.778378
    },
    "Gaston Shoals": {
      "lng": -81.5975,
      "lat": 35.1381
    },
    "Gaston Solar I - SC": {
      "lng": -81.110546,
      "lat": 33.774657
    },
    "Georgetown LFGTE": {
      "lng": -79.316111,
      "lat": 33.453611
    },
    "Gold Mine": {
      "lng": -82.2263,
      "lat": 34.0726
    },
    "Goldenrod Solar": {
      "lng": -79.562891,
      "lat": 33.962884
    },
    "Great Falls (SC)": {
      "lng": -80.8917,
      "lat": 34.5592
    },
    "H B Robinson": {
      "lng": -80.1589,
      "lat": 34.4017
    },
    "Hagood": {
      "lng": -79.9634,
      "lat": 32.8265
    },
    "Haley Solar": {
      "lng": -81.29337,
      "lat": 33.023402
    },
    "Hampton Solar 2": {
      "lng": -81.136,
      "lat": 32.881
    },
    "Hampton Solar I": {
      "lng": -81.133,
      "lat": 32.879
    },
    "Hardeeville": {
      "lng": -81.073866,
      "lat": 32.298805
    },
    "Hecate Energy West Newberry, LLC": {
      "lng": -81.648947,
      "lat": 34.249814
    },
    "Hilton Head Gas Turbine Site": {
      "lng": -80.698754,
      "lat": 32.208871
    },
    "Hollidays Bridge Hydro": {
      "lng": -82.374964,
      "lat": 34.527475
    },
    "Honea Path": {
      "lng": -82.3652,
      "lat": 34.44128
    },
    "Horry Land Fill Gas Site": {
      "lng": -78.967222,
      "lat": 33.8125
    },
    "Huntley": {
      "lng": -80.634174,
      "lat": 33.335583
    },
    "International Paper Georgetown Mill": {
      "lng": -79.302573,
      "lat": 33.360185
    },
    "International Paper-Eastover Mill": {
      "lng": -80.6397,
      "lat": 33.8872
    },
    "J Strom Thurmond": {
      "lng": -82.1961,
      "lat": 33.6602
    },
    "Jamison Solar Farm": {
      "lng": -80.837105,
      "lat": 33.57398
    },
    "Jasper County Generating Facility": {
      "lng": -81.1242,
      "lat": 32.3594
    },
    "Jefferies": {
      "lng": -79.990909,
      "lat": 33.244404
    },
    "Jessamine Solar": {
      "lng": -79.390606,
      "lat": 34.230544
    },
    "Jocassee": {
      "lng": -82.9147,
      "lat": 34.9594
    },
    "John S. Rainey Generating Station": {
      "lng": -82.774478,
      "lat": 34.347734
    },
    "Keowee": {
      "lng": -82.8872,
      "lat": 34.7981
    },
    "Lee County Landfill": {
      "lng": -80.267777,
      "lat": 34.181042
    },
    "Lily Solar": {
      "lng": -81.434842,
      "lat": 33.094931
    },
    "Limelight Solar II LLC": {
      "lng": -81.87819,
      "lat": 34.99389
    },
    "Lockhart": {
      "lng": -81.4561,
      "lat": 34.7792
    },
    "Marlboro Mill": {
      "lng": -79.785,
      "lat": 34.605
    },
    "McMeekin": {
      "lng": -81.2172,
      "lat": 34.0556
    },
    "MCRD Parris Island PV Hybrid": {
      "lng": -80.694722,
      "lat": 32.329402
    },
    "Midlands": {
      "lng": -80.942818,
      "lat": 33.648979
    },
    "Midway Green Solar": {
      "lng": -81.393,
      "lat": 34.421
    },
    "Mill Creek Combustion Turbine Station": {
      "lng": -81.4306,
      "lat": 35.1597
    },
    "Moffett Solar Project": {
      "lng": -80.995994,
      "lat": 32.620653
    },
    "Mohea Solar Energy Center, LLC": {
      "lng": -81.62438,
      "lat": 35.08849
    },
    "Myrtle Beach Gas Turbine Site": {
      "lng": -78.924146,
      "lat": 33.708283
    },
    "Neal Shoals": {
      "lng": -81.4486,
      "lat": 34.6642
    },
    "North Road Peak": {
      "lng": -80.9109,
      "lat": 33.5452
    },
    "Oconee": {
      "lng": -82.8986,
      "lat": 34.7939
    },
    "Odyssey Solar": {
      "lng": -81.266,
      "lat": 33.79
    },
    "Omtanke Solar": {
      "lng": -80.253716,
      "lat": 33.133705
    },
    "Otarre Solar Park": {
      "lng": -81.0514,
      "lat": 33.9324
    },
    "Otis Elevator Company Solar": {
      "lng": -79.79227,
      "lat": 34.23933
    },
    "Pacolet Diesel Generation Facility": {
      "lng": -81.7397,
      "lat": 34.8986
    },
    "Palmetto Plains": {
      "lng": -80.697336,
      "lat": 33.340179
    },
    "Parr GT": {
      "lng": -81.3308,
      "lat": 34.2642
    },
    "Parr Hydro": {
      "lng": -81.330852,
      "lat": 34.26132
    },
    "Pelzer Lower": {
      "lng": -82.447913,
      "lat": 34.616522
    },
    "Pelzer Solar I": {
      "lng": -82.501964,
      "lat": 34.657642
    },
    "Pelzer Upper": {
      "lng": -82.45444,
      "lat": 34.642371
    },
    "Peony Solar": {
      "lng": -81.254,
      "lat": 33.502
    },
    "Piedmont Hydro Power Project": {
      "lng": -82.462102,
      "lat": 34.702021
    },
    "Rankin Solar Center, LLC": {
      "lng": -79.655149,
      "lat": 34.23453
    },
    "Redwing Solar": {
      "lng": -79.887503,
      "lat": 34.274414
    },
    "Richardson Solar": {
      "lng": -81.35173,
      "lat": 33.27774
    },
    "Richland County Landfill": {
      "lng": -80.790494,
      "lat": 34.102996
    },
    "Ridgeland Solar Project": {
      "lng": -80.961,
      "lat": 32.465
    },
    "River Solar": {
      "lng": -80.128107,
      "lat": 34.107849
    },
    "Rocky River (SC)": {
      "lng": -82.6097,
      "lat": 34.2572
    },
    "Rowesville Rd Plant": {
      "lng": -80.847389,
      "lat": 33.436637
    },
    "Runway  Solar Farm": {
      "lng": -78.917988,
      "lat": 33.667675
    },
    "Saluda Dam": {
      "lng": -82.483959,
      "lat": 34.852224
    },
    "Saluda Lexington": {
      "lng": -81.2172,
      "lat": 34.0533
    },
    "Saluda Solar II": {
      "lng": -81.739,
      "lat": 34.011
    },
    "Saluda Solar, LLC": {
      "lng": -81.7829,
      "lat": 33.96839
    },
    "Sapphire Solar": {
      "lng": -79.783683,
      "lat": 33.887786
    },
    "Savannah River Site Biomass Cogen": {
      "lng": -81.666666,
      "lat": 33.283333
    },
    "SCE&G Curie CSG": {
      "lng": -81.106,
      "lat": 32.89
    },
    "SCE&G Nimitz CSG": {
      "lng": -80.956084,
      "lat": 32.497759
    },
    "SCE&G Springfield CSG": {
      "lng": -81.297,
      "lat": 33.491
    },
    "Seabrook Solar": {
      "lng": -80.742047,
      "lat": 32.56445
    },
    "Sediver": {
      "lng": -81.1872,
      "lat": 34.965278
    },
    "Seneca City of": {
      "lng": -82.9525,
      "lat": 34.6875
    },
    "Shaw Creek Solar, LLC": {
      "lng": -81.754444,
      "lat": 33.674689
    },
    "Sonoco Products Co": {
      "lng": -80.067778,
      "lat": 34.385278
    },
    "South Solar": {
      "lng": -79.606062,
      "lat": 33.995392
    },
    "Southern Current One, LLC": {
      "lng": -81.215942,
      "lat": 32.947262
    },
    "Spartanburg Water System": {
      "lng": -81.9675,
      "lat": 35.1103
    },
    "Spillway": {
      "lng": -80.163986,
      "lat": 33.453759
    },
    "St Stephen": {
      "lng": -79.93,
      "lat": 33.4261
    },
    "St. Matthews Solar": {
      "lng": -80.7586,
      "lat": 33.6867
    },
    "Substation 20 Plant": {
      "lng": -80.8417,
      "lat": 33.440183
    },
    "Swamp Fox Solar": {
      "lng": -81.256166,
      "lat": 33.791505
    },
    "Tedder Solar": {
      "lng": -79.924456,
      "lat": 34.301806
    },
    "Thermal Kem": {
      "lng": -81.0725,
      "lat": 34.8894
    },
    "Trask East Solar": {
      "lng": -80.748189,
      "lat": 32.470606
    },
    "TWE Bowman Solar Project": {
      "lng": -80.68244,
      "lat": 33.316159
    },
    "Union Renewable Energy Facility": {
      "lng": -81.818661,
      "lat": 34.61577
    },
    "Upper Pacolet Hydro": {
      "lng": -81.745556,
      "lat": 34.921389
    },
    "Urquhart": {
      "lng": -81.9111,
      "lat": 33.435
    },
    "V C Summer": {
      "lng": -81.3153,
      "lat": 34.2983
    },
    "Valenite": {
      "lng": -83.03832,
      "lat": 34.65724
    },
    "Vincent Solar": {
      "lng": -80.108313,
      "lat": 34.091583
    },
    "W S Lee": {
      "lng": -82.435,
      "lat": 34.6022
    },
    "Ware Shoals Hydro Project": {
      "lng": -82.233581,
      "lat": 34.404006
    },
    "Watauga Solar": {
      "lng": -79.844619,
      "lat": 34.523944
    },
    "Wateree": {
      "lng": -80.6228,
      "lat": 33.8264
    },
    "Wateree Hydro": {
      "lng": -80.7021,
      "lat": 34.3355
    },
    "Weaver Solar": {
      "lng": -80.078469,
      "lat": 34.069422
    },
    "Webb Forging": {
      "lng": -81.4919,
      "lat": 34.5567
    },
    "Wellford Renewable Energy Plant": {
      "lng": -82.130278,
      "lat": 35.001194
    },
    "WestRock Charleston Kraft, LLC": {
      "lng": -79.9667,
      "lat": 32.9
    },
    "WestRock CP, LLC Florence Mill": {
      "lng": -79.5606,
      "lat": 34.1497
    },
    "White Horse Solar A": {
      "lng": -82.398437,
      "lat": 34.786327
    },
    "Whitetail Solar": {
      "lng": -79.383611,
      "lat": 34.398056
    },
    "Whitt Solar": {
      "lng": -82.524315,
      "lat": 34.721613
    },
    "Williams": {
      "lng": -79.9297,
      "lat": 33.0158
    },
    "Willis Solar": {
      "lng": -79.564983,
      "lat": 33.984625
    },
    "Winyah": {
      "lng": -79.357236,
      "lat": 33.33184
    },
    "Wylie": {
      "lng": -81.0078,
      "lat": 35.0218
    },
    "Yemassee Solar": {
      "lng": -80.903275,
      "lat": 32.705139
    },
    "Aberdeen Generating Station": {
      "lng": -98.49396,
      "lat": 45.429884
    },
    "Angus Anson": {
      "lng": -96.6356,
      "lat": 43.60347
    },
    "Astoria Station": {
      "lng": -96.55942,
      "lat": 44.574965
    },
    "Aurora County Wind": {
      "lng": -98.752,
      "lat": 43.719
    },
    "Beethoven Wind": {
      "lng": -98.08,
      "lat": 43.16
    },
    "Ben French": {
      "lng": -103.260959,
      "lat": 44.087235
    },
    "Big Bend Dam": {
      "lng": -99.4463,
      "lat": 44.0384
    },
    "Big Stone": {
      "lng": -96.510067,
      "lat": 45.303652
    },
    "Brule County Wind": {
      "lng": -98.922,
      "lat": 43.717
    },
    "Buffalo Ridge I LLC": {
      "lng": -96.4779,
      "lat": 44.4328
    },
    "Buffalo Ridge II LLC": {
      "lng": -96.6267,
      "lat": 44.5281
    },
    "Campbell County Wind Farm": {
      "lng": -100.275,
      "lat": 45.753333
    },
    "Chamberlain Wind Project": {
      "lng": -99.24757,
      "lat": 43.848529
    },
    "Clark (SD)": {
      "lng": -97.732102,
      "lat": 44.876484
    },
    "Coyote Ridge": {
      "lng": -96.55,
      "lat": 44.45
    },
    "Crocker Wind Farm": {
      "lng": -97.826423,
      "lat": 45.051544
    },
    "Crowned Ridge Wind Energy Center": {
      "lng": -96.836839,
      "lat": 45.154947
    },
    "Crowned Ridge Wind II Energy Center": {
      "lng": -96.929945,
      "lat": 45.0054
    },
    "Dakota Range III Wind Project": {
      "lng": -97.103982,
      "lat": 45.267621
    },
    "Day County Wind LLC": {
      "lng": -97.9056,
      "lat": 45.215
    },
    "Deer Creek Station": {
      "lng": -96.526667,
      "lat": 44.290833
    },
    "Deuel Harvest Wind Energy LLC": {
      "lng": -96.534631,
      "lat": 44.863583
    },
    "Faulkton": {
      "lng": -99.120186,
      "lat": 45.036685
    },
    "Fort Randall": {
      "lng": -98.5539,
      "lat": 43.0653
    },
    "Ft. Pierre": {
      "lng": -100.380833,
      "lat": 44.368056
    },
    "Groton Generating Station": {
      "lng": -98.098689,
      "lat": 45.3735
    },
    "Huron": {
      "lng": -98.17223,
      "lat": 44.369507
    },
    "Lake Preston": {
      "lng": -97.381063,
      "lat": 44.364871
    },
    "Lange": {
      "lng": -103.26,
      "lat": 44.1203
    },
    "Oahe": {
      "lng": -100.3866,
      "lat": 44.4504
    },
    "Oak Tree Energy": {
      "lng": -97.741389,
      "lat": 44.933333
    },
    "Pierre Solar": {
      "lng": -100.307043,
      "lat": 44.388753
    },
    "POET Bioprocessing- Mitchell": {
      "lng": -98.10182,
      "lat": 43.80237
    },
    "POET Biorefining - Chancellor": {
      "lng": -96.9598,
      "lat": 43.3705
    },
    "POET Biorefining - Hudson": {
      "lng": -96.476983,
      "lat": 43.096898
    },
    "Prairie Winds SD1": {
      "lng": -98.7875,
      "lat": 43.886389
    },
    "Prevailing Wind Park": {
      "lng": -97.526,
      "lat": 43.039
    },
    "Rolling Thunder Wind Farm Hybrid": {
      "lng": -99.133889,
      "lat": 44.477778
    },
    "South Dakota Wind Energy Cente": {
      "lng": -99.5,
      "lat": 44.5492
    },
    "Spearfish Hydro": {
      "lng": -103.854722,
      "lat": 44.478333
    },
    "Spirit Mound": {
      "lng": -96.990494,
      "lat": 42.897663
    },
    "State Auto Insurance": {
      "lng": -96.616781,
      "lat": 45.220717
    },
    "Tatanka Ridge": {
      "lng": -96.576,
      "lat": 44.592
    },
    "Triple H Wind Project": {
      "lng": -99.603836,
      "lat": 44.40104
    },
    "Valley Queen Cheese": {
      "lng": -96.637222,
      "lat": 45.221111
    },
    "Watertown Power Plant": {
      "lng": -97.108333,
      "lat": 44.901667
    },
    "Wessington Springs": {
      "lng": -98.590833,
      "lat": 44.0025
    },
    "Willow Creek Wind Power LLC": {
      "lng": -103.207412,
      "lat": 44.825883
    },
    "Yankton": {
      "lng": -97.353333,
      "lat": 42.893333
    },
    "Allen": {
      "lng": -90.14868,
      "lat": 35.074087
    },
    "Apalachia": {
      "lng": -84.295601,
      "lat": 35.167712
    },
    "Bi-County Gas Producers": {
      "lng": -87.544444,
      "lat": 36.540556
    },
    "Boone Dam": {
      "lng": -82.4381,
      "lat": 36.4403
    },
    "Brownsville Combustion Turbine Plant": {
      "lng": -89.198,
      "lat": 35.5438
    },
    "Buffalo Mountain": {
      "lng": -84.338733,
      "lat": 36.120994
    },
    "Buffalo Mountain Energy Center": {
      "lng": -84.344168,
      "lat": 36.127092
    },
    "Bull Run": {
      "lng": -84.1567,
      "lat": 36.0211
    },
    "Calderwood": {
      "lng": -83.9806,
      "lat": 35.4923
    },
    "Center Hill": {
      "lng": -85.8274,
      "lat": 36.0972
    },
    "Chattanooga Metropolitan Airport Solar": {
      "lng": -85.208617,
      "lat": 35.024583
    },
    "Cheatham": {
      "lng": -87.2219,
      "lat": 36.3208
    },
    "Cherokee Dam": {
      "lng": -83.49768,
      "lat": 36.168176
    },
    "Chestnut Ridge Gas Recovery": {
      "lng": -84.0317,
      "lat": 36.1172
    },
    "Chickamauga": {
      "lng": -85.229467,
      "lat": 35.101791
    },
    "Chilhowee": {
      "lng": -84.0503,
      "lat": 35.5453
    },
    "Cordell Hull": {
      "lng": -85.94416,
      "lat": 36.289539
    },
    "Cumberland": {
      "lng": -87.6539,
      "lat": 36.3903
    },
    "Dale Hollow": {
      "lng": -85.4517,
      "lat": 36.5378
    },
    "Domtar Kingsport Mill": {
      "lng": -82.5667,
      "lat": 36.5489
    },
    "Douglas Dam": {
      "lng": -83.5393,
      "lat": 35.9623
    },
    "Eastman Chemical Company": {
      "lng": -82.5431,
      "lat": 36.5219
    },
    "Fort Loudoun": {
      "lng": -84.2431,
      "lat": 35.7917
    },
    "Fort Patrick Henry": {
      "lng": -82.5086,
      "lat": 36.4982
    },
    "Gallatin": {
      "lng": -86.4006,
      "lat": 36.3156
    },
    "Gleason Combustion Turbine Plant": {
      "lng": -88.612,
      "lat": 36.2454
    },
    "Great Falls (TN)": {
      "lng": -85.634063,
      "lat": 35.806732
    },
    "Haywood Solar": {
      "lng": -89.286944,
      "lat": 35.5775
    },
    "IKEA Memphis 508": {
      "lng": -89.799302,
      "lat": 35.189991
    },
    "J P Priest": {
      "lng": -86.6186,
      "lat": 36.1564
    },
    "John Sevier": {
      "lng": -82.9639,
      "lat": 36.3767
    },
    "Johnsonville": {
      "lng": -87.9861,
      "lat": 36.0278
    },
    "Kingston": {
      "lng": -84.5194,
      "lat": 35.8992
    },
    "Lagoon Creek": {
      "lng": -89.3964,
      "lat": 35.6578
    },
    "Latitude Solar Center": {
      "lng": -89.167539,
      "lat": 35.306387
    },
    "McMinnville": {
      "lng": -85.7439,
      "lat": 35.705
    },
    "Melton Hill": {
      "lng": -84.3003,
      "lat": 35.8853
    },
    "Millington Solar Farm": {
      "lng": -89.854725,
      "lat": 35.351533
    },
    "Mountain Home Energy Center": {
      "lng": -82.377504,
      "lat": 36.309609
    },
    "MTSU Power Co-Gen Plant": {
      "lng": -86.365,
      "lat": 35.851389
    },
    "Mulberry Farm LLC": {
      "lng": -88.586667,
      "lat": 35.131944
    },
    "Music City Community Solar": {
      "lng": -86.746776,
      "lat": 36.248832
    },
    "Nickajack": {
      "lng": -85.6218,
      "lat": 35.0017
    },
    "Norris Dam": {
      "lng": -84.0914,
      "lat": 36.2242
    },
    "Ocoee 1": {
      "lng": -84.6478,
      "lat": 35.0947
    },
    "Ocoee 2": {
      "lng": -84.49115,
      "lat": 35.082271
    },
    "Ocoee 3": {
      "lng": -84.4833,
      "lat": 35.075
    },
    "Old Hickory": {
      "lng": -86.6556,
      "lat": 36.2972
    },
    "Opryland USA": {
      "lng": -86.694444,
      "lat": 36.211389
    },
    "Packaging Corporation of America": {
      "lng": -88.2661,
      "lat": 35.0471
    },
    "Pickwick Landing Dam": {
      "lng": -88.2494,
      "lat": 35.0683
    },
    "Powell Valley": {
      "lng": -83.0321,
      "lat": 36.5748
    },
    "Providence Solar": {
      "lng": -89.047779,
      "lat": 35.578333
    },
    "Raccoon Mountain": {
      "lng": -85.387934,
      "lat": 35.055795
    },
    "Resolute Forest Products - Calhoun Ops": {
      "lng": -84.7569,
      "lat": 35.2964
    },
    "Selmer Farm LLC": {
      "lng": -88.555278,
      "lat": 35.149722
    },
    "Selmer I": {
      "lng": -88.551111,
      "lat": 35.140556
    },
    "Selmer II": {
      "lng": -88.646667,
      "lat": 35.166111
    },
    "Sequoyah": {
      "lng": -85.0917,
      "lat": 35.2267
    },
    "Somerville Solar": {
      "lng": -89.35083,
      "lat": 35.22492
    },
    "South Holston": {
      "lng": -82.090213,
      "lat": 36.523174
    },
    "SR Innovation - NIKE PV": {
      "lng": -89.962991,
      "lat": 35.300066
    },
    "SR Jonesborough": {
      "lng": -82.520924,
      "lat": 36.252039
    },
    "SR Washington I, LLC": {
      "lng": -82.598402,
      "lat": 36.347384
    },
    "Tate & Lyle Loudon Plant": {
      "lng": -84.319301,
      "lat": 35.735092
    },
    "Tims Ford": {
      "lng": -86.2783,
      "lat": 35.1967
    },
    "University of Tennessee Steam Plant": {
      "lng": -83.926065,
      "lat": 35.949209
    },
    "Vanderbilt University Power Plant": {
      "lng": -86.803833,
      "lat": 36.145896
    },
    "Volkswagen Solar System": {
      "lng": -85.126944,
      "lat": 35.0975
    },
    "Watauga": {
      "lng": -82.1264,
      "lat": 36.3413
    },
    "Watts Bar Hydro": {
      "lng": -84.785527,
      "lat": 35.619673
    },
    "Watts Bar Nuclear Plant": {
      "lng": -84.7895,
      "lat": 35.6021
    },
    "West Camden": {
      "lng": -88.141965,
      "lat": 36.057892
    },
    "West Tennessee Solar Farm": {
      "lng": -89.3875,
      "lat": 35.408611
    },
    "Wilbur": {
      "lng": -82.126389,
      "lat": 36.341667
    },
    "Wildberry": {
      "lng": -89.368619,
      "lat": 35.058165
    },
    "2W Permian Solar Project Hybrid": {
      "lng": -102.528333,
      "lat": 32.116388
    },
    "Abbott TP 3": {
      "lng": -98.040734,
      "lat": 29.593951
    },
    "Aeolus Wind Facility": {
      "lng": -101.3278,
      "lat": 36.4725
    },
    "Air Liquide Bayport Complex": {
      "lng": -95.0458,
      "lat": 29.6225
    },
    "Air Products Port Arthur": {
      "lng": -93.96515,
      "lat": 29.866497
    },
    "Alamo 6": {
      "lng": -102.270833,
      "lat": 30.993056
    },
    "Alexis Solar, LLC": {
      "lng": -98.233324,
      "lat": 26.88913
    },
    "Alvin": {
      "lng": -95.249367,
      "lat": 29.433245
    },
    "Amadeus Wind Farm": {
      "lng": -100.56428,
      "lat": 32.96736
    },
    "Ameresco Dallas LLC": {
      "lng": -96.644444,
      "lat": 32.648611
    },
    "Amistad Dam & Power": {
      "lng": -101.06011,
      "lat": 29.449827
    },
    "Anacacho Wind Farm, LLC": {
      "lng": -100.209167,
      "lat": 29.234722
    },
    "Angleton": {
      "lng": -95.440647,
      "lat": 29.167065
    },
    "ANSON Solar Center, LLC": {
      "lng": -99.91749,
      "lat": 32.811041
    },
    "Antelope Elk Energy Center": {
      "lng": -101.843333,
      "lat": 33.865
    },
    "Aragorn Solar Project": {
      "lng": -104.442292,
      "lat": 31.719983
    },
    "Arlington Outlet Hydroelectric Generator": {
      "lng": -97.242906,
      "lat": 32.639069
    },
    "Ascend Performance Materials Texas Inc.": {
      "lng": -95.2103,
      "lat": 29.2558
    },
    "Astra Wind Farm": {
      "lng": -102.052422,
      "lat": 34.769544
    },
    "Austin": {
      "lng": -97.784408,
      "lat": 30.293424
    },
    "Austin Gas Recovery": {
      "lng": -97.6217,
      "lat": 30.3344
    },
    "Aviator Wind": {
      "lng": -100.69729,
      "lat": 31.79261
    },
    "Azure Sky Solar": {
      "lng": -99.5985,
      "lat": 32.998948
    },
    "Bacliff Generating Station": {
      "lng": -94.98483,
      "lat": 29.49233
    },
    "Baffin Wind": {
      "lng": -97.59818,
      "lat": 27.184652
    },
    "Bakke": {
      "lng": -102.54,
      "lat": 32.295946
    },
    "Bandera Electric Coop PV": {
      "lng": -99.762212,
      "lat": 29.681032
    },
    "Barilla Solar": {
      "lng": -103.35,
      "lat": 30.96
    },
    "Barney M. Davis": {
      "lng": -97.3117,
      "lat": 27.6064
    },
    "Barton Chapel Wind Farm": {
      "lng": -98.328415,
      "lat": 33.052226
    },
    "BASF Freeport Works": {
      "lng": -95.394,
      "lat": 29.002
    },
    "Bastrop Clean Energy Center": {
      "lng": -97.55,
      "lat": 30.1458
    },
    "Bat Cave": {
      "lng": -99.231778,
      "lat": 30.737745
    },
    "Baytown Energy Center": {
      "lng": -94.9019,
      "lat": 29.7731
    },
    "BayWa r.e Mozart LLC": {
      "lng": -100.524167,
      "lat": 32.969722
    },
    "Bearkat": {
      "lng": -101.582,
      "lat": 31.727237
    },
    "Bethel Wind Farm LLC": {
      "lng": -102.474698,
      "lat": 34.575311
    },
    "Big Spring Wind Power Facility": {
      "lng": -101.388333,
      "lat": 32.2075
    },
    "Blackhawk Station": {
      "lng": -101.36,
      "lat": 35.6957
    },
    "Blue Cloud Wind Energy LLC": {
      "lng": -102.665242,
      "lat": 34.03821
    },
    "Blue Summit II Wind, LLC": {
      "lng": -99.477986,
      "lat": 34.225882
    },
    "Blue Summit III Wind": {
      "lng": -99.563436,
      "lat": 34.170488
    },
    "Blue Summit Storage, LLC": {
      "lng": -99.399,
      "lat": 34.299
    },
    "Blue Summit Wind LLC": {
      "lng": -99.364722,
      "lat": 34.29
    },
    "Blue Wing Solar Energy Generation": {
      "lng": -98.400339,
      "lat": 29.304177
    },
    "Bluebell Solar": {
      "lng": -100.828,
      "lat": 31.87
    },
    "Bluebell Solar II": {
      "lng": -100.825,
      "lat": 31.888
    },
    "BMP Wind (TX)": {
      "lng": -99.291596,
      "lat": 32.28938
    },
    "Bobcat Bluff Wind Project LLC": {
      "lng": -98.581944,
      "lat": 33.493056
    },
    "Borger Plant": {
      "lng": -101.4354,
      "lat": 35.6647
    },
    "Bosque County Power Plant": {
      "lng": -97.3586,
      "lat": 31.8594
    },
    "Bovine": {
      "lng": -96.076894,
      "lat": 29.631324
    },
    "BP Chemicals Green Lake Plant": {
      "lng": -96.8333,
      "lat": 28.5707
    },
    "Brandon Station": {
      "lng": -101.886093,
      "lat": 33.585257
    },
    "Brazoria": {
      "lng": -95.577587,
      "lat": 29.062635
    },
    "Brazos Valley Energy, LP": {
      "lng": -95.6244,
      "lat": 29.4731
    },
    "Brazos Wind Farm": {
      "lng": -101.147784,
      "lat": 32.94793
    },
    "Briar Creek Solar 1": {
      "lng": -96.585136,
      "lat": 32.067483
    },
    "Bridgeport Gas Processing Plant": {
      "lng": -97.802547,
      "lat": 33.195763
    },
    "Briscoe Wind Farm": {
      "lng": -101.237233,
      "lat": 34.432267
    },
    "Brisket Wind (8) LLC": {
      "lng": -101.8614,
      "lat": 36.0075
    },
    "Bronson": {
      "lng": -95.969548,
      "lat": 29.537482
    },
    "Bruennings Breeze Wind Farm": {
      "lng": -97.691111,
      "lat": 26.522222
    },
    "Bryan (TX)": {
      "lng": -96.372316,
      "lat": 30.64741
    },
    "Bryan Solar, LLC": {
      "lng": -104.3,
      "lat": 29.6
    },
    "BT Cooke Solar, LLC": {
      "lng": -97.1798,
      "lat": 33.5493
    },
    "Buchanan Dam (TX)": {
      "lng": -98.4176,
      "lat": 30.7507
    },
    "Buckthorn Westex": {
      "lng": -102.551292,
      "lat": 30.57552
    },
    "Buckthorn Wind": {
      "lng": -98.377389,
      "lat": 32.371306
    },
    "Buffalo Gap 2 Wind Farm": {
      "lng": -100.176986,
      "lat": 32.318221
    },
    "Buffalo Gap 3 Wind Farm": {
      "lng": -100.1,
      "lat": 32.2878
    },
    "Buffalo Gap Wind Farm": {
      "lng": -100.061589,
      "lat": 32.311556
    },
    "Bull Creek Wind": {
      "lng": -101.578617,
      "lat": 32.929014
    },
    "C. R. Wing Cogeneration Plant": {
      "lng": -101.422386,
      "lat": 32.273178
    },
    "Cactus Flats Wind Energy Project": {
      "lng": -100.028142,
      "lat": 31.112474
    },
    "Callahan Divide Wind Energy Center": {
      "lng": -100.021667,
      "lat": 32.303889
    },
    "Calpine Hidalgo Energy Center": {
      "lng": -98.175759,
      "lat": 26.34172
    },
    "Cameron Wind 1 LLC": {
      "lng": -97.466111,
      "lat": 26.195278
    },
    "Canadian Breaks, LLC": {
      "lng": -102.315517,
      "lat": 35.200164
    },
    "Canyon": {
      "lng": -98.194733,
      "lat": 29.87056
    },
    "Capricorn Ridge Wind LLC": {
      "lng": -101.018936,
      "lat": 31.951958
    },
    "Carbon Capture Plant": {
      "lng": -95.634167,
      "lat": 29.475278
    },
    "Cascade Solar (TX)": {
      "lng": -96.023384,
      "lat": 29.205355
    },
    "Castle Gap Solar Hybrid": {
      "lng": -102.272,
      "lat": 31.255
    },
    "CatanSolar": {
      "lng": -97.696944,
      "lat": 28.871642
    },
    "CED Crane Solar": {
      "lng": -102.31165,
      "lat": 31.254361
    },
    "Cedar Bayou": {
      "lng": -94.9256,
      "lat": 29.75
    },
    "Cedar Bayou 4": {
      "lng": -94.92312,
      "lat": 29.751645
    },
    "Cedro Hill Wind LLC": {
      "lng": -98.905278,
      "lat": 27.576111
    },
    "Central Utility Plant": {
      "lng": -97.842586,
      "lat": 30.397351
    },
    "Central Utility Plant - Texas A&M": {
      "lng": -96.343333,
      "lat": 30.617778
    },
    "CFB Power Plant": {
      "lng": -96.539722,
      "lat": 28.650278
    },
    "Chamon Power, LLC": {
      "lng": -95.111345,
      "lat": 29.883214
    },
    "Champion Wind Farm LLC": {
      "lng": -100.6481,
      "lat": 32.3983
    },
    "Channel Energy Center": {
      "lng": -95.2319,
      "lat": 29.7189
    },
    "Channelview Cogeneration Facility": {
      "lng": -95.121744,
      "lat": 29.836952
    },
    "Chapman Ranch Wind I": {
      "lng": -97.500833,
      "lat": 27.5925
    },
    "Chisholm Grid Energy Storage System": {
      "lng": -97.386111,
      "lat": 32.904444
    },
    "Chisum": {
      "lng": -95.39071,
      "lat": 33.63291
    },
    "Cirrus Wind 1 LLC": {
      "lng": -101.6875,
      "lat": 33.033056
    },
    "Citrus City": {
      "lng": -98.351744,
      "lat": 26.320997
    },
    "Clara": {
      "lng": -95.553945,
      "lat": 29.845265
    },
    "Colbeck's Corner, LLC": {
      "lng": -101.186622,
      "lat": 35.254184
    },
    "Coleto Creek": {
      "lng": -97.214167,
      "lat": 28.7128
    },
    "Colorado Bend Energy Center": {
      "lng": -96.0683,
      "lat": 29.2878
    },
    "Colorado Bend II": {
      "lng": -96.065399,
      "lat": 29.289415
    },
    "Comanche Peak": {
      "lng": -97.785515,
      "lat": 32.298365
    },
    "Commerce ESS": {
      "lng": -98.61765,
      "lat": 29.43445
    },
    "Commerce Solar": {
      "lng": -98.61765,
      "lat": 29.43445
    },
    "Coniglio Solar": {
      "lng": -96.082472,
      "lat": 33.404486
    },
    "Copper Station": {
      "lng": -106.375,
      "lat": 31.7569
    },
    "Corazon Energy LLC": {
      "lng": -99.258329,
      "lat": 27.634677
    },
    "Corpus Christi": {
      "lng": -97.595813,
      "lat": 27.811083
    },
    "Corpus Christi Energy Center": {
      "lng": -97.4283,
      "lat": 27.8139
    },
    "Corpus Refinery": {
      "lng": -97.5281,
      "lat": 27.8322
    },
    "CoServ Community Solar Station": {
      "lng": -96.979168,
      "lat": 33.2706
    },
    "Cotton Plains Wind Farm": {
      "lng": -101.187628,
      "lat": 34.060333
    },
    "Cottonwood Energy Project": {
      "lng": -93.7353,
      "lat": 30.2588
    },
    "Covel Gardens Gas Recovery": {
      "lng": -98.6536,
      "lat": 29.3467
    },
    "Coyote Wind LLC": {
      "lng": -100.866458,
      "lat": 32.84879
    },
    "CPS 1 Community Solar": {
      "lng": -98.26922,
      "lat": 29.377966
    },
    "Cutten": {
      "lng": -95.527643,
      "lat": 29.956811
    },
    "Decker Creek": {
      "lng": -97.6128,
      "lat": 30.3033
    },
    "Decordova": {
      "lng": -97.700556,
      "lat": 32.403056
    },
    "Deer Park Energy Center": {
      "lng": -95.134508,
      "lat": 29.713414
    },
    "Denison": {
      "lng": -96.5692,
      "lat": 33.8181
    },
    "Denton Energy Center": {
      "lng": -97.209692,
      "lat": 33.214869
    },
    "Dermott Wind": {
      "lng": -100.975128,
      "lat": 32.863769
    },
    "Desert Sky": {
      "lng": -102.0867,
      "lat": 30.9178
    },
    "DeWind Frisco": {
      "lng": -101.605278,
      "lat": 36.498514
    },
    "DFW Gas Recovery": {
      "lng": -96.961986,
      "lat": 33.019646
    },
    "Dickinson": {
      "lng": -95.056987,
      "lat": 29.461255
    },
    "Dunlap TP 1": {
      "lng": -98.045934,
      "lat": 29.640187
    },
    "E. Porte Ct.": {
      "lng": -97.664239,
      "lat": 26.19208
    },
    "Eagle Pass": {
      "lng": -100.552234,
      "lat": 28.829535
    },
    "East Blackland Solar Project 1": {
      "lng": -97.4614,
      "lat": 30.4214
    },
    "East Pecos Solar": {
      "lng": -102.281744,
      "lat": 31.000883
    },
    "Eastman Cogeneration Facility": {
      "lng": -94.6903,
      "lat": 32.4481
    },
    "Ector County Energy Center": {
      "lng": -102.585556,
      "lat": 32.069167
    },
    "Eddy II": {
      "lng": -97.260738,
      "lat": 31.291151
    },
    "EG178 Facility": {
      "lng": -100.954661,
      "lat": 32.747058
    },
    "El Campo Wind": {
      "lng": -99.726392,
      "lat": 33.732181
    },
    "Elara Solar": {
      "lng": -99.178605,
      "lat": 28.883196
    },
    "Elbow Creek Wind Project LLC": {
      "lng": -101.3986,
      "lat": 32.1408
    },
    "Electra Wind Farm": {
      "lng": -99.118746,
      "lat": 34.102943
    },
    "Elk Station": {
      "lng": -101.843333,
      "lat": 33.865
    },
    "Elm Branch Solar 1": {
      "lng": -96.706121,
      "lat": 32.23256
    },
    "ENGIE Long Draw Solar LLC": {
      "lng": -101.621793,
      "lat": 32.741383
    },
    "Engineered Carbons Borger Cogen": {
      "lng": -101.432004,
      "lat": 35.66704
    },
    "Ennis Power Company, LLC": {
      "lng": -96.675,
      "lat": 32.32
    },
    "Enterprise Products Operating": {
      "lng": -94.9197,
      "lat": 29.8247
    },
    "ETT Presidio NaS Battery": {
      "lng": -104.35535,
      "lat": 29.566263
    },
    "E-Volve Energy Holdings LLC": {
      "lng": -96.15994,
      "lat": 29.740788
    },
    "ExxonMobil Baytown Refinery": {
      "lng": -94.997277,
      "lat": 29.753517
    },
    "ExxonMobil Baytown Turbine": {
      "lng": -95.009648,
      "lat": 29.759124
    },
    "ExxonMobil Beaumont Refinery": {
      "lng": -94.0753,
      "lat": 30.0636
    },
    "Falcon Dam & Power": {
      "lng": -99.1642,
      "lat": 26.5575
    },
    "Farmers Branch Renewable Energy Facility": {
      "lng": -96.957222,
      "lat": 33.031111
    },
    "FELPS 1 - Calaveras": {
      "lng": -98.250865,
      "lat": 29.220361
    },
    "FELPS 2 - Calaveras": {
      "lng": -98.251671,
      "lat": 29.220942
    },
    "FELPS 3 - Floresville South": {
      "lng": -98.106298,
      "lat": 29.129219
    },
    "FELPS 4 - Floresville South": {
      "lng": -98.107043,
      "lat": 29.128666
    },
    "FELPS 5- Floresville South": {
      "lng": -98.107343,
      "lat": 29.128151
    },
    "FELPS 6- Floresville West": {
      "lng": -98.193353,
      "lat": 29.13398
    },
    "FELPS 7- Floresville West": {
      "lng": -98.192753,
      "lat": 29.133613
    },
    "Filet Wind (5) LLC": {
      "lng": -101.8761,
      "lat": 36.4453
    },
    "Flat Iron Wind (7) LLC": {
      "lng": -101.9,
      "lat": 36.0264
    },
    "Flat Top Wind I": {
      "lng": -98.603358,
      "lat": 31.684511
    },
    "Flower Valley I": {
      "lng": -103.361769,
      "lat": 31.197283
    },
    "Fluvanna": {
      "lng": -101.143611,
      "lat": 32.931944
    },
    "Foard City Wind": {
      "lng": -99.740278,
      "lat": 33.915315
    },
    "Forest Creek Wind Farm LLC": {
      "lng": -101.22,
      "lat": 32.039
    },
    "Formosa Utility Venture Ltd": {
      "lng": -96.5417,
      "lat": 28.6917
    },
    "Forney Power Plant": {
      "lng": -96.4916,
      "lat": 32.7563
    },
    "Fort Bliss (DEA EPIC)": {
      "lng": -106.372861,
      "lat": 31.829986
    },
    "Frankel": {
      "lng": -102.78282,
      "lat": 32.381434
    },
    "Freeport Energy": {
      "lng": -95.407481,
      "lat": 28.991289
    },
    "Freeport Energy Center": {
      "lng": -95.395417,
      "lat": 28.988761
    },
    "Freeport LP Pretreatment Facility": {
      "lng": -95.306944,
      "lat": 28.979167
    },
    "Freestone Power Generation": {
      "lng": -96.1131,
      "lat": 31.8907
    },
    "G.S.E. One LLC": {
      "lng": -96.146489,
      "lat": 33.500378
    },
    "Galloway 1 Solar Farm": {
      "lng": -99.796613,
      "lat": 31.435932
    },
    "Gambit Energy Storage - Angleton Storage": {
      "lng": -95.445062,
      "lat": 29.16801
    },
    "GCWA": {
      "lng": -94.953418,
      "lat": 29.380114
    },
    "GE-Lubbock": {
      "lng": -102.056875,
      "lat": 33.59145
    },
    "Goat Wind LP": {
      "lng": -100.791389,
      "lat": 31.951944
    },
    "Golden Spread Panhandle Wnd Rch": {
      "lng": -102.21,
      "lat": 35.243611
    },
    "Goldsmith": {
      "lng": -102.63501,
      "lat": 31.975269
    },
    "Goldthwaite Wind Energy Facility": {
      "lng": -98.466667,
      "lat": 31.383333
    },
    "Gonzales Hydro Plant": {
      "lng": -97.45516,
      "lat": 29.497166
    },
    "Goodyear Beaumont Chemical Plant": {
      "lng": -94.2142,
      "lat": 29.9728
    },
    "Gopher Creek Wind Farm": {
      "lng": -101.208955,
      "lat": 32.875073
    },
    "Graham": {
      "lng": -98.6117,
      "lat": 33.1344
    },
    "Grandview Wind Farm, LLC": {
      "lng": -101.310833,
      "lat": 35.238333
    },
    "Granite Shoals": {
      "lng": -98.3384,
      "lat": 30.556
    },
    "Greasewood Solar": {
      "lng": -102.488209,
      "lat": 31.030996
    },
    "Great Plains Windpark LLC": {
      "lng": -101.3917,
      "lat": 36.4964
    },
    "Green Pastures Wind I": {
      "lng": -99.411169,
      "lat": 33.628222
    },
    "Green Pastures Wind II": {
      "lng": -99.532944,
      "lat": 33.645964
    },
    "Greens Bayou": {
      "lng": -95.219429,
      "lat": 29.822201
    },
    "Gregory Power Facility": {
      "lng": -97.258417,
      "lat": 27.88929
    },
    "Griffin Solar": {
      "lng": -97.141218,
      "lat": 31.474114
    },
    "Griffin Trail Wind": {
      "lng": -99.58229,
      "lat": 33.609313
    },
    "Guadalupe Generating Station": {
      "lng": -98.1419,
      "lat": 29.6244
    },
    "Gunsight Mountain Wind Energy LLC": {
      "lng": -101.473611,
      "lat": 32.240278
    },
    "H 4": {
      "lng": -97.624948,
      "lat": 29.495193
    },
    "H 5": {
      "lng": -97.4919,
      "lat": 29.4681
    },
    "H W Pirkey Power Plant": {
      "lng": -94.4852,
      "lat": 32.4607
    },
    "Hackberry Wind Farm": {
      "lng": -99.4228,
      "lat": 32.7683
    },
    "Hal C Weaver Power Plant": {
      "lng": -97.7356,
      "lat": 30.2867
    },
    "Hale Community Wind Farm": {
      "lng": -101.742222,
      "lat": 33.927778
    },
    "Handley Generating Station": {
      "lng": -97.2192,
      "lat": 32.7283
    },
    "Hardin County Peaking Facility": {
      "lng": -94.2526,
      "lat": 30.3038
    },
    "Harrington Station": {
      "lng": -101.747187,
      "lat": 35.29816
    },
    "Harrison County Power Project": {
      "lng": -94.4367,
      "lat": 32.3958
    },
    "Hays Energy Facility": {
      "lng": -97.9894,
      "lat": 29.7806
    },
    "Heart of Texas Wind Project": {
      "lng": -99.421266,
      "lat": 31.242177
    },
    "HEB00020": {
      "lng": -95.744444,
      "lat": 29.992502
    },
    "HEB00028": {
      "lng": -95.09527,
      "lat": 29.466105
    },
    "HEB00038": {
      "lng": -98.46543,
      "lat": 26.20327
    },
    "HEB00054": {
      "lng": -95.250237,
      "lat": 29.601653
    },
    "HEB00057": {
      "lng": -97.28064,
      "lat": 27.665051
    },
    "HEB00070": {
      "lng": -99.758324,
      "lat": 32.433318
    },
    "HEB00099": {
      "lng": -95.576602,
      "lat": 29.995329
    },
    "HEB00109": {
      "lng": -95.539403,
      "lat": 29.786159
    },
    "HEB00110": {
      "lng": -95.53628,
      "lat": 29.53658
    },
    "HEB00182": {
      "lng": -97.370901,
      "lat": 31.07212
    },
    "HEB00292": {
      "lng": -95.958458,
      "lat": 28.982463
    },
    "Heights": {
      "lng": -94.949867,
      "lat": 29.389595
    },
    "Hidalgo Wind Farm II": {
      "lng": -98.448267,
      "lat": 26.492862
    },
    "Hidalgo Wind Farm LLC": {
      "lng": -98.411111,
      "lat": 26.465556
    },
    "High Lonesome Wind Power, LLC Hybrid": {
      "lng": -101.881147,
      "lat": 31.171652
    },
    "Highway 56 Solar": {
      "lng": -96.690448,
      "lat": 33.614436
    },
    "HO Clarke Generating": {
      "lng": -95.45169,
      "lat": 29.64536
    },
    "Hoef's Road Storage": {
      "lng": -103.46256,
      "lat": 31.031247
    },
    "Holstein 1 Solar Farm": {
      "lng": -100.162375,
      "lat": 32.104125
    },
    "Horse Creek Wind Farm": {
      "lng": -99.54,
      "lat": 33.35
    },
    "Horse Hollow Wind Energy Center": {
      "lng": -100.05712,
      "lat": 32.214024
    },
    "Houston Chemical Complex Battleground": {
      "lng": -95.0839,
      "lat": 29.7314
    },
    "Houston Plant": {
      "lng": -95.270562,
      "lat": 29.71977
    },
    "Hubbard Wind": {
      "lng": -96.828196,
      "lat": 31.77471
    },
    "IKEA Grand Prairie Rooftop PV System": {
      "lng": -97.05,
      "lat": 32.82
    },
    "IKEA Live Oak Rooftop PV System": {
      "lng": -98.25,
      "lat": 29.48
    },
    "IKEA Round Rock 027": {
      "lng": -97.690286,
      "lat": 30.557044
    },
    "Impact Solar 1": {
      "lng": -95.370856,
      "lat": 33.467803
    },
    "Inadale Wind Farm LLC Hybrid": {
      "lng": -100.5797,
      "lat": 32.4969
    },
    "Ingleside Cogeneration": {
      "lng": -97.242778,
      "lat": 27.882778
    },
    "Inks": {
      "lng": -98.385422,
      "lat": 30.731599
    },
    "International Paper - Orange": {
      "lng": -93.7422,
      "lat": 30.2178
    },
    "International Paper Texarkana Mill": {
      "lng": -94.0696,
      "lat": 33.2553
    },
    "J K Spruce": {
      "lng": -98.3203,
      "lat": 29.309722
    },
    "J Robert Massengale Generating Station": {
      "lng": -101.8408,
      "lat": 33.6039
    },
    "J T Deely": {
      "lng": -98.3228,
      "lat": 29.308056
    },
    "Jack County Generation Facility": {
      "lng": -97.9574,
      "lat": 33.101
    },
    "Jameson Gas Processing Plant": {
      "lng": -100.6917,
      "lat": 32.05
    },
    "Javelina Wind Energy II, LLC": {
      "lng": -98.91,
      "lat": 27.44
    },
    "Javelina Wind Energy, LLC": {
      "lng": -99,
      "lat": 27.32
    },
    "JCO Oxides Olefins Plant": {
      "lng": -93.929737,
      "lat": 29.963502
    },
    "Johnson County Generation Facility": {
      "lng": -97.4078,
      "lat": 32.3994
    },
    "Jones Station": {
      "lng": -101.7392,
      "lat": 33.5239
    },
    "Jumbo Hill Wind Project": {
      "lng": -102.949,
      "lat": 32.386
    },
    "Juno Solar Project": {
      "lng": -101.39102,
      "lat": 32.772889
    },
    "Karankawa Wind LLC": {
      "lng": -97.817507,
      "lat": 28.170179
    },
    "Keechi Wind": {
      "lng": -98.161111,
      "lat": 33.143056
    },
    "Kellam Solar": {
      "lng": -95.7156,
      "lat": 32.4744
    },
    "King Mountain Wind Ranch 1": {
      "lng": -102.2417,
      "lat": 31.2092
    },
    "Kingsberry Energy Storage System": {
      "lng": -97.686139,
      "lat": 30.266028
    },
    "Knox Lee Power Plant": {
      "lng": -94.6415,
      "lat": 32.3766
    },
    "La Chalupa, LLC": {
      "lng": -97.511842,
      "lat": 26.249888
    },
    "Lake Hubbard": {
      "lng": -96.5458,
      "lat": 32.8358
    },
    "Lamar Power Plant": {
      "lng": -95.59,
      "lat": 33.6308
    },
    "Lamesa II": {
      "lng": -101.885218,
      "lat": 32.713326
    },
    "Lamesa Solar": {
      "lng": -101.926522,
      "lat": 32.715611
    },
    "Lampwick": {
      "lng": -99.537261,
      "lat": 30.872228
    },
    "Langford Wind Power": {
      "lng": -100.67882,
      "lat": 31.085435
    },
    "Lapetus": {
      "lng": -102.672809,
      "lat": 32.460054
    },
    "LaPorte Generating Station": {
      "lng": -95.0718,
      "lat": 29.7028
    },
    "Laredo": {
      "lng": -99.5089,
      "lat": 27.5667
    },
    "Las Lomas Wind Project": {
      "lng": -99.05745,
      "lat": 26.69842
    },
    "Las Majadas Wind Farm": {
      "lng": -97.545341,
      "lat": 26.364843
    },
    "Leon Creek": {
      "lng": -98.5761,
      "lat": 29.3525
    },
    "Leon Solar": {
      "lng": -96.239739,
      "lat": 33.16114
    },
    "Lewis Creek": {
      "lng": -95.5214,
      "lat": 30.4356
    },
    "Lewisville": {
      "lng": -96.963806,
      "lat": 33.068473
    },
    "Lily Solar Hybrid": {
      "lng": -96.428985,
      "lat": 32.533273
    },
    "Limestone": {
      "lng": -96.2525,
      "lat": 31.4219
    },
    "Little Pringle I Wind Farm": {
      "lng": -101.540238,
      "lat": 35.964272
    },
    "Little Pringle II Wind Farm": {
      "lng": -101.541363,
      "lat": 35.924858
    },
    "Live Oak Wind Project": {
      "lng": -100.623114,
      "lat": 31.048557
    },
    "Llano Estacado Wind Ranch": {
      "lng": -101.2533,
      "lat": 35.4677
    },
    "Lockett Windfarm": {
      "lng": -99.264676,
      "lat": 34.072251
    },
    "Logans Gap Wind LLC": {
      "lng": -98.690556,
      "lat": 31.8225
    },
    "Longhorn Wind": {
      "lng": -101.251111,
      "lat": 34.311111
    },
    "Loop 463": {
      "lng": -97.074867,
      "lat": 28.816475
    },
    "Loraine Windpark Project LLC": {
      "lng": -100.7444,
      "lat": 32.4375
    },
    "Lorenzo Wind": {
      "lng": -101.524722,
      "lat": 33.699167
    },
    "Los Vientos V Wind Power": {
      "lng": -98.571554,
      "lat": 26.411689
    },
    "Los Vientos Wind 1A": {
      "lng": -97.5857,
      "lat": 26.33072
    },
    "Los Vientos Wind 1B": {
      "lng": -97.6668,
      "lat": 26.34892
    },
    "Los Vientos Windpower III": {
      "lng": -98.806111,
      "lat": 26.388611
    },
    "Los Vientos Windpower IV": {
      "lng": -98.818333,
      "lat": 26.380556
    },
    "Lost Pines 1": {
      "lng": -97.2714,
      "lat": 30.1478
    },
    "Magic Valley Generating Station": {
      "lng": -98.19,
      "lat": 26.3403
    },
    "Magic Valley Wind Farm I LLC": {
      "lng": -97.691111,
      "lat": 26.475278
    },
    "Magnolia TX": {
      "lng": -95.133947,
      "lat": 29.488335
    },
    "Majestic 1 Wind Farm": {
      "lng": -101.53,
      "lat": 35.3919
    },
    "Majestic II Wind": {
      "lng": -101.578572,
      "lat": 35.386038
    },
    "Marble Falls": {
      "lng": -98.257593,
      "lat": 30.555288
    },
    "Mariah del Norte": {
      "lng": -102.577778,
      "lat": 34.6675
    },
    "Marlin Solar": {
      "lng": -96.877205,
      "lat": 31.286739
    },
    "Mars Solar": {
      "lng": -98.834962,
      "lat": 27.421553
    },
    "Marshall Ford": {
      "lng": -97.9073,
      "lat": 30.3899
    },
    "Martin Lake": {
      "lng": -94.5706,
      "lat": 32.2606
    },
    "Maryneal Windpower": {
      "lng": -100.491908,
      "lat": 32.199366
    },
    "McAdoo Wind Energy LLC": {
      "lng": -100.967402,
      "lat": 33.752201
    },
    "Mckeever": {
      "lng": -95.479339,
      "lat": 29.508402
    },
    "Mesquite Creek LFGTE Project": {
      "lng": -98.0228,
      "lat": 29.7372
    },
    "Mesquite Creek Wind": {
      "lng": -101.741111,
      "lat": 32.7
    },
    "Mesquite Star": {
      "lng": -100.320309,
      "lat": 32.255978
    },
    "Mesquite Wind Power LLC": {
      "lng": -99.4928,
      "lat": 32.6256
    },
    "Mesteno": {
      "lng": -98.745522,
      "lat": 26.523692
    },
    "Miami Wind Energy Center": {
      "lng": -100.538611,
      "lat": 35.653889
    },
    "Midlothian Energy": {
      "lng": -97.0537,
      "lat": 32.4302
    },
    "Midway Solar - TX": {
      "lng": -102.223537,
      "lat": 30.995503
    },
    "Midway Wind, LLC": {
      "lng": -97.300202,
      "lat": 27.954976
    },
    "Misae Solar": {
      "lng": -100.099625,
      "lat": 34.380532
    },
    "Mont Belvieu Cogeneration Unit": {
      "lng": -94.900661,
      "lat": 29.838138
    },
    "Montana Power Station": {
      "lng": -106.211944,
      "lat": 31.823889
    },
    "Montana Solar Facility": {
      "lng": -106.218892,
      "lat": 31.813061
    },
    "Montgomery County Power Station": {
      "lng": -95.5275,
      "lat": 30.44083
    },
    "Morgan Creek": {
      "lng": -100.9156,
      "lat": 32.3358
    },
    "Motiva Enterprises Port Arthur Refinery": {
      "lng": -93.950984,
      "lat": 29.888462
    },
    "Mountain Creek Generating Station": {
      "lng": -96.9358,
      "lat": 32.7231
    },
    "Mustang Station": {
      "lng": -102.7417,
      "lat": 32.9728
    },
    "Mustang Station (56326)": {
      "lng": -102.743607,
      "lat": 32.974554
    },
    "N. Mary Francis": {
      "lng": -102.431039,
      "lat": 31.921616
    },
    "Nacogdoches Generating Facility": {
      "lng": -94.90064,
      "lat": 31.832132
    },
    "NAFTA Region Olefins Complex": {
      "lng": -93.883592,
      "lat": 29.954321
    },
    "NASA Johnson Space Center CHP": {
      "lng": -95.089302,
      "lat": 29.562372
    },
    "Nelson Gardens Landfill Gas to Energy": {
      "lng": -98.66,
      "lat": 29.33
    },
    "NET Power La Porte Station": {
      "lng": -95.054145,
      "lat": 29.646791
    },
    "Newman Power Station": {
      "lng": -106.431777,
      "lat": 31.983587
    },
    "Newman Solar": {
      "lng": -106.4275,
      "lat": 31.978333
    },
    "Niagara Bottling - Seguin": {
      "lng": -97.977198,
      "lat": 29.589139
    },
    "Nichols Station": {
      "lng": -101.746423,
      "lat": 35.283357
    },
    "Nolte": {
      "lng": -97.932106,
      "lat": 29.537565
    },
    "North Fork TX": {
      "lng": -97.875708,
      "lat": 30.748955
    },
    "North Gainesville Solar": {
      "lng": -97.148569,
      "lat": 33.656072
    },
    "Notrees Windpower Hybrid": {
      "lng": -102.828333,
      "lat": 31.995
    },
    "NRG Elbow Creek Energy Storage Project": {
      "lng": -101.479217,
      "lat": 32.167575
    },
    "Nueces Bay": {
      "lng": -97.419203,
      "lat": 27.819412
    },
    "NWP Indian Mesa Wind Farm": {
      "lng": -102.201433,
      "lat": 30.931467
    },
    "O W Sommers": {
      "lng": -98.3242,
      "lat": 29.308056
    },
    "Oberon IA": {
      "lng": -102.701206,
      "lat": 31.703258
    },
    "Oberon IB": {
      "lng": -102.69743,
      "lat": 31.692358
    },
    "OCI Alamo 2, LLC": {
      "lng": -98.331944,
      "lat": 29.469444
    },
    "OCI Alamo 3 LLC": {
      "lng": -98.297222,
      "lat": 29.479167
    },
    "OCI Alamo 4, LLC": {
      "lng": -100.383889,
      "lat": 29.328889
    },
    "OCI Alamo 5 LLC": {
      "lng": -99.716111,
      "lat": 29.216667
    },
    "OCI Alamo 7 LLC": {
      "lng": -99.5525,
      "lat": 33.04
    },
    "OCI Alamo Solar I Hybrid": {
      "lng": -98.444722,
      "lat": 29.271667
    },
    "Ocotillo Windpower": {
      "lng": -101.3853,
      "lat": 32.1217
    },
    "Odessa": {
      "lng": -102.398409,
      "lat": 31.871315
    },
    "Odessa Ector Generating Station": {
      "lng": -102.3264,
      "lat": 31.8403
    },
    "Odyssey Energy Altura Cogen, LLC": {
      "lng": -95.10773,
      "lat": 29.8161
    },
    "Old Settler Wind": {
      "lng": -101.187628,
      "lat": 34.060333
    },
    "Oxy Renewable Energy - Goldsmith": {
      "lng": -102.340588,
      "lat": 31.982702
    },
    "Oyster Creek Unit VIII": {
      "lng": -95.342,
      "lat": 28.9802
    },
    "Palmas Wind, LLC": {
      "lng": -97.496389,
      "lat": 26.288889
    },
    "Palo Duro Wind": {
      "lng": -101.001389,
      "lat": 36.243889
    },
    "Panda Sherman Power Station": {
      "lng": -96.617893,
      "lat": 33.582573
    },
    "Panda Temple Power Station": {
      "lng": -97.317222,
      "lat": 31.055833
    },
    "Pantex": {
      "lng": -101.5325,
      "lat": 35.322778
    },
    "Panther Creek Wind Farm I": {
      "lng": -101.451385,
      "lat": 32.100823
    },
    "Panther Creek Wind Farm II": {
      "lng": -101.2983,
      "lat": 31.9361
    },
    "Panther Creek Wind Farm Three": {
      "lng": -101.1167,
      "lat": 31.9925
    },
    "Papalote Creek I LLC": {
      "lng": -97.4586,
      "lat": 27.9386
    },
    "Papalote Creek II LLC": {
      "lng": -97.2967,
      "lat": 27.9958
    },
    "Paris Energy Center": {
      "lng": -95.5577,
      "lat": 33.6968
    },
    "Pasadena Power Plant": {
      "lng": -95.176479,
      "lat": 29.72475
    },
    "Patriot Wind Farm": {
      "lng": -97.640077,
      "lat": 27.580489
    },
    "Pattern Gulf Wind": {
      "lng": -97.5994,
      "lat": 27.0011
    },
    "Pattern Panhandle Wind 2 LLC": {
      "lng": -101.372778,
      "lat": 35.442778
    },
    "Pattern Panhandle Wind LLC": {
      "lng": -101.253056,
      "lat": 35.4275
    },
    "Pearl Solar": {
      "lng": -102.26831,
      "lat": 30.98784
    },
    "Pearsall": {
      "lng": -99.0919,
      "lat": 28.9275
    },
    "Penascal II Wind Project LLC": {
      "lng": -97.557372,
      "lat": 27.122529
    },
    "Penascal Wind Power LLC": {
      "lng": -97.498478,
      "lat": 27.022112
    },
    "Permian Basin": {
      "lng": -102.9633,
      "lat": 31.5839
    },
    "Peyton Creek Wind Farm LLC": {
      "lng": -95.8631,
      "lat": 28.8433
    },
    "Phantom Solar": {
      "lng": -97.845111,
      "lat": 31.114983
    },
    "Phelps Dodge Refining": {
      "lng": -106.391314,
      "lat": 31.764458
    },
    "Phoebe Solar": {
      "lng": -102.868295,
      "lat": 31.843422
    },
    "Plant X": {
      "lng": -102.4114,
      "lat": 34.1661
    },
    "Pleasant Hill Wind Energy Project": {
      "lng": -101.485,
      "lat": 33.738889
    },
    "Port Arthur Texas Refinery": {
      "lng": -93.891283,
      "lat": 29.964938
    },
    "Port Comfort Peaking Facility": {
      "lng": -96.54621,
      "lat": 28.64807
    },
    "Port Neches": {
      "lng": -93.952294,
      "lat": 29.97876
    },
    "Porterhouse Wind (4) LLC": {
      "lng": -101.3864,
      "lat": 36.4144
    },
    "Post Oak Wind LLC": {
      "lng": -99.6564,
      "lat": 32.5144
    },
    "Post Wind Farm LP": {
      "lng": -101.219397,
      "lat": 32.92544
    },
    "Power Lane Steam Plant": {
      "lng": -96.1264,
      "lat": 33.1707
    },
    "Power Station 4": {
      "lng": -94.92195,
      "lat": 29.378164
    },
    "PowerFin Kingsbery": {
      "lng": -97.684861,
      "lat": 30.265877
    },
    "Prairie Hill Wind Project": {
      "lng": -96.904072,
      "lat": 31.549868
    },
    "Prospect Storage": {
      "lng": -95.651111,
      "lat": 29.149166
    },
    "Prospero Solar": {
      "lng": -102.840399,
      "lat": 32.287873
    },
    "Prospero Solar II": {
      "lng": -102.844806,
      "lat": 32.275814
    },
    "PRSI FCC Generator": {
      "lng": -95.210278,
      "lat": 29.723333
    },
    "PYCO Industries, Inc. Wind Farm": {
      "lng": -101.792412,
      "lat": 33.553184
    },
    "Pyron Wind Farm LLC Hybrid": {
      "lng": -100.6728,
      "lat": 32.5886
    },
    "Quail Run Energy Center": {
      "lng": -102.315,
      "lat": 31.8414
    },
    "R W Miller": {
      "lng": -98.3103,
      "lat": 32.6581
    },
    "Rabbit Hill Energy Storage Project": {
      "lng": -97.687023,
      "lat": 30.587828
    },
    "Ralls Wind Farm": {
      "lng": -101.39,
      "lat": 33.661111
    },
    "Rambler": {
      "lng": -100.6,
      "lat": 31.517
    },
    "Ranchero Wind Farm LLC": {
      "lng": -101.453021,
      "lat": 30.594908
    },
    "Ranchtown": {
      "lng": -98.735948,
      "lat": 29.617885
    },
    "Rattlesnake Den": {
      "lng": -101.493407,
      "lat": 31.74325
    },
    "Rattlesnake Power, LLC": {
      "lng": -99.551356,
      "lat": 31.360331
    },
    "Ray Olinger": {
      "lng": -96.452481,
      "lat": 33.068055
    },
    "Raymond Wind Farm, LLC": {
      "lng": -97.780439,
      "lat": 26.364153
    },
    "RC Thomas Hydroelectric Project": {
      "lng": -95.01,
      "lat": 30.632222
    },
    "RE Maplewood": {
      "lng": -102.257753,
      "lat": 30.976638
    },
    "RE Roserock": {
      "lng": -103.306662,
      "lat": 30.960209
    },
    "Red Gate Power Plant": {
      "lng": -98.1775,
      "lat": 26.451111
    },
    "Reloj del Sol Wind Farm": {
      "lng": -99.388388,
      "lat": 27.03028
    },
    "Rentech Nitrogen Pasadena Cogeneration": {
      "lng": -95.194722,
      "lat": 29.739444
    },
    "Ribeye Wind (11) LLC": {
      "lng": -101.9789,
      "lat": 35.8119
    },
    "Rice University": {
      "lng": -95.39536,
      "lat": 29.721048
    },
    "Ridge Rd": {
      "lng": -98.09831,
      "lat": 26.16815
    },
    "Rio Bravo Windpower, LLC": {
      "lng": -99.0387,
      "lat": 26.649
    },
    "Rio Grande Valley Sugar Growers": {
      "lng": -97.867012,
      "lat": 26.269683
    },
    "Rio Nogales Power Project, LP": {
      "lng": -97.973215,
      "lat": 29.593056
    },
    "Roadrunner, LLC Hybrid": {
      "lng": -102.19222,
      "lat": 31.219444
    },
    "Robert D Willis": {
      "lng": -94.1756,
      "lat": 30.7975
    },
    "Robert Mueller Energy Center": {
      "lng": -97.707675,
      "lat": 30.304994
    },
    "Rocksprings": {
      "lng": -100.79431,
      "lat": 29.72753
    },
    "Roland C. Dansby Power Plant": {
      "lng": -96.4608,
      "lat": 30.7217
    },
    "Roscoe Wind Farm LLC": {
      "lng": -100.6664,
      "lat": 32.4694
    },
    "Route 66 Wind Plant": {
      "lng": -101.434167,
      "lat": 35.205278
    },
    "Sabine": {
      "lng": -93.878,
      "lat": 30.0242
    },
    "Sage Draw Wind": {
      "lng": -101.65722,
      "lat": 33.303497
    },
    "Salt Fork Wind Project, LLC": {
      "lng": -100.934408,
      "lat": 35.18172
    },
    "Sam Rayburn Dam": {
      "lng": -94.1062,
      "lat": 31.060893
    },
    "Sam Rayburn Plant": {
      "lng": -97.135,
      "lat": 28.8947
    },
    "Sam Seymour": {
      "lng": -96.7506,
      "lat": 29.9172
    },
    "San Jacinto County Peaking Facility": {
      "lng": -95.0118,
      "lat": 30.4193
    },
    "San Jacinto Steam Electric Station": {
      "lng": -95.040622,
      "lat": 29.694838
    },
    "San Miguel": {
      "lng": -98.4775,
      "lat": 28.7044
    },
    "San Roman Wind I, LLC": {
      "lng": -97.377599,
      "lat": 26.125979
    },
    "Sand Bluff Wind Farm": {
      "lng": -101.302637,
      "lat": 31.991442
    },
    "Sand Hill Energy Center": {
      "lng": -97.6129,
      "lat": 30.2098
    },
    "Sandy Creek Energy Station": {
      "lng": -96.957149,
      "lat": 31.474378
    },
    "Santa Rita East": {
      "lng": -101.110787,
      "lat": 31.207988
    },
    "Santa Rita Wind Energy": {
      "lng": -101.318787,
      "lat": 31.181377
    },
    "Santa Rosa": {
      "lng": -97.82349,
      "lat": 26.23055
    },
    "Scurry County Wind II": {
      "lng": -100.793333,
      "lat": 32.718056
    },
    "Scurry County Wind LP": {
      "lng": -100.9952,
      "lat": 32.7204
    },
    "Seadrift Coke LP": {
      "lng": -96.794154,
      "lat": 28.513929
    },
    "Security": {
      "lng": -95.2681,
      "lat": 30.3225
    },
    "Senate Wind LLC": {
      "lng": -98.365069,
      "lat": 33.205801
    },
    "Sendero": {
      "lng": -98.916667,
      "lat": 27.18
    },
    "Seymour Hills Wind Project, LLC": {
      "lng": -99.1921,
      "lat": 33.57824
    },
    "Shannon Wind": {
      "lng": -98.360833,
      "lat": 33.509722
    },
    "Shell Deer Park": {
      "lng": -95.128856,
      "lat": 29.723185
    },
    "Sherbino I Wind Farm": {
      "lng": -102.355628,
      "lat": 30.807272
    },
    "Sherbino II": {
      "lng": -102.505808,
      "lat": 30.772575
    },
    "Signal Hill Generating LLC": {
      "lng": -98.5894,
      "lat": 33.8625
    },
    "Silas Ray": {
      "lng": -97.5214,
      "lat": 25.9131
    },
    "Silver Star I Wind Power Project": {
      "lng": -98.476051,
      "lat": 32.338659
    },
    "Sim Gideon": {
      "lng": -97.2708,
      "lat": 30.1456
    },
    "Sky Global Power One": {
      "lng": -96.537778,
      "lat": 29.550278
    },
    "Snider Industries": {
      "lng": -94.373889,
      "lat": 32.574722
    },
    "Snyder Wind Farm": {
      "lng": -100.738949,
      "lat": 32.739294
    },
    "SolaireHolman Solar Project": {
      "lng": -103.4747,
      "lat": 30.45512
    },
    "South Houston Green Power Site": {
      "lng": -94.932773,
      "lat": 29.378097
    },
    "South Plains II": {
      "lng": -101.381494,
      "lat": 34.196962
    },
    "South Plains Wind Phase I": {
      "lng": -101.323611,
      "lat": 34.080556
    },
    "South Texas Project": {
      "lng": -96.0481,
      "lat": 28.795
    },
    "South Trent Wind Farm": {
      "lng": -100.1286,
      "lat": 32.4081
    },
    "Southwick": {
      "lng": -95.36578,
      "lat": 29.59576
    },
    "Spencer": {
      "lng": -97.1061,
      "lat": 33.1975
    },
    "Spinning Spur Wind II": {
      "lng": -102.682778,
      "lat": 35.276944
    },
    "Spinning Spur Wind III": {
      "lng": -102.721111,
      "lat": 35.295
    },
    "Spinning Spur Wind LLC": {
      "lng": -102.203056,
      "lat": 35.2375
    },
    "SRO Cogen Limited Partnership": {
      "lng": -93.7579,
      "lat": 30.0552
    },
    "Stanton Wind Energy LLC": {
      "lng": -101.8367,
      "lat": 32.2353
    },
    "State Farm Insur Support Center Central": {
      "lng": -96.9894,
      "lat": 32.9192
    },
    "Stella Wind Farm": {
      "lng": -97.6425,
      "lat": 26.941667
    },
    "Stephens Ranch Wind Energy LLC": {
      "lng": -101.647778,
      "lat": 32.926389
    },
    "Sterling Solar (TX)": {
      "lng": -96.172617,
      "lat": 33.166173
    },
    "Stonegate": {
      "lng": -95.875497,
      "lat": 29.785353
    },
    "Stryker Creek": {
      "lng": -94.989834,
      "lat": 31.93985
    },
    "SunE CPS1 LLC": {
      "lng": -98.418333,
      "lat": 29.245
    },
    "SunE CPS2 LLC": {
      "lng": -98.418333,
      "lat": 29.245
    },
    "SunE CPS3 LLC": {
      "lng": -98.669167,
      "lat": 29.221944
    },
    "Sunray Wind I": {
      "lng": -101.9005,
      "lat": 35.9454
    },
    "Sweeny": {
      "lng": -95.687237,
      "lat": 29.055615
    },
    "Sweeny Cogeneration Facility": {
      "lng": -95.745,
      "lat": 29.0728
    },
    "Sweetwater Wind 1 LLC": {
      "lng": -100.3389,
      "lat": 32.3606
    },
    "Sweetwater Wind 2 LLC": {
      "lng": -100.3703,
      "lat": 32.3472
    },
    "Sweetwater Wind 3 LLC": {
      "lng": -100.4219,
      "lat": 32.290833
    },
    "Sweetwater Wind 4 LLC": {
      "lng": -100.5303,
      "lat": 32.3122
    },
    "Sweetwater Wind 5": {
      "lng": -100.4833,
      "lat": 32.236944
    },
    "Swoose I": {
      "lng": -103.096242,
      "lat": 31.45115
    },
    "System Control Center": {
      "lng": -97.708808,
      "lat": 30.219946
    },
    "T C Ferguson Power Plant": {
      "lng": -98.3705,
      "lat": 30.558
    },
    "T H Wharton": {
      "lng": -95.5306,
      "lat": 29.9417
    },
    "Tahoka Wind": {
      "lng": -101.679188,
      "lat": 33.150904
    },
    "Taygete Energy Project LLC": {
      "lng": -103.125868,
      "lat": 31.165245
    },
    "T-Bone Wind (10) LLC": {
      "lng": -101.9986,
      "lat": 35.8081
    },
    "TECO CHP-1": {
      "lng": -95.398317,
      "lat": 29.703454
    },
    "Tejas Power Generation Unit 1": {
      "lng": -95.451516,
      "lat": 29.647267
    },
    "Tenaska Frontier Generating Station": {
      "lng": -95.9178,
      "lat": 30.5924
    },
    "Tenaska Gateway Generating Station": {
      "lng": -94.619743,
      "lat": 32.017826
    },
    "Tenderloin Wind (6) LLC": {
      "lng": -101.8222,
      "lat": 36.4669
    },
    "Tenet Hospital": {
      "lng": -106.5014,
      "lat": 31.7697
    },
    "Tessman Road": {
      "lng": -98.3433,
      "lat": 29.4289
    },
    "Texas City Cogeneration": {
      "lng": -94.943829,
      "lat": 29.378697
    },
    "Texas City Plant Union Carbide": {
      "lng": -94.946477,
      "lat": 29.37612
    },
    "Texas Petrochemicals": {
      "lng": -95.2547,
      "lat": 29.6981
    },
    "The Methodist Hospital, Gas Turbine": {
      "lng": -95.400278,
      "lat": 29.709444
    },
    "Ticona Polymers Inc": {
      "lng": -97.8228,
      "lat": 27.5697
    },
    "Titan Solar Project": {
      "lng": -104.458136,
      "lat": 31.70038
    },
    "Toledo Bend": {
      "lng": -93.565877,
      "lat": 31.173484
    },
    "Tolk Station": {
      "lng": -102.56999,
      "lat": 34.186494
    },
    "Topaz Generating": {
      "lng": -94.976667,
      "lat": 29.426389
    },
    "Torrecillas Wind Energy, LLC": {
      "lng": -98.7945,
      "lat": 27.614411
    },
    "Toyah Power Station": {
      "lng": -103.66551,
      "lat": 31.113909
    },
    "Toyota HQ Plan": {
      "lng": -96.84113,
      "lat": 33.084219
    },
    "TP 4": {
      "lng": -97.999587,
      "lat": 29.548136
    },
    "TPE Erath Solar, LLC": {
      "lng": -98.207541,
      "lat": 32.26246
    },
    "TPE Whitney Solar, LLC": {
      "lng": -97.35,
      "lat": 32.05
    },
    "Trent Wind Farm LP": {
      "lng": -100.235652,
      "lat": 32.4301
    },
    "Trinidad": {
      "lng": -96.101277,
      "lat": 32.124519
    },
    "Trinity Hills": {
      "lng": -98.711944,
      "lat": 33.383611
    },
    "Triple Butte I": {
      "lng": -103.06786,
      "lat": 30.75956
    },
    "Tri-Tip Wind (9) LLC": {
      "lng": -101.8033,
      "lat": 35.9614
    },
    "Tumbleweed": {
      "lng": -102.5577,
      "lat": 32.35123
    },
    "Turkey Track Wind Energy LLC": {
      "lng": -100.2686,
      "lat": 32.1981
    },
    "Twin Oaks": {
      "lng": -96.69503,
      "lat": 31.091925
    },
    "TX Hereford Wind": {
      "lng": -102.174444,
      "lat": 34.748056
    },
    "TX Jumbo Road Wind": {
      "lng": -102.253333,
      "lat": 34.72
    },
    "TX2 Port Lavaca": {
      "lng": -96.68376,
      "lat": 28.58673
    },
    "TX7 Flat Top": {
      "lng": -103.36067,
      "lat": 31.19791
    },
    "TX8 Worsham": {
      "lng": -103.33645,
      "lat": 31.35658
    },
    "Ty Cooke": {
      "lng": -101.7906,
      "lat": 33.5211
    },
    "Tyler Bluff Wind Project, LLC": {
      "lng": -97.372009,
      "lat": 33.703851
    },
    "UL Advanced Wind Turbine Test Facility": {
      "lng": -101.78412,
      "lat": 34.966012
    },
    "Union Carbide Seadrift Cogen": {
      "lng": -96.7706,
      "lat": 28.5105
    },
    "Upton County Solar": {
      "lng": -102.29027,
      "lat": 31.241808
    },
    "V H Braunig": {
      "lng": -98.384045,
      "lat": 29.257046
    },
    "Valero Refinery Corpus Christi West": {
      "lng": -97.4814,
      "lat": 27.8175
    },
    "Vaquero": {
      "lng": -97.654049,
      "lat": 26.168074
    },
    "Victoria City Peaking Facility": {
      "lng": -97.0093,
      "lat": 28.7853
    },
    "Victoria Port Peaking Facility": {
      "lng": -96.945409,
      "lat": 28.695805
    },
    "Victoria Power Station": {
      "lng": -97.01,
      "lat": 28.7883
    },
    "Victoria Texas Plant": {
      "lng": -96.956014,
      "lat": 28.675088
    },
    "Villa Cavasos": {
      "lng": -97.596284,
      "lat": 26.02131
    },
    "Village Creek Water Reclamation Facility": {
      "lng": -97.14209,
      "lat": 32.775093
    },
    "W A Parish": {
      "lng": -95.6311,
      "lat": 29.4828
    },
    "Wagyu": {
      "lng": -95.658144,
      "lat": 29.242304
    },
    "Wake Wind Energy Center": {
      "lng": -101.099722,
      "lat": 33.825
    },
    "WAL1040": {
      "lng": -95.651482,
      "lat": 29.877618
    },
    "WAL1062": {
      "lng": -95.159125,
      "lat": 29.551973
    },
    "WAL1103": {
      "lng": -95.486202,
      "lat": 29.998163
    },
    "WAL1232": {
      "lng": -97.457755,
      "lat": 31.080808
    },
    "WAL1272": {
      "lng": -100.91789,
      "lat": 32.691018
    },
    "WAL1455": {
      "lng": -97.169261,
      "lat": 32.764483
    },
    "WAL194": {
      "lng": -94.974458,
      "lat": 29.776274
    },
    "WAL202": {
      "lng": -96.800708,
      "lat": 33.100353
    },
    "WAL2257": {
      "lng": -95.509083,
      "lat": 29.853797
    },
    "WAL2439": {
      "lng": -94.900456,
      "lat": 29.817549
    },
    "WAL266": {
      "lng": -97.081986,
      "lat": 32.922049
    },
    "WAL2667": {
      "lng": -96.689543,
      "lat": 32.793829
    },
    "WAL2724": {
      "lng": -95.210408,
      "lat": 29.693424
    },
    "WAL284": {
      "lng": -97.130048,
      "lat": 32.576159
    },
    "WAL2883": {
      "lng": -96.798596,
      "lat": 33.097094
    },
    "WAL2980": {
      "lng": -97.286531,
      "lat": 32.89517
    },
    "WAL2993": {
      "lng": -95.644645,
      "lat": 29.613582
    },
    "WAL3226": {
      "lng": -95.804075,
      "lat": 29.783981
    },
    "WAL3284": {
      "lng": -97.209254,
      "lat": 32.66674
    },
    "WAL3285": {
      "lng": -96.946914,
      "lat": 32.59732
    },
    "WAL3297": {
      "lng": -95.602626,
      "lat": 29.920574
    },
    "WAL3298": {
      "lng": -95.018815,
      "lat": 29.535168
    },
    "WAL3302": {
      "lng": -95.5614,
      "lat": 29.679366
    },
    "WAL3320": {
      "lng": -98.315859,
      "lat": 26.260742
    },
    "WAL3390": {
      "lng": -95.589487,
      "lat": 30.190279
    },
    "WAL3425": {
      "lng": -95.237434,
      "lat": 29.630428
    },
    "WAL3432": {
      "lng": -96.879836,
      "lat": 32.592923
    },
    "WAL3500": {
      "lng": -95.1652,
      "lat": 29.80599
    },
    "WAL3510": {
      "lng": -95.235941,
      "lat": 29.545976
    },
    "WAL3518": {
      "lng": -99.471825,
      "lat": 27.612565
    },
    "WAL3572": {
      "lng": -95.382896,
      "lat": 29.557643
    },
    "WAL3584": {
      "lng": -95.46539,
      "lat": 29.72355
    },
    "WAL3631": {
      "lng": -97.338514,
      "lat": 32.576636
    },
    "WAL3773": {
      "lng": -97.483775,
      "lat": 32.765261
    },
    "WAL3777": {
      "lng": -96.883994,
      "lat": 33.179721
    },
    "WAL3827": {
      "lng": -95.774624,
      "lat": 29.690142
    },
    "WAL3886": {
      "lng": -98.204552,
      "lat": 26.267703
    },
    "WAL407": {
      "lng": -97.729279,
      "lat": 31.089722
    },
    "WAL414": {
      "lng": -98.499006,
      "lat": 33.857046
    },
    "WAL4298": {
      "lng": -95.25261,
      "lat": 29.936325
    },
    "WAL447": {
      "lng": -100.901074,
      "lat": 29.395822
    },
    "WAL4509": {
      "lng": -97.467729,
      "lat": 32.677829
    },
    "WAL4512": {
      "lng": -95.767961,
      "lat": 29.818196
    },
    "WAL452": {
      "lng": -98.245055,
      "lat": 26.243944
    },
    "WAL4526": {
      "lng": -95.377269,
      "lat": 29.829831
    },
    "WAL4538": {
      "lng": -95.72045,
      "lat": 29.803665
    },
    "WAL456": {
      "lng": -97.519445,
      "lat": 25.976057
    },
    "WAL461": {
      "lng": -100.482722,
      "lat": 28.702031
    },
    "WAL462": {
      "lng": -95.230784,
      "lat": 29.421764
    },
    "WAL5045": {
      "lng": -95.594987,
      "lat": 30.014278
    },
    "WAL5091": {
      "lng": -95.697345,
      "lat": 29.975997
    },
    "WAL5116": {
      "lng": -95.098302,
      "lat": 29.664286
    },
    "WAL5165": {
      "lng": -98.128306,
      "lat": 26.191785
    },
    "WAL5211": {
      "lng": -96.682687,
      "lat": 33.156306
    },
    "WAL522": {
      "lng": -95.06497,
      "lat": 29.894458
    },
    "WAL5287": {
      "lng": -95.51277,
      "lat": 30.077307
    },
    "WAL529": {
      "lng": -95.000453,
      "lat": 29.372305
    },
    "WAL5311": {
      "lng": -96.735704,
      "lat": 33.216844
    },
    "WAL5312": {
      "lng": -97.291803,
      "lat": 32.774643
    },
    "WAL5316": {
      "lng": -97.387586,
      "lat": 32.882111
    },
    "WAL536": {
      "lng": -99.698015,
      "lat": 32.48078
    },
    "WAL537": {
      "lng": -102.332512,
      "lat": 31.8957
    },
    "WAL5388": {
      "lng": -95.089643,
      "lat": 29.470152
    },
    "WAL546": {
      "lng": -95.761271,
      "lat": 29.558105
    },
    "WAL5479": {
      "lng": -97.606035,
      "lat": 30.453115
    },
    "WAL5480": {
      "lng": -97.612014,
      "lat": 30.53366
    },
    "WAL5612": {
      "lng": -95.313027,
      "lat": 29.716069
    },
    "WAL5713": {
      "lng": -99.462215,
      "lat": 27.610637
    },
    "WAL5764": {
      "lng": -97.411976,
      "lat": 32.967341
    },
    "WAL602": {
      "lng": -95.45769,
      "lat": 30.12423
    },
    "WAL6929": {
      "lng": -97.414441,
      "lat": 31.11497
    },
    "WAL744": {
      "lng": -95.176468,
      "lat": 29.995621
    },
    "WAL752": {
      "lng": -95.161425,
      "lat": 29.64662
    },
    "WAL768": {
      "lng": -95.722232,
      "lat": 29.789117
    },
    "WAL772": {
      "lng": -95.646224,
      "lat": 29.721093
    },
    "WAL791": {
      "lng": -98.043187,
      "lat": 27.769271
    },
    "WAL849": {
      "lng": -95.429453,
      "lat": 30.064779
    },
    "WAL872": {
      "lng": -95.283899,
      "lat": 29.573168
    },
    "WAL896": {
      "lng": -97.036234,
      "lat": 32.673532
    },
    "WAL897": {
      "lng": -102.907161,
      "lat": 30.896466
    },
    "WAL940": {
      "lng": -97.433843,
      "lat": 32.755036
    },
    "WAL947": {
      "lng": -96.612131,
      "lat": 33.673162
    },
    "WAL972": {
      "lng": -97.424739,
      "lat": 32.811848
    },
    "Walnut Springs Solar": {
      "lng": -97.768464,
      "lat": 32.07868
    },
    "Wasson CO2 Removal Plant": {
      "lng": -102.756945,
      "lat": 33.009944
    },
    "Webberville Solar Project": {
      "lng": -97.508812,
      "lat": 30.238492
    },
    "Welsh Power Plant": {
      "lng": -94.839993,
      "lat": 33.05522
    },
    "West Moore Solar II": {
      "lng": -96.627,
      "lat": 33.619
    },
    "West of the Pecos Solar": {
      "lng": -103.7289,
      "lat": 31.4308
    },
    "West Raymond Wind Farm LLC": {
      "lng": -97.993045,
      "lat": 26.376651
    },
    "Westhollow Technology Center": {
      "lng": -95.638249,
      "lat": 29.726017
    },
    "Westover": {
      "lng": -102.4461,
      "lat": 31.8362
    },
    "WestRock (TX)": {
      "lng": -94.0656,
      "lat": 30.3419
    },
    "Westside Landfill Gas Recovery": {
      "lng": -97.537118,
      "lat": 32.724553
    },
    "Whirlwind Energy Center": {
      "lng": -101.0986,
      "lat": 34.105
    },
    "White Mesa Wind": {
      "lng": -101.263,
      "lat": 30.91
    },
    "Whitesboro Solar": {
      "lng": -96.868721,
      "lat": 33.616526
    },
    "Whitesboro Solar II": {
      "lng": -96.867474,
      "lat": 33.624853
    },
    "Whitetail": {
      "lng": -99.005278,
      "lat": 27.488056
    },
    "Whitewright Solar": {
      "lng": -96.378894,
      "lat": 33.498186
    },
    "Whitney": {
      "lng": -97.3667,
      "lat": 31.8694
    },
    "Wildcat Creek Wind Farm LLC": {
      "lng": -97.32958,
      "lat": 33.54915
    },
    "Wildcat Ranch Wind Project": {
      "lng": -102.9619,
      "lat": 33.5296
    },
    "Wildorado Wind LLC": {
      "lng": -102.303056,
      "lat": 35.291944
    },
    "Wilkes Power Plant": {
      "lng": -94.547986,
      "lat": 32.848163
    },
    "Willow Springs Wind Farm": {
      "lng": -99.624257,
      "lat": 33.345366
    },
    "Winchester Power Park": {
      "lng": -96.9875,
      "lat": 30.0381
    },
    "Windthorst-2": {
      "lng": -98.5,
      "lat": 33.495278
    },
    "Wise County Power Company, LLC": {
      "lng": -97.9103,
      "lat": 33.0583
    },
    "Wolf Hollow Generating Station": {
      "lng": -97.731686,
      "lat": 32.33422
    },
    "Wolf Hollow II": {
      "lng": -97.734612,
      "lat": 32.337679
    },
    "Wolf Ridge Wind": {
      "lng": -97.428406,
      "lat": 33.733006
    },
    "Woodall Gas Plant": {
      "lng": -100.240278,
      "lat": 35.690833
    },
    "Woodville Renewable Power Project": {
      "lng": -94.4375,
      "lat": 30.748333
    },
    "Woodward Mountain I": {
      "lng": -102.414067,
      "lat": 30.9514
    },
    "Woodward Mountain II": {
      "lng": -102.414067,
      "lat": 30.9514
    },
    "Works 4": {
      "lng": -98.553539,
      "lat": 34.003489
    },
    "Yellow Jacket": {
      "lng": -97.653747,
      "lat": 31.933841
    },
    "Bartholomew": {
      "lng": -111.499461,
      "lat": 40.167136
    },
    "Beryl Solar Plant": {
      "lng": -113.645833,
      "lat": 37.639167
    },
    "Bloomington Power Plant": {
      "lng": -113.626736,
      "lat": 37.041997
    },
    "Bloomington Solar I": {
      "lng": -113.627523,
      "lat": 37.043828
    },
    "Blue Mountain Biogas": {
      "lng": -113.297222,
      "lat": 38.173889
    },
    "Blundell": {
      "lng": -112.8533,
      "lat": 38.4889
    },
    "Bonanza": {
      "lng": -109.2844,
      "lat": 40.0864
    },
    "Boulder": {
      "lng": -111.4383,
      "lat": 37.9858
    },
    "Bountiful City": {
      "lng": -111.885332,
      "lat": 40.886867
    },
    "Brigham City": {
      "lng": -111.99345,
      "lat": 41.501619
    },
    "Buckhorn Solar Plant": {
      "lng": -112.731667,
      "lat": 38.023333
    },
    "BYU Central Heating Plant": {
      "lng": -111.6462,
      "lat": 40.24724
    },
    "Causey": {
      "lng": -111.588849,
      "lat": 41.297229
    },
    "Cedar Valley Solar Plant": {
      "lng": -113.09,
      "lat": 37.807778
    },
    "Central Energy Plant USU": {
      "lng": -111.811667,
      "lat": 41.748611
    },
    "Clover Creek Solar(Community Solar)": {
      "lng": -111.887656,
      "lat": 39.840652
    },
    "Cove Mountain Solar": {
      "lng": -113.619219,
      "lat": 37.623123
    },
    "Cove Mountain Solar 2": {
      "lng": -113.637097,
      "lat": 37.638602
    },
    "Currant Creek Power Project": {
      "lng": -111.89345,
      "lat": 39.82144
    },
    "Cutler Hydro": {
      "lng": -112.052114,
      "lat": 41.834702
    },
    "Deer Creek (UT)": {
      "lng": -111.528446,
      "lat": 40.404354
    },
    "Echo Dam": {
      "lng": -111.433937,
      "lat": 40.964828
    },
    "Enel Cove Fort": {
      "lng": -112.581111,
      "lat": 38.560278
    },
    "Enterprise Solar, LLC": {
      "lng": -113.6125,
      "lat": 37.641111
    },
    "Escalante Solar I, LLC": {
      "lng": -113.033889,
      "lat": 38.524167
    },
    "Escalante Solar II, LLC": {
      "lng": -113.03,
      "lat": 38.500833
    },
    "Escalante Solar III, LLC": {
      "lng": -112.986944,
      "lat": 38.497778
    },
    "Fiddler's Canyon #1": {
      "lng": -113.253724,
      "lat": 37.750268
    },
    "Fiddler's Canyon #2": {
      "lng": -113.283666,
      "lat": 37.794678
    },
    "Fiddler's Canyon 3": {
      "lng": -113.216634,
      "lat": 37.731338
    },
    "Flaming Gorge": {
      "lng": -109.421662,
      "lat": 40.914646
    },
    "Gadsby": {
      "lng": -111.9289,
      "lat": 40.7686
    },
    "Gateway": {
      "lng": -111.831441,
      "lat": 41.136547
    },
    "Granite": {
      "lng": -111.78208,
      "lat": 40.619173
    },
    "Granite Mountain Solar East, LLC": {
      "lng": -113.226403,
      "lat": 37.77509
    },
    "Granite Mountain Solar West, LLC": {
      "lng": -113.322,
      "lat": 37.7989
    },
    "Granite Peak Solar Plant": {
      "lng": -112.988889,
      "lat": 38.402778
    },
    "Greenville Solar Plant": {
      "lng": -112.735833,
      "lat": 38.256111
    },
    "Heber City": {
      "lng": -111.425246,
      "lat": 40.50368
    },
    "Hill AFB LFG Facility, Bldg #737": {
      "lng": -111.964147,
      "lat": 41.12902
    },
    "HTW Plant 303 COGEN": {
      "lng": -111.841667,
      "lat": 40.763056
    },
    "Hunter": {
      "lng": -111.0289,
      "lat": 39.1747
    },
    "Hunter Solar LLC (UT)": {
      "lng": -111.051111,
      "lat": 39.142217
    },
    "Huntington": {
      "lng": -111.0781,
      "lat": 39.3792
    },
    "Hurricane City Power": {
      "lng": -113.2975,
      "lat": 37.185556
    },
    "Hydro II": {
      "lng": -111.783169,
      "lat": 41.743728
    },
    "Hydro III": {
      "lng": -111.746023,
      "lat": 41.746364
    },
    "Hydro Plant No 3": {
      "lng": -111.334381,
      "lat": 39.205623
    },
    "Intermountain": {
      "lng": -112.58018,
      "lat": 39.509731
    },
    "Iron Springs Solar, LLC": {
      "lng": -113.1516,
      "lat": 37.7212
    },
    "Jordanelle Dam Hydroelectric Project": {
      "lng": -111.423589,
      "lat": 40.596605
    },
    "Kennecott Power Plant": {
      "lng": -112.1225,
      "lat": 40.7119
    },
    "Lagoon Cogeneration Facility": {
      "lng": -111.8983,
      "lat": 40.9872
    },
    "Laho Solar Plant": {
      "lng": -113.035556,
      "lat": 38.291389
    },
    "Lake Creek Dam": {
      "lng": -111.312506,
      "lat": 40.493631
    },
    "Lake Side Power Plant": {
      "lng": -111.754167,
      "lat": 40.331667
    },
    "Latigo Wind Park": {
      "lng": -109.36839,
      "lat": 37.886706
    },
    "Little Cottonwood": {
      "lng": -111.800605,
      "lat": 40.584985
    },
    "Logan City": {
      "lng": -111.8433,
      "lat": 41.725833
    },
    "Manti Lower": {
      "lng": -111.621491,
      "lat": 39.255911
    },
    "Manti Upper": {
      "lng": -111.621491,
      "lat": 39.255911
    },
    "Milford 2": {
      "lng": -112.990812,
      "lat": 38.388243
    },
    "Milford Flat Solar Plant": {
      "lng": -113.008333,
      "lat": 38.291389
    },
    "Milford Solar 1": {
      "lng": -113.018283,
      "lat": 38.490667
    },
    "Milford Wind Corridor I LLC": {
      "lng": -112.935,
      "lat": 38.53569
    },
    "Milford Wind Corridor Stage II LLC": {
      "lng": -112.931,
      "lat": 38.58523
    },
    "Millcreek Power": {
      "lng": -113.516581,
      "lat": 37.112092
    },
    "Murray Turbine": {
      "lng": -111.89,
      "lat": 40.67
    },
    "Nebo Power Station": {
      "lng": -111.7294,
      "lat": 40.0614
    },
    "Pavant Solar II LLC": {
      "lng": -112.343,
      "lat": 39.1625
    },
    "Pavant Solar III": {
      "lng": -112.359849,
      "lat": 39.154474
    },
    "Pavant Solar, LLC": {
      "lng": -112.36327,
      "lat": 38.960519
    },
    "Payson": {
      "lng": -111.73023,
      "lat": 40.06084
    },
    "Pine View Dam": {
      "lng": -111.848525,
      "lat": 41.254331
    },
    "Pioneer": {
      "lng": -111.946478,
      "lat": 41.243643
    },
    "Provo Power Plant": {
      "lng": -111.66298,
      "lat": 40.2427
    },
    "Quail Creek Hydro Plant #1": {
      "lng": -113.3583,
      "lat": 37.1854
    },
    "Quichapa 1": {
      "lng": -113.216679,
      "lat": 37.658223
    },
    "Quichapa 2": {
      "lng": -113.188953,
      "lat": 37.666772
    },
    "Quichapa 3": {
      "lng": -113.236928,
      "lat": 37.649816
    },
    "Sage Solar I-III": {
      "lng": -111.06,
      "lat": 41.77
    },
    "Salt Lake Energy Systems": {
      "lng": -112.0339,
      "lat": 40.7461
    },
    "Salt Palace Solar Gen Plant": {
      "lng": -111.895,
      "lat": 40.767222
    },
    "Sigurd Solar LLC": {
      "lng": -112.005212,
      "lat": 38.850544
    },
    "Snake Creek": {
      "lng": -111.502911,
      "lat": 40.544942
    },
    "Snowbird Power Plant": {
      "lng": -111.653903,
      "lat": 40.58337
    },
    "South Milford Solar Plant": {
      "lng": -113.053611,
      "lat": 38.311111
    },
    "Spanish Fork": {
      "lng": -111.604846,
      "lat": 40.080042
    },
    "Spanish Fork Wind Park 2 LLC": {
      "lng": -111.582222,
      "lat": 40.071389
    },
    "St George Red Rock": {
      "lng": -113.568944,
      "lat": 37.113736
    },
    "Stairs": {
      "lng": -111.7533,
      "lat": 40.6236
    },
    "Sunnyside Cogeneration Associates": {
      "lng": -110.3917,
      "lat": 39.5472
    },
    "Tesoro SLC Cogeneration Plant": {
      "lng": -111.9038,
      "lat": 40.7931
    },
    "Thermo No 1": {
      "lng": -113.194462,
      "lat": 38.160653
    },
    "Three Peaks Power": {
      "lng": -113.137082,
      "lat": 37.827082
    },
    "Tooele Army Depot(CSG)": {
      "lng": -112.37059,
      "lat": 40.50303
    },
    "Trans-Jordan Generating Station": {
      "lng": -112.0631,
      "lat": 40.55
    },
    "Uintah": {
      "lng": -110.066111,
      "lat": 40.538056
    },
    "Unit 4": {
      "lng": -111.418727,
      "lat": 39.544108
    },
    "Upper Beaver": {
      "lng": -112.4806,
      "lat": 38.2683
    },
    "US Magnesium": {
      "lng": -112.733889,
      "lat": 40.913333
    },
    "Utah Red Hills Renewable Energy Park": {
      "lng": -112.904167,
      "lat": 37.882778
    },
    "Veyo Heat Recovery Project": {
      "lng": -113.765,
      "lat": 37.348
    },
    "Wanship": {
      "lng": -111.4043,
      "lat": 40.7904
    },
    "Washington City Electric Generation": {
      "lng": -113.439167,
      "lat": 37.155
    },
    "Weber": {
      "lng": -111.884948,
      "lat": 41.137621
    },
    "Weber State University - Davis Campus PV": {
      "lng": -111.982167,
      "lat": 41.098957
    },
    "West Valley Power Plant": {
      "lng": -112.0317,
      "lat": 40.6667
    },
    "Whitehead": {
      "lng": -111.6203,
      "lat": 40.1817
    },
    "Altavista Power Station": {
      "lng": -79.273551,
      "lat": 37.118674
    },
    "Altavista Solar": {
      "lng": -79.347497,
      "lat": 37.148199
    },
    "Amelia": {
      "lng": -78.0589,
      "lat": 37.3097
    },
    "Ameresco Stafford": {
      "lng": -77.413056,
      "lat": 38.384167
    },
    "Bath County": {
      "lng": -79.8,
      "lat": 38.20889
    },
    "Bayview": {
      "lng": -75.9682,
      "lat": 37.2717
    },
    "Bear Garden Generating Station": {
      "lng": -78.285278,
      "lat": 37.695556
    },
    "Bedford Solar": {
      "lng": -76.1647,
      "lat": 36.7
    },
    "Belle Haven Diesel Generation Facility": {
      "lng": -75.790823,
      "lat": 37.54864
    },
    "Birchwood Power Facility": {
      "lng": -77.3147,
      "lat": 38.2667
    },
    "Bluestone Solar": {
      "lng": -78.483909,
      "lat": 36.811533
    },
    "Boydton Plank Road Cogen Plant": {
      "lng": -77.467185,
      "lat": 37.193584
    },
    "Brasfield": {
      "lng": -77.524504,
      "lat": 37.220833
    },
    "Briel Solar Farm": {
      "lng": -77.26358,
      "lat": 37.52364
    },
    "Bristol Plant": {
      "lng": -82.143089,
      "lat": 36.60194
    },
    "Brunswick County Power Station": {
      "lng": -77.713889,
      "lat": 36.765
    },
    "Brunswick Landfill Gas": {
      "lng": -77.8108,
      "lat": 36.7639
    },
    "Buchanan Units 1 & 2": {
      "lng": -81.961388,
      "lat": 37.175278
    },
    "Buck Hydro": {
      "lng": -80.938714,
      "lat": 36.808229
    },
    "Buckingham Solar LLC": {
      "lng": -78.379237,
      "lat": 37.502944
    },
    "Byllesby 2": {
      "lng": -80.9333,
      "lat": 36.7858
    },
    "Caden Energix Hickory LLC": {
      "lng": -76.194266,
      "lat": 36.624113
    },
    "Caden Energix Pamplin LLC": {
      "lng": -78.76591,
      "lat": 37.32936
    },
    "Caden Energix Rives Road LLC": {
      "lng": -77.337645,
      "lat": 37.177825
    },
    "Celanese Acetate LLC": {
      "lng": -80.765,
      "lat": 37.3439
    },
    "Charles City": {
      "lng": -77.128611,
      "lat": 37.438889
    },
    "Chesapeake": {
      "lng": -76.3019,
      "lat": 36.7711
    },
    "Chesterfield Landfill Gas": {
      "lng": -77.4969,
      "lat": 37.3517
    },
    "Chesterfield Power Station": {
      "lng": -77.3833,
      "lat": 37.3822
    },
    "Church Street Plant": {
      "lng": -77.462918,
      "lat": 38.752347
    },
    "Claytor": {
      "lng": -80.5847,
      "lat": 37.075
    },
    "Clinch River": {
      "lng": -82.1997,
      "lat": 36.9333
    },
    "Clover Power Station": {
      "lng": -78.704,
      "lat": 36.869
    },
    "Coastal Virginia Offshore Wind (CVOW) pilot project": {
      "lng": -75.4916,
      "lat": 36.89155
    },
    "Coleman Falls": {
      "lng": -79.3006,
      "lat": 37.5035
    },
    "Colonial Trail West": {
      "lng": -76.902222,
      "lat": 37.138889
    },
    "Commonwealth Chesapeake": {
      "lng": -75.54,
      "lat": 37.9892
    },
    "Correctional Solar LLC": {
      "lng": -76.844612,
      "lat": 37.471464
    },
    "Covanta Alexandria/Arlington Energy": {
      "lng": -77.1288,
      "lat": 38.8014
    },
    "Covanta Fairfax Energy": {
      "lng": -77.2411,
      "lat": 38.695
    },
    "Cushaw": {
      "lng": -79.380823,
      "lat": 37.592887
    },
    "Danville": {
      "lng": -79.29906,
      "lat": 36.660591
    },
    "Danville Farm, LLC": {
      "lng": -79.302,
      "lat": 36.591
    },
    "Danville Kentuck Road Plant": {
      "lng": -79.2906,
      "lat": 36.6533
    },
    "Danville New Design Plant": {
      "lng": -79.3894,
      "lat": 36.6308
    },
    "Darbytown Combustion Turbine": {
      "lng": -77.368544,
      "lat": 37.498223
    },
    "Desper Solar": {
      "lng": -78.043131,
      "lat": 38.015735
    },
    "DG AMP Solar Front Royal": {
      "lng": -78.18,
      "lat": 38.929
    },
    "Dominion/Lo Mar": {
      "lng": -77.4974,
      "lat": 38.7443
    },
    "Doswell Limited Partnership": {
      "lng": -77.446246,
      "lat": 37.820805
    },
    "Downtown": {
      "lng": -76.921944,
      "lat": 36.680278
    },
    "Eastern Shore Solar, LLC": {
      "lng": -75.569556,
      "lat": 37.943895
    },
    "Electric Avenue Facility": {
      "lng": -77.59,
      "lat": 38.28
    },
    "Elizabeth River Combustion Turbine Sta": {
      "lng": -76.3083,
      "lat": 36.775
    },
    "Elkton": {
      "lng": -78.6528,
      "lat": 38.3836
    },
    "Emporia": {
      "lng": -77.56,
      "lat": 36.696
    },
    "Energix Buckingham, LLC": {
      "lng": -78.381,
      "lat": 37.5064
    },
    "Energix Hollyfield, LLC": {
      "lng": -77.17,
      "lat": 37.671
    },
    "Energix Leatherwood, LLC": {
      "lng": -79.709472,
      "lat": 36.687071
    },
    "Essex Solar Center": {
      "lng": -76.799967,
      "lat": 37.834094
    },
    "Frederick County LFGTE Facility": {
      "lng": -78.0975,
      "lat": 39.140833
    },
    "Fries Hydroelectric Project": {
      "lng": -80.985621,
      "lat": 36.715103
    },
    "Gardy's Mill Solar": {
      "lng": -76.606956,
      "lat": 38.006741
    },
    "Gateway Gen": {
      "lng": -77.509722,
      "lat": 38.725278
    },
    "Gloucester Solar": {
      "lng": -76.45,
      "lat": 37.45
    },
    "Godwin Drive Plant": {
      "lng": -77.508061,
      "lat": 38.74054
    },
    "Gordonsville Power Station": {
      "lng": -78.203543,
      "lat": 38.124688
    },
    "GP Big Island, LLC": {
      "lng": -79.357,
      "lat": 37.534
    },
    "Grasshopper Solar": {
      "lng": -78.45,
      "lat": 36.85
    },
    "Gravel Neck Combustion Turbine": {
      "lng": -76.6911,
      "lat": 37.1575
    },
    "Greensville County Power Station": {
      "lng": -77.649559,
      "lat": 36.718236
    },
    "Greensville County Solar Project, LLC": {
      "lng": -77.57,
      "lat": 36.64
    },
    "Halifax County Biomass": {
      "lng": -78.873333,
      "lat": 36.701389
    },
    "Hecate Energy Cherrydale LLC": {
      "lng": -75.917171,
      "lat": 37.37
    },
    "Hecate Energy Clarke County LLC": {
      "lng": -78.138207,
      "lat": 39.06485
    },
    "Henrico": {
      "lng": -77.5678,
      "lat": 37.6788
    },
    "Highlander Solar Energy Station 1": {
      "lng": -77.786223,
      "lat": 38.247123
    },
    "Holcomb Rock": {
      "lng": -79.3006,
      "lat": 37.5035
    },
    "Hollyfield": {
      "lng": -77.170439,
      "lat": 37.672005
    },
    "Hopewell Cogeneration Facility": {
      "lng": -77.281111,
      "lat": 37.291667
    },
    "Hopewell Mill": {
      "lng": -77.2816,
      "lat": 37.2914
    },
    "Hopewell Power Station": {
      "lng": -77.282982,
      "lat": 37.297834
    },
    "HP Hood CT": {
      "lng": -78.187474,
      "lat": 39.132519
    },
    "I 95 Landfill Phase II": {
      "lng": -77.2375,
      "lat": 38.6892
    },
    "IKEA Norfolk Rooftop PV System": {
      "lng": -76.200935,
      "lat": 36.875877
    },
    "International Paper Franklin Mill": {
      "lng": -76.9128,
      "lat": 36.6803
    },
    "John H Kerr": {
      "lng": -78.3005,
      "lat": 36.59943
    },
    "King & Queen": {
      "lng": -76.76,
      "lat": 37.6711
    },
    "Ladysmith Combustion Turbine Sta": {
      "lng": -77.5133,
      "lat": 38.0722
    },
    "Leesville": {
      "lng": -79.4025,
      "lat": 37.0933
    },
    "Louisa Generation Facility": {
      "lng": -78.2139,
      "lat": 38.1181
    },
    "Low Moor": {
      "lng": -79.892,
      "lat": 37.777
    },
    "Luray Hydro Station": {
      "lng": -78.498888,
      "lat": 38.676667
    },
    "Marsh Run Generation Facility": {
      "lng": -77.7681,
      "lat": 38.5283
    },
    "Martin Solar Center": {
      "lng": -78.049191,
      "lat": 37.88051
    },
    "Martinsville": {
      "lng": -79.883588,
      "lat": 36.664145
    },
    "Martinsville LFG Generator": {
      "lng": -79.845278,
      "lat": 36.717222
    },
    "Mas Suffolk RNG, LLC": {
      "lng": -76.4961,
      "lat": 36.7589
    },
    "Mechanicsville Solar": {
      "lng": -77.207725,
      "lat": 37.665975
    },
    "Merck": {
      "lng": -78.643688,
      "lat": 38.383222
    },
    "MM Prince William Energy": {
      "lng": -77.414722,
      "lat": 38.638333
    },
    "Molson Coors Shenandoah Brewery": {
      "lng": -78.676667,
      "lat": 38.356944
    },
    "Monterey Diesel Generation Facility": {
      "lng": -79.591389,
      "lat": 38.421389
    },
    "Montross Solar": {
      "lng": -76.786,
      "lat": 38.077
    },
    "Mount Clinton": {
      "lng": -78.865,
      "lat": 38.4681
    },
    "Mt. Jackson Solar": {
      "lng": -78.660158,
      "lat": 38.739621
    },
    "NASA Wallops Flight Facility Solar": {
      "lng": -75.473611,
      "lat": 37.938056
    },
    "New River": {
      "lng": -80.699,
      "lat": 37.183
    },
    "Newport Hydro Station": {
      "lng": -78.593611,
      "lat": 38.571389
    },
    "Niagara": {
      "lng": -79.8756,
      "lat": 37.2544
    },
    "North Anna": {
      "lng": -77.7897,
      "lat": 38.06
    },
    "Northern Neck": {
      "lng": -76.711,
      "lat": 37.95
    },
    "Nottoway Diesel Generating Facility": {
      "lng": -77.105,
      "lat": 36.697222
    },
    "Oceana Solar": {
      "lng": -76.051419,
      "lat": 36.789753
    },
    "Onancock Diesel Generation Facility": {
      "lng": -75.720487,
      "lat": 37.74542
    },
    "Orange Street G1": {
      "lng": -79.51109,
      "lat": 37.33206
    },
    "Palmer Solar Center": {
      "lng": -78.230963,
      "lat": 37.96724
    },
    "Park 500 Philip Morris USA": {
      "lng": -77.2806,
      "lat": 37.3389
    },
    "Perdue Diesel Generation Facility": {
      "lng": -75.655979,
      "lat": 37.741967
    },
    "Philip Morris": {
      "lng": -77.287027,
      "lat": 37.341838
    },
    "Philpott Lake": {
      "lng": -80.0281,
      "lat": 36.7803
    },
    "Pinnacles": {
      "lng": -80.447897,
      "lat": 36.666866
    },
    "Pleasant Valley (VA)": {
      "lng": -78.8983,
      "lat": 38.4006
    },
    "Pleinmont Solar 1": {
      "lng": -77.786223,
      "lat": 38.247123
    },
    "Pleinmont Solar 2": {
      "lng": -77.786223,
      "lat": 38.247123
    },
    "Ponton Diesel Generating Facility": {
      "lng": -77.9862,
      "lat": 37.2953
    },
    "Possum Point Power Station": {
      "lng": -77.2806,
      "lat": 38.5367
    },
    "Potomac Energy Center, LLC": {
      "lng": -77.545,
      "lat": 39.058056
    },
    "Pretlow": {
      "lng": -76.926667,
      "lat": 36.659167
    },
    "Puller Solar": {
      "lng": -76.46734,
      "lat": 37.57245
    },
    "Radford": {
      "lng": -80.572726,
      "lat": 37.078366
    },
    "Radford Army Ammunition Plant": {
      "lng": -80.542767,
      "lat": 37.180441
    },
    "Rappahannock Solar, LLC": {
      "lng": -76.383228,
      "lat": 37.657039
    },
    "Remington Combustion Turbine Station": {
      "lng": -77.7714,
      "lat": 38.5442
    },
    "Remington Solar Facility": {
      "lng": -77.776389,
      "lat": 38.548333
    },
    "Reusens": {
      "lng": -79.1856,
      "lat": 37.4639
    },
    "Richmond Energy": {
      "lng": -77.373889,
      "lat": 37.505833
    },
    "Richmond Spider Solar": {
      "lng": -77.786223,
      "lat": 38.247123
    },
    "Rochambeau Solar": {
      "lng": -76.76,
      "lat": 37.35
    },
    "RockTenn West Point Mill": {
      "lng": -76.8053,
      "lat": 37.5392
    },
    "Rockville 1 & 2": {
      "lng": -77.6639,
      "lat": 37.7036
    },
    "Sadler Solar": {
      "lng": -77.558,
      "lat": 36.6867
    },
    "Salem Electric Department": {
      "lng": -80.07,
      "lat": 37.2894
    },
    "Salem Street Dept": {
      "lng": -80.0447,
      "lat": 37.2794
    },
    "Salem Water Plant": {
      "lng": -80.0771,
      "lat": 37.2875
    },
    "Schoolfield Dam": {
      "lng": -79.432503,
      "lat": 36.576778
    },
    "Scott Solar Farm": {
      "lng": -77.934,
      "lat": 37.523
    },
    "Scott-II Solar LLC": {
      "lng": -77.944637,
      "lat": 37.526244
    },
    "Smith Mountain": {
      "lng": -79.5356,
      "lat": 37.0413
    },
    "Snowden": {
      "lng": -79.3715,
      "lat": 37.5736
    },
    "Southampton Power Station": {
      "lng": -76.9953,
      "lat": 36.6525
    },
    "Southampton Solar, LLC": {
      "lng": -77.166,
      "lat": 36.614
    },
    "Spring Grove I": {
      "lng": -76.919245,
      "lat": 37.173916
    },
    "Spruance Genco, LLC": {
      "lng": -77.4308,
      "lat": 37.4556
    },
    "Surry": {
      "lng": -76.6986,
      "lat": 37.1661
    },
    "Sussex Drive, LLC": {
      "lng": -77.41776,
      "lat": 36.940529
    },
    "Tangier": {
      "lng": -75.9915,
      "lat": 37.8277
    },
    "Tasley Energy Center": {
      "lng": -75.7031,
      "lat": 37.7061
    },
    "Tenaska Virginia Generating Station": {
      "lng": -78.3813,
      "lat": 37.8667
    },
    "TWE Myrtle Solar Farm, LLC": {
      "lng": -76.67,
      "lat": 36.79
    },
    "Twittys Creek Solar, LLC": {
      "lng": -78.581272,
      "lat": 37.018427
    },
    "Virginia Beach": {
      "lng": -76.2019,
      "lat": 36.7858
    },
    "Virginia City Hybrid Energy Center": {
      "lng": -82.338055,
      "lat": 36.916389
    },
    "Virginia Tech Power Plant": {
      "lng": -80.4211,
      "lat": 37.2319
    },
    "VMEA 1 Credit Gen": {
      "lng": -77.508061,
      "lat": 38.74054
    },
    "VMEA Peaking Gen": {
      "lng": -77.508061,
      "lat": 38.74054
    },
    "Warren County Power Station": {
      "lng": -78.177222,
      "lat": 38.971667
    },
    "Waste Management Bethel LFGTE": {
      "lng": -76.444373,
      "lat": 37.076352
    },
    "Waste Management King George LFGTE": {
      "lng": -77.3081,
      "lat": 38.2747
    },
    "Waste Management Middle Peninsula LFGTE": {
      "lng": -76.624506,
      "lat": 37.519714
    },
    "Water Strider Solar": {
      "lng": -79.027061,
      "lat": 37.016375
    },
    "Water Treatment Plant Generators": {
      "lng": -77.62089,
      "lat": 38.760695
    },
    "Western Branch High School": {
      "lng": -76.407143,
      "lat": 36.848105
    },
    "Westmoreland County Solar Project": {
      "lng": -76.744034,
      "lat": 38.007488
    },
    "WestRock Virginia Corp Covington Ops": {
      "lng": -79.9947,
      "lat": 37.799722
    },
    "Wheelabrator Portsmouth": {
      "lng": -76.3036,
      "lat": 36.8083
    },
    "Whitehorn Solar": {
      "lng": -79.358743,
      "lat": 36.931378
    },
    "Whitehouse Solar Farm": {
      "lng": -77.972,
      "lat": 38.023
    },
    "Wolf Hills Energy": {
      "lng": -82.1044,
      "lat": 36.6644
    },
    "Woodland Solar Farm": {
      "lng": -76.611,
      "lat": 36.89
    },
    "Yorktown Power Station": {
      "lng": -76.4611,
      "lat": 37.2144
    },
    "158th Fighter Wing Solar Farm": {
      "lng": -73.153412,
      "lat": 44.477705
    },
    "Ascutney": {
      "lng": -72.4203,
      "lat": 43.4011
    },
    "Ball Mountain Hydro": {
      "lng": -72.776111,
      "lat": 43.126944
    },
    "Barton Solar Farm": {
      "lng": -72.184044,
      "lat": 44.725418
    },
    "Battle Creek Solar": {
      "lng": -73.2142,
      "lat": 42.9133
    },
    "Beldens": {
      "lng": -73.1762,
      "lat": 44.0525
    },
    "Bellows Falls": {
      "lng": -72.4464,
      "lat": 43.1375
    },
    "Berlin 5": {
      "lng": -72.6027,
      "lat": 44.251
    },
    "Billings Road": {
      "lng": -72.36986,
      "lat": 44.52344
    },
    "Bolton Falls": {
      "lng": -72.81673,
      "lat": 44.359381
    },
    "Boltonville Hydro Associates": {
      "lng": -72.0994,
      "lat": 44.1706
    },
    "Cadys Falls": {
      "lng": -72.611861,
      "lat": 44.577553
    },
    "Canaan": {
      "lng": -71.530164,
      "lat": 44.999274
    },
    "Cavendish": {
      "lng": -72.598242,
      "lat": 43.381225
    },
    "Chace Mill Winooski One": {
      "lng": -73.1875,
      "lat": 44.49
    },
    "Charlotte Solar LLC VT": {
      "lng": -73.197608,
      "lat": 44.321972
    },
    "Chester Power Partners": {
      "lng": -72.59757,
      "lat": 43.280929
    },
    "Chittenden County Solar Partners": {
      "lng": -73.145277,
      "lat": 44.436111
    },
    "Claire Solar Farm": {
      "lng": -73.156389,
      "lat": 44.426111
    },
    "Clarendon Solar Farm": {
      "lng": -72.955833,
      "lat": 43.539722
    },
    "Clark Falls": {
      "lng": -73.12,
      "lat": 44.6356
    },
    "Colchester 16": {
      "lng": -73.1703,
      "lat": 44.4906
    },
    "Cold River Road Solar": {
      "lng": -72.952,
      "lat": 43.5857
    },
    "Coolidge Solar 1, LLC": {
      "lng": -72.665336,
      "lat": 43.424817
    },
    "Coventry Clean Energy Corporation": {
      "lng": -72.221446,
      "lat": 44.908716
    },
    "Coventry Photovoltaic, LLC": {
      "lng": -72.21975,
      "lat": 44.910152
    },
    "Deerfield Wind LLC": {
      "lng": -72.993721,
      "lat": 42.877215
    },
    "Deweys Mill": {
      "lng": -72.4061,
      "lat": 43.6411
    },
    "East Barnet": {
      "lng": -72.035894,
      "lat": 44.326335
    },
    "Elizabeth Mines Solar 1": {
      "lng": -72.33111,
      "lat": 43.823611
    },
    "ER Salvage Yard": {
      "lng": -72.61194,
      "lat": 44.54628
    },
    "Essex Junction 19": {
      "lng": -73.1164,
      "lat": 44.4821
    },
    "Fairfax Falls": {
      "lng": -72.9961,
      "lat": 44.6534
    },
    "Georgia Mountain Community Wind Farm": {
      "lng": -73.07,
      "lat": 44.661667
    },
    "Gilman Mill": {
      "lng": -71.7228,
      "lat": 44.4122
    },
    "Glen": {
      "lng": -72.9758,
      "lat": 43.6368
    },
    "GMP Solar - Hartford": {
      "lng": -72.418566,
      "lat": 43.632074
    },
    "GMP Solar - Panton Hybrid": {
      "lng": -73.195385,
      "lat": 44.085795
    },
    "GMP Solar - Richmond": {
      "lng": -73.008489,
      "lat": 44.42691
    },
    "GMP Solar - Williamstown": {
      "lng": -72.510509,
      "lat": 44.136051
    },
    "GMP Solar - Williston": {
      "lng": -73.103241,
      "lat": 44.465485
    },
    "GMP Solar/Storage-Essex Hybrid": {
      "lng": -73.023366,
      "lat": 44.285937
    },
    "GMP Solar/Storage-Ferrisburgh Hybrid": {
      "lng": -73.144064,
      "lat": 44.133411
    },
    "GMP Solar/Storage-Milton Hybrid": {
      "lng": -73.10233,
      "lat": 44.392141
    },
    "Golden Solar": {
      "lng": -71.97124,
      "lat": 44.43415
    },
    "Gorge 18": {
      "lng": -73.169312,
      "lat": 44.489997
    },
    "Great Falls (VT)": {
      "lng": -71.998921,
      "lat": 44.500222
    },
    "GSPP Gilman, LLC": {
      "lng": -71.714483,
      "lat": 44.414773
    },
    "Harriman": {
      "lng": -72.9144,
      "lat": 42.7936
    },
    "Highgate Falls": {
      "lng": -73.051111,
      "lat": 44.933611
    },
    "Hinesburg": {
      "lng": -73.070764,
      "lat": 44.36115
    },
    "Huntington Falls": {
      "lng": -73.1961,
      "lat": 44.0703
    },
    "J C McNeil": {
      "lng": -73.208056,
      "lat": 44.4917
    },
    "Kingdom Community Wind": {
      "lng": -72.425278,
      "lat": 44.748333
    },
    "Lawrence Brook": {
      "lng": -72.62542,
      "lat": 44.51701
    },
    "Limerick Road Solar Farm": {
      "lng": -73.247083,
      "lat": 44.368889
    },
    "Lower Middlebury": {
      "lng": -73.1768,
      "lat": 44.0249
    },
    "MacKinnon Solar": {
      "lng": -72.9573,
      "lat": 43.5786
    },
    "Main Street Solar Project": {
      "lng": -72.975556,
      "lat": 43.626389
    },
    "Marshfield 6": {
      "lng": -72.3356,
      "lat": 44.3606
    },
    "Mcindoes": {
      "lng": -72.0592,
      "lat": 44.2603
    },
    "MHG Wallingford": {
      "lng": -72.982687,
      "lat": 43.476875
    },
    "Middlebury College": {
      "lng": -73.176944,
      "lat": 44.0075
    },
    "Middlesex 2": {
      "lng": -72.6824,
      "lat": 44.292391
    },
    "Milton": {
      "lng": -73.113364,
      "lat": 44.641433
    },
    "Moretown": {
      "lng": -72.711389,
      "lat": 44.312222
    },
    "Moretown Generating Station": {
      "lng": -72.710324,
      "lat": 44.285945
    },
    "Morrisville": {
      "lng": -72.602369,
      "lat": 44.561831
    },
    "Nebraska Valley Solar Farm": {
      "lng": -72.770856,
      "lat": 44.46234
    },
    "Newport": {
      "lng": -72.179968,
      "lat": 44.937579
    },
    "Next Generation Solar Farm": {
      "lng": -73.226647,
      "lat": 44.084824
    },
    "Open View Solar Farm": {
      "lng": -73.203889,
      "lat": 44.139444
    },
    "Ottauquechee Hydro": {
      "lng": -72.3506,
      "lat": 43.5933
    },
    "Park St": {
      "lng": -72.970431,
      "lat": 43.596913
    },
    "Penny Lane Gas Turbine": {
      "lng": -73.2239,
      "lat": 44.4814
    },
    "Peterson": {
      "lng": -73.1625,
      "lat": 44.6381
    },
    "Pittsford": {
      "lng": -72.955,
      "lat": 43.694
    },
    "Proctor": {
      "lng": -73.034023,
      "lat": 43.662775
    },
    "Rutland": {
      "lng": -72.9923,
      "lat": 43.603
    },
    "Ryegate Associates": {
      "lng": -72.0572,
      "lat": 44.2131
    },
    "Saint Albans Solar": {
      "lng": -73.105823,
      "lat": 44.779729
    },
    "Salisbury": {
      "lng": -73.1007,
      "lat": 43.8954
    },
    "Searsburg": {
      "lng": -72.92732,
      "lat": 42.87084
    },
    "Searsburg Wind Turbine": {
      "lng": -72.9628,
      "lat": 42.8625
    },
    "Sheffield Wind": {
      "lng": -72.1013,
      "lat": 44.67598
    },
    "Sheldon Solar": {
      "lng": -72.979444,
      "lat": 44.910556
    },
    "Sheldon Springs Hydroelectric": {
      "lng": -72.9736,
      "lat": 44.9108
    },
    "Silver Lake (VT)": {
      "lng": -73.053125,
      "lat": 43.898584
    },
    "Smith (VT)": {
      "lng": -72.1282,
      "lat": 43.9921
    },
    "St. Albans SPEED Project": {
      "lng": -73.088611,
      "lat": 44.787778
    },
    "Stafford Hill Solar Hybrid": {
      "lng": -72.950277,
      "lat": 43.619444
    },
    "Sudbury Solar": {
      "lng": -73.192,
      "lat": 43.833
    },
    "SunGen Sharon 1 LLC": {
      "lng": -72.49,
      "lat": 43.79
    },
    "SVEP Solar Project Company": {
      "lng": -73.226111,
      "lat": 42.753611
    },
    "Technology Drive Solar": {
      "lng": -72.565833,
      "lat": 42.876111
    },
    "VEC Alburgh Array": {
      "lng": -73.274146,
      "lat": 44.96234
    },
    "VEC Magee Hill Solar": {
      "lng": -73.0608,
      "lat": 44.3682
    },
    "Vergennes 9": {
      "lng": -73.2575,
      "lat": 44.1664
    },
    "Vernon Dam": {
      "lng": -72.5146,
      "lat": 42.77165
    },
    "VPPSA Project 10": {
      "lng": -73.1,
      "lat": 44.916666
    },
    "W K Sanders": {
      "lng": -72.530859,
      "lat": 44.625282
    },
    "Waterbury 22": {
      "lng": -72.76769,
      "lat": 44.38126
    },
    "West Charleston": {
      "lng": -72.055357,
      "lat": 44.887379
    },
    "West Danville 15": {
      "lng": -72.1881,
      "lat": 44.4056
    },
    "Westminster": {
      "lng": -72.4554,
      "lat": 43.0864
    },
    "Weybridge": {
      "lng": -73.2158,
      "lat": 44.0661
    },
    "Whitcomb Solar Farm": {
      "lng": -73.136944,
      "lat": 44.485556
    },
    "Wilder": {
      "lng": -72.303642,
      "lat": 43.667927
    },
    "Williamstown Solar": {
      "lng": -72.605833,
      "lat": 44.113056
    },
    "Wrightsville Hydro Plant": {
      "lng": -72.575305,
      "lat": 44.309693
    },
    "Adams Nielson Solar": {
      "lng": -118.621244,
      "lat": 46.95528
    },
    "Alder": {
      "lng": -122.3102,
      "lat": 46.8016
    },
    "Arlington Microgrid": {
      "lng": -122.1502,
      "lat": 48.15614
    },
    "Big Horn Wind II": {
      "lng": -120.3039,
      "lat": 45.9192
    },
    "Big Horn Wind Project": {
      "lng": -120.2896,
      "lat": 45.8877
    },
    "Biotech LS 0836": {
      "lng": -117.1583,
      "lat": 46.7331
    },
    "Black Creek": {
      "lng": -121.710517,
      "lat": 47.549652
    },
    "Boulder Park": {
      "lng": -117.147222,
      "lat": 47.697222
    },
    "Boundary": {
      "lng": -117.347847,
      "lat": 48.987111
    },
    "Box Canyon": {
      "lng": -117.418337,
      "lat": 48.780856
    },
    "Calligan Creek Hydroelectric Project": {
      "lng": -121.68861,
      "lat": 47.6008
    },
    "Cedar Falls (WA)": {
      "lng": -121.781858,
      "lat": 47.4193
    },
    "Cedar Hills": {
      "lng": -122.042778,
      "lat": 47.455833
    },
    "Centralia": {
      "lng": -122.859764,
      "lat": 46.755938
    },
    "Chandler": {
      "lng": -119.589821,
      "lat": 46.26678
    },
    "Chehalis Generation Facility": {
      "lng": -122.9131,
      "lat": 46.6225
    },
    "Chelan": {
      "lng": -120.0133,
      "lat": 47.8347
    },
    "Chief Joseph": {
      "lng": -119.6404,
      "lat": 47.9951
    },
    "Coastal Energy Project": {
      "lng": -124.065412,
      "lat": 46.793581
    },
    "Columbia Generating Station": {
      "lng": -119.3339,
      "lat": 46.4711
    },
    "Cosmo Specialty Fibers Plant": {
      "lng": -123.761086,
      "lat": 46.954071
    },
    "Cowiche": {
      "lng": -120.737194,
      "lat": 46.692628
    },
    "Cowlitz Falls": {
      "lng": -122.1097,
      "lat": 46.4661
    },
    "Crystal Mountain": {
      "lng": -121.475198,
      "lat": 46.953468
    },
    "Cushman 1": {
      "lng": -123.2252,
      "lat": 47.4181
    },
    "Cushman 2": {
      "lng": -123.1603,
      "lat": 47.3698
    },
    "Darrington": {
      "lng": -121.599979,
      "lat": 48.269737
    },
    "Diablo": {
      "lng": -121.131736,
      "lat": 48.713853
    },
    "Drop 2 (WA)": {
      "lng": -120.5342,
      "lat": 46.4492
    },
    "Drop 3 (WA)": {
      "lng": -120.559997,
      "lat": 46.423133
    },
    "Electron": {
      "lng": -122.174921,
      "lat": 46.990967
    },
    "Eltopia Branch Canal 4.6": {
      "lng": -119.254522,
      "lat": 46.378381
    },
    "Encogen Generating Station": {
      "lng": -122.486,
      "lat": 48.746
    },
    "Ferndale Generating Station": {
      "lng": -122.685114,
      "lat": 48.828996
    },
    "FPL Energy Vansycle LLC (WA)": {
      "lng": -118.917,
      "lat": 46.06
    },
    "Frederickson": {
      "lng": -122.365,
      "lat": 47.079722
    },
    "Frederickson Power LP": {
      "lng": -122.365674,
      "lat": 47.086111
    },
    "Fredonia Generating Station": {
      "lng": -122.4358,
      "lat": 48.4558
    },
    "Glacier Battery Storage": {
      "lng": -121.9462,
      "lat": 48.88804
    },
    "Goldendale Generating Station": {
      "lng": -120.833,
      "lat": 45.8114
    },
    "Goodnoe Hills": {
      "lng": -120.521151,
      "lat": 45.781825
    },
    "Gorge": {
      "lng": -121.208611,
      "lat": 48.698056
    },
    "Grand Coulee": {
      "lng": -118.977323,
      "lat": 47.957511
    },
    "Grays Harbor Energy Center": {
      "lng": -123.48,
      "lat": 46.9692
    },
    "Grimes Way": {
      "lng": -117.1514,
      "lat": 46.7289
    },
    "H M Jackson": {
      "lng": -121.8144,
      "lat": 47.9084
    },
    "Hancock Creek Hydroelectric Project": {
      "lng": -121.6897,
      "lat": 47.5722
    },
    "Harvest Wind Project": {
      "lng": -120.3475,
      "lat": 45.8131
    },
    "Hopkins Ridge Wind": {
      "lng": -117.8331,
      "lat": 46.4211
    },
    "Horn Rapids Solar, Storage and Training": {
      "lng": -119.3222,
      "lat": 46.3529
    },
    "Ice Harbor": {
      "lng": -118.879838,
      "lat": 46.249801
    },
    "Juniper Canyon I Wind Project": {
      "lng": -120.2355,
      "lat": 45.9215
    },
    "Kettle Falls Generating Station": {
      "lng": -118.111059,
      "lat": 48.620336
    },
    "Koma Kulshan Associates": {
      "lng": -121.723294,
      "lat": 48.680178
    },
    "LaGrande": {
      "lng": -122.3196,
      "lat": 46.8328
    },
    "Lilliwaup Falls Generating": {
      "lng": -123.1153,
      "lat": 47.4706
    },
    "Linden Wind Energy Project": {
      "lng": -120.783568,
      "lat": 45.744171
    },
    "Little Falls (WA)": {
      "lng": -117.916667,
      "lat": 47.821667
    },
    "Little Goose": {
      "lng": -118.0273,
      "lat": 46.5838
    },
    "Long Lake": {
      "lng": -117.836111,
      "lat": 47.834444
    },
    "Longview Fibre": {
      "lng": -122.918429,
      "lat": 46.103887
    },
    "Lower Baker": {
      "lng": -121.7407,
      "lat": 48.5478
    },
    "Lower Granite": {
      "lng": -117.429385,
      "lat": 46.659503
    },
    "Lower Monumental": {
      "lng": -118.5397,
      "lat": 46.5635
    },
    "Lower Snake River Wind Energy Project": {
      "lng": -117.821666,
      "lat": 46.524167
    },
    "LRI LFGTE Facility": {
      "lng": -122.290556,
      "lat": 46.975556
    },
    "Main Canal Headworks": {
      "lng": -119.2992,
      "lat": 47.6164
    },
    "Marengo Wind Plant": {
      "lng": -117.7772,
      "lat": 46.3747
    },
    "Mayfield": {
      "lng": -122.5885,
      "lat": 46.5035
    },
    "McNary Dam Fish Attraction Project": {
      "lng": -119.299078,
      "lat": 45.940034
    },
    "Merwin": {
      "lng": -122.554444,
      "lat": 45.9567
    },
    "MESA 1": {
      "lng": -122.246917,
      "lat": 47.936194
    },
    "Meyers Falls": {
      "lng": -118.0603,
      "lat": 48.5953
    },
    "Mint Farm Generating Station": {
      "lng": -122.985506,
      "lat": 46.138824
    },
    "Monroe Street": {
      "lng": -117.420556,
      "lat": 47.653611
    },
    "Mossyrock": {
      "lng": -122.4247,
      "lat": 46.5347
    },
    "Newhalem": {
      "lng": -121.240826,
      "lat": 48.675944
    },
    "Nine Canyon": {
      "lng": -119.0964,
      "lat": 46.0958
    },
    "Nine Mile": {
      "lng": -117.537222,
      "lat": 47.769722
    },
    "Nippon Dynawave Packaging Longview WA": {
      "lng": -122.9728,
      "lat": 46.1236
    },
    "Nooksack Hydro": {
      "lng": -121.814167,
      "lat": 48.907778
    },
    "Northeast (WA)": {
      "lng": -117.370538,
      "lat": 47.73511
    },
    "Orchard Avenue 1": {
      "lng": -120.677489,
      "lat": 46.596413
    },
    "Packwood": {
      "lng": -121.569444,
      "lat": 46.595833
    },
    "Palouse": {
      "lng": -117.364444,
      "lat": 47.155833
    },
    "PEC Headworks": {
      "lng": -119.896306,
      "lat": 47.159553
    },
    "Port Townsend Paper": {
      "lng": -122.7958,
      "lat": 48.0931
    },
    "Potholes East Canal 66.0": {
      "lng": -119.254443,
      "lat": 46.378469
    },
    "Priest Rapids": {
      "lng": -119.908,
      "lat": 46.6451
    },
    "Puget Sound Refinery": {
      "lng": -122.56,
      "lat": 48.4708
    },
    "Quincy Chute": {
      "lng": -119.257764,
      "lat": 46.982514
    },
    "Rattlesnake Flat": {
      "lng": -118.567136,
      "lat": 46.941015
    },
    "River Road": {
      "lng": -122.7256,
      "lat": 45.6497
    },
    "Rock Island": {
      "lng": -120.0917,
      "lat": 47.346111
    },
    "Rocky Brook Hydroelectric": {
      "lng": -122.9431,
      "lat": 47.7197
    },
    "Rocky Reach": {
      "lng": -120.295278,
      "lat": 47.533611
    },
    "Ross": {
      "lng": -121.06788,
      "lat": 48.732577
    },
    "Roza": {
      "lng": -120.477759,
      "lat": 46.618413
    },
    "Russell D Smith": {
      "lng": -119.1122,
      "lat": 46.7392
    },
    "Sagebrush Power Partners": {
      "lng": -120.6872,
      "lat": 47.1356
    },
    "SDS Lumber Gorge Energy Division": {
      "lng": -121.469178,
      "lat": 45.713079
    },
    "Sheep Creek Hydro": {
      "lng": -117.795376,
      "lat": 48.947741
    },
    "Sierra Pacific Aberdeen": {
      "lng": -123.7739,
      "lat": 46.9728
    },
    "Sierra Pacific Burlington Facility": {
      "lng": -122.4331,
      "lat": 48.4481
    },
    "Skookumchuck": {
      "lng": -122.7197,
      "lat": 46.7844
    },
    "Skookumchuck Wind Facility": {
      "lng": -122.664447,
      "lat": 46.85074
    },
    "Snoqualmie": {
      "lng": -121.83702,
      "lat": 47.54011
    },
    "Snoqualmie 2": {
      "lng": -121.8414,
      "lat": 47.5445
    },
    "South Fork Tolt": {
      "lng": -121.783333,
      "lat": 47.699167
    },
    "Spokane Waste to Energy": {
      "lng": -117.50424,
      "lat": 47.626327
    },
    "Sumas Generating Station": {
      "lng": -122.2733,
      "lat": 48.9905
    },
    "Summer Falls Power Plant": {
      "lng": -119.2917,
      "lat": 47.5025
    },
    "Swauk Wind LLC": {
      "lng": -120.753056,
      "lat": 47.139167
    },
    "Swift 1": {
      "lng": -122.201667,
      "lat": 46.060833
    },
    "Swift 2": {
      "lng": -122.2594,
      "lat": 46.0594
    },
    "Tieton Dam Hydro Electric Project": {
      "lng": -121.1287,
      "lat": 46.658
    },
    "Tucannon River Wind Farm": {
      "lng": -118.026944,
      "lat": 46.421111
    },
    "Tuolumne Wind Project": {
      "lng": -120.8072,
      "lat": 45.8797
    },
    "Twin Falls Hydro": {
      "lng": -121.687812,
      "lat": 47.444554
    },
    "Twin Reservoirs": {
      "lng": -118.254722,
      "lat": 46.088333
    },
    "University of Washington Power Plant": {
      "lng": -122.3036,
      "lat": 47.6539
    },
    "Upper Baker": {
      "lng": -121.6907,
      "lat": 48.649
    },
    "Upper Falls": {
      "lng": -117.418889,
      "lat": 47.654444
    },
    "Upriver Dam Hydro Plant": {
      "lng": -117.3319,
      "lat": 47.6822
    },
    "Vantage Wind Energy LLC": {
      "lng": -120.1819,
      "lat": 46.9547
    },
    "Wanapum": {
      "lng": -119.9703,
      "lat": 46.8731
    },
    "Weeks Falls": {
      "lng": -121.6483,
      "lat": 47.43348
    },
    "Wells": {
      "lng": -119.8653,
      "lat": 47.9469
    },
    "West Point Treatment Plant": {
      "lng": -122.446389,
      "lat": 47.661111
    },
    "WestRock (WA)": {
      "lng": -122.428288,
      "lat": 47.266069
    },
    "White Creek Wind Farm": {
      "lng": -120.341883,
      "lat": 45.815467
    },
    "Whitehorn": {
      "lng": -122.75164,
      "lat": 48.885636
    },
    "Wild Horse": {
      "lng": -120.206494,
      "lat": 47.036269
    },
    "Windy Flats Wind Project": {
      "lng": -120.8339,
      "lat": 45.7269
    },
    "Wynoochee": {
      "lng": -123.605225,
      "lat": 47.385072
    },
    "Yale": {
      "lng": -122.335467,
      "lat": 45.96277
    },
    "Yelm": {
      "lng": -122.635795,
      "lat": 46.974912
    },
    "Youngs Creek Hydroelectric Project": {
      "lng": -121.8,
      "lat": 47.7916
    },
    "Alexander": {
      "lng": -89.7553,
      "lat": 45.1875
    },
    "Ameresco Janesville": {
      "lng": -89.0208,
      "lat": 42.7083
    },
    "Apple River": {
      "lng": -92.716214,
      "lat": 45.157552
    },
    "Appleton": {
      "lng": -88.40956,
      "lat": 44.25393
    },
    "Appleton Property Ventures LLC": {
      "lng": -88.3011,
      "lat": 44.2717
    },
    "Arcadia Electric": {
      "lng": -91.5034,
      "lat": 44.2524
    },
    "Arcadia Solar": {
      "lng": -91.458244,
      "lat": 44.243436
    },
    "Argyle": {
      "lng": -89.8697,
      "lat": 42.7019
    },
    "Arpin Dam": {
      "lng": -91.2028,
      "lat": 45.7517
    },
    "Badger Hollow I": {
      "lng": -90.34942,
      "lat": 42.94556
    },
    "Barron": {
      "lng": -91.840382,
      "lat": 45.40193
    },
    "Barron Solar Array": {
      "lng": -91.827567,
      "lat": 45.396196
    },
    "Bay Front": {
      "lng": -90.9017,
      "lat": 46.587222
    },
    "Beloit Memorial Hospital Power Plant": {
      "lng": -89.0083,
      "lat": 42.5486
    },
    "Berlin Landfill Gas": {
      "lng": -88.9158,
      "lat": 43.9603
    },
    "Big Falls": {
      "lng": -91.0469,
      "lat": 45.491667
    },
    "Biron": {
      "lng": -89.7808,
      "lat": 44.4306
    },
    "Biron Mill": {
      "lng": -89.78,
      "lat": 44.43
    },
    "Blount Street": {
      "lng": -89.3744,
      "lat": 43.0789
    },
    "Blue Prairie Solar, LLC": {
      "lng": -90.9158,
      "lat": 44.285255
    },
    "Blue Sky Green Field Wind Project": {
      "lng": -88.2708,
      "lat": 43.8794
    },
    "Brule": {
      "lng": -88.2189,
      "lat": 45.9472
    },
    "Butler Ridge": {
      "lng": -88.481111,
      "lat": 43.400278
    },
    "Caldron Falls": {
      "lng": -88.2306,
      "lat": 45.3569
    },
    "Capitol Heat and Power": {
      "lng": -89.377261,
      "lat": 43.078512
    },
    "Cashton": {
      "lng": -90.780325,
      "lat": 43.742874
    },
    "Cashton Greens Wind Farm": {
      "lng": -90.805,
      "lat": 43.733889
    },
    "Cashton Solar": {
      "lng": -90.805,
      "lat": 43.737
    },
    "Castle Rock": {
      "lng": -89.9514,
      "lat": 43.8656
    },
    "Cedar Falls (WI)": {
      "lng": -91.8883,
      "lat": 44.9367
    },
    "Cedar Ridge": {
      "lng": -88.329742,
      "lat": 43.692647
    },
    "Chippewa Falls": {
      "lng": -91.3889,
      "lat": 44.9308
    },
    "Clam River Dam": {
      "lng": -92.5383,
      "lat": 45.9467
    },
    "Combined Locks": {
      "lng": -88.300548,
      "lat": 44.273511
    },
    "Concord": {
      "lng": -88.69,
      "lat": 43.1669
    },
    "Conrath DPC Solar": {
      "lng": -91.0482,
      "lat": 45.36614
    },
    "Consumer Operations LLC": {
      "lng": -88.0303,
      "lat": 44.4936
    },
    "Cornell": {
      "lng": -91.157427,
      "lat": 45.164024
    },
    "Cumberland (WI)": {
      "lng": -92.0224,
      "lat": 45.5336
    },
    "Cumberland Solar": {
      "lng": -92.025,
      "lat": 45.511
    },
    "Custer Energy Center": {
      "lng": -87.726102,
      "lat": 44.085437
    },
    "Dairyland WTE Plant": {
      "lng": -87.805587,
      "lat": 44.540115
    },
    "Danbury Dam": {
      "lng": -92.3725,
      "lat": 46
    },
    "Danbury Diesel": {
      "lng": -92.371377,
      "lat": 46.002834
    },
    "Dane County Airport Solar": {
      "lng": -89.329495,
      "lat": 43.160591
    },
    "Deertrack Park Gas Recovery": {
      "lng": -88.739677,
      "lat": 43.089999
    },
    "Dells": {
      "lng": -91.511111,
      "lat": 44.828056
    },
    "Depere Energy Center": {
      "lng": -88.07204,
      "lat": 44.44861
    },
    "Diesel Generators": {
      "lng": -89.3747,
      "lat": 43.0802
    },
    "Domtar Paper Company Rothschild": {
      "lng": -89.6272,
      "lat": 44.8867
    },
    "Downsville DPC Solar": {
      "lng": -91.8841,
      "lat": 44.81423
    },
    "Du Bay": {
      "lng": -89.6511,
      "lat": 44.6647
    },
    "Edgewater (4050)": {
      "lng": -87.705863,
      "lat": 43.715142
    },
    "Elk Mound Generating Station": {
      "lng": -91.568096,
      "lat": 44.902814
    },
    "Elm Road Generating Station": {
      "lng": -87.8336,
      "lat": 42.8492
    },
    "Elroy": {
      "lng": -90.2675,
      "lat": 43.7395
    },
    "Elroy Solar": {
      "lng": -90.025,
      "lat": 43.732
    },
    "ENGIE 2020 ProjectCo-WI1 LLC": {
      "lng": -88.473775,
      "lat": 43.735027
    },
    "Epic Verona": {
      "lng": -89.568745,
      "lat": 42.996838
    },
    "Fairbanks Morse Engine": {
      "lng": -89.0287,
      "lat": 42.51846
    },
    "FCPC Renewable Generation": {
      "lng": -87.940914,
      "lat": 43.030016
    },
    "Fennimore": {
      "lng": -90.6558,
      "lat": 42.9828
    },
    "Fennimore Solar": {
      "lng": -90.662,
      "lat": 42.971
    },
    "Fiber Recovery, Inc.": {
      "lng": -89.3859,
      "lat": 44.8875
    },
    "Fitchburg Generating Station": {
      "lng": -89.4603,
      "lat": 43.0147
    },
    "Flambeau Crowley Rapids Project": {
      "lng": -90.5844,
      "lat": 45.8683
    },
    "Flambeau Hydroelectric Station": {
      "lng": -91.047211,
      "lat": 45.491826
    },
    "Flambeau Lower Project": {
      "lng": -90.44746,
      "lat": 45.913526
    },
    "Flambeau Solar Partners": {
      "lng": -90.471,
      "lat": 45.7368
    },
    "Forward Wind Energy Center": {
      "lng": -88.4969,
      "lat": 43.6161
    },
    "Fox Energy Center": {
      "lng": -88.208889,
      "lat": 44.3204
    },
    "Frederic Diesel": {
      "lng": -92.465063,
      "lat": 45.654835
    },
    "French Island": {
      "lng": -91.2597,
      "lat": 43.8292
    },
    "Galactic Wind": {
      "lng": -89.331855,
      "lat": 43.101605
    },
    "Genoa": {
      "lng": -91.2325,
      "lat": 43.5589
    },
    "Georgia-Pacific Corp - Nekoosa Mill": {
      "lng": -89.8964,
      "lat": 44.3142
    },
    "Germantown Power Plant": {
      "lng": -88.1496,
      "lat": 43.1952
    },
    "GL Dairy Biogas": {
      "lng": -89.545,
      "lat": 43.124444
    },
    "Glacier Hills": {
      "lng": -89.148056,
      "lat": 43.563889
    },
    "Gordon": {
      "lng": -91.785532,
      "lat": 46.23669
    },
    "Grandfather Falls": {
      "lng": -89.7917,
      "lat": 45.3019
    },
    "Grantsburg Diesel": {
      "lng": -92.678047,
      "lat": 45.779189
    },
    "Hat Rapids": {
      "lng": -89.4806,
      "lat": 45.5711
    },
    "High Falls (WI)": {
      "lng": -88.1997,
      "lat": 45.2794
    },
    "Holcombe": {
      "lng": -91.127222,
      "lat": 45.223611
    },
    "HQC Rock River Solar Power Gen Station": {
      "lng": -89.033021,
      "lat": 42.578831
    },
    "IKEA Oak Creek Rooftop PV System": {
      "lng": -87.935448,
      "lat": 42.902949
    },
    "Island Street Peaking Plant": {
      "lng": -88.2658,
      "lat": 44.2761
    },
    "J P Madgett": {
      "lng": -91.912647,
      "lat": 44.303583
    },
    "Jefferson Solar Park": {
      "lng": -88.7992,
      "lat": 43.0146
    },
    "Jim Falls": {
      "lng": -91.274,
      "lat": 45.051
    },
    "Johnson Falls": {
      "lng": -88.1603,
      "lat": 45.2875
    },
    "Kaukauna City Hydro": {
      "lng": -88.319074,
      "lat": 44.274582
    },
    "Kaukauna Gas Turbine": {
      "lng": -88.265445,
      "lat": 44.276199
    },
    "Kaukauna Paper Mill": {
      "lng": -88.2534,
      "lat": 44.2826
    },
    "Kilbourn": {
      "lng": -89.780764,
      "lat": 43.625845
    },
    "Kimberly Mill": {
      "lng": -88.3344,
      "lat": 44.2769
    },
    "La Farge": {
      "lng": -90.6391,
      "lat": 43.5722
    },
    "LAC Courte Oreilles Hydroelect": {
      "lng": -91.076944,
      "lat": 45.888333
    },
    "Ladysmith Dam": {
      "lng": -91.083611,
      "lat": 45.464444
    },
    "Lafayette DPC Solar": {
      "lng": -91.3524,
      "lat": 44.88782
    },
    "Liberty Pole DPC Solar": {
      "lng": -90.9071,
      "lat": 43.49412
    },
    "Little Chute": {
      "lng": -88.25558,
      "lat": 44.279897
    },
    "Little Quinnesec Falls Hydro Project": {
      "lng": -87.9894,
      "lat": 45.7736
    },
    "Manitowoc": {
      "lng": -87.6558,
      "lat": 44.082
    },
    "Marathon Electric": {
      "lng": -89.6328,
      "lat": 44.9783
    },
    "Marshfield Utilities Combustion Turbine": {
      "lng": -90.134444,
      "lat": 44.634722
    },
    "Medford DPC Solar": {
      "lng": -90.3729,
      "lat": 45.16305
    },
    "Menomonie": {
      "lng": -91.929444,
      "lat": 44.884167
    },
    "Merrill": {
      "lng": -89.685,
      "lat": 45.1786
    },
    "Metro Gas Recovery": {
      "lng": -88.069722,
      "lat": 42.850556
    },
    "Middleton Airport Solar": {
      "lng": -89.53245,
      "lat": 43.11626
    },
    "MMSD Jones Island Wastewater": {
      "lng": -87.9014,
      "lat": 43.0214
    },
    "MMSD South Shore Wastewater": {
      "lng": -87.8422,
      "lat": 42.8869
    },
    "Montfort Wind Energy Center": {
      "lng": -90.386667,
      "lat": 42.9625
    },
    "Mosinee Mill": {
      "lng": -89.6917,
      "lat": 44.7881
    },
    "Mt. Hope DPC Solar": {
      "lng": -90.8318,
      "lat": 42.97498
    },
    "Neenah Energy Facility": {
      "lng": -88.5064,
      "lat": 44.1936
    },
    "New Auburn DPC Solar": {
      "lng": -91.4353,
      "lat": 45.21
    },
    "New Badger": {
      "lng": -88.2678,
      "lat": 44.2772
    },
    "New Lisbon": {
      "lng": -90.1645,
      "lat": 43.8797
    },
    "New Lisbon Solar": {
      "lng": -90.136,
      "lat": 43.883
    },
    "Nine Springs": {
      "lng": -89.3586,
      "lat": 43.0386
    },
    "O'Brien Solar Fields": {
      "lng": -89.458945,
      "lat": 42.997665
    },
    "Oconto Falls Lower": {
      "lng": -88.1439,
      "lat": 44.8719
    },
    "Oconto Falls Upper": {
      "lng": -88.1475,
      "lat": 44.8764
    },
    "Omega Hills Gas Recovery": {
      "lng": -88.1597,
      "lat": 43.1922
    },
    "Onalaska Campus Landfill Biogas": {
      "lng": -91.18,
      "lat": 43.873889
    },
    "Packaging of America Tomahawk Mill": {
      "lng": -89.7342,
      "lat": 45.4431
    },
    "Paris": {
      "lng": -88.0131,
      "lat": 42.6658
    },
    "Park Mill": {
      "lng": -87.6514,
      "lat": 45.1058
    },
    "Petenwell": {
      "lng": -90.0217,
      "lat": 44.0572
    },
    "Pheasant Run Landfill Gas Recovery": {
      "lng": -88.0436,
      "lat": 42.5825
    },
    "Pine": {
      "lng": -88.2483,
      "lat": 45.8275
    },
    "Point Beach Nuclear Plant": {
      "lng": -87.5369,
      "lat": 44.2806
    },
    "Point Beach Solar": {
      "lng": -87.555326,
      "lat": 44.28337
    },
    "Port Edwards Mill": {
      "lng": -89.8672,
      "lat": 44.3364
    },
    "Port Washington Generating Station": {
      "lng": -87.8689,
      "lat": 43.3842
    },
    "Potato Rapids": {
      "lng": -87.7586,
      "lat": 45.1136
    },
    "Prairie Du Sac": {
      "lng": -89.727778,
      "lat": 43.309932
    },
    "Pulliam": {
      "lng": -88.0086,
      "lat": 44.54
    },
    "Quilt Block Wind Farm LLC": {
      "lng": -90.265277,
      "lat": 42.673333
    },
    "Rapide Croche": {
      "lng": -88.3147,
      "lat": 44.3164
    },
    "Rhinelander Mill": {
      "lng": -89.4205,
      "lat": 45.6394
    },
    "Rice Lake Solar Array": {
      "lng": -91.756931,
      "lat": 45.526558
    },
    "Richland Center Renewable Energy LLC": {
      "lng": -90.373056,
      "lat": 43.314444
    },
    "Ridgeview": {
      "lng": -87.828519,
      "lat": 44.175007
    },
    "Riverside Energy Center": {
      "lng": -89.035833,
      "lat": 42.583056
    },
    "Rockgen Energy Center": {
      "lng": -89.0492,
      "lat": 42.9767
    },
    "Rothschild Biomass Cogeneration Facility": {
      "lng": -89.629722,
      "lat": 44.887778
    },
    "Sand Lake DPC Solar": {
      "lng": -92.549,
      "lat": 45.4455
    },
    "Sandstone Rapids": {
      "lng": -88.0678,
      "lat": 45.2333
    },
    "Sauk DPC Solar": {
      "lng": -90.3226,
      "lat": 43.59591
    },
    "Saxon Falls": {
      "lng": -90.3742,
      "lat": 46.5392
    },
    "Shamrock Solar LLC": {
      "lng": -90.84654,
      "lat": 44.17659
    },
    "Sheboygan Falls Energy Facility": {
      "lng": -87.878056,
      "lat": 43.751826
    },
    "Shirley Wind": {
      "lng": -87.9278,
      "lat": 44.3481
    },
    "Solon Diesel": {
      "lng": -91.823631,
      "lat": 46.358956
    },
    "South Fond Du Lac": {
      "lng": -88.496667,
      "lat": 43.7353
    },
    "South Oak Creek": {
      "lng": -87.8294,
      "lat": 42.8457
    },
    "Spooner Solar Array": {
      "lng": -91.862475,
      "lat": 45.919047
    },
    "St Croix Falls": {
      "lng": -92.6469,
      "lat": 45.41167
    },
    "Stevens Point": {
      "lng": -89.585,
      "lat": 44.5153
    },
    "Stevens Point Mill": {
      "lng": -89.5836,
      "lat": 44.3528
    },
    "Stiles": {
      "lng": -88.1586,
      "lat": 44.8747
    },
    "Strobus": {
      "lng": -90.85707,
      "lat": 44.4332
    },
    "Stromland Solar": {
      "lng": -91.80897,
      "lat": 44.19955
    },
    "Sycamore (WI)": {
      "lng": -89.3128,
      "lat": 43.1133
    },
    "Thornapple": {
      "lng": -91.218283,
      "lat": 45.411139
    },
    "Timberline Trail Gas Recovery": {
      "lng": -91.3583,
      "lat": 45.4533
    },
    "Tomahawk": {
      "lng": -89.7306,
      "lat": 45.4411
    },
    "Trego": {
      "lng": -91.888374,
      "lat": 45.94794
    },
    "Twin Falls (WI)": {
      "lng": -88.070833,
      "lat": 45.872778
    },
    "Two Creeks Solar": {
      "lng": -87.54534,
      "lat": 44.249598
    },
    "UW Madison Charter Street Plant": {
      "lng": -89.4006,
      "lat": 43.07
    },
    "Valley (WEPCO)": {
      "lng": -87.9233,
      "lat": 43.0303
    },
    "Viola": {
      "lng": -90.669,
      "lat": 43.507
    },
    "Viresco Turtle Lake": {
      "lng": -92.1625,
      "lat": 45.390278
    },
    "Warren DPC Solar": {
      "lng": -92.5182,
      "lat": 44.95963
    },
    "Washington Island": {
      "lng": -86.930822,
      "lat": 45.370922
    },
    "Waupun Correctional Central Heating Plt": {
      "lng": -88.7297,
      "lat": 43.6233
    },
    "Wausau": {
      "lng": -89.6367,
      "lat": 44.9575
    },
    "Waxdale": {
      "lng": -87.885278,
      "lat": 42.709444
    },
    "West Campus Cogeneration Facility": {
      "lng": -89.4251,
      "lat": 43.075071
    },
    "West Marinette": {
      "lng": -87.6892,
      "lat": 45.0869
    },
    "West Riverside Energy Center": {
      "lng": -89.041214,
      "lat": 42.582051
    },
    "Weston": {
      "lng": -89.6553,
      "lat": 44.8606
    },
    "Wheaton Generating Plant": {
      "lng": -91.515833,
      "lat": 44.885416
    },
    "Whistling Winds DPC Solar": {
      "lng": -90.0719,
      "lat": 43.9721
    },
    "White River (WI)": {
      "lng": -90.903889,
      "lat": 46.498333
    },
    "Whitewater Cogeneration Facility": {
      "lng": -88.72992,
      "lat": 42.855463
    },
    "Whiting": {
      "lng": -89.5756,
      "lat": 44.4889
    },
    "Wind Turbine": {
      "lng": -87.6333,
      "lat": 44.6694
    },
    "Winnebago County Landfill Gas": {
      "lng": -88.5411,
      "lat": 44.083
    },
    "Winter": {
      "lng": -90.97352,
      "lat": 45.836675
    },
    "Wisconsin Rapids": {
      "lng": -89.825,
      "lat": 44.3939
    },
    "Wisconsin Rapids Pulp Mill": {
      "lng": -89.826,
      "lat": 44.3953
    },
    "Wissota": {
      "lng": -91.3406,
      "lat": 44.9378
    },
    "AEP Milton NaS": {
      "lng": -82.1256,
      "lat": 38.3622
    },
    "Axiall Corporation Natrium Plant": {
      "lng": -80.8547,
      "lat": 39.7475
    },
    "Beech Ridge Energy LLC": {
      "lng": -80.4925,
      "lat": 38.0878
    },
    "Beech Ridge Energy Storage": {
      "lng": -80.4925,
      "lat": 38.0878
    },
    "Beech Ridge II Wind Energy Center": {
      "lng": -80.613055,
      "lat": 38.112222
    },
    "Belleville Dam": {
      "lng": -81.7375,
      "lat": 39.1192
    },
    "Big Sandy Peaker Plant": {
      "lng": -82.5938,
      "lat": 38.3441
    },
    "Ceredo Generating Station": {
      "lng": -82.5339,
      "lat": 38.3681
    },
    "Dam No. 4 Hydro Station": {
      "lng": -77.826944,
      "lat": 39.493056
    },
    "Dam No. 5 Hydro Station": {
      "lng": -77.923055,
      "lat": 39.605
    },
    "Fort Martin Power Station": {
      "lng": -79.9275,
      "lat": 39.710833
    },
    "Gauley River Power Partners": {
      "lng": -80.89061,
      "lat": 38.21921
    },
    "Glen Ferris Hydro": {
      "lng": -81.2147,
      "lat": 38.1483
    },
    "Grant Town Power Plant": {
      "lng": -80.163138,
      "lat": 39.561831
    },
    "Harrison Power Station": {
      "lng": -80.3325,
      "lat": 39.384167
    },
    "Hawks Nest Hydro": {
      "lng": -81.1753,
      "lat": 38.1478
    },
    "John E Amos": {
      "lng": -81.8233,
      "lat": 38.4731
    },
    "Laurel Mountain Hybrid": {
      "lng": -79.8866,
      "lat": 39.0072
    },
    "London": {
      "lng": -81.3706,
      "lat": 38.1944
    },
    "Longview Power": {
      "lng": -79.958974,
      "lat": 39.707893
    },
    "Marmet": {
      "lng": -81.5695,
      "lat": 38.2526
    },
    "Millville Hydro Station": {
      "lng": -77.784444,
      "lat": 39.273056
    },
    "Mitchell (WV)": {
      "lng": -80.8153,
      "lat": 39.8297
    },
    "Morgantown Energy Facility": {
      "lng": -79.960556,
      "lat": 39.6397
    },
    "Mount Storm Power Station": {
      "lng": -79.2636,
      "lat": 39.2008
    },
    "Mountaineer (1301)": {
      "lng": -81.9344,
      "lat": 38.9794
    },
    "Mountaineer Wind Energy Center": {
      "lng": -79.5372,
      "lat": 39.1828
    },
    "NedPower Mount Storm": {
      "lng": -79.2094,
      "lat": 39.2175
    },
    "New Creek Wind": {
      "lng": -79.140639,
      "lat": 39.200639
    },
    "New Martinsville Hannibal Hydro": {
      "lng": -80.8642,
      "lat": 39.6672
    },
    "New River Clean Energy": {
      "lng": -81.16193,
      "lat": 37.81577
    },
    "Pinnacle Wind Force LLC": {
      "lng": -79.03,
      "lat": 39.445
    },
    "Pleasants Energy, LLC": {
      "lng": -81.3642,
      "lat": 39.3325
    },
    "Pleasants Power Station": {
      "lng": -81.294444,
      "lat": 39.366667
    },
    "Willow Island Hydroelectric Plant": {
      "lng": -81.31795,
      "lat": 39.35776
    },
    "Winfield": {
      "lng": -81.91392,
      "lat": 38.5274
    },
    "Alcova": {
      "lng": -106.717536,
      "lat": 42.548412
    },
    "Arvada": {
      "lng": -106.1092,
      "lat": 44.69
    },
    "Barber Creek": {
      "lng": -105.7689,
      "lat": 43.7219
    },
    "Beaver Creek Gas Plant": {
      "lng": -108.3136,
      "lat": 42.8475
    },
    "Big Sand Draw Plant": {
      "lng": -108.1697,
      "lat": 42.7527
    },
    "Boysen": {
      "lng": -108.177545,
      "lat": 43.417872
    },
    "Buffalo Bill": {
      "lng": -109.170661,
      "lat": 44.508169
    },
    "Campbell Hill Windpower": {
      "lng": -105.999,
      "lat": 43.01455
    },
    "Casper Wind Farm": {
      "lng": -106.221442,
      "lat": 42.884703
    },
    "Cedar Springs I": {
      "lng": -105.472139,
      "lat": 43.019714
    },
    "Cedar Springs II": {
      "lng": -105.365616,
      "lat": 42.969007
    },
    "Cedar Springs III": {
      "lng": -105.584352,
      "lat": 43.076692
    },
    "Cheyenne Prairie Generating Station": {
      "lng": -104.72,
      "lat": 41.123611
    },
    "Corriedale Wind Energy": {
      "lng": -104.984011,
      "lat": 41.1133
    },
    "Dave Johnston": {
      "lng": -105.7769,
      "lat": 42.8378
    },
    "Dry Fork Station": {
      "lng": -105.460833,
      "lat": 44.388889
    },
    "Dunlap": {
      "lng": -106.160278,
      "lat": 42.043611
    },
    "Ekola Flats": {
      "lng": -106.35843,
      "lat": 41.932405
    },
    "Elk Basin Gasoline Plant": {
      "lng": -108.8428,
      "lat": 44.9806
    },
    "Fontenelle": {
      "lng": -110.063973,
      "lat": 42.027013
    },
    "Foote Creek I": {
      "lng": -106.2013,
      "lat": 41.6283
    },
    "Fremont Canyon": {
      "lng": -106.795901,
      "lat": 42.4766
    },
    "Garland Canal Power Plant": {
      "lng": -108.8644,
      "lat": 44.7264
    },
    "General Chemical": {
      "lng": -109.7542,
      "lat": 41.5933
    },
    "Genesis Alkali": {
      "lng": -109.811944,
      "lat": 41.621667
    },
    "Glendo": {
      "lng": -104.955526,
      "lat": 42.467254
    },
    "Glenrock": {
      "lng": -105.835278,
      "lat": 43.0181
    },
    "Guernsey": {
      "lng": -104.760634,
      "lat": 42.289868
    },
    "Happy Jack Windpower Project": {
      "lng": -104.997778,
      "lat": 41.139722
    },
    "Hartzog": {
      "lng": -105.5322,
      "lat": 43.8294
    },
    "Heart Mountain": {
      "lng": -109.129588,
      "lat": 44.513767
    },
    "High Plains": {
      "lng": -106.011124,
      "lat": 41.675052
    },
    "Jim Bridger": {
      "lng": -108.7875,
      "lat": 41.7378
    },
    "Kortes": {
      "lng": -106.880543,
      "lat": 42.174301
    },
    "Lake (WY)": {
      "lng": -110.5739,
      "lat": 44.415
    },
    "Laramie River": {
      "lng": -104.8825,
      "lat": 42.108889
    },
    "McFadden Ridge": {
      "lng": -105.9906,
      "lat": 41.7244
    },
    "Medicine Bow": {
      "lng": -106.242778,
      "lat": 41.835278
    },
    "Mountain Wind Power II LLC": {
      "lng": -110.544444,
      "lat": 41.263889
    },
    "Mountain Wind Power LLC": {
      "lng": -110.480741,
      "lat": 41.288056
    },
    "Naughton": {
      "lng": -110.5983,
      "lat": 41.7581
    },
    "Neil Simpson II": {
      "lng": -105.3833,
      "lat": 44.2856
    },
    "Neil Simpson II (CT2)": {
      "lng": -105.3786,
      "lat": 44.285
    },
    "Old Faithful": {
      "lng": -110.8347,
      "lat": 44.4533
    },
    "Pilot Butte": {
      "lng": -108.787681,
      "lat": 43.219017
    },
    "Pioneer Wind Park, LLC": {
      "lng": -105.866765,
      "lat": 42.728198
    },
    "Rock River I LLC": {
      "lng": -106.1077,
      "lat": 41.7172
    },
    "Rolling Hills": {
      "lng": -105.8543,
      "lat": 43.0572
    },
    "Roundhouse Wind Energy Project": {
      "lng": -105.107423,
      "lat": 41.095126
    },
    "Seminoe": {
      "lng": -106.908483,
      "lat": 42.156127
    },
    "Seven Mile Hill": {
      "lng": -106.3758,
      "lat": 41.9203
    },
    "Shoshone (WY)": {
      "lng": -109.181228,
      "lat": 44.502897
    },
    "Shute Creek Facility": {
      "lng": -110.0904,
      "lat": 41.8805
    },
    "Silver Sage Windpower": {
      "lng": -105.024,
      "lat": 41.12968
    },
    "Simplot Phosphates": {
      "lng": -109.1328,
      "lat": 41.5417
    },
    "Sinclair Oil Refinery": {
      "lng": -107.10972,
      "lat": 41.77952
    },
    "Spirit Mountain": {
      "lng": -109.129878,
      "lat": 44.512699
    },
    "Strawberry Creek": {
      "lng": -110.893422,
      "lat": 42.905142
    },
    "Sweetwater Solar": {
      "lng": -109.683472,
      "lat": 41.629056
    },
    "Swift Creek": {
      "lng": -110.916592,
      "lat": 42.728439
    },
    "TB Flats": {
      "lng": -106.234141,
      "lat": 42.177245
    },
    "Top of the World Windpower Project": {
      "lng": -105.7872,
      "lat": 42.9258
    },
    "TransAlta Wyoming Wind": {
      "lng": -110.5556,
      "lat": 41.2874
    },
    "Wygen I": {
      "lng": -105.3833,
      "lat": 44.2858
    },
    "Wygen II": {
      "lng": -105.3811,
      "lat": 44.2919
    },
    "Wygen III": {
      "lng": -105.3806,
      "lat": 44.2919
    },
    "Wyodak": {
      "lng": -105.381482,
      "lat": 44.290128
    }
  }

  module.exports = PlantsData