/**
 * @swagger
 * tags:
 *   name: Plants
 *   description: API endpoints for managing power plants
 */

const express = require('express');
const router = express.Router();
const { uploadData, getPlants } = require('./controller');
const multer = require('multer');

const upload = multer({ dest: 'uploads/' });

/**
 * @swagger
 * /upload:
 *   post:
 *     summary: Upload data to the database
 *     tags:
 *       - Plants
 *     consumes:
 *       - multipart/form-data
 *     parameters:
 *       - in: formData
 *         name: file
 *         type: file
 *         description: JSON data file to upload
 *     responses:
 *       200:
 *         description: Data uploaded successfully
 *       500:
 *         description: Failed to upload data to the database
 */
router.post('/upload', upload.single('file'), uploadData);

/**
 * @swagger
 * /plants:
 *   get:
 *     summary: Get plants with optional filters
 *     tags:
 *       - Plants
 *     parameters:
 *       - in: query
 *         name: state
 *         type: string
 *         description: Filter plants by state (optional)
 *       - in: query
 *         name: limit
 *         type: integer
 *         description: Limit the number of plants (optional)
 *     responses:
 *       200:
 *         description: Successfully retrieved plants
 *       500:
 *         description: Failed to fetch plants from the database
 */
router.get('/plants', getPlants);

module.exports = router;
