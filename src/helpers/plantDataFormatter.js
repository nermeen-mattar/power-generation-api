const fs = require('fs');

/* Unused for now */

class PlantDataFormatter {
  static plantformattedData = {};

  static formatData() {
    const filePath = './data/plants-data.json';
    const fileData = fs.readFileSync(filePath, 'utf8');
    const USPlantsData = JSON.parse(fileData);
    USPlantsData.forEach((plant) => {
      this.plantformattedData[plant['Plant name']] = { lng: plant['Plant longitude'], lat: plant['Plant latitude'] };
    });
  }

  static saveFormattedData() {
    const filePath = './data/plants-data.json';
    const formattedData = JSON.stringify(this.plantformattedData, null, 2);
    fs.writeFileSync(filePath, formattedData, 'utf8');
    console.log('Formatted data saved successfully.');
  }
}

// PlantDataFormatter.formatData();
// PlantDataFormatter.saveFormattedData();

module.exports = PlantDataFormatter;
