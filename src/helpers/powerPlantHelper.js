const PlantsData = require('../constants/plantsData');

function getStateTotalNetGeneration(USPowerPlantsData) {
  const stateNetGeneration = {};
  for (const plant of USPowerPlantsData) {
    const stateAbbreviation = plant['Plant state abbreviation'];
    const plantNetGeneration = parseNetGeneration(plant['Generator annual net generation (MWh)']);
    stateNetGeneration[stateAbbreviation] = (stateNetGeneration[stateAbbreviation] || 0) + plantNetGeneration;
  }
  return stateNetGeneration;
}

function addPositionAndPowerPercentageToPlant(USPowerPlantsData, stateNetGeneration) {
  for (const plant of USPowerPlantsData) {
    const stateAbbreviation = plant['Plant state abbreviation'];
    const netGeneration = parseNetGeneration(plant['Generator annual net generation (MWh)']);
    const totalNetGeneration = stateNetGeneration[stateAbbreviation];
    if(totalNetGeneration) {
      plant['Percentage'] = ((netGeneration / totalNetGeneration) * 100).toFixed(2);
    }
    plant['Generator annual net generation (MWh)'] = netGeneration;
    // plant['Position'] = PlantDataFormatter.plantformattedData[plant['Plant name']]; - unused for now
    plant['Position'] = PlantsData[plant['Plant name']];
  }
}

function parseNetGeneration(netGeneration) {
  if (typeof netGeneration === 'string') {
    const absoluteNetGeneration = parseInt(netGeneration.replace(/[(),]/g, ''));
    return Math.abs(absoluteNetGeneration);
  }
  return Math.abs(netGeneration) || 0;
}

module.exports = {
  getStateTotalNetGeneration,
  addPositionAndPowerPercentageToPlant,
};
