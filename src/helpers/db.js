const { MongoClient } = require('mongodb');
require('dotenv').config();

const mongoURL = process.env.MONGO_DB_URL;
const dbName = process.env.DB_NAME;
const collectionName = 'plants';

let dbClient;
let dbCollection;

async function connectToDatabase() {
  if (dbClient && dbClient.isConnected()) {
    return { dbClient, dbCollection };
  }

  dbClient = await MongoClient.connect(mongoURL);
  const db = dbClient.db(dbName);
  dbCollection = db.collection(collectionName);
  return { dbClient, dbCollection };
}

function closeDatabaseConnection() {
  if (dbClient) {
    dbClient.close();
    dbClient = null;
    dbCollection = null;
  }
}

module.exports = {
  connectToDatabase,
  closeDatabaseConnection,
};
