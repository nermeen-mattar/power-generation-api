const { connectToDatabase, closeDatabaseConnection } = require('./helpers/db');
const {
  getStateTotalNetGeneration,
  addPositionAndPowerPercentageToPlant,
} = require('./helpers/powerPlantHelper');
const fs = require('fs');

// Upload data to the database
async function uploadData(req, res) {
  try {
    if (!req.file) {
      return res.status(400).json({ error: 'No file uploaded' });
    }

    const uploadedPowerPlantsFile = fs.readFileSync(req.file.path, 'utf-8');
    const USPowerPlantsData = JSON.parse(uploadedPowerPlantsFile);

    const { dbCollection } = await connectToDatabase();

    await dbCollection.deleteMany({}); // Clean existing data

    // Calculate total net generation for each federal state
    const stateNetGeneration = getStateTotalNetGeneration(USPowerPlantsData);

    addPositionAndPowerPercentageToPlant(USPowerPlantsData, stateNetGeneration);

    await dbCollection.insertMany(USPowerPlantsData);

    closeDatabaseConnection();

    return res.status(200).json({ message: 'Data uploaded successfully' });
  } catch (error) {
    console.error('Error uploading data to the database:', error);
    return res.status(500).json({ error: 'Failed to upload data to the database' });
  }
}

// Get plants with optional filters for state and top N plants
async function getPlants(req, res) {
  const { state, limit } = req.query;
  let filter = {};

  if (state) {
    filter['Plant state abbreviation'] = state;
  }

  try {
    const { dbCollection } = await connectToDatabase();

    let plantsQuery = dbCollection.find(filter);

    if (limit) {
      plantsQuery = plantsQuery.sort({ 'Generator annual net generation (MWh)': -1 }).limit(parseInt(limit));
    }

    const plants = await plantsQuery.toArray();
    closeDatabaseConnection();

    return res.status(200).json(plants);
  } catch (error) {
    console.error('Error fetching plants from the database:', error);
    return res.status(500).json({ error: 'Failed to fetch plants from the database' });
  }
}


module.exports = {
  uploadData,
  getPlants,
};
