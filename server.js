const express = require('express');
const app = express();
const routes = require('./src/routes');
const swaggerSetup = require('./swagger');
const multer = require('multer');
const cors = require('cors');

const port = process.env.PORT || 3000;

// Enable CORS
app.use(
  cors({
    origin: '*',
    methods: ['GET', 'POST'],
    allowedHeaders: ['Content-Type'],
  })
);

app.use('/', routes);

app.listen(port, () => {
  console.log(`Server is running on port ${port}`);
});

// Setup Swagger
swaggerSetup(app);
